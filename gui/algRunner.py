# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 17:00:20 2021

@author: Dunaev


Модуль запуска алгоритмов

При добавлении нового алгоритма, нужно прописать запуск его основной функции.
Для этого в "runAlgorithm" нужно (по аналогии) определить имя алгоритма (задается произвольно)
и соответствующую функцию алгоритма.
По определенному тут имени алгоритма происходит его запуск из скриптов lua
Примечание: Нужно добавить в algorithm/__init__.py импорт соответствующего файла


Взаимодействие с системой испытаний:
    Параметры запуска (рекомендации по структуре алгоритма):
        "START" : перед началом расчета. 
        "CALC"  : Шаг расчета
        "DONE"  : Рассчитать итоговые данные
        "XXX"   : Произвольный шаг, в зависимости нужд алгоритма.

"""

import algorithm


# режим автономной отладки модулей алгоритма
ALG_DEBUG           = 0

calibLastPath = ""

def runAlgorithm(algName, param, debug=0):
    global calibLastPath
    # print("RUN ALG[%s, param: %s, [debug:%d]]" % (algName, param, debug))
    if (algName == "CALIBDIR"):
        if len(param) > 0:
            calibLastPath = param
            result = param
        else:
            result = calibLastPath
    
    if (algName == "RFLeak"):
        result = algorithm.algorithmRFLeak(param, debug)
    if (algName == "RFCalib"):
        result = algorithm.algorithmRFCalib(param, debug)
    if (algName == "RFGen"):
        result = algorithm.algorithmRFGen(param, debug)
    if (algName == "RFNoise"):
        result = algorithm.algorithmRFNoise(param, debug)
    if (algName == "RefAngle"):
        result = algorithm.algorithmRefAngle(param, debug)
    if (algName == "CPYCTEST"):
        result = algorithm.algorithmCPYCTest(param, debug)
    if (algName == "CPRRADC"):
        result = algorithm.algorithmCPRRAdcShow(param, debug)
    if (algName == "EEPROM_TEST"):
        result = algorithm.algorithmCrrEepromTest(param, debug)
    if (algName == "EEPROM_ERASE"):
        result = algorithm.algorithmCrrEepromErase(param, debug)        
    if (algName == "EEPROM_READ"):
        result = algorithm.algorithmCrrEepromRead(param, debug)       
    if (algName == "EEPROM_TEMPCODE_READ"):
        result = algorithm.algorithmCrrTempCodeRead(param, debug)  

    if (algName == "CPRR_OFF_WDT"):
        result = algorithm.algorithmCprrOffWdt(param, debug)  
        
    if (algName == "EEPROM_WRITELEAK"):
        result = algorithm.algorithmCrrEepromWriteLeak(param, debug)
    if (algName == "EEPROM_WRITECALIB"):
        result = algorithm.algorithmCrrEepromWriteCalib(param, debug)
        
    if (algName == "TELNET_SET_NET_PARAM"):
        result = algorithm.algorithmCprrNetSettings(param, debug)
        
    if (algName == "TELNET_GET_SN"):
        result = algorithm.algorithmCprrGetSN(param, debug)
        
    if (algName == "CPYCAngle"):
        result = algorithm.algorithmCPYCAngle(param, debug)    
    if (algName == "CPYCCANDrv"):
        result = algorithm.algorithmCPYCCAN(param, debug)  
    if (algName == "CPYCCALIB"):
        result = algorithm.algorithmCPYCCalib(param, debug)  
    if (algName == "CPYCNOISE"):
        result = algorithm.algorithmCPYCNoise(param, debug)  
    try:
        return result
    except:
        pass


# алгоритмы, которые требуется останавливать при выходе из программы
def stopAlgorithm():
    algorithm.algorithmCPYCCAN("DONE", debug = 0)  
    


if ALG_DEBUG == 1:
    print("\n !!! Режим отладки алгоритма !!! \n")
#    runAlgorithm("RFGen", "START", debug=1)
#    runAlgorithm("RFGen", "CALC", debug=1)
#    runAlgorithm("RFGen", "DONE", debug=1)

#    runAlgorithm("RFNoise", "START", debug=1)
#    runAlgorithm("RFNoise", "CALC", debug=1)
#    runAlgorithm("RFNoise", "DONE", debug=1)
    
    # runAlgorithm("RefAngle", "START", debug=1)
    # runAlgorithm("RefAngle", "CONFIG", debug=1)
    # for i in range(25):
    #     runAlgorithm("RefAngle", "CALC", debug=1)
    # runAlgorithm("RefAngle", "DONE", debug=1)

    # runAlgorithm("CPYCTEST", "START", debug=1)
    # runAlgorithm("CPYCTEST", "CALC", debug=1)
    # runAlgorithm("CPYCTEST", "DONE", debug=1)
    
    runAlgorithm("CPRRADC", "START", debug=1)
    runAlgorithm("CPRRADC", "CALC", debug=1)
    runAlgorithm("CPRRADC", "DONE", debug=1)
    
