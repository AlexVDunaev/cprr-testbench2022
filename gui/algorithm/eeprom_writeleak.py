# -*- coding: utf-8 -*-
"""
Created on  02.06.2022

@author: Rodionov GV

Запись вектора калибровки, пролаза и калибровки по умолчанию.

"""
import sys
import os

scriptName = os.path.basename(__file__)
curScriptPath = os.path.abspath(__file__).replace(scriptName, '')
sys.path.insert(0, os.path.join(curScriptPath, 'eeprom_test_files'))
file_path = "logging_config.ini"
file_path = os.path.join(os.getcwd(), 'eeprom_test_files')

from cprrControl import CPRR_load_leak_calib

# Функция алгоритма.
def algorithmCrrEepromWriteLeak(param, debug=0):
    if (param == "START"):
        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/"
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if (param == "DONE"):
        pass
        
    if (param == "CALC"):
        print("----------- CALC START ------------- {0}".format('-'))
        result =  CPRR_load_leak_calib()
        
        print("----------- EEPROM_WRITE algRunner.py ------------- {0}".format(result))
        return result
        
    return "OK"

# режим автономной отладки модуля через algRunner.py 
if __name__ == '__main__':
    algorithmCrrEepromWriteLeak("START", debug=1)
    result = algorithmCrrEepromWriteLeak("CALC", debug=1)
    print('main output: ' + result)
    algorithmCrrEepromWriteLeak("DONE", debug=1)
# ALG_DEBUG           = 1
#
