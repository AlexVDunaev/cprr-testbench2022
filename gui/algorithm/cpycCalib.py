# -*- coding: utf-8 -*-
"""
Created 2022.06.20

@author: Dunaev


Алгоритм "Калибровка":


"""
import os
import numpy as np
import matplotlib.pyplot as plt
import mmap
import math
import array
from shutil import copyfile
import json

import sys
sys.path.append(os.path.abspath("./algorithm/"))
from algCornerRes import prs_s1, prs_s2, prs_s3, win_nfft


# Параметры формата данных (как в алгоритме матлаба)
CHIRP_SAMPLE_NUM    = 256
RADAR_RX_CHANNEL    = 4
RADAR_TX_CHANNEL    = 3
MIMO_CHANNEL_NUM    = RADAR_RX_CHANNEL * RADAR_TX_CHANNEL

CHIRP_NUM           = 128
FFT1_SIZE           = 256
FFT3_SIZE           = 1024
# коэф. перевода в дБ (хотя Александра сказала что это неверные дБ)
FFT_SCALE           = CHIRP_SAMPLE_NUM*(2**15)*CHIRP_NUM*MIMO_CHANNEL_NUM

M_PI                = 3.14159265358979

# Возможность нормировать калиб. коэф. к единице (значение re и im <= 1.0)
CALIB_USE_NORM      = 0 # 1: используется нормировка, 0: не используется

# путь к GUI (Сводный график используется для GUI)
DIRECTORY_GUI_PNG = "./design"
# относительный путь к рабочей директории
DIRECTORY_WORK = "./algorithm/workdir"
# относительный путь к директории с отладочн. массивами
DIRECTORY_DEBUG = "./algorithm/dbg/rAngle"
# относительный путь к директории параметров
DIRECTORY_CONFIG = "./algorithm/config"
# файл калибровки радара
RADAR_CALIB_FILE = "radarCalib.json"

# формат сетки графика
Y_SCALE = "linear"  # ["log" | "linear"]
# для простоты задал константой


def algorithmPrint(text, error=0):
    print("algorithm[CALIB]>", text)
    pass


def mmap_io(filename):
    with open(filename, mode="rb") as file_obj:
        with mmap.mmap(file_obj.fileno(), length=0, access=mmap.ACCESS_READ) as mmap_obj:
            buff = mmap_obj.read()
            return buff



# Функция алгоритма.
def algorithmCPYCCalib(param, debug=0):
    # радочая директори, куда скрипт испытаний (Lua) кладет данные
    global workDirectory
    global calibration_coef
    global config
    global phase_dist
    global number_pd


    if (param == "TEST"):
        return "TEST-OK"

    if (param == "START"):
        config = None
        calibration_coef = [complex(1, 0)] * MIMO_CHANNEL_NUM
        phase_dist = [complex(0, 0)] * MIMO_CHANNEL_NUM
        number_pd = 0

        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/" + DIRECTORY_WORK
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if (param == "CLEAR"):
        # удалить файл конфигурации радара, если он там есть
        # конфигурация должна вычитываться из радара на шаге "CONFIG"
        print("Delete file:", workDirectory + "/" + RADAR_CALIB_FILE)
        os.remove(workDirectory + "/" + RADAR_CALIB_FILE)
        return "OK"


    # Шаг получения кадра данных (может быть несколько)
    if (param == "CALC"):
        fileName = "%s/calib.bin" % workDirectory
        if debug > 0:
            # загружаю данные из файла
            fileName = "%s/refcpycAngle.bin%d" % (workDirectory, debug - 1)

        #print(fileName)
        # Получаем данные от системы испытаний.
        # (полный кадр стрима, включая заголовок)
        data = mmap_io(fileName)

        adc_raw_array = array.array('h', data)

        # проверка на корректность размера данных
        if (len(adc_raw_array) != 2*CHIRP_SAMPLE_NUM * CHIRP_NUM * RADAR_RX_CHANNEL):
            algorithmPrint("DATA FORMAT ERROR!!!", error=1)
            return "ABORT"

        # разбивка массива int16*2 на список чирпов
        adc = np.array(adc_raw_array).reshape(CHIRP_NUM*RADAR_RX_CHANNEL*CHIRP_SAMPLE_NUM,2)
        # перевод к комплексному виду
        adc = adc[...,0] + 1j*adc[...,1];
        # разбивка на чирпы (adc[128][4][256])
        chirpArray = np.array(adc).reshape(CHIRP_NUM, CHIRP_SAMPLE_NUM, RADAR_RX_CHANNEL)

        cube = np.zeros((CHIRP_NUM, MIMO_CHANNEL_NUM, FFT1_SIZE), dtype=complex)
        
        for chirp in range(CHIRP_NUM):
            for rx in range(RADAR_RX_CHANNEL):
                fft1 = np.fft.fft(np.multiply(chirpArray[chirp, :, rx], win_nfft))
                cube[chirp, 0 + rx, :] = fft1 * prs_s1[chirp]
                cube[chirp, 4 + rx, :] = fft1 * prs_s2[chirp]
                cube[chirp, 8 + rx, :] = fft1 * prs_s3[chirp]

                    
        fft2 = np.fft.fft(cube, axis=0)

        # найдем максимум в 0-м доплере (большими усилиями обнаружил, что в argmax нужно подавать модуль, иначе есть отличия от матлаба)
        fft2_zeroDoppler = np.abs(fft2[0, 0, :]) #20*np.log10(np.abs(fft2[0][0]))
        # все что выходит за пределы БЭК (с запасом) сделаем шумом
        fft2_zeroDoppler[0:7] = 1E-16
        fft2_zeroDoppler[64:] = 1E-16
        # поиск максимального значения в нулевом доплере
        maxRIndex = np.argmax(fft2_zeroDoppler)        
        
        phase_distribution = fft2[0, :, maxRIndex]
        phase = np.mean(np.angle(phase_distribution))
        
        rotateValue = np.exp(complex(0, -1*phase))
        phase_distribution = phase_distribution * rotateValue;
        phase_dist = phase_dist + phase_distribution
        number_pd = number_pd + 1
        # print(phase_dist)
        algorithmPrint("[CALC] maxRIndex: %d, phase: %f" % (maxRIndex, phase))
        
        return ("%.2f %d" % (phase, maxRIndex))

    if param.find("DONE") >= 0: # Формат : "UPLOAD:filename.json"
        filename = workDirectory +"/" + RADAR_CALIB_FILE
        if param.find(":") > 0:
            filename = param[param.find(":") + 1:]
        algorithmPrint("DONE")
        print("------------------------------")
        print("Calibration Done. Frames :", number_pd)
        print("------------------------------")
        if number_pd > 0:
            phase_dist = phase_dist / number_pd
            calib = phase_dist[(MIMO_CHANNEL_NUM//2) - 1] / phase_dist
            if CALIB_USE_NORM == 1:
                # Если нужно приводить коэф. калибровки к 1,
                normK = max([max(np.abs(np.real(calib))), max(np.abs(np.imag(calib)))])
                calib = calib / normK
            
            calibObj = {}
            mode = ""
            # Если в имени файла указаг признак режима, то в именах параметров нужно явно указать MIMOx
            if param.find("FIELD.json") >= 0:
                mode = "MIMO0: "
            if param.find("NEAR.json") >= 0:
                mode = "MIMO1: "
            if param.find("FAR.json") >= 0:
                mode = "MIMO2: "
                
            for i in range(MIMO_CHANNEL_NUM):
                print("[%.5f, %.5f]" % (np.real(calib[i]), np.imag(calib[i])))
                # "MIMO0: calib[%d].im"
                calibObj["%scalib[%d].re" % (mode, i)] = round(np.real(calib[i]), 8)
                calibObj["%scalib[%d].im" % (mode, i)] = round(np.imag(calib[i]), 8)
            try:
                print("Write to file: ", filename)
                with open(filename, 'w', encoding='utf-8') as f:
                    f.write(json.dumps(calibObj, ensure_ascii=False).replace(",", ",\n"))
            except  OSError as e:
                print('[File:"{}"] Error: {}'.format(filename, e.strerror))
                return "ERROR"
        return "OK"

if __name__ == '__main__':
    global workDirectory
    
    DIRECTORY_GUI_PNG = "." + DIRECTORY_GUI_PNG
    DIRECTORY_CONFIG = "." + DIRECTORY_CONFIG
    algorithmCPYCCalib("START")
    workDirectory = "./workdir"
    # algorithmCPYCCalib("CALC", 1)
    for n in range(21):
        algorithmCPYCCalib("CALC", n + 1)
    algorithmCPYCCalib("DONE")

