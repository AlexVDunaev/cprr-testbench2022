# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 09:57:51 2022

@author: Dunaev
"""


import re
import os
import array
import json

# import sys
# sys.path.append(os.path.abspath("./algorithm/"))
# from algCornerRes import prs_s1, prs_s2, prs_s3, win_nfft

# относительный путь к текущей директории
DIRECTORY_CANDRV = "./algorithm/"

# относительный путь к рабочей директории
DIRECTORY_WORK = "./algorithm/workdir"
# относительный путь к директории с отладочн. массивами
DIRECTORY_DEBUG = "./algorithm/dbg/rAngle"
# относительный путь к директории параметров
DIRECTORY_CONFIG = "./algorithm/config"

workDirectory = ""

temperatureOfRadar = None
radarConfigObject = None
subscriberSpn = None
subscriberSpnValue = ""
radarTargetsList = []
radarTargetsFlag = None
radarSerialNumber = "00000000"


from time import sleep

# import sys
# sys.path.append(os.path.abspath("./j1939driver/"))

try:
    # импорт при запуске как отдельного файла
    from j1939driver import j1939DriverStart, j1939DriverStop, j1939DriverSetEvents, j1939DriverSetSPN, j1939DriverSetWorkDir, j1939DriverGetSpnList
except ImportError:
    # импорт при запуске как отдельного файла
    import sys
    sys.path.append(os.path.abspath("./j1939driver/"))
    from j1939driver.j1939driver import j1939DriverStart, j1939DriverStop, j1939DriverSetEvents, j1939DriverSetSPN, j1939DriverSetWorkDir, j1939DriverGetSpnList
    

def userErrorEvent(text):
    print("ERROR!>", text)

def userNotifyEvent(notifyObject):
    global subscriberSpn
    global subscriberSpnValue
    # print(notifyObject)
    if subscriberSpn is not None:
        if notifyObject["Name"] == ("SPN%d" % subscriberSpn):
            print("subscriber>", notifyObject["Name"], notifyObject["Value"])
            subscriberSpn = None
            subscriberSpnValue = notifyObject["Value"]
            
    pass
    
def userGetConfigEvent(configObject):
    global workDirectory
    global configWriteDone
    global radarConfigObject
    global radarSerialNumber
    
    if configObject is not None:
        if len(configObject) > 0:
            radarConfigObject = configObject
            sn = radarConfigObject["SN"]
            radarSerialNumber = "%.9d" % int(sn)
            print("SN:", radarSerialNumber)
            with open('%s/radarConfig.json' % (workDirectory), 'w', encoding='utf-8') as f:
                f.write(json.dumps(radarConfigObject, ensure_ascii=False).replace(",", ",\n"))
            configWriteDone = True

    print(configObject)
    
def userGetTargetsEvent(targetsList):
    global radarTargetsList
    global radarTargetsFlag
    radarTargetsList = targetsList
    radarTargetsFlag = 1
    pass
    
def userGetStatusEvent(status):
    global temperatureOfRadar
    statusValue, maxTemper = status
    temperatureOfRadar = maxTemper
    print(statusValue, maxTemper)
    
def targetToStr(targets):
    strResult = ""
    for t in targets:
        strResult = strResult + str(t['id']) + " "
        strResult = strResult + str(t['x']) + " "
        strResult = strResult + str(t['y']) + " "
        strResult = strResult + str(t['vx']) + " "
        strResult = strResult + str(t['vy']) + " "
        strResult = strResult + str(t['a']) + " "
        strResult = strResult + str(t['amp']) + " "
    if len(targets) > 0:
        strResult = strResult[:-1]
    return strResult 


# Функция алгоритма.
def algorithmCPYCCAN(param, debug=0):
    # радочая директори, куда скрипт испытаний (Lua) кладет данные
    global workDirectory
    global calibration_coef
    global subscriberSpn
    global subscriberSpnValue
    global radarConfigObject
    global radarTargetsList
    global radarTargetsFlag
    global radarSerialNumber
    
    if (param == "SN"):
        return radarSerialNumber
    

    if (param == "TEMP"):
        global temperatureOfRadar
        temperatureOfRadar = None
        for _ in range(5*2):
            sleep(0.5)
            if temperatureOfRadar is not None:
                print("STATUS OK!!!!!!!!!!!")
                return "%d" % temperatureOfRadar
        
        return "ERROR"

    if (param == "START"):
        j1939DriverSetEvents(StatusEvent = userGetStatusEvent, 
                              TargetsEvent = userGetTargetsEvent,
                              NotifyEvent = userNotifyEvent,
                              ConfigEvent = userGetConfigEvent,
                              ErrorEvent = userErrorEvent)
        
        # задаем имя файла
        j1939DriverStart(spnlistFileName = DIRECTORY_CANDRV + "j1939driver/spnlist.txt") #j1939driver
        # Для корректной записи стрима, зададим путь к Tbtool.exe
        j1939DriverSetWorkDir('../core/src/')
        
        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/" + DIRECTORY_WORK
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)

        print("workDirectory: ", workDirectory)        
        return workDirectory

    # "TARGET:ALL" | MAXAPML 
    if param.find("TARGET") >= 0: 
        print("MODE>", param)
        radarTargetsFlag = None
        # 3 sec timeOut
        for _ in range(3*100):
            sleep(0.01)
            if radarTargetsFlag is not None:
                break
        if radarTargetsFlag is None:
            return "ERROR"
        if param.find(":ALL") >= 0:
            return targetToStr(radarTargetsList)
        return ""
    

    if param.find("CONFIG") >= 0: # "CONFIG[SPN1,SPN34 , SPN14]"
        global configWriteDone

        configWriteDone = None
        j1939DriverSetSPN(60001, 1)
        # ждем 60 сек получения конфига радара
        for _ in range(60):
            sleep(1)
            if configWriteDone is not None:
                result = "OK"
                reqList = re.compile('\((.*)\)').findall(param)
                if reqList:
                    # Если задан список (а он не обязателен), выдача значений
                    spnList = reqList[0].replace(" ", "").split(",")
                    result = ""
                    driverSpnList = j1939DriverGetSpnList()
                    # В объекте конфигурации нет id spn, только имя!
                    # для преобразования имени в ID spn нужен SpnList драйвера
                    for spn in spnList:
                        idOfSpn = re.compile('SPN(\d+)').findall(spn)
                        if idOfSpn:
                            idOfSpn[0]
                            for info in driverSpnList:
                                if info['id'] == idOfSpn[0]:
                                    if len(result) > 0:
                                        result = result + " "
                                    result = result + "{}".format(radarConfigObject[info['name']])
                                    print(radarConfigObject[info['name']])

                print("OK! result-> [%s]" % result)
                return result
        print("TIMEOUT")
        return "ERROR"

    if (param.find("WRITE") >= 0): # Формат : "WRITE:SPN1234 = 33.3"
        kv = re.compile(':SPN(\d+)\s*=\s*(\S*)').findall(param)
        if kv:
            print("-------------------------------------------------------")
            print(kv)
            (spn, value) = (int(kv[0][0]), kv[0][1])
            print("WRITE: ", spn, value)
            print("-------------------------------------------------------")
            j1939DriverSetSPN(spn, value)
            print("-------------------------------------------------------")

# -------------------------------------------------------
# [('1011', '0')]
# 1011 0
# -------------------------------------------------------
# 1011 is not in list


    if (param.find("READ") >= 0): # Формат : "READ:SPN1234"
        spn = re.compile(':SPN(\d+)').findall(param)
        if spn:
            subscriberSpnValue = ""
            subscriberSpn = int(spn[0])
            for _ in range(10):
                print("WAIT... SPN", subscriberSpn)
                sleep(1)
                if subscriberSpn is None:
                    print(spn[0], subscriberSpnValue)
                    return "%s" % subscriberSpnValue
        print("TIMEOUT")
        return "ERROR"
            


    if param.find("UPLOAD") >= 0: # Формат : "UPLOAD:filename.json"
        if param.find(":") > 0:
            filename = "file://" + DIRECTORY_CANDRV + param[param.find(":") + 1:]
            print("UPLOAD File: ", filename)
            j1939DriverSetSPN(100001, filename)
        return "OK"

    # Шаг получения кадра данных (может быть несколько)
    if (param == "CALC"):
        print("CALC")
        return ("%.2f %d" % (0, 0))

    if (param == "DONE"):
        print("[j1939Drv] DONE")
        j1939DriverStop()
        return "OK"

if __name__ == '__main__':
    
    DIRECTORY_CONFIG = "." + DIRECTORY_CONFIG
    algorithmCPYCCAN("START")
    j1939DriverSetWorkDir('../../core/src/')
    DIRECTORY_CANDRV = "./"
    try:
        workDirectory = "./workdir"
        sleep(5)
        # algorithmCPYCCAN("CONFIG")
        algorithmCPYCCAN("CONFIG(SPN4400)")
        # algorithmCPYCCAN("CONFIG(SPN4400, SPN4401, SPN1011)")
        # algorithmCPYCCAN("WRITE:SPN1111=0")
        # algorithmCPYCCAN("WRITE:SPN60002=1")
        # print("READ RESULT>", algorithmCPYCCAN("READ:SPN60110"))
        # sleep(5)
        # algorithmCPYCCAN("WRITE:SPN60002=0")
        sleep(1)
    finally:
        algorithmCPYCCAN("DONE")

