# -*- coding: utf-8 -*-
"""
Created on  27.06.2022

@author: Rodionov GV

Запись IP адреса и PORT по умолчанию.
Получение серийного номера SN
"""
import sys
import os

scriptName = os.path.basename(__file__)
curScriptPath = os.path.abspath(__file__).replace(scriptName, '')

sys.path.insert(0, os.path.join(curScriptPath, 'eeprom_test_files'))

file_path = "logging_config.ini"
file_path = os.path.join(os.getcwd(), 'eeprom_test_files')

from test_EEPROM import cprr_ip_port_life, cprr_get_SN

# Функция алгоритма.
def algorithmCprrNetSettings(param, debug=0):
    if (param == "START"):
        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/"
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if (param == "DONE"):
        pass
        
    if (param == "CALC"):
        result =  cprr_ip_port_life()
        
        print("----------- SET NEW IP PORT: %s ------------- " % result)
        return result
        
    return "OK"

# Функция алгоритма.
def algorithmCprrGetSN(param, debug=0):
    if (param == "START"):
        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/"
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if (param == "DONE"):
        pass
        
    if (param == "CALC"):
        result =  cprr_get_SN()
        return result
        
    return "OK"
    
# режим автономной отладки модуля через algRunner.py 
if __name__ == '__main__':
    # algorithmCprrNetSettings("START", debug=1)
    # result = algorithmCprrNetSettings("CALC", debug=1)
    # print('main output: ' + str(result))
    # algorithmCprrNetSettings("DONE", debug=1)
    
    # algorithmCprrNetSettings("CALC", debug=1)
    
    algorithmCprrGetSN("START", debug=1)
    result = algorithmCprrGetSN("CALC", debug=1)
    print('main output: ' + str(result))
    algorithmCprrGetSN("DONE", debug=1)
    
    algorithmCprrGetSN("CALC", debug=1)    
# ALG_DEBUG           = 1
#
