# -*- coding: utf-8 -*-
"""
Created on  02.06.2022

@author: Rodionov GV

Чтение вектора калибровки.

"""
import sys
import os

scriptName = os.path.basename(__file__)
curScriptPath = os.path.abspath(__file__).replace(scriptName, '')

sys.path.insert(0, os.path.join(curScriptPath, 'eeprom_test_files'))

file_path = "logging_config.ini"
file_path = os.path.join(os.getcwd(), 'eeprom_test_files')

from test_EEPROM import read_eeprom, read_tempCode

# Чтение температурного вектора из EEPROM.
def algorithmCrrEepromRead(param, debug=0):
    if (param == "START"):
        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/"
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if (param == "DONE"):
        pass
        
    if (param == "CALC"):
        result =  read_eeprom()
        
        print("----------- EEPROM_READ algRunner.py ------------- {0}".format(result))
        return result
        
    return "OK"


# Чтение температурного кода.
def algorithmCrrTempCodeRead(param, debug=0):
    if (param == "START"):
        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/"
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if (param == "DONE"):
        return "OK"
        
    if (param == "CALC"):
        result =  read_tempCode()
        
        print("----------- EEPROM_READ algRunner.py ------------- {0}".format(result))
        return result
        
    return "OK"

# режим автономной отладки модуля через algRunner.py 
if __name__ == '__main__':
    algorithmCrrEepromRead("START", debug=1)
    result = algorithmCrrEepromRead("CALC", debug=1)
    print('main output: ' + str(result))
    algorithmCrrEepromRead("DONE", debug=1)
    
    algorithmCrrTempCodeRead("CALC", debug=1)
# ALG_DEBUG           = 1
#
