# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 16:32:36 2021

@author: Dunaev
"""

import socket
import time
import selectors
import logging
from   threading import Thread


#  Класс-обертка для создания потоков
class MyThread(Thread):
    def __init__(self, name, func):
        Thread.__init__(self)
        self.name = name
        self.func = func

    def run(self):
        # print('Thread start: %s' % self.name)
        self.func()
# ------------------------------------------------------------------------------

# За основу работы взял модуль с github. Добавил корректное закрытие сокетов и 
# и сделал проброс событий приема данных
# rxDataEvent : событие получения данных по сокету (возможность ответа в сокет)
# tcpServiceEvent : сервисное событие (подсоединение/отключение клиента)

# https://github.com/eliben/python3-samples/blob/master/async/selectors-async-tcp-server.py
# 
class SelectorServer:
    def __init__(self, host, port, event, serviceEvent):
        # Create the main socket that accepts incoming connections and start
        # listening. The socket is nonblocking.
        self.rxEvent = event
        self.serviceEvent = serviceEvent
        self.main_socket = socket.socket()
        # добавил опцию SO_REUSEADDR. как же без нее?!
        self.main_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.main_socket.bind((host, port))
        self.main_socket.listen(100)
        self.main_socket.setblocking(False)

        # Create the selector object that will dispatch events. Register
        # interest in read events, that include incoming connections.
        # The handler method is passed in data so we can fetch it in
        # serve_forever.
        self.selector = selectors.DefaultSelector()
        self.selector.register(fileobj=self.main_socket,
                               events=selectors.EVENT_READ,
                               data=self.on_accept)

        # Keeps track of the peers currently connected. Maps socket fd to
        # peer name.
        self.current_peers = {}

    def on_accept(self, sock, mask):
        # This is a handler for the main_socket which is now listening, so we
        # know it's ready to accept a new connection.
        conn, addr = self.main_socket.accept()
        logging.info('accepted connection from {0}'.format(addr))
        conn.setblocking(False)

        self.current_peers[conn.fileno()] = conn #.getpeername()
        
        # Register interest in read events on the new socket, dispatching to
        # self.on_read
        self.selector.register(fileobj=conn, events=selectors.EVENT_READ,
                               data=self.on_read)
        if self.serviceEvent:
            self.serviceEvent(conn.getpeername(), 'accepted')

    def close_connection(self, conn, textInfo):
        # We can't ask conn for getpeername() here, because the peer may no
        # longer exist (hung up); instead we use our own mapping of socket
        # fds to peer names - our socket fd is still open.
        getpeername = conn.getpeername()
        logging.info('closing connection to {0}'.format(getpeername))
        if self.serviceEvent:
            self.serviceEvent(getpeername, textInfo)
        
        del self.current_peers[conn.fileno()]
        self.selector.unregister(conn)
        conn.close()

    def on_read(self, conn, mask):
        # This is a handler for peer sockets - it's called when there's new
        # data.
        logging.debug("on_read()>")
        try:
            data = conn.recv(4096)
            if data:
                peername = conn.getpeername()
                logging.debug('got data from {}: {!r}'.format(peername, data))
                # Assume for simplicity that send won't block
                if self.rxEvent:
                    answer=self.rxEvent(conn, data)
                    if answer:
                        conn.send(answer)
            else:
                self.close_connection(conn, 'close')
        except ConnectionResetError:
            self.close_connection(conn, 'error')
            logging.exception("on_read()>except!")

    def serverPoll(self):
        last_report_time = time.time()
        global isTcpServerUp
        logging.debug("serverPoll> Start")    
        while isTcpServerUp:
            # Wait until some registered socket becomes ready. This will block
            # for 200 ms.
            events = self.selector.select(timeout=0.2)

            # For each new event, dispatch to its handler
            for key, mask in events:
                handler = key.data
                handler(key.fileobj, mask)

            # This part happens roughly every second.
            cur_time = time.time()
            if cur_time - last_report_time > 5:
                logging.info('Num active peers = {0}'.format(len(self.current_peers)))
                last_report_time = cur_time


        logging.info('Close clients...')
        for connid in self.current_peers:
            logging.warning('Close {}'.format(connid))
            conn = self.current_peers[connid]
            self.selector.unregister(conn)
            conn.close()
        logging.debug("Close main_socket...")     
        self.main_socket.close()

#  поток обслуживания сокета
def tcpServerThread():
    global serverObject
    serverObject.serverPoll()
    logging.debug("tcpServerThread> done")    

def tcpServerExit():     
    global tcpServer_thread
    global isTcpServerUp
    logging.info('tcpServerExit()...')
    isTcpServerUp = False  # флаг окончания работы
    tcpServer_thread.join()
    logging.info('server is closed')
    logging.info('Stop logging')
    logging.shutdown()
    
# посылаем всем клиентам данные
def tcpServerSend(data):        
    global serverObject
    try:
        current_peers = serverObject.current_peers
        for connid in current_peers:
            logging.debug('Client->Send({!r})'.format(data))
            conn = serverObject.current_peers[connid]
            conn.send(data)
    except socket.timeout:
        logging.error('Send>Timeout!')
        pass
    except Exception as e:
        logging.exception('Send>Exception!--> Close sock!')
        serverObject.close_connection(conn, 'Send>Exception!')
        pass

            
def tcpServer(port, rxDataEvent, tcpServiceEvent):        
    global tcpServer_thread
    global isTcpServerUp
    global serverObject
    # Настройка логера
    logging.root.handlers = []
    
    logging.basicConfig(
         filename='tcpServer.log',
         # level=logging.DEBUG,
         level=logging.WARNING,
         format= '[%(asctime)s] %(levelname)s - %(message)s',
         datefmt='%H:%M:%S'
     )
    
    logging.info('Start logging')
    serverObject = SelectorServer(host='localhost', port=port, event=rxDataEvent, serviceEvent=tcpServiceEvent)
    isTcpServerUp = True
    tcpServer_thread = MyThread('tcpServer', tcpServerThread)
    tcpServer_thread.start()
    logging.info('tcpServer start on {}...'.format(port))
        
         
    

    