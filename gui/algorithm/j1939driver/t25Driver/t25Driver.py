# -*- coding: utf-8 -*-
"""
Created on 07.06.2022

@author: Dunaev
"""


import socket
from time import sleep
import logging
from   threading import Thread
import struct
import math
from enum import Enum

T25_TARGETS_LIMIT = 85  # древний баг t25imit. Если целей больше 85 нужно бить
                        # на пакеты по 85 целей. Если Целей всего 85, нужно либо одну отбросить,
                        # либо добавить (и передать в другом пакете)

T25_IMIT_TIMEOUT = 3    # Интервал ожидания любых данных от t25imit
                        # Если таймаут превышен, посылаем сообщение "версия"

# Значения параметра в событии приема
class t25Param(Enum):
    ENABLE  = 0  # Команда "ENABLE". 1/0. Выдается при вкл/откл записи в DAQ
    SPEED   = 1  # Передача значения скорости платформы
    YAW     = 2  # Передача значения уговой скорости
    FORWARD = 3  # Вперед/Назад/Остановка


#  Класс-обертка для создания потоков
class ctrlThread(Thread):
    def __init__(self, name, func):
        Thread.__init__(self)
        self.name = name
        self.func = func
    def __del__(self):
        pass
    def run(self):
        self.func()



# t25Driver(host, port, protocolVer, radarVer, rxEvent)
#  host - IP адрес где выполняется t25imit
#  port - порт t25imit (задается параметром "-t" см. t25imit --help)
#  protocolVer - номер версии протокола (используется 1)
#  radarVer - номер версии радара (у Сергея Ч. было значение 10)
#  rxEvent - Событие пользователя, которое будет получать команды от t25imit
class t25Driver:
    def sockWrite(self, data):
        sock = self.sock
        if sock is not None:
            try:
                sock.sendto(data, (self.host, self.port))
            except socket.error as error:
                logging.error("t25Driver.sendto([%s:%d]): Error[%s]" % (self.host, self.port, error))
                self.sock = None
    
    def sendVersion(self):
        pkt = struct.pack("<qq", self.protocolVer, self.radarVer)
        self.sockWrite(pkt)

    def parseData(self, data):
        if len(data) == 8:
            enable = struct.unpack_from( "<q", data, 0)[0]
            self.rxEvent(t25Param.ENABLE, enable)
        if len(data) == 3*8:
            speed, yaw, forward = struct.unpack_from( "<ddq", data, 0)
            self.rxEvent(t25Param.SPEED, speed)
            self.rxEvent(t25Param.YAW, yaw)
            self.rxEvent(t25Param.FORWARD, forward)
        
    # Функция ожидания приема данных. 
    # Выход при ошибке или doExit. Возврат: FALSE: сокет закрыт, True : сокет еще открыт
    def sockReading(self, sock):
        sock.settimeout(T25_IMIT_TIMEOUT)
        self.sendVersion()
        # reading...                    
        while self.doExit == 0:
            try:
                (data, fromip) = sock.recvfrom(1024)
                if not data:
                    self.sock = None
                    sock.close()
                    return False
                else:
                    self.parseData(data)
            except socket.timeout:
                logging.debug("sockReading>timeout[%s:%d]..." % (self.host, self.port))
                self.sendVersion()
                # Более жесткая альтернатива: при отсутствии ответов сбросить сокет.
                # return True
        return True
    
    def udpClientThread(self):
        logging.debug("udpClientThread>[%s:%d]." % (self.host, self.port))
        while self.doExit == 0:
            sock = socket.socket(socket.AF_INET,  socket.SOCK_DGRAM) 
            sock.setblocking(False)
            try:
                # udp connect (просто связывание сокета и порта назначения)
                # sock.connect((self.host, self.port))
                if self.port == 7777:
                    print("t25Driver.DebugPort. Local port: 7778!")
                    sock.bind(("127.0.0.1", self.port + 1))
                
                self.sock = sock
                if self.sockReading(sock):
                    self.sock = None
                    sock.close()
            except socket.error as error:
                logging.error("t25Driver.udpClientThread>[%s:%d]: Error[%s]" % (self.host, self.port, error))
                self.sock = None
                sock.close()
                sleep(0.5)
        logging.debug("udpClientThread>[%s:%d].done" % (self.host, self.port))
        pass    
    
    def __init__(self, host, port, protocolVer, radarVer, rxEvent):
        self.host = host
        self.port = port
        self.rxEvent = rxEvent
        self.protocolVer = protocolVer
        self.radarVer = radarVer
        self.doExit = 0
        self.sock = None
        logging.debug("t25Driver.Init[%s:%d]." % (host, port))    
            
        self.thread = ctrlThread("[%s:%d]" % (host, port), self.udpClientThread)
        self.thread.start()        
        
    def __del__(self):
        logging.debug("t25Driver.DEL[%s:%d]." % (self.host, self.port))
        if self.doExit == 0:
            self.close()

    def getHost(self):
        return self.host

    def getPort(self):
        return self.port

    def getSock(self):
        return self.sock

    def close(self):
        logging.debug("t25Driver.Close[%s:%d]..." % (self.host, self.port))
        self.doExit = 1
        self.thread.join(5)
        logging.debug("t25Driver.Close[%s:%d]...Done" % (self.host, self.port))

        
    def sendTargets(self, timeStamp, grubNum, targets):
        logging.debug("t25Driver.sendTargets[%s:%d]..." % (self.host, self.port))
        # print("sendTargets. len:>", len(targets))
        if self.sock is not None:
            try:
                tgBuffer = struct.pack("<qq", grubNum, timeStamp)
                for n, t in enumerate(targets):
                    i = t['id']
                    x = t['x']
                    y = t['y']
                    a = t['a']
                    amp = t['amp']
                    vx = t['vx']
                    vy = t['vy']
                    if y <= 0:
                        y = 0.1
                    tgData = struct.pack("<qddqqddddddd", 
                            i,   # id
                            math.sqrt(x**2 + y**2), # range (не используется)
                            math.atan(x / y)*180.0 / 3.141592, # azimuth (не используется)
                            555, # live_time (нет аналогичного параметра в corner77)
                            0,   # prediction_time (нет аналогичного параметра в corner77)
                            amp, # rcs
                            x,   # Xcord
                            vx,  # Xrate
                            0,   # Xacceleration (нет отдельного ускорения по Y/X)
                            y,   # Ycord
                            vy,  # Yrate
                            a    # Yacceleration
                            )
                    tgBuffer = tgBuffer + tgData
                    # Если число целей превышает 85 (magic) разбиваем посылку на части
                    if (n + 1) % T25_TARGETS_LIMIT == 0:
                        self.sockWrite(tgBuffer)
                        tgBuffer = struct.pack("<qq", grubNum, timeStamp)
                        
                self.sockWrite(tgBuffer)
                        
            except socket.error as error:
                logging.error("t25Driver.sendTargets[%s:%d]: Error[%s]" % (self.host, self.port, error))

## ----------------------------------------------------------------------------

def democode():
    # Настройка логера
    logging.root.handlers = []
    
    logging.basicConfig(
         filename='t25Driver.log',
         level=logging.DEBUG,
         # level=logging.WARNING,
         format= '[%(asctime)s] %(levelname)s - %(message)s',
         datefmt='%H:%M:%S'
     )    
    def RxHandler(param, value):
        print("RxHandler>", param, value)
    
    count = 0
    print("t25Driver Initializing")
    try:
        netObj = t25Driver("192.168.56.2", 8080, 1, 10, RxHandler)
        while True:
            sleep(0.5)
            count = count + 1
            timeStamp = count * 10
            grubNum = count
            tg = [{"id":x+1,"x":x/10,"y":1+x/2,"a":0,"amp":-50+x,"vx":0.1,"vy":0.15} for x in range(count & 127)]
            netObj.sendTargets(timeStamp, grubNum, tg)
    #------------------------------------------------------------------------------
    except KeyboardInterrupt:
        print('User Interrupted. Please wait...')
        logging.debug("---- CTRL^C -----")
    finally:
        netObj.close()
        print("Deinitializing")
    #------------------------------------------------------------------------------
    print('Done')         
    logging.shutdown()

if __name__ == '__main__':
    democode()        
    