#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CAN DEMO

@author: Dunaev

Переустановка драйвера Kvaser CANlib SDK (часто слетает):
sudo make purge     
sudo make install 

"""
import os
import logging
from time import sleep, time
import can
import j1939
import struct
import array
import socket
import re
import json
import tcpServer
import t25Driver
from threading import Timer, Thread
import urllib   # URL decoding (Jordan%20-%20Good -> "Jordan - Good")
import subprocess


# Для генерации ключей используется внешний файл. Тут задается его имя и файл, который он генерирует
EXT_SECRET_UTIL = 'rsaEncoder.exe' # 'ToolName.exe' # None - Если не используется
EXT_SECRET_FILE = 'cipher.bin'

#
SPNINFO_FILENAME = "spnlist.txt"
spnInfoList = [] # 0:#SPN, 1:Имя, 2:Размерность, 3:Коэф, 4:Смещ, 5:Маска, 6: #PGN, 7: Позиция
TYPECODE = {"i8":0,"i16":1,"i32":2,"u8":3,"u16":4,"u32":5,"f32":6, "f":6}
PYTHONTYPE = ["B", "H", "I", "B", "H", "I", "f"]
json.encoder.FLOAT_REPR = lambda f: ("%.2f" % f)

TARGET_AMP_LIMIT = -80 # отбрасывать цели меньше данной яркости
SERVER_PORT = 7000
T25_IMIT_EMUL_PORT = -1 #8080 # -1 : Не использовать
softStreamerPort = None

# Выдача целей в DAQ через t25imit
# T25_IMIT_IP   = "192.168.56.2"  # IP адрес где выполняется t25imit
T25_IMIT_IP   = "127.0.0.1"  # IP адрес где выполняется t25imit
T25_IMIT_PORT = 8080  # порт t25imit (задается параметром "-t" см. t25imit --help)
# T25_IMIT_IP   = None # не использовать выдачу отметок
# T25_IMIT_PORT = None # не использовать выдачу отметок
# --------------------------------

logging.getLogger('j1939').setLevel(logging.WARNING)
logging.getLogger('can').setLevel(logging.WARNING)

PGN_NOTIFY = 65282
PGN_LOG    = 65280
PGN_DIAG   = 65281
PGN_TARGET = 65286
PGN_SYNC   = 65287
PGN_STATUS = 65288

rxCount = 0
grubNum = 0
lastms = time() * 1000
# print(lastms)
radarConfig = { }
radarConfigUpdateCount = 0

fpsFilterArray = []
FPS_FILTER_LEN = 10
readConfigFlag = False #Флаг чтения конфигурации. Нажата кнопка чтения конфигурации, после чего ожидание конфигурации.

doSomething = None
doSomethingArg = 0

workDirectory = "./"
doExit = 0
isRun = 0
threadObject = None
userNotifyEvent = None
userGetConfigEvent = None
userGetTargetsEvent = None
userGetStatusEvent = None
userErrorEvent = None

def runExternalCmd(args):
    # code, stdout = runExternalCmd(['tbTool1.exe', 'param'])
    try:
        res = subprocess.Popen(args, stdout=subprocess.PIPE)
        res.wait() # wait for process to finish;
    except OSError:
        print ("[ERROR] runExternalCmd: ", args)
        return (-1, "")
    stdout = res.stdout.read()
    return (res.returncode, stdout.decode("utf-8"))


def writeRadarConfig():
    global radarConfigUpdateCount
    global userGetConfigEvent
    
    radarConfigUpdateCount = radarConfigUpdateCount - 1
    if radarConfigUpdateCount <= 0:
        if userGetConfigEvent is not None:
            userGetConfigEvent(radarConfig)
        print("SAVE CONFIG...", radarConfig)
        global readConfigFlag
        readConfigFlag = False # Закончил ожидание приема конфигурации
        with open(workDirectory + 'radarConfig.json', 'w', encoding='utf-8') as f:
            f.write(json.dumps(radarConfig, ensure_ascii=False).replace(",", ",\n"))
        # сохранить конфиг для стримера
        with open(workDirectory + 'radar_mimo.conf', 'w', encoding='utf-8') as f:
            configId = 0
            if "configId" in radarConfig:
                configId = int(radarConfig["configId"])
            try:
                mode = "MIMO%d: " % configId
                f.write('# кол-во чирпов в кадре (должно быть равно 128) Топик:"/RadarChirps"\n')
                f.write('chirps=128\n\n')
                f.write('# кол-во каналов АЦП (должно быть равно 4). Топик:"/RadarAdcNum"\n')
                f.write('adc=4\n\n')
                f.write('# кол-во выборок АЦП в чирпе. Топик:"/RadarPoints"\n')
                f.write('samples=256\n\n')
                f.write('# Начало ЛЧМ, ГГц (77). Топик: "/RadarStartFreq"\n')
                f.write('startFreq=%s\n\n' % radarConfig[mode + "startFreq"])
                f.write('# пауза между чирпами, мкс. Топик: "/RadarIdleTime"\n')
                f.write('idleTime=%s\n\n' % radarConfig[mode + "idleTime"])
                f.write('# пауза запуска АЦП после начала рампы, мкс. Топик: "/RadarAdcStartTime"\n')
                f.write('adcStartTime=%s\n\n' % radarConfig[mode + "adcStartTime"])
                f.write('# время генерации ЛЧМ, мкс. Топик: "/RadarRampTime"\n')
                f.write('rampTime=%s\n\n' % radarConfig[mode + "rampEnd"])
                f.write('# наклон рампы, MHz/мкс. Топик: "/RadarFreqSlope"\n')
                f.write('freqSlope=%s\n\n' % radarConfig[mode + "freqSlope"])
                f.write('# скорость преобразования АЦП, ksps. Топик: "/RadarAdcRate"\n')
                f.write('adcRate=%s\n\n' % radarConfig[mode + "digOutSampleRate"])
                f.write('# Маска передающих каналов [1 | 3 | 7]. Топик: "/RadarTxEnable" array\n')
                f.write('txEnable=%s\n\n' % radarConfig[mode + "txMask"])
                f.write('# Заводской номер радара\n')
                f.write('SN=%s\n' % radarConfig["SN"])
            except KeyError:
                print("ERROR CONFIG...")
                
            


def tcpSend(text):
    tcpServer.tcpServerSend(bytes(text, encoding='UTF-8'))
    pass
               
def unpackTargets(data):
    target = []
    bytePerTarget = 7  # кол-во байт, которые описывают цель
    targetNum = len(data) // bytePerTarget
    for i in range(targetNum):
        try:
            (tid, x, y, vx, vy, a, amp) = struct.unpack_from("BbBbbbB", data, i*bytePerTarget)
            target.append({'id':tid, 'x':round(x*0.5, 2), 'y':round(y*0.3,2), 'vx':round(vx*1.0, 2), 'vy':round(vy*1.0, 2), 'a':round(vx*0.1,1), 'amp': round(amp * 0.5 - 137.5 + 60,1)})
        except:
            print("unpackTargets> Parse error!")
            return []
    return target 

def unpackValue(data, t):
    try:
        return struct.unpack_from(PYTHONTYPE[t], data, 0)[0]
    except:
        return 0

def packValue(value, t):
    try:
        return struct.pack(PYTHONTYPE[t], value+0)
    except:
        return bytes()


def on_message(pgn, data):
    global spnInfoList
    global spnIdList  
    global rxCount
    global radarConfig
    global t25imit
    

    def getIdIndex(spnid):
        try:
            return spnIdList.index(spnid)
        except ValueError:
            return -1
    
    """Receive incoming messages from the bus

    :param int pgn:
        Parameter Group Number of the message
    :param bytearray data:
        Data of the PDU
    """
    # print("PGN {} length {} data: {} \n".format(pgn, len(data), data))
#    print("\nRX>", data)
    rxCount += 1
    if pgn == PGN_NOTIFY:
        (spnid, t) = struct.unpack_from("<HB", data, 0)
        value = unpackValue(data[3:], t)
        spnIndex = getIdIndex(spnid)
        if spnIndex == -1:
            print("ERROR!!! SPN%d is undef!!!" % spnid)
        if spnIndex >= 0:
            if spnid < 10000:
                print(spnid, "T:",t, "V:",value)
            spnInfo = spnInfoList[spnIndex]
            jdata = {"Name":"SPN{}".format(spnid), "Title": spnInfo['name']}
            if len(spnInfo['dim']) > 0:
                jdata['Dimension'] = spnInfo['dim']
            jdata['Value'] = '%{}'.format(spnInfo['format']) % (value)
            global userNotifyEvent
            if userNotifyEvent is not None:
                userNotifyEvent(jdata)
            
            tcpSend(json.dumps(jdata))
            # для отладочных целей, выводим некоторые SPN в лог
            if (spnid == 3):# or (spnid == 60306): #or (spnid == 60321):
                print(jdata)
                
            # Если SPN является частью конфигурации (spnid < 10000)
            if spnid < 10000:
                radarConfig[spnInfo['name']] = value

                if readConfigFlag == True:
                    global radarConfigUpdateCount
                    radarConfigUpdateCount += 1
                    # отложенная запись файла конфигурации
                    Timer(2, writeRadarConfig).start()       
            
    if pgn == PGN_LOG:
        value = bytes(data).decode("utf-8")
        value = json.dumps(value)
        print("LOG: [%s]" % value)
        tcpSend('{"Name":"SPN1111", "Title":"LOG", "Value":%s}\n' % (value))

    if pgn == PGN_SYNC:
        # print("PGN_SYNC : len:", len(data))
        (SN, syncID, syncParam, syncReserv) = struct.unpack_from("<IBHB", bytes(data), 0)
        print("SN, syncID, syncParam, syncReserv : ", SN, syncID, syncParam, syncReserv)
    
    
    if pgn == PGN_DIAG:
        print("PGN_DIAG : len:", len(data))
        # структура диагностики статична.
        if (len(data) == 4 + 4*(256 + 64)):
            # range, doppler index
            (r, d, rx, chirp) = struct.unpack_from("<BBBB", bytes(data), 0)
            fft1data = bytes(data[4:4 + 256*4])
            fft1arr = [round(struct.unpack_from("f", fft1data, i*4)[0],2) for i in range(len(fft1data)//4)]
            # print("DIAG: fft1arr : ", fft1arr)
            fft3data = bytes(data[4 + 256*4:])
            fft3arr = [round(struct.unpack_from("f", fft3data, i*4)[0],2) for i in range(len(fft3data)//4)]
            # print("DIAG: fft3arr : ", fft3arr)
            jdata = {"Name":"SPN{}".format(100001), "Title": "FFT1"}
            jdata['Range'] = r
            jdata['Doppler'] = d
            jdata['Rx'] = rx
            jdata['Chirp'] = chirp
            jdata['GId'] = 1  # FFT1 ID
            jdata['Value'] = fft1arr
            tcpSend(json.dumps(jdata))
            # на приемной стороне "слипшиеся" пакеты пропадают! => делаю паузу!
            sleep(0.025)
            # print(json.dumps(jdata))
            print("DIAG FFT1,FFT3:", r, d, rx, chirp)
            jdata = {"Name":"SPN{}".format(100002), "Title": "FFT3"}
            jdata['Range'] = r
            jdata['Doppler'] = d
            jdata['Rx'] = rx
            jdata['Chirp'] = chirp
            jdata['GId'] = 2  # FFT3 ID
            jdata['Value'] = fft3arr
            tcpSend(json.dumps(jdata))
            # print(json.dumps(jdata))
        else:
            secretKey = bytes(data).hex().upper() # "EE00DDAA..."
            print("USER DATA:", secretKey)
            if EXT_SECRET_UTIL is not None:
                exitCode, stdout = runExternalCmd([EXT_SECRET_UTIL, secretKey])
                if exitCode == 0:
                    print("OK")
                    uploadFileAsync((EXT_SECRET_FILE, 1)) # id : 1
                else:
                    print("ERROR")
                print("stdout>", stdout)
                
            
            
        
    if pgn == PGN_TARGET:
        global lastms
        global fpsFilterArray
        global grubNum
        grubNum = grubNum + 1
        dTime = time()*1000.0 - lastms
        lastms = time() * 1000
        # Реализация простого фильтра, что бы fps не скакал
        if len(fpsFilterArray) != FPS_FILTER_LEN:
            fpsFilterArray = [dTime] * FPS_FILTER_LEN
        fpsFilterArray.pop(0)            
        fpsFilterArray.append(dTime)
        averageTime = sum(fpsFilterArray) / FPS_FILTER_LEN
        # ------------------------------------------------
        tg = unpackTargets(bytes(data))
        if t25imit is not  None:
            t25imit.sendTargets(int(lastms), grubNum, tg)
            
        tgNum = len(tg)
        tg = [x for x in tg if x['amp'] > TARGET_AMP_LIMIT]
        json_data = json.dumps({'Name':'SPN5555', 'fps': (1000.0/averageTime), 'Value':tg})
        tcpSend(json_data)
        dummy = ""
        if len(tg) == 1:
            if tg[0]['id'] == 255:
                dummy = " (EMPTY)"
        print("{}.[+{:.1f}ms] PGN_TARGET. Num:".format(rxCount, dTime), "%d (drop:%d)" % (len(tg), tgNum - len(tg)), dummy)
        global userGetTargetsEvent
        if userGetTargetsEvent is not None:
            userGetTargetsEvent(tg)
        
    if pgn == PGN_STATUS:
        (statusValue, maxTemper) = struct.unpack_from("<IB", bytes(data), 0)
        global userGetStatusEvent
        if userGetStatusEvent is not None:
            userGetStatusEvent((statusValue, maxTemper))
        spnid = 60305
        spnIndex = getIdIndex(spnid)
        print("Radar status: 0x%X, T:%d" % (statusValue, maxTemper))
        if spnIndex >= 0:
            spnInfo = spnInfoList[spnIndex]
            jdata = {"Name":"SPN{}".format(spnid), "Title": spnInfo['name']}
            jdata['Value'] = '%{}'.format(spnInfo['format']) % (statusValue)
            tcpSend(json.dumps(jdata))
            sleep(0.025)
        spnid = 60110
        spnIndex = getIdIndex(spnid)
        if spnIndex >= 0:
            spnInfo = spnInfoList[spnIndex]
            jdata = {"Name":"SPN{}".format(spnid), "Title": spnInfo['name']}
            jdata['Value'] = '%{}'.format(spnInfo['format']) % (maxTemper)
            tcpSend(json.dumps(jdata))
        
        
            


def readSpnFile(dataFileName):
    data = []
    try:
        f = open(dataFileName, "r", encoding="utf-8")
        for line in f:
            # Имя SPN может содержать
            items = re.split('''\s(?=(?:[^'"]|'[^']*'|"[^"]*")*$)''', line)
            param = []
            for i in items:
                if len(i) > 0:
                    if i[0] == '"' and i[-1] == '"':
                        i = i[1:-1]
                    param.append(i)
            # обработка возможности генерить имена SPN по правилу
            # После поля "ARRAY" идет диапазон вида: 0..2 и затем шаг ID (может быть знаковый, DEC/HEX)"
            if len(param)>=13:
                if str(param[10]).upper() == "ARRAY":
                    (lo, hi) = str(param[11]).split('..')
                    (lo, hi) = (int(lo), int(hi))
                    if str(param[12]).find("0x") >= 0:
                        step = int(param[12], 16) # HEX
                    else:
                        step = int(param[12]) #DEC
                        
                    n = 0
                    while hi >= (lo + n):
                        newparam = param[:] # копирование списка
                        newparam[0] = str(int(param[0]) + (n * step))
                        newparam[1] = str(param[1]) % (lo + n)
                        n = n + 1
                        spnParam = dict(zip(['id', 'name', 'dim', 'k1', 'k0', 'format', 'pgn', 'offset', 'access', 'type'], newparam))
                        data.append(spnParam)
            else:
                # именованный список параметров (см. файл spnlist.txt)
                spnParam = dict(zip(['id', 'name', 'dim', 'k1', 'k0', 'format', 'pgn', 'offset', 'access', 'type'], param))
                data.append(spnParam)
    except  OSError as e:
        print('[File:"{}"] Error: {}'.format(dataFileName, e.strerror))
        return data
    except:
        print("ERROR FORMAT: IGNORE")
    f.close()
    return data


# передать файл конфигурации в радар

def uploadConfigFile(filename):
    global spnInfoList
    try:
        with open(filename, encoding="utf-8") as f:
            try:
                config = json.load(f)
                for k in config:
                    id = [int(x['id']) for x in spnInfoList if x['name'] == k]
                    if len(id) > 0:
                        onWriteSPN(id[0], config[k])
                        sleep(0.05)
                        print(id[0], config[k])
                    else:
                        print("Error! SPN '%s' is missing!" % k)
                        sleep(3)
                        
                    
            except ValueError:
                print("Config file format error!!!")
    except  OSError as e:
        print('[File:"{}"] Error: {}'.format(filename, e.strerror))
    pass
            

def sendMemoryFile(filename, canWriter, serviceId = 0xFF):
    PSIZE = 700  # Размер блока данных, который передаем через TP
    try:
        file = open(filename, "rb")
        alldata = file.read(1024*1024)
        bufferSize = len(alldata)
        bufferSum = sum(alldata)
        # в конце буфера контрольная сумма всех байт прошивки
        alldata = alldata + struct.pack("<I", sum(alldata))
        page = []
        k = len(alldata) // PSIZE
        
        for n in range(k):
            page.append(alldata[n * PSIZE : (1 + n) * PSIZE])
        if len(alldata) > PSIZE * k:
            page.append(alldata[k * PSIZE : ])
        # посылка порциями по 700 байт (кратно 7байт)
        for n,p in enumerate(page):
            print('page #%d of %d' % (n + 1, len(page)))
            canWriter(n * PSIZE, p, serviceId = serviceId)
            sleep(0.08)

        print('Done. Size: %d (+4), (CHECK SUM:%d)' % (bufferSize, bufferSum))
        file.close()
        return bufferSize

    except KeyboardInterrupt:
        print('*********************\nCTRL+C : File upload canceled!\n*********************')
    except FileNotFoundError:
        print("File is not exist or accessible")
    # except:
    #     print("Can lib error! ")
        
    return 0

def sandMemBuffer(offset, buffer, serviceId = 0xFF):
    datasize = len(buffer)
    listbuffer = list(buffer)
    n = datasize // 7
    if datasize > n * 7:
        n = n + 1
    try:
        # учтем служебную посылку
        size = datasize + 7
        ecu.send_pgn(0, 0xEC, 0xFF, 7, 0, [32, size & 255, size >> 8, n, 0xFF, 0x09, 0xFF, 0x00]) #CM BAM
        # служебная посылка (смещение и резерв)
        ecu.send_pgn(0, 0xEB, 0xFF, 7, 0, [1, offset & 255, (offset >> 8) & 255, (offset >> 16) & 255, 0, serviceId, 0xFF, 0xFF])
        for k in range(n):
            d = [k + 2] # номер посылки
            d.extend([listbuffer.pop(0) for _ in range(7) if len(listbuffer) > 0])
            d.extend([0xff]*(8 - len(d)))
            ecu.send_pgn(0, 0xEB, 0xFF, 7, 0, d) 
    except KeyboardInterrupt:
        print('USER BREAK (CTRL+C)')
        raise


def putData(buffer, offset, value, strTypeId):
    if type(value) is str:
        if strTypeId == 'f32':
            value = float(value)
        else:
            value = int(value)
    binValue = packValue(value, TYPECODE[strTypeId])
    arrValue = [x for x in binValue]
    for i in range(len(arrValue)):
        buffer[i + offset] = arrValue[i]
    return buffer


# передача файла в CAN
def uploadProgFile(args):
    if len(args) == 2:
        filename, serviceId = args
        print("UPLOAD FILE...[{}]".format(filename))
        size = sendMemoryFile(filename, sandMemBuffer, serviceId = serviceId)
        # команда записи образа. В значении SPN выдается размер образа в байтах
        if (size > 0) and (serviceId == 255):
            onWriteSPN(61116, size)
            print("Please don't turn off the radar. Recording in progress...")
            sleep(10)  # Для записи 350кБ нужно примерно 10сек.
            print("Wait done.")      
    

def uploadFileAsync(args):
    global doSomething
    global doSomethingArg
    doSomethingArg = args
    doSomething = uploadProgFile
    


def onWriteSPN(spnid, value):
    global radarConfig
    global t25emulSock
    global softStreamerPort
    global readConfigFlag
    global doSomething
    global doSomethingArg
    
    #print("EVENT[SPN:{} = {}]".format(spnid, value))
    # spn > 65535 Это служебные SPN (команды от GUI драйверу)
    if spnid > 65535:
        print("EVENT[SPN:{} = {}]".format(spnid, value))
        
        if spnid == 100001:
            value = urllib.parse.unquote(value.replace("file://", ""))
            print("UPLOAD CONFIG...[{}]".format(value))
            uploadConfigFile(value)
            print("UPLOAD CONFIG DONE!")
            
        if spnid == 100002:
            # Тест конфликта адреса
            ecu.send_pgn(0, 0xEC, 0xFF, 7, 0, [32, 9, 0, 2, 0xFF, 0xD8, 0xFE, 0x00]) #CM BAM
            ecu.send_pgn(0, 0xEB, 0xFF, 7, 0, [1, 0x1, 0x00, 0x60, 0x45, 0, 0, 0]) #TP 1
            ecu.send_pgn(0, 0xEB, 0xFF, 7, 0, [2, 0, 0x78, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]) #TP 2
            # ecu.send_tp_dt(0, 0x77, [1, 2, 3, 4, 5, 6, 7, 8, 9])

        if spnid == 100003:
            # Передача файла прошивки. (через передачу функции в main())
            doSomethingArg = (urllib.parse.unquote(value.replace("file://", "")),255)
            doSomething = uploadProgFile

        if spnid == 100004:
            # Тест записи в память
            ecu.send_pgn(0, 0xEC, 0xFF, 7, 0, [32, 14, 0, 2, 0xFF, 0x09, 0xFF, 0x00]) #CM BAM
            ecu.send_pgn(0, 0xEB, 0xFF, 7, 0, [1, 1, 2, 3, 4, 5, 6, 7]) #TP 1
            ecu.send_pgn(0, 0xEB, 0xFF, 7, 0, [2, 8, 9, 10, 11, 12, 13, 14]) #TP 2

        if spnid == 100005:
            # Передача шифрованных данных (по кнопке).
            doSomethingArg = (urllib.parse.unquote(value.replace("file://", "")), 1)
            doSomething = uploadProgFile

        if spnid == 100006:
            # Передача закрытого ключа (по кнопке).
            doSomethingArg = (urllib.parse.unquote(value.replace("file://", "")), 2)
            doSomething = uploadProgFile
            
        # if spnid == 111111:  # TODO Нужно изменить число. 
        #     d = [0xff] * 8
        #     pgn = PGN_LOG
        #     if not isinstance(value, int):
        #         print("CONSOLE>>>", value)
        #         value = bytes(value, encoding='UTF-8')
        #         d[0] = len(value)
        #         for i in range(min(len(value), 7)):
        #             d[i + 1] = value[i]
        #     else:
        #         print("CONSOLE BYTE>>>[0x%x]" % value)
        #         d[0] = 1
        #         d[1] = value & 255
        #     if ecu is not None:
        #         ecu.send_pgn(0, pgn >> 8, pgn & 255, 6, 255, d)
        #     return
            
        return
    
        
    if spnid == 1111:  # TODO Нужно изменить число. 
        d = [0xff] * 8
        pgn = PGN_LOG
        if not isinstance(value, int):
            print("CONSOLE>>>", value)
            value = bytes(value, encoding='UTF-8')
            d[0] = len(value)
            for i in range(min(len(value), 7)):
                d[i + 1] = value[i]
        else:
            print("CONSOLE BYTE>>>[0x%x]" % value)
            d[0] = 1
            d[1] = value & 255
        if ecu is not None:
            ecu.send_pgn(0, pgn >> 8, pgn & 255, 6, 255, d)
        return
    
    if spnid == 60001:
        radarConfig = {}
        print("READ CONFIG")
        readConfigFlag = True # Флаг ожидания конфигурации
        
    # Start
    if spnid == 2221 and  T25_IMIT_EMUL_PORT > 0:
        if softStreamerPort == None:
            t25emulSock.settimeout(0.25)
            try:
                _, softStreamerPort = t25emulSock.recvfrom(1024)
                print(softStreamerPort)
            except socket.timeout:
                pass
            except socket.error:
                print("STREAMER ERROR")
        if softStreamerPort != None:
            print("STREAMER SEND START:", value)
            t25emulSock.sendto(bytes(array.array('i', [int(value), 0])), softStreamerPort)
        
        
    # print("onWriteSPN( id : {}, value : {})".format(spnid, value))
    spnInfo = spnInfoList[spnIdList.index(spnid)]
    pgn = int(spnInfo['pgn'])
    offset = int(spnInfo['offset'])
    strType = spnInfo['type']
    access = spnInfo['access']
    if access in ['w', 'rw', '*']:
        # Буфер [FF FF FF FF FF FF FF FF]
        d = [0xff] * 8
        if access == '*':
            # запись через интерфейс NOTIFY
            d = putData(d, 0, spnid, 'i16')
            d = putData(d, 2, TYPECODE[strType], 'i8')
            d = putData(d, 3, value, strType)
        if access in ['w', 'rw']:
            # запись по собственному PGN
            d = putData(d, offset, value, strType)
            print("WRITE BUFFER: ",pgn, d)
            
        # print("WRITE SPN{} '{}' PGN:{} DATA:{} (VALUE:{})".format(spnInfo['id'], spnInfo['name'], pgn, d, value))
        if ecu is not None:
            ecu.send_pgn(0, pgn >> 8, pgn & 255, 6, 255, d)


jBuffer = ""

# команда в формате JSON. Возврат (строка: незавершенная JSON)
def jTryParse(jText, onWriteSPN):
    try:
        j = json.loads(jText)
        try:
            if j["Name"] != None and j["Value"] != None:
                if onWriteSPN:
                    spnid = re.findall(r'^SPN(\d+)$', j["Name"])
                    if len(spnid) > 0:
                        onWriteSPN(int(spnid[0]), j["Value"])
        except KeyError as e:
            print("Ошибка ключа: {}".format(e))
        except ValueError as e:
            print("Ошибка значения :  {}".format(e))
        
    except ValueError as e:
        print("Ошибка парсинга JSON :  {}".format(e))
        return ""
            
    return ""

def jParse(jText, onWriteSPN):
    global jBuffer
    jBuffer = jBuffer + jText
    while len(jBuffer) > 0:
        if jBuffer[0] == "{":
            break
        jBuffer = jBuffer[1:]
    jBuffer = jTryParse(jBuffer, onWriteSPN)    
    

def tcpRxHandler(sock, data):
    print("Client Rx>", data.decode('UTF-8'))
    jParse(data.decode('UTF-8'), onWriteSPN)
    pass
    
    
# reason : ['accepted' | 'close' | 'error']
def tcpServiceHandler(peername, reason):
    global jBuffer
    print("Client>", peername, reason)
    jBuffer = ""    
    pass

def t25DriverRxHandler(param, value):
    if param == t25Driver.t25Param.ENABLE:
        print("T25: ENABLE:",value)
    pass

# интерфейс для внешнего управления при использовании драйвера как py-модуля
def j1939DriverStop():
    global doExit
    global isRun
    global threadObject
    if isRun == 1:
        doExit = 1
        if threadObject is not None:
            threadObject.join()    
        isRun = 0

def j1939DriverStart(spnlistFileName = None):
    global doExit
    global isRun
    global threadObject
    global SPNINFO_FILENAME
    if spnlistFileName is not None:
        SPNINFO_FILENAME = spnlistFileName
    if isRun == 0:
        x = Thread(target=main)
        x.start()
        isRun = 1

def j1939DriverSetEvents(StatusEvent = None, TargetsEvent = None, NotifyEvent = None, ConfigEvent = None, ErrorEvent = None):
    global userNotifyEvent
    global userGetConfigEvent
    global userGetTargetsEvent 
    global userGetStatusEvent
    global userErrorEvent

    userNotifyEvent = NotifyEvent
    userGetConfigEvent = ConfigEvent
    userGetTargetsEvent = TargetsEvent
    userGetStatusEvent = StatusEvent
    userErrorEvent = ErrorEvent

def j1939DriverSetSPN(spnid, value):
    global isRun
    if isRun == 1:
        onWriteSPN(spnid, value)
    
def j1939DriverSetWorkDir(path):
    global workDirectory
    workDirectory = path

def j1939DriverGetSpnList():
    global spnInfoList
    return spnInfoList

#

def main():
    global doSomething
    global doSomethingArg
    global doExit
    global spnInfoList
    global spnIdList
    global ecu
    global t25emulSock
    global t25imit
    
    doExit = 0
    # Возможность запуска стримера при получении команды "старт"
    if T25_IMIT_EMUL_PORT > 0:
        t25emulSock = socket.socket(socket.AF_INET,  socket.SOCK_DGRAM) 
        t25emulSock.bind(("",T25_IMIT_EMUL_PORT))

    ecu = None
    print("Load SPN info")
    spnInfoList = readSpnFile(SPNINFO_FILENAME)[1:]
    spnIdList = [int(x['id']) for x in spnInfoList if len(x) > 0]
    
    print("Initializing")
    try:
        tcpServer.tcpServer(SERVER_PORT, tcpRxHandler, tcpServiceHandler)
        
        # инициализация драйвера выдачи отметок в t25imit (версии протокола и радара: 1 и 10)
        if (T25_IMIT_PORT is not None) and (T25_IMIT_IP is not None):
            t25imit = t25Driver.t25Driver(T25_IMIT_IP, T25_IMIT_PORT, 1, 10, t25DriverRxHandler)
        else:
            t25imit = None
        
    
        # # create the ElectronicControlUnit (one ECU can hold multiple ControllerApplications)
        ecu = j1939.ElectronicControlUnit()
    
        # # Connect to the CAN bus
        # # Arguments are passed to python-can's can.interface.Bus() constructor
        # # (see https://python-can.readthedocs.io/en/stable/bus.html).
        # # ecu.connect(bustype='socketcan', channel='can0')
        # # ecu.connect(bustype='pcan', channel='PCAN_USBBUS1', bitrate=250000)
        # # ecu.connect(bustype='ixxat', channel=0, bitrate=250000)
        # # ecu.connect(bustype='vector', app_name='CANalyzer', channel=0, bitrate=250000)
        # # ecu.connect(bustype='nican', channel='CAN0', bitrate=250000)    
        if ecu is None: 
            print('Warning!!! ecu disabled! Demo mode!!!')
        else:
            ecu.connect(bustype='kvaser', channel=0, bitrate=250000)
            sleep(1)
            ecu.subscribe(on_message)
            # sleep(2)
            # data_page, pdu_format, pdu_specific, priority, src_address, data
            # ecu.send_pgn(0, 0xff, 2, 6, 255, [255, 1, 255, 255, 255, 255, 255, 255])
            # ecu.send_message(0x18FF03FF, [1, 255, 255, 255, 255, 255, 255, 255])
            #ecu.send_tp_eom_ack(src_address, dest_address, message_size, num_packets, pgn_value)
        count = 0
        try:
            #------------------------------------------------------------------------------
            tcpSend('{"Name":"SPN1","Value":555}') # start indicator
            while doExit==0:
                sleep(1)
                count = count + 1
                if doSomething is not None:
                    doSomething(doSomethingArg)
                    doSomething = None
                # tcpSend('{"Name":"SPN1","Value":%d}' % (count))
            #------------------------------------------------------------------------------
        except KeyboardInterrupt:
            print('User Interrupted.')
        finally:
            print("Deinitializing")
            if ecu is not None:
                ecu.disconnect()
        #------------------------------------------------------------------------------
    finally:
        tcpServer.tcpServerExit()
        if t25imit is not None:
            t25imit.close()
        
    if T25_IMIT_EMUL_PORT > 0:
        t25emulSock.close()
    print('Done')


if __name__ == '__main__':
    main()        