# -*- coding: utf-8 -*-
"""
Created on 05/07/2021

@author: Dunaev


"""

import os
import numpy as np
import matplotlib.pyplot as plt
import mmap
import math
import array
import struct
from shutil import copyfile
import shelve


FRAME_HEADER_SIZE   = 52
# Параметры формата данных (как в алгоритме матлаба)
CHIRP_SAMPLE_OFFSET = 14 
CHIRP_SAMPLE_NUM    = 670
RADAR_RX_CHANNEL    = 16
RADAR_TX_CHANNEL    = 2
# первые чирпы содержат перех.процесс. они отбрасываются
CHIRP_DROP_NUM      = 4
FFT1_SIZE           = 1024
FFT1_SCALE          = 1 # (1/FFT1_SIZE)
# кол-во точек вначале спектра, которые обнуляю, что бы не мешали обнаружить пик на нужной частоте
ZERO_HEAD_PADDING   = 10

M_PI                = 3.14159265358979

# параметр DPI для figure. Значение больше: элементы крупнее.
DPI = 72 


# размер графиков в пикселях (делаю все графики одинаковые по размеру)
PNG_SIZE_X          = 1200
PNG_SIZE_Y          = 650

# путь к GUI (Сводный график используется для GUI)
DIRECTORY_GUI_PNG = "./design"
# относительный путь к рабочей директории
DIRECTORY_WORK = "./algorithm/workdir"
# относительный путь к директории с отладочн. массивами
DIRECTORY_DEBUG = "./algorithm/dbg"
# формат сетки графика
Y_SCALE = "linear"  # ["log" | "linear"]


def algorithmPrint(text, error=0):
    print("algorithm[LEAK]>", text)
    pass


def mmap_io(filename):
    with open(filename, mode="rb") as file_obj:
        with mmap.mmap(file_obj.fileno(), length=0, access=mmap.ACCESS_READ) as mmap_obj:
            buff = mmap_obj.read()
            return buff

# сохранение изображения графика в файл
def exportFigure(plt, filename):
    try:
        plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=0.5)                
        plt.savefig(filename)
        algorithmPrint(filename + "...OK")
    except PermissionError:
        algorithmPrint('save fig Error...', error=1)
    pass


# формируем метки на оси X
def getXticks(N, step=1, first=0):
    return [x*step + first for x in range(N)]


def puzzleCh(i):
    puzzle = [13, 14, 16, 15, 9, 10, 12, 11, 5, 6, 8, 7, 1, 2, 4, 3]
    N = i // 16
    p = i % 16
    return (puzzle[p] - 1) + N*16


# Общее оформление графиков
def getGraph(title, x = PNG_SIZE_X, y = PNG_SIZE_Y, dpi = DPI):
    fig = plt.figure(dpi=dpi, figsize=(x / dpi, y / dpi))
    ax = fig.add_subplot(111)  # 111 - это сетка. 111 означает сетка 1x1
    ax.set_yscale(Y_SCALE)
    ax.set_title((title))
    ax.grid()
    return fig, ax

# Leakage - int[32][720] - Массив для экспорта
def exportLeak(filename, Leakage):
    LeakFileName = filename + ".h"
    LeakBinFileName = filename + ".bin"
    try:
        print('Export leak (bin) to "{}"'.format(LeakBinFileName))
        
        f = open(LeakBinFileName, "wb")
        for ch in Leakage:
            f.write(array.array('h', ch))
        f.close()
    except  OSError as e:
        print('[File:"{}"] Error: {}'.format(LeakBinFileName, e.strerror))
        
    try:
        print('Export leak (text) to "{}"'.format(LeakFileName))
        f = open(LeakFileName, "w")
        for i, ch in enumerate(Leakage):
            line = "{"
            for a in ch:
                line = line + ("%d, " %  a)
            line = line[:-2] + "}"
            if i < 31:    
               line = line + ","
            f.write("%s\n" % line)
        f.close()
    except  OSError as e:
        print('[File:"{}"] Error: {}'.format(LeakFileName, e.strerror))



# Функция алгоритма.
def algorithmRFLeak(param, debug=0):
    # Максимальные значения по всем кадрам (новый кадр - новая частота)
    global Leakage
    # Средние значения FFT для каждого кадра отдельно
    global resultMeanFFTFrame
    global RADAR_RX_CHANNEL
    global workDirectory

    if (param == "START"):
        Leakage = []

        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/" + DIRECTORY_WORK
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if param.find("DONE") >= 0: 
        algorithmPrint("DONE")
        if (len(Leakage) > 0):
            Leakage = np.mean(Leakage, axis=0)
            # меняем очередность каналов
            LeakagePuzzle = np.zeros((32, 720))
            for i, x in enumerate(Leakage):
                LeakagePuzzle[puzzleCh(i)] = np.rint(x)
            # print("---------------------------")
            # print(Leakage[0, 0])
            # print("---------------------------")
            SN = "0"*9
            if param.find(":") > 0:
                SN = param[param.find(":") + 1:]
            fileName = workDirectory + ("/Leak_radar_%s" % SN)
            exportLeak(fileName, LeakagePuzzle.astype(int))
            # Графики
            fig, ax = getGraph('Графики пролаза (полезная часть чирпа)', 1000, 600, 72)
            for i, leak in enumerate(LeakagePuzzle):
                leak = leak[CHIRP_SAMPLE_OFFSET:CHIRP_SAMPLE_NUM + CHIRP_SAMPLE_OFFSET]
                x = getXticks(len(leak))
                ax.plot(x, leak, label=('RX %d' % (i+1)))
                # корректно активировать легенду можно только после ax.plot()
                #ax.legend(loc='upper right')
                # x1, x2, y1, y2 = ax.axis()
                # ax.axis((x1, x2, -100, 100))
            exportFigure(fig, DIRECTORY_GUI_PNG + "/leak.png")
            # copyfile(DIRECTORY_GUI_PNG + "/leak.png",  workDirectory + "/leak.png")
            # plt.show()
            
            
        else:
            algorithmPrint("no data!", error=1)
        return "DONE"

    if (param == "CALC"):
        if debug == 1:
            # загружаю данные из файла
            fileName = ("%s/rawadc_err2.bin" % DIRECTORY_DEBUG)
        else:
            fileName = ("%s/leak_adc.bin" % workDirectory)

        # Получаем данные от системы испытаний.
        # (полный кадр стрима, включая заголовок)
        data = mmap_io(fileName)

        # Если данных меньше чем заголовок - ошибка!
        if (len(data) < FRAME_HEADER_SIZE):
            # Ошибка формата данных.
            algorithmPrint("MMAP ERROR!!! Data size:%d" % (len(data)), error=1)
            return -1

        # Парсим заголовок кадра. В нем содержится информация
        # о кол-ве точек на чирп (возможно 720 и 378)
        STX, LEN, T, TIME, GN, T1, T2, T3, T4, T5, T6, N_CHIRP, N_CH, N = struct.unpack_from( "<iiiqqhhhhhhIII", data, 0)

        algorithmPrint("FORMAT: [%d x %d x %d]" % (N_CHIRP, N_CH, N))

        RADAR_RX_CHANNEL = N_CH
        
        # Кол-во рабочих чирпов (без MIMO), которые идут в алгоритм
        CHIRP_NUM = N_CHIRP - CHIRP_DROP_NUM

        # Исходя из данных заголовка,
        # отбрасываем мусорные чирпы (N : кол-во точек АЦП в чирпе)
        drop_data_size = N * CHIRP_DROP_NUM * N_CH * 2
        adc_raw_array = array.array('h', data[FRAME_HEADER_SIZE + drop_data_size:])

        # проверка на корректность размера данных
        if (len(adc_raw_array) != N * CHIRP_NUM * N_CH):
            algorithmPrint("DATA FORMAT ERROR!!!", error=1)
            return 0

        # % Генерируем пролаз с сохраненных данных
        # Leakage = Leakage + ( squeeze(mean(Signal_Data_3D, 2)) );
        
        adcPack32 = np.array(adc_raw_array).reshape(CHIRP_NUM//2, 2*RADAR_RX_CHANNEL, N)
        leakFrame = np.mean(adcPack32, axis = 0)
        # print("---------------------------")
        # print(leakFrame[0, 0])
        # print("---------------------------")

        Leakage.append(leakFrame)

    return "OK"

# режим автономной отладки модуля через algRunner.py 
if __name__ == '__main__':
    DIRECTORY_DEBUG = "." + DIRECTORY_DEBUG
    DIRECTORY_GUI_PNG = "." + DIRECTORY_GUI_PNG
    algorithmRFLeak("START", debug=1)
    workDirectory = "./workdir"
    algorithmRFLeak("CALC", debug=1)
    algorithmRFLeak("CALC", debug=1)
    algorithmRFLeak("DONE", debug=1)
# ALG_DEBUG           = 1
#
