# -*- coding: utf-8 -*-
"""
Created 2022.06.08

@author: Dunaev


Алгоритм "контроль положения отражателя":

Цель проверки: Получить характеристику изменения амплитуды цели (уголка) в зависимости от угла
    
 
    
Взаимодействие с системой испытаний:
    Параметры запуска:
        "START" : перед началом расчета. 
        Возврат: Путь к рабочей директории
        
        "CONFIG" : Считать из файлов конфигурации LEAK и TM_VECTOR. 
        Возврат:  ["OK" | "ERROR"] - статус (пока игнорируется)
        
        "CALC" : Очередной кадр снят радаром и размещен в файл данных (mmap).
        Возврат: ["ABORT" | "%f"] - Угол, для которого нашли пик

        "DONE" : Все кадры обработаны, нужно взять "суммарный" FFT и сравнить с порогом.
        Возврат: ["OK" | "ERROR"] - статус. "ERROR" - отношение мин/макс. больше чем нужно


"""
import os
import numpy as np
import matplotlib.pyplot as plt
import mmap
import math
import array
from shutil import copyfile
import json

import sys
sys.path.append(os.path.abspath("./algorithm/"))
from algCornerRes import prs_s1, prs_s2, prs_s3, win_nfft


# Параметры формата данных (как в алгоритме матлаба)
CHIRP_SAMPLE_NUM    = 256
RADAR_RX_CHANNEL    = 4
RADAR_TX_CHANNEL    = 3
MIMO_CHANNEL_NUM    = RADAR_RX_CHANNEL * RADAR_TX_CHANNEL

CHIRP_NUM           = 128
FFT1_SIZE           = 256
FFT3_SIZE           = 1024
# коэф. перевода в дБ (хотя Александра сказала что это неверные дБ)
FFT_SCALE           = CHIRP_SAMPLE_NUM*(2**15)*CHIRP_NUM*MIMO_CHANNEL_NUM

M_PI                = 3.14159265358979

# параметр DPI для figure. Значение больше: элементы крупнее.
dpi = 109

# размер графиков в пикселях (делаю все графики одинаковые по размеру)
PNG_SIZE_X          = 1200
PNG_SIZE_Y          = 650

# путь к GUI (Сводный график используется для GUI)
DIRECTORY_GUI_PNG = "./design"
# относительный путь к рабочей директории
DIRECTORY_WORK = "./algorithm/workdir"
# относительный путь к директории с отладочн. массивами
DIRECTORY_DEBUG = "./algorithm/dbg/rAngle"
# относительный путь к директории параметров
DIRECTORY_CONFIG = "./algorithm/config"
# файл конфигурации радара (все параметры)
RADAR_CONFIG_FILE = "radarConfig.json"

# формат сетки графика
Y_SCALE = "linear"  # ["log" | "linear"]
# для простоты задал константой


def algorithmPrint(text, error=0):
    print("algorithm[ОПУ]>", text)
    pass


def mmap_io(filename):
    with open(filename, mode="rb") as file_obj:
        with mmap.mmap(file_obj.fileno(), length=0, access=mmap.ACCESS_READ) as mmap_obj:
            buff = mmap_obj.read()
            return buff


# сохранение изображения графика в файл
def exportFigure(plt, filename):
    try:
        plt.savefig(filename)
        algorithmPrint(filename + "...OK")
    except PermissionError:
        algorithmPrint('save fig Error...', error=1)
    pass


# формируем метки на оси X
def getXticks(N, step=1, first=0):
    return [x*step + first for x in range(N)]


# Общее оформление графиков
def getGraph(title):
    fig = plt.figure(dpi=dpi, figsize=(PNG_SIZE_X / dpi, PNG_SIZE_Y / dpi))
    ax = fig.add_subplot(111)  # 111 - это сетка. 111 означает сетка 1x1
    ax.set_yscale(Y_SCALE)
    ax.set_title((title))
    ax.grid()
    return fig, ax


# построение графика порога
def addThresholdPlot(ax, x, level, err=0):
    # Порог
    color = 'green'
    if err == 1:
        color = 'red'
    ax.plot(x, level, linestyle='--', color=color, linewidth=3, label='Порог')
    pass


# Получаем порог из файла. Возврат: [[x, amp], [x, amp]...]
def getHiLimit(mode):
    # AMP ANGLE
    # ...
    # AMP ANGLE
    if mode is not None:
        dataFileName = DIRECTORY_CONFIG + ("/refangle77_%s.txt" % mode)
    else:
        dataFileName = DIRECTORY_CONFIG + "/refangle77.txt"
    data = []
    try:
        with open(dataFileName, "r") as f:
            for line in f:
                items = line.split()
                if (len(items) == 2):
                    data.append([float(items[1]), float(items[0])])
                else:
                    raise ValueError('Format error!')
    except IOError as err:
        print("File error: {0}".format(err))
    except ValueError:
        print("Could not convert data to an float (%s)." % line)
    return data

# Функция алгоритма.
def algorithmCPYCAngle(param, debug=0):
    # Результирующий список точек
    global resultPoint
    # радочая директори, куда скрипт испытаний (Lua) кладет данные
    global workDirectory
    global calibration_coef
    # Порог, ниже, которого не должна опускаться наш график
    global HiLimitlevel
    global config
    global ampZero


    if (param == "TEST"):
        return "TEST-OK"

    if param.find("START") >= 0:
        resultPoint = []
        ampZero = []
        config = None
        mode = None
        if param.find(":") > 0:
            mode = param[param.find(":") + 1:]

        # Загрузка порога из файла
        HiLimitlevel = getHiLimit(mode)
        
        calibration_coef = [complex(1,0)]*MIMO_CHANNEL_NUM

        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/" + DIRECTORY_WORK
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        # удалить файл конфигурации радара, если он там есть
        # конфигурация должна вычитываться из радара на шаге "CONFIG"
        # os.remove(workDirectory + "/" + RADAR_CONFIG_FILE)
        
        return workDirectory

    if (param == "CLEAR"):
        # удалить файл конфигурации радара, если он там есть
        # конфигурация должна вычитываться из радара на шаге "CONFIG"
        print("Delete file:", workDirectory + "/" + RADAR_CONFIG_FILE)
        os.remove(workDirectory + "/" + RADAR_CONFIG_FILE)
        return "OK"

    if (param == "GET_SN"):
        # запрос заводского номера
        if config is not None:
            return "%d" % config["SN"]
        return "0"


    if (param == "CONFIG"):
        configFileName = workDirectory + "/" + RADAR_CONFIG_FILE
        algorithmPrint("CONFIG, File:", configFileName)
        config = None
        with open(configFileName, encoding="utf-8") as f:
            try:
                config = json.load(f)
            except ValueError:
                print("Config file format error!!!")
                return "ERROR"
        if config is not None:
            for n in range(12):
                calibration_coef[n] = complex(config["calib[%d].Re" % n], config["calib[%d].Im" % n])
        
        print(calibration_coef)
        return "OK"

    # Шаг получения кадра данных (может быть несколько)
    if (param == "CALC"):
        algorithmPrint("CALC")
        if debug == 1:
            # загружаю данные из файла
            fileName = ("%s/refcpycAngle.bin%d" % (DIRECTORY_DEBUG, (len(resultPoint)+1)*1))
        else:
            fileName = ("%s/refcpycAngle.bin" % workDirectory)
        algorithmPrint("File : %s" % fileName)

        # Получаем данные от системы испытаний.
        # (полный кадр стрима, включая заголовок)
        data = mmap_io(fileName)

        adc_raw_array = array.array('h', data)

        # проверка на корректность размера данных
        if (len(adc_raw_array) != 2*CHIRP_SAMPLE_NUM * CHIRP_NUM * RADAR_RX_CHANNEL):
            algorithmPrint("DATA FORMAT ERROR!!!", error=1)
            return "ABORT"

        # разбивка массива int16*2 на список чирпов
        adc = np.array(adc_raw_array).reshape(CHIRP_NUM*RADAR_RX_CHANNEL*CHIRP_SAMPLE_NUM,2)
        # перевод к комплексному виду
        adc = adc[...,0] + 1j*adc[...,1];
        # разбивка на чирпы (adc[128][4][256])
        chirpArray = np.array(adc).reshape(CHIRP_NUM, CHIRP_SAMPLE_NUM, RADAR_RX_CHANNEL)
        # ПСП
        # prs = [prs_s1, prs_s2, prs_s3]

        cube = np.zeros((CHIRP_NUM, MIMO_CHANNEL_NUM, FFT1_SIZE), dtype=complex)
        
        # print("sum(prs_s1)", sum(prs_s1))
        # print("sum(prs_s2)", sum(prs_s2))
        # print("sum(prs_s3)", sum(prs_s3))
        # print("sum(win_nfft)", sum(win_nfft))
        
        # print("chirpArray[0, 0:5, 1]", chirpArray[0, 0:5, 1])
        
        for chirp in range(CHIRP_NUM):
            for rx in range(RADAR_RX_CHANNEL):
                fft1 = np.fft.fft(np.multiply(chirpArray[chirp, :, rx], win_nfft))
                # if (chirp == 5) and (rx == 1):
                    # print("fft1 [5, 1]", fft1[0:3])
                    
                cube[chirp, 0 + rx, :] = fft1 * prs_s1[chirp]
                cube[chirp, 4 + rx, :] = fft1 * prs_s2[chirp]
                cube[chirp, 8 + rx, :] = fft1 * prs_s3[chirp]

                    
        # print("cube[0, :, 0]:", cube[2, :, 1])
        fft2 = np.fft.fft(cube, axis=0)
        # print("----------------------------")
        # print(fft2[0:10,1, 2])
        # print("----------------------------")

        # найдем максимум в 0-м доплере (большими усилиями обнаружил, что в argmax нужно подавать модуль, иначе есть отличия от матлаба)
        fft2_zeroDoppler = np.abs(fft2[0, 0, :]) #20*np.log10(np.abs(fft2[0][0]))
        # print("----------------------------")
        # print(fft2_zeroDoppler[7:40])
        # print("sum:",sum(fft2_zeroDoppler))
        # print("----------------------------")

        fft2_zeroDoppler[0:7] = 1E-16
        fft2_zeroDoppler[64:] = 1E-16
        # поиск максимального значения в нулевом доплере
        maxRIndex = np.argmax(fft2_zeroDoppler)        
        # print("----------------------------")
        # print(fft2_zeroDoppler[maxRIndex])
        # print("----------------------------")

        ampZero = 20*np.log10(np.abs(fft2[0, :, maxRIndex])/FFT_SCALE)
        # print(ampZero)
        
        
        # калибровка
        for mimo in range(MIMO_CHANNEL_NUM):
            fft2[:, mimo, :] = fft2[:, mimo, :] * calibration_coef[mimo]

        # возьмем срез по каналам. по нему сделаем fft3
        a = fft2[0, :, maxRIndex]
        # делаем zero padding (в матлабе используется fft по 1024 точкам)
        aExt = np.concatenate((a, np.zeros(FFT3_SIZE - MIMO_CHANNEL_NUM, dtype=complex)))
        
        spectr_3D_cut = 20*np.log10(np.abs(np.fft.fftshift(np.fft.fft(aExt))/FFT_SCALE))
        # print("----------------------------")
        # print("--- spectr_3D_cut ----------")
        # print("----------------------------")
        # print(spectr_3D_cut)
        # print("----------------------------")
        # находим максимум по углу
        maxAzimuthIndex = np.argmax(spectr_3D_cut)   
        # перевод из индекса (0..1023) в шкалу [-1..1)
        angle = -1 + (2 * maxAzimuthIndex / FFT3_SIZE) # [-1..1)
        # перевод из [-1..1) -> [-90 .. 90)
        angle = math.asin(angle)*180/M_PI
        print("R index:%d, A index:%d, Angle:%.1f deg, value:%.1f" % (maxRIndex, maxAzimuthIndex, angle, spectr_3D_cut[maxAzimuthIndex]))
        # формируем точку результата
        point = [angle, spectr_3D_cut[maxAzimuthIndex]]
        resultPoint.append(point)
        # индикация в лог
        algorithmPrint("УГОЛ ПИКА: %.2f (Дальность: %d)" % (angle, maxRIndex))
        # результатом проверки является угол детектирования пика ( и индекс дальности)
        return ("%.2f %d" % (angle, maxRIndex))

    # Шаг формирования графика амплитуд для 0 положения
    if (param == "ZERO"):
        algorithmPrint("ZERO")
        print(ampZero)
        fig, ax = getGraph("энергетика по каналам радара")
        plt.xlabel("Номер приемного канала")
        plt.ylabel("Амплитуда, дБ")
        # график зависимости уровня сигнала от угла радара
        x = [x + 1 for x in range(12)]
        ax.plot(x, ampZero, 'o-', linewidth=2.0, markevery=1)
        ax.set_xlim(1,12)
        ax.set_xticks(x)
        # ax.grid(which='both') # скрыть сетку
        # экспорт графика. График идет в отчет и окно GUI
        exportFigure(fig, DIRECTORY_GUI_PNG + "/refAngle0Result.png")
        copyfile(DIRECTORY_GUI_PNG + "/refAngle0Result.png",
                 workDirectory + "/refAngle0Result.png")
        ##plt.show()

        

    if (param == "DONE"):
        algorithmPrint("DONE")
        print(resultPoint)
        HiLimitDetect = 0
        if (len(resultPoint) > 0):
            # значения точек нашей кривой. Вектор X - углы, V - значения
            x = [x for x, v in resultPoint]
            v = [v for x, v in resultPoint]
            # Если задан порог, проводим сравнение с порогом
            if len(HiLimitlevel) > 0:
                # значения порога (размерность может отличаться)
                limx = [x for x, v in HiLimitlevel]
                limv = [v for x, v in HiLimitlevel]

                # сформируем область значений между граничными углами измерения
                # приведем порог и измерения радара к одной область значений
                xCommon = np.linspace(resultPoint[0][0], resultPoint[-1][0], 50)
                vRadar = np.interp(xCommon, x, v, period=360)
                vLim = np.interp(xCommon, limx, limv, period=360)
                for r, limit in zip(vRadar, vLim):
                    if r < limit:
                        HiLimitDetect = 1

            fig, ax = getGraph("график зависимости амплитуды сигнала от угла поворота радара")
            plt.xlabel("Угол, градусы")
            plt.ylabel("Амплитуда (макс.), дБ")
            # график зависимости уровня сигнала от угла радара
            ax.plot(x, v, 'o-', label=('Радар'))

            print(x)
            
            if len(HiLimitlevel) > 0:
                addThresholdPlot(ax, limx, limv, err=HiLimitDetect)

            # график интерполированных значений
#            ax.plot(xCommon, vRadar, label=('Р1'))
#            ax.plot(xCommon, vLim, label=('Porog'))

            ax.legend(loc='upper right')
            # экспорт графика. График идет в отчет и окно GUI
            exportFigure(fig, DIRECTORY_GUI_PNG + "/refAngleResult.png")
            copyfile(DIRECTORY_GUI_PNG + "/refAngleResult.png",
                     workDirectory + "/refAngleResult.png")

            #plt.show()
            plt.close('all')
        else:
            algorithmPrint("no data!", error=1)

        if HiLimitDetect == 0:
            return "OK"
        else:
            return "ERROR"

        return "OK"

if __name__ == '__main__':
    print(win_nfft[0])
    global workDirectory
    
    DIRECTORY_GUI_PNG = "." + DIRECTORY_GUI_PNG
    DIRECTORY_CONFIG = "." + DIRECTORY_CONFIG
    algorithmCPYCAngle("START")
    workDirectory = "./workdir"
    algorithmCPYCAngle("CONFIG")
    algorithmCPYCAngle("CALC")
    algorithmCPYCAngle("ZERO")
    algorithmCPYCAngle("DONE")

