# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 17:00:20 2021

@author: Dunaev


Алгоритм "уровень шума в приёмном тракте":

Цель проверки: При отключенных передатчиках радара получить "суммарный" спектр из кадра
и сопоставить его с массивом порога. Если спектр выходит за порог значит НЕНОРМА.
Примечания: 
    1. Массив порога может отсутствовать, тогда проверка всегда завершается с результатом НОРМА
    2. Можно набрать любое число кадров, из всех спектров кадров будет взято максимальное значение.
    
Алгоритм расчета:
    После поучения кадра (набора чирпов), находим среднее значение для каждого канала (вдоль чирпов),
    получаем chirpmean (32х670), Усредненный чирп по каждому каналу.
    Перед первым fft из сырого сигнала вычитаем средний чирп (chirpmean), накладываем окно,
    делаем zero padding. 
    После fft берем максимум из всех чирпов. Получаем поканальный max fft (32х512).
    Его добавляем стат. массив (накопление по кадрам. Число кадров - произвольное).
    В конце сбора кадров, из стат.массива берем максимумы по кадрам.
    Получаем max max fft по каждому каналу. Далаем его сглаживание (movmean(c, 11)).
    Эти 32 графика являются основным результатом теста "FFT приемных каналов 1..16 TX1/2(max)"
    (Графики сгруппированы 2х16)
    Дополнительно, выводится max среди каналов. "Макс. по всем FFT для всех приемных каналов"
    

    
    
Взаимодействие с системой испытаний:
    Параметры запуска:
        "START" : перед началом расчета. 
        
        Возврат: Путь к рабочей директории
        
        "CALC" : Очередной кадр снят радаром и размещен в файл данных (mmap).
        Возврат: ["OK" | "ERROR"] - статус (пока игнорируется)

        "DONE" : Все кадры обработаны, нужно взять "суммарный" FFT и сравнить с порогом.
        Возврат: ["OK" | "ERROR"] - статус. "ERROR" - "суммарный" FFT выше порога -> НЕНОРМА


"""
from time import sleep

import os
import numpy as np
import matplotlib.pyplot as plt
import mmap
import math
import array
import struct
from shutil import copyfile
import shelve


FRAME_HEADER_SIZE   = 52
# Параметры формата данных (как в алгоритме матлаба)
CHIRP_SAMPLE_OFFSET = 14 
CHIRP_SAMPLE_NUM    = 670
RADAR_RX_CHANNEL    = 16
RADAR_TX_CHANNEL    = 2
# первые чирпы содержат перех.процесс. они отбрасываются
CHIRP_DROP_NUM      = 4
FFT1_SIZE           = 1024
WIN_KOEF            = 1
FFT1_SCALE          = (1/(CHIRP_SAMPLE_NUM * 32768 * 0.5)) # * 32 27.07.2022 Убираем, так как не должно быть. FFT2 не выполняется, делить а 32 не нужно.
# кол-во точек вначале спектра, которые обнуляю, что бы не мешали обнаружить пик на нужной частоте
ZERO_HEAD_PADDING   = 10

M_PI                = 3.14159265358979

# параметр DPI для figure. Значение больше: элементы крупнее.
dpi = 109

# размер графиков в пикселях (делаю все графики одинаковые по размеру)
PNG_SIZE_X          = 1200
PNG_SIZE_Y          = 650

# путь к GUI (Сводный график используется для GUI)
DIRECTORY_GUI_PNG = "./design"
# относительный путь к рабочей директории
DIRECTORY_WORK = "./algorithm/workdir"
# относительный путь к директории с отладочн. массивами
DIRECTORY_DEBUG = "./algorithm/dbg"
# относительный путь к директории параметров
DIRECTORY_CONFIG = "./algorithm/config"
# формат сетки графика
Y_SCALE = "linear"  # ["log" | "linear"]
# для простоты задал константой
CHEBWIN = [0.144313086501651, 0.128178606726778, 0.179593829248546, 0.239647659242570,0.307563918069857, 0.382090969451977, 0.461525309144597, 0.543764749628232,0.626389413100373, 0.706766693213832, 0.782174499840408, 0.849935611117232,0.907554947916852, 0.952851146902723, 0.984073985186177, 1, 1,0.984073985186177, 0.952851146902723, 0.907554947916852, 0.849935611117232,0.782174499840408, 0.706766693213832, 0.626389413100373, 0.543764749628232,0.461525309144597, 0.382090969451977, 0.307563918069857, 0.239647659242570, 0.179593829248546, 0.128178606726778, 0.144313086501651]


def algorithmPrint(text, error=0):
    print("algorithm[АЧХ]>", text)
    pass


def mmap_io(filename):
    with open(filename, mode="rb") as file_obj:
        with mmap.mmap(file_obj.fileno(), length=0, access=mmap.ACCESS_READ) as mmap_obj:
            buff = mmap_obj.read()
            return buff


# Для автономной отладки алгоритма (без радара/генератора)
# Используя радар и генератор, сохраняю ("save") данные измерений,
# Не имея радара эти измерения можно использовать для отладки ("load")
# action: ["save" | "load" | "anytext" - для возврата переменной без изменения
def dbgSerialize(filename, action, var):
    result = var
    if action == "save":
        # Сохраню накопл. данные для послед. анализа
        s = shelve.open(filename)
        # Кол-во точек измерения
        s['N'] = len(var)
        for i, r in enumerate(var):
            s[str(i)] = r
        s.close()
    if action == "load":
        s = shelve.open(filename)
        n = s['N']
        result = [[] for _ in range(n)]
        for i in range(n):
            result[i] = s[str(i)]
        s.close()
    return result


# Получаем порог из файла
def getHiLimit(mode):
    
    if mode is not None:
        dataFileName = DIRECTORY_CONFIG + ("/rfnoise77_%s.txt" % mode)
    else:
        dataFileName = DIRECTORY_CONFIG + "/rfnoise77.txt"
    
    data = []
    try:
        with open(dataFileName,"r") as f:
            for line in f:
                data.append(-95.0) #float(line))
    except IOError as err:
        print("File error: {0}".format(err))
    except ValueError:
        print("Could not convert data to an float (%s)." % line)
    if len(data) == 512:
        return data
    else:
        if len(data) > 0:
            print("Error threshold array size (%d)." % len(data))
        return []


# формирование окна Хемминга (алгоритм как в матлабе)
def getRangeWin(N, useChebwin=0):
    win = []
    meanValue = 0
    for i in range(N):
        value = 0.54 - 0.46 * math.cos(2.0 * M_PI * i / (N - 1))
        win.append(value)
        meanValue += value
    meanValue = meanValue / N
    if useChebwin > 0:
        chebmean = np.mean(CHEBWIN)
        meanValue = meanValue * chebmean
    for i in range(N):
        win[i] = win[i] / meanValue
    return win


def puzzleCh(i):
    puzzle = [13, 14, 16, 15, 9, 10, 12, 11, 5, 6, 8, 7, 1, 2, 4, 3]
    N = i // 16
    p = i % 16
    return (puzzle[p] - 1) + N*16


# сохранение изображения графика в файл
def exportFigure(plt, filename):
    try:
        plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=0.5)                
        plt.savefig(filename)
        algorithmPrint(filename + "...OK")
    except PermissionError:
        algorithmPrint('save fig Error...', error=1)
    pass


# формируем метки на оси X
def getXticks(N, step=1, first=0):
    return [x*step + first for x in range(N)]


# Общее оформление графиков
def getGraph(title):
    fig = plt.figure(dpi=dpi, figsize=(PNG_SIZE_X / dpi, PNG_SIZE_Y / dpi))
    ax = fig.add_subplot(111)  # 111 - это сетка. 111 означает сетка 1x1
    ax.set_yscale(Y_SCALE)
    ax.set_title((title))
    ax.grid()
    return fig, ax

# - изменяет кол-во точек! не так как в матлабе!
#def movmean(data_set, periods=3):
#    weights = np.ones(periods) / periods
#    return np.convolve(data_set, weights, mode='valid')


# сделал только симметричное окно (m - нечетное)
def movmean(x, m):
    n = len(x)
    half = m // 2
    movmean = np.zeros(n)
    for i in range(n):
        left = i - half
        right = i + half
        if (left < 0):
            left = 0
        if (right >= n):
            right = n - 1
        movmean[i]=np.mean(x[left:right+1])
    return movmean


def addThresholdPlot(ax, x, level, err=0):
    # Порог
    if (len(level) == 512):
        color='green'
        if err == 1:
            color='red'
        ax.plot(x, level, linestyle='--', color=color, linewidth=3, label='Порог')
    pass


# Функция алгоритма.
def algorithmRFNoise(param, debug=0):
    # окно для FFT
    global rangeWin
    # Максимальные значения FFT(вдоль доплера) для каждого кадра
    global resultMaxFFTFrame
    global resultMedianFFTFrame
    global RADAR_RX_CHANNEL
    global workDirectory
    global HiLimitlevel
    
    # результат проверки FFT с массивом порога. (Превышен/нет)
    # Если порог не задан, считаем, что ошибки нет
    HiLimitDetect = 0
    
    if (param == "TEST"):
        fileName = ("%s/rfNoise.bin" % workDirectory)
        data = mmap_io(fileName)
        if (len(data) < FRAME_HEADER_SIZE):
            algorithmPrint("MMAP ERROR!!! Data size:%d" % (len(data)), error=1)
        STX, LEN, T, TIME, GN, T1, T2, T3, T4, T5, T6, N_CHIRP, N_CH, N = struct.unpack_from( "<iiiqqhhhhhhIII", data, 0)
        algorithmPrint("STX: %d" % (STX))
        return "TEST-OK"

    if param.find("START") >= 0:
        resultMaxFFTFrame = []
        resultMedianFFTFrame = []
        rangeWin = getRangeWin(CHIRP_SAMPLE_NUM, useChebwin=0)
        rangeWin.extend(np.zeros(FFT1_SIZE - CHIRP_SAMPLE_NUM))
        mode = None
        if param.find(":") > 0:
            mode = param[param.find(":") + 1:]
        
        HiLimitlevel = getHiLimit(mode)

        copyfile(DIRECTORY_GUI_PNG + "/rfgenResultErr.png",
                 DIRECTORY_GUI_PNG + "/rfnoiseResult.png")

        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/" + DIRECTORY_WORK
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    # Шаг получения кадра данных (может быть несколько)
    if (param == "CALC"):
        algorithmPrint("CALC")
        if debug == 1:
            # загружаю данные из файла
            fileName = ("%s/rfNoise.bin" % DIRECTORY_DEBUG)
        else:
            fileName = ("%s/rfNoise.bin" % workDirectory)

        # Получаем данные от системы испытаний.
        # (полный кадр стрима, включая заголовок)
        data = mmap_io(fileName)

        # Если данных меньше чем заголовок - ошибка!
        if (len(data) < FRAME_HEADER_SIZE):
            # Ошибка формата данных.
            algorithmPrint("MMAP ERROR!!! Data size:%d" % (len(data)), error=1)
            return "ABORT"

        # Парсим заголовок кадра. В нем содержится информация
        # о кол-ве точек на чирп (возможно 720 и 378)
        STX, LEN, T, TIME, GN, T1, T2, T3, T4, T5, T6, N_CHIRP, N_CH, N = struct.unpack_from( "<iiiqqhhhhhhIII", data, 0)

        # algorithmPrint("STX: %d" % (STX))
        # algorithmPrint("FORMAT: [%d x %d x %d]" % (N_CHIRP, N_CH, N))

        RADAR_RX_CHANNEL = N_CH

        # Кол-во рабочих чирпов (без MIMO), которые идут в алгоритм
        CHIRP_NUM = N_CHIRP - CHIRP_DROP_NUM

        # Исходя из данных заголовка,
        # отбрасываем мусорные чирпы (N : кол-во точек АЦП в чирпе)
        drop_data_size = N * CHIRP_DROP_NUM * N_CH * 2
        adc_raw_array = array.array('h', data[FRAME_HEADER_SIZE + drop_data_size:])

        # проверка на корректность размера данных
        if (len(adc_raw_array) != N * CHIRP_NUM * N_CH):
            algorithmPrint("DATA FORMAT ERROR!!!", error=1)
            return 0

        # дополнение чирпа нулями, как в матлабе
        zeros = np.zeros(FFT1_SIZE - CHIRP_SAMPLE_NUM)

        # разбивка массива int16 на список чирпов (берем только полезную часть)
        # [xxxxxxADCADCADCADCADCADC...ADCADCxxxxx] - Массив выборок АЦП
        #        ^ <- CHIRP_SAMPLE_NUM  -> ^ - кол-во полезных выборок АЦП
        #        ^CHIRP_SAMPLE_OFFSET - Смещение полезных выборок АЦП
        # Сразу приводим очередность каналов в норму
        chirps = [[] for _ in range(RADAR_RX_CHANNEL * RADAR_TX_CHANNEL)]
        for n in range(len(adc_raw_array) // N):
            chirp = adc_raw_array[n*N + CHIRP_SAMPLE_OFFSET:n*N + CHIRP_SAMPLE_NUM + CHIRP_SAMPLE_OFFSET]
            chirps[puzzleCh(n % 32)].append(chirp)

        # находим среднее (вдоль доплера)
        chirpmean = np.mean(chirps, axis=1)
        # print("chirpmean:", len(chirpmean), "x", len(chirpmean[0]))
        # print(rangeWin[0:10], rangeWin[300])
        

        # формирую список из 32 пустых списков (один список на канал (MIMO))
        fft = [[] for _ in range(RADAR_RX_CHANNEL * RADAR_TX_CHANNEL)]
        # находим FFT
        for k, chirp in enumerate(chirps):
            for c, adc in enumerate(chirp):
                adc = adc - chirpmean[k]
                arg = np.concatenate((adc, zeros))
                arg = np.multiply(arg, rangeWin) #* CHEBWIN[c]
                result = np.abs(FFT1_SCALE * np.fft.rfft(arg))
                fft[k].append(result[0:512])

        # беру max по всем чирпам для каждого канала (вдоль Доплера)
#        fftmax = [fft[x][31] for x in range(32)]  # 1-й чирп
        fftmax = np.max(fft, axis=1)
        # TODO: Добавить вариант с накоплением по медианному значению 
        fftmedian = np.median(fft, axis=1)

        # Добавляем данные в список кадров
        resultMaxFFTFrame.append(fftmax)
        resultMedianFFTFrame.append(fftmedian)
        
        return "OK"
        
        
    if (param == "DONE"):
        algorithmPrint("DONE")
        print(len(resultMaxFFTFrame))
        if (len(resultMaxFFTFrame) > 0):
            # Найдем максимум вдоль всех кадров (если их больше чем 1)
            resultMaxFFT = np.max(resultMaxFFTFrame, axis=0)
            resultMedianFFT = np.max(resultMedianFFTFrame, axis=0)

            # Найдем минимальное значение на всех графиках (включая порог)
            minValue = 20*np.log10(np.min(resultMaxFFTFrame))
            if len(HiLimitlevel) > 0:
                minValue = np.min([np.min(HiLimitlevel), minValue])

            print("len(resultMaxFFT):", len(resultMaxFFT))

            # Алгоритм матлаб деалет movmean. (линейный формат)
            for i, c in enumerate(resultMaxFFT):
                resultMaxFFT[i] = movmean(c, 11)
                
            for i, c in enumerate(resultMedianFFT):
                resultMedianFFT[i] = movmean(c, 11)

            # После сглаживания переводим в лог.формат
            resultMaxFFT = 20*np.log10(resultMaxFFT)
            
            # После сглаживания переводим в лог.формат
            resultMedianFFT = 20*np.log10(resultMedianFFT)
                        
            # для сводки, возьму макс. по всем каналам
            resultMaxFFTCommon = np.max(resultMaxFFT, axis=0)
            resultMedianFFTCommon = np.max(resultMedianFFT, axis=0)

            # определим превышен порог или нет?
            if len(HiLimitlevel) > 0:
                if np.array_equal(np.maximum(resultMaxFFTCommon, HiLimitlevel) , HiLimitlevel) == False:
                    HiLimitDetect = 1
                    

            x = getXticks(len(resultMaxFFTCommon))
            fig, ax = getGraph("Макс. по всем FFT для всех приемных каналов")
            ax.plot(x, resultMaxFFTCommon, label=("Max"), linewidth=2)  # график спектра
            ax.plot(x, resultMedianFFTCommon, label=("Median"), linewidth=2)  # график спектра
            ax.legend(loc='upper right')
            # Порог
            addThresholdPlot(ax, x, HiLimitlevel, err=HiLimitDetect)
            # Для автомасштабирования меняю Y1 на minValue,
            # Что бы все графики были одного масштаба
            x1, x2, y1, y2 = ax.axis()
            ax.axis((x1, x2, minValue, y2))

            exportFigure(fig, DIRECTORY_GUI_PNG + "/rfnoiseResult.png")
            copyfile(DIRECTORY_GUI_PNG + "/rfnoiseResult.png",
                     workDirectory + "/rfnoiseResult.png")

#            plt.show()
            x1, x2, y1, y2 = ax.axis()

            for tx in [1, 2]:
                fig, ax = getGraph("FFT приемных каналов 1..16 TX%d(max)" % tx)
                for i in range(RADAR_RX_CHANNEL):
                    ax.plot(x, resultMaxFFT[i + RADAR_RX_CHANNEL * (tx - 1)], label=('RX %d' % (i + 1)), linewidth=1 + 1*(i//8))
                addThresholdPlot(ax, x, HiLimitlevel, err=HiLimitDetect)
                ax.legend(loc='upper right')
                ax.axis((x1, x2, y1, y2))
                exportFigure(fig, workDirectory + "/rfnoiseResultTX%d.png" % tx)
#            plt.show()
            # plt.close('all')

            for tx in [1, 2]:
                fig, ax = getGraph("FFT приемных каналов 1..16 TX%d(median)" % tx)
                for i in range(RADAR_RX_CHANNEL):
                    ax.plot(x, resultMedianFFT[i + RADAR_RX_CHANNEL * (tx - 1)], label=('RX %d' % (i + 1)), linewidth=1 + 1*(i//8))
                addThresholdPlot(ax, x, HiLimitlevel, err=HiLimitDetect)
                ax.legend(loc='upper right')
                ax.axis((x1, x2, y1, y2))
                exportFigure(fig, workDirectory + "/rfnoiseResultMedTX%d.png" % tx)
#            plt.show()
            plt.close('all')
            

        else:
            algorithmPrint("no data!", error=1)

        if HiLimitDetect == 0:
            return "OK"
        else:
            return "ERROR"



# режим автономной отладки модуля через algRunner.py 
if __name__ == '__main__':
    DIRECTORY_DEBUG = "." + DIRECTORY_DEBUG
    DIRECTORY_GUI_PNG = "." + DIRECTORY_GUI_PNG
    algorithmRFNoise("START", debug=1)
    workDirectory = "./workdir"
    algorithmRFNoise("CONFIG", debug=1)
    
    # algorithmRFCalib("CALC", debug=1)
    algorithmRFNoise("CALC", debug=1)
    algorithmRFNoise("DONE", debug=1)
# ALG_DEBUG           = 1