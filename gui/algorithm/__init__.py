# При добавлении новых алгоритмов в пакет "algorithm" нужно
# from .<имя файла> import <название функции алгоритма>
# Далее нужно открыть файл ../algRunner.py и добавить запуск нового алгоритма
import os
import sys


from .rfgen import algorithmRFGen
from .rfnoise import algorithmRFNoise
from .refangle import algorithmRefAngle
from .cpycTest import algorithmCPYCTest
from .cprradcshow import algorithmCPRRAdcShow
from .cpycAngle import algorithmCPYCAngle
from .eeprom_test import algorithmCrrEepromTest
from .eeprom_erase import algorithmCrrEepromErase
from .eeprom_read import algorithmCrrEepromRead, algorithmCrrTempCodeRead
from .eeprom_writeleak import algorithmCrrEepromWriteLeak
from .eeprom_writecalib import algorithmCrrEepromWriteCalib
from .cprr_wdt_off import algorithmCprrOffWdt
from .telnet_setNet import algorithmCprrNetSettings, algorithmCprrGetSN
from .cpycCalib import algorithmCPYCCalib
from .cpycNoise import algorithmCPYCNoise

from .rfLeak import algorithmRFLeak
from .rfCalib import algorithmRFCalib

sys.path.append(os.path.abspath("./algorithm/j1939driver/"))

# CAN драйвер радара
from .cpycCanDriver import algorithmCPYCCAN
