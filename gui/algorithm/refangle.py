# -*- coding: utf-8 -*-
"""
Created on Tue Jul  19 11:00:20 2021

@author: Dunaev


Алгоритм "контроль положения отражателя":

Цель проверки: Получить характеристику изменения амплитуды цели (уголка) в зависимости от угла
    
 
    
Взаимодействие с системой испытаний:
    Параметры запуска:
        "START" : перед началом расчета. 
        Возврат: Путь к рабочей директории
        
        "CONFIG" : Считать из файлов конфигурации LEAK и TM_VECTOR. 
        Возврат:  ["OK" | "ERROR"] - статус (пока игнорируется)
        
        "CALC" : Очередной кадр снят радаром и размещен в файл данных (mmap).
        Возврат: ["ABORT" | "%f"] - Угол, для которого нашли пик

        "DONE" : Все кадры обработаны, нужно взять "суммарный" FFT и сравнить с порогом.
        Возврат: ["OK" | "ERROR"] - статус. "ERROR" - отношение мин/макс. больше чем нужно


"""
from time import sleep

import os
import numpy as np
import matplotlib.pyplot as plt
import mmap
import math
import array
import struct
from shutil import copyfile
import shelve
import random  # для отладки нештатных ситуаций


FRAME_HEADER_SIZE   = 52
# Параметры формата данных (как в алгоритме матлаба)
CHIRP_SAMPLE_OFFSET = 14 
CHIRP_SAMPLE_NUM    = 670
RADAR_RX_CHANNEL    = 16
RADAR_TX_CHANNEL    = 2
MIMO_CHANNEL_NUM    = RADAR_RX_CHANNEL * RADAR_TX_CHANNEL
# первые чирпы содержат перех.процесс. они отбрасываются
CHIRP_DROP_NUM      = 4
FFT1_SIZE           = 1024
FFT1_SCALE          = 1 #(1/FFT1_SIZE)
WIN_KOEF            = (CHIRP_SAMPLE_NUM * 32 * 32768 * 0.5)
# кол-во точек вначале спектра, которые обнуляю, что бы не мешали обнаружить пик на нужной частоте
ZERO_HEAD_PADDING   = 10

M_PI                = 3.14159265358979

# параметр DPI для figure. Значение больше: элементы крупнее.
dpi = 109

# размер графиков в пикселях (делаю все графики одинаковые по размеру)
PNG_SIZE_X          = 1200
PNG_SIZE_Y          = 650

# путь к GUI (Сводный график используется для GUI)
DIRECTORY_GUI_PNG = "./design"
# относительный путь к рабочей директории
DIRECTORY_WORK = "./algorithm/workdir"
# относительный путь к директории с отладочн. массивами
DIRECTORY_DEBUG = "./algorithm/dbg/rAngle"
# относительный путь к директории параметров
DIRECTORY_CONFIG = "./algorithm/config"
# формат сетки графика
Y_SCALE = "linear"  # ["log" | "linear"]
# для простоты задал константой
CHEBWIN = [0.144313086501651, 0.128178606726778, 0.179593829248546, 0.239647659242570,0.307563918069857, 0.382090969451977, 0.461525309144597, 0.543764749628232,0.626389413100373, 0.706766693213832, 0.782174499840408, 0.849935611117232,0.907554947916852, 0.952851146902723, 0.984073985186177, 1, 1,0.984073985186177, 0.952851146902723, 0.907554947916852, 0.849935611117232,0.782174499840408, 0.706766693213832, 0.626389413100373, 0.543764749628232,0.461525309144597, 0.382090969451977, 0.307563918069857, 0.239647659242570, 0.179593829248546, 0.128178606726778, 0.144313086501651]


def algorithmPrint(text, error=0):
    print("algorithm[ОПУ]>", text)
    pass


def mmap_io(filename):
    with open(filename, mode="rb") as file_obj:
        with mmap.mmap(file_obj.fileno(), length=0, access=mmap.ACCESS_READ) as mmap_obj:
            buff = mmap_obj.read()
            return buff


# Для автономной отладки алгоритма (без радара/генератора)
# Используя радар и генератор, сохраняю ("save") данные измерений,
# Не имея радара эти измерения можно использовать для отладки ("load")
# action: ["save" | "load" | "anytext" - для возврата переменной без изменения
def dbgSerialize(filename, action, var):
    result = var
    if action == "save":
        # Сохраню накопл. данные для послед. анализа
        s = shelve.open(filename)
        # Кол-во точек измерения
        s['N'] = len(var)
        for i, r in enumerate(var):
            s[str(i)] = r
        s.close()
    if action == "load":
        s = shelve.open(filename)
        n = s['N']
        result = [[] for _ in range(n)]
        for i in range(n):
            result[i] = s[str(i)]
        s.close()
    return result


# Получаем порог из файла. Возврат: [[x, amp], [x, amp]...]
def getHiLimit():
    # AMP ANGLE
    # ...
    # AMP ANGLE
    dataFileName = DIRECTORY_CONFIG + "/refangle.txt"
    data = []
    try:
        with open(dataFileName, "r") as f:
            for line in f:
                items = line.split()
                if (len(items) == 2):
                    data.append([float(items[1]), float(items[0])])
                else:
                    raise ValueError('Format error!')
    except IOError as err:
        print("File error: {0}".format(err))
    except ValueError:
        print("Could not convert data to an float (%s)." % line)
    return data


# формирование окна Хемминга (алгоритм как в матлабе)
def getRangeWin(N, useChebwin=0):
    win = []
    meanValue = 0
    for i in range(N):
        value = 0.54 - 0.46 * math.cos(2.0 * M_PI * i / (N - 1))
        win.append(value)
        meanValue += value
    meanValue = meanValue / N
    if useChebwin > 0:
        chebmean = np.mean(CHEBWIN)
        meanValue = meanValue * chebmean
    for i in range(N):
        win[i] = win[i] / (meanValue * WIN_KOEF)
    return win


def puzzleCh(i):
    puzzle = [13, 14, 16, 15, 9, 10, 12, 11, 5, 6, 8, 7, 1, 2, 4, 3]
    N = i // 16
    p = i % 16
    return (puzzle[p] - 1) + N*16


# сохранение изображения графика в файл
def exportFigure(plt, filename):
    try:
        plt.savefig(filename)
        algorithmPrint(filename + "...OK")
    except PermissionError:
        algorithmPrint('save fig Error...', error=1)
    pass


# формируем метки на оси X
def getXticks(N, step=1, first=0):
    return [x*step + first for x in range(N)]


# Общее оформление графиков
def getGraph(title):
    fig = plt.figure(dpi=dpi, figsize=(PNG_SIZE_X / dpi, PNG_SIZE_Y / dpi))
    ax = fig.add_subplot(111)  # 111 - это сетка. 111 означает сетка 1x1
    ax.set_yscale(Y_SCALE)
    ax.set_title((title))
    ax.grid()
    return fig, ax


# построение графика порога
def addThresholdPlot(ax, x, level, err=0):
    # Порог
    color = 'green'
    if err == 1:
        color = 'red'
    ax.plot(x, level, linestyle='--', color=color, linewidth=3, label='Порог')
    pass


# Загружаем температурный вектор
def loadTmVector(fileName):
    data = []
    tmcode = 0
    try:
        with open(fileName, 'rb') as f:
            buff = f.read(4)
            if (len(buff) == 4):
                tmcode = struct.unpack('f', buff)[0]
                algorithmPrint("Температурный вектор для ТМ кода: %f" % tmcode)
            data = np.frombuffer(f.read(4*MIMO_CHANNEL_NUM*2 + 4), dtype=np.float32)
            data = [complex(data[i*2], data[i*2 + 1]) for i in range(MIMO_CHANNEL_NUM)]
    except IOError as err:
        print("File error: {0}".format(err))
        return [], 0
    except ValueError:
        print("Could not convert data to an float!")
        return [], 0
    if len(data) == 32:
        return data, tmcode
    else:
        if len(data) > 0:
            print("Error threshold array size (%d)." % len(data))
        return [], 0


# Загружаем вектор пролаза
def loadLeakVector(fileName):
    try:
        with open(fileName, 'rb') as f:
            buff = f.read(100000)
            N = len(buff) // MIMO_CHANNEL_NUM
            return [np.frombuffer(buff[i*N:i*N + N], dtype=np.int16) for i in range(MIMO_CHANNEL_NUM)]
    except IOError as err:
        print("File error: {0}".format(err))
    except ValueError:
        print("Could not convert data to an float!")

    return [[] for _ in range(MIMO_CHANNEL_NUM)]


# Функция алгоритма.
def algorithmRefAngle(param, debug=0):
    # окно для FFT
    global rangeWin
    # Результирующий список точек
    global resultPoint
    # число приемных антенн, получаемых из файла стрима
    global RADAR_RX_CHANNEL
    # радочая директори, куда скрипт испытаний (Lua) кладет данные
    global workDirectory
    # Порог, ниже, которого не должна опускаться наш график
    global HiLimitlevel
    # Вектор калибровки, для текущей температуры радара
    global TmVector
    # Пролаз
    global LeakVector

    # результат проверки отклика от цели с порогом. (Превышен/нет)
    # Если порог не задан, считаем, что ошибки нет
    HiLimitDetect = 0

    if (param == "TEST"):
        return "TEST-OK"

    if (param == "START"):
        resultPoint = []
        rangeWin = getRangeWin(CHIRP_SAMPLE_NUM, useChebwin=1)
        rangeWin.extend(np.zeros(FFT1_SIZE - CHIRP_SAMPLE_NUM))
        # Загрузка порога из файла
        HiLimitlevel = getHiLimit()

        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/" + DIRECTORY_WORK
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if (param == "CONFIG"):
        algorithmPrint("CONFIG")

        # загружаю данные из файла
        if debug == 1:
            tmVectorFileName = ("%s/config.tm.bin" % DIRECTORY_DEBUG)
            LeakFileName = ("%s/config.leak.bin" % DIRECTORY_DEBUG)
        else:
            tmVectorFileName = ("%s/config.tm.bin" % workDirectory)
            LeakFileName = ("%s/config.leak.bin" % workDirectory)

        # вектор калибровки для текущей температуры радара
        TmVector, tmCode = loadTmVector(tmVectorFileName)
        # Пролаз
        LeakVector = loadLeakVector(LeakFileName)
        # Краткое инфо о конфигурации
        algorithmPrint("TM File : %s. tmCode:%.1f [%f, %f ...]" % (tmVectorFileName, tmCode, np.real(TmVector[0]), np.imag(TmVector[0])))
        algorithmPrint("Leak File : %s" % LeakFileName)
        return "OK"

    # Шаг получения кадра данных (может быть несколько)
    if (param == "CALC"):
        algorithmPrint("CALC")
        if debug == 1:
            # загружаю данные из файла
            fileName = ("%s/refangle.bin%d" % (DIRECTORY_DEBUG, (len(resultPoint)+1)*1))
        else:
            fileName = ("%s/refangle.bin" % workDirectory)
        algorithmPrint("File : %s" % fileName)

        # Получаем данные от системы испытаний.
        # (полный кадр стрима, включая заголовок)
        data = mmap_io(fileName)

        # Если данных меньше чем заголовок - ошибка!
        if (len(data) < FRAME_HEADER_SIZE):
            # Ошибка формата данных.
            algorithmPrint("MMAP ERROR!!! Data size:%d" % (len(data)), error=1)
            return "ABORT"

        # Парсим заголовок кадра. В нем содержится информация
        # о кол-ве точек на чирп (возможно 720 и 378)
        STX, LEN, T, TIME, GN, T1, T2, T3, T4, T5, T6, N_CHIRP, N_CH, N = struct.unpack_from("<iiiqqhhhhhhIII", data, 0)

        algorithmPrint("STX: %d" % (STX))
        algorithmPrint("FORMAT: [%d x %d x %d]" % (N_CHIRP, N_CH, N))

        RADAR_RX_CHANNEL = N_CH

        # Кол-во рабочих чирпов (без MIMO), которые идут в алгоритм
        CHIRP_NUM = N_CHIRP - CHIRP_DROP_NUM

        # Исходя из данных заголовка,
        # отбрасываем мусорные чирпы (N : кол-во точек АЦП в чирпе)
        drop_data_size = N * CHIRP_DROP_NUM * N_CH * 2
        adc_raw_array = array.array('h', data[FRAME_HEADER_SIZE + drop_data_size:])

        # проверка на корректность размера данных
        if (len(adc_raw_array) != N * CHIRP_NUM * N_CH):
            algorithmPrint("DATA FORMAT ERROR!!!", error=1)
            return "ABORT"

        # дополнение чирпа нулями, как в матлабе
        zeros = np.zeros(FFT1_SIZE - CHIRP_SAMPLE_NUM)

        # разбивка массива int16 на список чирпов (берем только полезную часть)
        # [xxxxxxADCADCADCADCADCADC...ADCADCxxxxx] - Массив выборок АЦП
        #        ^ <- CHIRP_SAMPLE_NUM  -> ^ - кол-во полезных выборок АЦП
        #        ^CHIRP_SAMPLE_OFFSET - Смещение полезных выборок АЦП
        # Сразу приводим очередность каналов в норму
        chirps = [[] for _ in range(RADAR_RX_CHANNEL * RADAR_TX_CHANNEL)]
        for n in range(len(adc_raw_array) // N):
            # индекс канала в системе MIMO 32
            mimoN = puzzleCh(n % 32)
            # чирп, включая все точки переходного процесса
            chirp = adc_raw_array[n*N:n*N + N]
            # вычитаем протаз
            chirp = chirp - LeakVector[mimoN]
            # применяем фильтр-корректор
            chirp = np.diff(chirp)
            # берем только рабочие точки
            chirp = chirp[CHIRP_SAMPLE_OFFSET:CHIRP_SAMPLE_OFFSET + CHIRP_SAMPLE_NUM]
            # дополняем чипр к списку канала (MIMO : 0..31)
            chirps[mimoN].append(chirp)

#        # находим среднее (вдоль доплера)
#        chirpmean = np.mean(chirps, axis=1)
#
        # формирую список из 32 пустых списков (один список на канал (MIMO))
        fft = [[] for _ in range(RADAR_RX_CHANNEL * RADAR_TX_CHANNEL)]
        # находим FFT
        for k, chirp in enumerate(chirps):
            for c, adc in enumerate(chirp):
                # резерв: вычитание фона
                # adc = adc - chirpmean[k]
                arg = np.concatenate((adc, zeros))
                arg = np.multiply(arg, rangeWin) * CHEBWIN[c] * TmVector[k]
                result = FFT1_SCALE * np.fft.fft(arg)
                # сохраняю только дальности, которые соответствуют положению уголка
#                fft[k].append(result[3:7])
                # сохраняем все дальности (как в матлабе)
                fft[k].append(result[0:512])

        # беру FFT2 вдоль доплера
        fft2_f = np.fft.fft(fft, axis=1)
        # делаю размерность как в матлабе (487 точек по дальности)
        fft2 = fft2_f[:, :, 0:487]
        #2022.06.08: Исключаю начальные точки (0,1,2)
        fft2[:,:,0:3] = 1E-12 # можно [0,0,0:3]
        # найдем максимум в 0-м доплере
        fft2_zeroDoppler = 20*np.log10(np.abs(fft2[0][0]))

        # поиск максимального значения в нулевом доплере
        maxIndex = np.argmax(fft2_zeroDoppler)
        rangeIndex = maxIndex
        phase_distribution = fft2[:, 0, maxIndex]
        if (maxIndex > 10)and(maxIndex < 2):
            algorithmPrint("ERROR! Peak index error! (%d)" % maxIndex)

        # в найденом срезе ведем поиск максимума
        # наложение окна. Влад сказал, что окно нужно чтобы увидеть
        # более мелкие цели, а в нашем случае их нет вообще
        arg = phase_distribution  # * CHEBWIN
        # дополняем zero padding
        arg = np.concatenate((arg, np.zeros(1024 - len(arg))))
        # расчет FFT c переводом в дБ
        azimuth_spectr = 20*np.log10(np.abs(np.fft.fftshift(np.fft.fft(arg))))
        # поиск индекса макс. по азимуту
        maxIndex = np.argmax(azimuth_spectr)
        # перевод индекса в градусы. Вся шкала 180.
        angle = 180/M_PI * math.asin((maxIndex / (len(arg)/2)) - 1)

        # генерация неверного угла (для отладки):
        # angle = random.random()*180 - 90

        # формируем точку результата
        point = [angle, azimuth_spectr[maxIndex]]
        resultPoint.append(point)
        # индикация в лог
        algorithmPrint("УГОЛ ПИКА: %.2f (Дальность: %d)" % (angle, rangeIndex))
        # результатом проверки является угол детектирования пика ( и индекс дальности)
        return ("%.2f %d" % (angle, rangeIndex))

    if (param == "DONE"):
        algorithmPrint("DONE")

        if (len(resultPoint) > 0):
            # значения точек нашей кривой. Вектор X - углы, V - значения
            x = [x for x, v in resultPoint]
            v = [v for x, v in resultPoint]
            # Если задан порог, проводим сравнение с порогом
            if len(HiLimitlevel) > 0:
                # значения порога (размерность может отличаться)
                limx = [x for x, v in HiLimitlevel]
                limv = [v for x, v in HiLimitlevel]

                # сформируем область значений между граничными углами измерения
                # приведем порог и измерения радара к одной область значений
                xCommon = np.linspace(resultPoint[0][0], resultPoint[-1][0], 50)
                vRadar = np.interp(xCommon, x, v, period=360)
                vLim = np.interp(xCommon, limx, limv, period=360)
                for r, limit in zip(vRadar, vLim):
                    if r < limit:
                        HiLimitDetect = 1

            fig, ax = getGraph("график зависимости амплитуды сигнала от угла поворота радара")
            plt.xlabel("Угол, градусы")
            plt.ylabel("Амплитуда (макс.), дБ")
            # график зависимости уровня сигнала от угла радара
            ax.plot(x, v, 'o-', label=('Радар'))

            if len(HiLimitlevel) > 0:
                addThresholdPlot(ax, limx, limv, err=HiLimitDetect)

            # график интерполированных значений
#            ax.plot(xCommon, vRadar, label=('Р1'))
#            ax.plot(xCommon, vLim, label=('Porog'))

            ax.legend(loc='upper right')
            # экспорт графика. График идет в отчет и окно GUI
            exportFigure(fig, DIRECTORY_GUI_PNG + "/refAngleResult.png")
            copyfile(DIRECTORY_GUI_PNG + "/refAngleResult.png",
                     workDirectory + "/refAngleResult.png")

            plt.show()
            plt.close('all')
        else:
            algorithmPrint("no data!", error=1)

        if HiLimitDetect == 0:
            return "OK"
        else:
            return "ERROR"
