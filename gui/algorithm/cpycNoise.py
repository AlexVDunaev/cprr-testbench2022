# -*- coding: utf-8 -*-
"""
Created 2022.06.22

@author: Dunaev


Алгоритм "Уровень шума в приёмном тракте":


"""
import os
import numpy as np
import matplotlib.pyplot as plt
import mmap
import math
import array
from shutil import copyfile
import json

import sys
sys.path.append(os.path.abspath("./algorithm/"))


# Параметры формата данных (как в алгоритме матлаба)
CHIRP_SAMPLE_NUM    = 256
RADAR_RX_CHANNEL    = 4
RADAR_TX_CHANNEL    = 3
MIMO_CHANNEL_NUM    = RADAR_RX_CHANNEL * RADAR_TX_CHANNEL

CHIRP_NUM           = 128
FFT1_SIZE           = 256
FFT3_SIZE           = 1024
# коэф. перевода в дБ (хотя Александра сказала что это неверные дБ)
FFT_SCALE           = CHIRP_SAMPLE_NUM*(2**15)

M_PI                = 3.14159265358979

# параметр DPI для figure. Значение больше: элементы крупнее.
dpi = 109

# размер графиков в пикселях (делаю все графики одинаковые по размеру)
PNG_SIZE_X          = 1200
PNG_SIZE_Y          = 650

# путь к GUI (Сводный график используется для GUI)
DIRECTORY_GUI_PNG = "./design"
# относительный путь к рабочей директории
DIRECTORY_WORK = "./algorithm/workdir"
# относительный путь к директории с отладочн. массивами
DIRECTORY_DEBUG = "./algorithm/dbg/rAngle"
# относительный путь к директории параметров
DIRECTORY_CONFIG = "./algorithm/config"
# файл калибровки радара
RADAR_CALIB_FILE = "radarCalib.json"

# формат сетки графика
Y_SCALE = "linear"  # ["log" | "linear"]
# для простоты задал константой


def algorithmPrint(text, error=0):
    print("algorithm[NOISE]>", text)
    pass

# сохранение изображения графика в файл
def exportFigure(plt, filename):
    try:
        plt.savefig(filename)
        algorithmPrint(filename + "...OK")
    except PermissionError:
        algorithmPrint('save fig Error...', error=1)
    pass


# формируем метки на оси X
def getXticks(N, step=1, first=0):
    return [x*step + first for x in range(N)]


# Общее оформление графиков
def getGraph(title):
    fig = plt.figure(dpi=dpi, figsize=(PNG_SIZE_X / dpi, PNG_SIZE_Y / dpi))
    ax = fig.add_subplot(111)  # 111 - это сетка. 111 означает сетка 1x1
    ax.set_yscale(Y_SCALE)
    ax.set_title((title))
    ax.grid()
    return fig, ax

def addThresholdPlot(ax, x, level, err=0):
    # Порог
    if (len(level) == FFT1_SIZE):
        color='green'
        if err == 1:
            color='red'
        ax.plot(x, level, linestyle='--', color=color, linewidth=3, label='Порог')
    pass


def mmap_io(filename):
    with open(filename, mode="rb") as file_obj:
        with mmap.mmap(file_obj.fileno(), length=0, access=mmap.ACCESS_READ) as mmap_obj:
            buff = mmap_obj.read()
            return buff


# сделал только симметричное окно (m - нечетное)
def movmean(x, m):
    n = len(x)
    half = m // 2
    movmean = np.zeros(n)
    for i in range(n):
        left = i - half
        right = i + half
        if (left < 0):
            left = 0
        if (right >= n):
            right = n - 1
        movmean[i]=np.mean(x[left:right+1])
    return movmean

# Получаем порог из файла
def getHiLimit():
    dataFileName = DIRECTORY_CONFIG + "/rfnoise77.txt"
    data = []
    try:
        with open(dataFileName,"r") as f:
            for line in f:
                data.append(float(line))
    except IOError as err:
        print("File error: {0}".format(err))
    except ValueError:
        print("Could not convert data to an float (%s)." % line)
    if len(data) == FFT1_SIZE:
        return data
    else:
        if len(data) > 0:
            print("Error threshold array size (%d)." % len(data))
        return []


# Функция алгоритма.
def algorithmCPYCNoise(param, debug=0):
    # радочая директори, куда скрипт испытаний (Lua) кладет данные
    global workDirectory
    global rxNoise
    global HiLimitlevel


    if (param == "START"):
        rxNoise = []
        HiLimitlevel = getHiLimit()

        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/" + DIRECTORY_WORK
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    # Шаг получения кадра данных (может быть несколько)
    if (param == "CALC"):
        algorithmPrint("CALC")
        fileName = "%s/noise.bin" % workDirectory
        if debug > 0:
            # загружаю данные из файла
            fileName = "%s/noise.bin%d" % (workDirectory, debug - 1)

        print(fileName)
        # Получаем данные от системы испытаний.
        # (полный кадр стрима, включая заголовок)
        data = mmap_io(fileName)

        adc_raw_array = array.array('h', data)

        # проверка на корректность размера данных
        if (len(adc_raw_array) != 2*CHIRP_SAMPLE_NUM * CHIRP_NUM * RADAR_RX_CHANNEL):
            algorithmPrint("DATA FORMAT ERROR!!!", error=1)
            return "ABORT"

        # разбивка массива int16*2 на список чирпов
        adc = np.array(adc_raw_array).reshape(CHIRP_NUM*RADAR_RX_CHANNEL*CHIRP_SAMPLE_NUM,2)
        # перевод к комплексному виду
        adc = adc[...,0] + 1j*adc[...,1];
        # разбивка на чирпы (adc[128][4][256])
        chirpArray = np.array(adc).reshape(CHIRP_NUM, CHIRP_SAMPLE_NUM, RADAR_RX_CHANNEL)

        rxData = np.zeros((CHIRP_NUM, RADAR_RX_CHANNEL, FFT1_SIZE))
        
        for chirp in range(CHIRP_NUM):
            for rx in range(RADAR_RX_CHANNEL):
                fft1 = np.fft.fft(chirpArray[chirp, :, rx])
                rxData[chirp, rx, :] = np.abs(fft1)/FFT_SCALE

        maxRx = np.max(rxData, axis = 0)
        
        for rx in range(RADAR_RX_CHANNEL):
            maxRx[rx, :] = 20*np.log10(movmean(maxRx[rx, :], 5))
            
        rxNoise.append(maxRx)
        return ("%.2f %d" % (0, 0))

    if (param == "DONE"):
        # Если порог не задан, считаем, что ошибки нет
        HiLimitDetect = 0             
        
        algorithmPrint("DONE")
        print("------------------------------")
        print("Noise test done. Frames :", len(rxNoise))
        print("------------------------------")
        if len(rxNoise) > 0:
            # максимумы по каждому каналу
            rxNoiseMax = np.max(rxNoise, axis = 0)
            # Сводный максимум (все каналы)
            rxAllNoiseMax = np.max(rxNoiseMax, axis = 0)

            # ---------------------------------------
            # Экспорт порога
            # porog = np.max(rxNoiseMax, axis = 0) + 5
            # with open(workDirectory + '/rfnoise77.txt', 'w', encoding='utf-8') as f:
            #     for i in range(FFT1_SIZE):
            #         f.write('%.5f\n' % porog[i])
            # ---------------------------------------
    
            # Найдем предельные значения на всех графиках (включая порог)
            minValue = np.min(rxAllNoiseMax)
            maxValue = np.max(rxAllNoiseMax)
            if len(HiLimitlevel) > 0:
                minValue = np.min([np.min(HiLimitlevel), minValue])
                maxValue = np.max([np.max(HiLimitlevel), maxValue])

            # определим превышен порог или нет?
            if len(HiLimitlevel) > 0:
                if np.array_equal(np.maximum(rxAllNoiseMax, HiLimitlevel) , HiLimitlevel) == False:
                    HiLimitDetect = 1
            
        
            x = [x for x in range(FFT1_SIZE)]

            fig, ax = getGraph("Шум в приемных каналах (max)")
            for i in range(RADAR_RX_CHANNEL):
                ax.plot(x, rxNoiseMax[i], label=('RX %d' % (i + 1)))
            ax.legend(loc='upper right')
            addThresholdPlot(ax, x, HiLimitlevel, err=HiLimitDetect)
            x1, x2, y1, y2 = ax.axis()
            ax.axis((x1, x2, minValue-1, maxValue))
            
            exportFigure(fig, workDirectory + "/rfnoiseResult.png")
            #copyfile(workDirectory + "/rfnoiseResult.png", DIRECTORY_GUI_PNG + "/rfnoiseResult.png")
            copyfile(DIRECTORY_GUI_PNG + "/rfnoiseResult.png", workDirectory + "/rfnoiseResult.png")
            # plt.show()
        if HiLimitDetect == 0:
            return "OK"
        else:
            return "ERROR"

if __name__ == '__main__':
    global workDirectory
    
    DIRECTORY_GUI_PNG = "." + DIRECTORY_GUI_PNG
    DIRECTORY_CONFIG = "." + DIRECTORY_CONFIG
    algorithmCPYCNoise("START")
    workDirectory = "./workdir"
    # algorithmCPYCNoise("CALC", 1)
    for n in range(20):
        algorithmCPYCNoise("CALC", n + 1)
    algorithmCPYCNoise("DONE")

