# -*- coding: utf-8 -*-
"""
Created on 06/07/2021

@author: Dunaev


"""

import os
import numpy as np
import matplotlib.pyplot as plt
import mmap
import math
import array
import struct
from shutil import copyfile
import shelve

import sys

sys.path.append(os.path.abspath("../"))

import algRunner


FRAME_HEADER_SIZE   = 52
# Параметры формата данных (как в алгоритме матлаба)
CHIRP_SAMPLE_OFFSET = 14 
CHIRP_SAMPLE_NUM    = 670
RADAR_RX_CHANNEL    = 16
RADAR_TX_CHANNEL    = 2
MIMO_CHANNEL_NUM    = RADAR_RX_CHANNEL * RADAR_TX_CHANNEL
# первые чирпы содержат перех.процесс. они отбрасываются
CHIRP_DROP_NUM      = 4
FFT1_SIZE           = 1024
FFT3_SIZE           = 1024
FFT1_SCALE          = (CHIRP_SAMPLE_NUM*32768//2) #(1/FFT1_SIZE)
# WIN_KOEF            = (CHIRP_SAMPLE_NUM * 32 * 32768 * 0.5)
# кол-во точек вначале спектра, которые обнуляю, что бы не мешали обнаружить пик на нужной частоте
ZERO_HEAD_PADDING   = 10

M_PI                = 3.14159265358979

# для простоты задал константой
CHEBWIN = [0.144313086501651, 0.128178606726778, 0.179593829248546, 0.239647659242570,0.307563918069857, 0.382090969451977, 0.461525309144597, 0.543764749628232,0.626389413100373, 0.706766693213832, 0.782174499840408, 0.849935611117232,0.907554947916852, 0.952851146902723, 0.984073985186177, 1, 1,0.984073985186177, 0.952851146902723, 0.907554947916852, 0.849935611117232,0.782174499840408, 0.706766693213832, 0.626389413100373, 0.543764749628232,0.461525309144597, 0.382090969451977, 0.307563918069857, 0.239647659242570, 0.179593829248546, 0.128178606726778, 0.144313086501651]

win_mfft = [0.203679205294911,0.218470244422645,0.259184907600764,0.318237554524374,0.388619272536956,0.465091533233986,0.543796133387493,0.621757998430097,0.696570971326454,0.766226864546524,0.829027102856969,0.883538879322962,0.928574928219786,0.963185622675767,0.986657181200979,0.998512480020220,0.998512480020220,0.986657181200979,0.963185622675767,0.928574928219786,0.883538879322962,0.829027102856969,0.766226864546524,0.696570971326454,0.621757998430097,0.543796133387493,0.465091533233986,0.388619272536956,0.318237554524374,0.259184907600764,0.218470244422645,0.203679205294911]  
TmVector = [complex(1, 0)] * MIMO_CHANNEL_NUM            
# параметр DPI для figure. Значение больше: элементы крупнее.
DPI = 72 


# размер графиков в пикселях (делаю все графики одинаковые по размеру)
PNG_SIZE_X          = 1200
PNG_SIZE_Y          = 650

# путь к GUI (Сводный график используется для GUI)
DIRECTORY_GUI_PNG = "./design"
# относительный путь к рабочей директории
DIRECTORY_WORK = "./algorithm/workdir"
# относительный путь к директории с отладочн. массивами
DIRECTORY_DEBUG = "./algorithm/dbg"
# формат сетки графика
Y_SCALE = "linear"  # ["log" | "linear"]

RADAR_CALIB_FILE = "Calibration_matrix_32_radar.h"


def algorithmPrint(text, error=0):
    print("algorithm[CALIB]>", text)
    pass


def mmap_io(filename):
    with open(filename, mode="rb") as file_obj:
        with mmap.mmap(file_obj.fileno(), length=0, access=mmap.ACCESS_READ) as mmap_obj:
            buff = mmap_obj.read()
            return buff

# сохранение изображения графика в файл
def exportFigure(plt, filename):
    try:
        plt.savefig(filename)
        algorithmPrint(filename + "...OK")
    except PermissionError:
        algorithmPrint('save fig Error...', error=1)
    pass


# формируем метки на оси X
def getXticks(N, step=1, first=0):
    return [x*step + first for x in range(N)]


def puzzleCh(i):
    puzzle = [13, 14, 16, 15, 9, 10, 12, 11, 5, 6, 8, 7, 1, 2, 4, 3]
    N = i // 16
    p = i % 16
    return (puzzle[p] - 1) + N*16


# формирование окна Хемминга (алгоритм как в матлабе)
def getRangeWin(N, useChebwin=0):
    win = []
    meanValue = 0
    for i in range(N):
        value = 0.54 - 0.46 * math.cos(2.0 * M_PI * i / (N - 1))
        win.append(value)
        meanValue += value
    meanValue = meanValue / N
    if useChebwin > 0:
        chebmean = np.mean(CHEBWIN)
        meanValue = meanValue * chebmean
    for i in range(N):
        win[i] = win[i] / (meanValue)
    return win


# Общее оформление графиков
def getGraph(title, x = PNG_SIZE_X, y = PNG_SIZE_Y, dpi = DPI):
    fig = plt.figure(dpi=dpi, figsize=(x / dpi, y / dpi))
    ax = fig.add_subplot(111)  # 111 - это сетка. 111 означает сетка 1x1
    ax.set_yscale(Y_SCALE)
    ax.set_title((title))
    ax.grid()
    return fig, ax

# Leakage - int[32][720] - Массив для экспорта
def exportLeak(filename, Leakage):
    LeakFileName = filename + ".h"
    LeakBinFileName = filename + ".bin"
    try:
        print('Export leak (bin) to "{}"'.format(LeakBinFileName))
        
        f = open(LeakBinFileName, "wb")
        for ch in Leakage:
            f.write(array.array('h', ch))
        f.close()
    except  OSError as e:
        print('[File:"{}"] Error: {}'.format(LeakBinFileName, e.strerror))
        
    try:
        print('Export leak (text) to "{}"'.format(LeakFileName))
        f = open(LeakFileName, "w")
        for i, ch in enumerate(Leakage):
            line = "{"
            for a in ch:
                line = line + ("%d, " %  a)
            line = line[:-2] + "}"
            if i < 31:    
               line = line + ","
            f.write("%s\n" % line)
        f.close()
    except  OSError as e:
        print('[File:"{}"] Error: {}'.format(LeakFileName, e.strerror))

# Загружаем температурный вектор
def loadTmVector(fileName):
    data = []
    tmcode = 0
    try:
        with open(fileName, 'rb') as f:
            buff = f.read(4)
            if (len(buff) == 4):
                tmcode = struct.unpack('f', buff)[0]
                algorithmPrint("Температурный вектор для ТМ кода: %f" % tmcode)
            data = np.frombuffer(f.read(4*MIMO_CHANNEL_NUM*2 + 4), dtype=np.float32)
            data = [complex(data[i*2], data[i*2 + 1]) for i in range(MIMO_CHANNEL_NUM)]
    except IOError as err:
        print("File error: {0}".format(err))
        return [], 0
    except ValueError:
        print("Could not convert data to an float!")
        return [], 0
    if len(data) == 32:
        return data, tmcode
    else:
        if len(data) > 0:
            print("Error threshold array size (%d)." % len(data))
        return [], 0


# Загружаем вектор пролаза
def loadLeakVector(fileName):
    try:
        with open(fileName, 'rb') as f:
            buff = f.read(100000)
            N = len(buff) // MIMO_CHANNEL_NUM
            return [np.frombuffer(buff[i*N:i*N + N], dtype=np.int16) for i in range(MIMO_CHANNEL_NUM)]
    except IOError as err:
        print("File error: {0}".format(err))
    except ValueError:
        print("Could not convert data to an float!")

    return [[] for _ in range(MIMO_CHANNEL_NUM)]


def CalibToStr(calib):
    strResult = ""
    for i in range(MIMO_CHANNEL_NUM):
        strResult = strResult + "%.10f %.10f " % (np.real(calib[i]), np.imag(calib[i]))
    strResult = strResult[:-1]
    return strResult 


# Функция алгоритма.
def algorithmRFCalib(param, debug=0):
    # окно для FFT
    global rangeWin
    global win_mfft
    # Результирующий список точек
    global resultPoint
    # число приемных антенн, получаемых из файла стрима
    global RADAR_RX_CHANNEL
    # радочая директори, куда скрипт испытаний (Lua) кладет данные
    global workDirectory
    # Порог, ниже, которого не должна опускаться наш график
    global HiLimitlevel
    # Вектор калибровки, для текущей температуры радара
    global TmVector
    # Пролаз
    global LeakVector
    global calibration_coef
    global phase_dist
    global number_pd


    if (param == "START"):
        phase_dist = [complex(0, 0)] * MIMO_CHANNEL_NUM
        number_pd = 0
        
        resultPoint = []
        rangeWin = getRangeWin(CHIRP_SAMPLE_NUM, useChebwin=0)
        rangeWin.extend(np.zeros(FFT1_SIZE - CHIRP_SAMPLE_NUM))

        algorithmPrint("START Calib [%f, %f , %f, %f...] " % (np.real(TmVector[0]), np.imag(TmVector[0]), np.real(TmVector[1]), np.imag(TmVector[1])))
        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/" + DIRECTORY_WORK
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if param.find("CONFIG") >= 0: 
        
        algorithmPrint("CONFIG")

        # загружаю данные из файла
        if debug == 1:
            tmVectorFileName = ("%s/config.tm.bin" % DIRECTORY_DEBUG)
            LeakFileName = ("%s/config.leak.bin" % DIRECTORY_DEBUG)
        else:
            tmVectorFileName = ("%s/config.tm.bin" % workDirectory)
            LeakFileName = ("%s/config.leak.bin" % workDirectory)

        # вектор калибровки для текущей температуры радара
        TmVector, tmCode = loadTmVector(tmVectorFileName)
        # Пролаз
        LeakVector = loadLeakVector(LeakFileName)
        # Использовать единичную калибровку!
        if param.find(":ONE") >= 0: 
            TmVector = [complex(1, 0)] * MIMO_CHANNEL_NUM
            print("Режим единичной матрицы калибровки")
        
        # Краткое инфо о конфигурации
        algorithmPrint("TM File : %s. tmCode:%.1f [%f, %f ...]" % (tmVectorFileName, tmCode, np.real(TmVector[0]), np.imag(TmVector[0])))
        algorithmPrint("Leak File : %s" % LeakFileName)
        return "INFO> tmCode:%.1f [%f, %f , %f, %f...] " % (tmCode, np.real(TmVector[0]), np.imag(TmVector[0]), np.real(TmVector[1]), np.imag(TmVector[1]))


    if param.find("DONE") >= 0: 
        algorithmPrint("DONE")
        filename = workDirectory +"/" + RADAR_CALIB_FILE
        if param.find(":") > 0:
            filename = param[param.find(":") + 1:]
        
        print("------------------------------")
        print("Calibration Done. Frames :", number_pd)
        print("------------------------------")
        if number_pd > 0:
            phase_dist = phase_dist / number_pd
            phase_distribution = phase_dist / phase_dist[(MIMO_CHANNEL_NUM//4) - 1]
            Calibration_matrix = np.diag(phase_distribution)
            Calibration_matrix = np.linalg.inv(Calibration_matrix)
            TmMatrix = np.diag(TmVector)
            Calibration_matrix = Calibration_matrix * TmMatrix;
            Calibration_array = Calibration_matrix.diagonal()
            # print("------------------------------")
            # print(Calibration_array)
            # print("------------------------------")
            calib = Calibration_array
            # Теперь это новая калибровка. 
            # Если на след. шаге не вызвать "CONFIG", будет использована данныя калибровка
            TmVector = Calibration_array
            try:
                filename = filename.replace("\\", "/")
                # Сообщим путь к файлам калибровки
                algRunner.runAlgorithm("CALIBDIR", os.path.dirname(filename))
                print("Write to file: ", filename)
                with open(filename, 'w', encoding='utf-8') as f:
                    for i in range(MIMO_CHANNEL_NUM):
                        f.write("{%.9f, %.9f}" % (np.real(calib[i]), np.imag(calib[i])))
                        if i < MIMO_CHANNEL_NUM - 1:
                            f.write(",\n")
            except  OSError as e:
                print('[File:"{}"] Error: {}'.format(filename, e.strerror))
                return "ERROR"

            return CalibToStr(calib)

        return "ERROR"

    if (param == "CALC"):
        if debug == 1:
            # загружаю данные из файла
            fileName = ("%s/calib_adc.bin" % DIRECTORY_DEBUG)
        else:
            fileName = ("%s/calib_adc.bin" % workDirectory)
            # algorithmPrint("[CALC] NONE!")
            # return "OK"
            

        # Получаем данные от системы испытаний.
        # (полный кадр стрима, включая заголовок)
        data = mmap_io(fileName)

        # Если данных меньше чем заголовок - ошибка!
        if (len(data) < FRAME_HEADER_SIZE):
            # Ошибка формата данных.
            algorithmPrint("MMAP ERROR!!! Data size:%d" % (len(data)), error=1)
            return -1

        # Парсим заголовок кадра. В нем содержится информация
        # о кол-ве точек на чирп (возможно 720 и 378)
        STX, LEN, T, TIME, GN, T1, T2, T3, T4, T5, T6, N_CHIRP, N_CH, N = struct.unpack_from( "<iiiqqhhhhhhIII", data, 0)

        # algorithmPrint("FORMAT: [%d x %d x %d]" % (N_CHIRP, N_CH, N))

        RADAR_RX_CHANNEL = N_CH
        
        # Кол-во рабочих чирпов (без MIMO), которые идут в алгоритм
        CHIRP_NUM = N_CHIRP - CHIRP_DROP_NUM

        # Исходя из данных заголовка,
        # отбрасываем мусорные чирпы (N : кол-во точек АЦП в чирпе)
        drop_data_size = N * CHIRP_DROP_NUM * N_CH * 2
        adc_raw_array = array.array('h', data[FRAME_HEADER_SIZE + drop_data_size:])

        # проверка на корректность размера данных
        if (len(adc_raw_array) != N * CHIRP_NUM * N_CH):
            algorithmPrint("DATA FORMAT ERROR!!!", error=1)
            return 0
        
        # adcPack32 = np.array(adc_raw_array).reshape(CHIRP_NUM//2, 2*RADAR_RX_CHANNEL, N)
        # дополнение чирпа нулями, как в матлабе
        zeros = np.zeros(FFT1_SIZE - CHIRP_SAMPLE_NUM)

        # разбивка массива int16 на список чирпов (берем только полезную часть)
        # [xxxxxxADCADCADCADCADCADC...ADCADCxxxxx] - Массив выборок АЦП
        #        ^ <- CHIRP_SAMPLE_NUM  -> ^ - кол-во полезных выборок АЦП
        #        ^CHIRP_SAMPLE_OFFSET - Смещение полезных выборок АЦП
        # Сразу приводим очередность каналов в норму
        chirps = [[] for _ in range(RADAR_RX_CHANNEL * RADAR_TX_CHANNEL)]
        for n in range(len(adc_raw_array) // N):
            # индекс канала в системе MIMO 32
            mimoN = puzzleCh(n % 32)
            # чирп, включая все точки переходного процесса
            chirp = adc_raw_array[n*N:n*N + N]
            # вычитаем протаз
            chirp = chirp - LeakVector[mimoN]
            # применяем фильтр-корректор
            chirp = np.diff(chirp)
            # берем только рабочие точки
            chirp = chirp[CHIRP_SAMPLE_OFFSET:CHIRP_SAMPLE_OFFSET + CHIRP_SAMPLE_NUM]
            # дополняем чипр к списку канала (MIMO : 0..31)
            chirps[mimoN].append(chirp)

        # Заготовка куба: 32 x 32 x 1024
        cube = np.zeros((CHIRP_NUM//2, MIMO_CHANNEL_NUM, FFT1_SIZE), dtype=complex)

        for k, chirp in enumerate(chirps):
            for c, adc in enumerate(chirp):
                adcWin = np.multiply(np.concatenate((adc, zeros)), rangeWin) * TmVector[k]
                cube[c, k, :] = adcWin
        

        fft1 = np.fft.fft(cube, axis=2)/FFT1_SCALE
        fft1 = fft1[:, :, 0:487]
        cWin = np.array(CHEBWIN)
        cWin = cWin / np.mean(cWin)
        for i in range(32):
            fft1[i,:,:] = fft1[i,:,:] * cWin[i]
        fft2 = np.fft.fft(fft1, axis=0) / 32
        
        # a = fft2[0, :, :]
        # print(len(a), " x ", len(a[0]))
        # найдем максимум в 0-м доплере
        VIEW_ALL_CHANNEL = 0
        if VIEW_ALL_CHANNEL == 1:
            zeroDoppler = np.abs(fft2[0, :, :])
            # поиск максимального значения в нулевом доплере
            zeroDopplerMaxAz = np.max(zeroDoppler, axis=0)        
            # print("---------------------------------")
            zeroDopplerMaxAz[0:2] = 1E-12
            zeroDopplerMaxAz[20:] = 1E-12
            maxRIndex = np.argmax(zeroDopplerMaxAz)                    
        else:
            zeroDoppler = np.abs(fft2[0, 0, :])
            # диапазон дальности уголка: 2..19
            zeroDoppler[0:2] = 1E-12
            zeroDoppler[20:] = 1E-12
            maxRIndex = np.argmax(zeroDoppler)                    
        
        if maxRIndex > 10:
            algorithmPrint("Warning! maxRIndex : %d" % maxRIndex)
    
        phase_distribution = fft2[0, :, maxRIndex]
        
        phase = np.mean(np.angle(phase_distribution))
        
        rotateValue = np.exp(complex(0, -1*phase))
        phase_distribution = phase_distribution * rotateValue;
        phase_dist = phase_dist + phase_distribution
        number_pd = number_pd + 1
        algorithmPrint("[CALC] N:%d, maxRIndex:%d, phase: %.3f" % (number_pd, maxRIndex, phase))


    return "OK"

# режим автономной отладки модуля через algRunner.py 
if __name__ == '__main__':
    DIRECTORY_DEBUG = "." + DIRECTORY_DEBUG
    DIRECTORY_GUI_PNG = "." + DIRECTORY_GUI_PNG
    algorithmRFCalib("START", debug=1)
    workDirectory = "./workdir"
    algorithmRFCalib("CONFIG", debug=1)
    
    # algorithmRFCalib("CALC", debug=1)
    algorithmRFCalib("CALC", debug=1)
    algorithmRFCalib("DONE", debug=1)
# ALG_DEBUG           = 1
#
