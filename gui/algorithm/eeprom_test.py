# -*- coding: utf-8 -*-
"""
Created on  02.06.2022

@author: Rodionov GV

Тест для новых радаров. Записывает данные в EEPROM. Перезапускает радар и считывает повторно.
Сравнивает с записанными данными.

"""
import sys
import os

scriptName = os.path.basename(__file__)
curScriptPath = os.path.abspath(__file__).replace(scriptName, '')

sys.path.insert(0, os.path.join(curScriptPath, 'eeprom_test_files'))

file_path = "logging_config.ini"
file_path = os.path.join(os.getcwd(), 'eeprom_test_files')

from test_EEPROM import eeprom_test

# Функция алгоритма.
def algorithmCrrEepromTest(param, debug=0):
    if (param == "START"):
        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/"
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if (param == "DONE"):
        pass
        
    if (param == "CALC"):
        return eeprom_test()
        print("----------- EEPROM_TEST algRunner.py -------------")

    return "OK"

# режим автономной отладки модуля через algRunner.py 
if __name__ == '__main__':
    algorithmCrrEepromTest("START", debug=1)
    algorithmCrrEepromTest("CALC", debug=1)
    algorithmCrrEepromTest("DONE", debug=1)
# ALG_DEBUG           = 1
#
