# -*- coding: utf-8 -*-
"""
Created on 27.05.2022

@author: Dunaev


Алгоритм "Тест Corner77":

 
    
Взаимодействие с системой испытаний:
    Параметры запуска:
        "START" : перед началом расчета. 
        
        Возврат: Путь к рабочей директории
        
        "CALC" : Очередной кадр снят радаром и размещен в файл данных (mmap).
        Возврат: ["OK" | "ERROR"] - статус (пока игнорируется)

        "DONE" : Все кадры обработаны, нужно взять "суммарный" FFT и сравнить с порогом.
        Возврат: ["OK" | "ERROR"] - статус. "ERROR" - "суммарный" FFT выше порога -> НЕНОРМА


"""
from time import sleep

import os
import numpy as np
import matplotlib.pyplot as plt
import mmap
import math
import array
import struct
from shutil import copyfile
import shelve


# Параметры формата данных (как в алгоритме матлаба)
CHIRP_NUM           = 128
CHIRP_SAMPLE_NUM    = 256
RADAR_RX_CHANNEL    = 4
RADAR_TX_CHANNEL    = 3
# первые чирпы содержат перех.процесс. они отбрасываются
FFT1_SIZE           = 256
FFT1_SCALE          = 1 #(1/FFT1_SIZE)
WIN_KOEF            = (CHIRP_SAMPLE_NUM * 32 * 32768 * 0.5)

M_PI                = 3.14159265358979

# параметр DPI для figure. Значение больше: элементы крупнее.
dpi = 109

# размер графиков в пикселях (делаю все графики одинаковые по размеру)
PNG_SIZE_X          = 1200
PNG_SIZE_Y          = 650

# путь к GUI (Сводный график используется для GUI)
DIRECTORY_GUI_PNG = "./design"
# относительный путь к рабочей директории
DIRECTORY_WORK = "./algorithm/workdir"
# относительный путь к директории с отладочн. массивами
DIRECTORY_DEBUG = "./algorithm/dbg"
# относительный путь к директории параметров
DIRECTORY_CONFIG = "./algorithm/config"
# формат сетки графика
Y_SCALE = "linear"  # ["log" | "linear"]
# для простоты задал константой


def algorithmPrint(text, error=0):
    print("algorithm[АЧХ]>", text)
    pass


def mmap_io(filename):
    with open(filename, mode="rb") as file_obj:
        with mmap.mmap(file_obj.fileno(), length=0, access=mmap.ACCESS_READ) as mmap_obj:
            buff = mmap_obj.read()
            return buff


# Для автономной отладки алгоритма (без радара/генератора)
# Используя радар и генератор, сохраняю ("save") данные измерений,
# Не имея радара эти измерения можно использовать для отладки ("load")
# action: ["save" | "load" | "anytext" - для возврата переменной без изменения
def dbgSerialize(filename, action, var):
    result = var
    if action == "save":
        # Сохраню накопл. данные для послед. анализа
        s = shelve.open(filename)
        # Кол-во точек измерения
        s['N'] = len(var)
        for i, r in enumerate(var):
            s[str(i)] = r
        s.close()
    if action == "load":
        s = shelve.open(filename)
        n = s['N']
        result = [[] for _ in range(n)]
        for i in range(n):
            result[i] = s[str(i)]
        s.close()
    return result



# сохранение изображения графика в файл
def exportFigure(plt, filename):
    try:
        plt.savefig(filename)
        algorithmPrint(filename + "...OK")
    except PermissionError:
        algorithmPrint('save fig Error...', error=1)
    pass


# формируем метки на оси X
def getXticks(N, step=1, first=0):
    return [x*step + first for x in range(N)]


# Общее оформление графиков
def getGraph(title):
    fig = plt.figure(dpi=dpi, figsize=(PNG_SIZE_X / dpi, PNG_SIZE_Y / dpi))
    ax = fig.add_subplot(111)  # 111 - это сетка. 111 означает сетка 1x1
    ax.set_yscale(Y_SCALE)
    ax.set_title((title))
    ax.grid()
    return fig, ax

# - изменяет кол-во точек! не так как в матлабе!
#def movmean(data_set, periods=3):
#    weights = np.ones(periods) / periods
#    return np.convolve(data_set, weights, mode='valid')


def addThresholdPlot(ax, x, level, err=0):
    # Порог
    if (len(level) == 512):
        color='green'
        if err == 1:
            color='red'
        ax.plot(x, level, linestyle='--', color=color, linewidth=3, label='Порог')
    pass


# Функция алгоритма.
def algorithmCPYCTest(param, debug=0):
    # окно для FFT
    global rangeWin
    # Максимальные значения FFT(вдоль доплера) для каждого кадра
    global resultTestChirp
    global RADAR_RX_CHANNEL
    global workDirectory
    global HiLimitlevel
    
    # результат проверки FFT с массивом порога. (Превышен/нет)
    # Если порог не задан, считаем, что ошибки нет
    HiLimitDetect = 0
    DATASIZE = 2*2*CHIRP_SAMPLE_NUM*RADAR_RX_CHANNEL*CHIRP_NUM
   

    if (param == "START"):
        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/" + DIRECTORY_WORK
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        print("workDirectory->>>>>>", workDirectory)
        return workDirectory

    # Шаг получения кадра данных (может быть несколько)
    if (param == "CALC"):
        algorithmPrint("CALC")
        if debug == 1:
            # загружаю данные из файла
            fileName = ("%s/cpyc_test.bin" % DIRECTORY_DEBUG)
        else:
            fileName = ("%s/cpyc_test.bin" % workDirectory)

        # Получаем данные от системы испытаний.
        # (полный кадр стрима, включая заголовок)
        data = mmap_io(fileName)

        # Если данных меньше чем заголовок - ошибка!
        if (len(data) < DATASIZE):
            # Ошибка формата данных.
            algorithmPrint("MMAP ERROR!!! Data size:%d" % (len(data)), error=1)
            return "ABORT"

        # Парсим заголовок кадра. В нем содержится информация
        # о кол-ве точек на чирп (возможно 720 и 378)
        T = struct.unpack_from( "<I", data, 0)
        print("\n STX : 0x%X \n" % T)
        
        adc_raw_array = array.array('h', data)

        # проверка на корректность размера данных
        if (len(adc_raw_array) != CHIRP_SAMPLE_NUM*RADAR_RX_CHANNEL*CHIRP_NUM*2):
            algorithmPrint("DATA FORMAT ERROR!!!", error=1)
            return 0


        # разбивка массива int16*2 на список чирпов
        adc = np.array(adc_raw_array).reshape(CHIRP_NUM*RADAR_RX_CHANNEL*CHIRP_SAMPLE_NUM,2)
        # перевод к комплексному виду
        adc = adc[...,0] + 1j*adc[...,1];
        # разбивка на чирпы (adc[128][4][256])
        chirpArray = np.array(adc).reshape(CHIRP_NUM, RADAR_RX_CHANNEL, CHIRP_SAMPLE_NUM)
        chirp0 = np.abs(chirpArray[0, 0, :])
        #
        resultTestChirp = chirp0
        # print(chirp0)
        x = getXticks(len(chirp0))
        fig, ax = getGraph("Тест-Чирп")
        ax.plot(x, chirp0)  # график спектра
        x1, x2, y1, y2 = ax.axis()
        ax.axis((x1, x2, 0, 200))        
        plt.show()

        # sleep(1)
        
        return "OK"
        
        
    if (param == "DONE"):
        algorithmPrint("DONE")
        return "OK"
