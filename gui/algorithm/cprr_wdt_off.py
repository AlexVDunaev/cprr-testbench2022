# -*- coding: utf-8 -*-
"""
Created on  02.06.2022

@author: Rodionov GV

Фукция отключения сторожевого таймера по Telnet.

"""
import sys
import os

scriptName = os.path.basename(__file__)
curScriptPath = os.path.abspath(__file__).replace(scriptName, '')

sys.path.insert(0, os.path.join(curScriptPath, 'eeprom_test_files'))

file_path = "logging_config.ini"
file_path = os.path.join(os.getcwd(), 'eeprom_test_files')

from test_EEPROM import cprr_off_wdt

# Функция алгоритма.
def algorithmCprrOffWdt(param, debug=0):
    if (param == "START"):
        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/"
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
        return workDirectory

    if (param == "DONE"):
        pass
        
    if (param == "CALC"):
        print("----------- OFF Watch dog timer ------------- {0}".format('-'))
        result =  cprr_off_wdt()
        
        print("----------- EEPROM_WRITE_CALIB algRunner.py ------------- {0}".format(result))
        return result
        
    return "OK"

# режим автономной отладки модуля через algRunner.py 
if __name__ == '__main__':
    algorithmCprrOffWdt("START", debug=1)
    result = algorithmCprrOffWdt("CALC", debug=1)
    print('main output: ' + result)
    algorithmCprrOffWdt("DONE", debug=1)
# ALG_DEBUG           = 1
#
