# -*- coding: utf-8 -*-

import sys
import struct
import array
import socket
from time import sleep
from threading import Thread

# -------------------------------
# содержимое заголовка:
# -------------------------------
STX = 0x0000ABCD
# -------------------------------


class dspRadar(object):
    """docstring"""

    def __init__(self, ip, portTx, portRx, cbData, cbError):
        """Constructor"""
        self.ip = ip
        self.port = portTx
        self.cbData = cbData
        self.cbError = cbError
        self.needExit = 0
        self.threadWork = 0

        sockcmd = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sockcmd.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        # Для работы на localhost сделал разные порты (на отправку и получения)
        sockcmd.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sockcmd.bind(("", portRx))
        
        self.sockcmd = sockcmd
        # TODO проверка на корректность sockcmd
        print('UDP OPEN (Target port : %d, Local port : %d, IP : %s)\n' % (portTx, portRx, ip))
        self.cmdRxThreadHandle = Thread(target=self.runListenThread, args=(sockcmd, ))

    def onError(self, errCode, errText):
        if self.cbError:
            self.cbError(errCode, errText)
        pass

    def send(self, pktid, data):
        header = bytes(array.array('i', [STX, len(data), pktid]))
        pkt = header + data
        self.sockcmd.sendto(pkt, (self.ip, self.port))
        pass

    def sendraw(self, data):
        print('>send(len:%d)\n' % len(data))
        pass

    def start(self):
        if self.cbData:
            self.cbData(1, 0, "")
        self.cmdRxThreadHandle.start()
        return "start"

    def stop(self):
        self.needExit = 1
        while self.threadWork > 0:
            sleep(0.5)
        self.sockcmd.close()
        print('[CMD]. закроем порт сервера.\n')
        #return "stop"

    # -------------------------------------------------------------------
    # --- поток приема команд -------------------------------------------
    # -------------------------------------------------------------------
    def runListenThread(self, sockcmd):
        global needExit
        global radarStarted
        sockcmd.settimeout(1.5)
        self.threadWork = 1
        # Ожидание данных ...
        while self.needExit == 0:
            try:
                sockcmd.settimeout(5)
                data, addr = sockcmd.recvfrom(1536)
                packet_format_ok = 0
                if len(data) >= 12:
                    STX, SIZE, TYPE = struct.unpack_from("<III", data, 0)
#                    print('[STX:%d, SIZE:%d, TYPE:%d]\n' % (STX, SIZE, TYPE))
                    if (STX == 0x0000ABCD):
                        if (SIZE < 1400):
                            if (TYPE <= 6):
                                packet_format_ok = 1
                            else:
                                print('[PACKET TYPE Error!]\n')
                        else:
                            print('[PACKET SIZE Error! (SIZE:%d)]\n' % SIZE)
                    else:
                        print('[PACKET Preamble Error! (Preamble:0x%.8x)]\n' % STX)
                if packet_format_ok == 1:
                    if self.cbData:
                        self.cbData(TYPE, SIZE, data[12:])
                else:
                    self.onError(0, "[PACKET Format Error!]")

            except socket.timeout:
                self.onError(1, '[таймаут!!!]')
                pass
            except socket.error:
                print('[error!!!!]\n')
                self.onError(2, '[sock error!!!]')
                self.needExit = 0
                pass
            except Exception as e:
                print(str(e))
                self.needExit = 1
                print('CMD Sock Error!\n')
                self.onError(-1, '[Sock Error!!!]')
                pass
        self.threadWork = 0
        pass
# ----------------------------------------------------------------------
