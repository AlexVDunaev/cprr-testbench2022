"""
Created on Thu May 31 2022

@author: Родионов Г.В.

Тестирование EEPROM
"""
import logging
from logging.config import fileConfig

import telnetlib
from time import sleep
import re
import os

from utils import escape_ansi

from cprrControl import CPRRCTRL

from utils import getLogSettingsFileName # возвращает путь к файлу настроек лога
from utils import getCurrentModulePath, getCprrDataPath
from utils import RadarLoadData

# Autogenerated with DRAKON Editor 1.31

def CPRR_EEPROM_clear(cprr, logger, ipCPRR="192.168.1.4", port=23, timeout=100):
    #item 218
    logger.info("-------------> START ERASE EEPROM <-------------")
    resultTest = []
    while True:
        #item 221
        if cprr.connect():
            break
        else:
            pass
        #item 219
        logger.info("Not connected")
    #item 240
    cprr.getFrames(2)
    #item 242
    dataCPRR = cprr.readCalibVectorEEPROM()
    #item 249
    if len(dataCPRR) == 0:
        #item 252
        logger.info("EEPROM ALREADY EMPTY")
        resultTest.append("EEPROM ALREADY EMPTY")
    else:
        #item 239
        # ON - стереть EEPROM - для теста
        # OFF - не стирать
        cprr.clearEEPROM("ON")
        #item 225
        cprr.restart("ON")
        while True:
            #item 227
            if cprr.connect():
                break
            else:
                pass
            #item 226
            logger.info("Not connected")
        #item 244
        dataCPRR = cprr.readCalibVectorEEPROM()
        #item 230
        if len(dataCPRR) == 0:
            #item 247
            resultTest.append("EEPROM SUCCEEDED")
        else:
            #item 233
            resultTest.append("EEPROM CLEAR FAILED")
            logger.error("EEPROM CLEAR FAILED")
    #item 248
    logger.info("-------------> END ERASE EEPROM <------------- {0}".format(resultTest[0]))
    #item 246
    return resultTest[0]


def CPRR_EEPROM_read(cprr, logger, ipCPRR="192.168.1.4", port=23, timeout=100):
    #item 259
    dataCPRR = cprr.readCalibVectorEEPROM()
    
    logger.info(dataCPRR)
    #item 260
    if len(dataCPRR) == 0:
        #item 263
        logger.info('EEPROM EMPTY')
    else:
        #item 264
        logger.info('WRITTEN')
    #item 265
    return dataCPRR


def CPRR_EEPROM_test(cprr, logger, ipCPRR="192.168.1.4", port=23, timeout=100, testFileData='Telnet_EEPROM_test.sh'):
    #item 6
    logger.info("-------------> START test <-------------")
    resultTest = []
    while True:
        #item 10
        if cprr.connect():
            break
        else:
            pass
        #item 7
        logger.info("Not connected")
    #item 176
    cprr.getFrames(2)
    #item 180
    dataCPRR = cprr.readCalibVectorEEPROM()
    #item 109
    if cprr.checkDataEEPROM("CLEAR_EEPROM_OFF", dataCPRR, []) == 'EEPROM_RECORDED':
        #item 9
        resultTest.append("EEPROM_RECORDED")
        resultTest.append("EEPROM ALREADY OK")
        
        logger.info("EEPROM ALREADY OK")
    else:
        #item 112
        dataFile = cprr.loadTestData(testFileData)
        #item 199
        cprr.writeCalibVectorEEPROM("ON", dataFile)
        #item 15
        cprr.saveData("ON")
        #item 16
        cprr.restart("ON")
        while True:
            #item 18
            if cprr.connect():
                break
            else:
                pass
            #item 17
            logger.info("Not connected")
        #item 253
        cprr.getFrames(2)
        #item 201
        dataCPRR = cprr.readCalibVectorEEPROM()
        #item 202
        resultTest = cprr.checkDataEEPROM("NONE", dataCPRR, dataFile)
        #item 23
        if resultTest[0] == 'EEPROM_RECORDED':
            pass
        else:
            #item 26
            logger.error("EEPROM TEST FAILED")
    #item 28
    logger.info("-------------> END test <------------- {0}".format(resultTest[1]))
    #item 203
    return resultTest[1]


def CPRR_EEPROM_write(cprr, logger, ipCPRR="192.168.1.4", port=23, timeout=100, testFileData='Telnet_EEPROM_test.sh'):
    #item 271
    logger.info("-------------> START test <-------------")
    resultTest = []
    while True:
        #item 275
        if cprr.connect():
            break
        else:
            pass
        #item 272
        logger.info("Not connected")
    #item 293
    cprr.getFrames(2)
    #item 295
    dataCPRR = cprr.readCalibVectorEEPROM()
    #item 289
    if cprr.checkDataEEPROM("CLEAR_EEPROM_OFF", dataCPRR, []) == 'EEPROM_RECORDED':
        #item 274
        resultTest.append("EEPROM_RECORDED")
        resultTest.append("ERASE EEPROM?")
        
        logger.info("EEPROM ALREADY WRITED")
    else:
        #item 292
        # PATH = getCurrentModulePath() +
        PATH = getCprrDataPath()
        logger.info(PATH)
        radarObj = RadarLoadData(PATH, cprr.SN)
        dataFile = cprr.loadTestData(radarObj.calibVector)
        #item 296
        cprr.writeCalibVectorEEPROM("ON", dataFile)
        #item 278
        cprr.saveData("ON")
        #item 279
        cprr.restart("ON")
        while True:
            #item 281
            if cprr.connect():
                break
            else:
                pass
            #item 280
            logger.info("Not connected")
        #item 300
        cprr.getFrames(2)
        #item 297
        dataCPRR = cprr.readCalibVectorEEPROM()
        #item 298
        resultTest = cprr.checkDataEEPROM("NONE", dataCPRR, dataFile)
        #item 284
        if resultTest[0] == 'EEPROM_RECORDED':
            pass
        else:
            #item 287
            logger.error("EEPROM TEST FAILED")
    #item 288
    logger.info("-------------> END test <------------- {0}".format(resultTest[1]))
    #item 299
    return resultTest[1]


def CPRR_GET_SN(cprr):
    #item 362
    dataCPRR = cprr.connect()
    #item 363
    return dataCPRR


def CPRR_IPPORT_SET(cprr, logger, ipCPRR_new="192.168.1.4", portCPRR_new=7000, mode="OFF"):
    #item 352
    dataCPRR = cprr.ipPortChange(ipCPRR_new, portCPRR_new, mode)
    #item 353
    return dataCPRR


def CPRR_TEMPCODE_ADF(cprr, logger, ipCPRR="192.168.1.4", port=23, timeout=100):
    #item 356
    cprr.connect()
    #item 321
    dataCPRR = cprr.readTempCodeADF1()
    
    logger.info(dataCPRR)
    #item 322
    if dataCPRR == 0:
        #item 325
        logger.error('Temperature code invalid!')
    else:
        #item 326
        logger.info('Temperature code OK')
    #item 327
    return str(dataCPRR)


def CPRR_WDT_OFF(cprr, logger, ipCPRR="192.168.1.4", port=23, timeout=100):
    #item 340
    dataCPRR = cprr.off_wdt()
    #item 346
    return "wdt off"

class CPRR:


    def __init__(self, telnet, ipCPRR, portTelnet, timeout, portCPRR, logger, logRaw):
        #item 96
        self.telnet = telnet
        self.portTelnet = portTelnet
        self.ipCPRR = ipCPRR
        self.timeout = timeout
        self.portCPRR = portCPRR
        self.log = logger
        self.logRaw = logRaw
        self.calib_vectors = [] # загружаем из файла
        self.SN = '' # серийный номер, получаем в connect


    def checkDataEEPROM(self, mode, dataFromCPRR, dataFromFile):
        #item 305
        """
        Сравнение данных загруженных с CPRR и из файла
        
        Args:
            dataFromCPRR: данные вычитанные из CPRR
            dataFromFile: данные загруженные из файла   
        
        Kwargs:
        
        Returns:
            строка "EEPROM_RECORDED" или "EEPROM_FALUE"
        
        """
        #item 187
        pattern = r'calibration selvector (?<=\D)\d{3}(?=\D)' # две рядом стоящие цифры
        if len(dataFromFile) != 0:
            dataMust = re.findall(pattern, str(dataFromFile))
            dataEst  = re.findall(pattern, str(dataFromCPRR))
            self.log.info("Data MUST:%s" % dataMust)
            self.log.info("Data EST:%s" % dataEst)
            self.log.info("set data and get data equal?: {0}".format(dataEst == dataMust))
            result = []
            if dataEst == dataMust:
              result.append("EEPROM_RECORDED")
              result.append("EEPROM TEST OK")
            else:
              result.append("EEPROM_FALUE")
              result.append("EEPROM TEST FAILED")
        
            self.log.info(result[1])	
            return result
        else:
            if len(dataFromCPRR) != 0 or mode == "CLEAR_EEPROM":
              self.log.info("This is dataFromCPRR length: {0}".format(len(dataFromCPRR)))
              return 'EEPROM_RECORDED'


    def clearEEPROM(self, mode="OFF"):
        #item 306
        """
        Очистка EEPROM
        
        Args:
        
        Kwargs:
            mode: "ON" очистка EEPROM включена
        
        Returns:
            None
        
        """
        #item 150
        if mode == "ON":
            #item 149
            self.log.warning("--------- START clearEEPROM ---------")
            
            all_result = []
            '''
            Вычитывание данных по команде show
            '''
            with self.telnet(self.ipCPRR, self.portTelnet, self.timeout) as session:
                session.write(b"show\n")
                sleep(0.1)
                all_result = session.read_very_eager().decode('utf-8')
                session.write(b"exit\n")
            #item 153
            '''
            Поиск всех доступных векторов из вычитанных данных
            '''
            pattern = r'calibration selvector (?<=\D)\d{3}(?=\D)' # две рядом стоящие цифры
            all_inclusions = re.findall(pattern, all_result)
            del_vectors = []
            for index, item in enumerate(all_inclusions):
                del_vectors.append(item.replace("selvector", "delvector"))
            
            if (del_vectors == []):
                self.log.info('NO VECTORS!')
            else:
                self.log.info("Delete this vectors:")
                self.log.info(del_vectors)
            #item 148
            '''
            Удаление всех векторов, сохранение в EEPROM
            '''            
            with self.telnet(self.ipCPRR, self.portTelnet, self.timeout) as session:
                #session.write(b"radarRestart\n")
                for item in del_vectors:
                    item = item +'\n'
                    item = bytes(item.encode("utf-8"))
                    session.write(item)
                    sleep(0.1)
                if mode == "ON":
                    session.write(b"save\n")
                    sleep(5)
                    all_result = session.read_very_eager().decode('utf-8')
                    session.write(b"radarRestart\n")
            
            self.log.info("--------- END clearEEPROM ---------")
        else:
            pass


    def connect(self):
        #item 307
        """
        Подключение к CPRR по Telnet и вычитывание серийного номера
        
        Args:
        
        Kwargs:
            mode: "ON" очистка EEPROM включена
        
        Returns:
            строка серийный номер
        
        """
        #item 102
        all_result = []
        with self.telnet(self.ipCPRR, self.portTelnet, self.timeout) as session:
            session.write(b"version\n")
            sleep(0.1)
            all_result = session.read_very_eager().decode('utf-8')
        #item 103
        sleep(0.5)    
        pattern = r'(?<=\D)\d{9}(?=\D)'
        all_inclusions = re.findall(pattern, all_result)
        try:
            SN = all_inclusions[0]
        except:
            SN = 0
            #print(all_result)
            if 'Console is busy' in all_result:
                self.log.error("Console is busy, restart CPRR")
        
        
        if (SN == 0):
            self.log.info("Radar NOT CONNECTED")
            #raise ValueError
        else:
            self.log.info("Radar SN: {0} is CONNECTED".format(SN))
        
        self.SN = str(SN)
        return self.SN


    def getFrames(self, N):
        #item 308
        """
        Подключение к CPRR по UDP и получение N кадров.
        
        Args:
            N: число кадров для получения.
        
        Kwargs:
        
        
        Returns:
            None
        
        """
        #item 175
        radar = CPRRCTRL(self.ipCPRR, self.portCPRR, self.log)
        
        radar.doFrames(N)


    def ipPortChange(self, ipCPRR_new="192.168.1.4", portCPRR_new=7000, mode="OFF"):
        #item 169
        '''
        Функция изменения в EEPROM ip и port радара.
        
        Args:
            ip: строка ip адреса в формате '192.168.1.4'
            port: порт, целое число
        
        Kwargs:
            mode: флаг включения записи данных
        
        Returns:
            Возвращает сообщение для лога.
        
        '''
        #item 168
        # НУЖНО ТЕСТИРОВАТЬ
        if type(ipCPRR_new) != str:
            return "Error ip not a str"
        
        if type(portCPRR_new) != int:
            return "Error port not a int"
        #item 164
        with self.telnet(self.ipCPRR, self.portTelnet, self.timeout) as session:
            self.log.info("Send command to save")
            command = '\nconfig ip %s\n'% ipCPRR_new
            byte_command = bytes(command.encode('utf-8'))
            session.write(byte_command)
            sleep(0.1)
        
            command = 'config port %d\n'% portCPRR_new
            byte_command = bytes(command.encode('utf-8'))
            session.write(byte_command)
            sleep(0.1)
            all_result = session.read_very_eager().decode("utf-8")
        
        self.log.info("Output telnet console:")
        self.logRaw.info(escape_ansi(all_result))
        #item 165
        if mode == "ON":
            #item 354
            self.saveData(mode)
            return "Write ip:%s and port:%d OK" % (ipCPRR_new, portCPRR_new)
        else:
            #item 355
            
            return "Data send ip:%s and port:%d\nBUT NOT WRITE!!!\n" % (ipCPRR_new, portCPRR_new)


    def loadTestData(self, calibFile):
        #item 304
        '''
        Функция загрузки из файла вектора калибровки
        
        Args:
            calibFile: путь с именем файла.
        Kwargs:
        
        
        Returns:
            Возвращает список с элементами для загрузки по Telnet.
        
        '''
        #item 120
        # Считываем вектор калибровки из файла
        
        with open(calibFile, "r") as w:
            for line in w:
                line = line.rstrip()
                self.calib_vectors.append(line)
        
        return self.calib_vectors


    def off_wdt(self):
        #item 334
        '''
        Функция отключения сторожевого таймера по Telnet.
        
        Args:
        
        Kwargs:
        
        
        Returns:
            
        
        '''
        #item 333
        all_result = []
        all_inclusions = []
        
        with self.telnet(self.ipCPRR, self.portTelnet, self.timeout) as session:
            session.write(b"wdTimer_offon 0\n")
            sleep(0.5)
            all_result = session.read_very_eager().decode('utf-8')
        
        self.log.info("Отключение сторожевого таймера:")
        self.log.info(escape_ansi(all_result))


    def readCalibVectorEEPROM(self):
        #item 301
        '''
        Функция вычитывания калибровочного вектора из радара по Telnet.
        
        Args:
        
        Kwargs:
        
        
        Returns:
            Возвращает список с заголовками вычитанными калибровочными векторами.
        
        '''
        #item 118
        all_result = []
        with self.telnet(self.ipCPRR, self.portTelnet, self.timeout) as session:
            session.write(b"show\n")
            sleep(0.1)
            all_result = session.read_very_eager().decode('utf-8')
        
        pattern = r'calibration selvector (?<=\D)\d{3}(?=\D)' # две рядом стоящие цифры
        all_inclusions = re.findall(pattern, all_result)
        return all_inclusions


    def readTempCodeADF1(self):
        #item 315
        '''
        Функция вычитывания температурного кода ADF1 радара по Telnet.
        
        Args:
        
        Kwargs:
        
        
        Returns:
            Возвращает температурный код.
        
        '''
        #item 314
        all_result = []
        all_inclusions = []
        
        with self.telnet(self.ipCPRR, self.portTelnet, self.timeout) as session:
            session.write(b"sysinfo\n")
            sleep(0.05)
        
            all_result = session.read_very_eager().decode('utf-8')
        #print(escape_ansi(all_result))
        
        pattern = r'ADF5904_1: (?<=\D)\d{3}(?=\D)' # три рядом стоящие цифры
        all_inclusions = re.findall(pattern, all_result)
        
        #all_inclusions.append(r'ADF5904_1: 0')
        
        pattern = r'(?<=\D)\d{3}(?=\D)' # три рядом стоящие цифры
        all_inclusions = re.findall(pattern, str(all_inclusions))
        if len(all_inclusions) == 1:
            return int(all_inclusions[0])
        else:
            return 0
            print('Это результат чтения температурного кода:')
            print(escape_ansi(all_inclusions))


    def restart(self, mode="ON"):
        #item 303
        """
        Отправить команду перезапуска радара по Telnet
        
        Args:
        
        
        Kwargs:
           mode: "ON" перезапуск включен
        
        Returns:
            None
        
        """
        #item 134
        if mode == "ON":
            #item 133
            self.log.info("Commant Restart!")
            with self.telnet(self.ipCPRR, self.portTelnet, self.timeout) as session:
                session.write(b"radarRestart\n")
            sleep(2.0)
        else:
            #item 137
            self.log.warning("Radar restart must be ON")


    def saveData(self, mode="ON"):
        #item 302
        """
        Отправить команду сохранения в EEPROM по Telnet
        
        Args:
        
        
        Kwargs:
           mode: "ON" запись включена
        
        Returns:
            None
        
        """
        #item 138
        if mode == "ON":
            #item 127
            with self.telnet(self.ipCPRR, self.portTelnet, self.timeout) as session:
                self.log.info("Send command to save")
                session.write(b"save\n")
                sleep(6.0)
                all_result = session.read_very_eager().decode("utf-8")
            
            self.log.info("Output telnet console:")
            self.logRaw.info(escape_ansi(all_result))
        else:
            #item 141
            self.log.warning("SAVE OFF!")
        #item 177
        # END class CPRR


    def writeCalibVectorEEPROM(self, mode, calib_vectors):
        #item 198
        """
        Отправить вектор калибровки в CPRR по Telnet
        
        Args:
           calib_vectors: список значений вектора калибровки
        
        Kwargs:
           mode: "ON" запись включена
        
        Returns:
            None
        
        """
        #item 195
        if mode == "ON":
            #item 194
            self.log.info("Start send calib vector by Telnet.")
            # Отправляем вектор калибровки по telnet
            cnt_vect = 0;
            while (cnt_vect < len(calib_vectors)):
                with self.telnet(self.ipCPRR, self.portTelnet, self.timeout) as session:
                    l_cnt = 0
                    while (l_cnt != 33):
                        item = calib_vectors[cnt_vect]
                        self.log.info(item + ': ' + str(cnt_vect))
                        item = item +'\n'
                        item = bytes(item.encode("utf-8"))
                        session.write(item)
                        sleep(0.01)
                        cnt_vect = cnt_vect + 1
                        l_cnt = l_cnt + 1
        else:
            #item 200
            self.log.warning("Write vector into CPRR is OFF")

    
if __name__ == "__main__":
    pass
#    main_test()
#    write_eeprom()
