from time import sleep
import os
import re
import json
import algRunner


def escape_ansi(line):
    '''
    Удаляет цвет из вывода терминала
    
    Args:
        arg1: Текст из которого нужно удалить коды цвета
    '''
    ansi_escape = re.compile(r'(?:\x1B[@-_]|[\x80-\x9F])[0-?]*[ -/]*[@-~]')
    return ansi_escape.sub('', line).replace("\r","")

def getCurrentModulePath():
    '''
    Возвращает путь к текущему исполняемому модулю
    '''
    scriptName = os.path.basename(__file__)
    curScriptPath = os.path.abspath(__file__).replace(scriptName, '')
    return curScriptPath
    
def getLogSettingsFileName():
    '''
    Возвращает путь к файлу настроек журнала logger
    '''
    fileP = 'logging_config.ini'
    if (os.path.exists(fileP)):
        fileP = 'logging_config.ini'
    else:
        #print("AHTUNG", os.getcwd())
        #print("AHTUNG", os.path.abspath(__file__))
        scriptName = os.path.basename(__file__)
        curScriptPath = os.path.abspath(__file__).replace(scriptName, '')
        #print("AHTUNG", curScriptPath)
        fileP = os.path.join(curScriptPath, 'logging_config.ini')
        #print(os.path.exists(fileP))
        #print(fileP)
    return fileP

def getCprrSettings(path):
    '''
    Возвращает словать с ip, port
    Если нет файла, возвращает настройки по умолчанию
    '''
    content = ''
    settings = {}
    file = path + "cprr_settings.json"
    try:
        with open(file, 'r', encoding='utf-8') as f:
            for line in f:
                line = line.rstrip()
                content+=line
        settings = json.loads(str(content))
        settings['log'] = "Load ip, port OK" + str(settings)
    except:
        settings = {'ip': '192.168.1.4', 'port': 7000}
        settings['log'] = "ATTENTION: default ip, port:" + str(settings)
    
    return settings


def getCprrDataPath():
    '''
    Возвращает путь к папке с файлами калибровки
    '''
    # return '2022_07'
    currentDir = algRunner.runAlgorithm("CALIBDIR", "")
    print(">>>>>>>>>> ", currentDir, " <<<<<<<<<<<<<")
    return currentDir

def getSNfromTelnet(telnetObj, host = "192.168.1.4", port=23, timeout=100):
    """
    Функция получения серейного номера радара по Telnet
    
    Args:
        arg1: telnetlib (import telnetlib)
        arg2: host - IP адрес радара
        arg3: port - порт подключения телнет
        arg4: timeout - таймаут подклчения
    
    Returns:
        Строку серийный номер
    
    """
    with telnetObj.Telnet(host, port, timeout) as session:
        #session.write(b"radarRestart\n")
        session.write(b"version\n")
        sleep(0.2)
        all_result = session.read_very_eager().decode('utf-8')
        #data = session.read_until(b'>')
        # socket = session.get_socket()
        # session.write(b"exit\n")
    sleep(1)    
    pattern = r'(?<=\D)\d{9}(?=\D)'
    all_inclusions = re.findall(pattern, all_result)
    try:
        SN = all_inclusions[0]
    except:
        SN = 0
        print(all_result)
        if 'Console is busy' in all_result:
            
            print("Console is busy!")
            
    if (SN == 0):
        raise ValueError
    return str(SN)

def getSysinfoFromTelnet(telnetObj, host = "192.168.1.4", port=23, timeout=100, rst=False):
    """
    Функция получения информации по Telnet
    
    Args:
        arg1: telnetlib (import telnetlib)
        arg2: host - IP адрес радара
        arg3: port - порт подключения телнет
        arg4: timeout - таймаут подклчения
    
    Returns:
        Строку серийный номер

    """
    if rst:
        with telnetObj.Telnet(host, port, timeout) as session:
           session.write(b"radarRestart\n")
        sleep(2)
    
    with telnetObj.Telnet(host, port, timeout) as session:
        session.write(b"sysinfo\n")
        sleep(0.1)
        all_result = session.read_very_eager().decode('utf-8')
        #data = session.read_until(b'>')
        session.write(b"exit\n")
        
    return all_result

class RadarLoadData:
    """
    Функция возвращает объект с полями:
    leak - имя файла пролаза
    calibVector - имя файла с вектором калибровок
    calibDefault - имя файла с калибровкой по умолчанию
    """
    leak = ''
    calibDefault = ''
    calibVector = ''
    def __init__(self, path, SN):    # special method __init__
        if not os.path.isdir(path):
            print("Папки {0} не найдено".format(path))
        # Получаем список калибровок
        try:
            listFile = os.listdir(path)
        except FileNotFoundError:
            print('Не найдены файлы в директории: {0}'.format(path))
            return "ERROR"


        for item in listFile:
            if (SN in item):
                #print("Yes it is in " + item)
                if "Leak" in item:
                    self.leak = os.path.join(path, item)
                if "Calibration_matrix" in item:
                    self.calibDefault = os.path.join(path, item)
                if "Telnet_radar" in item:
                    self.calibVector = os.path.join(path, item)
        if self.leak == '':
            raise ValueError
        if self.calibDefault == '':
            raise ValueError
        if self.calibVector == '':
            raise ValueError
    
    def show(self):
        return 'Leak: {0}\nCalibDef: {1}\nVector: {2}'.format(self.leak, self.calibDefault, self.calibVector)    