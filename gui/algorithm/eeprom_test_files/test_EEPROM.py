"""
Created on Thu May 31 2022

@author: Родионов Г.В.

Тестирование EEPROM
"""
import logging
from logging.config import fileConfig

import telnetlib
from time import sleep
import re
import os

from utils import escape_ansi

from cprrControl import CPRRCTRL

from utils import getLogSettingsFileName # возвращает путь к файлу настроек лога
from utils import getCurrentModulePath, getCprrDataPath, getCprrSettings
from utils import RadarLoadData

from EEPROM_drn_fxn import CPRR
from EEPROM_drn_fxn import CPRR_EEPROM_test, CPRR_EEPROM_clear, CPRR_EEPROM_read
from EEPROM_drn_fxn import CPRR_EEPROM_write, CPRR_TEMPCODE_ADF, CPRR_WDT_OFF, CPRR_IPPORT_SET
from EEPROM_drn_fxn import CPRR_GET_SN


# Настройки радара    
ipCPRR = "192.168.1.4"
portCPRR = 7000
portTelnet = 23
timeout = 100


def eeprom_test():
    # Получаем путь к файлу настроек лога
    filePathSettingLog = getLogSettingsFileName()
    path2Module = getCurrentModulePath()
    
    setCprr = getCprrSettings(path2Module)
    ipCPRR = setCprr['ip']
    portCPRR = setCprr['port']
    
    fileConfig(filePathSettingLog, defaults={'logfilename': 'EEPROM_test.log'}, disable_existing_loggers=False)
    logger = logging.getLogger('radarAlive')
    logRaw = logging.getLogger("rawOutput")

    # Создаём объект радара
    cprr = CPRR(telnetlib.Telnet, ipCPRR, portTelnet, timeout, portCPRR, logger, logRaw)
    
    # Выполняем тест EEPROM
    testFileData = 'Telnet_EEPROM_test.sh'
    print('---------------------->' + path2Module)
    result = CPRR_EEPROM_test(cprr, logger, ipCPRR, portTelnet, timeout, path2Module + testFileData)
    
    # this is end
    logging.shutdown()
    return result

def erase_eeprom():
    # Получаем путь к файлу настроек лога
    filePathSettingLog = getLogSettingsFileName()
    
    fileConfig(filePathSettingLog, defaults={'logfilename': 'EEPROM_test.log'}, disable_existing_loggers=False)
    logger = logging.getLogger('radarAlive')
    logRaw = logging.getLogger("rawOutput")
    
    path2Module = getCurrentModulePath()
    
    setCprr = getCprrSettings(path2Module)
    ipCPRR = setCprr['ip']
    portCPRR = setCprr['port']
    
    # Создаём объект радара
    cprr = CPRR(telnetlib.Telnet, ipCPRR, portTelnet, timeout, portCPRR, logger, logRaw)
    
    # Выполняем тест EEPROM
    result = CPRR_EEPROM_clear(cprr, logger, ipCPRR, portTelnet, timeout)
    
    # this is end
    logger.info(result)
    logging.shutdown()
    return result
    
def read_eeprom():
    # Получаем путь к файлу настроек лога
    filePathSettingLog = getLogSettingsFileName()
    
    fileConfig(filePathSettingLog, defaults={'logfilename': 'EEPROM_test.log'}, disable_existing_loggers=False)
    logger = logging.getLogger('radarAlive')
    logRaw = logging.getLogger("rawOutput")
    
    path2Module = getCurrentModulePath()
    
    setCprr = getCprrSettings(path2Module)
    ipCPRR = setCprr['ip']
    portCPRR = setCprr['port']
    
    # Создаём объект радара
    cprr = CPRR(telnetlib.Telnet, ipCPRR, portTelnet, timeout, portCPRR, logger, logRaw)
    
    # Выполняем чтение EEPROM
    result = CPRR_EEPROM_read(cprr, logger, ipCPRR, portTelnet, timeout)
    
    # выводим прочитанное в лог
    logger.info(result)
    logging.shutdown()
    return result    
    
def write_eeprom():
    # Получаем путь к файлу настроек лога
    filePathSettingLog = getLogSettingsFileName()
    
    fileConfig(filePathSettingLog, defaults={'logfilename': 'EEPROM_test.log'}, disable_existing_loggers=False)
    logger = logging.getLogger('radarAlive')
    logRaw = logging.getLogger("rawOutput")
    
    path2Module = getCurrentModulePath()
    
    setCprr = getCprrSettings(path2Module)
    ipCPRR = setCprr['ip']
    portCPRR = setCprr['port']
    
    # Создаём объект радара
    cprr = CPRR(telnetlib.Telnet, ipCPRR, portTelnet, timeout, portCPRR, logger, logRaw)
    
    # Выполняем чтение EEPROM
    result = CPRR_EEPROM_write(cprr, logger, ipCPRR, portTelnet, timeout)
    
    # выводим прочитанное в лог
    logger.info(result)
    logging.shutdown()
    return "DONE"    

def read_tempCode():
    # Получаем путь к файлу настроек лога
    filePathSettingLog = getLogSettingsFileName()
    
    fileConfig(filePathSettingLog, defaults={'logfilename': 'EEPROM_test.log'}, disable_existing_loggers=False)
    logger = logging.getLogger('radarAlive')
    logRaw = logging.getLogger("rawOutput")
    
    path2Module = getCurrentModulePath()
    
    setCprr = getCprrSettings(path2Module)
    ipCPRR = setCprr['ip']
    portCPRR = setCprr['port']
    
    # Создаём объект радара
    cprr = CPRR(telnetlib.Telnet, ipCPRR, portTelnet, timeout, portCPRR, logger, logRaw)
    
    # Выполняем чтение EEPROM
    result = CPRR_TEMPCODE_ADF(cprr, logger, ipCPRR, portTelnet, timeout)
    
    # выводим прочитанное в лог
    logger.info(result)
    logging.shutdown()
    return result  
    
def cprr_off_wdt():
    # Получаем путь к файлу настроек лога
    filePathSettingLog = getLogSettingsFileName()
    
    fileConfig(filePathSettingLog, defaults={'logfilename': 'EEPROM_test.log'}, disable_existing_loggers=False)
    logger = logging.getLogger('radarAlive')
    logRaw = logging.getLogger("rawOutput")
    
    path2Module = getCurrentModulePath()
    
    setCprr = getCprrSettings(path2Module)
    ipCPRR = setCprr['ip']
    portCPRR = setCprr['port']
    
    # Создаём объект радара
    cprr = CPRR(telnetlib.Telnet, ipCPRR, portTelnet, timeout, portCPRR, logger, logRaw)
    
    # Выполняем чтение EEPROM
    result = CPRR_WDT_OFF(cprr, logger, ipCPRR, portTelnet, timeout)
    
    # выводим прочитанное в лог
    logger.info(result)
    logging.shutdown()
    return result      

def cprr_get_SN():
    # Получаем путь к файлу настроек лога
    filePathSettingLog = getLogSettingsFileName()
    
    fileConfig(filePathSettingLog, defaults={'logfilename': 'EEPROM_test.log'}, disable_existing_loggers=False)
    logger = logging.getLogger('radarAlive')
    logRaw = logging.getLogger("rawOutput")
    
    path2Module = getCurrentModulePath()
    
    setCprr = getCprrSettings(path2Module)
    ipCPRR = setCprr['ip']
    portCPRR = setCprr['port']
    
    # Создаём объект радара
    cprr = CPRR(telnetlib.Telnet, ipCPRR, portTelnet, timeout, portCPRR, logger, logRaw)
    
    # Выполняем чтение EEPROM
    result = CPRR_GET_SN(cprr)
    
    # выводим прочитанное в лог
    logger.info(result)
    logging.shutdown()
    return result     

def cprr_ip_port_life():
    # Получаем путь к файлу настроек лога
    filePathSettingLog = getLogSettingsFileName()
    
    fileConfig(filePathSettingLog, defaults={'logfilename': 'EEPROM_test.log'}, disable_existing_loggers=False)
    logger = logging.getLogger('radarAlive')
    logRaw = logging.getLogger("rawOutput")
    
    path2Module = getCurrentModulePath()
    
    setCprr = getCprrSettings(path2Module)
    ipCPRR = setCprr['ip']
    portCPRR = setCprr['port']
    ipCPRR_new = setCprr['ip_in_life']
    portCPRR_new = setCprr['port_in_life']
    
    # Создаём объект радара
    cprr = CPRR(telnetlib.Telnet, ipCPRR, portTelnet, timeout, portCPRR, logger, logRaw)
    
    # Выполняем чтение EEPROM
    result = CPRR_IPPORT_SET(cprr, logger, ipCPRR_new, portCPRR_new, "ON")
    
    # выводим прочитанное в лог
    logger.info(result)
    logging.shutdown()
    return result      
    
if __name__ == "__main__":
#    main_test()
#    write_eeprom()
    write_eeprom()
    # print(off_wdt())
