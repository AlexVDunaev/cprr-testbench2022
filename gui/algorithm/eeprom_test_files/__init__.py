import logging
from logging.config import fileConfig

import telnetlib
from time import sleep
import re

from .utils import escape_ansi

from .utils import RadarLoadData, getSNfromTelnet # файл с фукциями
from .utils import getCprrSettings # возвращает ip, port
from .utils import getCprrDataPath # возвращает путь к файлам

from .cprrControl import CPRRCTRL

print("init done")