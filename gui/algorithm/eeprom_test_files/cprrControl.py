# -*- coding: utf-8 -*-
"""
Created on Tue May 24 15:58:52 2022

@author: RodionovGV

Подключение к радару и запрос целей

"""

from time import sleep
import struct
# import sys
# import os

import telnetlib

import logging
from logging.config import fileConfig

from dspRadar import dspRadar
from utils import RadarLoadData, getSNfromTelnet # файл с фукциями
from utils import getCprrSettings # возвращает ip, port
from utils import getCprrDataPath # возвращает путь к файлам
from utils import getLogSettingsFileName # возвращает путь к файлу настроек лога
from utils import getCurrentModulePath # возвращает путь к текущему исполняемому модулю
 
TEST_DSP = 1
CHIRP_SIZE = 720
pktMaxSize = 1024
#offset = 0x4000000
TARGET_DATA_SIZE = 88
targetItem = []

HardVersion=0
SoftVersion=0
SerialNumb =[]
PKT_ID_FIRMWARE = 16777

PKT_ID_REQUEST = 1 # Запрос Версии
REQUEST_CODE_GET_VER = 1 # Код для запроса версии
RADAR_ENABLED = 1
RADAR_DISABLED = 0
PKT_ID_MODE = 2
RADAR_STREAM_OFF = 0
RADAR_STREAM_ON = 0

# Получение серийного номера из данных радара
def setSN(SN):
    #print('Серийный номер {0}'.format(SN))
    SerialNumb.append(SN)


class CPRRCTRL:
    ack = 0
    def __init__(self, ipCPRR, port, logger):
        self.item = dspRadar(ipCPRR, port, 7000, self.onDataCallBack, self.onErrorCallBack)
        self.SN = getSNfromTelnet(telnetlib, ipCPRR)
        self.logger = logger
        self.status = '' # результат записи данных в CPRR
        #self.item = dspRadar("127.0.0.1", 7000, 7001, self.onDataCallBack, self.onErrorCallBack)
    
    
    def onDataCallBack(self, TYPE, SIZE, data):
    # Обработчик получения данных с радара    
        #print('The type of: {0}'.format(TYPE))
        if TYPE == 4:  # data
            Stat, GrabNumber, TimeStamp, Speed = struct.unpack_from("=iqqd", data, 0)
            self.logger.info('>>> Stat:%d, GrabNumber:%d, TimeStamp:%d, Speed%f' % (Stat, GrabNumber, TimeStamp, Speed))
            for i in range(int(SIZE) // TARGET_DATA_SIZE):
                ID, R, A, TTL, RCS, X, Xv, Xa, Y, Yv, Ya = struct.unpack_from("=qddqddddddd", data, 28 + i*TARGET_DATA_SIZE )
                #print('[%d] ID:%d, R:%.1f, A:%.2f, [X:%.2f, Y:%.2f]' % (i, ID, R, A, X, Y))
                targetItem.append([ID, X, Xv, Y, Yv, RCS, R, A])
                
        if TYPE == 5:  # version
            HardVersion, SoftVersion, SN = struct.unpack_from("=iii", data, 0)
            self.logger.info('[ver] HardVersion:%d, SoftVersion:%d, SerialNumb:%d' % (HardVersion, SoftVersion, SN))
            setSN(SN)
            
        if TYPE == 6:  # error
            error = struct.unpack_from("=i", data, 0)
            if (error[0] == 0):
                self.ack = 1  #             print('[] ACK OK!' )
            else:
                self.ack = -1  #            print('[] ACK ERROR!')
        pass    
    # Обработчик ошибок при подключении к радару
    def onErrorCallBack(self, errCode, errText):
        if errCode != 1:
            print('onErrorCallBack> code:%d ("%s")\n' % (errCode, errText))
        pass
    
    def getFrames(self, N):
        cnt = 0
        while cnt < N:
            cnt = cnt + 1
            self.item.send(PKT_ID_MODE, bytes(struct.pack("=ii", RADAR_ENABLED , RADAR_STREAM_OFF)))
            sleep(0.01)    
            
    def doFrames(self, N):        
        self.item.start()
        sleep(0.5)
        self.item.send(PKT_ID_REQUEST, bytes(struct.pack("=i", REQUEST_CODE_GET_VER)))
        sleep(0.1)
        self.logger.info("Serial number: {0}".format(SerialNumb[0]))
        self.getFrames(N)
        self.item.stop()
        
    def sendData(self, configDataSize, data):
        # Передаём данные в радар
        try:
            offset = 0x4000000 # Смещение во флешке
            size = 0
            writeWait = 0
            sendCount = 0
            while True:
                # break  #!!!
                pktSize = pktMaxSize
                if (configDataSize >= pktMaxSize):
                    configDataSize = configDataSize - pktMaxSize
                else:
                    pktSize = configDataSize
                    configDataSize = 0
                byte = data[sendCount * pktMaxSize : sendCount * pktMaxSize + pktSize]
                sendCount = sendCount + 1
                size = len(byte)
                # Прорежение вывода информации
                if sendCount%10==0 or sendCount==1:
                    self.logger.info("%d. send data block. size : %d (total:%.3fMb)" % (sendCount, size, (size)/1024/1024)) #offset + size
                if size == 0:
                    self.logger.info("send EOF")
                    sleep(0.5)
                self.ack = 0
                self.item.send(PKT_ID_FIRMWARE, bytes(struct.pack("=ii", offset, size)) + byte)
                sleep(0.1)
                offset = offset + size
                
                            
                if (size == 0):
                    self.logger.info("FLASH WRITE. PLEASE WAIT!")
                    sleep(1)
                    wCount = 0
                    self.ack = 0
                    while self.ack == 0:
                        sleep(0.01)
                        wCount = wCount + 1
                        if (wCount > 10):
                            sleep(1)
                            print(".", end='')
                            writeWait = writeWait + 1
                            if (writeWait > 50):
                                self.logger.critical("WAIT TIMEOUT!")
                                self.ack = -1
                    break;
                
                wCount = 0
                while self.ack == 0:
                    sleep(0.01)
                    wCount = wCount + 1
                    if (wCount > 10):
                        sleep(1)
                        print(".", end='')
                        writeWait = writeWait + 1
                        if (writeWait > 360):
                            self.logger.critical("WAIT TIMEOUT!")
                            self.ack = -1


            if (self.ack < 0):
                self.logger.critical("ACK> WRITE ERROR!")
                self.status = "ERROR"
            else:
                self.logger.info("ACK> WRITE DONE OK!")
                self.status = "OK"
            
        except KeyboardInterrupt:
            self.logger.info('Выход по Ctrl + C.\n')        

def loadFilesLeakCalib(logger, LEAK_FILENAME, CALIB_FILENAME):
    # итоговый массив для загрузки
    dig = []
    # Открываем файлы калибровки и пролаза
    try:
        f = open(CALIB_FILENAME, "r")
        counter = 0
        for line in f:
            line = line.strip()
            if line[-1:] == ",":
                line = line[1:-2]
            else:
                line = line[1:-1]
            line = line.split(',')
            counter = counter + 1
            if counter < 3 or counter >30:
                logger.info('%s' % line)
            if counter == 2:
                logger.info("first two-------------------last two")
            for item in line:
                dig.append(float(item))

    except FileNotFoundError:
        logger.error("File is not exist or accessible")
        
    leak = []
    if LEAK_FILENAME > "":
        try:
            f = open(LEAK_FILENAME, "r")
            counter = 0
            for line in f:
                line = line.strip()
                if line[-1:] == ",":
                    line = line[1:-2]
                else:
                    line = line[1:-1]
                line = line.split(',')
                #print('-------------- ')
                for item in line:
                    if item > '':
                        #print('%s ' % item, end='')
                        leak.append(int(item, base=10))
            logger.info('First N element in Leak: {}'.format(leak[:5]))
        except FileNotFoundError:
            logger.critical("File is not exist or accessible")
    else:
        logger.critical("LEAK File ignore")
    logger.info("Leak and Calib default was loaded")
    return {'dig':dig,'leak':leak}

def buildData(logger, dig, leak):
    LeakData = bytearray()
    calibData = bytearray()
    
    if len(dig) != 64:    
        logger.critical("File format error!")
    else:
        for koef in dig:
            calibData = calibData + struct.pack('f', koef)


    if len(leak) != CHIRP_SIZE*32:    
        logger.critical("Leak file format error!")
    else:
        # count = 0
        for koef in leak:
            LeakData = LeakData + struct.pack('h', koef)
    #        print("%d.%d" %(count,koef))
    #        count += 1
    configData = calibData + LeakData
    logger.info('Data build, size is: %d bytes' % len(configData))
    return configData


def makeCheckCode(data):
    return sum(data) & 0xFFFF

def Test_Load_LEAK_VECT_CALIB(radar, logger):
    ''' Загружаем калибровку и пролаз по умолчанию ''' 
    # Задание IP радара
    path2Module = getCurrentModulePath()
    setCprr = getCprrSettings(path2Module)

    host =  setCprr["ip"]

    # Получаем путь к папке с калибровкой и пролазом
    PATH = getCprrDataPath()
    print("getCprrDataPath()>", PATH)
    # Получаем серийный номер
    SN = getSNfromTelnet(telnetlib, host) 
    print("getSNfromTelnet()>", SN)
    # print(path2Module + PATH)
    
    '''Находим калибровку, пролаз, вектор калибровок по вычитанному серийному номеру'''
    
    # Получение в объект имён файлов для загрузки
    radarObj = RadarLoadData(PATH, SN) # DNV: убрал path2Module
    logger.info(radarObj.show())  
    try:     
        LEAK_FILENAME = radarObj.leak
        CALIB_FILENAME = radarObj.calibDefault
        print("LEAK>", LEAK_FILENAME)
        print("CALIB>", CALIB_FILENAME)
        # Загружаем данные из файлов
        dig = loadFilesLeakCalib(logger, LEAK_FILENAME, CALIB_FILENAME)
        # # Объединяем данные
        sendData = buildData(logger, dig['dig'], dig['leak'])
        # Вычисляем проверочный под и объединяем с данными
        checkCode = makeCheckCode(sendData)
        logger.info("checkCode : %d (0x%x)" % (checkCode, checkCode))
        sendData = sendData + struct.pack('H', checkCode)
        configDataSize = len(sendData)
        logger.info("config length (with check code) : %d" % configDataSize)
        # Отправляем данные
        print("sendData> size:", configDataSize)
        radar.sendData(configDataSize, sendData)
    except:
        print("Error in module cprrContorl.py")
        
def CPRR_load_leak_calib():
    # Задание IP радара
    path2Module = getCurrentModulePath()
    setCprr = getCprrSettings(path2Module)
    UDP_RADAR_IP = setCprr["ip"]
    UDP_RADAR_PORT = setCprr["port"]

    host =  setCprr["ip"]

    print("load_leak_calib> getSNfromTelnet(", host ,")")

    # Получаем серийный номер
    SN = getSNfromTelnet(telnetlib, host)

    print("load_leak_calib> getSNfromTelnet() : ", SN)

    # Получаем путь к файлу настроек лога
    filePathSettingLog = getLogSettingsFileName()
        
    fileConfig(filePathSettingLog, defaults={'logfilename': SN + '.log'}, disable_existing_loggers=False)
    logger = logging.getLogger('radarAlive')

    logger.info('START FIRST CONNECTION:', UDP_RADAR_IP, UDP_RADAR_PORT)
    logger.info('-------------------------------')
    logger.info(setCprr['log'])
    
    # Иницилаизируем объект радара
    radar = CPRRCTRL(UDP_RADAR_IP, UDP_RADAR_PORT, logger)

    # Запускаем с текущими настройками
    radar.item.start()
    sleep(0.5)

    # Запрашиваем пакет
    radar.item.send(PKT_ID_REQUEST, bytes(struct.pack("=i", REQUEST_CODE_GET_VER)))
    sleep(0.1)
    logger.info("Serial number: {0}".format(SerialNumb[0]))

    # Запрашиваем цели
    radar.getFrames(1)
    Test_Load_LEAK_VECT_CALIB(radar, logger)
    
    radar.item.stop()
    return radar.status

if __name__ == "__main__":
    CPRR_load_leak_calib()
    
