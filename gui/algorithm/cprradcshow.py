# -*- coding: utf-8 -*-
"""
Created on  02.06.2022

@author: Dunaev



"""

import os
import re
import matplotlib.pyplot as plt
import mmap
import array
import struct
import numpy as np
from shutil import copyfile


FRAME_HEADER_SIZE   = 52
# Параметры формата данных (как в алгоритме матлаба)
CHIRP_SAMPLE_OFFSET = 14 
CHIRP_SAMPLE_NUM    = 670
RADAR_RX_CHANNEL    = 16
RADAR_TX_CHANNEL    = 2
# первые чирпы содержат перех.процесс. они отбрасываются
CHIRP_DROP_NUM      = 0

# точки чирпа, для которых проводим анализ
VIEW_SAMPLE_START   = 30
VIEW_SAMPLE_END     = 660

# параметр DPI для figure. Значение больше: элементы крупнее.
DPI = 72 

# размер графиков в пикселях (делаю все графики одинаковые по размеру)
PNG_SIZE_X          = 3500
PNG_SIZE_Y          = 1000

# Величина выброса для отдельного канала
PEAK_AMP_VALUE      = 80
# Величина среднего перепада для других каналов
PEAK_AMP_OTHER_VALUE= 40

ZERO_CHIRP_TEST_VALUE = 50

# путь к GUI (Сводный график используется для GUI)
DIRECTORY_GUI_PNG = "./design"
# относительный путь к рабочей директории
DIRECTORY_WORK = "./algorithm/workdir"
# относительный путь к директории с отладочн. массивами
DIRECTORY_DEBUG = "./algorithm/dbg"
# формат сетки графика
Y_SCALE = "linear"  # ["log" | "linear"]


def algorithmPrint(text, error=0):
    print("algorithm[АЧХ]>", text)
    pass


def mmap_io(filename):
    with open(filename, mode="rb") as file_obj:
        with mmap.mmap(file_obj.fileno(), length=0, access=mmap.ACCESS_READ) as mmap_obj:
            buff = mmap_obj.read()
            return buff




# сохранение изображения графика в файл
def exportFigure(plt, filename):
    try:
        plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=0.5)                
        plt.savefig(filename)
        algorithmPrint(filename + "...OK")
    except PermissionError:
        algorithmPrint('save fig Error...', error=1)
    pass


# формируем метки на оси X
def getXticks(N, step=1, first=0):
    return [x*step + first for x in range(N)]


def puzzleCh(i):
    puzzle = [13, 14, 16, 15, 9, 10, 12, 11, 5, 6, 8, 7, 1, 2, 4, 3]
    N = i // 16
    p = i % 16
    return (puzzle[p] - 1) + N*16


# Общее оформление графиков
def getGraph(title, x = PNG_SIZE_X, y = PNG_SIZE_Y, dpi = DPI):
    fig = plt.figure(dpi=dpi, figsize=(x / dpi, y / dpi))
    ax = fig.add_subplot(111)  # 111 - это сетка. 111 означает сетка 1x1
    ax.set_yscale(Y_SCALE)
    ax.set_title((title))
    ax.grid()
    return fig, ax

def readConfigFile(dataFileName):
    data = []
    try:
        f = open(dataFileName, "r", encoding="utf-8")
        for line in f:
            if line.find("//") >= 0:
               line = line[:line.find("//")]
            items = re.findall('(\S+)\s*=\s*(\S+)', line)
            for k,v  in items:
                # print(k, v)
                data.append((k, v))
    except  OSError as e:
        print('[File:"{}"] Error: {}'.format(dataFileName, e.strerror))
        return data
    except:
        print("ERROR FORMAT: IGNORE")
    f.close()
    return data

def applyConfig(config):
    global PEAK_AMP_VALUE
    global PEAK_AMP_OTHER_VALUE
    global ZERO_CHIRP_TEST_VALUE
    
    for k, v  in config:
        # print(k, v)
        if str(k).upper() == "Амплитуда_выброса_канала".upper():
            print("Амплитуда_выброса_канала -->", int(v))
            PEAK_AMP_VALUE = int(v)
            
        if str(k).upper() == "Амплитуда_выбросов_суммарная".upper():
            print("Амплитуда_выбросов_суммарная -->", int(v))
            PEAK_AMP_OTHER_VALUE = int(v)
            
        if str(k).upper() == "Порог_нуля".upper():
            print("Порог_нуля -->", int(v))
            ZERO_CHIRP_TEST_VALUE = int(v)


# Функция алгоритма.
def algorithmCPRRAdcShow(param, debug=0):
    global PEAK_AMP_VALUE
    global PEAK_AMP_OTHER_VALUE
    global ZERO_CHIRP_TEST_VALUE
    global RADAR_RX_CHANNEL
    global workDirectory
    global frameIndex
    global channelErrorDetect
    global channelErrorList
    global diff2Stat

    if (param == "START"):
        
        applyConfig(readConfigFile("./AlgConfig.txt"))
        
        frameIndex = 0
        channelErrorDetect = [0]*16
        channelErrorList = []
        diff2Stat = []

        # Определим путь рабочей папки и вернем его, скриптам проверки
        workDirectory = os.getcwd() + "/" + DIRECTORY_WORK
        if not os.path.exists(workDirectory):
            os.makedirs(workDirectory)
                       
            
        copyfile(DIRECTORY_GUI_PNG + "/nodata.png",  workDirectory + "/adcerrors.png")
        copyfile(DIRECTORY_GUI_PNG + "/nodata.png",  workDirectory + "/adcDiffStat.png")
        copyfile(DIRECTORY_GUI_PNG + "/nodata.png",  workDirectory + "/adcall.png")
        return workDirectory

    if (param == "DONE"):
        
        print("channelErrorList len=", len(channelErrorList))
        print(channelErrorDetect)
        
        if len(channelErrorList) > 0:
            fig, ax = getGraph('Графики некорректных чирпов. (F-Номер кадра, C-номер чирпа (1..68), E-число ошибок)', 1200, 400, 72)
            for f, i, channelId, chirp in channelErrorList:
                x = getXticks(len(chirp))
                ax.plot(x, chirp, label=('RX %d (F:%d, C:%d, E:%d)' % (channelId+1, f+1, (i//16)+1, channelErrorDetect[channelId])))
                # корректно активировать легенду можно только после ax.plot()
                ax.legend(loc='upper right')
                # x1, x2, y1, y2 = ax.axis()
                # ax.axis((x1, x2, -100, 100))
            exportFigure(fig, DIRECTORY_GUI_PNG + "/adcerrors.png")
            copyfile(DIRECTORY_GUI_PNG + "/adcerrors.png",  workDirectory + "/adcerrors.png")

        
        if len(diff2Stat) > 0:
            diff2StatMax = np.max(diff2Stat, axis=0)
            
            fig, ax = getGraph("Статистика по выбросам (макс. выбросы по всем кадрам (%d), среднее значение выбросов по 64 чирпам)" % len(diff2Stat), 1200, 300, 72)
            for i in range(RADAR_RX_CHANNEL):
                ch = i % 16
                showdata = diff2StatMax[i, CHIRP_SAMPLE_OFFSET:CHIRP_SAMPLE_OFFSET+CHIRP_SAMPLE_NUM] #+ -150 + i*20
                x = getXticks(len(showdata))
                ax.plot(x, showdata, label=('RX %d (%d)' % (puzzleCh(ch)+1, ch+1)))
                ax.legend(loc='upper right')
                x1, x2, y1, y2 = ax.axis()
                ax.axis((x1, x2, 0, 100))
            exportFigure(fig, DIRECTORY_GUI_PNG + "/adcDiffStat.png")
            copyfile(DIRECTORY_GUI_PNG + "/adcDiffStat.png",  workDirectory + "/adcDiffStat.png")
            


        algorithmPrint("DONE")
        result = "%d" % sum(channelErrorDetect)
        for n in channelErrorDetect:
            result = result + (" %d" % n)
            
        # "5 0 0 0 0 0 1 2 1 1 0 0 0 0 0 0 0"
        # Общее число ошибок и ошибки по каждому каналу (строка)
        print("DONE--->", result)
        return result

    if (param == "CALC"):
        if debug == 1:
            # загружаю данные из файла
            fileName = ("%s/rawadc_err2.bin" % DIRECTORY_DEBUG)
        else:
            fileName = ("%s/rawadc.bin" % workDirectory)

        # Получаем данные от системы испытаний.
        # (полный кадр стрима, включая заголовок)
        data = mmap_io(fileName)

        # Если данных меньше чем заголовок - ошибка!
        if (len(data) < FRAME_HEADER_SIZE):
            # Ошибка формата данных.
            algorithmPrint("MMAP ERROR!!! Data size:%d" % (len(data)), error=1)
            return -1

        # Парсим заголовок кадра. В нем содержится информация
        # о кол-ве точек на чирп (возможно 720 и 378)
        STX, LEN, T, TIME, GN, T1, T2, T3, T4, T5, T6, N_CHIRP, N_CH, N = struct.unpack_from( "<iiiqqhhhhhhIII", data, 0)

        # algorithmPrint("FORMAT: [%d x %d x %d]" % (N_CHIRP, N_CH, N))

        RADAR_RX_CHANNEL = N_CH
        
        # Кол-во рабочих чирпов (без MIMO), которые идут в алгоритм
        CHIRP_NUM = N_CHIRP - CHIRP_DROP_NUM

        # Исходя из данных заголовка,
        # отбрасываем мусорные чирпы (N : кол-во точек АЦП в чирпе)
        drop_data_size = N * CHIRP_DROP_NUM * N_CH * 2
        adc_raw_array = array.array('h', data[FRAME_HEADER_SIZE + drop_data_size:])

        # проверка на корректность размера данных
        if (len(adc_raw_array) != N * CHIRP_NUM * N_CH):
            algorithmPrint("DATA FORMAT ERROR!!!", error=1)
            print("len(adc_raw_array):%d, (N * CHIRP_NUM * N_CH = %d)" % (len(adc_raw_array), N * CHIRP_NUM * N_CH))
            return 0

        # массив чирпов без привязки к каналам и доплеру (1024 чирпа)
        chirps = []
        # формирую список из 32 пустых списков (один список на канал (MIMO))

        # разбивка массива int16 на список чирпов (берем все данные)
        # [xxxxxxADCADCADCADCADCADC...ADCADCxxxxx] - Массив выборок АЦП 
        #        ^ <- CHIRP_SAMPLE_NUM  -> ^ - кол-во полезных выборок АЦП
        #        ^CHIRP_SAMPLE_OFFSET - Смещение полезных выборок АЦП
        for n in range(len(adc_raw_array) // N):
            chirp = adc_raw_array[n*N :n*N + N]
            # chirp = adc_raw_array[n*N + CHIRP_SAMPLE_OFFSET:n*N + CHIRP_SAMPLE_NUM + CHIRP_SAMPLE_OFFSET]
            chirps.append(chirp)

        cube3d = np.array(adc_raw_array).reshape(CHIRP_NUM, RADAR_RX_CHANNEL, N)
        # возьму двойной diff (анализ одиночного провала, т.е. один отсчет АЦП прыгнул)
        diff2channels = np.diff(np.diff(cube3d)) 
        # сложу суммарные выбросы по всем каналам внутри чирпа
        diff2channelsAll = np.sum(np.abs(diff2channels), axis=1)

        # найду максимальные значения выброса в каждом чирпе, приведу их к величине выброса
        # т.е. делю на 16 - если помеха во всех каналах одинаковая
        # и делю на 2, т.к. учитывается суммарный выброс (туда + обратно)
        # реально выбросы в каналах разные и бывают не во всех. 
        diff2max = np.max(diff2channelsAll[:,VIEW_SAMPLE_START:VIEW_SAMPLE_END], axis = 1) / (16*2)
        diff2max = diff2max.astype(int)
        
        # отладка, мониторинг чиров по выбросам        
        # ch = 55-2
        # c = puzzleCh(10)
        # showdata = cube3d[ch, c, :]
        # fig, ax = getGraph("TEST Кадр:%d" % (frameIndex + 1), 1500, 800, 72)
        # x = getXticks(len(showdata))
        # ax.plot(x, showdata)
        # plt.show()
        diff2channelsStat = np.mean(np.abs(diff2channels[4:]), axis=0)
        # # diff2channelsStat = np.mean(diff2channelsStat, axis=0)
        # fig, ax = getGraph("TEST Кадр:%d" % (frameIndex + 1), 1500, 800, 72)
        # showdata = diff2channelsStat[0, VIEW_SAMPLE_START:VIEW_SAMPLE_END]
        # x = getXticks(len(showdata))
        # ax.plot(x, showdata)
        # plt.show()
        diff2Stat.append(diff2channelsStat)

        if frameIndex == 0:
            channels = []
            for i in range(RADAR_RX_CHANNEL):
                channel = []
                for chirpindex in range(CHIRP_NUM):
                    channel.extend(chirps[chirpindex*16 + i])
                channels.append(channel)


            fig, ax = getGraph("Полезная часть первых чирпов АЦП. Кадр:%d" % (frameIndex + 1), 1200, 400, 72)
            for i in range(RADAR_RX_CHANNEL):
                ch = i % 16
                offset = 720*4*8
                showdata = channels[i][offset+CHIRP_SAMPLE_OFFSET:offset + CHIRP_SAMPLE_OFFSET+ CHIRP_SAMPLE_NUM]
                x = getXticks(len(showdata))
                ax.plot(x, showdata, label=('RX %d (%d)' % (puzzleCh(ch)+1, ch+1)))
                ax.legend(loc='upper right')
                x1, x2, y1, y2 = ax.axis()
                ax.axis((x1, x2, -500, 500))
            exportFigure(fig, DIRECTORY_GUI_PNG + "/adcall.png")
            copyfile(DIRECTORY_GUI_PNG + "/adcall.png",
                      workDirectory + "/adcall.png")

        errors = 0
        print("TEST. Frame:%d" % (frameIndex))
        for i, chirp in enumerate(chirps):
            usefulChirp = chirp[CHIRP_SAMPLE_OFFSET:CHIRP_SAMPLE_OFFSET+CHIRP_SAMPLE_NUM]
            sumTest = sum([abs(x) for x in usefulChirp])
            diffTest = np.max(np.abs(np.diff(usefulChirp)))  
            # контроль выброса (с учетом наличия выбросов в соседних каналах)
            if (sumTest <= ZERO_CHIRP_TEST_VALUE) or ((diffTest > PEAK_AMP_VALUE)and(diff2max[i//16]>PEAK_AMP_OTHER_VALUE)):
                errors = errors + 1
                print("Error! Frame:%d, Chirp:%d !!!!!!!!!!!!!!!! sum:%d, diff:%d" % (frameIndex, i, sumTest,diffTest ))
                channelId = puzzleCh(i % 16)
                # При первой ошибке сохраним график
                if channelErrorDetect[channelId] == 0:
                    channelErrorList.append((frameIndex, i, channelId, usefulChirp[:]))
                channelErrorDetect[channelId] = channelErrorDetect[channelId] + 1

        frameIndex = frameIndex + 1
        return "%d" % errors
    return "ERROR"

# режим автономной отладки модуля через algRunner.py 
if __name__ == '__main__':
    DIRECTORY_DEBUG = "." + DIRECTORY_DEBUG
    DIRECTORY_GUI_PNG = "." + DIRECTORY_GUI_PNG
    algorithmCPRRAdcShow("START", debug=1)
    algorithmCPRRAdcShow("CALC", debug=1)
    algorithmCPRRAdcShow("DONE", debug=1)
# ALG_DEBUG           = 1
#
