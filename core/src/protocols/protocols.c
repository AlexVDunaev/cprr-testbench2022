#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../main.h"
#include "../core/commandUnit.h"
#include "protocols.h"
#include "../core/coreUInterface.h"
#include "../log/log.h"



int RAW_Protocol(char typeCallBack, void * ext, int clientIndex, TCMD * C, unsigned char * outBuffer);
int PROT_framing_raw(char typeCallBack, char * buffInput, int *buffInputSize, char * frameOutput, int * frameOutputSize, char * ErrorText);
int PROT_framing_Test1(char typeCallBack, char * buffInput, int *buffInputSize, char * frameOutput, int * frameOutputSize, char * ErrorText);

int DummyProtocolMNG(char typeCallBack, TMCUI * ui, char * signalName, Tsub_param_rec * value, char * pktInBuffer, int pktInSize, char * pktOutBuffer, int * pktOutSize, char * ErrorText);

void initProtocols(TSystemState * state) {
    TProtocolRec * a;
    TProtFramingRec * framing;

    //----------- тестовые обертки -----------------
    //--- сырой (что получаем, то передаем) "DATA" -> DATA
    framing = BuildFramingRec(state, "RAW", "", 0);
    framing->callBack = PROT_framing_raw;
    AppendFramingUnit(state, framing, OBJ_SOURCE_EMBEDDED);
    //--- тест "<DATA>CC" -> DATA
    //    framing = BuildFramingRec(state, "FR_TEST1", "CRC8", 0);
    //    framing->callBack = PROT_framing_Test1;
    //    AppendFramingUnit(state, framing, OBJ_SOURCE_EMBEDDED);
    //    //--- BSAP "10 02 <DATA> 10 03 CC CC" -> DATA
    //    framing = BuildFramingRec(state, "BSAP", "", 0);
    //    framing->callBack = PROT_BSAP_Framing;
    //    AppendFramingUnit(state, framing, OBJ_SOURCE_EMBEDDED);
    //    //--- MODBUS: A, CMD, START [HI,LO], NUM[HI, LO], CRC 
    //    framing = BuildFramingRec(state, "MODBUS", "", 0);
    //    framing->callBack = PROT_MODBUS_Framing;
    //    AppendFramingUnit(state, framing, OBJ_SOURCE_EMBEDDED);


    //----------- протокол - заглушка -----------------
    a = BuildProtocolRec(state, "DUMMY", "FRAMING=RAW; POLLING=INTERNAL", 0);
    a->callBack = DummyProtocolMNG;
    AppendProtocolUnit(state, a, OBJ_SOURCE_EMBEDDED);
    //    //----------- тестовый протокол -----------------
    //    a = BuildProtocolRec(state, "TESTPROPOCOL", "FRAMING=FR_TEST1", 0);
    //    a->callBack = MyProtocolMNG;
    //    AppendProtocolUnit(state, a, OBJ_SOURCE_EMBEDDED);
}

void initProtocol(TSystemState * state, int clientIndex, char Protocol) {
    if (state) {
        state->buf[clientIndex].ProtocolCallBack = RAW_Protocol;
    }
}


// Функция унификации протокольного уровня
// перенос буфера CMD в TX

int RAW_Protocol(char typeCallBack, void * ext, int clientIndex, TCMD * C, unsigned char * outBuffer) {
    //  
    TSystemState * State;
    int n = 0;
    if (ext != 0) {
        State = (TSystemState *) ext;

        if (State->buf[clientIndex].CmdSize) {
            memcpy(State->buf[clientIndex].Tx, State->buf[clientIndex].Cmd, State->buf[clientIndex].CmdSize);
            n = State->buf[clientIndex].TxSize = State->buf[clientIndex].CmdSize;
        }
    }
    return n;
}

// тестовый формат кадра "<DATA>CC"

int PROT_framing_Test1(char typeCallBack, char * buffInput, int *buffInputSize, char * frameOutput, int * frameOutputSize, char * ErrorText) {
    int i;
    int startIndex = -1;
    int stopIndex = -1;
    int buffSize;

    if (typeCallBack == PROT_FRAMING_PACK) {
        buffSize = *buffInputSize;
        frameOutput[0] = '<'; // флаг начала
        for (i = 0; i < buffSize; i++)
            frameOutput[i + 1] = buffInput[ i ];
        frameOutput[buffSize + 1] = '>'; // флаг конца
        frameOutput[buffSize + 2] = 'C'; //CRC
        frameOutput[buffSize + 3] = 'C'; //CRC
        *frameOutputSize = buffSize + 4;
        return 1; // фрейм создан
    }
    if (typeCallBack == PROT_FRAMING_UNPACK) {
        // buffOutput -     новый буфер. 
        // buffOutputSize - новый размер в буфере
        buffSize = *buffInputSize;
        for (i = 0; i < buffSize; i++)
            if (buffInput[i] == '<') {
                Log_DBG("DBG. PROT_framing_Test1 - startIndex = %d", i);
                startIndex = i;
                break;
            }
        if (startIndex == -1) { // начало кадра не найдено
            *frameOutputSize = 0;
            *buffInputSize = 0;
            Log_DBG("DBG. PROT_framing_Test1 - startIndex = %d", i);
            return 0;
        } else {
            if (startIndex != 0) {
                for (i = 0; i < buffSize - 1; i++)
                    buffInput[i + 0] = buffInput[i + startIndex];
                buffSize = buffSize - startIndex;
                *buffInputSize =
                        buffSize;
            }
            // поиск метки конца
            for (i = 0; i < buffSize; i++)
                if (buffInput[i] == '>') {
                    stopIndex = i;
                    Log_DBG("DBG. PROT_framing_Test1 - stopIndex = %d", stopIndex);
                    break;
                }
            if (stopIndex > 0) {
                if (buffSize > (stopIndex + 2)) // 2 - байта CRC
                {
                    if ((buffInput[ stopIndex + 1 ] == 'C')&&
                            (buffInput[ stopIndex + 2 ] == 'C')) { //имитация расчета CRC
                        for (i = 0; i < buffSize; i++)
                            frameOutput[ i ] = buffInput[ i + 1 ]; // "<DATA>CC" -> "DATA"
                        *frameOutputSize = stopIndex - 1;
                        *buffInputSize = 0;
                        Log_DBG("DBG. PROT_framing_Test1 - OK!");

                        return 1; // фрейм найжен
                    } else { // ошибка CRC
                        sprintf(ErrorText, "Error");
                        // очистим буфер
                        *frameOutputSize = 0;
                        *buffInputSize = 0;
                    }
                } else {
                    // буфер неполный
                }
            }
        }
    }
    return 0; // ошибка
}


// тестовый формат:  кадра "DATA" 

int PROT_framing_raw(char typeCallBack, char * buffInput, int *buffInputSize, char * frameOutput, int * frameOutputSize, char * ErrorText) {
    int i;
    int buffSize;

    if (typeCallBack == PROT_FRAMING_PACK) {
        buffSize = *buffInputSize;
        for (i = 0; i < buffSize; i++)
            frameOutput[ i ] = buffInput[ i ];
        *frameOutputSize = buffSize;
        return 1; // фрейм создан
    }
    if (typeCallBack == PROT_FRAMING_UNPACK) {
        // buffOutput -     новый буфер. 
        // buffOutputSize - новый размер в буфере
        buffSize = *buffInputSize;
        for (i = 0; i < buffSize; i++)
            frameOutput[ i ] = buffInput[ i ];
        *frameOutputSize = buffSize;
        *buffInputSize = 0; // очистим входной буфер
        ErrorText[0] = 0; //""
        return 1; // фрейм найден
    }
    return 0; // ошибка
}


int DummyProtocolMNG(char typeCallBack, TMCUI * ui, char * signalName, Tsub_param_rec * value, char * pktInBuffer, int pktInSize, char * pktOutBuffer, int * pktOutSize, char * ErrorText) {
    //-------------------------------------------------------------------------
    if (typeCallBack == PROT_MNG_RX) {
        pktInBuffer[pktInSize] = 0;
        if (pktInSize)
            pktInBuffer[pktInSize - 1] = 0; 
        memcpy(pktOutBuffer, pktInBuffer, pktInSize);
        // сформируем ответную посылку:
        *pktOutSize = pktInSize;
        return PROT_MNG_RESULT_OK;
    }
    //-------------------------------------------------------------------------
    return PROT_MNG_RESULT_ERR;
}




