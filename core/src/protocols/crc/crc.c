// модуль расчета КС различными методами, которые используются в различных протоколах

#include "crc.h"

unsigned int WordSUM32(unsigned short int * W, int count, int option)
{
    unsigned int result = -1;
    int i;

    if (option & CRC_OPTION_INIT_ZERO) 
        result = 0;
    
    for (i = 0; i < count; i++)
        {
        result += W[i];
        }
    if (option & CRC_OPTION_RESULT_INVERT) 
        result = ~result;

    if (option & CRC_OPTION_RESULT_LE) 
        result = ((result & 0xFFFF0000) >> 16) | ((result & 0xFFFF) << 16);

    return result;
}


