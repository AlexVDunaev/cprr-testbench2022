/* 
 * File:   crc.h
 * Author: Dunaev
 *
 * Created on 21 Ноябрь 2014 г., 14:22
 */

#ifndef CRC_H
#define	CRC_H


#define	CRC_OPTION_RESULT_INVERT	(1 << 0)       //результат побитово инвертируется.
#define	CRC_OPTION_RESULT_LE    	(1 << 1)       //результат размещается в формате LE (иначе BE)
#define	CRC_OPTION_INIT_ZERO    	(1 << 2)       //начальное значение равно 0. (иначе -1)


unsigned int WordSUM32(unsigned short int * W, int count, int option);

#endif	/* CRC_H */

