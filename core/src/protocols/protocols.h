/* 
 * File:   protocols.h
 * Author: Dunaev
 *
 * Created on 27 Август 2014 г., 13:57
 */

#ifndef PROTOCOLS_H
#define	PROTOCOLS_H

#include "../main.h"


void    initProtocol (TSystemState * state, int  clientIndex, char Protocol);
void    initProtocols(TSystemState * state);


#endif	/* PROTOCOLS_H */

