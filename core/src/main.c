#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "core/coreUInterface.h"
#include <stdlib.h>

#include "main.h"
#include "mCore.h"

#include "./drivers/cprr/cprr24.h"
#include "./drivers/corner77/corner77.h"
#include "./drivers/daq/t25Imit.h"
#include "./ROS/rosbagInterface.h"

   
#include <sys/time.h>
#include <sys/resource.h>
//#include <readline.h>

#define _GNU_SOURCE
#include <sched.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
 

int closeServer = 0;
TMCUI * mc;
 
#define SERVER_BASE_PORT_USER        4558

extern TSystemState SystemState;


int rosbagDriverTest(char * bagFileName);

int main(int argc, char **argv) {
    int MC_MODE = 1 * MC_MODE_MULTITHREAD;
    int i;

    //---------------------------------------------------
    // TODO сделать добавление через mcAppendUserAlgorithm
    //mcAppendUserAlgorithm("CPRR24", NULL);
    //---------------------------------------------------
    
    // Установка высокого приоритета процесса 
    if (setpriority(PRIO_PROCESS, 0, -15) < 0) {
        Log_DBG("setting priority to valid level");
        setpriority(PRIO_PROCESS, 0, 0);
    }

    for (i = 0; i < argc; i++) {
        if (strcmp(argv[i], "nmt") == 0) {
            {
                MC_MODE = 0;
                Log_DBG("nmt option! Start on NOMULTITHREAD MODE...");
            }
        }
    }
 
    mc = mcInit(argc, argv, SERVER_BASE_PORT_USER, MC_MODE);
    mcAppendUserLineDriver("CPRR24", "", &CPRR24_DRV_CallBack, MAC_ID_WRITE_BUFFER | MAC_ID_READ_BUFFER |  MAC_ID_POLL);
    mcAppendUserLineDriver("DAQ", "", &DAQ_DRV_CallBack, MAC_ID_WRITE_BUFFER | MAC_ID_READ_BUFFER |  MAC_ID_POLL);
    mcAppendUserLineDriver("CPYC77", "", &CPYC77_DRV_CallBack, MAC_ID_WRITE_BUFFER | MAC_ID_READ_BUFFER |  MAC_ID_POLL);
    if (mcStart() == 0) {
        while (closeServer == 0) {
            if (MC_MODE != MC_MODE_MULTITHREAD)
                mcProcessing();
            usleep(10 * 1000);
        }
    }
    return 0;
}