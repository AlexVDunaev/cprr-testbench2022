#ifndef TTYS_FILE_H
#define TTYS_FILE_H

#include "../../main.h"

int  p232_init (TSystemState * state, char * pname, int Baud, char Protocol, char Line);
int  p232_read (TSystemState * state, int index);
int  p232_write(TSystemState * state, int fd, int index);
int  p232_send (int fd, unsigned char *d, int size);
void p232_DTR  (int fd, char DTR);
void p232_RTS  (int fd, char RTS);
int  p232_CTS  (int fd);

int  p232_printbuf(char * title, unsigned char *d,int size);

int p232CallBack(TMCExtDriver * event);
int p232SelfTest(void);

#endif
