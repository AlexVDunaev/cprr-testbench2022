#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdlib.h>
#include "ttys.h"
#include "../../main.h"
#include "../../log/log.h"
#include "../interfaces.h"


void p232_RTS(int fd, char RTS);

void p232_SG_signal(int signal) {
    //
    Log_DBG("p232_SG> signal = %d", signal);
}

int p232_init(TSystemState * state, char * pname, int Baud, char Protocol, char Line) {
    struct termios newtio;
    struct termios tty;
    int fd = 0;

    if ((fd = open(pname, O_RDWR | O_NOCTTY | O_NDELAY)) < 0) {
        Log_DBG("Serial port (%s) opened Error!\n", pname);
        //Log_DBG("INFO: /etc/inittab: '/sbin/agetty -L ttyS0 115200' - block port! ",pname);
        return 0;
    } else
        Log_DBG("Serial port (%s) opened OK!\n", pname);

    state->inf[state->clients].Client_type = SERIAL_CLIENT_TYPE;
    state->inf[state->clients].Client_prot = Protocol;

    state->Line[state->clients].typeLine = Line;
    state->Line[state->clients].LineStatus = CLIENT_STATUS_OFF;

    tcflush(fd, TCIOFLUSH);

    tcgetattr(fd, &tty);

    cfsetospeed(&tty, (speed_t) Baud);
    cfsetispeed(&tty, (speed_t) Baud);
    tty.c_iflag = IGNBRK;
    tty.c_lflag = IEXTEN;
    tty.c_oflag = 0;
    tty.c_cflag |= CS8 | CLOCAL | CREAD;

    tty.c_cc[VMIN] = 1; // кол-во символов после получения, которых вызавается read() 12 - размер пакета В7-64
    tty.c_cc[VTIME] = 0;

    tty.c_iflag &= ~(IXON | IXOFF | IXANY);

    tty.c_cflag &= ~(PARENB | PARODD | CRTSCTS | HUPCL);
    tty.c_cflag &= ~CSTOPB;

    tty.c_cflag |= HUPCL;
    //tty.c_cflag |= CRTSCTS ;    // Если нужно выдать RTS

    tcsetattr(fd, TCSANOW, &tty);

    //p232_RTS(fd, 1);

    state->fds[state->clients].fd = fd;
    state->Line[state->clients].LineStatus = CLIENT_STATUS_ON;
    state->clients++;



    return fd;

}


#define UART_BAUD_DEFAULT 9600

int p232_baud2uartCode(int baud) {
    int result = MB9600;
    switch (baud) {
        case 115200:
            result = MB115200;
            break;
        case 57600:
            result = MB57600;
            break;
        case 38400:
            result = MB38400;
            break;
        case 19200:
            result = MB19200;
            break;
        case 9600:
            result = MB9600;
            break;
        case 4800:
            result = MB4800;
            break;
    }
    return result;
}

int p232_init_Ex(char * pname, char * params) {
    struct termios newtio;
    struct termios tty;
    int fd = 0;
    int Baud = 9600;

    // TO DO: Сделать следующий формат: "9600,8N1" (или "9600,8,N,1")
    Baud = atoi(params);
    if (Baud == 0)
        Baud = UART_BAUD_DEFAULT;

    if ((fd = open(pname, O_RDWR | O_NOCTTY | O_NDELAY)) < 0) {
        Log_DBG("Serial port (%s) opened Error!\n", pname);
        //Log_DBG("INFO: /etc/inittab: '/sbin/agetty -L ttyS0 115200' - block port! ",pname);
        return 0;
    } else
        Log_DBG("Serial port (%s, baud:%d) opened OK!\n", pname, Baud);

    tcflush(fd, TCIOFLUSH);

    tcgetattr(fd, &tty);
    Baud = p232_baud2uartCode(Baud);
    cfsetospeed(&tty, (speed_t) Baud);
    cfsetispeed(&tty, (speed_t) Baud);
    tty.c_iflag = IGNBRK;
    tty.c_lflag = IEXTEN;
    tty.c_oflag = 0;
    tty.c_cflag |= CS8 | CLOCAL | CREAD;

    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 0;

    tty.c_iflag &= ~(IXON | IXOFF | IXANY);

    tty.c_cflag &= ~(PARENB | PARODD | CRTSCTS | HUPCL);
    tty.c_cflag &= ~CSTOPB;

    tty.c_cflag |= HUPCL;
    //tty.c_cflag |= CRTSCTS ;    //если нужно выдать RTS

    tcsetattr(fd, TCSANOW, &tty);

    return fd;
}

int p232_read(TSystemState * state, int index) {
    int i, j, n, fd;
    int packet = 0;
    unsigned char buf[CLIENT_CM_SIZE];

    fd = state->fds[index].fd;

    n = read(fd, buf, CLIENT_CM_SIZE);

    if (n < 0) {
        Log_DBG("fds[%d] reading error n=%d\n", index, n);
        return 0;
    } else
        if (n == 0) {
        state->fds[index].events = 0;
        tcflush(fd, TCIOFLUSH);
    } else {
#ifdef DEBUG_TTY
        if (n > 0) p232_printbuf("PORT RX: \n", &buf[0], n);
#endif
        if ((state->buf[index].RxSize + n) < CLIENT_CM_SIZE) {
            memcpy(&state->buf[index].Rx[state->buf[index].RxSize], buf, n);
            state->buf[index].RxSize += n;
            packet = state->buf[index].RxSize;
        } else {
            Log_DBG("ERR!!!...p232.Buffer Error. N = %d\n", state->buf[index].RxSize + n);
            state->buf[index].RxSize = 0;
        }

        //========================================================

    }
    return packet;
}

int p232_read_new(TLineRec * a) {
    int n, fd;
    int packet = 0;
    unsigned char buf[CLIENT_CM_SIZE];

    fd = a->fd;

    n = read(fd, buf, CLIENT_CM_SIZE);

    if (n < 0) {
        Log_DBG("fd[%s]  reading error n=%d\n", a->name, n);
        return 0;
    } else
        if (n == 0) {
        //state->fds[index].events = 0;
        //tcflush(fd, TCIOFLUSH);
    } else {
#ifdef DEBUG_TTY
        if (n > 0) p232_printbuf("PORT RX: \n", &buf[0], n);
#endif
        if ((a->buffers.RxSize + n) < CLIENT_CM_SIZE) {
            memcpy(&a->buffers.Rx[a->buffers.RxSize], buf, n);
            a->buffers.RxSize += n;
            packet = a->buffers.RxSize;
        } else {
            Log_DBG("ERR!!!...p232.Buffer Error. N = %d\n", a->buffers.RxSize + n);
            a->buffers.RxSize = 0;
        }
        //========================================================
    }
    return packet;
}

int p232_write(TSystemState * state, int fd, int index) {
    //return p232_send(fd, fd_tty->buf[index].Tx, fd_tty->buf[index].TxSize); 
}

int p232_printbuf(char * title, unsigned char *d, int size) {
#define RX_STR_LENGTH 60
    int i;
    char s[RX_STR_LENGTH + 10] = "";

    for (i = 0; i < size; i++)
        if (3 * (i + 1) < RX_STR_LENGTH)
            sprintf(s, "%s %.2X", s, d[i]);
        else {
            sprintf(s, "%s [%d]", s, size - i);
            break;
        }

    Log_DBG("\n%s>%s", title, s);
}

int p232_send(int fd, unsigned char *d, int size) {
#define TX_STR_LENGTH 60
    int i;
    char s[TX_STR_LENGTH + 10] = "";

    for (i = 0; i < size; i++)
        if (3 * (i + 1) < TX_STR_LENGTH)
            sprintf(s, "%s %.2X", s, d[i]);
        else {
            sprintf(s, "%s [%d]", s, size - i);
            break;
        }


    //p232_DTR (fd,false);
#ifndef DEBUG_NO232 
    i = write(fd, d, size);
    tcflush(fd, TCOFLUSH);
#endif
    //p232_DTR (fd,true);
    //#ifdef DEBUG_TTY
    Log_DBG("\nPORT TX:>%s", s);
    //#endif

    return i;
}

void p232_DTR(int fd, char DTR) {
    int status;

    ioctl(fd, TIOCMGET, &status);
    if (!DTR)
        status &= ~(TIOCM_DTR);
    else
        status |= TIOCM_DTR;

    ioctl(fd, TIOCMSET, &status);
    //Log_DBG("\nSet DTR (%d)=%d",TIOCM_DTR,DTR);
}

void p232_RTS(int fd, char RTS) {
    int status;

    ioctl(fd, TIOCMGET, &status);

    //Log_DBG("p232_RTS(TIOCMGET : 0x%X)", status);
    if (!RTS)
        status &= ~(TIOCM_RTS);
    else
        status |= TIOCM_RTS;

    ioctl(fd, TIOCMSET, &status);
}

int p232_CTS(int fd) {
    int status;

    ioctl(fd, TIOCMGET, &status);
    return (status & TIOCM_CTS);
}

int p232CallBack(TMCExtDriver * event) {
    int result;

    if (event->id == MAC_ID_OPEN) {
        //TxBuffer - имя устройства
        //RxBuffer - параметры устройства
        result = p232_init_Ex(event->param.prOPEN.device, event->param.prOPEN.param); //"/dev/ttyS6", "");
        //result = open ("/dev/ttyS6", O_RDWR | O_NOCTTY | O_NDELAY);
        //Log_DBG("(MAC_ID_OPEN): %d", result);
        return result;
    }
    if (event->id == MAC_ID_WRITE_BUFFER) {
        result = write(event->fd, event->param.prTX.inBuff, event->param.prTX.inCount);
        if (result > 0)
            return MAC_RESULT_OK;
        else
            return MAC_RESULT_ERROR;

    }
    if (event->id == MAC_ID_READ_BUFFER) {
        result = read(event->fd, event->param.prRX.rxBuff, 100);
        *event->param.prRX.rxCount = result;
        if (result)
            return MAC_RESULT_OK;
        else
            return MAC_RESULT_ERROR;

    }
    if (event->id == MAC_ID_POLL) {
        result = poll(event->param.prPOLL.in_fds, event->param.prPOLL.in_fds_count, 100); //пользователь сам выбирает там-ауйт
    }
    if (event->id == MAC_ID_CTRL) {
        Log_DBG("USER_LINE(MAC_ID_CTRL): %s", event->param.prCTRL.ctrlCommand);
        if (strcmp(event->param.prCTRL.ctrlCommand, "RTS:ON") == 0) {
            p232_RTS(event->fd, 1);
        } else
            if (strcmp(event->param.prCTRL.ctrlCommand, "RTS:OFF") == 0) {
            p232_RTS(event->fd, 0);
        }

        result = 0;
    }

    return result;

}

// функция тестирования модуля

int p232SelfTest(void) {
    return MAC_RESULT_OK; // 0 : нет ошибок.
}


