#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "eth.h"
#include "../../protocols/protocols.h"
#include "../../log/log.h"

static struct sockaddr_in claddr; // client socket structure address
static struct sockaddr_in addr4;

#define MAX_TCP_CLIENT_COUNT  10

typedef struct {
    int Sock;
    struct sockaddr_in Address;
    char ClientStatus; //статус клиента: Отключен/Включен
    char pollDisabled; //фоновое чтение отключено. Чтение определяется пользователем. 

} TTCP_CLIENT;

typedef struct {
    unsigned int Count;
    TTCP_CLIENT Client[MAX_TCP_CLIENT_COUNT];


} TTCP_CLIENTS;

static TTCP_CLIENTS TcpClients = {.Count = 0};

int mc_sock_init(TSystemState *fd_sock, int port, char Protocol, char Line) {
    int qw;

    int listenp = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    bzero(&addr4, sizeof (addr4));
    addr4.sin_family = AF_INET;
    addr4.sin_addr.s_addr = htonl(INADDR_ANY);
    addr4.sin_port = htons(port);

    fd_sock->Line[fd_sock->clients].LineStatus = CLIENT_STATUS_OFF;
    fd_sock->inf[fd_sock->clients].Client_type = TCP_SERVER_TYPE;
    fd_sock->inf[fd_sock->clients].Client_prot = Protocol;
    fd_sock->Line[fd_sock->clients].typeLine = Line;
    fd_sock->Line[fd_sock->clients].LineCallBack = mc_ethCallBack;
    fd_sock->Line[fd_sock->clients].BufferedMode = GLOBAL_BUFFERED_MODE_DEFAULT;


    if (bind(listenp, (struct sockaddr *) &addr4, sizeof (addr4)) == -1) {
        Log_Ex("err", LEVEL_ERR, (char *) "Bind Error! port = %d", port);
        return 0;
    } else {
        qw = listen(listenp, TCP_CLIENT_MAX);
    }

    if (qw)
        Log_Ex("err", LEVEL_ERR, (char *) "listen error");
    else {
        //printf("listen OK\n");
        //printf("------------------------------------\n");
    }
    fd_sock->fds[fd_sock->clients].fd = listenp;
    fd_sock->Line[fd_sock->clients].LineStatus = CLIENT_STATUS_ON;
    fd_sock->clients++;

    return 1;
}

int mc_Get_sock_connections(TSystemState *fd_sock) {
    int i;
    int n = 0;
    for (i = 0; i < fd_sock->clients; i++) {
        if (fd_sock->inf[i].Client_type == TCP_FROM_CLIENT_TYPE)
            if (fd_sock->Line[i].LineStatus != CLIENT_STATUS_OFF) { // заменим отпавшего клиента новым
                n++;
            }
    }
    return n;
}

int mc_client_accept(TSystemState *fd_sock, int index, char Protocol, char Line) {
    socklen_t len; // server socket structure address

    int i;
    int cl_index; // индекс в структуре 

    cl_index = fd_sock->clients;
    //---------------------------------------------------------
    // --- поиск клиентов с типом "CLIENT_STATUS_OFF"
    for (i = 0; i < fd_sock->clients; i++) {
        if (fd_sock->inf[i].Client_type == TCP_FROM_CLIENT_TYPE)
            if (fd_sock->Line[i].LineStatus == CLIENT_STATUS_OFF) { // заменим отпавшего клиента новым
                cl_index = i;
                break;
            }
    }

    if (fd_sock->clients >= CLIENT_MAX + 1) {
        Log_Ex("err", LEVEL_ERR, (char *) "Lotos client too much. (Client limit = %d)", CLIENT_MAX + 1);
        return 0;
    }
    len = sizeof (claddr);
    int fd_client = accept(fd_sock->fds[index].fd, (struct sockaddr *) &claddr, &len);
    if (fd_client == -1) {
        Log_Ex("err", LEVEL_ERR, (char *) "Error accept!!!!!!!!");
        return 0;
    } else {
        fd_sock->fds[cl_index].fd = fd_client;
        fd_sock->inf[cl_index].Client_type = TCP_FROM_CLIENT_TYPE;
        fd_sock->inf[cl_index].Client_prot = Protocol;
        fd_sock->Line[cl_index].typeLine = Line;
        fd_sock->Line[cl_index].LineStatus = CLIENT_STATUS_ON;


        //----------------------------------------------
        // typedef int (  TLineCallBack(char typeCallBack, void * ext, int  clientIndex, char * Buffer, int count)); 
        fd_sock->Line[cl_index].LineCallBack = mc_ethCallBack;
        //----------------------------------------------
        inet_ntop(AF_INET, &claddr.sin_addr, fd_sock->inf[cl_index].Client_ip, 100);
        Log_DBG("Connections %d: Host IP :%s\n", mc_Get_sock_connections(fd_sock) + 1, fd_sock->inf[cl_index].Client_ip);
        if (Line == LOTOS_BUFFERED_LINE)
            Log_DBG("---------- BUFFERED MODE! ----------------\n");
        if (Line == LOTOS_UNBUFFERED_LINE)
            Log_DBG("--------- UNBUFFERED MODE! ---------------\n");


        if (cl_index == fd_sock->clients)
            fd_sock->clients++;
    }
    return 1;
}

int mc_sock_read(TSystemState *fd_sock, int index) {

    int packet = 0;
    int i, j, n;
    unsigned char buf[CLIENT_RX_SIZE];


    n = read(fd_sock->fds[index].fd, buf, CLIENT_RX_SIZE);
    if (n == 0) {
        // нет данных! (вероятно клиент отключился)
        Log_Ex("err", LEVEL_ERR, (char *) "Network client ID = %d disconnected", index);
        fd_sock->Line[index].LineStatus = CLIENT_STATUS_OFF;
        close(fd_sock->fds[index].fd);

        return 0;
    } else
        if (n < 0) {
        printf("read process error. index = %d [read = %d]\n", index, n);
        printf("-------------------------\n");
        // ошибка на линии (вероятно клиент отключился)
        fd_sock->Line[index].LineStatus = CLIENT_STATUS_OFF;
        close(fd_sock->fds[index].fd);

        return 0;
    } else
        if (n > 0) {
        if ((fd_sock->buf[index].RxSize + n) < CLIENT_RX_SIZE) {
            memcpy(&fd_sock->buf[index].Rx[fd_sock->buf[index].RxSize], buf, n);
            fd_sock->buf[index].RxSize += n;
        } else {
            printf("ERR!!!...eth.Buffer Error. N = %d\n", fd_sock->buf[index].RxSize + n);
            fd_sock->buf[index].RxSize = 0;
        }
        packet = fd_sock->buf[index].RxSize;
    }

    return packet;
}

int mc_ethWrite(int fd, unsigned char * Buffer, int count) {
    int n;
    n = write(fd, Buffer, count);
    //    Log_DBG("DBG!!!...mc_ethWrite(0x%x, count=%d) -> %d", fd, count, n);
    return n;
}

int mc_sock_write(TSystemState *fd_sock, int index) {
    int n;
    n = mc_ethWrite(fd_sock->fds[index].fd, fd_sock->buf[index].Tx, fd_sock->buf[index].TxSize);
    fd_sock->buf[index].TxSize = 0;
    return n;
}

int mc_ethRead(int fd, unsigned char * RxBuffer, int RxBufferSize) {
    int readResult;

    readResult = read(fd, RxBuffer, RxBufferSize);

    if (readResult <= 0) {
        Log_DBG("******************************************************");
        Log_DBG("TCP Driver: Reading Error! code : %d. [fd = %d]", readResult, fd);
        Log_DBG("******************************************************");
        usleep(100 * 1000);
        //exit(1);
    }

    if (readResult < 0) {
        // Ошибка. 
        return 0;
    } else
        if (readResult == 0) {
        // Ошибка. 
    } else { //n > 0. Данные считаны из линии связи
    }

    return readResult;
}

int mc_ethReadEx(int fd, unsigned char * RxBuffer, int needSize, int timeOut) {
    struct pollfd fd4poll; // структра pollfd для организации ожидания ответа от шлюза
    int pollResult;
    int readCount;
    int size;
    unsigned char iRxBuffer[CLIENT_RX_SIZE];
    int iRxSize = 0;

    if (RxBuffer) {
        if (timeOut) {
            do {
                fd4poll.fd = fd;
                fd4poll.events = (POLLIN | POLLRDHUP);
                fd4poll.revents = 0;
                //Log_DBG("mc_ethReadEx[poll] -->  ");

                pollResult = poll(&fd4poll, 1, timeOut);

                //Log_DBG("mc_ethReadEx[poll] : %d", pollResult);

                if (pollResult > 0) {

                    readCount = mc_ethRead(fd, &iRxBuffer[iRxSize], CLIENT_RX_SIZE - iRxSize);
                    if (readCount) {
                        iRxSize = iRxSize + readCount;
                        // !!! Для отладки
                        if (iRxSize >= needSize) { // прием завершен
                            memcpy(RxBuffer, &iRxBuffer[0], iRxSize);
                            return iRxSize + LINK_RETURN_OK;
                        }
                    } else { // ошибка. 
                        Log_Ex("eth", LEVEL_INFO, (char *) "mc_ethRead: Read error.");
                        return LINK_RETURN_ERR_READ;
                    }
                } else
                    if (pollResult < 0) {
                    Log_Ex("err", LEVEL_ERR, (char *) "mc_ethRead. ERROR = %d", pollResult);
                    return LINK_RETURN_ERR_TIMEOUT;
                } else {
                    if (iRxSize) { // Выдаем сколько есть
                        memcpy(RxBuffer, &iRxBuffer[0], iRxSize);
                        return iRxSize + LINK_RETURN_OK;
                    }
                    return 0 + LINK_RETURN_OK;
                }
            } while (1);
        } else { // timeOut = 0
            if (needSize < 0)
                size = 0;
            else
                size = CLIENT_RX_SIZE;

            readCount = mc_ethRead(fd, &RxBuffer[0], size);
            if (readCount >= 0)
                return readCount + LINK_RETURN_OK;
            else
                return LINK_RETURN_ERR_READ;
        }

    }
    return LINK_RETURN_OK;


}

int mc_sock_printbuf(char * title, unsigned char *d, int size) {
#define RX_STR_LENGTH 60
    int i;
    char s[RX_STR_LENGTH + 10] = "";

    for (i = 0; i < size; i++)
        if (3 * (i + 1) < RX_STR_LENGTH)
            sprintf(s, "%s %.2X", s, d[i]);
        else {
            sprintf(s, "%s [...]", s);
            break;
        }
#ifdef DEBUG_TTY
    printf("\n%s>%s", title, s);
#endif

}

int mc_ethCallBack(char typeCallBack, void * ext, int clientIndex, unsigned char * Buffer, int count, unsigned char * RxBuffer, int *RxCount) {
    if (ext) {
        mc_sock_write((TSystemState*) ext, clientIndex);
    }
    return 0;
}

int mc_ClientConnect(unsigned int IP, unsigned int port) {
    //	struct sockaddr_in claddr;	// client socket structure address
    //	struct sockaddr_in addr4;

    if (TcpClients.Count >= MAX_TCP_CLIENT_COUNT) {
        Log_DBG("TcpClients[%d]>ERROR! Client too much...client limit : %d", TcpClients.Count, MAX_TCP_CLIENT_COUNT);
        return 0;
    }

    TTCP_CLIENT *pClient = &TcpClients.Client[TcpClients.Count];
    pClient->ClientStatus = CLIENT_STATUS_OFF;
    pClient->Sock = -1;

#if 0  // резервирую дискриптов для клиента
    int *pSock = &pClient->Sock;
    *pSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (*pSock < 0) {
        Log_DBG("*************************************");
        Log_DBG("!!!!!TCP CLIENT: Socket create error!");
        Log_DBG("*************************************");
        exit(1);
        return 0;
    }

    struct sockaddr_in *pAddr = &pClient->Address;
    bzero(pAddr, sizeof (*pAddr));
    pAddr->sin_family = AF_INET;
    pAddr->sin_addr.s_addr = htonl(IP);
    pAddr->sin_port = htons(port);


    if (connect(*pSock, (struct sockaddr *) pAddr, sizeof (*pAddr))) {

        Log_DBG("************************************************");
        Log_DBG("!!!!!TCP CLIENT: Connect Error! (IP = %d.%d.%d.%d)\n", (IP >> 24) & 0xFF, (IP >> 16) & 0xFF, (IP >> 8) & 0xFF, IP & 0xFF);
        Log_DBG("************************************************");
        exit(1);
        pClient->ClientStatus = CLIENT_STATUS_OFF;
    } else {
        Log_DBG("TCP CLIENT: Connect OK.");
        pClient->ClientStatus = CLIENT_STATUS_ON;
    }
#endif
    TcpClients.Count++;
    return TcpClients.Count;
}

int mc_ClientReconnect(TTCP_CLIENT *pClient, unsigned int IP, unsigned int port) {
    int *pSock = &pClient->Sock;
    if (*pSock >= 0) {
        close(*pSock);
        *pSock = -1;
    }
    *pSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (*pSock < 0) {
        Log_DBG("!!!!!TCP CLIENT: Socket create error!");
        return 0;
    }

    struct sockaddr_in *pAddr = &pClient->Address;
    bzero(pAddr, sizeof (*pAddr));
    pAddr->sin_family = AF_INET;
    pAddr->sin_addr.s_addr = htonl(IP);
    pAddr->sin_port = htons(port);


    if (connect(*pSock, (struct sockaddr *) pAddr, sizeof (*pAddr))) {
        Log_DBG("!!!!!TCP CLIENT: Connect Error! (IP = %d.%d.%d.%d)\n", (IP >> 24) & 0xFF, (IP >> 16) & 0xFF, (IP >> 8) & 0xFF, IP & 0xFF);
        pClient->ClientStatus = CLIENT_STATUS_OFF;
    } else {
        Log_DBG("TCP CLIENT: Connect OK.");
        pClient->ClientStatus = CLIENT_STATUS_ON;
    }

    return pClient->ClientStatus;
}

int mc_ClientInit(unsigned int IP, unsigned int port, char * extParam) {

    return mc_ClientConnect(IP, port);

}

static int getClientIndex(int fd) {
    if ((fd > 0) && (fd <= TcpClients.Count)) {
        return fd - 1;
    }
    Log_DBG("[TcpClients] FD ERROR! %d", fd);
    exit(1);
}

int mcTCPLink_CallBack(TMCExtDriver * event) {
    unsigned long IP;
    unsigned int port = 8080;
    struct in_addr ip_addr;
    int result;
    TTCP_CLIENT *pClient;
    struct pollfd fds [CLIENT_MAX];
    int i;

    if (event->id == MAC_ID_OPEN) { //TxBuffer - IP адрес для клиента. "" - для сервера
        // TCP Client Драйвера на старте не открываются! Только в ручную командой OPEN
        //RxBuffer - сетевой порт
        port = atoi(event->param.prOPEN.param); // в параметрах могут быть другие опции настройки
        inet_aton(event->param.prOPEN.device, &ip_addr);
        IP = htonl(ip_addr.s_addr);
        return mc_ClientInit(IP, port, event->param.prOPEN.param);
    }
    if (event->id == MAC_ID_WRITE_BUFFER) {
        pClient = &TcpClients.Client[getClientIndex(event->fd)];
        if (mc_ethWrite(pClient->Sock, event->param.prTX.inBuff, event->param.prTX.inCount) > 0) {
            return MAC_RESULT_OK;
        } else {
            Log_DBG("mc_ethWrite = MAC_RESULT_ERROR");
            return MAC_RESULT_ERROR;
        }

    }
    if (event->id == MAC_ID_READ_BUFFER) {
        //TxCount - используем как параметр timeOut
        pClient = &TcpClients.Client[getClientIndex(event->fd)];
        result = mc_ethReadEx(pClient->Sock, event->param.prRX.rxBuff, *event->param.prRX.rxCount, event->param.prRX.timeOut);
        *event->param.prRX.rxCount = result >= 0 ? result : 0;
        if (result >= 0) return MAC_RESULT_OK;
        else
            return MAC_RESULT_ERROR;
    }
    if (event->id == MAC_ID_POLL) {

        // событие возвращает кол-во дескрипторов по которым произошло событие поступления данных, 
        // или -1 в случае ошибки.
        pClient = &TcpClients.Client[getClientIndex(event->fd)];
        for (i = 0; i < event->param.prPOLL.in_fds_count; i++) {
            fds[i].events = event->param.prPOLL.in_fds[i].events;
            fds[i].fd = pClient->Sock;
            fds[i].revents = event->param.prPOLL.in_fds[i].revents;
        }
        result = poll(fds, event->param.prPOLL.in_fds_count, 5);
        if (result == -1) {
            usleep(1000 * 1000);
        }
        for (i = 0; i < event->param.prPOLL.in_fds_count; i++) {
            event->param.prPOLL.in_fds[i].revents = fds[i].revents;
            if ((fds[i].revents & POLLIN) == 0)
                usleep(1000 * 1000);
        }
        return result;
    }

    if (event->id == MAC_ID_CTRL) { 
        //event->prCTRL.ctrlCommand
        if (strcmp(event->param.prCTRL.ctrlCommand, "OPEN") == 0) {
            port = atoi(event->param.prCTRL.param); // в параметрах могут быть другие опции настройки
            inet_aton(event->param.prCTRL.device, &ip_addr);
            IP = htonl(ip_addr.s_addr);

            pClient = &TcpClients.Client[getClientIndex(event->fd)];

            return mc_ClientReconnect(pClient, IP, port);
        }

        return 0;
    }

    if (event->id == MAC_ID_TEST) {
        return 0;
    }
}

