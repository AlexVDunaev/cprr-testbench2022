#ifndef ETH_H
#define ETH_H

#include "../../main.h"

#define	LINK_RETURN_OK           0       //
#define	LINK_RETURN_ERR_TIMEOUT -1 // ошибка ожидания данных (функция poll)
#define	LINK_RETURN_ERR_READ    -2 // ошибка чтения данных
#define	LINK_RETURN_ERR_WRITE   -3 // ошибка записи данных


int mc_sock_init	 (TSystemState *fd_sock, int port,  char Protocol, char Line );
int mc_client_accept(TSystemState *fd_sock, int index, char Protocol, char Line );
int mc_sock_read	 (TSystemState *fd_sock, int index);
int mc_sock_write	 (TSystemState *fd_sock, int index);
int mc_sock_printbuf(char * title, unsigned char *d,int size);
int mc_ethCallBack  (char typeCallBack, void * ext, int  clientIndex, unsigned char * Buffer, int count,  unsigned char * RxBuffer, int *RxCount);

int mcTCPLink_CallBack(TMCExtDriver * event);
#endif
