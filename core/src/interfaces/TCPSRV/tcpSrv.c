#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "../../protocols/protocols.h"
#include "../../log/log.h"
#include "../../core/other.h"

// Драйвер TCP - соединений (серверных). Каждое соединение должно иметь уникальный
// порт, и не более TCPSRV_MAX_CONNECTION подключенных клиентов.
// Если трубуется отправить данные на запись, по данные получат все подключенные клиенты.
//------------------------------------------------------------------------------
#define  TCPSRV_MAX_SERVERS  10 // ограничение на использование каналов.
#define  TCPSRV_MAX_CONNECTION  10

#define  TCPSRV_SRSOCK_STATUS_OK  1  // серверный сокет открыт и не генерирует ошибок
#define  TCPSRV_SRSOCK_STATUS_ERR 0  // серверный сокет открыт но в ходе работы появились ошибки

#define         TCPSRV_CLIENT_STATUS_ON         1 //Клиент активен
#define         TCPSRV_CLIENT_STATUS_OFF       56 //Клиент отключен
#define  TCPSRV_CLIENT_STATUS_ERR 2 //Клиент возвращает ошибку (нужно пересоединиться)

#define         TCP_CLIENT_MAX                  5 //кол-во сетевых клиентов с которыми система будет работать


// состояние клиента на соките

typedef struct {
    unsigned int status; // стутус. актиный/отключен (fd - можно переписывать)
    unsigned int fd; // дискриптор клиента
    char ip[20]; // ip адрес для сетевых побключений

} TTCPSRV_CLIENT;

typedef struct {
    unsigned char count; // кол-во клиентов
    unsigned int listenp; // дискриптор сокета (listenp = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);)
    unsigned int port; // требуемый порт
    unsigned int status; // статус серв. сокета
    TTCPSRV_CLIENT client[TCPSRV_MAX_CONNECTION]; // максимальное число клиентов на одном серверном соките

} TTCPSRV_STATE;



unsigned char ts_ServersCount = 0; // кол-во открытых серверных сокетов
TTCPSRV_STATE * ts_ServersSockets[TCPSRV_MAX_SERVERS]; // 

int mcTCPSRV_Add(int port) {
    int qw;
    int i;
    struct sockaddr_in claddr; // client socket structure address
    struct sockaddr_in addr4;

    int listenp = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    bzero(&addr4, sizeof (addr4));
    addr4.sin_family = AF_INET;
    addr4.sin_addr.s_addr = htonl(INADDR_ANY);
    addr4.sin_port = htons(port);

    if (bind(listenp, (struct sockaddr *) &addr4, sizeof (addr4)) == -1) {
        Log_Ex("err", LEVEL_ERR, (char *) "[TCPSRV] Bind Error! Port:%d", port);
        return 0;
    } else {
        qw = listen(listenp, TCP_CLIENT_MAX);
    }

    if (qw)
        Log_Ex("err", LEVEL_ERR, (char *) "[TCPSRV]. listen error");
    else {

        i = ts_ServersCount++;
        ts_ServersSockets[i] = (TTCPSRV_STATE *) mallocEx(sizeof (TTCPSRV_STATE));
        ts_ServersSockets[i]->count = 0;
        ts_ServersSockets[i]->listenp = listenp;
        ts_ServersSockets[i]->port = port;
        ts_ServersSockets[i]->status = TCPSRV_SRSOCK_STATUS_OK;
        Log_DBG("[TCPSRV] Start OK on port: %d", port);
    }
    return listenp;
}

int mcTCPSRV_GetConnections(int ServersSocketIndex) {
    int i;
    int n = 0;
    if (ServersSocketIndex < ts_ServersCount) {
        for (i = 0; i < ts_ServersSockets[ServersSocketIndex]->count; i++) {
            if (ts_ServersSockets[ServersSocketIndex]->client[i].status != TCPSRV_CLIENT_STATUS_OFF) { //считаем только живых клиентов
                n++;
            }
        }
        return n;

    } else return -1;
}

int mcTCPSRV_client_accept(int listenp) {
#define TCPSRV_FD_APPEND        1
#define TCPSRV_FD_CHANGE        0

    socklen_t len; // server socket structure address
    char ip[100]; // ip адрес для сетевых побключений
    int i, n;
    int cl_index = -1; // индекс в структуре 
    int fd_client = 0;
    int cl_append = 1; //1: флаг добавлять дискриптор к концу или 0: заменить отпавший дискриптор
    struct sockaddr_in claddr; // client socket structure address
    TTCPSRV_CLIENT *a;

    for (n = 0; n < ts_ServersCount; n++)
        if (ts_ServersSockets[n]->listenp == listenp) {
            //---------------------------------------------------------
            // --- поиск клиентов с типом "CLIENT_STATUS_OFF"
            for (i = 0; i < ts_ServersSockets[n]->count; i++) {
                if (ts_ServersSockets[n]->client[i].status == TCPSRV_CLIENT_STATUS_OFF) {
                    cl_index = i;
                    cl_append = TCPSRV_FD_CHANGE;
                    break;
                }
            }
            //
            if (cl_index == -1)
                cl_index = ts_ServersSockets[n]->count;
            //            Log_DBG("DBG mcTCPSRV_client_accept: cl_index = %d", cl_index);
            if (cl_index >= TCPSRV_MAX_CONNECTION) { // превышено число соединений по порту...
                Log_Ex("err", LEVEL_ERR, (char *) "[TCPSRV] client too much. (Client limit = %d)", TCPSRV_MAX_CONNECTION);
            } else { // открываем соединение...
                a = &ts_ServersSockets[n]->client[cl_index];
                len = sizeof (claddr);
                fd_client = accept(listenp, (struct sockaddr *) &claddr, &len);
                // ----------------------------------------
                // SET: Non-blocking Socket
                // fcntl (fd_client, F_SETFL, O_NONBLOCK);
                // ----------------------------------------
                if (fd_client == -1) {
                    Log_Ex("err", LEVEL_ERR, (char *) "[TCPSRV] Error accept!");
                    return 0;
                } else {
                    inet_ntop(AF_INET, &claddr.sin_addr, ip, 100);
                    Log_DBG("[TCPSRV] Connections %d: Host IP :%s [index : %d]", mcTCPSRV_GetConnections(n) + 1, ip, cl_index);
                    a->fd = fd_client;
                    snprintf(a->ip, sizeof (a->ip), "%s", ip);
                    a->status = TCPSRV_CLIENT_STATUS_ON;
                    if (cl_append == TCPSRV_FD_APPEND)
                        ts_ServersSockets[n]->count++;
                }
            }

            break;
        }
    return fd_client;
}

int mcTCPSRV_ethWrite(int fd, unsigned char * Buffer, int count) {
    return send(fd, Buffer, count, MSG_DONTWAIT);
}

int mcTCPSRV_sock_write(int listenp, unsigned char * Buffer, int count) {
    int i, n;
    int result = 1;
    int wr_count = 0;
    TTCPSRV_CLIENT *a;

    result = count;
    for (n = 0; n < ts_ServersCount; n++)
        if (ts_ServersSockets[n]->listenp == listenp) {
            // нашли нужное соединение 
            // отправим данные всем клиентам
            for (i = 0; i < ts_ServersSockets[n]->count; i++) {
                if (ts_ServersSockets[n]->client[i].status != TCPSRV_CLIENT_STATUS_OFF) {
                    a = &ts_ServersSockets[n]->client[i];
                    wr_count = mcTCPSRV_ethWrite(a->fd, Buffer, count);
                    if (wr_count != count) {
                        Log_Ex("err", LEVEL_ERR, (char *) "[TCPSRV] Client ip = %s disconnected on write error [index : %d]", a->ip, i);
                        ts_ServersSockets[n]->client[i].status = TCPSRV_CLIENT_STATUS_OFF;
                        close(a->fd);
                        result = 0;
                    }
                } else {

                }
            }
            return result;
        }
}

int mcTCPSRV_sock_poll(struct pollfd *fds, int count, struct pollfd *fds_out) {
    int i, n;
    int result = 0;
    int fds_num = 0;
    struct pollfd fds_poll[TCPSRV_MAX_CONNECTION]; //дискрипторы клиентов
    int sock_index;
    int client_index[TCPSRV_MAX_CONNECTION]; //индекс клиентов для соответствия с fds_poll

    if (count == 1) {
        for (n = 0; n < ts_ServersCount; n++)
            if (ts_ServersSockets[n]->listenp == fds->fd) {
                // нашли нужное соединение 
                // отправим данные всем клиентам
                sock_index = n;
                // разместим дискриптор серверного сокета для возможности подключения клиентов
                fds_poll[0].revents = 0;
                fds_poll[0].fd = ts_ServersSockets[n]->listenp;
                fds_poll[0].events = (POLLIN | POLLRDHUP); //POLLHUP + POLLERR
                client_index[0] = -1;
                fds_num = 1;
                //-------------------------------------------------------
                for (i = 0; i < ts_ServersSockets[n]->count; i++) {
                    if (ts_ServersSockets[n]->client[i].status != TCPSRV_CLIENT_STATUS_OFF) {
                        fds_poll[fds_num].revents = 0;
                        fds_poll[fds_num].fd = ts_ServersSockets[n]->client[i].fd;
                        fds_poll[fds_num].events = (POLLIN | POLLRDHUP); //POLLHUP + POLLERR
                        client_index[fds_num] = i;
                        fds_num++;
                    }
                }
            }
        if (fds_num) {
            // возврат poll: -1 Ошибка, иначе кол-во fds_poll.revents > 0
            result = poll(fds_poll, fds_num, 5);
            if (result) {
                // анализ результата, на предмет закрытия соединения и подключения новых клиентов
                for (n = 0; n < fds_num; n++) {
                    if (fds_poll[n].revents & (POLLRDHUP)) {
                        // произошло закрытие соединения...
                        close(fds_poll[n].fd);
                        i = client_index[n];
                        if (i == -1) {
                            Log_Ex("err", LEVEL_ERR, (char *) "[TCPSRV] Server detect POLLRDHUP");
                        } else {
                            ts_ServersSockets[sock_index]->client[i].status = TCPSRV_CLIENT_STATUS_OFF;
                            Log_Ex("err", LEVEL_ERR, (char *) "[TCPSRV] Client ip = %s disconnected ! ", ts_ServersSockets[sock_index]->client[i].ip);
                            result--;
                        }
                    }
                    if (fds_poll[n].revents & (POLLNVAL)) { // не верный дискриптор! на всякий случай предусмотим. т.к. пользователь может передать любое значение...
                        i = client_index[n];
                        if (i == -1) {
                            Log_Ex("err", LEVEL_ERR, (char *) "[TCPSRV] Server detect POLLNVAL");
                        } else {
                            ts_ServersSockets[sock_index]->client[i].status = TCPSRV_CLIENT_STATUS_OFF;
                            Log_Ex("err", LEVEL_ERR, (char *) "[TCPSRV] POLL Invalid file descriptor (%d). connection %d", fds_poll[n].fd, i);
                        }
                    }
                }
                if (fds_poll[0].revents & (POLLIN)) {
                    // есть подключение нового клиента
                    mcTCPSRV_client_accept(fds_poll[0].fd);
                }
                // сформируем выходную таблицу дискрипторов для того, что бы 
                // ядро смогло вызвать событие чтения по конкретному каналу
                if (fds_out != NULL) {
                    result = 0;
                    for (n = 1; n < fds_num; n++)
                        if (fds_poll[n].revents & (POLLIN)) {
                            memcpy(&fds_out[result], &fds_poll[n], sizeof (fds_poll[n]));
                            result++;
                        }
                }
            }
        }
    } else {
        Log_DBG("ERR! mcTCPSRV_sock_poll(count <> 1). unsupported");
    }
    return result;
}

int mcTCPSRV_ethRead(int fd, unsigned char * RxBuffer, int RxBufferSize) {
    int readResult;
    readResult = read(fd, RxBuffer, RxBufferSize);
    if (readResult < 0) {
        Log_DBG("ethSRVRead Error (%d). fd = %d", readResult, fd);
    } else
        if (readResult == 0) {
        // Ошибка. 
        Log_DBG("ethSRVRead Error (%d). fd = %d", readResult, fd);
    } else { //n > 0. Данные считаны из линии связи
    }
    return readResult;
}

int mcTCPSRV_sock_read(int Linefd, int Clientfd, unsigned char * RxBuffer, int RxMaxSize) {

    int i;
    int n;
    int result = 0;
    TTCPSRV_CLIENT *a;

    for (n = 0; n < ts_ServersCount; n++)
        if (ts_ServersSockets[n]->listenp == Linefd)
            for (i = 0; i < ts_ServersSockets[n]->count; i++)
                if (ts_ServersSockets[n]->client[i].status != TCPSRV_CLIENT_STATUS_OFF)
                    if (ts_ServersSockets[n]->client[i].fd == Clientfd) {
                        a = &ts_ServersSockets[n]->client[i];
                        result = mcTCPSRV_ethRead(Clientfd, RxBuffer, RxMaxSize);
                        if (result <= 0) {
                            Log_Ex("err", LEVEL_ERR, (char *) "[TCPSRV] Client ip = %s disconnected on read error", a->ip);
                            a->status = TCPSRV_CLIENT_STATUS_OFF;
                            close(Clientfd);
                            result = 0;
                        } else {
                            // чтение без ошибок...
                        }
                    }
    return result;
}

int mcTCPSRV_CallBack(TMCExtDriver * event) {
    unsigned long IP;
    unsigned int port = 8080;
    struct in_addr ip_addr;
    int result;

    if (event->id == MAC_ID_OPEN) { //TxBuffer - IP адрес для клиента. "" - для сервера
        //RxBuffer - сетевой порт
        port = atoi(event->param.prOPEN.param); // в параметрах могут быть другие опции настройки
        inet_aton(event->param.prOPEN.device, &ip_addr);
        IP = htonl(ip_addr.s_addr);
        if (strlen(event->param.prOPEN.device)) { // клиент
            Log_DBG("[TCPSRV] ERROR. Request to creation client socket. IP = %s", event->param.prOPEN.device);

        } else { // серверный сокет
            return mcTCPSRV_Add(port);
        }
    }
    if (event->id == MAC_ID_WRITE_BUFFER) {
        if (mcTCPSRV_sock_write(event->fd, event->param.prTX.inBuff, event->param.prTX.inCount))
            return MAC_RESULT_OK;
        else
            return MAC_RESULT_ERROR;
    }
    if (event->id == MAC_ID_READ_BUFFER) {
        result = mcTCPSRV_sock_read(event->fd, event->param.prRX.fdpoll, event->param.prRX.rxBuff, 4096);
        *event->param.prRX.rxCount = result;
        if (result)
            return MAC_RESULT_OK;
        else
            return MAC_RESULT_ERROR;
    }
    if (event->id == MAC_ID_CTRL) {//TxBuffer - имя свойства
        //RxBuffer - параметры (если нужно)
        Log_DBG("[TCPSRV](MAC_ID_CTRL): %s", event->param.prCTRL.ctrlCommand);
        return 0;
    }

    if (event->id == MAC_ID_POLL) {
        // событие возвращает кол-во дискрипторов по которым произошло событие поступления данных, 
        // или -1 в случае ошибки.
        result = mcTCPSRV_sock_poll(event->param.prPOLL.in_fds, event->param.prPOLL.in_fds_count, event->param.prPOLL.out_fds);
        return result;
    }

    if (event->id == MAC_ID_TEST) {
        return 0;
    }

    return 0; // ошибка. событие не потдерживается..
}

