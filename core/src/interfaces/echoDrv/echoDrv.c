#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "../../protocols/protocols.h"
#include "../../log/log.h"

#define		ECHO_MAX_CONNECTION		10

#define		ECHO_TX_BUFFER_SIZE		255

typedef struct {
    int  delayOption;
    char reqFlag; // Флаг того что была выдана команда
    char txBuffer[ECHO_TX_BUFFER_SIZE];
    int txSize;
} TECHO_CLIENT;

typedef struct {
    //    unsigned char count; // кол-во клиентов
    TECHO_CLIENT client[ECHO_MAX_CONNECTION]; // максимальное число клиентов 
} TECHO_STATE;



unsigned char echoInstanceCount = 0; // кол-во открытых серверных-null сокетов
TECHO_STATE echoState; // 

int mcECHO_Add(int delayOption) {
    echoState.client[echoInstanceCount].reqFlag = 0;
    echoState.client[echoInstanceCount].txSize = 0;
    echoState.client[echoInstanceCount].delayOption = delayOption;
    Log_DBG("[ECHO%d] Max delay = %d. Start OK", echoInstanceCount, delayOption);
    echoInstanceCount++;
    return (echoInstanceCount - 1) | 0x100;
}

int mcECHO_CallBack(TMCExtDriver * event) {
    int result;
    int i;
    int vIndex = 0;
    int waitAnswerBlockCount = 0;
    float rndDelay = 0;
    int delayOption = 0;


    //Log_DBG("mcTCPSRV_CallBack(typeCallBack = %d)", typeCallBack);

    if (event->id == MAC_ID_OPEN) {
        if (event->param.prOPEN.param != NULL) 
        {
            delayOption = atoi(event->param.prOPEN.param);
        }
        return mcECHO_Add(delayOption);
    }
    if (event->id == MAC_ID_WRITE_BUFFER) {
        vIndex = event->fd & 0xFF;
        if ((vIndex >= 0)&&(vIndex < echoInstanceCount)) {
            echoState.client[vIndex].reqFlag = 1;
            i = event->param.prTX.inCount;
            //event->prTX.outBuff[0] = 1;
            //*event->prTX.outBuffCount = 1;
            
            if (i > ECHO_TX_BUFFER_SIZE) i = ECHO_TX_BUFFER_SIZE;
            echoState.client[vIndex].txSize = i;
            memcpy(echoState.client[vIndex].txBuffer, event->param.prTX.inBuff, i);
        }
        return MAC_RESULT_OK;
    }

    if (event->id == MAC_ID_READ_BUFFER) {
        vIndex = event->param.prRX.fdpoll & 0xFF;
        if ((vIndex >= 0)&&(vIndex < echoInstanceCount)) {
        if (echoState.client[vIndex].txSize) {
            memcpy(event->param.prRX.rxBuff, echoState.client[vIndex].txBuffer, echoState.client[vIndex].txSize);
        }
        *event->param.prRX.rxCount = echoState.client[vIndex].txSize;
        return MAC_RESULT_OK;
        }
        else
        {
        return MAC_RESULT_ERROR;
        }
    }
    if (event->id == MAC_ID_CTRL) {//TxBuffer - имя свойства
        //RxBuffer - параметры (если нужно)
        return 0;
    }

    if (event->id == MAC_ID_POLL) {
        vIndex = event->fd & 0xFF;
        if ((vIndex >= 0)&&(vIndex < echoInstanceCount)) {
            if (echoState.client[vIndex].reqFlag) {
                echoState.client[vIndex].reqFlag = 0;
                event->param.prPOLL.out_fds[waitAnswerBlockCount].fd = vIndex | 0x100;
                event->param.prPOLL.out_fds[waitAnswerBlockCount].revents = POLLIN;
                waitAnswerBlockCount++;
            }
        rndDelay = echoState.client[vIndex].delayOption * 10 * (random() % 100);
        }
        
//        srand(time(NULL));
//        rndDelay = rand() % 20;
//       Log_DBG("[ECHO] DBG rndDelay = %f", rndDelay);
        usleep((int)(rndDelay)); // 0..delayOption мс
        return waitAnswerBlockCount;
    }

    if (event->id == MAC_ID_TEST) {
        return 0;
    }

    return 0; // ошибка. событие не потдерживается..
}

