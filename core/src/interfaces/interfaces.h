/* 
 * Author: Dunaev
 *
 */

#ifndef INTERFACES_H
#define	INTERFACES_H

#include "../core/coreUInterface.h"

void    initLinkDrivers(TSystemState * state);
int     ifuncLinkDrivers(TMACRec * mac, TLineRec * line, TMCExtDriver * event);


#endif	

