#ifndef UDPDRV_H
#define UDPDRV_H

#include "../../main.h"

#define	LINK_RETURN_OK           0       //
#define	LINK_RETURN_ERR_TIMEOUT -1 // ошибка ожидания данных (функция poll)
#define	LINK_RETURN_ERR_READ    -2 // ошибка чтения данных
#define	LINK_RETURN_ERR_WRITE   -3 // ошибка записи данных


int mcUDPClient_CallBack(TMCExtDriver * event);
#endif
