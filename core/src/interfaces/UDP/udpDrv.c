#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "../../protocols/protocols.h"
#include "../../log/log.h"
#include "../../core/other.h"


#define MAX_UDP_CLIENT_COUNT  20

#define UDP_RXBUFFER_SIZE     1000

typedef struct {
    int Sock;
    struct sockaddr_in Address;
    //struct sockaddr Address;
    char ClientStatus; //статус клиента: Отключен/Включен
    char * rxbuffer; // буфер для приема данных
    int rxSize; // размер принятых данных

} TUDP_CLIENT;

typedef struct {
    unsigned int Count;
    TUDP_CLIENT Client[MAX_UDP_CLIENT_COUNT];


} TUDP_CLIENTS;

static TUDP_CLIENTS UDPClients = {.Count = 0};

static int getClientIndex(int fd) {
    if ((fd > 0) && (fd <= UDPClients.Count)) {
        return fd - 1;
    }
    Log_DBG("[UDPClients] FD ERROR! %d", fd);
    exit(1);
}

int mcUDPClientWrite(int index, unsigned char * Buffer, int count) {
    int n;
    int fd;
    if ((index >= 0)&&(index < MAX_UDP_CLIENT_COUNT)) {
        fd = UDPClients.Client[index].Sock;
        n = sendto(fd, Buffer, count, 0, (struct sockaddr *) &UDPClients.Client[index].Address, sizeof (UDPClients.Client[index].Address));
        return n;
    } else {
        Log_DBG("ERR!!!...mcUDPClientWrite: index error(%d)", index);
        exit(1);
    }
}

int mcUDPClientRead(int index, unsigned char * RxBuffer, int RxBufferSize) {
    int readResult;
    int fd;

    if ((index >= 0)&&(index < MAX_UDP_CLIENT_COUNT)) {
        fd = UDPClients.Client[index].Sock;
        readResult = recv(fd, RxBuffer, RxBufferSize, 0);

        if (readResult < 0) {
            Log_DBG("mcUDPClientRead Error (%d). fd = %d", readResult, fd);
        } else
            if (readResult == 0) {
            // Ошибка. 
            Log_DBG("mcUDPClientRead Error (%d). fd = %d", readResult, fd);
        } else { //n > 0. Данные считаны из линии связи
        }

        return readResult;
    } else {
        Log_DBG("ERR!!!...mcUDPClientRead: index error(%d)", index);
        exit(1);
    }
}

int mcUDPClientReadEx(int fd, unsigned char * RxBuffer, int needSize, int timeOut) {
    Log_DBG("!!!!!mcUDPClientReadEx: error!");
    return -1;
}

int mc_UDPClientInit(unsigned int IP, unsigned int port, char * extParam) {

    int nTimeVar = 0;
    struct timeval timeOutVar = {0};

    if (UDPClients.Count >= MAX_UDP_CLIENT_COUNT) {
        Log_DBG("UDPClients[%d]>ERROR! Client too much...client limit : %d", UDPClients.Count, MAX_UDP_CLIENT_COUNT);
        return 0;
    }

    TUDP_CLIENT *pClient = &UDPClients.Client[UDPClients.Count];


    pClient->Sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (pClient->Sock < 0) {
        Log_DBG("!!!!!UDP CLIENT: Socket create error!");
        return 0;
    }

    bzero(&pClient->Address, sizeof (pClient->Address));
    pClient->Address.sin_family = AF_INET;
    pClient->Address.sin_addr.s_addr = htonl(IP);
    pClient->Address.sin_port = htons(port);


    Log_DBG("UDP CLIENT: IP = %d.%d.%d.%d : %d", (IP >> 24) & 0xFF, (IP >> 16) & 0xFF, (IP >> 8) & 0xFF, IP & 0xFF, port);
    if (connect(pClient->Sock, (struct sockaddr *) &pClient->Address, sizeof (pClient->Address)) < 0) {
        //bind(*pSock, (struct sockaddr *) pAddr, sizeof (*pAddr))) {
        Log_DBG("!!!!!UDP CLIENT: connect Error!");
        pClient->ClientStatus = CLIENT_STATUS_OFF;
    } else {
        Log_DBG("UDP CLIENT: connect OK.");
        pClient->ClientStatus = CLIENT_STATUS_ON;
    }

    // Timeout checking
    nTimeVar = setsockopt(pClient->Sock, SOL_SOCKET, SO_RCVTIMEO,
            (struct timeval *) &timeOutVar,
            sizeof (struct timeval));

    if (0 > nTimeVar) {
        Log_DBG("!!!!!UDP CLIENT: Timeout value Error!");
        close(pClient->Sock);
        exit(1);
    }
    pClient->rxbuffer = (char *) mallocEx(UDP_RXBUFFER_SIZE);
    pClient->rxSize = 0;

    // mcUDPClientWrite(UDPClients.Count, "*IDN?", 5);

    UDPClients.Count++;
    return UDPClients.Count;



}

int mcUDPClient_CallBack(TMCExtDriver * event) {

    unsigned long IP;
    unsigned int port = 8080;
    struct in_addr ip_addr;
    int rxSize;
    int index;
    TUDP_CLIENT *pClient;


    if (event->id == MAC_ID_OPEN) { //TxBuffer - IP адрес 
        //RxBuffer - сетевой порт
        port = atoi(event->param.prOPEN.param); // в параметрах могут быть другие опции настройки
        inet_aton(event->param.prOPEN.device, &ip_addr);
        IP = htonl(ip_addr.s_addr);

        if (strlen(event->param.prOPEN.device)) { // клиент

        } else { 
            
        }
        return mc_UDPClientInit(IP, port, event->param.prOPEN.param);
    }

    index = getClientIndex(event->fd);

    if (event->id == MAC_ID_WRITE_BUFFER) {
        if (mcUDPClientWrite(index, event->param.prTX.inBuff, event->param.prTX.inCount) > 0) {
            return MAC_RESULT_OK;
        } else {
            Log_DBG("mc_ethWrite = MAC_RESULT_ERROR");
            return MAC_RESULT_ERROR;
        }

    }
    if (event->id == MAC_ID_READ_BUFFER) {
        // буфер сформирован при событии POLL
        pClient = &UDPClients.Client[getClientIndex(event->fd)];
        if (pClient->rxSize) {
            memcpy(event->param.prRX.rxBuff, pClient->rxbuffer, pClient->rxSize);
            *event->param.prRX.rxCount = pClient->rxSize;
            pClient->rxSize = 0;
        } else {
            *event->param.prRX.rxCount = 0;
        }
        return MAC_RESULT_OK;
    }
    if (event->id == MAC_ID_POLL) {
        //
        pClient = &UDPClients.Client[index];

        if (event->param.prPOLL.in_fds_count != 1) {
            Log_DBG("ERR!!!...POLL fds[] num != 1! (%d)", event->param.prPOLL.in_fds_count);
            exit(1);
        }

        rxSize = mcUDPClientRead(index, pClient->rxbuffer, UDP_RXBUFFER_SIZE);
        // Log_DBG("DBG!!!...mcUDPClientRead : %d", rxSize);
        if (rxSize > 0) {
            event->param.prPOLL.in_fds[0].revents = POLLIN;
            pClient->rxSize = rxSize;
        } else { // Ошибка!
            event->param.prPOLL.in_fds[0].revents = POLLERR;
        }

        return 1; // Возврат: кол-во дискрипторов по которым произошло событие. Всегда 1.

    }

    if (event->id == MAC_ID_CTRL) {
        return 0;
    }

    if (event->id == MAC_ID_TEST) {
        return 0;
    }



}
