#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "../../protocols/protocols.h"
#include "../../log/log.h"

#define		NULL_MAX_CONNECTION		10

typedef struct {
    char reqFlag; // Флаг того что была выдана команда

} TNULL_CLIENT;

typedef struct {
    //    unsigned char count; // кол-во клиентов
    TNULL_CLIENT client[NULL_MAX_CONNECTION]; // максимальное число клиентов 
} TNULL_STATE;



unsigned char nullInstanceCount = 0; // кол-во открытых серверных-null сокетов
TNULL_STATE nullState; // 

int mcNULL_Add(int port) {
    nullState.client[nullInstanceCount].reqFlag = 0;
    Log_DBG("[NULL%d] Start OK", nullInstanceCount);
    nullInstanceCount++;
    return (nullInstanceCount - 1) | 0x100;
}

int mcNULL_CallBack(TMCExtDriver * event) {
    int result;
    int i;
    int vIndex = 0;
    int waitAnswerBlockCount = 0;


    //Log_DBG("mcTCPSRV_CallBack(typeCallBack = %d)", typeCallBack);

    if (event->id == MAC_ID_OPEN) {
        return mcNULL_Add(0);
    }
    if (event->id == MAC_ID_WRITE_BUFFER) {
        vIndex = event->fd & 0xFF;
        if ((vIndex >= 0)&&(vIndex < nullInstanceCount)) {
            nullState.client[vIndex].reqFlag = 1;
        }
        return MAC_RESULT_OK;
    }

    if (event->id == MAC_ID_READ_BUFFER) {
        event->param.prRX.rxBuff[0] = 0;
        *event->param.prRX.rxCount = 0;
        return MAC_RESULT_OK;
    }
    if (event->id == MAC_ID_CTRL) {//TxBuffer - имя свойства
        //RxBuffer - параметры (если нужно)
        return 0;
    }

    if (event->id == MAC_ID_POLL) {
        usleep(1000 * 1);
        return 0;
    }

    if (event->id == MAC_ID_TEST) {
        return 0;
    }

    return 0; // ошибка. событие не потдерживается..
}

