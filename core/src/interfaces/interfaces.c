#include "../core/commandUnit.h"
#include "../main.h"
#include "../log/log.h"
#include "eth/eth.h"
#include "uart/ttys.h"
#include "TCPSRV/tcpSrv.h"
//#include "nullDrv/nullDrv.h"
// #include "echoDrv/echoDrv.h"
#include "UDP/udpDrv.h"
#include <string.h>

extern TSystemState SystemState;
//

void initLinkDrivers(TSystemState * state) {
    //char source, char * DriverName, char * DriverParams, void * proc)
    AppendLinkDriver(state, OBJ_SOURCE_EMBEDDED, "COM", "", p232CallBack, MAC_ID_MASK_ALL);
    AppendLinkDriver(state, OBJ_SOURCE_EMBEDDED, "TCP", "", mcTCPLink_CallBack, MAC_ID_MASK_ALL);
    AppendLinkDriver(state, OBJ_SOURCE_EMBEDDED, "UDP", "", mcUDPClient_CallBack, MAC_ID_MASK_ALL);
    AppendLinkDriver(state, OBJ_SOURCE_EMBEDDED, "TCPSRV", "", mcTCPSRV_CallBack, MAC_ID_MASK_ALL);
    // AppendLinkDriver(state, OBJ_SOURCE_EMBEDDED, "NULL", "", mcNULL_CallBack, MAC_ID_MASK_ALL);

}

// LinkEntryPoint())

int ifuncLinkDrivers(TMACRec * mac, TLineRec * line, TMCExtDriver * event) {
    int i;
    int result;

    if (strcmp(mac->name, "COM") == 0) {
        return p232CallBack(event);
    } else
        if (strcmp(mac->name, "TCP") == 0) {
        //        a->source
        return mcTCPLink_CallBack(event);
    } else
        if (strcmp(mac->name, "UDP") == 0) {
        return mcUDPClient_CallBack(event);
    } else
        if (strcmp(mac->name, "TCPSRV") == 0) {
        result = mcTCPSRV_CallBack(event);
        return result;

        //        a->source
    } else {// поищем во всем массиве драйверов
        for (i = 0; i < SystemState.MAC.count; i++)
            if (strcmp(mac->name, SystemState.MAC.Drivers[i]->name) == 0) {
                if ((SystemState.MAC.Drivers[i]->source == OBJ_SOURCE_USER) ||
                        (SystemState.MAC.Drivers[i]->source == OBJ_SOURCE_EMBEDDED)) {
                    result = SystemState.MAC.Drivers[i]->callBackExt(event);
                    //Log_DBG("DBG. User DRV [%s]: id = %d.  result = %d", SystemState.MAC.Drivers[i]->name, event->id, result);
                    return result;
                }

            }
        return 0;
    }

}
