/**
 * @file  cprr24Streamer.c 
 * @brief Модуль поддержки режима стриминга радара CPRR
 * 
 * Радар поддерживает разновидности стриминга: 
 * - стриминг сырых данных;
 * - стриминг сырых данных из архива (до 200 последних кадров);
 * - передача конфигурации в формате стриминга;
 * - нештатные режимы стриминга.
 * ПИВ: https://drive.google.com/file/d/19l-8i_hIdwiCSrkh0TXVCMn-uLWAFQwS/view
 * РП: https://clnas.cognitivepilot.com/index.php/s/w49j7ZkWb4Jkkie?
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cprr24Streamer.h"

#if 1
// Вывод отладочной информации
#    define LOG_DBG(...) Log_DBG(__VA_ARGS__)
void Log_DBG(const char * text, ...);
#else
#    define LOG_DBG(...) 
#endif

// Двойная буферизация
static char * streamerBuffer[2] = {NULL, NULL};
static TStreamerThreadData streamerThreadData = {.initDone = 0};

/**
 * @brief callback. Вызывается, когда получен полный кадр в режиме стриминга
 * 
 * Используется в udpStreamerListener_thread
 * Цель обработчика события сохранить указатель на данные и размер (данных и заголовка).
 * и установить семафор, для пробуждения потока обработки данных. 
 * @param pkt - буфер кадр, указывает на начало заголовка
 * @param frameSize - полный размер кадра.
 * @param HeaderSize - размер поля заголовка
 */
static void udpStreamerFrameRxEvent(void * pkt, uint32_t frameSize, uint32_t HeaderSize) {
    // Установить параметры буфера и пробудить поток обработки 
    streamerThreadData.writerDataLock = 1;
    streamerThreadData.writerData.pkt = pkt;
    streamerThreadData.writerData.frameSize = frameSize;
    streamerThreadData.writerData.HeaderSize = HeaderSize;
    streamerThreadData.writerDataLock = 0;
    sem_post(&streamerThreadData.frameReadySemaphore);
}


/**
 * @brief Установка приоритета потока
 * 
 */
static int udpStreamerThreadSetPriority(pthread_attr_t * tattr, int priority) { //

    int ret = -1;
    struct sched_param param;

    /* initialized with default attributes */
    if (pthread_attr_init(tattr) == 0) {
        /* safe to get existing scheduling param */
        if (pthread_attr_getschedparam(tattr, &param) == 0) {
            /* set the priority; others are unchanged */
            param.sched_priority = priority;
            /* setting the new scheduling param */
            ret = pthread_attr_setschedparam(tattr, &param);
        }
    }
    return ret;
}

/**
 * @brief Поток обработки кадра данных стриминга
 * 
 * Поток находится в ожидании семафора, при его срабатывании вызывается пользовательский обработчик
 */
void* fileStreamerWriter_thread(void * arg) {
    int sem_result;
    volatile TStreamerThreadData * data = arg;
    TStreamerWriterData writerData = {.HeaderSize = 0, .frameSize = 0};
    char * buffer = (char *) malloc(CPRR_STREAM_FRAME_MAX_SIZE);
    if (buffer) {
        while (data->needStop == 0) {
            sem_result = sem_wait((sem_t *)&data->frameReadySemaphore);
            // LOG_DBG("[DBG!!!] sem_wait(%d) -> ", sem_result);
            if (data->needStop == 0) {
                if (streamerThreadData.writerDataLock) {
                    LOG_DBG("[CPRR DRIVER (Writer)] [DataLocked!!!]");
                }
                writerData = streamerThreadData.writerData;
                //-------------------------------------------
                uint32_t frameSize = writerData.frameSize; // полный размер
                uint32_t HeaderSize = writerData.HeaderSize; // размер заголовка
                //LOG_DBG("[memcpy] %p, %p, %d", buffer, writerData.pkt, frameSize);
                if (frameSize < CPRR_STREAM_FRAME_MAX_SIZE) {
                    memcpy(buffer, writerData.pkt, frameSize);
                } else {
                    LOG_DBG("[CPRR DRIVER (Writer)] Error frameSize : %d", frameSize);
                }
                //LOG_DBG("[CPRR DRIVER (Writer)] frameSize: %d, HeaderSize : %d", frameSize, HeaderSize);

                if (frameSize + HeaderSize > 0)
                    if (data->event) {
                        //LOG_DBG("[DBG!!!] data->event(%p, %d, %d) -> ", buffer, frameSize, HeaderSize);
                        int writeDone = data->event(CPRR24_EVENT_FRAME_DONE, buffer, frameSize, HeaderSize);
                        //LOG_DBG("[DBG!!!] -> END", buffer, frameSize, HeaderSize);
                    }
                //-------------------------------------------
                int sem_value = 0;
                sem_result = sem_getvalue((sem_t *)&data->frameReadySemaphore, &sem_value);
                if ((!sem_result)&&(sem_value)) {
                    LOG_DBG("[CPRR DRIVER] Skip frames : %d", sem_value);
                    for (int i = 0; i <sem_value; i++) {
                        //LOG_DBG("[CPRR DRIVER (Writer)] [sem[%d] : clear ... ", i);
                        sem_wait((sem_t *)&data->frameReadySemaphore);
                    }
                }
            }
        }
        free(buffer);
    }
    //LOG_DBG("[CPRR DRIVER (fileStreamerWriter_thread)] EXIT");
    return NULL;
}

static void* udpStreamerListener_thread(void * arg) {
    volatile TStreamerThreadData * data = arg;
    ssize_t readResult;
    char * receiveBuff;
    TStreamerPktBuffer * pktHeader = NULL;
    // Размер служебных данных в кадре
    uint32_t curFrameHeaderSize = 0;
    uint32_t curFrameRxSize = 0;
    int32_t curFrameTailSize = 0;
    char * frameBuffer;
    uint32_t frameCount = 0;
    uint32_t dbgPktCount = 0;
    char isPreambleError = 0;

    // streamerBuffer - буфер для сохранения двух кадров (с одним работаем, другой пишем)
    streamerBuffer[0] = (char *) malloc(CPRR_STREAM_FRAME_MAX_SIZE);
    streamerBuffer[1] = (char *) malloc(CPRR_STREAM_FRAME_MAX_SIZE);
    if ((streamerBuffer[0] == NULL) || (streamerBuffer[1] == NULL)) {
        data->needStop = 1;
        LOG_DBG("[CPRR DRIVER (Listener)] Memory Error!\n");
    }

    while (data->needStop == 0) {
        // Один буфер для заполнения, второй для работы
        frameBuffer = streamerBuffer[frameCount & 1];
        if ((curFrameRxSize + CPRR_UDP_PKT_MAX_SIZE) >= CPRR_STREAM_FRAME_MAX_SIZE) {
            LOG_DBG("[CPRR DRIVER (Listener)] Buffer overflow!!! (curFrameRxSize : %d)\n", curFrameRxSize);
            curFrameRxSize = 0;
        }
        receiveBuff = &frameBuffer[curFrameRxSize];
        pktHeader = (TStreamerPktBuffer *) receiveBuff;
        
        size_t receiveSize = CPRR_UDP_PKT_MAX_SIZE;
        if ((curFrameTailSize)&&(curFrameTailSize < CPRR_UDP_PKT_MAX_SIZE)) {
            receiveSize = curFrameTailSize;
        }
        readResult = recv(data->sock, receiveBuff, receiveSize, 0);
        //readResult = recvfrom(data->sock, receiveBuff, receiveSize, 0, (struct sockaddr *) NULL, NULL);
        dbgPktCount++;
        if (readResult > 0) {
            if (readResult > 1400) {
                LOG_DBG("[CPRR DRIVER (Listener)] readResult : %d", readResult);
            }
            
            if (pktHeader->Preamble == RADAR_PKT_PREAMBLE) {
                if (curFrameRxSize) {
                    LOG_DBG("[CPRR DRIVER (Listener)] PREAMBLE DETECT! IGNORE BUFFER!  Size: %d, TailSize: %d, (count:%d)\n", curFrameRxSize, curFrameTailSize, dbgPktCount);
                }
                curFrameRxSize = 0;
                curFrameTailSize =0;
            }
            
            
            if (curFrameRxSize) {
                // Данные кадра
                curFrameRxSize += readResult;
#if 0 //// Работа с тех. прошивкой, у которой в каждом пакете есть счетчик (i32)
                static uint32_t dbgPktInBuildCount = 0;
                uint32_t * PktCount = (uint32_t *) & receiveBuff[0];
                dbgPktInBuildCount++;
                if (dbgPktInBuildCount != *PktCount) {
                    LOG_DBG("[CPRR DRIVER (Listener)] ----> [count : %d, radar: %d] Delta: %d", dbgPktInBuildCount, *PktCount, *PktCount - dbgPktInBuildCount);
                    dbgPktInBuildCount = *PktCount;
                }
#endif                
                if (readResult > curFrameTailSize) {
                    // Ошибка. Размер принятых данных не соответствует ожиданию
                    LOG_DBG("[CPRR DRIVER (Listener)] ------------- TailSize: %d, readResult: %d ------------------ ", curFrameTailSize, readResult);
                    uint32_t * Preamble = (uint32_t *) & frameBuffer[curFrameRxSize - (readResult - curFrameTailSize)];
                    LOG_DBG("[CPRR DRIVER (Listener)] ----> 0x%.8X", *Preamble);
                    readResult = curFrameTailSize;

                }

                curFrameTailSize -= readResult;
                if (curFrameTailSize <= 0) {
                    // полный кадр принят
                    udpStreamerFrameRxEvent(frameBuffer, curFrameRxSize, curFrameHeaderSize);
                    curFrameRxSize = 0;
                    curFrameTailSize = 0;
                    frameCount++;
                }
            } else {
                // Заголовок [ + Данные кадра]
                if (pktHeader->Preamble == RADAR_PKT_PREAMBLE) {
                    isPreambleError = 0;
                    dbgPktCount = 0;
                    if (pktHeader->Type == 10) {
                        // Контроль потери кадров
                        static uint64_t gNum = 0;
                        // LOG_DBG("[CPRR DRIVER (Listener)] TYPE: %d, LEN: %d, GN: %d (readResult:%d)", pktHeader->Type, pktHeader->Length,pktHeader->GrabNumber, readResult );
                        if ((gNum)&&(gNum + 1 != pktHeader->GrabNumber)) {
                            LOG_DBG("[CPRR DRIVER (Listener)] GN: %d (LAST GN:%d)", pktHeader->GrabNumber, gNum);
                        }
                        gNum = pktHeader->GrabNumber;
                        //----------------------------

                        if (pktHeader->Length < (CPRR_STREAM_FRAME_MAX_SIZE - CPRR_FIXED_HEADER_SIZE)) {
                            curFrameTailSize = pktHeader->Length - (readResult - CPRR_FIXED_HEADER_SIZE);
                            curFrameHeaderSize = readResult;
                            curFrameRxSize = readResult;
                            if (curFrameTailSize <= 0) {
                        // полный кадр принят
                                curFrameRxSize = 0;
                                curFrameTailSize = 0;
                                
                            }

                        } else {
                            LOG_DBG("[CPRR DRIVER (Listener)] FRAME SIZE OVERFLOW!: %d --> IGNORE!", pktHeader->Length);
                        }
                    } else {
                        LOG_DBG("[CPRR DRIVER (Listener)] TYPE : %d (ignore pkt)", pktHeader->Type);
                        TStreamerT25PktBuffer * pPkt = (TStreamerT25PktBuffer *) pktHeader;
                        if (data->t25event) {
                            data->t25event(pPkt->Type, pPkt->Length + CPRR_FIXED_HEADER_SIZE, pPkt);
                        }
                    }
                    //Корректный пакет
                } else {
                    if (isPreambleError == 0) {
                        LOG_DBG("[CPRR DRIVER (Listener)] PREAMBLE : 0x%.8X (ignore pkt [%d]) RxSize:%d (readResult : %d)", pktHeader->Preamble, dbgPktCount, curFrameRxSize, readResult);
                        //игнорируем  пакет
                        isPreambleError = 1;
                    }
                }
            }
        } else {
            if (data->needStop == 0) {
                LOG_DBG("[CPRR DRIVER (Listener)] ERROR! (%d)\n", readResult);
            }

            // Ошибка
            if (data->needStop == 0) {
                LOG_DBG("[CPRR DRIVER (Listener)] NO DATA...\n");
                usleep(1000 * 10);
            }
        }
    }
    free(streamerBuffer[0]);
    free(streamerBuffer[1]);
    return NULL;
}

/**
 * @brief Очистка буфера данных UDP. 
 * 
 * Читаем все что есть в буфере, что бы очистить его.
 * Вначале читаем буферами размера MTU, затем по байтам.
 * 
 * @return Размер буфера, который удалось считать
*/
static int cprr24StreamerReadAll(void) {
    char sockBuff[CPRR_UDP_PKT_MAX_SIZE];
    int readResult;
    int result = 0;
    while (1) {
        readResult = recv(streamerThreadData.sock, &sockBuff, CPRR_UDP_PKT_MAX_SIZE, MSG_DONTWAIT);
        if (readResult <= 0) {
            while (1) {
                readResult = recv(streamerThreadData.sock, &sockBuff, 1, MSG_DONTWAIT);
                if (readResult <= 0) break;
            }
            break;
        }
        result += readResult;
    }
    return result;
}

/**
 * @brief Увеличиваем сетевой буфер ОС
 * 
 * Буфера размером 50 кадров данных должно хватить при нормальной работе приложения
  * 
 * @return код ошибки. 0 - нет ошибок.
*/
static int cprr24setBufferSize(int sock) {
    int optionValue;
    int optionValueRead;
    unsigned int optionSize = sizeof (optionValue);

    getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (void *) &optionValueRead, &optionSize);

    optionValue = CPRR_STREAM_FRAME_MAX_SIZE * 50;
    setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &optionValue, optionSize);
    // ---------------------------------------------------------------------
    getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (void *) &optionValue, &optionSize);
    // LOG_DBG("[CPRR DRIVER] Info : OS receive buffer size = %d -> %d\n", optionValueRead, optionValue);
    // ---------------------------------------------------------------------

#ifndef SO_MAX_PACING_RATE
#    define SO_MAX_PACING_RATE 47
#endif    
    // If socket pacing is specified, try it.
//    unsigned int fqrate = 400 * 1000000 / 8;
//    if (setsockopt(sock, SOL_SOCKET, SO_MAX_PACING_RATE, &fqrate, sizeof (fqrate)) < 0) {
//        LOG_DBG("[CPRR DRIVER] Unable to set socket pacing");
//    }
    return 0;
}

int cprr24StreamerStart(char * IP, unsigned int dstPort, unsigned int srcPort, TCPRR24_Streamer_callback callback, TCPRR24_T25_callback T25callback) {
    int sock;

    memset((void*) &streamerThreadData, 0, sizeof (streamerThreadData));

    // Настройка sockaddr для sendto()
    streamerThreadData.sockaddr.sin_family = AF_INET;
    streamerThreadData.sockaddr.sin_addr.s_addr = inet_addr(IP);
    streamerThreadData.sockaddr.sin_port = htons(dstPort);

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        LOG_DBG("[CPRR DRIVER] Open socket error! (%d)\n", sock);
        return -1;
    }

    cprr24setBufferSize(sock);
    
    if (srcPort) {
        struct sockaddr_in address;
        // для работы с эмулятором, который выдает на конкретный порт, требуется биндить порт
        address.sin_family = AF_INET;
        // оставил возможность получать данные от любого IP
        address.sin_addr.s_addr = htonl(INADDR_ANY);
        address.sin_port = htons(srcPort);

        if (bind(sock, (struct sockaddr *) &address, sizeof (address)) < 0) {
            LOG_DBG("[CPRR DRIVER] Bind error!\n");
            return -1;
        }
    } else {
        if (connect(sock, (struct sockaddr *) &streamerThreadData.sockaddr, sizeof (streamerThreadData.sockaddr)) < 0) {
            LOG_DBG("[CPRR DRIVER] UDP: Connect Failed!");
            return -1;
        }
        // LOG_DBG("[CPRR DRIVER] Connect OK.");
    }


    // Семафор для синхронизации получателя и потребителя
    sem_init(&streamerThreadData.frameReadySemaphore, 0, 0);

    streamerThreadData.needStop = 0;
    streamerThreadData.sock = sock;
    streamerThreadData.event = callback;
    streamerThreadData.t25event = T25callback;
    streamerThreadData.initDone = 1;

    // Послать команду остановить стример (если вдруг он уже работает)
    TStreamerCmdBuffer streamerCmdOFF = {.Power = 0, .Streaming = 0};
    cprr24StreamerSend(CPRR_CMD_ID_SET_MODE, (char*) &streamerCmdOFF, sizeof (streamerCmdOFF));

    usleep(1000 * 50);
    cprr24StreamerReadAll();

    // Создание двух потоков с максимальным приоритетом выполнения
    pthread_attr_t attr;
    pthread_attr_init(&attr);

    int priority_max = sched_get_priority_max(SCHED_FIFO);
    // LOG_DBG("[CPRR DRIVER] priority_max : %d", priority_max);

    int r = udpStreamerThreadSetPriority(&attr, priority_max); // 20 Максимальный приоритет
    // LOG_DBG("[CPRR DRIVER] ThreadSetPriority : %d (priority : %d)", r, priority_max);
    if (pthread_create(&streamerThreadData.fileStreamerWriter, NULL, fileStreamerWriter_thread, &streamerThreadData) == 0) {
        streamerThreadData.isWriterRun = 1;
        if (pthread_create(&streamerThreadData.udpStreamerListener, &attr, udpStreamerListener_thread, &streamerThreadData) == 0) {
            streamerThreadData.isListenerRun = 1;
            // Успешный запуск
            return 0;
        } else {
            LOG_DBG("[CPRR DRIVER] Started Writer thread Error!");
        }
    } else {
        LOG_DBG("[CPRR DRIVER] Started Streamer thread Error!");
    }

    // Ошибка
    return -1;

}

int cprr24StreamerSend(uint32_t type, char * cmdBuff, unsigned int size) {
    TStreamerT25PktBuffer pkt;
    int result = 0;
    if (size < sizeof (pkt.Buffer)) {
        pkt.Preamble = 0x0000ABCD;
        pkt.Type = type;
        pkt.Length = size;
        memcpy((void*) &pkt.Buffer[0], cmdBuff, size);

        if ((streamerThreadData.initDone)&&(streamerThreadData.needStop == 0)) {
            result = sendto(streamerThreadData.sock, (void*) &pkt, CPRR_FIXED_HEADER_SIZE + size, 0, (struct sockaddr *) &streamerThreadData.sockaddr, sizeof (streamerThreadData.sockaddr));
            if (result < 0) {
                LOG_DBG("[CPRR DRIVER] sendto() Error!");
            }
        }
    }
    return result;
}

int cprr24StreamerStop() {

    if (streamerThreadData.initDone) {
        // остановка потоков
        streamerThreadData.needStop = 1;
        // остановка сокета (выход из recv)
        shutdown(streamerThreadData.sock, SHUT_RD);
        close(streamerThreadData.sock);
        // Ожидаем завершения потоков
        if (streamerThreadData.isListenerRun)
            if (pthread_join(streamerThreadData.udpStreamerListener, NULL)) {
                LOG_DBG("[CPRR DRIVER] join Listener thread error!");
            }
        // пробуждаем поток "Writer"
        sem_post(&streamerThreadData.frameReadySemaphore);
        if (streamerThreadData.isWriterRun)
            if (pthread_join(streamerThreadData.fileStreamerWriter, NULL)) {
                LOG_DBG("[CPRR DRIVER] join Writer thread error!");
            }
        sem_close(&streamerThreadData.frameReadySemaphore);

        streamerThreadData.initDone = 0;
        LOG_DBG("[CPRR DRIVER] DONE...");
    } else {
        LOG_DBG("[CPRR DRIVER] IGNORE...");
    }
    return 0;
}

