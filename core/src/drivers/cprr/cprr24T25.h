#ifndef CPRR24T25_H
#define CPRR24T25_H


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <stdatomic.h>

#include "cprr24Protocol.h"

#define CPRR_T25_BUFFER_MAX_SIZE       (2*1024*88) // 1024 цели + двойной запас



typedef struct {
    // флаг готовности к работе
    int initDone; // 
    // команда выхода для потоков
    int needStop; // 
    // сокет радара
    int sock;
    // заполненная структура адреса (для отправки команды)
    struct sockaddr_in sockaddr;
    // счетчик принятых (необработанных) кадров
    int frameCount;
    // потоки приема и записи
    pthread_t udpT25Listener;
    // флаги запуска потоков
    int isListenerRun;
    TCPRR24_T25_callback      t25event;    
} TT25ThreadData;



int cprr24T25Start(char * IP, unsigned int dstPort, unsigned int srcPort, TCPRR24_T25_callback T25callback);
int cprr24T25Send(uint32_t type, char * cmdBuff, unsigned int size);
int cprr24T25Stop();

extern int CPRR24T25_RxCount;

#endif
