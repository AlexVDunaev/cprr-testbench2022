
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "cprr24Tester.h"

#if 1
// Вывод отладочной информации
#    define LOG_DBG(...) Log_DBG(__VA_ARGS__)
void Log_DBG(const char * text, ...);
#else
#    define LOG_DBG(...) 
#endif

// Двойная буферизация
static TTesterThreadData testerThreadData = {.initDone = 0};
int CPRR24Tester_Errors = 0;
int CPRR24Tester_LastErrGN = 0;
int CPRR24Tester_RxCount = 0;
int CPRR24Tester_headerSize = 0;
char CPRR24Tester_headerBuffer[100];

static int udpTesterThreadSetPriority(pthread_attr_t * tattr, int priority) { //

    int ret = -1;
    struct sched_param param;

    /* initialized with default attributes */
    if (pthread_attr_init(tattr) == 0) {
        /* safe to get existing scheduling param */
        if (pthread_attr_getschedparam(tattr, &param) == 0) {
            /* set the priority; others are unchanged */
            param.sched_priority = priority;
            /* setting the new scheduling param */
            ret = pthread_attr_setschedparam(tattr, &param);
        }
    }
    return ret;
}



void* udpTesterListener_thread(void * arg) {
    volatile TTesterThreadData * data = arg;
    ssize_t readResult;
    char * receiveBuff;
    TStreamerPktBuffer * pktHeader = NULL;
    // Размер служебных данных в кадре
    uint32_t curFrameHeaderSize = 0;
    uint32_t curFrameRxSize = 0;
    int32_t curFrameTailSize = 0;
    uint32_t frameCount = 0;
    uint32_t dbgPktCount = 0;
    char isPreambleError = 0;

    receiveBuff = (char *) malloc(CPRR_STREAM_FRAME_MAX_SIZE);
    
    if (receiveBuff == NULL) {
        data->needStop = 1;
        LOG_DBG("[CPRR DRIVER (Listener)] Memory Error!\n");
    }

    while (data->needStop == 0) {
        // Один буфер для заполнения, второй для работы
        if ((curFrameRxSize + CPRR_UDP_PKT_MAX_SIZE) >= CPRR_STREAM_FRAME_MAX_SIZE) {
            LOG_DBG("[CPRR DRIVER (Listener)] Buffer overflow!!! (curFrameRxSize : %d, curFrameTailSize: %d)\n", curFrameRxSize, curFrameTailSize);
            curFrameRxSize = 0;
        }
        size_t receiveSize = CPRR_UDP_PKT_MAX_SIZE;
        if ((curFrameTailSize)&&(curFrameTailSize < CPRR_UDP_PKT_MAX_SIZE)) {
            receiveSize = curFrameTailSize;
        }
        readResult = recv(data->sock, &receiveBuff[curFrameRxSize], receiveSize, 0);

        dbgPktCount++;
        if (readResult > 0) {
            if (readResult > 1400) {
                LOG_DBG("[CPRR DRIVER (Listener)] readResult : %d", readResult);
            }
            if (curFrameRxSize) {
                // Данные кадра
                curFrameRxSize += readResult;
                if (curFrameTailSize < readResult) {
                    LOG_DBG("[CPRR DRIVER (Listener)] curFrameTailSize (%d) < readResult (%d)", curFrameTailSize, readResult);
                }
                curFrameTailSize -= readResult;
                if (curFrameTailSize <= 0) {
                    // полный кадр принят
                    curFrameRxSize = 0;
                    curFrameTailSize = 0;
                    frameCount++;
                }
            } else {
                // Заголовок [ + Данные кадра]
                pktHeader = (TStreamerPktBuffer *) receiveBuff;
                if (pktHeader->Preamble == RADAR_PKT_PREAMBLE) {
                    isPreambleError = 0;
                    dbgPktCount = 0;
                    if (pktHeader->Type == 10) {
                        CPRR24Tester_RxCount++;
                        // Контроль потери кадров
                        static uint64_t gNum = 0;
                        // LOG_DBG("[CPRR DRIVER (Listener)] TYPE: %d, LEN: %d, GN: %d (readResult:%d)", pktHeader->Type, pktHeader->Length, pktHeader->GrabNumber, readResult );
                        if ((gNum)&&(gNum + 1 != pktHeader->GrabNumber)) {
                            // LOG_DBG("[CPRR DRIVER (Listener)] GN: %d (LAST GN:%d)", pktHeader->GrabNumber, gNum);
                            CPRR24Tester_Errors++;
                            CPRR24Tester_LastErrGN = pktHeader->GrabNumber;
                        }
                        memcpy(CPRR24Tester_headerBuffer, receiveBuff, 52);
                        CPRR24Tester_headerSize = 52;
                        
                        gNum = pktHeader->GrabNumber;
                        //----------------------------
                        if (pktHeader->Length < (CPRR_STREAM_FRAME_MAX_SIZE - CPRR_FIXED_HEADER_SIZE)) {
                            curFrameTailSize = pktHeader->Length - (readResult - CPRR_FIXED_HEADER_SIZE);
                            curFrameHeaderSize = readResult;
                            curFrameRxSize = readResult;
                            if (curFrameTailSize <= 0) {
                                // полный кадр принят
                                curFrameRxSize = 0;
                                curFrameTailSize = 0;
                                frameCount++;
                            }
                        } else {
                            LOG_DBG("[CPRR DRIVER (Listener)] FRAME SIZE OVERFLOW!: %d --> IGNORE!", pktHeader->Length);
                        }
                    } else {
                        LOG_DBG("[CPRR DRIVER (Listener)] TYPE : %d (ignore pkt)", pktHeader->Type);
                    }
                    //Корректный пакет
                } else {
                    
                }
            }
        } else {
            if (data->needStop == 0) {
                LOG_DBG("[CPRR DRIVER (Listener)] ERROR! (%d)\n", readResult);
            }

            // Ошибка
            if (data->needStop == 0) {
                LOG_DBG("[CPRR DRIVER (Listener)] NO DATA...\n");
                usleep(1000 * 10);
            }
        }
    }
    return NULL;
}


int cprr24setBufferSize(int sock) {
    int optionValue;
    int optionValueRead;
    unsigned int optionSize = sizeof (optionValue);

    getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (void *) &optionValueRead, &optionSize);

    optionValue = CPRR_STREAM_FRAME_MAX_SIZE * 50;
    setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &optionValue, optionSize);
    // ---------------------------------------------------------------------
    getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (void *) &optionValue, &optionSize);
    // LOG_DBG("[CPRR DRIVER] Info : OS receive buffer size = %d -> %d\n", optionValueRead, optionValue);
    // ---------------------------------------------------------------------

}

int cprr24TesterStart(char * IP, unsigned int dstPort, unsigned int srcPort) {
    int sock;

    memset((void*) &testerThreadData, 0, sizeof (testerThreadData));

    // Настройка sockaddr для sendto()
    testerThreadData.sockaddr.sin_family = AF_INET;
    testerThreadData.sockaddr.sin_addr.s_addr = inet_addr(IP);
    testerThreadData.sockaddr.sin_port = htons(dstPort);

    LOG_DBG("[cprr24TesterStart] %s:%d[bind:%d]", IP, dstPort, srcPort);
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        LOG_DBG("[CPRR DRIVER] Open socket error! (%d)\n", sock);
        return -1;
    }

    cprr24setBufferSize(sock);
    
    if (srcPort) {
        struct sockaddr_in address;
        // для работы с эмулятором, который выдает на конкретный порт, требуется биндить порт
        address.sin_family = AF_INET;
        // оставил возможность получать данные от любого IP
        address.sin_addr.s_addr = htonl(INADDR_ANY);
        address.sin_port = htons(srcPort);

        if (bind(sock, (struct sockaddr *) &address, sizeof (address)) < 0) {
            LOG_DBG("[CPRR DRIVER] Bind error!\n");
            return -1;
        }
    } else {
        if (connect(sock, (struct sockaddr *) &testerThreadData.sockaddr, sizeof (testerThreadData.sockaddr)) < 0) {
            LOG_DBG("[CPRR DRIVER] Connect Failed!");
            return -1;
        }
        LOG_DBG("[CPRR DRIVER] Connect OK.");
    }



    testerThreadData.needStop = 0;
    testerThreadData.sock = sock;
    testerThreadData.initDone = 1;

    // Создание двух потоков с максимальным приоритетом выполнения
    pthread_attr_t attr;
    pthread_attr_init(&attr);

    int priority_max = sched_get_priority_max(SCHED_FIFO);

    int r = udpTesterThreadSetPriority(&attr, priority_max); // 20 Максимальный приоритет
    if (pthread_create(&testerThreadData.udpTesterListener, &attr, udpTesterListener_thread, &testerThreadData) == 0) {
        testerThreadData.isListenerRun = 1;
        // Успешный запуск
        return 0;
    } else {
        LOG_DBG("[CPRR DRIVER] Started Listener thread Error!");
    }

    // Ошибка
    return -1;

}

int cprr24TesterSend(uint32_t type, char * cmdBuff, unsigned int size) {
    TStreamerT25PktBuffer pkt;
    int result = 0;
    if (size < sizeof (pkt.Buffer)) {
        pkt.Preamble = 0x0000ABCD;
        pkt.Type = type;
        pkt.Length = size;
        memcpy((void*) &pkt.Buffer[0], cmdBuff, size);

        if ((testerThreadData.initDone)&&(testerThreadData.needStop == 0)) {
            result = sendto(testerThreadData.sock, (void*) &pkt, CPRR_FIXED_HEADER_SIZE + size, 0, (struct sockaddr *) &testerThreadData.sockaddr, sizeof (testerThreadData.sockaddr));
            if (result < 0) {
                LOG_DBG("[CPRR DRIVER] sendto() Error!");
            }
        }
    }
    return result;
}

int cprr24TesterStop() {

    if (testerThreadData.initDone) {
        // остановка потоков
        testerThreadData.needStop = 1;
        // остановка сокета (выход из recv)
        shutdown(testerThreadData.sock, SHUT_RD);
        close(testerThreadData.sock);
        // Ожидаем завершения потоков
        if (testerThreadData.isListenerRun)
            if (pthread_join(testerThreadData.udpTesterListener, NULL)) {
                LOG_DBG("[CPRR DRIVER] join Listener thread error!");
            }

        testerThreadData.initDone = 0;
        LOG_DBG("[CPRR DRIVER] DONE...");
    } else {
        LOG_DBG("[CPRR DRIVER] IGNORE...");
    }
    return 0;
}

