#ifndef CPRR24PROTOCOL_H
#define CPRR24PROTOCOL_H

#include <stdint.h>


//----------------------------------------------------
// Преамбула данного сервера
#define RADAR_PKT_PREAMBLE              0x0000ABCD
   
// Возможные команды сервера
#define RADAR_PKT_REQUEST_ID            1
#define RADAR_PKT_MODE_ID               2
#define RADAR_PKT_PLATFORM_ID           3
#define RADAR_PKT_DATA_ID               4
#define RADAR_PKT_INFO_ID               5
#define RADAR_PKT_ERROR_ID              6
#define RADAR_PKT_MIN_ID                1
#define RADAR_PKT_MAX_ID                6
#define RADAR_PKT_FIRMWARE_ID           16777
#define RADAR_PKT_DATASET_ID            16778

// Коды запроса в сообщении REQUEST
#define RADAR_REQ_GETVER                1
//----------------------------------------------------

#define CPRR_UDP_PKT_MAX_SIZE        (1536)

// Данные заголовка, которые не учтены в поле длины
#define CPRR_FIXED_HEADER_SIZE       (12)



typedef struct __attribute__ ((__packed__)) {
    uint16_t minor;
    uint16_t major;
} TRadarVersion;

typedef struct __attribute__ ((__packed__)) {
    TRadarVersion  HardVersion;
    TRadarVersion  SoftVersion;
    uint32_t SerialNumb;
} TStreamerPackInfo;



typedef struct __attribute__ ((__packed__)) {
    uint32_t Preamble; // 0x0000ABCD  ;
    uint32_t Length; // = 40 + *количество данных во фрейме в байтах* 
    uint32_t Type; //
    uint64_t Time; //
    uint64_t GrabNumber; //
    uint16_t Temperature[6]; //
    uint32_t NumChirps; //
    uint32_t NumChanels; //
    uint32_t NumPoints; //
    uint8_t Buffer[255]; //
}TStreamerPktBuffer;

typedef struct __attribute__ ((__packed__)) {
    uint32_t Preamble; // 0x0000ABCD  ;
    uint32_t Length; // без учета заголовка (pktSize - 12)
    uint32_t Type; //
    uint8_t Buffer[255]; //
}TStreamerT25PktBuffer;

typedef struct __attribute__ ((__packed__)) {
    uint32_t Power ;     // 2 - режим сырых 
    uint32_t Streaming ; // режим
} TStreamerCmdBuffer;


// ID, x, vx, ax, y, vy, ay, Amp, lTime, sbTime
typedef struct  __attribute__((__packed__)){
    int64_t ObjID;
    double Range;
    double Azimuth;
    int64_t LiveTime;
    double Rcs;
    double Xoord;
    double Xrate;
    double Xacceleration;
    double Ycoord;
    double Yrate;
    double Yacceleration;
} TRadarTarget;

typedef struct __attribute__((__packed__)){
    uint32_t Status;
    uint64_t GrabNumber;
    uint64_t TimeStamp;
    double   Speed;
} TRadarTargetParam;

typedef struct __attribute__((__packed__)){
    uint32_t Preamble; // 0x0000ABCD  ;
    uint32_t Length; // без учета заголовка (pktSize - 12)
    uint32_t Type; //
    uint32_t Status;
    uint64_t GrabNumber;
    uint64_t TimeStamp;
    double   Speed;
    TRadarTarget Targets [1];
} TT25PackData;

typedef void (*TCPRR24_T25_callback)(uint32_t Type, uint32_t dataLength, TStreamerT25PktBuffer * data);


#endif