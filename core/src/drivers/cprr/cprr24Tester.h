#ifndef CPRR24TESTER_H
#define CPRR24TESTER_H


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <stdatomic.h>

#include "cprr24Protocol.h"

#define CPRR_STREAM_FRAME_MAX_SIZE   (2*1024*1024)
//#define CPRR_STREAM_BUFFER_SIZE      (CPRR_STREAM_FRAME_NUM*CPRR_STREAM_FRAME_MAX_SIZE)
// Данные заголовка, которые не учтены в поле длины
#define CPRR_FIXED_HEADER_SIZE       (12)

#define CPRR_CMD_ID_SET_MODE         (RADAR_PKT_MODE_ID)
//#define CPRR_STREAMING_TIMEOUT       (5000) // ожидание данных после включения режима, ms



typedef struct {
    void * pkt;
    uint32_t frameSize;
    uint32_t HeaderSize;
} TTesterWriterData;

typedef struct {
    // флаг готовности к работе
    int initDone; // 
    // команда выхода для потоков
    int needStop; // 
    // сокет радара
    int sock;
    // заполненная структура адреса (для отправки команды)
    struct sockaddr_in sockaddr;
    // счетчик принятых (необработанных) кадров
    int frameCount;
    // потоки приема и записи
    pthread_t udpTesterListener;
    // флаги запуска потоков
    int isListenerRun;
} TTesterThreadData;



int cprr24TesterStart(char * IP, unsigned int dstPort, unsigned int srcPort);
int cprr24TesterSend(uint32_t type, char * cmdBuff, unsigned int size);
int cprr24TesterStop();

extern int CPRR24Tester_Errors;
extern int CPRR24Tester_RxCount;
extern int CPRR24Tester_LastErrGN;
extern int CPRR24Tester_headerSize;
extern char CPRR24Tester_headerBuffer[100];


#endif
