#ifndef CPRR24STREAMER_H
#define CPRR24STREAMER_H

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <stdatomic.h>

#include "cprr24Protocol.h"

//#define CPRR_STREAM_FRAME_NUM        (10)
#define CPRR_STREAM_FRAME_MAX_SIZE   (2*1024*1024)
//#define CPRR_STREAM_BUFFER_SIZE      (CPRR_STREAM_FRAME_NUM*CPRR_STREAM_FRAME_MAX_SIZE)

#define CPRR_CMD_ID_SET_MODE         (RADAR_PKT_MODE_ID)
#define CPRR_STREAMING_TIMEOUT       (5000) // ожидание данных после включения режима, ms


typedef enum {
    CPRR24_EVENT_FRAME_DONE = 0x0U, // Получили кадр
    CPRR24_EVENT_TIMEOUT = 0x1U, // Истекло время ожидания
    CPRR24_EVENT_ERROR = 0x2U // Ошибка
} TCPRR24StreamerEventId;

typedef int (*TCPRR24_Streamer_callback)(TCPRR24StreamerEventId eventId, char * data, int size, int headerSize);

typedef struct {
    void * pkt;
    uint32_t frameSize;
    uint32_t HeaderSize;
} TStreamerWriterData;

typedef struct {
    // флаг готовности к работе
    int initDone; // 
    // команда выхода для потоков
    int needStop; // 
    // сокет радара
    int sock;
    // заполненная структура адреса (для отправки команды)
    struct sockaddr_in sockaddr;

    // Данные для обработчика + флаг блокировки
    atomic_int writerDataLock;
    TStreamerWriterData writerData;

    // счетчик принятых (необработанных) кадров
    int frameCount;
    // потоки приема и записи
    pthread_t udpStreamerListener;
    pthread_t fileStreamerWriter;
    // флаги запуска потоков
    int isListenerRun;
    int isWriterRun;
    // Семафор готовности данных
    sem_t frameReadySemaphore;
    TCPRR24_Streamer_callback event;
    TCPRR24_T25_callback      t25event;

} TStreamerThreadData;



int cprr24StreamerStart(char * IP, unsigned int dstPort, unsigned int srcPort, TCPRR24_Streamer_callback callback, TCPRR24_T25_callback T25callback);
int cprr24StreamerSend(uint32_t type, char * cmdBuff, unsigned int size);
int cprr24StreamerStop();


#endif
