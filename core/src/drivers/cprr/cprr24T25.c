
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "cprr24T25.h"

#if 1
// Вывод отладочной информации
#    define LOG_DBG(...) Log_DBG(__VA_ARGS__)
void Log_DBG(const char * text, ...);
#else
#    define LOG_DBG(...) 
#endif

// Двойная буферизация
static TT25ThreadData t25ThreadData = {.initDone = 0};

int CPRR24T25_RxCount = 0;

static int udpT25ThreadSetPriority(pthread_attr_t * tattr, int priority) { //

    int ret = -1;
    struct sched_param param;

    /* initialized with default attributes */
    if (pthread_attr_init(tattr) == 0) {
        /* safe to get existing scheduling param */
        if (pthread_attr_getschedparam(tattr, &param) == 0) {
            /* set the priority; others are unchanged */
            param.sched_priority = priority;
            /* setting the new scheduling param */
            ret = pthread_attr_setschedparam(tattr, &param);
        }
    }
    return ret;
}

void* udpT25Listener_thread(void * arg) {
    volatile TT25ThreadData * data = arg;
    ssize_t readResult;
    char * targetsBuffer;
    TStreamerT25PktBuffer * pktHeader = NULL;
    uint32_t dbgPktCount = 0;
    uint32_t preambleErrorCount = 0;
    char pktBuffer[CPRR_UDP_PKT_MAX_SIZE];
    TT25PackData * t25pkt = (TT25PackData *) & pktBuffer[0];
    uint64_t currentGrabNumber = -1;
    uint32_t pktCount = 0;
    uint32_t targetCount = 0;
    uint32_t headerSize = (uint32_t)((char*)&t25pkt->Targets[0] - (char*)&t25pkt->Preamble);
    uint32_t targetsBufferSize = 0;
    CPRR24T25_RxCount = 0;
    LOG_DBG("[CPRR DRIVER (Listener)] headerSize : %d", headerSize);
    

    targetsBuffer = (char *) malloc(CPRR_T25_BUFFER_MAX_SIZE);
    pktHeader = (TStreamerT25PktBuffer *)targetsBuffer;
    if (targetsBuffer == NULL) {
        data->needStop = 1;
        LOG_DBG("[CPRR DRIVER (Listener)] Memory Error!\n");
    }

    while (data->needStop == 0) {
        readResult = recv(data->sock, &pktBuffer[0], CPRR_UDP_PKT_MAX_SIZE, 0);

        dbgPktCount++;
        if (readResult > 0) {
            if (readResult > 1400) {
                LOG_DBG("[CPRR DRIVER (Listener)] readResult : %d", readResult);
            }
            if (t25pkt->Preamble == RADAR_PKT_PREAMBLE) {
                if (t25pkt->Type == RADAR_PKT_DATA_ID) {
                    if (currentGrabNumber == -1) currentGrabNumber = t25pkt->GrabNumber;
                    if (currentGrabNumber != t25pkt->GrabNumber) {
                        CPRR24T25_RxCount++;

                        // выгрузить данные. Полный кадр есть
                        // ...
                        // ...
                        // LOG_DBG("[CPRR DRIVER (Listener)] FRAME DONE: targetCount : %d (Buff: %d, pktCount: %d)", targetCount, targetsBufferSize, pktCount);
                        if (data->t25event) {
                            data->t25event(t25pkt->Type, targetsBufferSize, pktHeader);
                        }
                        pktCount = 0;                        
                        targetCount = 0;
                        currentGrabNumber = t25pkt->GrabNumber;
                    }
                    uint32_t targetNum = (readResult - headerSize)/sizeof(t25pkt->Targets[0]);
                    targetCount += targetNum;
                    // LOG_DBG("[CPRR DRIVER (Listener)] targetNum in pkt : %d", targetNum);
                    
                    if (pktCount == 0) {
                        // первый пакет из очереди пакетов отметок.
                        // копируем буфер целиком
                        memcpy(targetsBuffer, pktBuffer, readResult);
                        targetsBufferSize = readResult;
                    } else {
                        // для остальных пакетов, копируем только цели
                        if (targetsBufferSize + readResult < CPRR_T25_BUFFER_MAX_SIZE) {
                            memcpy(&targetsBuffer[targetsBufferSize], &t25pkt->Targets[0], readResult - headerSize);
                            targetsBufferSize += readResult - headerSize;    
                        } else {
                            LOG_DBG("[CPRR DRIVER (Listener)] TARGET BUFFER OVERFLOW!!!");
                        }
                    }
                    pktCount++;
                    
                }
            } else {
                LOG_DBG("[CPRR DRIVER (Listener)] PREAMBLE ERROR!!! (Error count:%d)", preambleErrorCount);
                preambleErrorCount++;
            }
        } else {
            if (data->needStop == 0) {
                LOG_DBG("[CPRR DRIVER (Listener)] NO DATA...\n");
                usleep(1000 * 10);
            }
        }
    }
    free(targetsBuffer);
    return NULL;
}

int cprr24T25setBufferSize(int sock) {
    int optionValue;
    int optionValueRead;
    unsigned int optionSize = sizeof (optionValue);

    getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (void *) &optionValueRead, &optionSize);

    optionValue = CPRR_T25_BUFFER_MAX_SIZE * 50;
    setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &optionValue, optionSize);
    // ---------------------------------------------------------------------
    getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (void *) &optionValue, &optionSize);
    // LOG_DBG("[CPRR DRIVER] Info : OS receive buffer size = %d -> %d\n", optionValueRead, optionValue);
    // ---------------------------------------------------------------------

}

int cprr24T25Start(char * IP, unsigned int dstPort, unsigned int srcPort, TCPRR24_T25_callback T25callback) {
    int sock;

    memset((void*) &t25ThreadData, 0, sizeof (t25ThreadData));

    // Настройка sockaddr для sendto()
    t25ThreadData.sockaddr.sin_family = AF_INET;
    t25ThreadData.sockaddr.sin_addr.s_addr = inet_addr(IP);
    t25ThreadData.sockaddr.sin_port = htons(dstPort);

    LOG_DBG("[cprr24T25Start] %s:%d[bind:%d]", IP, dstPort, srcPort);
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        LOG_DBG("[CPRR DRIVER] Open socket error! (%d)\n", sock);
        return -1;
    }

    cprr24T25setBufferSize(sock);

    if (srcPort) {
        struct sockaddr_in address;
        // для работы с эмулятором, который выдает на конкретный порт, требуется биндить порт
        address.sin_family = AF_INET;
        // оставил возможность получать данные от любого IP
        address.sin_addr.s_addr = htonl(INADDR_ANY);
        address.sin_port = htons(srcPort);

        if (bind(sock, (struct sockaddr *) &address, sizeof (address)) < 0) {
            LOG_DBG("[CPRR DRIVER] Bind error!\n");
            return -1;
        }
    } else {
        if (connect(sock, (struct sockaddr *) &t25ThreadData.sockaddr, sizeof (t25ThreadData.sockaddr)) < 0) {
            LOG_DBG("[CPRR DRIVER] Connect Failed!");
            return -1;
        }
        LOG_DBG("[CPRR DRIVER] Connect OK.");
    }



    t25ThreadData.t25event = T25callback;
    t25ThreadData.needStop = 0;
    t25ThreadData.sock = sock;
    t25ThreadData.initDone = 1;

    // Создание двух потоков с максимальным приоритетом выполнения
    pthread_attr_t attr;
    pthread_attr_init(&attr);

    int priority_max = sched_get_priority_max(SCHED_FIFO);

    int r = udpT25ThreadSetPriority(&attr, priority_max); // 20 Максимальный приоритет
    if (pthread_create(&t25ThreadData.udpT25Listener, &attr, udpT25Listener_thread, &t25ThreadData) == 0) {
        t25ThreadData.isListenerRun = 1;
        // Успешный запуск
        return 0;
    } else {
        LOG_DBG("[CPRR DRIVER] Started Listener thread Error!");
    }

    // Ошибка
    return -1;

}

int cprr24T25Send(uint32_t type, char * cmdBuff, unsigned int size) {
    TStreamerT25PktBuffer pkt;
    int result = 0;
    if (size < sizeof (pkt.Buffer)) {
        pkt.Preamble = 0x0000ABCD;
        pkt.Type = type;
        pkt.Length = size;
        memcpy((void*) &pkt.Buffer[0], cmdBuff, size);

        if ((t25ThreadData.initDone)&&(t25ThreadData.needStop == 0)) {
            result = sendto(t25ThreadData.sock, (void*) &pkt, CPRR_FIXED_HEADER_SIZE + size, 0, (struct sockaddr *) &t25ThreadData.sockaddr, sizeof (t25ThreadData.sockaddr));
            if (result < 0) {
                LOG_DBG("[CPRR DRIVER] sendto() Error!");
            }
        }
    }
    return result;
}

int cprr24T25Stop() {

    if (t25ThreadData.initDone) {
        // остановка потоков
        t25ThreadData.needStop = 1;
        // остановка сокета (выход из recv)
        shutdown(t25ThreadData.sock, SHUT_RD);
        close(t25ThreadData.sock);
        // Ожидаем завершения потоков
        if (t25ThreadData.isListenerRun)
            if (pthread_join(t25ThreadData.udpT25Listener, NULL)) {
                LOG_DBG("[CPRR DRIVER] join Listener thread error!");
            }

        t25ThreadData.initDone = 0;
        LOG_DBG("[CPRR DRIVER] DONE...");
    } else {
        LOG_DBG("[CPRR DRIVER] IGNORE...");
    }
    return 0;
}

