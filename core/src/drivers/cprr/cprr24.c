//#define UDP_FORCE_MODE //Все что связано с оптимизацией приема по UDP

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "cprr24.h"
#include "cprr24Streamer.h"
#include "cprr24Tester.h"
#include "cprr24T25.h"

#include "../../core/coreUInterface.h"
#include "../../main.h"
#include "../../ROS/rosbagInterface.h"

typedef enum {
    SE_FRAME_NUM_DONE = 0x0U, // получили нужное число кадров
    SE_FRAME_LUA_BREAK = 0x1U // скрипт вернул код "BREAK"
} TStreamingExitCode;

typedef enum {
    STREAM_FORMAT_BIN = 0x0U, // запись в сыром виде
    STREAM_FORMAT_ROS = 0x1U // запись в ROSBAG
} TStreamingFileFormat;

static int LineIndex = 0;
static uint32_t eventFrameCount = 0;
static uint32_t writeFrameCount = 0;
static uint32_t writeFrameLimit = 0;
// Выход из режима приема сырых данных
static uint32_t streamingExit = 0;
static FILE *streamfile = NULL;
static uint32_t streamRosWriteCount = 0;
static TStreamingFileFormat streamingFileFormat = STREAM_FORMAT_BIN;
static TStreamerPackInfo radarPackInfo = {.SerialNumb = 0};

void CPRR24_StreamingExit(TStreamingExitCode causeCode) {
    streamingExit = 1;
}

void CPRR24_SetErrorStatusLua() {
    static char outdata[255];
    TLineRec* a;
    int RxSize = 0;
    TMACDriverArg arg = {.prIO.inBuff = (char*) "", .prIO.inCount = 0, .prIO.pLightuserdata = NULL,  .prIO.outBuff = (char*) &outdata[0], .prIO.outBuffCount = &RxSize};
    a = SystemState.LLC.Lines[LineIndex];
    if (a->callBackLua) {
        Log_DBG("[CPRR DRIVER] Error status -> Lua");
        if (SystemState.pScriptEngine->Events.OnLine("RX", a->name, a->callBackLua, arg, NULL) == MAC_RESULT_BREAK) {
            Log_DBG("[CPRR DRIVER] Lua -> BREAK");
            CPRR24_StreamingExit(SE_FRAME_LUA_BREAK);
        }
    }
}

static uint32_t scramblerBuffer[CPRR_STREAM_FRAME_MAX_SIZE / 4];
static int16_t frameInterlacing[CPRR_STREAM_FRAME_MAX_SIZE / 2];

void frameDoInterlacing(uint16_t * restrict normal, uint16_t * restrict interlacing, const int points, const int channels, const int chirps) {
    int step = 0;
    for (int chirp = 0; chirp < chirps; ++chirp) {
        for (int chan = 0; chan < channels; ++chan) {
            for (int pts = 0; pts < points; ++pts) {
                interlacing[chan + channels * pts + chirp * channels * points] = normal[step];
                ++step;
            };
        };
    };
}

void frameUnInterlacing(uint16_t * restrict interlacing, uint16_t * restrict normal, const int points, const int channels, const int chirps) {
    int step = 0;
    for (int chirp = 0; chirp < chirps; ++chirp) {
        for (int chan = 0; chan < channels; ++chan) {
            for (int pts = 0; pts < points; ++pts) {
                normal[step] = interlacing[chan + channels * pts + chirp * channels * points];
                ++step;
            };
        };
    };
}

static void Scrambler(uint32_t * restrict raw_u32, uint32_t * restrict scrmb, uint32_t length) {
    const int size = length / 4;
    for (int i = 16; i < size; ++i) {
        scrmb[i] = raw_u32[i] ^ scrmb[i - 15] ^ scrmb[i - 16];
    };
}


// Возврат : статус записи. 1 - ОК, 0 : ошибка!

int CPRR24_StreamROSWrite(char * data, int size, char * header, int headerSize) {
    TStreamerPktBuffer * frameHeader = (TStreamerPktBuffer *) header;
    int rosError = 0;
    ros_message_data uData;
    // Тип хранения вектора калибровки в радаре:
    typedef struct __attribute__((__packed__)) {
        uint32_t    valid;  // 1 : значения вектора задано. 0: вектор удален
        float       tCode;  // значение температуры, для которого применяется этот вектор
        float       value[32][2]; // 32 x [AMP, PHASE]
    } TMCALVector;

    if ((frameHeader->NumChirps == 0)&&(frameHeader->NumChanels == 0)&&(frameHeader->NumPoints == 0)) {
        int dOffset = 0;
        TMCALVector * pTmVector;
        // кадр конфигурации. Формат:
        // int TMCAL_VECTOR_MAX_NUM;
        // int TMCAL_POLY_MAX_POWER;
        // TMCALCalibrationData cData;
        // float Calibration_Rx[RX_CHANNELS_NUM*2];
        // int16_t configLeakage[RX_CHANNELS_NUM][CHIRP_SAMPLE_FROM_RADAR];
        // uint16_t checkCodeSum;
        // где TMCALCalibrationData : struct {
        //    TMCALVector tmVector[TMCAL_VECTOR_MAX_NUM];
        //    float       polyKoef[RX_CHANNELS_NUM][TMCAL_POLY_MAX_POWER + 1][2];
        // }

        uint16_t checkCodeSum = 0;
        uint16_t CodeSum = *((uint16_t*)&data[size - 2]);
        for (int i = 0; i < size - 2; i++)
            checkCodeSum += (uint8_t)data[i];
        if (CodeSum != checkCodeSum) {
            static int checkCodeSumError = 0;
            Log_DBG("[CPRR DRIVER] Error! Check code does not match (%d calc code: %d, size:%d)\n", CodeSum, checkCodeSum, size);
            if (checkCodeSumError++ > 10) exit(1);
            return 0;
        } else {
            Log_DBG("[CPRR DRIVER] Config checksum OK.\n");

            int32_t tmVectorNum = *((int32_t *)&data[dOffset]);
            dOffset += sizeof(tmVectorNum);
            int32_t tmPolyPowerNum = *((int32_t *)&data[dOffset]);
            dOffset += sizeof(tmPolyPowerNum);

            uData.int32 = (int32_t) radarPackInfo.SerialNumb; //
            rosError += rosWriteValue(RadarSerialNumb, uData, "RadarSerialNumb");

            // Выделяю буфер для хранения списка векторов, что бы первый вектор был с 0-м кодом!
            int tmVectorSize = sizeof(TMCALVector) - sizeof(uint32_t);
            char * tmVectorBuffer = (char *) malloc(tmVectorSize * (tmVectorNum + 1));
            int tmVectorBufferIndex = 1; // 0-й индекс для вектора по умолчанию

            for (int i = 0; i < tmVectorNum; i++) {
                pTmVector = (TMCALVector*)&data[dOffset];
                dOffset += sizeof(TMCALVector);
                if (pTmVector->valid) {
                    if ((pTmVector->tCode >= 0)&&(pTmVector->tCode < 1000.0)) {
                        // Если вектор валидный, нужно сохранить его в rosbag
                        memcpy((void*)&tmVectorBuffer[tmVectorBufferIndex*tmVectorSize], &pTmVector->tCode, tmVectorSize);
                        tmVectorBufferIndex++;                
                    } else {
                        Log_DBG("[CPRR DRIVER] ERROR! TM CODE: %f --> ignore vector!\n", pTmVector->tCode);
                    }
                    Log_DBG("[CPRR DRIVER] TM Vector[%d] valid : 1 (%d) --> write\n", i, (int)pTmVector->tCode);
                } else {
                    Log_DBG("[CPRR DRIVER] TM Vector[%d] valid : 0 --> ignore\n", i);
                }
            }
    #if 0   // poly в rosbag не записываю (пока не ясно что с ним делать...)       
            uData.float32Array = (float*)&data[dOffset];
            rosError += rosWriteValueArray(RadarTMPoly, uData, 2*32*(tmPolyPowerNum + 1), "RadarTMPoly");
    #endif        
            dOffset += 2*32*(tmPolyPowerNum + 1) * sizeof(float); // Смещение на LEAK

            // калибровка по умолчанию. Делаю формат как у TmVector. 
            *((float*)tmVectorBuffer) = 0.0f; // tCode = 0: Признак калибровки по умолчанию!
            memcpy((void *)&tmVectorBuffer[4], (void *)&data[dOffset], sizeof(float)*32*2);
            dOffset += 2*32 * sizeof(float);
            // запишем калибровку. Первая - по умолчанию
            for (int i = 0; i < tmVectorBufferIndex; i++) {
                uData.float32Array = (float*) &tmVectorBuffer[tmVectorSize * i];
                rosError += rosWriteValueArray(RadarTMCalib, uData, 64 + 1, "RadarTMCalib");
            }

            int leakSize = size - dOffset - sizeof(checkCodeSum);

            uData.int16Array = (int16_t*)&data[dOffset];
            rosError += rosWriteValueArray(RadarLeak, uData, leakSize / sizeof(int16_t), "RadarLeak");
    #if 0   // вывод в лог
            Log_DBG("[CPRR DRIVER] tmVectorNum = %d\n", tmVectorNum);
            Log_DBG("[CPRR DRIVER] tmPolyPowerNum = %d\n", tmPolyPowerNum);
            Log_DBG("[CPRR DRIVER] LEAK SIZE %d \n", leakSize);

            for (int i = 0; i < 4; i++) {
                Log_DBG("[CPRR DRIVER] LEAK[%d] : %d\n", i, (int)uData.int16Array[i]);
            }
            for (int j = 0; j < tmVectorNum + 1; j++) {
                if (j) 
                 Log_DBG("[CPRR DRIVER] CALIB : %d\n", j);
                else
                 Log_DBG("[CPRR DRIVER] CALIB DEFAULT:\n");

                for (int i = 0; i < 32; i++) {
                    TMCALVector * pV = (TMCALVector *) &tmVectorBuffer[-4 + j*tmVectorSize];
                    Log_DBG("%d. [%f, %f]\n", i, pV->value[i][0], pV->value[i][1]);
                }
            }
    #endif
            free(tmVectorBuffer);
            // if check sum OK
            streamRosWriteCount++;
            }


    } else {
        // кадр данных
        if (streamRosWriteCount == 0) {
            // Запись параметров файла
            rosSetTime(0, 10);
            uData.int32 = frameHeader->NumChirps;
            rosError += rosWriteValue(RadarChirps, uData, "NumChirps");
            uData.int32 = frameHeader->NumChanels;
            rosError += rosWriteValue(RadarChanels, uData, "NumChanels");
            uData.int32 = frameHeader->NumPoints;
            rosError += rosWriteValue(RadarPoints, uData, "NumPoints");
            uData.int32 = (int32_t) radarPackInfo.SerialNumb; //
            rosError += rosWriteValue(RadarSerialNumb, uData, "RadarSerialNumb");
            int16_t Temperature[6];
            for (int i = 0; i < 6; i++) Temperature[i] = frameHeader->Temperature[i];
            uData.int16Array = (int16_t*) & Temperature[0];
            rosError += rosWriteValueArray(RadarTemper, uData, 6, "TEMPERATURE");
        }

        // что бы отделить тех. сообщения от данных кадра, время данных не может быть 0!
        if (frameHeader->Time == 0)
            rosSetTime(0, 1000); // 1мкс
        else            
            rosSetTime(frameHeader->Time / 1000000, (frameHeader->Time % 1000000)*1000);

        frameDoInterlacing((uint16_t *) & data[0], &frameInterlacing[0], frameHeader->NumPoints, frameHeader->NumChanels, frameHeader->NumChirps);

        Scrambler((uint32_t*) & frameInterlacing[0], &scramblerBuffer[0], size);

        uData.int32Array = (int32_t*) & scramblerBuffer[0];
        rosError += rosWriteValueArray(RadarInt32Array, uData, size / 4, "DATA");

        if (rosError) {
            Log_DBG("[CPRR DRIVER] WRITE ROSBAG ERROR!...\n");
        }
        streamRosWriteCount++;
    }
    return (rosError == 0) ? 1 : 0;
}

// Возврат: 0 - нет ошибок, иначе ошибка!

int CPRR24_StreamROSWriteOpen(char * fileName, char appendMode) {
    streamRosWriteCount = 0;
    return rosDriverInit(fileName, (appendMode == '+')? ROSBAG_APPEND : ROSBAG_WRITE);
}

// Возврат: 0 - нет ошибок, иначе ошибка!

int CPRR24_StreamROSWriteClose() {
    return rosDriverClose();
}


// Возврат : кол-во записанных байт

int CPRR24_StreamBinWrite(char * data, int size, char * header, int headerSize) {
    int w = fwrite((void*) data, 1, size, streamfile);
    if (w != size) {
        Log_DBG("[CPRR DRIVER] WRITE FILE ERROR!...");
    }
    return w;
}

// Возврат: 0 - нет ошибок, иначе ошибка!

int CPRR24_StreamBinWriteOpen(char * fileName, char appendMode) {
    streamfile = fopen(fileName, "w");
    return (streamfile) ? 0 : -1;
}

// Возврат: 0 - нет ошибок, иначе ошибка!

int CPRR24_StreamBinWriteClose() {
    if (streamfile) {
        fflush(streamfile);
        fclose(streamfile);
    }
    return 0;
}

int CPRR24_StreamWrite(char * data, int size, char * header, int headerSize) {
    if (streamingFileFormat == STREAM_FORMAT_BIN)
        return CPRR24_StreamBinWrite(data, size, header, headerSize);
    if (streamingFileFormat == STREAM_FORMAT_ROS)
        return CPRR24_StreamROSWrite(data, size, header, headerSize);

}

int CPRR24_StreamWriteOpen(char * fileName, char appendMode) {
    char ext[] = ".bag";
    int fnLen = strlen(fileName);
    int extLen = strlen(ext);
    if (fnLen < extLen) return -1;

    streamingFileFormat = (strncmp(&fileName[fnLen - extLen], ext, extLen) == 0) ? STREAM_FORMAT_ROS : STREAM_FORMAT_BIN;

    if (streamingFileFormat == STREAM_FORMAT_ROS) Log_DBG("[CPRR DRIVER] STREAM -> ROSBAG");
    if (streamingFileFormat == STREAM_FORMAT_BIN) Log_DBG("[CPRR DRIVER] STREAM -> RAW BIN");

    if (streamingFileFormat == STREAM_FORMAT_BIN)
        return CPRR24_StreamBinWriteOpen(fileName, appendMode);
    if (streamingFileFormat == STREAM_FORMAT_ROS)
        return CPRR24_StreamROSWriteOpen(fileName, appendMode);

}

int CPRR24_StreamWriteClose() {
    if (streamingFileFormat == STREAM_FORMAT_BIN)
        return CPRR24_StreamBinWriteClose();
    if (streamingFileFormat == STREAM_FORMAT_ROS)
        return CPRR24_StreamROSWriteClose();
}


// пакеты протокола T25

void CPRR24_T25Event(uint32_t Type, uint32_t dataLength, TStreamerT25PktBuffer * data) {
    static char outdata[8255];
    TLineRec* a;
    if (streamingExit) return;

    int RxSize = 0;
    TMACDriverArg arg = {.prIO.inBuff = (char*) data, .prIO.inCount = dataLength, .prIO.pLightuserdata = NULL, .prIO.inCountEx = dataLength, .prIO.outBuff = (char*) &outdata[0], .prIO.outBuffCount = &RxSize};
    a = SystemState.LLC.Lines[LineIndex];
    if (Type == RADAR_PKT_INFO_ID) {
        // Версия радара. 
        TStreamerPackInfo * pRadarPackInfo = (TStreamerPackInfo *) &data->Buffer[0];
        radarPackInfo = *pRadarPackInfo;
        //Log_DBG("[CPRR DRIVER] INFO: Soft: v%d.%d, Hadr: v%d.%d, SN: %u", radarPackInfo.SoftVersion.major, radarPackInfo.SoftVersion.minor,
        //        radarPackInfo.HardVersion.major, radarPackInfo.HardVersion.minor, radarPackInfo.SerialNumb);
    }
    if ((Type == RADAR_PKT_DATA_ID)||(Type == RADAR_PKT_INFO_ID)) {
        if (a->callBackLua) {
            // Log_DBG("[CPRR DRIVER DBG] PKT_ID : %d", Type);
            int result = SystemState.pScriptEngine->Events.OnLine("RX", a->name, a->callBackLua, arg, NULL);
            if (result == MAC_RESULT_BREAK) {
                Log_DBG("[CPRR DRIVER] LUA result BREAK");
                CPRR24_StreamingExit(SE_FRAME_LUA_BREAK);
			}
        }
    }
}

int CPRR24_StreamFrameEvent(TCPRR24StreamerEventId eventId, char * data, int size, int headerSize) {
    static char outdata[255];
    TLineRec* a;
    // выход, если стриминг отключен 
    if (streamingExit) return 0;
#if 0
    eventFrameCount++;
#else
    // Log_DBG("[CPRR24_StreamFrameEvent] eventId : %d (SIZE:%d (header:%d))", eventId, size, headerSize);
    if (eventId == CPRR24_EVENT_FRAME_DONE) {
        // Счетчик принятых кадров
        eventFrameCount++;
        int RxSize = 0;
        TMACDriverArg arg = {.prIO.inBuff = (char*) data, .prIO.inCount = headerSize, .prIO.pLightuserdata = NULL, .prIO.inCountEx = size, .prIO.outBuff = (char*) &outdata[0],
        .prIO.outBuffLimit = sizeof(outdata)-1,  .prIO.outBuffCount = &RxSize};
        a = SystemState.LLC.Lines[LineIndex];
        if (a->callBackLua) {
            char * pHeader = &data[0];
        //Log_DBG("[CPRR DRIVER DBG] FRAME_DONE...--> LUA [size:%d]", size);
            
            //Log_DBG("[DBG!!!] Events.OnLine(%p, %d, %d) --> ", data, headerSize, size);
            int result = SystemState.pScriptEngine->Events.OnLine("RX", a->name, a->callBackLua, arg, NULL);
            //Log_DBG("[DBG!!!] Events.OnLine --> DONE");

            if (result == MAC_RESULT_BREAK) {
                Log_DBG("[CPRR DRIVER] LUA result BREAK");
                CPRR24_StreamingExit(SE_FRAME_LUA_BREAK);
            } else if (result == MAC_RESULT_IGNORE) {
                // Log_DBG("[CPRR DRIVER] LUA result IGNORE");
                // ничего не делаем
            } else if (result == MAC_RESULT_OK) {
                // запись / обработка кадра
                if (RxSize) {
#    if 0               // запретил менять заголовок     
                    //скрипт поменял содержимое заголовка 
                    pHeader = &outdata[0];
                    headerSize = RxSize;
#    else
                    Log_DBG("[CPRR DRIVER] IGNORE (#if 0) HEADER CHANGE. (new size:%d)", RxSize);
#    endif                    
                }
                // TODO: обработка / запись заголовка (актуально для rosbag)
                // заголовок не записываем в файл
                int bodySize = size - headerSize;
                // запись в файл
                //Log_DBG("[DBG!!!] CPRR24_StreamWrite !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                //Log_DBG("[DBG!!!] CPRR24_StreamWrite(%p[%d], %d, %p, %d) --> ", data, headerSize, bodySize, pHeader, headerSize);
                int writeResult = CPRR24_StreamWrite(&data[headerSize], bodySize, pHeader, headerSize);
                //Log_DBG("[DBG!!!] CPRR24_StreamWrite --> DONE");
                if (writeResult > 0) writeFrameCount++;
                //Log_DBG("[CPRR DRIVER DBG] writeFrameCount : %d (%d), writeResult : %d", writeFrameCount, writeFrameLimit, writeResult);

                if (writeFrameCount >= writeFrameLimit) {
                    CPRR24_StreamingExit(SE_FRAME_NUM_DONE);
                }
                return writeResult; // кадр записан/обработан
            }
        }
    } else {
        if ((eventId == CPRR24_EVENT_TIMEOUT) || (eventId == CPRR24_EVENT_ERROR)) {
            CPRR24_SetErrorStatusLua();
        }
    }
#endif
    return 0; // Frame ignore
}

int CPRR24_SaveStreamFile(char * IP, unsigned int dstPort, unsigned int srcPort, char * fileName, char appendMode, int frameNum, int streamMode) {

    eventFrameCount = 0;
    writeFrameCount = 0;
    streamingExit = 0;
    writeFrameLimit = frameNum;

    int timeout = CPRR_STREAMING_TIMEOUT;
    TLineRec* a;
    a = SystemState.LLC.Lines[LineIndex];
    if (a)
        if (a->LineControl.timeOutOption)
            timeout = a->LineControl.timeOutOption;
        
    
    Log_DBG("[CPRR DRIVER] Radar to STREAM mode (File:%s, Num:%d, Mode:%d)", fileName, frameNum, streamMode);

    if (CPRR24_StreamWriteOpen(fileName, appendMode) == 0) {
        cprr24StreamerStart(IP, dstPort, srcPort, CPRR24_StreamFrameEvent, CPRR24_T25Event);
        usleep(50 * 1000);
        uint32_t CmdGetVer = RADAR_REQ_GETVER;
        cprr24StreamerSend(RADAR_PKT_REQUEST_ID, (char*) &CmdGetVer, sizeof (CmdGetVer));
        usleep(50 * 1000);
        TStreamerCmdBuffer streamerCmdON = {.Power = streamMode, .Streaming = 1};
        cprr24StreamerSend(CPRR_CMD_ID_SET_MODE, (char*) &streamerCmdON, sizeof (streamerCmdON));
        while (streamingExit == 0) {
            uint32_t fCount = eventFrameCount;
            // Log_DBG("[CPRR DRIVER] WAIT...");
            for (int i = 0; i < timeout / 10; i++) {
                usleep(10 * 1000);
                if (streamingExit) break;
            }
            if ((streamingExit == 0)&&(fCount == eventFrameCount)) {
                // на 1 сек не принято кадров! сообщаем о таймауте
                Log_DBG("[CPRR DRIVER] TIMEOUT! [%d ms]", timeout);
                CPRR24_SetErrorStatusLua();
            }
        }
        TStreamerCmdBuffer streamerCmdOFF = {.Power = 0, .Streaming = 0};
        cprr24StreamerSend(CPRR_CMD_ID_SET_MODE, (char*) &streamerCmdOFF, sizeof (streamerCmdOFF));
        usleep(150 * 1000);
        cprr24StreamerStop();
        CPRR24_StreamWriteClose();
        Log_DBG("[CPRR DRIVER] STREAM DONE (Write:%d, Total:%d)", writeFrameCount, eventFrameCount);
        return writeFrameCount;
    } else {
        Log_DBG("[CPRR DRIVER] FILE OPEN ERROR! [%s]", fileName);

    }
}

typedef int (*TCPRR24SendFunc)(uint32_t type, char * cmdBuff, unsigned int size);

static TCPRR24SendFunc SendFunc = NULL;

int CPRR24_Tester(char * IP, unsigned int dstPort, unsigned int srcPort, int frameNum) {
    static char outdata[255];
    TLineRec* a;
    int RxSize = 0;

    eventFrameCount = 0;
    writeFrameCount = 0;
    streamingExit = 0;

    Log_DBG("[CPRR DRIVER] Radar to TEST mode..  CTRL + C for Exit");
    cprr24TesterStart(IP, dstPort, srcPort);
    while (streamingExit == 0) {
        usleep(1000 * 1000);
        TMACDriverArg arg = {.prIO.inBuff = (char*) CPRR24Tester_headerBuffer, .prIO.inCount = CPRR24Tester_headerSize, .prIO.pLightuserdata = NULL, .prIO.outBuff = (char*) &outdata[0], .prIO.outBuffCount = &RxSize};
        a = SystemState.LLC.Lines[LineIndex];
        if (a->callBackLua) {
            SystemState.pScriptEngine->Events.OnLine("RX", a->name, a->callBackLua, arg, NULL);
        }
        if (frameNum > 0) // -1 - бесконечно
            if (CPRR24Tester_RxCount > frameNum) break;
    }
    Log_DBG("[CPRR TEST] Done. Total rx: %d", CPRR24Tester_RxCount);
    cprr24TesterStop();
    return 0;
}

int CPRR24_T25(char * IP, unsigned int dstPort, unsigned int srcPort, int frameNum, int mode) {
    static char outdata[255];
    TLineRec* a;
    int RxSize = 0;

    eventFrameCount = 0;
    writeFrameCount = 0;
    streamingExit = 0;

    Log_DBG("[CPRR DRIVER] Radar to TARGET mode..  CTRL + C for Exit");
    cprr24T25Start(IP, dstPort, srcPort, CPRR24_T25Event);
    TStreamerCmdBuffer t25CmdON = {.Power = mode, .Streaming = 1};
    TStreamerCmdBuffer t25CmdOFF = {.Power = 0, .Streaming = 1};
    cprr24T25Send(CPRR_CMD_ID_SET_MODE, (char*) &t25CmdON, sizeof (t25CmdON));

    while (streamingExit == 0) {
        usleep(10 * 1000);
        // Log_DBG("[CPRR TEST] DATA PKT ");
        if (frameNum > 0) // -1 - бесконечно
            if (CPRR24T25_RxCount > frameNum) break;
    }
    Log_DBG("[CPRR TEST] Done. Total rx: %d", CPRR24Tester_RxCount);
    cprr24T25Send(CPRR_CMD_ID_SET_MODE, (char*) &t25CmdOFF, sizeof (t25CmdOFF));
    cprr24T25Stop();
    return 0;
}

int CPRR24_DRV_CallBack(TMCExtDriver * event) {
    static char IP[100] = "192.168.1.4";
    static int dstPort = 7000;
    static int srcPort = 0;
    int result = 0;

    char idEvent[100] = "ERROR!";
    if (event->id == MAC_ID_WRITE_BUFFER) sprintf(idEvent, "TX");
    if (event->id == MAC_ID_READ_BUFFER) sprintf(idEvent, "RX");
    if (event->id == MAC_ID_OPEN) sprintf(idEvent, "INIT");
    if (event->id == MAC_ID_POLL) sprintf(idEvent, "POLL");
    if (event->id == MAC_ID_CLOSE) sprintf(idEvent, "CLOSE");
    if (event->id == MAC_ID_CTRL) sprintf(idEvent, "CTRL");



    if (event->id == MAC_ID_OPEN) {
        // сохраним идентификатор линии, что бы потом вызывать Lua обработчик.
        LineIndex = event->LineIndex;

        // for (int i = 0; i < SystemState.LLC.count; i++) Log_DBG("CPRR> LINE: %s", SystemState.LLC.Lines[i]->name);

        strncpy(IP, event->param.prOPEN.device, sizeof (IP));
        //Может быть задана пара значений ["7000;7001" | "7000"]
        int sepIndex = -1;
        for (int i = 0; i < strlen(event->param.prOPEN.param); i++) {
            if (event->param.prOPEN.param[i] == ';') {
                sepIndex = i;
                break;
            }
        }
        if (sepIndex == -1) {
            // только один параметр
            dstPort = atoi(event->param.prOPEN.param);
        } else {
            // два параметра
            char dstPortStr[10] = "0";
            if (sepIndex < sizeof (dstPortStr)) {
                strncpy(dstPortStr, event->param.prOPEN.param, sepIndex);
            }
            dstPort = atoi(dstPortStr);
            srcPort = atoi(&event->param.prOPEN.param[sepIndex + 1]);
        }
        Log_DBG("CPRR>OPEN[%d] : [IP:%s, port:%d[%d]]\n", LineIndex, IP, dstPort, srcPort);
        result = 1;
        return result;
    }
    if (event->id == MAC_ID_CTRL) {
        // Различные форматы стриминга. Общий формат: "STREAM:3", где 3 - код режима стриминга
        // "STREAM" - по умолчанию код стриминга 2.
#define STREAM_MODE_ID  "STREAM"
        int strGroupLen = strlen(STREAM_MODE_ID);
        int extMode = 2; // согласно ПИВ ()
        if (strncmp(event->param.prCTRL.ctrlCommand, STREAM_MODE_ID, strGroupLen) == 0) {
            if (event->param.prCTRL.ctrlCommand[strGroupLen] == ':') {
                extMode = atoi(&event->param.prCTRL.ctrlCommand[strGroupLen + 1]);
            }
            SendFunc = NULL;
            //prCTRL.device[0] - mode : W - Запись файла в режиме перезаписи
            //                        : A - Запись файла в режиме дополнения
            //                        : иначе, часть имени файла 
            // для пользователей, которые забыли про указание режима
            int nameOffset = 0;
            char appendMode = event->param.prCTRL.device[0];
            if (appendMode == '+') {
                // Если первый символ "+" - это режим дополнения файла
                nameOffset++;
            } else {
                appendMode = 0;
            }
            CPRR24_SaveStreamFile(IP, dstPort, srcPort, &event->param.prCTRL.device[nameOffset], appendMode, atoi(event->param.prCTRL.param), extMode);
            result = writeFrameCount;
        }
        if (strcmp(event->param.prCTRL.ctrlCommand, "TESTER") == 0) {
            SendFunc = cprr24TesterSend;
            CPRR24_Tester(IP, dstPort, srcPort, atoi(event->param.prCTRL.param));
            result = 0;
        }
		
#define T25_MODE_ID  "T25"
        strGroupLen = strlen(T25_MODE_ID);
		extMode = 1;
        if (strncmp(event->param.prCTRL.ctrlCommand, T25_MODE_ID, strGroupLen) == 0) {
            if (event->param.prCTRL.ctrlCommand[strGroupLen] == ':') {
                extMode = atoi(&event->param.prCTRL.ctrlCommand[strGroupLen + 1]);
            }
            SendFunc = cprr24T25Send;
            CPRR24_T25(IP, dstPort, srcPort, atoi(event->param.prCTRL.param), extMode);
            result = CPRR24T25_RxCount;
        }
    }

    if (event->id == MAC_ID_WRITE_BUFFER) {
        result = MAC_RESULT_OK;
        //Log_DBG("CPRR>[MAC_ID_WRITE_BUFFER] N:%d\n", event->param.prTX.inCount);
        if (SendFunc) {
            uint32_t * cmd = (uint32_t *) & event->param.prTX.inBuff[0];
            //Log_DBG("CPRR>[MAC_ID_WRITE_BUFFER] cmd:%d\n", *cmd);
            if (event->param.prTX.inCount >= 4) {
                SendFunc(*cmd, &event->param.prTX.inBuff[4], (unsigned int) (event->param.prTX.inCount - 4));
            }
        }
    }
    if (event->id == MAC_ID_READ_BUFFER) {
        event->param.prRX.rxBuff[0] = '*';
        *event->param.prRX.rxCount = 1;
        Log_DBG("CPRR>[MAC_ID_READ_BUFFER] N:%d\n", 1);
        result = MAC_RESULT_OK;
    }
    if (event->id == MAC_ID_POLL) {
        result = 0;
        usleep(1000);
        Log_DBG("CPRR>[MAC_ID_POLL] \n");
    }
    return result;

}



