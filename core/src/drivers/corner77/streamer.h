/* 
 * File:   streamer.h
 * Author: Dunaev
 *
 * Created on 30 сентября 2021 г., 11:18
 */

#ifndef STREAMER_H
#define STREAMER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>


// Преамбула на уровне LVDS    
#define LVDS_FRAME_PREAMBLE            0x0000B800
// т.к. ПЛИС фильтрует преамбулу LVDS, а она учтена в CRC, инициализируем значением,
// в котором учтен расчет 0x0000B800
#define LVDS_FRAME_CRC_INIT            0x85f5133e
    
#define STREAM_DURATION_PER_FILE       10   // Длительность записи в один файл
#define NUVO_TIMEOUT                   5000 // ожидание данных от NUVO, ms
#define RADAR_STREAM_FRAME_MAX_SIZE    (2*1024*1024) // максимальный размер кадра (с запасом)
#define RADAR_STREAMING_TIMEOUT        5000 // ожидание данных после включения режима, ms
#define RADAR_UDP_PKT_MAX_SIZE         65535 // Максимальный размер пакета UDP (фрагментированый UDP)
#define RADAR_PKT_PREAMBLE             0x0000ABCD
#define CHIRP_PER_FRAME_NUM            128  // кол-во чирпов в одном кадре
//#define RAW_DATA_SIZE                  524288        /*!< Размер сырых данных одного фрейма (только данные АЦП) */
#define RADAR_PROTOCOL_RAWPEAK         0x7777 // протокол выдачи "сырых" пиков
#define RADAR_PROTOCOL_ADCDATA         0x1111 // протокол выдачи АЦП

// Типы пакетов стримера. См. https://docs.google.com/document/d/1DEd7Qm7QqLLI0tUSJH5uZMxdN2cfcEwGTN_KM4877es/edit
#define STMR_PROTOCOL_SENSOR           10

// Data format : bit 0: [0 - Re, Im; 1 - Im, Re]
#define RADAR_DATA_FORMAT_MASK         1
#define RADAR_DATA_FORMAT_ImRe         1    // Формат texas [Im, Re]
#define RADAR_DATA_FORMAT_ReIm         0    // Формат матлаба [Re, Im]

//Быстрые команды запуска/останова сенсора
#define SENSOR_START_CODE   2   //STX
#define SENSOR_STOP_CODE    3   //ETX

    
typedef enum {
    RECORD_OFF = 0x0U, // записи стрима нет
    RECORD_ON = 0x1U   // Началась/идет запись
} TRecordState;

typedef enum {
    RADAR_ID_0 = 0x0U, // сенсор (радара) №1
    RADAR_ID_1 = 0x1U, // сенсор (радара) №2
    RADAR_MAX_NUM      // максимальное кол-во сенсоров
} TRADAR_ID;

typedef struct __attribute__((__packed__)){
     int16_t                    ChirpIndex      ;   //0..кол-во чирпов в кадре - 1 / 0xFFFF - данные не чирпа
    uint32_t                    FrameIndex      ;   //0..FFFFFFFF - уникальная идентификация кадра в сеансе работы
    uint16_t                    dataFormat      ;   //формат данных АЦП
    uint16_t                    dataSource      ;   //Маска приемных каналов
    uint16_t                    samplesNum      ;   //кол-во выборок в чирпе (в каждом блоке данных одинаковое число выборок)
    uint32_t                    serialNum       ;   // SN радара
    uint8_t                     data[]          ;   // Данные 
 } TPrStreamFormat;

 typedef struct __attribute__((__packed__)){
     int16_t                    peaksNum        ;   //0..кол-во пиков в пакете
    uint32_t                    FrameIndex      ;   //0..FFFFFFFF - уникальная идентификация кадра в сеансе работы
    uint16_t                    peaksTotal      ;   //Всего пиков в кадре
    uint32_t                    data[]          ;   // Данные 
 } TPrPeakFormat;

typedef struct __attribute__((__packed__)){
// Начало данных LVDS. Внимание! STX = 0x0000B800 стример удалил! Но она участвует в расчете CRC!
//  uint32_t                    startMarker[2]  ;   //0x0000B800 -> фильтруется стримером
    uint64_t                    timeStamp       ;   // Время, нс
    uint16_t                    dataSize        ;   // размер следующего блока данных (CRC не учитывает)
    uint16_t                    protocol        ;   // идентификатор протокола (0x1111:стример, 0x7777: пики и проч.)
    uint32_t                    reserv          ;   // Резерв для выравнивания
    union {
        TPrStreamFormat         stream;
        TPrPeakFormat           peak;
    };
 } TLvdsFrame;

 typedef struct __attribute__((__packed__)){
    uint32_t                    startMarker     ;   //0x0000ABCD Преамбула протокола
    uint32_t                    length          ;   // длина поля данных. ! CRC в конце тоже учитывается!
    uint32_t                    typePkt         ;   // Данные АЦП/Пиков тип 10 (0x0a)
 } UdpHeadFormat;
 
typedef struct __attribute__((__packed__)){
    UdpHeadFormat               head;               // заголовок датаграммы
    union {    
    // различные форматы сообщений
    TLvdsFrame                  sensor;             // Данные телеметрии сенсора (АЦП/Пики)
    };
    //uint32_t                  crc[2];             // CRC Lane0/Lane1
 } UdpPktFormat;



#ifdef __cplusplus
}
#endif

#endif /* STREAMER_H */

