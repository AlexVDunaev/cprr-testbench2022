//#define UDP_FORCE_MODE //Все что связано с оптимизацией приема по UDP

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "corner77.h"

#include "../../core/coreUInterface.h"
#include "../../main.h"
#include "../../ROS/rosbagInterface.h"



#define CPYC77_STREAMER_TIMEOUT_DEFAULT 5000

//-----------------------------------------------

typedef enum {
    STRMR_REQ_STATUS_READY  = 0x0U, // Запрос статуса готовности стримера к приему
    STRMR_REQ_STATUS_FPS,           // Запрос текущего
} TStreamerStatusReqId;


typedef void (*TStreamerUpStatusCallback)(int radarId, int isUP);
typedef int (*TStreamerReqWriteFrameCallback)(int eventId, int radarId, uint64_t timeStamp, uint64_t frameIndex, uint16_t dataFormat, void * pAdcData, int size, double * pChirpsTime, int serialNum);
int streamerMain(int argc, char ** argv);
void streamerDoExit(void) ;
void streamerSetReqWriteFrame(TStreamerReqWriteFrameCallback cb);
int getStreamerStatus(TStreamerStatusReqId reqId);
void streamerSetRecord(char onoff);
void streamerSetFilename(char * filename);
void streamerSetUpCallback(TStreamerUpStatusCallback upCallback);

// Режим буферизации (очередь кадров) включен по умолчанию.
// Однако, если вести обработку на лету, без сохранения, очерь может забиться.
// Для такого режима нужно отключить режим буфера кадров
void streamerQueueEnable(char onoff);

typedef enum {
    EXPORT_EVENT_DATA   = 0x0U, // Данные
    EXPORT_EVENT_EMPTY  = 0x1U, // Буфер пуст
    EXPORT_EVENT_FULL   = 0x2U, // Буфер полный
    EXPORT_EVENT_ERROR  = 0x3U  // Ошибка
} TExporterEventId;

//-----------------------------------------------



static int LineIndex = 0;
static uint32_t eventFrameCount = 0;
static uint32_t writeFrameCount = 0;
static uint32_t writeFrameLimit = 0;
static uint32_t streamerTimeOut = CPYC77_STREAMER_TIMEOUT_DEFAULT;


void CPYC77_SetErrorStatusLua() {
    static char outdata[255];
    TLineRec* a;
    int RxSize = 0;
    TMACDriverArg arg = {.prIO.inBuff = (char*) "", .prIO.inCount = 0, .prIO.pLightuserdata = NULL, .prIO.outBuff = (char*) &outdata[0], .prIO.outBuffCount = &RxSize};
    a = SystemState.LLC.Lines[LineIndex];
    if (a->callBackLua) {
        //Log_DBG("[CPYC DRIVER] Error status -> Lua");
		int userResult = SystemState.pScriptEngine->Events.OnLine("RX", a->name, a->callBackLua, arg, NULL);
        if (userResult == MAC_RESULT_BREAK) {
            Log_DBG("[CPYC DRIVER] Lua -> BREAK");
			streamerDoExit();
        }
        if (userResult == MAC_RESULT_RETRY) {
            Log_DBG("[CPYC DRIVER] Lua -> RETRY");
			streamerSetRecord(1);
        }
    }
}

// TODO: сделать сообщение о поднятии стримера
void CPYC77_SetStreamerUpStatusLua(int radarId, int isUP) {
    static char outdata[255];
    TLineRec* a;
    int RxSize = 0;
    TMACDriverArg arg = {.prIO.inBuff = (char*) "", .prIO.inCount = 0, .prIO.pLightuserdata = NULL, .prIO.outBuff = (char*) &outdata[0], .prIO.outBuffCount = &RxSize};
    a = SystemState.LLC.Lines[LineIndex];
    if (a->callBackLua) {
		SystemState.pScriptEngine->Events.OnLine((isUP)? "UP" : "DOWN", a->name, a->callBackLua, arg, NULL);
    }
}


static volatile char streamerTimeoutDoExit = 0;
static void* streamerTimeout_thread(void * arg) {
	streamerTimeoutDoExit = 0;
	Log_DBG("Starting streamer timeout thread. [timeout:%d]\n", streamerTimeOut);
	while(!streamerTimeoutDoExit) {
		for (int i = 0; i < streamerTimeOut /100; i++) {
			if (streamerTimeoutDoExit) break;
			usleep(100*1000);
			if (getStreamerStatus(STRMR_REQ_STATUS_FPS) > 0) i = 0;
		}
		// В течении CPYC77_STREAMER_TIMEOUT не было приема! Фиксируем тайм-аут
		Log_DBG("******************  streamerTimeout_thread Timeout *****************\n");
		if (!streamerTimeoutDoExit)
			CPYC77_SetErrorStatusLua();
	}	
	Log_DBG("******************  streamerTimeout_thread end *****************\n");
	return NULL;
}


//по сути запуск программы стримера (CornerStreamer) с небольшим API для 
// управления (запуск стрима / остановка, выход из приложения)
static void* streamer_thread(void * arg) {
	Log_DBG("******************  streamer_thread start *****************\n");
	char * str[2] = {"", "./streamer.conf"};
	streamerMain(2, str);
	Log_DBG("******************  streamer_thread end *****************\n");
}

int streamerReqWriteFrame(int eventId, int radarId, uint64_t timeStamp, uint64_t frameIndex, uint16_t dataFormat, void * pAdcData, int size, double * pChirpsTime, int serialNum) {
	struct __attribute__((__packed__)) {
		uint32_t radarId;
		uint64_t timeStamp;
		uint64_t frameIndex;
		uint32_t serialNum;
	} header;
    static char outdata[255];
    TLineRec* a;
    // выход, если стриминг отключен 
    if (eventId == EXPORT_EVENT_DATA) {
		// Формируем структуру данных для скрипта
		header.radarId = radarId;
		header.timeStamp = timeStamp;
		header.frameIndex = frameIndex;
		header.serialNum = serialNum;
		// ------
        // Счетчик принятых кадров
        eventFrameCount++;
        int RxSize = 0;
        TMACDriverArg arg = {.prIO.inBuff = (char*) &header, .prIO.inCount = sizeof(header), .prIO.pLightuserdata = pAdcData, .prIO.inCountEx = size, .prIO.outBuff = (char*) &outdata[0],
        .prIO.outBuffLimit = sizeof(outdata)-1,  .prIO.outBuffCount = &RxSize};
        a = SystemState.LLC.Lines[LineIndex];
        if (a->callBackLua) {
            
            //Log_DBG("[DBG!!!] Events.OnLine(%p, %d, %d) --> ", data, headerSize, size);
            int result = SystemState.pScriptEngine->Events.OnLine("RX", a->name, a->callBackLua, arg, NULL);
            //Log_DBG("[DBG!!!] Events.OnLine --> DONE");

            if (result == MAC_RESULT_BREAK) {
                Log_DBG("[DRIVER] LUA result BREAK");
				streamerSetRecord(0);
				streamerDoExit();

            } else if (result == MAC_RESULT_IGNORE) {
                // Log_DBG("[CPRR DRIVER] LUA result IGNORE");
                Log_DBG("[DRIVER] LUA result MAC_RESULT_IGNORE[%d (%d/%d)]", eventFrameCount, writeFrameCount, writeFrameLimit);
                // ничего не делаем
            } else if (result == MAC_RESULT_OK) {
                // запись / обработка кадра
				writeFrameCount++;
                //Log_DBG("[DRIVER] LUA result MAC_RESULT_OK[%d/%d]", writeFrameCount, writeFrameLimit);
				if (writeFrameCount >= writeFrameLimit) {
					streamerSetRecord(0);
					streamerDoExit();
				}
			    return 1; // Frame writeing to rosbag
            }
        }
    } else {
		//
    }
    return 0; // Frame ignore
}


int CPYC77_SaveStreamFile(char * IP, unsigned int dstPort, unsigned int srcPort, char * fileName, char appendMode, int frameNum, int isUseQueue) {
	pthread_t streamerThread;
	pthread_t streamerTimeOutThread;
	Log_DBG("[SaveStreamFile]> : File: '%s' [%s], Frame limit: %d. Queue:%s\n", fileName, (appendMode)? "APPEND" : "REWRITE", frameNum, (isUseQueue)? "ON" : "OFF");

	eventFrameCount = 0;
	writeFrameCount = 0;
	writeFrameLimit = frameNum;
	
	// Используем значение таймаута, которое пользователь может изменить через: mcFunc("TIMEOUT", 5000)
    TLineRec* a = SystemState.LLC.Lines[LineIndex];
    if (a)
        if (a->LineControl.timeOutOption)
            streamerTimeOut = a->LineControl.timeOutOption;
	
	// Управление очередью. Если очередь включена, то пропусков кадров нет, 
	// но нельзя долго захватывать управление! иначе очередь переполниться.
	// Если отключить очередь, то кадры данных будут просто игнорироваться пока
	// Lua скрипт не вернет управление
	streamerQueueEnable(isUseQueue);
	streamerSetReqWriteFrame(streamerReqWriteFrame);
	streamerSetFilename(fileName);
	streamerSetUpCallback(CPYC77_SetStreamerUpStatusLua);
		
	if (pthread_create(&streamerThread, NULL, streamer_thread, NULL) == 0) {
		 // pStreamerData->isBufferReaderRun = 1;
	 } else {
		 Log_DBG("[] Started Streamer thread Error!\n");
		 return 0;
	 }
	// Ожидание запуска стримера (инициализация всех драйверов и готовности к приему)
	while(1) {
		usleep(10000);
		if (getStreamerStatus(STRMR_REQ_STATUS_READY))
			break;
		Log_DBG("Wait streamer ready...\n");
	}
	Log_DBG("Start in 1 s...\n");
	sleep(1);
	// Запускаем поток контроля таймаута
	pthread_create(&streamerTimeOutThread, NULL, streamerTimeout_thread, NULL);
	// Даем команду включения ЛЧМ
	streamerSetRecord(1);
	// Ждем завершения работы стримера
	if (pthread_join(streamerThread, NULL)) {
		Log_DBG("join Streamer thread error!\n");
	}
	// Остановим поток контроля таймаута
	streamerTimeoutDoExit = 1;
	pthread_join(streamerTimeOutThread, NULL);
	
	Log_DBG("[SaveStreamFile]> : write count : %d\n", writeFrameCount);
	return writeFrameCount;
}


int CPYC77_DRV_CallBack(TMCExtDriver * event) {
    static char IP[100] = "192.168.1.1";
    static int dstPort = 1772;
    static int srcPort = 0;
    int result = 0;

    char idEvent[100] = "ERROR!";
    if (event->id == MAC_ID_WRITE_BUFFER) sprintf(idEvent, "TX");
    if (event->id == MAC_ID_READ_BUFFER) sprintf(idEvent, "RX");
    if (event->id == MAC_ID_OPEN) sprintf(idEvent, "INIT");
    if (event->id == MAC_ID_POLL) sprintf(idEvent, "POLL");
    if (event->id == MAC_ID_CLOSE) sprintf(idEvent, "CLOSE");
    if (event->id == MAC_ID_CTRL) sprintf(idEvent, "CTRL");

    if (event->id == MAC_ID_OPEN) {
        // сохраним идентификатор линии, что бы потом вызывать Lua обработчик.
        LineIndex = event->LineIndex;
        strncpy(IP, event->param.prOPEN.device, sizeof (IP));
        //Может быть задана пара значений ["7000;7001" | "7000"]
        int sepIndex = -1;
        for (int i = 0; i < strlen(event->param.prOPEN.param); i++) {
            if (event->param.prOPEN.param[i] == ';') {
                sepIndex = i;
                break;
            }
        }
        if (sepIndex == -1) {
            // только один параметр
            dstPort = atoi(event->param.prOPEN.param);
        } else {
            // два параметра
            char dstPortStr[10] = "0";
            if (sepIndex < sizeof (dstPortStr)) {
                strncpy(dstPortStr, event->param.prOPEN.param, sepIndex);
            }
            dstPort = atoi(dstPortStr);
            srcPort = atoi(&event->param.prOPEN.param[sepIndex + 1]);
        }
        Log_DBG("CPYC>OPEN[%d] : [IP:%s, port:%d[%d]]\n", LineIndex, IP, dstPort, srcPort);
        result = 1;
        return result;
    }
    if (event->id == MAC_ID_CTRL) {
        // Различные форматы стриминга. Общий формат: "STREAM:3", где 3 - код режима стриминга
        // "STREAM" - по умолчанию код стриминга 2.
#define STREAM_MODE_ID  "STREAM"
        int strGroupLen = strlen(STREAM_MODE_ID);
		int isUseQueue = 0;
        if (strncmp(event->param.prCTRL.ctrlCommand, STREAM_MODE_ID, strlen(STREAM_MODE_ID)) == 0) {
            if (event->param.prCTRL.ctrlCommand[strGroupLen] == ':') {
                isUseQueue = atoi(&event->param.prCTRL.ctrlCommand[strGroupLen + 1]);
            }
			
            int nameOffset = 0;
            char appendMode = event->param.prCTRL.device[0];
            if (appendMode == '+') {
                // Если первый символ "+" - это режим дополнения файла
                nameOffset++;
            } else {
                appendMode = 0;
            }
            result = CPYC77_SaveStreamFile(IP, dstPort, srcPort, &event->param.prCTRL.device[nameOffset], appendMode, atoi(event->param.prCTRL.param), isUseQueue);
        }
    }

    if (event->id == MAC_ID_WRITE_BUFFER) {
        result = MAC_RESULT_OK;
    }
    if (event->id == MAC_ID_READ_BUFFER) {
        event->param.prRX.rxBuff[0] = '*';
        *event->param.prRX.rxCount = 1;
        Log_DBG("CPYC>[MAC_ID_READ_BUFFER] N:%d\n", 1);
        result = MAC_RESULT_OK;
    }
    if (event->id == MAC_ID_POLL) {
        result = 0;
        usleep(1000);
        Log_DBG("CPYC>[MAC_ID_POLL] \n");
    }
    return result;

}



