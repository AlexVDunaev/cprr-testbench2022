
#include "t25Imit.h"
#include <sys/poll.h>
#include <string.h>
#include "../../main.h"

typedef enum {
    RECORD_OFF = 0x0U, // записи стрима нет
    RECORD_ON = 0x1U   // Началась/идет запись
} TRecordState;

typedef enum {
    DAQ_CMD_ID_RAW = 0x0U,    // Выдача данных как есть
    DAQ_CMD_ID_VER = 0x1U,    // Загрузить данные версии
    DAQ_CMD_ID_TARGET = 0x2U  // Выдать в DAQ время/id кадра
} TDAQCMD_CodeId;


static int LineIndex = 0;   // индекс драйвера в системе

static TNuvoDriverData nuvoDriverData = {.initDone = 0};
// Секретные параметры от Сергея Ч.
uint64_t protokol_ver = 1;
uint64_t radar_ver = 10;

static target_t target_0;
static target_t target_1;

static void SetTarget1(target_t* target) {
    target->traget_id = 1;
    target->range = 1;
    target->azimut = 1;
    target->live_time = 1;
    target->prediction_time = 1;
    target->rcs = 1;
    target->Xcord = 10;
    target->Xrate = 1;
    target->Xacceleration = 1;
    target->Ycord = 10;
    target->Yrate = 1;
    target->Yacceleration = 1;
};

static void SetTarget0(target_t* target) {
    target->traget_id = 0;
    target->range = 0;
    target->azimut = 0;
    target->live_time = 0;
    target->prediction_time = 0;
    target->rcs = 0;
    target->Xcord = 0;
    target->Xrate = 0;
    target->Xacceleration = 0;
    target->Ycord = 1;
    target->Yrate = 0;
    target->Yacceleration = 0;
};

static int SendVersionToNuvo(TNuvoDriverData * pDrv) {
    NuvoVersion_t message;
    
    if (pDrv == NULL) 
        return 0;
    
    message.protocol = protokol_ver;
    message.radar = radar_ver;
    int result = sendto(pDrv->sock, &message, sizeof (message),
            MSG_WAITALL, (struct sockaddr *) &pDrv->sockaddr, sizeof (pDrv->sockaddr));

    if (result <= 0) {
        Log_DBG("[T25Imit] UDP send error! (version packet. code:%d)\n", result);
    }
    return (result > 0) ? 0 : -1;
};

static int SendMesageToNuvo(TNuvoDriverData * pDrv, uint8_t msg, uint64_t timeStamp, uint64_t grubNum) {
    NuvoMsg_t message;
    if (pDrv) {
        if (pDrv->nuvoIsOnline) {
            message.time_stamp = (uint64_t) timeStamp;
            message.grub_num = (uint64_t) grubNum;

            message.target = (msg) ? target_1 : target_0;

            int result = sendto(pDrv->sock, &message, sizeof (message),
                    MSG_WAITALL, (struct sockaddr *) &pDrv->sockaddr, sizeof (pDrv->sockaddr));

            static int isUdpSendError = 0;

            if (result <= 0) {
                if (isUdpSendError == 0)
                    Log_DBG("[T25Imit] UDP send error! (target packet. code:%d)\n", result);
                isUdpSendError = 1;
            } else {
                isUdpSendError = 0;
            }
            return (result > 0) ? 0 : -1;
        }
    }
    return 0;
};

static int SendMesageToNuvoRaw(TNuvoDriverData * pDrv, uint8_t * buffer, int size) {
    if (pDrv) {
            int result = sendto(pDrv->sock, buffer, size,
                    MSG_WAITALL, (struct sockaddr *) &pDrv->sockaddr, sizeof (pDrv->sockaddr));

            static int isUdpSendError = 0;

            if (result <= 0) {
                if (isUdpSendError == 0)
                    Log_DBG("[T25Imit] UDP send error! (target packet. code:%d)\n", result);
                isUdpSendError = 1;
            } else {
                isUdpSendError = 0;
            }
            return (result > 0) ? 0 : -1;
    }
    return 0;
};

static void* nuvoReader_thread(void * arg) {
    volatile TNuvoDriverData * pDrv = arg;
    NuvoTypeValue buffer[10];
    int timeOutCount = 0;
    struct pollfd nuvo;
    int sockErrorCount = 0;
    int sock;

    while (pDrv->needStop == 0) {
    
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        Log_ERR("[T25Imit] Open socket error! (%d)\n", sock);
        return NULL;
    }

    if (connect(sock, (struct sockaddr *) &pDrv->sockaddr, sizeof (pDrv->sockaddr)) < 0) {
        Log_ERR("[T25Imit] UDP: Connect Failed!\n");
        return NULL;
    }

    pDrv->sock = sock;
    if (sockErrorCount)
        Log_DBG("[T25Imit] reInit\n");

    while (pDrv->needStop == 0) {
        nuvo.revents = 0;
        nuvo.fd = pDrv->sock;
        nuvo.events = POLLIN | POLLERR;
        int k = poll(&nuvo, 1, NUVO_TIMEOUT);
        // При закрытии приложения и сокета k -> -1. В этом случае просто выход.
        if (pDrv->needStop == 0) {
            if (k < 0) {
                Log_DBG("[T25Imit] poll error! %d\n", k);
            } else {
                if (k) {
                    if (nuvo.revents & (POLLIN)) {
                        // есть ответ от nuvo
                        int n = read(pDrv->sock, (void*) &buffer, sizeof (buffer));
                        if (n) {
                            timeOutCount = 0;
                            // Log_DBG("[NUVO DRIVER] RX Size : %d\n", n);
                            pDrv->nuvoIsOnline = 1;
                            if (pDrv->callBack) {
                                if (n == 8) {
                                    pDrv->callBack(EU_ID_PARAM_ENABLED, buffer[0]); //int64
                                }
                                if (n == 24) {
                                    pDrv->callBack(EU_ID_PARAM_SPEED, buffer[0]); //double
                                    pDrv->callBack(EU_ID_PARAM_YAW_SPEED, buffer[1]); //double
                                    pDrv->callBack(EU_ID_PARAM_FORWARD, buffer[2]); //int64
                                }
                            }
                        }
                    } else {
                        if (pDrv->needStop == 0) {
                            close(pDrv->sock);
                            Log_ERR("[T25Imit] SOCK ERROR! (%d)\n", ++sockErrorCount);
                            for (int i = 0; i < 100; i++)
                                if (pDrv->needStop == 0)
                                    usleep(10*1000);
                            break;
                        }
                    }
                } else {
                    timeOutCount++;
                    if (timeOutCount > 1) {
                        Log_DBG("[T25Imit] TIMEOUT! (%d)\n", timeOutCount - 1);
                        pDrv->nuvoIsOnline = 0;
                    }
                    SendVersionToNuvo((TNuvoDriverData *) pDrv);
                }
            }
        }
    }
    }
    Log_DBG("[T25Imit] DONE!\n");
    return NULL;
}

static TNuvoDriverData * nuvoStart(char * IP, unsigned int dstPort, NuvoUserEvent * callBack) {

    memset((void*) &nuvoDriverData, 0, sizeof (nuvoDriverData));

    // Настройка sockaddr для sendto()
    nuvoDriverData.sockaddr.sin_family = AF_INET;
    nuvoDriverData.sockaddr.sin_addr.s_addr = inet_addr(IP);
    nuvoDriverData.sockaddr.sin_port = htons(dstPort);

    nuvoDriverData.callBack = callBack;
    nuvoDriverData.initDone = 1;

    // Создание потока опроса NUVO

    if (pthread_create(&nuvoDriverData.nuvoListener, NULL, nuvoReader_thread, &nuvoDriverData) == 0) {
        nuvoDriverData.isListenerRun = 1;
    } else {
        Log_ERR("[T25Imit] Started thread Error!\n");
        return NULL;
    }

    // маркеры целей для отправки t25imit
    SetTarget0(&target_0);
    SetTarget1(&target_1);

    // Успешная инициализация
    return &nuvoDriverData;

}

static void nuvoStop(TNuvoDriverData * pDrv) {
    if (pDrv) {
        if (pDrv->initDone) {
            // остановка потоков
            pDrv->needStop = 1;
            // остановка сокета (выход из recv/poll)
            shutdown(pDrv->sock, SHUT_RD);
            close(pDrv->sock);
            // Ожидаем завершения потоков
            if (pDrv->isListenerRun)
                if (pthread_join(pDrv->nuvoListener, NULL)) {
                    Log_ERR("[T25Imit] join Listener thread error!\n");
                }
            pDrv->initDone = 0;
            Log_DBG("[T25Imit] CLOSE\n");
        } else {
            Log_DBG("[T25Imit] CLOSE IGNORE (CLOSED)...\n");
        }
    }
}

// Состояние стримера: пишем данные или нет
static TRecordState isRecordData = RECORD_OFF;

static TNuvoDriverData * t25ImitDriver = NULL;

static void t25ImitCallback(NuvoEvents idEvent, NuvoTypeValue value) {
    TLineRec* a;
    int RxSize = 0;
    static char outdata[1024]; // резерв
	
    if (idEvent == EU_ID_PARAM_ENABLED) {
        
        isRecordData = (value.i64 > 0) ? RECORD_ON : RECORD_OFF;
        
        // Команда включения/отключения записи
        Log_DBG("[T25Imit] 'ENABLED' : %d\n", (int) value.i64);
        
        char state = (isRecordData == RECORD_ON) ? 1: 0;        
    
        TMACDriverArg arg = {.prIO.inBuff = (char*) &state, .prIO.inCount = 1, .prIO.inCountEx = 1, .prIO.pLightuserdata = NULL, .prIO.outBuff = (char*) &outdata[0], .prIO.outBuffCount = &RxSize};
        a = SystemState.LLC.Lines[LineIndex];
        if (a->callBackLua) {
            Log_DBG("[T25Imit] OnLine:'RX' : %s\n", a->name);
            SystemState.pScriptEngine->Events.OnLine("RX", a->name, a->callBackLua, arg, NULL);
        }
    }
    // Остальные параметры не актуальны для CPRR, однако сделаю их доставку в скрипт
    if (idEvent == EU_ID_PARAM_SPEED) {
		struct __attribute__ ((packed)) {
			char  id;
			float value	;
		} param;
		
		param.id = (char)idEvent;
		param.value = value.f64;
		
        TMACDriverArg arg = {.prIO.inBuff = (char*) &param, .prIO.inCount = sizeof(param), .prIO.inCountEx = 0, .prIO.pLightuserdata = NULL, .prIO.outBuff = (char*) &outdata[0], .prIO.outBuffCount = &RxSize};
        a = SystemState.LLC.Lines[LineIndex];
        if (a->callBackLua) {
            SystemState.pScriptEngine->Events.OnLine("RX", a->name, a->callBackLua, arg, NULL);
        }
		
    }
}


int DAQ_DRV_CallBack(TMCExtDriver * event) {
    static char IP[100] = "127.0.0.1";
    static int dstPort = 8081;
    int result = 0;

    char idEvent[100] = "ERROR!";
    if (event->id == MAC_ID_WRITE_BUFFER) sprintf(idEvent, "TX");
    if (event->id == MAC_ID_READ_BUFFER) sprintf(idEvent, "RX");
    if (event->id == MAC_ID_OPEN) sprintf(idEvent, "INIT");
    if (event->id == MAC_ID_POLL) sprintf(idEvent, "POLL");
    if (event->id == MAC_ID_CLOSE) sprintf(idEvent, "CLOSE");
    if (event->id == MAC_ID_CTRL) sprintf(idEvent, "CTRL");

    if (event->id == MAC_ID_CLOSE) {
        Log_DBG("DAQ>CLOSE...\n");
        if (t25ImitDriver)
            nuvoStop(t25ImitDriver);
    }


    if (event->id == MAC_ID_OPEN) {
        LineIndex = event->LineIndex;
        
        //IP
        strncpy(IP, event->param.prOPEN.device, sizeof (IP));
        //Порт
        dstPort = atoi(event->param.prOPEN.param);
        Log_DBG("DAQ>OPEN : [IP:%s, port:%d]\n", IP, dstPort);
        t25ImitDriver = nuvoStart(IP, dstPort, &t25ImitCallback);
        return result;
    }
    if (event->id == MAC_ID_CTRL) {
        Log_DBG("DAQ>MAC_ID_CTRL\n");
    }

    if (event->id == MAC_ID_WRITE_BUFFER) {
        result = MAC_RESULT_OK;
        char cmdid = event->param.prTX.inBuff[0];
        int size = event->param.prTX.inCount - 1;
        if (cmdid == DAQ_CMD_ID_RAW) {
            if (size > 0)
                SendMesageToNuvoRaw(t25ImitDriver, &event->param.prTX.inBuff[1], size);
        }
        if (cmdid == DAQ_CMD_ID_VER) {
            uint64_t * arg = (uint64_t *) & event->param.prTX.inBuff[1];
            if (size >= 16) {
                protokol_ver = arg[0];
                radar_ver = arg[1];
                Log_DBG("DAQ> SET VERSION : [protokol:%ld, radar:%ld] : %d\n", protokol_ver, radar_ver);
                SendVersionToNuvo(t25ImitDriver);
            }
        }
        
        if (cmdid == DAQ_CMD_ID_TARGET) {
            uint64_t * arg = (uint64_t *) & event->param.prTX.inBuff[1];
            if (size >= 16) {
                Log_DBG("DAQ> WRITE : [TIMESTAMP:%ld, FrameIndex:%ld] isRecordData : %d\n", arg[0], arg[1], isRecordData);
                // Посылаем сообщение (с одной целью) что бы DAQ сделал запись в журнал (метку времени)
                SendMesageToNuvo(t25ImitDriver, isRecordData, arg[0], arg[1]);            
            }
        }
        
        
    }
    if (event->id == MAC_ID_READ_BUFFER) {
        event->param.prRX.rxBuff[0] = (isRecordData == RECORD_ON) ? 1: 0;
        *event->param.prRX.rxCount = 1;
        result = MAC_RESULT_OK;
    }
    if (event->id == MAC_ID_POLL) {
        result = 0;
        usleep(1000);
        Log_DBG("DAQ>[MAC_ID_POLL] \n");
    }
    return result;

}


