#ifndef DRIVERNUVO_H
#define DRIVERNUVO_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include "../../core/coreUInterface.h"

#define NUVO_TIMEOUT                   5000 // ожидание данных от NUVO, ms


    typedef enum {
        EU_ID_PARAM_ENABLED = 0x0U, // 
        EU_ID_PARAM_SPEED = 0x1U, // 
        EU_ID_PARAM_YAW_SPEED = 0x2U, // 
        EU_ID_PARAM_FORWARD = 0x3U // 
    } NuvoEvents;

    typedef union {
        int64_t i64;
        double f64;
    } NuvoTypeValue;

    typedef void (NuvoUserEvent) (NuvoEvents idEvent, NuvoTypeValue value);



/*!
 *    @brief Структура хранения версии.
 *
 *     Сохраняет версии протокола и радара в принимаем nuvo формате
 */
typedef struct __attribute__ ((packed))
{
    int64_t  protocol  ; /*!< версия протокола информационного взаимодействия  */
    int64_t  radar     ; /*!< версия радара CPRR  */
  }NuvoVersion_t;
  
/*!
 *    @brief Структура хранения хедера.
 *
 *     Структура хранения хедера пакетов для взаимодействия с nuvo
 */
typedef struct __attribute__ ((packed))
{
    int32_t  type  ;  /*!< тип пакета  */
    int32_t  size  ;  /*!< размер настоящего пакета  */
  }NuvoHeader_t;
  
/*!
 *    @brief Структура конфигурационного пакета.
 *
 *     Структура хранения конфигурационного пакета для взаимодействия с nuvo
 */
typedef struct __attribute__ ((packed))
 {
     NuvoHeader_t      head    ;  /*!< Хедер  */
     int64_t           enable  ;  /*!< команда на включение(старт записи) 1 - вкл, 0 - выкл  */
   }NuvoConfPack_t;
   
/*!
 *    @brief Структура хранения скорости.
 *
 *     Структура хранения собственной скорости комплекса, передает nuvo 
 */
typedef struct __attribute__ ((packed))
{
    double   velocity   ; /*!< Линейная скорость  */
    double   yaw_rate   ; /*!< Угловая скорость  */
    int64_t  forward    ; /*!< Направление движения*/
  }NuvoSpeed_t;

/*!
 *    @brief Структура хранения информации об обнаруженном объекте.
 *
 *     Структура хранения информации об обнаруженном CPRR объекте 
 *    @warning В режиме все поля данной ст руктуры имеют значение 1 - если запись ведется и 0 - если запись остановлена   
 */
typedef struct __attribute__ ((packed))
{
    int64_t   traget_id       ;  /*!< ID данной цели */
    double    range           ;  /*!< Дальность до цели в м */
    double    azimut          ;  /*!< Угол линии визирования на цель в азимутальной плоскости в град */
    int64_t   live_time       ;  /*!< Время жизни объекта в с */
    int64_t   prediction_time ;  /*!< Время, которое объект вне поля зрения в с*/
    double    rcs             ;  /*!< Мощность принятого сигнала от цели в Дб */
    double    Xcord           ;  /*!< X координата цели в декартовой системе координат в м */
    double    Xrate           ;  /*!< X компонента скорости цели в м/с */
    double    Xacceleration   ;  /*!< X компонента скорости цели в м/с */
    double    Ycord           ;  /*!< Y координата цели в декартовой системе координат в м */
    double    Yrate           ;  /*!< Y компонента скорости цели в м/с */
    double    Yacceleration   ;  /*!< Y координата цели в декартовой системе координат в м */
  }target_t;
  
/*!
 *    @brief Структура хранения исходящего для nuvo пакета.
 *
 *     Структура хранения исходящего для nuvo пакета 
 */
typedef struct __attribute__ ((packed))
{
    int64_t       grub_num    ; /*!< Порядковый номер кадра CPRR */
    int64_t       time_stamp  ; /*!< время захвата кадра CPRR в мкс, обнуляется при подачи питания на CPRR */
    target_t      target      ; /*!< Информция об обнаруженном объекте @sa  target_t*/
  }NuvoMsg_t;


typedef struct {
    // флаг готовности к работе
    int initDone; // 
    // команда выхода для потоков
    int needStop; // 
    // сокет радара
    int sock;
    // признак наличия ответов от nuvo
    int nuvoIsOnline;
    // заполненная структура адреса (для отправки команды)
    struct sockaddr_in sockaddr;
    pthread_t nuvoListener;
    int isListenerRun;
    NuvoUserEvent * callBack;
} TNuvoDriverData;


/*
// 
TNuvoDriverData * nuvoStart(char * IP, unsigned int dstPort, NuvoUserEvent * callBack);
void nuvoStop(TNuvoDriverData * pDrv);
int SendMesageToNuvo(TNuvoDriverData * pDrv, uint8_t msg, uint64_t timeStamp, uint64_t grubNum );
*/

int DAQ_DRV_CallBack(TMCExtDriver * event);

#endif // DRIVERNUVO_H