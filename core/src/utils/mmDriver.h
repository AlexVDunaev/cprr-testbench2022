
#ifndef _MMDRIVER_H_
#define _MMDRIVER_H_

#include <stdint.h>

typedef struct {
    int enabled;
    int fd;
    void * addr;
    char * fileName;
    int size;
    int writeCount;
} TMMDriverDescriptor;


TMMDriverDescriptor * mmDriverInit(char * mmFileName, int size);
int mmDriverWrite(TMMDriverDescriptor * pMMDriver, void * buffer);
void mmDriverClose(void * pDescriptor);
void mmDriverTest(void);
#endif



