/* 
 * File:   argsParam.h
 * Author: Dunaev
 *
 * Created on 11 Январь 2017 г., 10:54
 */

#ifndef ARGSPARAM_H
#define	ARGSPARAM_H

int mcGetArgsInit(int argc, char **argv);
int mcGetArgValue(char * paramName, char argFrmt, void * pValue);

#endif	/* ARGSPARAM_H */

