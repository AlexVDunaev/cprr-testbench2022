#include <string.h>
// использование библиотеки popt несет неудобства:
// при распостранении нужно иметь файл cygpopt-0.dll 
// при сборке под linux нужно скачивать и устанавливать popt
// По простому используем собственный парсинг параметров.
// Но при необходимости все же использование popt возможно.

static int mcArgc = 0;
static char **mcArgv;

int mcGetArgsInit(int argc, char **argv) {
    mcArgc = argc;
    mcArgv = argv;
}

int mcGetArgValue(char * paramName, char argFrmt, void * pValue) {
    int i;
    int result = 0;

    union {
        void * pVoid;
        int * pInt;
        float * pFloat;
        double * pDouble;
        char * pChar;
    } uniPtr;

    uniPtr.pVoid = pValue;

    switch (argFrmt) {
        case 'f': // разместить параметр во float
        case 'd': // разместить параметр в int
            break;
        default: // Если такой параметр есть в списке аргументов вернем 1
            for (i = 0; i < mcArgc; i++) {
                if (strcmp(mcArgv[i], paramName) == 0) {
                    result = 1;
                }
        }
    }
    return result;
}
