#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "mmDriver.h"

char actualFileName[255];

#define DEBUG_PRINT(...)   printf(__VA_ARGS__)
// Возврат: кол-во записанных байт
int mmDriverWrite(TMMDriverDescriptor * pMMDriver, void * buffer) {

    int result;
    
    if ((pMMDriver == NULL)||(buffer == NULL)) {
        DEBUG_PRINT("[mmDriver] write args error! : drv:%p, buff:%p\n", pMMDriver, buffer);
        return 0;
    }

#if 0    
    #warning "mmDriver -> file"
    lseek(pMMDriver->fd, 0, SEEK_SET);
    result = (int) write(pMMDriver->fd, buffer, pMMDriver->size);
#else 
    #warning "mmDriver -> memBuffer"
    // запись напрямую в память (было, что выдает исключение! Однако сейчас работает!)
    if (pMMDriver->addr != MAP_FAILED) {
        //static int i = 0;
        //*((int*)buffer) = i++;
        memcpy(pMMDriver->addr, buffer, pMMDriver->size);
        result = pMMDriver->size;
    }
#endif    
    if (result != pMMDriver->size) {
        DEBUG_PRINT("[mmDriver] write error : %d\n", result);
    } else {
        pMMDriver->writeCount++;
        // синхронизация вызывает паузу каждые ~100кадров на 1 сек.
        // хотя без fsync работает нормально.
        //fsync(pMMDriver->fd);
    }
    return result;
}

TMMDriverDescriptor * mmDriverInit(char * mmFileName, int size) {
    TMMDriverDescriptor * pMMDriver = NULL;
    int result;

    DEBUG_PRINT("[mmDriver] Init(%s, %d)\n", mmFileName, size);
    // Сохраним имя файла.
    snprintf(actualFileName, sizeof (actualFileName), "%s", mmFileName);

    // Попробуем получить права root.
    // т.к. иначе матлаб блокирует доступ к файлу.
    result = setuid(0);
    // DEBUG_PRINT("[mmDriver] setuid : %d\n", result);
    result = setgid(0);
    // DEBUG_PRINT("[mmDriver] setgid : %d\n", result);

    pMMDriver = (TMMDriverDescriptor *) malloc(sizeof (TMMDriverDescriptor));

    if (pMMDriver == NULL) {
        DEBUG_PRINT("[mmDriver] fatal error!\n");
        return NULL;
    }

    memset(pMMDriver, 0, sizeof (TMMDriverDescriptor));
    pMMDriver->size = size;

    // режим при создании файла (всем читать, всем писать)
    pMMDriver->fd = open(actualFileName, O_WRONLY | O_CREAT | O_TRUNC, 0666); //
    if (pMMDriver->fd == -1) {
        DEBUG_PRINT("[mmDriver] create file error! (%s)\n", mmFileName);

        if (pMMDriver->fd == -1) {
            //exit(1);
        } else {
        }
    } else {
        // Создам файл (если не создан) забитый нулями. Что бы если нет данных клиент видел нули.
        void * zero = malloc(size);
        memset(zero, 0, size);
        write(pMMDriver->fd, zero, size);
        close(pMMDriver->fd);
        free(zero);
        // ---------------------------------
    }
    pMMDriver->fileName = actualFileName;

    // Получим файл с которым будем работать через mmap. Права всем писать и читать
    // (Без флага O_CREAT режим задавать не нужно)
    pMMDriver->fd = open(pMMDriver->fileName, O_RDWR); //| O_TRUNC
    if (pMMDriver->fd == -1) {
        DEBUG_PRINT("[mmDriver] open file error! (%s)\n", pMMDriver->fileName);
        // exit(1);
    } else {
        // Файл открыт успешно.
        pMMDriver->addr = mmap(NULL, pMMDriver->size, PROT_WRITE | PROT_READ, MAP_SHARED, pMMDriver->fd, 0); //|MAP_POPULATE
        if (pMMDriver->addr == (void*) - 1) {
            DEBUG_PRINT("[mmDriver] mmap addr error! (-1)\n");
            // Закрываем файл
            close(pMMDriver->fd);
        } else {
            // Успешно.
            pMMDriver->enabled = 1;
            return pMMDriver;
        }
    }
    free(pMMDriver);
    return NULL;
}

void mmDriverClose(void * pDescriptor) {
    TMMDriverDescriptor * pMMDriver = (TMMDriverDescriptor *) pDescriptor;

    pMMDriver->enabled = 0;
    if (munmap(pMMDriver->addr, pMMDriver->size) == -1) {
        DEBUG_PRINT("[mmDriver] munmap error...\n");
        
    }
    close(pMMDriver->fd);
    DEBUG_PRINT("[mmDriver] close...\n");
    //Попробуем удалить файл
    if (remove(pMMDriver->fileName)) {
        DEBUG_PRINT("[mmDriver] File %s is not deleted!\n", pMMDriver->fileName);
    }
    free(pDescriptor);

    return;
}

void mmDriverTest(void) {
#define TEST_SIZE 1024
    //#define MMAP_FILE_NAME  "./../R24RZD.M/rzd_matlab_13_12_19.mmap/mmfile1.dat"
#define MMAP_FILE_NAME  "./mmfile1.dat"
    int16_t * testdata;
    TMMDriverDescriptor * mmDriver;

    testdata = (int16_t *) malloc(TEST_SIZE);
    
    if (testdata == NULL)
        return;

    for (int n = 0; n < TEST_SIZE / 2; n++) testdata[n] = n;
    
    mmDriver = mmDriverInit(MMAP_FILE_NAME, TEST_SIZE);
    for (int i = 0; i < 30; i++) {
        usleep(1000 * 500);
        printf("[TEST] mmDriverWrite : %d\n", i);
        for (int n = 0; n < TEST_SIZE / 2; n++) {
            testdata[n]++;
        }
        mmDriverWrite(mmDriver, testdata);
    }
    mmDriverClose(mmDriver);

}

