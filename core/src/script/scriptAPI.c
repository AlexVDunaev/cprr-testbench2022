#include "scriptAPI.h"



Tsub_param_rec * Signal_script_run(char * idEvent, int index, int func, Tsub_param_rec * value, char * flag);
int script_framing_callback(char * idEvent, int func, char * buffInput, int * buffInputSize, char * buffOutput, int * buffOutputSize, char * ErrorText);
int script_line_callback(char * idEvent, char * Name, int func, TMACDriverArg arg, char * ErrorText);
int script_polling_callback(char * idEvent, char * Name, int func);
int script_protocol_callback(char * idEvent, int LineIndex, int func, void * UI, char * signalName, Tsub_param_rec * value, char * pktInBuffer, int pktInSize, char * pktOutBuffer, int * pktOutSize, char * ErrorText);
int Signal_script_callback(int LuaFuncId, char typeCallBack, void * ext, int index, TMCSignalsInfo * sgInfo, unsigned short int addr, Tsub_param_rec * value, char * flag, char ExtMode, int srcInterface);


TScriptInstance Instance = {
    .Events.OnSignal = Signal_script_callback,
    .Events.OnPolling = script_polling_callback,
    .Events.OnProtocol = script_protocol_callback,
    .Events.OnFraming = script_framing_callback,
    .Events.OnLine = script_line_callback,
    .Events.OnCalc = Signal_script_run
    
};


lua_State * Script;
int flagUseSignalBufferValue = 0;
Tsub_param_rec tempSignalBufferValue;

//int coreLockCount = 0;
int linkFuncEntryCount = 0;

char script_ErrorText[255]; // строка ошибки, которая может заполняться из скрипта


int script_panic(lua_State *L) {
    Log_DBG("\n panic...");
    lua_close(L);
    return 0; /* Zero return value */
}


void scrAppendAPIFunc(lua_State * L, char * FuncName, lua_CFunction f) {

    lua_register(L, FuncName, f);
    scrAppendDebugFunc(FuncName, 0);
}

TScriptInstance * scrInit(int argc, char **argv, char * startFileName) {
    // Пока сделаю только один экземпляр, возможно нужно будет создавать несколько.

    Script = luaL_newstate();
    Instance.scrObject = (void *)Script;


    luaL_openlibs(Script);

    lua_atpanic (Script, script_panic);
    
//    Резерв:
//    scrAppendAPIFunc(Script, "set", script_set_signal);
//    scrAppendAPIFunc(Script, "cmd", script_cmd_signal);
//    scrAppendAPIFunc(Script, "req", script_req_signal);
//    scrAppendAPIFunc(Script, "get", script_get_proc);
//    scrAppendAPIFunc(Script, "link", script_link_proc);
//    scrAppendAPIFunc(Script, "mcSetParam", script_mcSetParam_proc);
//    scrAppendAPIFunc(Script, "mcSignalsSelect", script_mcSignalsSelect_proc);
//    scrAppendAPIFunc(Script, "mcConfig", script_mcConfig_proc);
//     scrAppendAPIFunc(Script, "mcArgTest", script_mcArgTest_proc);
    scrAppendAPIFunc(Script, "log", script_log);
    scrAppendAPIFunc(Script, "hexlog", script_hexlog);
    scrAppendAPIFunc(Script, "pause", script_pause);
    scrAppendAPIFunc(Script, "setLastError", script_setLastError);
    scrAppendAPIFunc(Script, "addObject", script_addObject_proc);
    scrAppendAPIFunc(Script, "mcLinkOpen", script_mcLinkOpen_proc);
    scrAppendAPIFunc(Script, "mcLinkTx", script_mcLinkTx_proc);
    scrAppendAPIFunc(Script, "mcLinkRx", script_mcLinkRx_proc);
    scrAppendAPIFunc(Script, "mcLinkCtrl", script_mcLinkCtrl_proc);
    scrAppendAPIFunc(Script, "mcFunc", script_core_proc);
    scrAppendAPIFunc(Script, "mcGetParam", script_mcGetParam_proc);
    scrAppendAPIFunc(Script, "mcCalcCRC", script_mcCalcCRC);
    // Алгоритм. Что бы упростить добавление функций, доступ к ним делаю через таблицу
    scrAppendAPIFunc(Script, "Algorithm_Init", script_alg_Init);
    
    
    //lua_register(Script, "mcExit", script_mcExit_proc);
    scrInitDebugLog(argc, argv);

    Instance.syncFunc = mcSynchronizeByName(MCORE_SYNC_INIT, "C.API", "scrInit()...");
    Instance.syncEvents = mcSynchronizeByName(MCORE_SYNC_INIT, "EVENTS", "scrInit()...");

    //script_synchronize(SCRIPT_SYNC_INIT, "INIT");
    //core_synchronize(CORE_SYNC_INIT, "INIT");


    if (luaL_dofile(Script, startFileName) == 0) {
        lua_getglobal(Script, "Init");
        if (lua_pcall(Script, 0, 0, 0) == LUA_OK) {
            
        } else {
            Log_DBG("ERR!  Error in Init() :[%s]", lua_tostring(Script, -1));
            lua_close(Script);
            Instance.scrObject = NULL;
        }
    } else {
        Log_DBG("ERR!  Error in file '%s' code:[%s]", startFileName, lua_tostring(Script, -1));
        lua_close(Script);
        Instance.scrObject = NULL;
    }
    //getchar();
    return &Instance;



}

