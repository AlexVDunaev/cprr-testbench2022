//
#include "scriptAPI.h"
#include "../utils/strutils.h"

typedef struct {
    int runCount;
} TScrDedugStat;

typedef struct {
    char funcName[40]; // Имя функции
    char isLogging;
    TScrDedugStat Stat;
} TScrFuncDedug;

#define MAX_DEBUG_FUNC  50

struct {
    int cAPIcount;
    TScrFuncDedug cAPI[MAX_DEBUG_FUNC];
} scrDedugInfo = {.cAPIcount = 0, .cAPI[0].funcName = "all"};

int scrGetStrArg(char *title, lua_State *L, int NumArg, char * buffer4Value, int bufferSize) {
    char * p;
    size_t t;
    int result = 0; // размер значения
    buffer4Value[0] = 0; // Обнулим значение в буфере
    p = (char*) lua_tolstring(L, NumArg, &t);
    if (p != NULL) {
        bufferSize--; // в буфере оставим место для размещения последнего 0 символа
        if (t <= bufferSize) {
            memcpy(buffer4Value, (char *) p, t);
            buffer4Value[t] = 0;
            result = t;
        } else {
            Log_DBG("[%s] scrGetStrArg: Argument buffer overflow. Buffer size = %d, Arg size = %d", title, bufferSize, t);
            Log_Ex("luaerr", LEVEL_ERR, (char *) "scrGetStrArg: Argument buffer overflow. Buffer size = %d, Arg size = %d", bufferSize, t);
            memcpy(buffer4Value, (char *) p, bufferSize);
            buffer4Value[bufferSize-1] = 0;
            result = bufferSize - 1;
        }
    }
    return result;
}

int scrTestString(char * buff, int size) {
    int i;
    if (size) {
        for (i = 0; i < size - 1; i++) {
            if (buff[i] == 0) return 0;
        }
        if (buff[i] == 0) return 1;
        // Если последний символ 0, (а остальные нет) то это строка
    } else {
        // Если размер буфера == 0, то будем считать что это строка.
        return 1;
    }
}


// формат кодировки аргументов как в формат строк для string.pack и string.unpack
// f: float (исконный размер) 
// d: double (исконный размер) 
// n: lua_Number 
// c: строка фиксированного размера ()
// z: завершаемая нулем строка 
// -: особый знак, означает, что параметр игнорируется.
// *: особый знак, означает, что тип параметра определяется автоматически и результат копируется в структуру TUniArgument
// Для типов z и c кроме указателей на буфер, нужно задачать его размер! Для типа c требуется указать 3-й параметр указатель на int (размер буфера)
// Вместо указателя на буфер значения может быть NULL.
// scrGetArgsFromStack(L, "Req()", -1, "nffz", &iParam, &fParam1, &fParam2, &strBuff, sizeof(strBuff)) 
// scrGetArgsFromStack(L, "Req()", 1, "nf", NULL, &fParam) 
// scrGetArgsFromStack(L, NULL, -1, "*--n", (TUniArgument *)&Param, &iParam) 
// stackBase: -1 снимать параметры с вершины стека, иначе, с указанного значения
// scrGetArgsFromStack(L, NULL, 3, "n", &iParam) // Забрать 3-й аргумент со стека
// scrGetArgsFromStack(L, "Req()", -1, "nc", &iParam, &Buff, sizeof(Buff), &outSize) 
// 

int scrGetArgsFromStack(lua_State *L, char * dbgTitle, int stackBase, const char * argsFrmt, ...) {
    // Значения возвращаемых параметров, в случае ошибки
#define DETECT_FLOAT_ERROR_VALUE      *uniPtr.pFloat = 0   
#define DETECT_DOUBLE_ERROR_VALUE     *uniPtr.pDouble = 0   
#define DETECT_INT_ERROR_VALUE       *uniPtr.pInt = 0   
#define DETECT_STR_ERROR_VALUE       uniPtr.pChar[0] = 0   

    union {
        void * pVoid;
        int * pInt;
        float * pFloat;
        double * pDouble;
        char * pChar;
        TUniArgument * pUni;
    } uniPtr;
    va_list args;
    int i;
    int luaArgIndex; // 
    char * dbgInfo = "[undef]";
    int userArgBufferSize; // 
    int * userArgOutSize; // 
    char strvalue[STR_MAX_SIZE]; // буфер для временного хранения аргумента



    if (dbgTitle != NULL) // Если dbgTitle задан то заменим им значение dbgInfo по умолчанию.
        dbgInfo = dbgTitle;

    if (stackBase != -1) {
        luaArgIndex = stackBase; // используем нумерацию аргументов, указанную явно.
    } else {
        // кол-во символов в строке формата аргументов равно кол-ву параметров снимаемых со стека Lua
        // -1 индекс - вершина стека (последний аргумент)
        // -2 индекс - второго с конца аргумента и.т.д
        luaArgIndex = -strlen(argsFrmt); // "nf" два параметра с индексами (-2, -1)
    }
    va_start(args, argsFrmt);
    for (i = 0; argsFrmt[ i ]; i++) {
        if (argsFrmt[ i ] != '-') {
            // читаем указатель на буфер, для размещения значения
            uniPtr.pVoid = va_arg(args, void *);
            if (uniPtr.pVoid != NULL) {
                // предусмотрим, что указатель может быть равен 0, тогда аргумент игнорируется
                userArgOutSize = NULL;
                switch (argsFrmt[ i ]) {
                    case 'f': // разместить параметр во float
                    case 'd': // разместить параметр в Double
                        if (lua_isnumber(L, luaArgIndex)) { // второй аргумент конвертируется в число
                            if (argsFrmt[ i ] == 'f')
                                *uniPtr.pFloat = (float) lua_tonumber(L, luaArgIndex);
                            else
                                *uniPtr.pDouble = lua_tonumber(L, luaArgIndex);
                        } else { // Ошибка. Это не число!!!
                            Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. Argument #%d, Not a number", dbgInfo, i + 1);
                            if (argsFrmt[ i ] == 'f') {
                                DETECT_FLOAT_ERROR_VALUE; // Будем возвращать значение или нет зависит от макроса...
                            } else {
                                DETECT_DOUBLE_ERROR_VALUE;
                            }
                        }
                        break;
                    case 'n':
                        if (lua_isinteger(L, luaArgIndex)) { // второй аргумент конвертируется в число
                            *uniPtr.pInt = (int) lua_tointeger(L, luaArgIndex);
                        } else { // Ошибка. Это не целое число!!!
                            Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. Argument #%d, Not a integer", dbgInfo, i + 1);
                            DETECT_INT_ERROR_VALUE;
                        }
                        break;
                    case 'c': // строки (массивы) произвольного содержания (обрабытываются одинакого) 
                        // но для массивов нужно вернуть реальный размер
                    case 'z': // строки заверщающиеся 0
                        userArgBufferSize = va_arg(args, int);
                        if (argsFrmt[ i ] == 'c')
                            userArgOutSize = va_arg(args, int *); // нужен еще аргумент в который будет размещен размер
                        if (lua_isstring(L, luaArgIndex)) { // второй аргумент конвертируется в число
                            userArgBufferSize = scrGetStrArg("uni", L, luaArgIndex, uniPtr.pChar, userArgBufferSize);
                            if (userArgOutSize != NULL)
                                *userArgOutSize = userArgBufferSize;
                        } else { // Ошибка. Это не целое число!!!
                            Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. Argument #%d, Not a string or buffer", dbgInfo, i + 1);
                            DETECT_STR_ERROR_VALUE;
                        }
                        break;
                    case '*': // Упаковка в универсальный формат TUniArgument
                        /*if (lua_isnumber(L, luaArgIndex)) { // 
                        scrGetUniArgFromStack(L, dbgInfo, luaArgIndex, uniPtr.pUni, TYPE_ID_DOUBLE);
                        } else ...
                         */
                        if (lua_isnumber(L, luaArgIndex)) { // 
                            uniPtr.pUni->typeId = 'n'; // 
                            uniPtr.pUni->valid = 1;
                            VALUE_SET_Double(&uniPtr.pUni->value, lua_tonumber(L, luaArgIndex));
                        } else
                            if (lua_isstring(L, luaArgIndex)) { // 
//                            userArgBufferSize = scrGetStrArg("uni", L, luaArgIndex, strvalue, STR_MAX_SIZE);
//                            if (scrTestString(strvalue, userArgBufferSize)) {
//                                uniPtr.pUni->typeId = 'z'; // строка
//                                uniPtr.pUni->valid = 1;
//                                VALUE_SET_String(&uniPtr.pUni->value, strvalue, userArgBufferSize);
//                            } else {
//                                uniPtr.pUni->typeId = 'c'; // массив
//                                VALUE_SET_Struct(&uniPtr.pUni->value, strvalue, userArgBufferSize);
//                            }
                        } else {
                            Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. Argument #%d, Not a string or buffer or number", dbgInfo, i + 1);
                        }
                        break;
                }
            }
        }
        luaArgIndex++;
    }
    va_end(args);
}

void script_synchronize(int lock, char * dbgText) {
    return; // Отключил синхронизацию по api-функциям. Т.к. Все CallBack-функции синхронизованы.
}

int core_synchronize_getCount(char * dbgText) {
    return mcSynchronizeGetCount();
}

void core_synchronize(int lock, char * dbgText) {
    //lock :-1 инициализация
    //lock : 1 захватить мутекс
    //lock : 0 освобoдить мутекс
    int result = 0;
    if (lock == CORE_SYNC_LOCK) {
        mcSynchronize(lock, Instance.syncEvents, dbgText);
        //Log_DBG("core_synchronize(CORE_SYNC_LOCK[%s])", dbgText);
    }
    if (lock == CORE_SYNC_UNLOCK) {
        //Log_DBG("core_synchronize(CORE_SYNC_UNLOCK[%s])", dbgText);
        mcSynchronize(lock, Instance.syncEvents, dbgText);
    }

    if (lock == CORE_SYNC_INIT) {
    }
    if (lock == CORE_SYNC_FREE) {
    }


}

int scrDebugGetFunc(char * funcKey) {
    int i;

    for (i = 0; i < scrDedugInfo.cAPIcount; i++) {
        if (strcmp(scrDedugInfo.cAPI[i].funcName, funcKey) == 0) {
            return i;
        }
    }
    return -1;

}

// Функция отладки работы C.API для Lua

int scrDebugLog(char * funcKey, const char * text, ...) {
    va_list args;
    int i;
    int isLogging = 0;

    if (SystemState.fDebugScript) { // глобальное разрешение логирования
        i = scrDebugGetFunc(funcKey);
        //Log_DBG("DBG scrDebugLog> scrDebugGetFunc() = %d", i);
        isLogging = i == -1 ? 1 : scrDedugInfo.cAPI[i].isLogging;
//        Log_DBG("DBG scrDebugLog> isLogging = %d", isLogging);
        if (isLogging) {
            va_start(args, text);
            Log_writeEx("dbg", LEVEL_DEBUG, (char *) text, args);
            va_end(args);
        }
    }

}

void scrAppendDebugFunc(char * FuncName, char defaultLogging) {
    int i;
    i = scrDedugInfo.cAPIcount;
    scrDedugInfo.cAPI[i].isLogging = defaultLogging;
    strncpy(scrDedugInfo.cAPI[i].funcName, FuncName, sizeof (scrDedugInfo.cAPI[i].funcName));
    scrDedugInfo.cAPIcount++;

}

// Инициализация структуры для вывода отладочной информации
// mCore.exe luadebug "hexlog,log,mcFunc"

void scrInitDebugLog(int argc, char **argv) {
    int i, j, n;
    char * subOpt;

    scrAppendDebugFunc("OnPolling", 0);
    scrAppendDebugFunc("OnFraming", 0);
    scrAppendDebugFunc("OnLine", 0);
    scrAppendDebugFunc("OnProtocol", 0);
    scrAppendDebugFunc("OnSignal", 0);
    scrAppendDebugFunc("OnCalc", 0);
    
    SystemState.fDebugScript = 0;
    for (n = 0; n < scrDedugInfo.cAPIcount; n++) {
        scrDedugInfo.cAPI[n].isLogging = 0;
    }
    for (i = 0; i < argc; i++) {
        //Log_DBG("DBG luadebug> debuging option %s", argv[i]);
        if (strcmp(argv[i], "luadebug") == 0) {
            Log_DBG("luadebug> debuging option : luadebug");
            if (strcmp(argv[i + 1], "all") == 0) {
                SystemState.fDebugScript = 1;
                Log_DBG("luadebug> debuging ON all functions");
                for (n = 0; n < MAX_DEBUG_FUNC; n++)
                    scrDedugInfo.cAPI[n].isLogging = 1;
                return;
            }

            for (j = 1; j < MAX_DEBUG_FUNC; j++) {
                subOpt = sutil_get_OptionPart(argv[i + 1], j, ',');
                //                Log_DBG("luadebug> subOpt  = %s", subOpt);
                if (subOpt[0] == 0) { // конец.
                    return;
                } else {
                    for (n = 0; n < scrDedugInfo.cAPIcount; n++) {
                        if (strcmp(subOpt, scrDedugInfo.cAPI[n].funcName) == 0) {
                            scrDedugInfo.cAPI[n].isLogging = 1;
                            SystemState.fDebugScript = 1;
                            Log_DBG("luadebug> function: '%s' debug ON", scrDedugInfo.cAPI[n].funcName);
                        }
                    }
                }
            }
        }
    }
}

