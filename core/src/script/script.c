#include "../main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../log/log.h"
#include "../core/commandUnit.h"
#include "../core/signals/defsignals.h"
#include "../core/other.h"
#include "../protocols/protocols.h"
#include "../core/coreUInterface.h"
#include <pthread.h>
#include <errno.h>

#define USE_START_SCRIPT    1   // объявляется в makefile. тут дублируется только для удобства редактирования

#ifdef USE_START_SCRIPT

//#define USE_DEFAULT_LUA         1   // использовать библиотечные файлы из состава Cygwin (Linux)

// нужно для корректности линковки, иначе будет "undefined reference to `_lua_ХХХ'"
#ifdef __cplusplus
extern "C" {
#endif

#ifdef USE_DEFAULT_LUA
#include <lua.h>
#include <lauxlib.h>                          
#include <lualib.h>                           
#else
    //D:\Dunaev\Source\Server.mc\Lua\lua-5.3.2\src
#include "../../Lua/lua-5.3.2/src/lua.h"
#include "../../Lua/lua-5.3.2/src/lauxlib.h"                         
#include "../../Lua/lua-5.3.2/src/lualib.h"                          
#endif

#ifdef __cplusplus
}
#endif

#endif

extern TSystemState SystemState;

#ifdef USE_START_SCRIPT

lua_State * Script;
int flagUseSignalBufferValue = 0;
Tsub_param_rec tempSignalBufferValue;

int coreLockCount = 0;
int linkFuncEntryCount = 0;

pthread_mutex_t mutex4Script; // все вызовы луа функций будем производить последовательно
pthread_mutex_t mutex4Core; // все вызовы из ядра будем производить последовательно

static int test_strget(lua_State *L) {
    lua_pushstring(L, SERVER_TITLE);
    return 1; /* One return value */
}

char script_ErrorText[255]; // строка ошибки, которая может заполняться из скрипта

static int scrGetStrArg(lua_State *L, int NumArg, char * buffer4Value, int bufferSize) {
    char * p;
    size_t t;
    int result = 0; // размер значения
    buffer4Value[0] = 0; // Обнулим значение в буфере
    p = (char*) lua_tolstring(L, NumArg, &t);
    if (p != NULL) {
        bufferSize--; // в буфере оставим место для размещения последнего 0 символа
        if (t <= bufferSize) {
            memcpy(buffer4Value, (char *) p, t);
            buffer4Value[t] = 0;
            result = t;
        } else {
            Log_Ex("luaerr", LEVEL_ERR, (char *) "scrGetStrArg: Argument buffer overflow. Buffer size = %d, Arg size = %d", bufferSize, t);
            result = 0;
        }
    }
    return result;
}

int scrTestString(char * buff, int size) {
    int i;
    if (size) {
        for (i = 0; i < size - 1; i++) {
            if (buff[i] == 0) return 0;
        }
        if (buff[i] == 0) return 1;
        // Если последний символ 0, (а остальные нет) то это строка
    } else {
        // Если размер буфера == 0, то будем считать что это строка.
        return 1;
    }
}

typedef struct {
    char typeId; // кодировка аргументов как в string.pack
    char valid; // Признак корректности данных внутри value
    Tsub_param_rec value; // 
} TUniArgument;

int scrGetUniArgFromStack(lua_State *L, char * dbgTitle, int stackBase, TUniArgument * uni, int targetType) {

    lua_Integer iValue = 0;
    lua_Number fValue = 0;
    char buffValue[STR_MAX_SIZE]; // буфер для временного хранения аргумента
    int buffSize; // 

    char * dbgInfo = "[undef]";

    if (uni != NULL) {
        if (dbgTitle != NULL) // Если dbgTitle задан то заменим им значение dbgInfo по умолчанию.
            dbgInfo = dbgTitle;

        uni->typeId = 'u'; // undef
        uni->valid = 0;
        VALUE_SET_TYPE(&uni->value, targetType);

        // получение значения со стека
        switch (targetType) {
            case TYPE_ID_BYTE:
            case TYPE_ID_WORD:
            case TYPE_ID_DWORD:
            case TYPE_ID_BYTE_SIGN:
            case TYPE_ID_WORD_SIGN:
            case TYPE_ID_DWORD_SIGN:
                if (lua_isinteger(L, stackBase)) {
                    iValue = lua_tointeger(L, stackBase);
                    uni->typeId = 'n'; // целое число
                    uni->valid = 1;
                } else
                    Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. UniArgument Not a integer", dbgInfo);
                break;
            case TYPE_ID_FLOAT:
            case TYPE_ID_DOUBLE:
                if (lua_isnumber(L, stackBase)) {
                    fValue = lua_tonumber(L, stackBase);
                    uni->typeId = TYPE_ID_FLOAT == targetType ? 'f' : 'd'; // целое число
                    uni->valid = 1;
                } else
                    Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. UniArgument Not a number", dbgInfo);
                break;

            default:;
        }

        //преобразование значения в требуемый формат
        switch (targetType) {
            case TYPE_ID_BYTE:
                uni->value.bvalue = iValue;
                break;
            case TYPE_ID_WORD:
                uni->value.wvalue = iValue;
                break;
            case TYPE_ID_DWORD:
                uni->value.dwvalue = iValue;
                break;
            case TYPE_ID_BYTE_SIGN:
                uni->value.sbvalue = iValue;
                break;
            case TYPE_ID_WORD_SIGN:
                uni->value.swvalue = iValue;
                break;
            case TYPE_ID_DWORD_SIGN:
                uni->value.ivalue = iValue;
                break;
            case TYPE_ID_FLOAT:
                uni->value.fvalue = fValue;
                break;
            case TYPE_ID_DOUBLE:
                uni->value.tvalue = fValue;
                break;
            case TYPE_ID_STRING0:
                if (lua_isstring(L, stackBase)) {
                    buffSize = scrGetStrArg(L, stackBase, buffValue, STR_MAX_SIZE);
                    uni->typeId = 'z'; // строка
                    uni->valid = 1;
                    VALUE_SET_String(&uni->value, buffValue, buffSize);
                } else
                    Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. UniArgument, Not a string", dbgInfo);
                break;
            case TYPE_ID_STRUCT:
                if (lua_isstring(L, stackBase)) {
                    buffSize = scrGetStrArg(L, stackBase, buffValue, STR_MAX_SIZE);
                    uni->typeId = 'c'; // строка
                    uni->valid = 1;
                    VALUE_SET_Struct(&uni->value, buffValue, buffSize);
                } else
                    Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. UniArgument, Not a buffer", dbgInfo);
                break;
            default:
                Log_Ex("luaerr", LEVEL_ERR, (char *) "Function 'scrGetUniArgFromStack'. Error targetType value (%d)", targetType);
        }

        return uni->valid;
    } else
        return 0;


}

// формат кодировки аргументов как в формат строк для string.pack и string.unpack
// f: float (исконный размер) 
// d: double (исконный размер) 
// n: lua_Number 
// c: строка фиксированного размера ()
// z: завершаемая нулем строка 
// -: особый знак, означает, что параметр игнорируется.
// *: особый знак, означает, что тип параметра определяется автоматически и результат копируется в структуру TUniArgument
// Для типов z и c кроме указателей на буфер, нужно задачать его размер! Для типа c требуется указать 3-й параметр указатель на int (размер буфера)
// Вместо указателя на буфер значения может быть NULL.
// scrGetArgsFromStack(L, "Req()", -1, "nffz", &iParam, &fParam1, &fParam2, &strBuff, sizeof(strBuff)) 
// scrGetArgsFromStack(L, "Req()", 1, "nf", NULL, &fParam) 
// scrGetArgsFromStack(L, NULL, -1, "*--n", (TUniArgument *)&Param, &iParam) 
// stackBase: -1 снимать параметры с вершины стека, иначе, с указанного значения
// scrGetArgsFromStack(L, NULL, 3, "n", &iParam) // Забрать 3-й аргумент со стека
// scrGetArgsFromStack(L, "Req()", -1, "nc", &iParam, &Buff, sizeof(Buff), &outSize) 
// 

int scrGetArgsFromStack(lua_State *L, char * dbgTitle, int stackBase, const char * argsFrmt, ...) {
    // Значения возвращаемых параметров, в случае ошибки
#define DETECT_FLOAT_ERROR_VALUE      *uniPtr.pFloat = 0   
#define DETECT_DOUBLE_ERROR_VALUE     *uniPtr.pDouble = 0   
#define DETECT_INT_ERROR_VALUE       *uniPtr.pInt = 0   
#define DETECT_STR_ERROR_VALUE       uniPtr.pChar[0] = 0   

    union {
        void * pVoid;
        int * pInt;
        float * pFloat;
        double * pDouble;
        char * pChar;
        TUniArgument * pUni;
    } uniPtr;
    va_list args;
    int i;
    int luaArgIndex; // 
    char * dbgInfo = "[undef]";
    int userArgBufferSize; // 
    int * userArgOutSize; // 
    char strvalue[STR_MAX_SIZE]; // буфер для временного хранения аргумента



    if (dbgTitle != NULL) // Если dbgTitle задан то заменим им значение dbgInfo по умолчанию.
        dbgInfo = dbgTitle;

    if (stackBase != -1) {
        luaArgIndex = stackBase; // используем нумерацию аргументов, указанную явно.
    } else {
        // кол-во символов в строке формата аргументов равно кол-ву параметров снимаемых со стека Lua
        // -1 индекс - вершина стека (последний аргумент)
        // -2 индекс - второго с конца аргумента и.т.д
        luaArgIndex = -strlen(argsFrmt); // "nf" два параметра с индексами (-2, -1)
    }
    va_start(args, argsFrmt);
    for (i = 0; argsFrmt[ i ]; i++) {
        if (argsFrmt[ i ] != '-') {
            // читаем указатель на буфер, для размещения значения
            uniPtr.pVoid = va_arg(args, void *);
            if (uniPtr.pVoid != NULL) {
                // предусмотрим, что указатель может быть равен 0, тогда аргумент игнорируется
                userArgOutSize = NULL;
                switch (argsFrmt[ i ]) {
                    case 'f': // разместить параметр во float
                    case 'd': // разместить параметр в Double
                        if (lua_isnumber(L, luaArgIndex)) { // второй аргумент конвертируется в число
                            if (argsFrmt[ i ] == 'f')
                                *uniPtr.pFloat = (float) lua_tonumber(L, luaArgIndex);
                            else
                                *uniPtr.pDouble = lua_tonumber(L, luaArgIndex);
                        } else { // Ошибка. Это не число!!!
                            Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. Argument #%d, Not a number", dbgInfo, i + 1);
                            if (argsFrmt[ i ] == 'f') {
                                DETECT_FLOAT_ERROR_VALUE; // Будем возвращать значение или нет зависит от макроса...
                            } else {
                                DETECT_DOUBLE_ERROR_VALUE;
                            }
                        }
                        break;
                    case 'n':
                        if (lua_isinteger(L, luaArgIndex)) { // второй аргумент конвертируется в число
                            *uniPtr.pInt = (int) lua_tointeger(L, luaArgIndex);
                        } else { // Ошибка. Это не целое число!!!
                            Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. Argument #%d, Not a integer", dbgInfo, i + 1);
                            DETECT_INT_ERROR_VALUE;
                        }
                        break;
                    case 'c': // строки (массивы) произвольного содержания (обрабытываются одинакого) 
                        // но для массивов нужно вернуть реальный размер
                    case 'z': // строки заверщающиеся 0
                        userArgBufferSize = va_arg(args, int);
                        if (argsFrmt[ i ] == 'c')
                            userArgOutSize = va_arg(args, int *); // нужен еще аргумент в который будет размещен размер
                        if (lua_isstring(L, luaArgIndex)) { // второй аргумент конвертируется в число
                            userArgBufferSize = scrGetStrArg(L, luaArgIndex, uniPtr.pChar, userArgBufferSize);
                            if (userArgOutSize != NULL)
                                *userArgOutSize = userArgBufferSize;
                        } else { // Ошибка. Это не целое число!!!
                            Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. Argument #%d, Not a string or buffer", dbgInfo, i + 1);
                            DETECT_STR_ERROR_VALUE;
                        }
                        break;
                    case '*': // Упаковка в универсальный формат TUniArgument
                        /*if (lua_isnumber(L, luaArgIndex)) { // 
                        scrGetUniArgFromStack(L, dbgInfo, luaArgIndex, uniPtr.pUni, TYPE_ID_DOUBLE);
                        } else ...
                            */
                        if (lua_isnumber(L, luaArgIndex)) { // 
                            uniPtr.pUni->typeId = 'n'; // 
                            uniPtr.pUni->valid = 1;
                            VALUE_SET_Double(&uniPtr.pUni->value, lua_tonumber(L, luaArgIndex));
                        } else
                            if (lua_isstring(L, luaArgIndex)) { // 
                            userArgBufferSize = scrGetStrArg(L, luaArgIndex, strvalue, STR_MAX_SIZE);
                            if (scrTestString(strvalue, userArgBufferSize)) {
                                uniPtr.pUni->typeId = 'z'; // строка
                                uniPtr.pUni->valid = 1;
                                VALUE_SET_String(&uniPtr.pUni->value, strvalue, userArgBufferSize);
                            } else {
                                uniPtr.pUni->typeId = 'c'; // массив
                                VALUE_SET_Struct(&uniPtr.pUni->value, strvalue, userArgBufferSize);
                            }
                        } else {
                            Log_Ex("luaerr", LEVEL_ERR, (char *) "Function '%s'. Argument #%d, Not a string or buffer or number", dbgInfo, i + 1);
                        }
                        break;
                }
            }
        }
        luaArgIndex++;
    }
    va_end(args);
}


/* Синхронизация...
 * Без синхронизации, сервер, в многопоточном режиме, вызывал бы функции обратного вызова
 * (такие как PollCallBack, ProtocolCallBack, FrameCallBack, LineCallBack, SignalCallBack)
 * асинхронно с работой скрипта. Т.о. при работе с lua-стеком произошел бы сбой.
 * Что бы этого избежать, вводим условие, что функции обратного вызова lua вызываются
 * только после завершения работы других функций обрабного вызова и после завершения
 * интерфейсных функций (set(), get(), req() и т.п.)
 * 
 
 
 
 
 */



#define SCRIPT_SYNC_LOCK        1
#define SCRIPT_SYNC_UNLOCK      0
#define SCRIPT_SYNC_INIT        -1
#define SCRIPT_SYNC_FREE        -2

static void script_synchronize(int lock, char * dbgText) {
    //lock :-1 инициализация
    //lock : 1 захватить мутекс
    //lock : 0 освобудить мутекс

    return; // Отключил синхронизацию по api-функциям. Т.к. Все CallBack-функции синхронизованы.

    int result = 0;

    if (lock == SCRIPT_SYNC_LOCK) {
        result = pthread_mutex_lock(&mutex4Script);
        if (SystemState.fDebugCore)
            Log_DBG("+++ SCRIPT_SYNC_LOCK (%s)\n", dbgText);
        if (result != 0) {
            if (result = EBUSY) {
                Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG script_synchronize> LOCK ERROR! Deadlock condition ");
                Log_DBG("ERR! script_synchronize[%s]> LOCK ERROR! Deadlock condition ", dbgText);
            } else
                Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG script_synchronize> LOCK ERROR! = %d\n ", result);
        }
    }
    if (lock == SCRIPT_SYNC_UNLOCK) {
        if (SystemState.fDebugCore)
            Log_DBG("--- SCRIPT_SYNC_LOCK (%s)\n", dbgText);
        pthread_mutex_unlock(&mutex4Script);
    }

    if (lock == SCRIPT_SYNC_INIT) {
        pthread_mutex_init(&mutex4Script, NULL);
    }
    if (lock == SCRIPT_SYNC_FREE) {
        pthread_mutex_destroy(&mutex4Script);
    }


}

#define CORE_SYNC_LOCK        1
#define CORE_SYNC_UNLOCK      0
#define CORE_SYNC_INIT        -1
#define CORE_SYNC_FREE        -2

static void core_synchronize(int lock, char * dbgText) {
    //lock :-1 инициализация
    //lock : 1 захватить мутекс
    //lock : 0 освобoдить мутекс
    int result = 0;
    if (lock == CORE_SYNC_LOCK) {

        coreLockCount++;
        if (coreLockCount > 1) {
            if (SystemState.fDebugCore)
                Log_DBG("DBG core_synchronize> coreLockCount = %d\n ", coreLockCount);
            //Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG core_synchronize> coreLockCount = %d\n ", coreLockCount);
        }
        result = pthread_mutex_lock(&mutex4Core);
        if (SystemState.fDebugCore)
            Log_DBG("+++ CORE_SYNC_LOCK(%s)\n", dbgText);
        if (result != 0) {
            if (result = EBUSY) {
                Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG core_synchronize> LOCK ERROR! Deadlock condition ");
                Log_DBG("ERR! core_synchronize[%s]> LOCK ERROR! Deadlock condition ", dbgText);
            } else
                Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG core_synchronize> LOCK ERROR! = %d\n ", result);
        } else { // вошли с секцию CORE

        }
        // для корректности работы со стеком LUA, при вызове Lua-обработчиков,
        // нужно гарантировать что все интерфейсные функции (mcX) завершили работу
        //pthread_mutex_trylock(&mutex4Script);

    }
    if (lock == CORE_SYNC_UNLOCK) {
        if (SystemState.fDebugCore)
            Log_DBG("--- CORE_SYNC_UNLOCK(%s)\n", dbgText);

        coreLockCount--;
        pthread_mutex_unlock(&mutex4Core);

    }

    if (lock == CORE_SYNC_INIT) {
        pthread_mutex_init(&mutex4Core, NULL);
    }
    if (lock == CORE_SYNC_FREE) {
        pthread_mutex_destroy(&mutex4Core);
    }


}

static int script_mcArgTest_proc(lua_State *L) {
    char TextArg1[255];
    char TextArg2[255];
    int i;
    int i2;
    int buffSize;
    float f;
    float f2;
    double d;
    double d2;
    TUniArgument uni;


    scrGetArgsFromStack(L, "mcArgTest()", 1, "n", &i);
    if (i == 1) {
        scrGetArgsFromStack(L, "mcArgTest()", 1, "-fd", &f, &d);
        Log_DBG("DBG mcArgTest(%d, %f, %f) ", i, f, d);
    }
    if (i == 2) {
        scrGetArgsFromStack(L, "mcArgTest()", -1, "nzn", NULL, TextArg1, sizeof (TextArg1), &i2);
        Log_DBG("DBG mcArgTest(%d, '%s', [%d]) ", i, TextArg1, i2);
    }
    if (i == 3) {
        scrGetArgsFromStack(L, "mcArgTest()", -1, "n*", NULL, &uni);
        if (uni.value.tp == TYPE_ID_DOUBLE)
            Log_DBG("DBG mcArgTest(%d, [%d, %d, %f]) ", i, uni.valid, uni.value.tp, uni.value.dvalue);
        else
            if (uni.value.tp == TYPE_ID_STRING0)
            Log_DBG("DBG mcArgTest(%d, [%d, %d, '%s']) ", i, uni.valid, uni.value.tp, uni.value.strvalue);
        else
            Log_DBG("DBG mcArgTest(%d, [%d, %d, ERR]) ", i, uni.valid, uni.value.tp);

    }

    if (i == 4) {
        scrGetArgsFromStack(L, "mcArgTest()", -1, "-fn*f", NULL, NULL, &uni, &f);
        Log_DBG("DBG mcArgTest(%d, f = %f) ", i, f);
        if (uni.value.tp == TYPE_ID_DOUBLE)
            Log_DBG("DBG mcArgTest(%d, [%d, %d, %f]) ", i, uni.valid, uni.value.tp, uni.value.dvalue);
        else
            if (uni.value.tp == TYPE_ID_STRING0)
            Log_DBG("DBG mcArgTest(%d, [%d, %d, '%s']) ", i, uni.valid, uni.value.tp, uni.value.strvalue);
        else
            Log_DBG("DBG mcArgTest(%d, [%d, %d, ERR]) ", i, uni.valid, uni.value.tp);
    }

    if (i == 5) {
        scrGetArgsFromStack(L, "mcArgTest()", -1, "-c", TextArg1, sizeof TextArg1, &buffSize);
        Log_DBG("DBG mcArgTest(%d, '%s', buffSize = %d) ", i, TextArg1, buffSize);
    }

    if (i == 6) {
        scrGetUniArgFromStack(L, "scrGetUniArgFromStack-Test", 2, &uni, TYPE_ID_DOUBLE);
        Log_DBG("DBG mcArgTest: UniArg(%d, [%f]) ", i, uni.value.tvalue);
    }
    if (i == 7) {
        scrGetUniArgFromStack(L, "scrGetUniArgFromStack-Test", 2, &uni, TYPE_ID_STRING0);
        Log_DBG("DBG mcArgTest: UniArg(%d, '%s', buffSize = %d) ", i, uni.value.strvalue, uni.value.size);
    }
    if (i == 8) {
        scrGetUniArgFromStack(L, "scrGetUniArgFromStack-Test", 2, &uni, TYPE_ID_BYTE);
        Log_DBG("DBG mcArgTest: UniArg(%d, %d) ", i, uni.value.bvalue);
    }


    return 0; /* Zero return value */

}
// Запись значения сигнала
// Пример вызова из Lua:   set("PUA[2].ENABLED", 0)

static int script_set_signal(lua_State *L) {
    int i;
    char sName[255] = "";
    char * p;
    int index; // индекс сигнала в таблице сигналов.
    int result;
    size_t t;
    char flag = 1;
    Tsub_param_rec rec;
    TValue * d;
    TSignalsRec * a;
    //    lua_Number needFlag = -1;

    double value;


    script_synchronize(SCRIPT_SYNC_LOCK, "set()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>set(...)\n ");

    i = lua_gettop(L);


    if ((i == 2) || (i == 3)) { // два аргумента
        scrGetStrArg(L, 1, sName, sizeof (sName));
        //        Log_DBG("DBG Script>set(%s)", sName);
        // Значение сигнала для сигналов типа структура и строка должны быть в соответствующем формате
        index = GetSignalByName(sName);
        if (index != -1) { // сигнал найден
            a = (TSignalsRec *) SystemState.Data.Values.values->i[index]->pSignalRec;
            if (a->TYPE == TYPE_ID_STRUCT) { // читаем второй аргумент как буфер
                p = (char*) lua_tolstring(L, 2, &t);
                VALUE_SET_Struct(&rec, p, t);
            } else
                if (a->TYPE == TYPE_ID_STRING0) { // читаем второй аргумент как строка
                p = (char*) lua_tolstring(L, 2, &t);
                VALUE_SET_String0(&rec, p);
            } else { // читаем второй аргумент как число
                value = lua_tonumber(L, 2);
                d = (TValue *) a->data;
                VALUE_COPY(&rec, &d->v);
                VALUE_CONVERT_from_Double(&rec, value);
            }
            //
            if (i == 3)
                flag = (lua_tonumber(L, 3) > 0);

            //--------------------------------------------------------------------
            lua_settop(L, 0); // вернем вершину стека на место, т.к. может вызваться lua-linkProc
            //Log_DBG("\n DBG... Script: Run SetSignalValueEx(%s)...", sName);
            // при вызове функции set( будем просто присваивать значение сигналу)
            SetSignalValueEx(&SystemState, index, &rec, &flag);
            //result = RunUserCallBack(CALLBACK_TYPE_SET_SIGNAL, &SystemState, index, sName, a->addr, &rec, &flag, LOTOS_BUFFERED_MODE_ON, -1);

        } else { // сигнал не найден
            Log_DBG("\n DBG... Script: SIGNAL MISMATCH!");
            Log_Ex("luaerr", LEVEL_ERR, (char *) "SIGNAL MISMATCH! method: set('%s')", sName);
            lua_settop(L, 0); // вернем вершину стека на место, т.к. может вызваться lua-linkProc
        }

    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'set') : d%", i);
    }
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>set(%s...)...end\n ", sName);
    script_synchronize(SCRIPT_SYNC_UNLOCK, "set()");

    return 0; /* Zero return value */
}


// Команда. req(PUA[2].cmd, 1)
// Пример вызова из Lua:   req("PUA[2].ENABLED", 1)
// Размещение в очередь события REQ для сигнала. 

static int script_req_signal(lua_State *L) {
    int i;
    char sName[255];
    char hexStr[25500];
    char typeId[255] = "";
    char * p;
    int index; // индекс сигнала в таблице сигналов.
    int needTypeId;
    int result;
    size_t t;
    char flag = 1;
    Tsub_param_rec rec;
    TValue * d;
    TSignalsRec * a;
    Tsub_param_rec * ReqRec;

    double value;

    script_synchronize(SCRIPT_SYNC_LOCK, "req()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>req(...)\n ");
    i = lua_gettop(L);
    if ((i == 2) || (i == 3)) { // два аргумента (или 3 аргумента)
        // req("sig", 65) -> f64 = 65.0
        // req("sig", 65, "i8") -> i8 = 65
        scrGetStrArg(L, 1, sName, sizeof (sName));
        if (lua_isnumber(L, 2)) { // второй аргумент конвертируется в число
            value = lua_tonumber(L, 2);
            //Log_DBG("DBG.  Script: req(%s, %f)", sName, value);
            VALUE_SET_Double(&rec, value);
        } else { // второй аргумент структура или строка, которую нельзя перевести в число
            p = (char*) lua_tolstring(L, 2, &t);
            memset(&rec, 0, sizeof (rec));
            VALUE_SET_Struct(&rec, p, t);
            //bufToHexStr(p, t, hexStr);
        }
        if (i == 3) {
            p = (char*) lua_tolstring(L, 3, &t);
            // нужно сконвертировать в указанный тип.
            needTypeId = SL_TypeStr2TypeID(p);
            if (needTypeId != TYPE_ID_UNDEF) {
                if (needTypeId == TYPE_ID_STRUCT) {
                    i = atoi(&p[1]); // c33 -> 33
                    VALUE_SET_Struct(&rec, &rec.scvalue[0], i);
                } else
                    if (needTypeId == TYPE_ID_STRING0) {
                    VALUE_SET_String0(&rec, &rec.scvalue[0]);
                } else { // числовые типы...
                    rec.tp = needTypeId;
                    rec.size = SL_GET_Size(needTypeId);
                    VALUE_CONVERT_from_Double(&rec, rec.tvalue);
                }
            } else
                Log_Ex("luaerr", LEVEL_ERR, (char *) "TYPE ILLEGAL! %s method: req()", p);


        }
        index = GetSignalByName(sName);
        if (index == -1) { // сигнал не найден
            Log_DBG("\n DBG... Script: SIGNAL MISMATCH!");
            Log_Ex("luaerr", LEVEL_ERR, (char *) "SIGNAL MISMATCH! %s method: req()", sName);
        } else {
            ReqRec = &SystemState.Data.Values.values->i[index]->reqValue;

            //Log_DBG("\n DBG... Script: SIGNAL FOUND");
            VALUE_COPY(ReqRec, &SystemState.Data.Values.values->i[index]->v);
            VALUE_COPY(ReqRec, &rec);
            // нужно разместить сигнал в очередь...
            CMD_ADD_Request(&SystemState, mcGetSignalRec(index)->interface, 5000, index, REQ_TYPE_INT, REQUEST_DIR_SET);
            CMD_PRINT_Request(&SystemState);
            //--------------------------------------------------------------------
            //result = RunUserCallBack(CALLBACK_TYPE_SET_SIGNAL, &SystemState, index, sName, a->addr, &rec, &flag, LOTOS_BUFFERED_MODE_ON, -1);
        }
    }
    lua_settop(L, 0);
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>req(...)...end\n ");
    script_synchronize(SCRIPT_SYNC_UNLOCK, "req()");


    return 0; /* Zero return value */
}




// Команда. PUA[2].ENABLED = 0
// Пример вызова из Lua:   cmd("PUA[2].ENABLED", 0)

static int script_cmd_signal(lua_State *L) {
    int i;
    char sName[255];
    char * p;
    int index; // индекс сигнала в таблице сигналов.
    int result;
    size_t t;
    char flag = 1;
    Tsub_param_rec rec;
    TValue * d;
    TSignalsRec * a;

    double value;
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>cmd(...)\n ");

    script_synchronize(SCRIPT_SYNC_LOCK, "cmd()");

    i = lua_gettop(L);
    if (i == 2) { // два аргумента
        scrGetStrArg(L, 1, sName, sizeof (sName));
        value = lua_tonumber(L, 2);

        Log_DBG("DBG.  Script: cmd(%s, %f)", sName, value);
        index = GetSignalByName(sName);
        if (index == -1) { // сигнал не найден
            Log_DBG("\n DBG... Script: SIGNAL MISMATCH!");
            Log_Ex("luaerr", LEVEL_ERR, (char *) "SIGNAL MISMATCH! %s method: set()", sName);
        } else {
            a = (TSignalsRec *) SystemState.Data.Values.values->i[index]->pSignalRec;
            //Log_DBG("\n DBG... Script: SIGNAL FOUND");
            d = (TValue *) a->data;
            VALUE_COPY(&rec, &d->v);
            VALUE_CONVERT_from_Double(&rec, value);
            //--------------------------------------------------------------------
            result = RunUserCallBack(CALLBACK_TYPE_SET_SIGNAL, &SystemState, index, sName, a->addr, &rec, &flag, LOTOS_BUFFERED_MODE_ON, -1);
        }
    }
    lua_settop(L, 0);
    script_synchronize(SCRIPT_SYNC_UNLOCK, "cmd()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>cmd(...)...end\n ");

    return 0; /* Zero return value */
}


// функция для связывания сигнала и функции скрипта
// Пример вызова из Lua: link("MKO_UK[1].MKVI.VREMY", callBack), где
// function callBack(value, flag) -- вызывается в момент обновления связанного сигнала
// ...
// return value, flag
// end

static int script_link_proc(lua_State *L) {
    int i;
    char sName[255];
    int value;
    char * p;
    int index; // индекс сигнала в таблице сигналов.
    size_t t;
    TValue * d;


    script_synchronize(SCRIPT_SYNC_LOCK, "link()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>link(...)\n ");

    i = lua_gettop(L);
    if (i == 2) { // два аргумента
        scrGetStrArg(L, 1, sName, sizeof (sName));
        // размещение последнего аргумента (функции) в реестре lua
        value = luaL_ref(L, LUA_REGISTRYINDEX);
        Log_DBG("DBG.  Script: link(%s, %d)", sName, value);
        index = GetSignalByName(sName);
        if (index == -1) { // сигнал не найден
            Log_Ex("luaerr", LEVEL_ERR, (char *) "SIGNAL MISMATCH! %s method: link()", sName);
        } else {
            d = (TValue *) mcGetSignalRec(index)->data;
            d->script_func = value;
        }
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'link')");

    }
    lua_settop(L, 0);
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>link(...)...end\n ");
    script_synchronize(SCRIPT_SYNC_UNLOCK, "link()");

    return 0;
}

// функция добавления объекта
// Пример вызова из Lua: addObject(OBJ_SIGNAL, "MySignalInt", "[LINE=1; ADDR=0x1000; TYPE=i32; HW=0x0001; VALUE={0}]", MySGProc)

static int script_addObject_proc(lua_State *L) {
    int i;
    char sName[255];
    char sParam[255];
    int IntParam;
    int value;
    char objType[50] = "";
    char * p;
    size_t t;
    TSignalsRec * a;
    TLineRec * line;
    TPollingRec* pollDrv;
    TProtFramingRec * framing;
    TProtocolRec * protocol;
    script_synchronize(SCRIPT_SYNC_LOCK, "addObject()");
    i = lua_gettop(L);
    if (i > 2) { // > 2 аргументов
        scrGetArgsFromStack(L, "addObject()", 1, "zz",
                &objType, sizeof (objType),
                &sName, sizeof (sName));
        //scrGetStrArg(L, 1, objType, sizeof(objType));
        //scrGetStrArg(L, 2, sName, sizeof(sName));

        if (strcmp(objType, "SIGNAL") == 0) { // добавление сигнала
            scrGetStrArg(L, 3, sParam, sizeof (sParam));
            // размещение последнего аргумента (функции) в реестре lua
            value = luaL_ref(L, LUA_REGISTRYINDEX);
            Log_DBG("DBG. Script: addObject(id = %s, %s, %s)", objType, sName, sParam);
            a = BuildSignalRec(&SystemState, sName, sParam, value);
            // Заполним поля описания сигнала
            //a->callBackAddr = value;
            AppendSignal(&SystemState, a, OBJ_SOURCE_SCRIPT, 0);
        } else
            if (strcmp(objType, "PROTOCOL") == 0) { // добавление протокола ("PROTOCOL.FRAMING" + "PROTOCOL.MANAGER"))
            Log_DBG("DBG. Script: addObject(PROTOCOL): ERROR! ");
        } else
            if (strcmp(objType, "PROTOCOL.FRAMING") == 0) { // добавление драйвера обертки
            scrGetStrArg(L, 3, sParam, sizeof (sParam));
            value = luaL_ref(L, LUA_REGISTRYINDEX);

            Log_DBG("DBG. Script: addObject(id = %s, %s, %s)", objType, sName, sParam);
            framing = BuildFramingRec(&SystemState, sName, sParam, value);
            AppendFramingUnit(&SystemState, framing, OBJ_SOURCE_SCRIPT);
        } else
            if (strcmp(objType, "PROTOCOL.MANAGER") == 0) { // добавление драйвера обработки
            scrGetStrArg(L, 3, sParam, sizeof (sParam));
            value = luaL_ref(L, LUA_REGISTRYINDEX);

            Log_DBG("DBG. Script: addObject(id = %s, %s, %s)", objType, sName, sParam);
            protocol = BuildProtocolRec(&SystemState, sName, sParam, value);
            AppendProtocolUnit(&SystemState, protocol, OBJ_SOURCE_SCRIPT);
        } else
            if (strcmp(objType, "POLLING") == 0) { // добавление драйвера опроса
            scrGetStrArg(L, 3, sParam, sizeof (sParam));
            //!!!! Внимание! тест!
            // было!  
            value = luaL_ref(L, LUA_REGISTRYINDEX);
            //value = (int) lua_tocfunction(L, 4);
            Log_DBG("DBG. Script: addObject(id = %s, %s, %s)", objType, sName, sParam);
            pollDrv = BuildPollingRec(&SystemState, sName, sParam, value);
            AppendPollingUnit(&SystemState, pollDrv, OBJ_SOURCE_SCRIPT);
        } else
            if (strcmp(objType, "LINE") == 0) { // добавление линии связи (открытие порта связи)
            IntParam = lua_tointeger(L, 3);
            scrGetStrArg(L, 4, sParam, sizeof (sParam));
            value = luaL_ref(L, LUA_REGISTRYINDEX);
            Log_DBG("DBG. Script: addObject(LINE, Name=%s, id=%d, Param=%s)", sName, IntParam, sParam);
            line = BuildLineRec(&SystemState, sName, IntParam, sParam, value);
            AppendLine(&SystemState, line, OBJ_SOURCE_SCRIPT);
        }
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'addObject')");

    }
    lua_settop(L, 0);
    script_synchronize(SCRIPT_SYNC_UNLOCK, "addObject()");

    return 0;
}

// передать сообщение об ошибке во время выполнения функции

static int script_setLastError(lua_State *L) {
    int i;
    char * p;
    size_t t;
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>LastError(...)\n ");

    script_synchronize(SCRIPT_SYNC_LOCK, "LastError()");

    i = lua_gettop(L);
    if (i == 1) { // 1 аргументов
        scrGetStrArg(L, 1, script_ErrorText, sizeof (script_ErrorText));
        Log_DBG("setLastError(%s)", script_ErrorText);
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'script_setLastError')");
    }
    lua_settop(L, 0);
    script_synchronize(SCRIPT_SYNC_UNLOCK, "LastError()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>LastError(...)...end\n ");

    return 0;
}

// Вывод в лог
// Пример вызова из Lua: log("текст")
// [Пример вызова из Lua: log("текст", "LogFileName")]

static int script_log(lua_State *L) {
    int i;
    char text[255];
    char logfile[20] = "dbg"; // ограничение по длине в модуле log
    char * p;
    size_t t;


    script_synchronize(SCRIPT_SYNC_LOCK, "log()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>log(...)\n ");

    i = lua_gettop(L);

    if ((i == 1) || (i == 2)) { // > 2 аргументов
        scrGetStrArg(L, 1, text, sizeof (text));
        if (i == 2) { // Если задано имя файла, пишем в него
            scrGetStrArg(L, 2, logfile, sizeof (logfile));
        }
        Log_Ex(logfile, LEVEL_ERR, "%s", text);
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'log')");
    }
    lua_settop(L, 0);
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>log(...)...end\n ");
    script_synchronize(SCRIPT_SYNC_UNLOCK, "log()");

    return 0;
}



// Вывод в лог
// Пример вызова из Lua: hexlog("текст>", buffer)

static int script_hexlog(lua_State *L) {
    int i;
    char text[255];
    char logfile[20] = "dbg"; // ограничение по длине в модуле log
    char * p;
    char * pLogfile;
    size_t t;
    char buffer[255];
    char hexStr[255 * 3];
    char ResultStr[255 * 4];
    int bufferSize;
    char result = 0;


    script_synchronize(SCRIPT_SYNC_LOCK, "hexlog()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> hexlog(...)\n ");

    i = lua_gettop(L);

    if ((i == 2) || (i == 3)) { // == 2 аргументов
        scrGetStrArg(L, 1, text, sizeof (text));
        p = (char*) lua_tolstring(L, 2, &t);
        bufferSize = t;
        if (i == 3) { // Если задано имя файла, пишем в него
            scrGetStrArg(L, 3, logfile, sizeof (logfile));
        }

        lua_settop(L, 0);
        //Log_DBG("script_hexlog(%s, [%d])",text, bufferSize);

        if (bufferSize >= sizeof (buffer)) {
            Log_Ex("luaerr", LEVEL_ERR, (char *) "hexlog: buffer size too much (%d)", bufferSize);
            bufferSize = sizeof (buffer) - 1;
        }
        memcpy(buffer, (char *) p, bufferSize);
        bufToHexStr(buffer, bufferSize, hexStr);
        snprintf(ResultStr, sizeof (ResultStr), "%s %s", text, hexStr);
        lua_pushstring(L, ResultStr); //
        result = 1;
        // Log_DBG("%s", ResultStr);
        Log_Ex(logfile, LEVEL_ERR, "%s", ResultStr);
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'log')");
        lua_settop(L, 0);
    }
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> hexlog(...)...end\n ");
    script_synchronize(SCRIPT_SYNC_UNLOCK, "hexlog()");
    return result;
}

static int script_pause(lua_State *L) {
    int i;
    char result = 0;
    int pause;


    script_synchronize(SCRIPT_SYNC_LOCK, "pause()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> pause(...)\n ");

    i = lua_gettop(L);
    if (i == 1) { // == 1 аргументов
        pause = lua_tonumber(L, 1);
        lua_settop(L, 0);
        usleep(pause);
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'pause')");
        lua_settop(L, 0);
    }
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> pause(...)...end\n ");
    script_synchronize(SCRIPT_SYNC_UNLOCK, "pause()");
    return result;
}

// доступ к параметрам сигнала

static int script_mcGetParam_proc(lua_State *L) {
    int i;
    char signalName[255];
    char paramName[255];
    char paramValue[255] = "";
    char * param = NULL;
    char * p;
    size_t t;
    char result = 0;


    script_synchronize(SCRIPT_SYNC_LOCK, "mcGetParam()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcGetParam(...)\n ");
    i = lua_gettop(L);

    if (i == 2) {
        scrGetStrArg(L, 1, signalName, sizeof (signalName));
        scrGetStrArg(L, 2, paramName, sizeof (paramName));
        lua_settop(L, 0);
        if (strcmp(signalName, "args") == 0) { // доступ к параметрам командной строки...
            for (i = 0; i < SystemState.argc; i++) {
                param = (char *) get_ParamValue(SystemState.argv[i], paramName);
                if (param != NULL) {
                    //i = atoi(param); lua_pushnumber(L, i); //
                    if (SystemState.fDebugCore)
                        Log_DBG("DBG Script mcGetParam()> (%s, %s) : '%s'", SystemState.argv[i], paramName, param);
                    lua_pushstring(L, param);
                    break;
                }
            }
            if (param == NULL) // Если параметр не найден вернем nil
            {
                lua_pushnil(L); // lua_pushnumber(L, -1);
            }
        } else {
            i = GetSignalByName(signalName);
            if (i != -1) {
                if (strcmp(paramName, "REQINTERVAL") == 0) { // Служебное поле
                    snprintf(paramValue, sizeof (paramValue), "%d", mcGetSignalRec(i)->readingdelay);
                    lua_pushstring(L, paramValue); //i = atoi(param); lua_pushnumber(L, i);
                } else {
                    param = (char *) get_ParamValue(mcGetSignalRec(i)->param, paramName);
                    i = -1;
                    if (param != NULL)
                        lua_pushstring(L, param); //i = atoi(param); lua_pushnumber(L, i);
                    else
                        lua_pushstring(L, "");
                }
            } else lua_pushnil(L); //lua_pushnumber(L, -1); //
        }
        result = 1;
        //------------------------------------
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcGetParam')");
        lua_settop(L, 0);
    }

    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcGetParam(...)...end\n ");
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcGetParam()");
    return result;
}


// доступ к параметрам сигнала (изменение)

static int script_mcSetParam_proc(lua_State *L) {
    int i;
    char signalName[255];
    char paramName[255];
    char paramValue[255];
    char * param = NULL;
    int iValue;
    char * p;
    size_t t;

    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcSetParam(...)\n ");

    script_synchronize(SCRIPT_SYNC_LOCK, "mcSetParam()");
    i = lua_gettop(L);
    if (i == 3) {
        scrGetStrArg(L, 1, signalName, sizeof (signalName));
        scrGetStrArg(L, 2, paramName, sizeof (paramName));
        scrGetStrArg(L, 3, paramValue, sizeof (paramValue));
        lua_settop(L, 0);
        if (strcmp(signalName, "sysparams...") == 0) { // доступ к служебным параметрам...

        } else {
            i = GetSignalByName(signalName);
            if (i != -1) {
                // param = (char *) get_ParamValue(mcGetSignalRec(i)->param, paramName);
                if (strcmp(paramName, "REQINTERVAL") == 0) { // Служебное поле
                    iValue = atoi(paramValue);
                    if (iValue) {
                        mcGetSignalRec(i)->readingdelay = iValue;
                        if (iValue > RD_TIME_MASK) {
                            mcGetSignalRec(i)->data->disabled = 1;
                            Log_DBG("DBG. Signal: '%s', DISABLED", signalName);
                        } else
                            mcGetSignalRec(i)->data->disabled = 0;
                        Log_DBG("mcSetParam('%s', '%s', '%s') : %d", signalName, paramName, paramValue, iValue);
                    }
                } else
                    if (strcmp(paramName, "ENABLED") == 0) { // Служебное поле
                    iValue = atoi(paramValue);
                    if (iValue) {
                        // Установим метку времени lastReqTime. По просьбе Максима, после
                        // Команды ENABLED=1, фоновый Req будет сформирован не раньше чем через 
                        // время заданное REQINTERVAL.
                        mcGetTime(&mcGetSignalRec(i)->data->lastReqTime); // сохраним время формирования запроса для данного сигнала
                        // -----------------------------------------------------
                        mcGetSignalRec(i)->data->disabled = 0;
                        Log_DBG("DBG. Signal: '%s', ENABLED", signalName);
                    } else {
                        mcGetSignalRec(i)->data->disabled = 1;
                        Log_DBG("DBG. Signal: '%s', DISABLED", signalName);
                    }
                } else {
                    Log_DBG("mcSetParam('%s', '%s', '%s') : param not found", signalName, paramName, paramValue);
                }

            } else
                Log_Ex("luaerr", LEVEL_ERR, (char *) "Error. func: 'mcSetParam('%s', '%s', '%s') : signal not found)", signalName, paramName, paramValue);

        }
        //------------------------------------
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcSetParam(Signal, Param, Value)')");
    }
    lua_settop(L, 0);
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcSetParam()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcSetParam(...)...end\n ");

    return 0; // кол-во возвращенных аргументов
}


// доступ к файлу конфигурации
// mcConfig("FileName.ini", "GET", "Param", "i8", 1) // 1 - значение по умолчанию
// mcConfig("FileName.ini", "SET", "Param", "i8", 1) -> Param = 1
// mcConfig("FileName.ini", "SET", "BuffParam", "c", buff) -> BuffParam = "00AACCBB"
// mcConfig("FileName.ini", "SET", "StrParam", "sz", "STR") -> StrParam = "STR"

static int script_mcConfig_proc(lua_State *L) {
    int i;
    int result = 0;
    char fileName[255];
    char opId[10];
    char paramName[255];
    char typeId[10];
    //char * p;
    double numValue = 0;
    size_t t;
    Tsub_param_rec Value;
    Tsub_param_rec * outValue;


    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcConfig(...)\n ");

    script_synchronize(SCRIPT_SYNC_LOCK, "mcConfig()");
    i = lua_gettop(L);
    if (i == 5) {//
        scrGetStrArg(L, 1, fileName, sizeof (fileName));
        scrGetStrArg(L, 2, opId, sizeof (opId));
        scrGetStrArg(L, 3, paramName, sizeof (paramName));
        scrGetStrArg(L, 4, typeId, sizeof (typeId));

        Value.tp = SL_TypeStr2TypeID(typeId);
        if (Value.tp != TYPE_ID_UNDEF) {
            if ((Value.tp == TYPE_ID_STRUCT) || (Value.tp == TYPE_ID_STRING0)) {
                Value.size = scrGetStrArg(L, 5, Value.scvalue, sizeof (Value.scvalue));
            } else { // числовые типы...
                //TYPE_ID_BYTE_SIGN
                Value.size = SL_GET_Size(Value.tp);
                numValue = lua_tonumber(L, 5);
                VALUE_CONVERT_from_Double(&Value, numValue);
            }
            lua_settop(L, 0);
            //------------------------------------    
            if (SystemState.fDebugCore) {
                Log_DBG("DBG Script> mcConfig(%s, %s, %s, %s)", fileName, opId, paramName, VALUE_CONVERT_to_Text(&Value));
            }
            if (strcmp(opId, "GET") == 0) { // 
                // чтение параметра из файла Tsub_param_rec * GetConfigParam(FILE * f, char * paramName, Tsub_param_rec * defaultValue) 
                // 
                outValue = GetConfigParam(fileName, paramName, &Value);
                if (&Value == outValue) { // параметр не найден

                } else { // 
                    if (SystemState.fDebugCore) {
                        Log_DBG("DBG Script> GetConfigParam(%s, %s) : %s", fileName, paramName, VALUE_CONVERT_to_Text(outValue));
                    }

                }
                // Для отладки сделаю вывод в диагностич. формате (с указанием типа)
                lua_pushstring(L, VALUE_CONVERT_to_Text(outValue));
                result = 1;
            } else
                if (strcmp(opId, "SET") == 0) { // Tsub_param_rec * SetConfigParam(FILE * f, char * paramName, Tsub_param_rec * Value) 
                // чтение параметра из файла
                outValue = SetConfigParam(fileName, paramName, &Value);

            } else {
                Log_Ex("luaerr", LEVEL_ERR, (char *) "OPERATION ILLEGAL! %s method: mcConfig()", opId);
            }


            //------------------------------------    
        } else {
            lua_settop(L, 0);
            Log_Ex("luaerr", LEVEL_ERR, (char *) "TYPE ILLEGAL! TYPE_ID_UNDEF method: mcConfig()");
            lua_pushnil(L); // 
            result = 1; // разместим nil как признак ошибки.

        }

        //------------------------------------
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcConfig(Signal, {GET|SET}, Param, Type, Value)')");
    }
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcConfig()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcConfig(...)...end\n ");

    return result; // кол-во возвращенных аргументов
}




// функция поиска сигналов по указанным параметрам
// mcSignalsSelect("A=1; B=5") -> {SignalName1, SignalName2, ... SignalNameN}

static int script_mcSignalsSelect_proc(lua_State *L) {
    int i, j, n = 0;
    int paramsCount = 0;
    int startPos = 0;
    int paramsCompareOk = 0;
    char sSelect[255];
    char paramSelectValue[255];
    char paramCurrentValue[255];
    char sCurrParams[255];
    char sParamsName[10][255]; //{"A", "B"...}
    int result = 0;

    char * p;
    size_t t;


    script_synchronize(SCRIPT_SYNC_LOCK, "mcSignalsSelect()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcSignalsSelect(...)\n ");


    i = lua_gettop(L);
    if (i == 1) {
        scrGetStrArg(L, 1, sSelect, sizeof (sSelect));
        // выделим из строки параметров поиска имена параметров
        for (j = 1; j < strlen(sSelect); j++) {
            if (sSelect[j] == '=')
                strncpy(sParamsName[paramsCount++], &sSelect[startPos], j - startPos);
            if ((sSelect[j] == ';') || (sSelect[j] == ' '))
                startPos = j + 1;
        }

        //if (SystemState.fDebugCore)
        //  for (j = 0; j < paramsCount; j++)
        //    Log_Ex("luaerr", LEVEL_ERR, (char *) "Param %d = %s", j, sParamsName[j]);
        //-----------------------------------------------------
        //--- найдем сигналы подходящие под указанные параметры и разместим их в таблицу результата
        //-----------------------------------------------------
        lua_settop(L, 0);
        lua_createtable(L, paramsCount, paramsCount);

        for (i = 0; i < mcGetSignalsCount(); i++) {
            snprintf(sCurrParams, sizeof (sCurrParams), "%s", mcGetSignalRec(i)->param);
            //
            paramsCompareOk = 0;
            for (j = 0; j < paramsCount; j++) { // проверим условие для каждого параметра
                p = (char *) get_ParamValue(sCurrParams, sParamsName[j]);
                if (p != NULL) {
                    snprintf(paramCurrentValue, sizeof (paramCurrentValue), "%s", p);
                    // возьмем требуемое значение из строки запроса
                    p = (char *) get_ParamValue(sSelect, sParamsName[j]);
                    snprintf(paramSelectValue, sizeof (paramSelectValue), "%s", p);

                    if (strcmp(paramCurrentValue, paramSelectValue) == 0) // значения равны
                    {
                        paramsCompareOk++;
                    }
                }
            }
            if (paramsCompareOk == paramsCount) {
                lua_pushnumber(L, n++); // ключ - индекс элемента
                lua_pushstring(L, mcGetSignalRec(i)->name); // имя сигнала
                lua_settable(L, -3); // 3-й элемент с вершины стека
            }
        }

        // напоминание: пример создания таблицы
        //        lua_settop(L, 0);
        //        lua_createtable(L, paramsCount, paramsCount);
        //        for (i = 0; i < paramsCount; i++) {
        //            lua_pushnumber(L, i); // ключ - значение. В качестве ключа будем использовать индекс элемента 
        //            lua_pushstring(L, sParamsName[i]); // разместим значение "V" в стек для функции lua_rawseti.
        //            lua_settable(L, -3); // 3-й элемент с вершины стека
        //        }
        result = 1; // кол-во возвращенных аргументов
        if (SystemState.fDebugCore)
            Log_DBG("DBG Script> mcSignalsSelect() lua_gettop = %d", lua_gettop(L));

        //------------------------------------
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcSignalsSelect')");
        lua_settop(L, 0);
    }
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcSignalsSelect()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcSignalsSelect(...)...end\n ");

    return result;
}



// связь с ядром. Функции поиска

static int script_core_proc(lua_State *L) {
    int i;
    char sName[255];
    char * p;
    size_t t;
    TValue * d;
    int result = 0;
    script_synchronize(SCRIPT_SYNC_LOCK, "mcFunc()");
    i = lua_gettop(L);
    if (i >= 1) {
        scrGetStrArg(L, 1, sName, sizeof (sName));
        // Реализация различных функций сервера
        //------------------------------------
        result = 0; // кол-во возвращенных аргументов
        if (strcmp(sName, "EXIT") == 0) //
        {
            // завершение работы. Аргумент: код возврата
            if (i >= 2) {
                i = lua_tonumber(L, 2);
                Log_DBG((char *) "mcFunc(EXIT, %d)", i);
                exit(i);
            } else {
                Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcFunc(EXIT, ...)')");
            }
        }
        if (strcmp(sName, "GETTIME") == 0) //
        {
            lua_pushnumber(L, mcGetTime(0));
            // завершение работы. Аргумент: код возврата
            result = 1; // кол-во возвращенных аргументов
        }
        if (strcmp(sName, "GETTICK") == 0) //
        {
            lua_pushnumber(L, mcGetDeltaTime(&SystemState.serverStartTime, NULL));
            // завершение работы. Аргумент: код возврата
            result = 1; // кол-во возвращенных аргументов
        }


        //------------------------------------
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcFunc')");
        lua_settop(L, 0);
    }
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcFunc()");
    return result;
}

static int script_mcLinkOpen_proc(lua_State *L) {
    // Открытие линии связи. Пример вызова:
    //mcLinkOpen (11, "COM", "/dev/ttyS0", "115200,8N1", "RAW")
    int N;
    int LinkID;
    char DriverName[255];
    char DeviceName[255];
    char linkParam[255];
    char ProtocolName[255];
    char ProtocolParams[255] = "";
    char * p;
    size_t t;
    TValue * d;
    script_synchronize(SCRIPT_SYNC_LOCK, "mcLinkOpen()");
    N = lua_gettop(L);
    if (N >= 5) { // >= 5 аргументов
        LinkID = lua_tonumber(L, 1);
        scrGetStrArg(L, 2, DriverName, sizeof (DriverName));
        scrGetStrArg(L, 3, DeviceName, sizeof (DeviceName));
        scrGetStrArg(L, 4, linkParam, sizeof (linkParam));
        scrGetStrArg(L, 5, ProtocolName, sizeof (ProtocolName));
        if (N > 5) { // > 5 аргументов +(параметры протокола))
            scrGetStrArg(L, 6, ProtocolParams, sizeof (ProtocolParams));
        }
        Log_DBG("mcLinkOpen(%d, %s, %s, %s, %s [%s])", LinkID, DriverName, DeviceName, linkParam, ProtocolName, ProtocolParams);
        // вызов функции открытия канала связи...
        // --------------------------------------
        coreLinkOpen(OBJ_SOURCE_SCRIPT, LinkID, DriverName, DeviceName, linkParam, ProtocolName, ProtocolParams);
        // --------------------------------------
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcLinkOpen')");
    }
    lua_settop(L, 0);
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcLinkOpen()");

    return 0;
}

static int script_mcLinkTx_proc(lua_State *L) {
    // Запись в канал связи:
    //mcLinkTx (11, "DATABUFF")
    int N;
    int LinkID;
    char buffer[STR_MAX_SIZE];
    int bufferSize;
    int result = -1;
    char * p;
    size_t t;

    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcLinkTx(...)\n ");

    script_synchronize(SCRIPT_SYNC_LOCK, "mcLinkTx()");
    N = lua_gettop(L);
    if (N == 2) { // = 2 аргумента
        LinkID = lua_tonumber(L, 1);
        p = (char*) lua_tolstring(L, 2, &t);
        bufferSize = t;
        if (bufferSize >= STR_MAX_SIZE) {
            Log_Ex("luaerr", LEVEL_ERR, (char *) "buffer overflow (size = %d). (func: 'mcLinkTx')", bufferSize);
            bufferSize = STR_MAX_SIZE;
        }
        memcpy(buffer, (char *) p, bufferSize);
        //Log_DBG("mcLinkTx(%d, buff[%d])",LinkID, bufferSize);
        // вызов функции записи в канал связи...
        // --------------------------------------
        result = coreLinkTx(OBJ_SOURCE_SCRIPT, LinkID, buffer, bufferSize);
        // --------------------------------------
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcLinkTx')");
    }
    lua_settop(L, 0);
    if (result == MAC_RESULT_DEFERRED)
        lua_pushstring(L, "DEF");
    else
        if (result == MAC_RESULT_OK)
        lua_pushstring(L, "OK");
    else
        lua_pushstring(L, "ERR");
    lua_pushinteger(L, result); // числовое значение возврата. -1, -2, ... Коды ошибок (определяются драйвером)

    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcLinkTx()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcLinkTx(...)...end\n ");

    return 2;
}

static int script_mcLinkRx_proc(lua_State *L) {
    // Чтение из канала связи:
    //mcLinkRx (11) - чтение того что есть в буфере (кол-во не указывается)
    //mcLinkRx (11, count, timeOut ) - чтение не менее count байт c тайм-аутом
    int N;
    int LinkID;
    int TimeOut;
    int Count;
    char buffer[STR_MAX_SIZE];
    int bufferSize;
    int resultCoreLinkRx;
    int result = 0;
    char * p;
    size_t t;
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcLinkRx(...)\n ");

    script_synchronize(SCRIPT_SYNC_LOCK, "mcLinkRx()");
    N = lua_gettop(L);
    if (N == 3) { // = 3 аргумента
        LinkID = lua_tonumber(L, 1);
        Count = lua_tonumber(L, 2);
        TimeOut = lua_tonumber(L, 3);
        lua_settop(L, 0);
        //Log_DBG("mcLinkRx(%d, count = %d, timeOut = %d)", LinkID, Count, TimeOut);
        // вызов функции чтения канала связи...
        // --------------------------------------
        bufferSize = Count;
        resultCoreLinkRx = coreLinkRx(OBJ_SOURCE_SCRIPT, LinkID, buffer, &bufferSize, TimeOut);
        if (resultCoreLinkRx == MAC_RESULT_DEFERRED) {
            lua_pushstring(L, "DEF");
            lua_pushstring(L, "");
        } else
            if (resultCoreLinkRx == MAC_RESULT_OK) {
            lua_pushstring(L, "OK");
            lua_pushlstring(L, buffer, bufferSize);
        } else {
            lua_pushstring(L, "ERR");
            lua_pushstring(L, "");
        }
        //        script_synchronize(SCRIPT_SYNC_UNLOCK);
        //        return 2; // возвращаем 2 параметра - статус операции и буфер
        result = 2;

        // статус: {"OK" | "ERR"}
        // --------------------------------------
    } else
        if (N == 1) { // = 1 аргумента
        LinkID = lua_tonumber(L, 1);
        lua_settop(L, 0);
        //Log_DBG("mcLinkRx(%d)",LinkID);
        // вызов функции записи в канал связи...
        // --------------------------------------
        bufferSize = 0;
        resultCoreLinkRx = coreLinkRx(OBJ_SOURCE_SCRIPT, LinkID, buffer, &bufferSize, 0);
        // --------------------------------------
        if (resultCoreLinkRx == MAC_RESULT_DEFERRED) {
            lua_pushstring(L, "DEF");
            lua_pushstring(L, "");
        } else
            if (resultCoreLinkRx == MAC_RESULT_OK) {
            lua_pushstring(L, "OK");
            lua_pushlstring(L, buffer, bufferSize);
        } else {
            lua_pushstring(L, "ERR");
            lua_pushstring(L, "");
        }
        result = 2;
        //return 2; // возвращаем 2 параметра - статус операции и буфер
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcLinkRx')");
        lua_settop(L, 0);
    }
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcLinkRx(...)...end\n ");
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcLinkRx()");

    return result;
}


// функция получения значения сигнала по имени
// Пример вызова из Lua: v, f = get("MKO_UK[1].MKVI.VREMY")

static int script_get_proc(lua_State *L) {
    int i;
    char sName[255];
    double value;
    char * p;
    int index; // индекс сигнала в таблице сигналов.
    size_t t;
    TValue * d;
    char result = 0;

    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> get(...)\n ");

    script_synchronize(SCRIPT_SYNC_LOCK, "get()");
    i = lua_gettop(L);

    if (i == 1) { // один аргумент
        scrGetStrArg(L, 1, sName, sizeof (sName));
        lua_settop(L, 0); // вернем вершину стека на место
        index = GetSignalByName(sName);
        if (index == -1) { // сигнал не найден
            Log_Ex("luaerr", LEVEL_ERR, (char *) "SIGNAL MISMATCH! %s method: get()", sName);
        } else {
            d = (TValue *) mcGetSignalRec(index)->data;
            if (d->v.tp == TYPE_ID_STRUCT) {
                lua_pushlstring(L, d->v.scvalue, d->v.size);
            } else
                if (d->v.tp == TYPE_ID_STRING0) {
                lua_pushstring(L, d->v.scvalue);
            } else {
                if (isVALUE_CONVERT_to_Double(&d->v)) {
                    value = VALUE_CONVERT_to_Double(&d->v);
                    lua_pushnumber(L, value);
                } else {
                    lua_pushnumber(L, 0);
                    Log_Ex("luaerr", LEVEL_ERR, (char *) "convertion error (func: 'get')");
                }
            }
            lua_pushboolean(L, d->flag);
            result = 2;
        }
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'get')");
        lua_settop(L, 0); // вернем вершину стека на место

    }
    script_synchronize(SCRIPT_SYNC_UNLOCK, "get()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> get(...)...end\n ");

    return result;
}

#endif

Tsub_param_rec * Signal_script_run(char * idEvent, int index, int func, TValue * value) {
#ifdef USE_START_SCRIPT
    double d, d0;
    int i, i0;
    int top;
    char strValue[255] = "";
    Tsub_param_rec * result = NULL;

    linkFuncEntryCount++;
    if (SystemState.fDebugCore) {
        Log_DBG("DBG Script:CORE> script_run(%d, %s) ", index, VALUE_CONVERT_to_Text(&value->v));
    }
    if (linkFuncEntryCount > 1) {
        Log_DBG("DBG Script:CORE> script_run(%d) Entry Count = %d! ", index, linkFuncEntryCount);
        Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG Script:CORE> script_run(%d) Entry Count = %d! ", index, linkFuncEntryCount);
    }

    // core_synchronize(CORE_SYNC_LOCK, "SignalScript");



    i0 = i = value->flag;
    if (value->v.tp == TYPE_ID_STRING0) {
        // аргумент - строка
    } else {
        if (isVALUE_CONVERT_to_Double(&value->v)) {
            d0 = d = VALUE_CONVERT_to_Double(&value->v);
        } else {
            d0 = d = 0;
            Log_Ex("luaerr", LEVEL_ERR, (char *) "convertion error (script_run)");
        }
    }

    // функция вложенная Нужно правильно работать со стеком
    top = lua_gettop(Script);
    if (top != 0)
        Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG Script:CORE> script_run(%s, %s)> lua_gettop = %d", idEvent, mcGetSignalRec(index)->name, top);

    lua_rawgeti(Script, LUA_REGISTRYINDEX, func);
    lua_pushstring(Script, idEvent); // Аргумент 1 ("SET"/"GET").
    if (value->v.tp == TYPE_ID_STRING0) {
        // аргумент - строка
        lua_pushstring(Script, value->v.strvalue);
    } else { // число
        lua_pushnumber(Script, d);
    }
    lua_pushboolean(Script, i);
    lua_pushstring(Script, mcGetSignalRec(index)->name); // Имя сигнала
    lua_call(Script, 4, 2); // вызовем связанную Lua - функцию

    if (value->v.tp == TYPE_ID_STRING0) {
        // аргумент - строка
        scrGetStrArg(Script, top + 1, &strValue[0], sizeof (strValue));
        //Log_Ex("dbg", LEVEL_ERR, (char *) "strValue = %s", strValue);

    } else { // число
        d = lua_tonumber(Script, top + 1);
    }
    i = lua_toboolean(Script, top + 2);
    lua_settop(Script, top); // вернем вершину стека на место
    // Log_DBG("DBG. Script: '%s' value: %f -> %f, flag: %d -> %d", Signals[index].name, d0, d, i0, i);
    value->flag = i;
    if (strcmp(idEvent, "GET") == 0) // событие запроса
    {
        //flagUseSignalBufferValue = 1;
        //        Log_DBG("DBG. Script: IN value->v.tp=%d, value->v.size=%d value->v.bvalue=%d", value->v.tp, value->v.size, value->v.bvalue);
        memcpy(&tempSignalBufferValue, &value->v, sizeof (tempSignalBufferValue));
        VALUE_CONVERT_from_Double(&tempSignalBufferValue, d);
        result = &tempSignalBufferValue;
    } else {
        if (value->v.tp == TYPE_ID_STRING0) {
            VALUE_SET_String0(&value->v, strValue);
        } else {
            VALUE_CONVERT_from_Double(&value->v, d);
        }
        //return NULL;
    }

    // core_synchronize(CORE_SYNC_UNLOCK, "SignalScript");

    if (SystemState.fDebugCore) {
        Log_DBG("DBG Script:CORE> script_run(%d): %s ...end ", index, VALUE_CONVERT_to_Text(&value->v));
    }

    linkFuncEntryCount--;

    return result;
#else
    Log_DBG("DBG. Script unused...");
    return NULL;

#endif
}

int script_polling_callback(char * idEvent, char * Name, int func) {
#ifdef USE_START_SCRIPT

    static char EventText[10] = "POLL"; // происходит непонятное падение в функции script_polling_callback, решил сократить аргумент idEvent
    int result;
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script:CORE> polling(%s) coreLockCount = %d\n ", EventText, coreLockCount);

    core_synchronize(CORE_SYNC_LOCK, "polling");
    //!!!! Внимание! Тест!

    //Было !!! 
    int top = lua_gettop(Script);
    if (top != 0)
        Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG Script:CORE> polling> lua_gettop = %d", top);

    lua_rawgeti(Script, LUA_REGISTRYINDEX, func);

    // не работает lua_pushcfunction(Script, (lua_CFunction) func); // Аргумент 1.
    lua_pushstring(Script, EventText); // Аргумент 1.
    lua_pushstring(Script, Name); // Аргумент 2. Имя Объекта
    lua_call(Script, 2, 1); // вызовем связанную Lua - функцию
    result = lua_tonumber(Script, top + 1);
    lua_settop(Script, top); // вернем вершину стека на место
    //Log_DBG("DBG. Script: polling(%s) -> %d",idEvent, result);
    core_synchronize(CORE_SYNC_UNLOCK, "polling");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script:CORE> polling(%s)...end\n ", EventText);

    return result;
#else
    Log_DBG("DBG. Script unused...");
    return 0;
#endif
}

int script_line_callback(char * idEvent, char * Name, int func, char * buffInput, int buffInputSize, char * buffOutput, int * buffOutputSize, char * ErrorText) {
#ifdef USE_START_SCRIPT
    char result[255]; // Идентификатор события с текстовом виде
    int resultSize = 0; // резмер возвращенного буффера
    char Buffer[255]; // Идентификатор события с текстовом виде
    int BufferSize = 0; // резмер возвращенного буффера
    //    char * pResult;
    int top;
    char * pBuffer;
    int resultCallback = 0; // резмер возвращенного буффера
    size_t t;
    //  function MyLineProcTest(idEvent, buffer)

    core_synchronize(CORE_SYNC_LOCK, "line");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script:CORE> line(%s)\n ", idEvent);

    top = lua_gettop(Script);
    if (top != 0)
        Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG Script:CORE> line(%s)> lua_gettop = %d", idEvent, top);

    lua_rawgeti(Script, LUA_REGISTRYINDEX, func);
    lua_pushstring(Script, idEvent); // Аргумент 1.
    lua_pushlstring(Script, buffInput, buffInputSize); // Аргумент 2 Буфер на входе.
    lua_pushstring(Script, Name); // Аргумент 3 Индекс линии.
    lua_call(Script, 3, 2); // вызовем связанную Lua - функцию
    scrGetStrArg(Script, top + 1, result, sizeof (result));
    //    t = sizeof (result);
    //    pResult = (char*) lua_tolstring(Script, top + 1, &t);
    //    resultSize = t;
    pBuffer = (char*) lua_tolstring(Script, top + 2, &t);
    BufferSize = t;
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script:CORE> line Lua(%s): Result size = %d, Buffer size = %d\n ", idEvent, resultSize, BufferSize);

    //    Log_DBG("DBG. Script: line(%s, [%d]) %s, [%d]",idEvent, buffInputSize, pResult, BufferSize);

    if (strcmp(result, "OK") == 0) // без ошибок
    { // Если буфер в ответе не пустой, значит уже получен ответ!
        if (buffOutput != NULL) { // переместим полученный от скрипта буфер по указанному адресу
            if (BufferSize < CLIENT_RX_SIZE)
                memcpy(buffOutput, (char *) pBuffer, BufferSize);
            *buffOutputSize = BufferSize;
        }
        resultCallback = MAC_RESULT_OK; //OK
    } else
        if (strcmp(result, "DEF") == 0) // Отложенная запись...
    { //
        *buffOutputSize = 0;
        resultCallback = MAC_RESULT_DEFERRED; //"DEF" - Отложенная запись...
    } else {
        Log_DBG("DBG. Script: Line CallBack(%s, [%d]) -> %s", idEvent, buffInputSize, result);
        resultCallback = MAC_RESULT_ERROR; //ERR
    }

    lua_settop(Script, top); // вернем вершину стека на место

    if (SystemState.fDebugCore)
        Log_DBG("DBG Script:CORE> line(%s)...end\n ", idEvent);
    core_synchronize(CORE_SYNC_UNLOCK, "line");

    return resultCallback;
#else
    Log_DBG("DBG. Script unused...");
    return 0;
#endif
}

int script_framing_callback(char * idEvent, int func, char * buffInput, int * buffInputSize, char * buffOutput, int * buffOutputSize, char * ErrorText) {
#ifdef USE_START_SCRIPT
    char result[255]; // Идентификатор события с текстовом виде
    int resultSize = 0; // резмер возвращенного буффера
    int resultFrame = 0; // резмер возвращенного буффера
    char * p;
    size_t t;
    //  function MyLineProcTest(idEvent, buffer)
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script:CORE> framing(%s)\n ", idEvent);

    core_synchronize(CORE_SYNC_LOCK, "framing");

    lua_rawgeti(Script, LUA_REGISTRYINDEX, func);
    lua_pushstring(Script, idEvent); // Аргумент 1.
    lua_pushlstring(Script, buffInput, *buffInputSize); // Аргумент 2 Буфер на входе.
    script_ErrorText[0] = 0; // очистим признак ошибки
    if (strcmp(idEvent, "RX") == 0) // для события "RX" - возврат 2-х аргументов
        lua_call(Script, 2, 2); // вызовем связанную Lua - функцию
    else
        lua_call(Script, 2, 1); // вызовем связанную Lua - функцию
    if (script_ErrorText[0]) // копирование строки ошибки (если есть)
        strcpy(ErrorText, script_ErrorText);
    t = sizeof (result);
    p = (char*) lua_tolstring(Script, 1, &t);
    resultSize = t;
    resultFrame = t;
    if (buffOutput != 0) { // переместим полученный от скрипта буфер по указанному адресу
        memcpy(buffOutput, (char *) p, resultSize);
        *buffOutputSize = resultSize;
    }
    if (strcmp(idEvent, "RX") == 0) // для события "RX" - возврат 2-х аргументов
    { // присвоим новое значение входного буфера
        // !!! нужно помнить, что при работе функции Lua запись в rx-буфер со стороны линии связи не должна производиться
        p = (char*) lua_tolstring(Script, 2, &t);
        resultSize = t;
        memcpy(buffInput, (char *) p, resultSize);
        *buffInputSize = resultSize;
    }

    lua_settop(Script, 0); // вернем вершину стека на место
    //Log_DBG("DBG. Script: line(%s, buff[%d]) -> buff[%d]",idEvent, *buffInputSize, resultSize);
    core_synchronize(CORE_SYNC_UNLOCK, "framing");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script:CORE> framing(%s):%d...end\n ", idEvent, resultFrame);

    return resultFrame;
#else
    Log_DBG("DBG. Script unused...");
    return 0;
#endif
}

int script_protocol_callback(char * idEvent, int LineIndex, int func, void * UI, char * signalName, Tsub_param_rec * value, char * pktInBuffer, int pktInSize, char * pktOutBuffer, int * pktOutSize, char * ErrorText) {
#ifdef USE_START_SCRIPT
    char result[255]; // Идентификатор события с текстовом виде
    int resultSize = 0; // резмер возвращенного буффера
    int resultValue = PROT_MNG_RESULT_ERR; // PROT_MNG_RESULT_ERR
    double f64value;
    char * p;
    size_t t;
    // ------------------------------------------------------------------------
    // function MyManagerProc(idEvent, signalName, signalValue, packet)
    // -- idEvent: "REQ", "SIGNAL_KU", "SIGNAL_REQ", "RX", "ERROR", "ABORT"
    // -- "REQ": событие запроса. Функция сама решает какой запрос формировать
    // -- "SIGNAL_KU": очередь на запись на данном интерфейсе не пустасобытие запроса. Функция сама решает какой запрос формировать
    // -- Возврат: ["OK" | "ERROR" | "REPEAT" | "RETRY" | "ABORT"], ПАКЕТ_НА_ЗАПИСЬ
    // ------------------------------------------------------------------------
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script:CORE> protocol(%s)\n ", idEvent);

    core_synchronize(CORE_SYNC_LOCK, "protocol");
    int top = lua_gettop(Script);
    if (top != 0)
        Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG Script:CORE> protocol(%s)> lua_gettop = %d", idEvent, top);


    lua_rawgeti(Script, LUA_REGISTRYINDEX, func);
    lua_pushstring(Script, idEvent); // Аргумент 1.
    lua_pushstring(Script, signalName); // Аргумент 2. (имя сигнала)
    if (value != NULL) {
        if (value->tp == TYPE_ID_STRUCT)
            lua_pushlstring(Script, value->stvalue, value->size); // передаем массив
        else
            if (value->tp == TYPE_ID_STRING0)
            lua_pushstring(Script, value->stvalue); // передаем строку
        else {
            if (isVALUE_CONVERT_to_Double(value)) {
                f64value = VALUE_CONVERT_to_Double(value);
                lua_pushnumber(Script, f64value); // передаем число
            } else {
                Log_Ex("luaerr", LEVEL_ERR, (char *) "convertion error (protocol_callback)");
                lua_pushnumber(Script, 0); // Ошибка!
            }
        }
    } else
        lua_pushstring(Script, ""); // Аргумент 3. (значение сигнала)
    if (pktInSize)
        lua_pushlstring(Script, pktInBuffer, pktInSize); // Аргумент 4 Буфер пакета
    else
        lua_pushstring(Script, ""); // Аргумент 4. пустой буфер

    if (LineIndex < SystemState.LLC.count)
        lua_pushnumber(Script, SystemState.LLC.Lines[LineIndex]->id); // Идентификатор линии
    else
        lua_pushnumber(Script, -1); // Идентификатор линии? Ошибка!

    script_ErrorText[0] = 0; // очистим признак ошибки
    lua_call(Script, 5, 2); // вызовем связанную Lua - функцию
    if (script_ErrorText[0]) // копирование строки ошибки (если есть)
        strcpy(ErrorText, script_ErrorText);
    //t = sizeof (result);
    //p = (char*) lua_tolstring(Script, top + 1, &t);
    //strcpy(result, (char *) p);
    scrGetStrArg(Script, top + 1, result, sizeof (result));

    p = (char*) lua_tolstring(Script, top + 2, &t);
    resultSize = t;
    if (pktOutBuffer != NULL) { // переместим полученный от скрипта буфер по указанному адресу
        memcpy(pktOutBuffer, (char *) p, resultSize);
        *pktOutSize = resultSize;
    } else
        Log_DBG("DBG. Script: protocol_callback(pktOutBuffer == NULL)!");

    lua_settop(Script, top); // вернем вершину стека на место
    //    Log_DBG("DBG. Script: protocol_callback(%s) : %s buff=%s",idEvent, result, Log_buf2str(pktOutBuffer, resultSize));
    if (strcmp(result, "OK") == 0) // 
    {
        resultValue = PROT_MNG_RESULT_OK;
    } else {
        Log_DBG("ERR!. Script: protocol_callback(%s) : %s", idEvent, result);
        if (strcmp(result, "ERROR") == 0)
            resultValue = PROT_MNG_RESULT_ERR;

    }
    core_synchronize(CORE_SYNC_UNLOCK, "protocol");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script:CORE> protocol(%s):%d...end\n ", idEvent, resultValue);

    return resultValue; //

#else
    Log_DBG("DBG. Script unused...");
    return 0;
#endif
}




// событие от сервера сигналу

int Signal_script_callback(int LuaFuncId, char typeCallBack, void * ext, int index, TMCSignalsInfo * sgInfo, unsigned short int addr, Tsub_param_rec * value, char * flag, char ExtMode, int srcInterface) {
    //int SYS_LineField (char typeCallBack, void * ext,  int  index, unsigned short int  addr, Tsub_param_rec * value, char * flag, char ExtMode, int srcInterface)

#ifdef USE_START_SCRIPT
    char idEvent[100]; // Идентификатор события с текстовом виде
    int indexSignal; // Идентификатор текущего сигнала
    int addrSignal; // Логич. адрес сигнала ???
    //    int    hwField;             // поле доп. информации
    int luaFuncId; // Идентификатор функции обработки, переданный при добавлении сигнала
    double numberValue = -1; // значение сигнала в числовом виде
    char strValue[255] = ""; // значение сигнала в строковом виде (только для типов TYPE_ID_STRING0)
    char valueType[6]; // тип сигнала // типы i8, i16, i32, f32, f64, sz, c1111
    int flagSignal = 0; // флаг достоверности сигнала
    // для обработки результата:
    char strResult[9]; // Результат {"OK" | "ERROR"}
    char resultType; // тип сигнала при возврате
    char buffValue[STR_MAX_SIZE] = ""; // буфер, который возвращает пользовательская функция
    int buffSize = 0; // размер буфера пользователя
    Tsub_param_rec * ValueIn; // указатель на данные значения сигнала, которое передается в скрипт
    char * p;
    size_t t;
    int resultCallback = 0;

    TSignalsRec * a;

    TSystemState * State;


    if (ext != 0) {
        State = (TSystemState *) ext;

        if (index >= State->Data.Values.Count) { // Ошибка!!!
            Log_DBG("ERR!.  Signal_script_callback...");

        } else {

            core_synchronize(CORE_SYNC_LOCK, "Signal");
            if (SystemState.fDebugCore)
                Log_DBG("DBG Script:CORE> Signal(id=%d)\n ", typeCallBack);

            int top = lua_gettop(Script);
            if (top != 0)
                Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG Script:CORE> Signal> lua_gettop = %d", top);

            indexSignal = index;
            //    Log_DBG("DBG.  Signal_script_callback...");
            a = (TSignalsRec *) State->Data.Values.values->i[index]->pSignalRec;
            luaFuncId = LuaFuncId; //a->callBackAddr;
            ValueIn = &a->data->v; // текущее значение сигнала
            if ((typeCallBack == CALLBACK_TYPE_EVENT_REQ) || (typeCallBack == CALLBACK_TYPE_SET_SIGNAL))
                if (value != NULL)
                    ValueIn = value;
                else
                    Log_DBG("ERR! Signal REQ Event. Value = NULL!!!");

            flagSignal = a->data->flag; // текущее значение флага
            switch (typeCallBack) { // определение идентификаторов событий и указатель на значения
                case CALLBACK_TYPE_GET_SIGNAL:
                    snprintf(idEvent, sizeof (idEvent), "GET_SIGNAL");
                    break;
                case CALLBACK_TYPE_SET_SIGNAL:
                    snprintf(idEvent, sizeof (idEvent), "SET_SIGNAL");
                    ValueIn = value; // значение извне
                    if (flag != NULL) flagSignal = flag[0];
                    break;
                case CALLBACK_TYPE_EVENT_REQ:
                    snprintf(idEvent, sizeof (idEvent), "REQ");
                    if (flag != NULL) flagSignal = flag[0]; // 0: Автоматический req, 1 - ручной
                    break;
                case CALLBACK_TYPE_EVENT_RX:
                    snprintf(idEvent, sizeof (idEvent), "RX");
                    break;
                case CALLBACK_TYPE_EVENT_ERR:
                    snprintf(idEvent, sizeof (idEvent), "ERR");
                    break;
                case CALLBACK_TYPE_EVENT_ABORT:
                    snprintf(idEvent, sizeof (idEvent), "ABORT");
                    break;

                default:
                    Log_DBG("ERR!.  Signal_script_callback. ID is undef: %d", typeCallBack);

            }
            // смормируем аргументы перед вызовом...
            if (SL_TypeID2TypeStr(ValueIn->tp, valueType) == 0) { // Ошибка!
                Log_DBG("ERR!.  SL_TypeID2TypeStr(%d) = 0!", ValueIn->tp);
                exit(1);

            }
            if (ValueIn->tp == TYPE_ID_STRUCT)
                snprintf(&valueType[strlen(valueType)], sizeof (valueType), "%d", ValueIn->size);
            lua_rawgeti(Script, LUA_REGISTRYINDEX, luaFuncId);
            lua_pushstring(Script, idEvent); // Аргумент 1.
            lua_pushstring(Script, sgInfo->reqName); // Аргумент 2.
            lua_pushstring(Script, valueType); // Аргумент 3.
            lua_pushnumber(Script, flagSignal); // Аргумент 4.

            if (SystemState.fDebugCore) {
                Log_DBG("DBG Script:CORE> USER(%s, %s, %s, %f ...)\n ", idEvent, sgInfo->reqName, valueType, flagSignal);
            }

            // помещение аргумента "value"
            if (typeCallBack == CALLBACK_TYPE_EVENT_RX) { // для события RX помещаем буфер для связи с протокольным уровнем
                if (sgInfo->cmd != NULL)
                    lua_pushlstring(Script, sgInfo->cmd->user.b, sgInfo->cmd->userSize); // Аргумент 4 (прием от протокола).
                else
                    lua_pushstring(Script, ""); // Аргумент 4 (прием от протокола).
            } else {
                if (ValueIn->tp == TYPE_ID_STRUCT) {
                    lua_pushlstring(Script, ValueIn->strvalue, ValueIn->size); // Аргумент 5.
                } else
                    if (ValueIn->tp == TYPE_ID_STRING0) {
                    snprintf(strValue, sizeof (strValue), "%s", ValueIn->strvalue);
                    lua_pushstring(Script, strValue); // Аргумент 5.
                } else {
                    if (isVALUE_CONVERT_to_Double(ValueIn)) {
                        numberValue = VALUE_CONVERT_to_Double(ValueIn);
                        lua_pushnumber(Script, numberValue); // Аргумент 5.
                        if (SystemState.fDebugCore)
                            Log_DBG("DBG Script:CORE> ...USER( value = %f)\n ", numberValue);
                        //if ((numberValue > (0xC7 + 0.1)) || (numberValue < (0xC7 - 0.1)))
                        //    Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG: Signal(%s), Value = %f", sgInfo->reqName, numberValue);
                    } else {
                        Log_Ex("luaerr", LEVEL_ERR, (char *) "convertion error (script_run)");
                        lua_pushnumber(Script, 0); // Аргумент 5.

                    }

                }
            }


            // Преобразуем typeCallBack в строку, для удобства (наглядности) работы в скрипте
            if (typeCallBack == CALLBACK_TYPE_GET_SIGNAL) {
                lua_call(Script, 5, 4); // Ожидаем 4 параметра: (результат выполения, тип, значение, флаг)
                // чтение резельтата:
                scrGetStrArg(Script, top + 1, strResult, sizeof (strResult));

                //p = (char*) lua_tolstring(Script, top + 1, &t);
                //strcpy(strResult, (char *) p);
                scrGetStrArg(Script, top + 2, valueType, sizeof (valueType));
                //p = (char*) lua_tolstring(Script, top + 2, &t);
                //strcpy(valueType, (char *) p);
                resultType = SL_TypeStr2TypeID(valueType);
                value->tp = resultType; // заполним поле типа
                if (resultType == TYPE_ID_STRUCT) { //скрипт вернул структуру
                    p = (char*) lua_tolstring(Script, top + 3, &t);
                    VALUE_SET_Struct(value, p, t);
                } else if (resultType == TYPE_ID_STRING0) { //скрипт вернул строку
                    scrGetStrArg(Script, top + 3, strValue, sizeof (strValue));
                    //p = (char*) lua_tolstring(Script, top + 3, &t);
                    //strcpy(strValue, (char *) p);
                    VALUE_SET_String0(value, strValue);
                } else { // не строка.
                    numberValue = lua_tonumber(Script, top + 3);
                    if (resultType != TYPE_ID_UNDEF) { // формируем ответ
                        value->tp = resultType;
                        value->size = SL_GET_Size(resultType);
                        VALUE_CONVERT_from_Double(value, numberValue);
                    } else { // ошибка типа сигнала
                        VALUE_SET_String0(value, "srcipt type error!");
                    }
                }
                flagSignal = lua_tonumber(Script, top + 4); // флаг при возврате
                lua_settop(Script, top); // вернем вершину стека на место
                flag[0] = flagSignal;
                //return CALLBACK_RESULT_OK;
                resultCallback = CALLBACK_RESULT_OK;

            } else
                if (typeCallBack == CALLBACK_TYPE_SET_SIGNAL) {
                lua_call(Script, 5, 4); // Ожидаем 4 параметра: (результат выполения, тип, значение, флаг)
                // чтение резельтата:
                scrGetStrArg(Script, top + 1, strValue, sizeof (strValue));
                scrGetStrArg(Script, top + 2, valueType, sizeof (valueType));
                resultType = SL_TypeStr2TypeID(valueType);
                flagSignal = 1;
                if (lua_isnumber(Script, top + 4))
                    flagSignal = lua_tonumber(Script, top + 4); // флаг при возврате
                lua_settop(Script, top); // вернем вершину стека на место
                flag[0] = flagSignal;
                //return CALLBACK_RESULT_OK;
                resultCallback = CALLBACK_RESULT_OK;

            } else
                if (typeCallBack == CALLBACK_TYPE_EVENT_REQ) {
                lua_call(Script, 5, 2); // Ожидаем 2 параметра: (результат выполения, буфер запроса (для протокольного уровня))
                // чтение резельтата:
                scrGetStrArg(Script, top + 1, strValue, sizeof (strValue));
                //p = (char*) lua_tolstring(Script, top + 1, &t);
                //strcpy(strValue, (char *) p);
                p = (char*) lua_tolstring(Script, top + 2, &t);
                buffSize = t; //
                if (SystemState.fDebugCore)
                    Log_DBG("DBG Script:CORE> USER( REQ ): %s, buffer size = %d\n ", strValue, buffSize);

                memcpy(buffValue, (char *) p, buffSize);

                sgInfo->cmd->userSize = buffSize;
                memcpy(&sgInfo->cmd->user.b[0], (char *) p, buffSize);

                //Log_DBG("DBG.  Result: %s [%s]", strValue, Log_buf2str(buffValue, buffSize));
                lua_settop(Script, top); // вернем вершину стека на место
                if (flag != NULL) flag[0] = 1;

                resultCallback = CALLBACK_RESULT_ERROR;
                if (strcmp(strValue, "REPEAT") == 0)
                    resultCallback = CALLBACK_RESULT_NEED_REPEAT;

                if (strcmp(strValue, "OK") == 0)
                    resultCallback = CALLBACK_RESULT_OK;

            } else
                if (typeCallBack == CALLBACK_TYPE_EVENT_RX) {
                lua_call(Script, 5, 2); // Ожидаем 2 параметра: (результат выполения, буфер запроса (для протокольного уровня))
                // чтение резельтата:
                scrGetStrArg(Script, top + 1, strValue, sizeof (strValue));
                //                p = (char*) lua_tolstring(Script, top + 1, &t);
                //                strcpy(strValue, (char *) p);
                p = (char*) lua_tolstring(Script, top + 2, &t);
                buffSize = t; //
                memcpy(buffValue, (char *) p, buffSize);

                sgInfo->cmd->userSize = buffSize;
                memcpy(&sgInfo->cmd->user.b[0], (char *) p, buffSize);

                //Log_DBG("DBG.  Result: %s [%s]", strValue, Log_buf2str(buffValue, buffSize));
                lua_settop(Script, top); // вернем вершину стека на место
                if (flag != NULL) flag[0] = 1;

                resultCallback = CALLBACK_RESULT_ERROR;

                if (strcmp(strValue, "OK") == 0)
                    resultCallback = CALLBACK_RESULT_OK;

            } else
                if (typeCallBack == CALLBACK_TYPE_EVENT_ERR) {
                lua_call(Script, 5, 2); // Ожидаем 2 параметра: (результат выполения, буфер запроса (для протокольного уровня))
                // чтение резельтата:
                scrGetStrArg(Script, top + 1, strValue, sizeof (strValue));
                //p = (char*) lua_tolstring(Script, top + 1, &t);
                //strcpy(strValue, (char *) p);

                //Log_DBG("DBG.  Result: %s ", strValue);
                lua_settop(Script, top); // вернем вершину стека на место

                resultCallback = CALLBACK_ERROR_IGNORE;
                if (strcmp(strValue, "IGNORE") == 0)
                    resultCallback = CALLBACK_ERROR_IGNORE;
                if (strcmp(strValue, "ABORT") == 0)
                    resultCallback = CALLBACK_ERROR_ABORT;
                if (strcmp(strValue, "RETRY") == 0)
                    resultCallback = CALLBACK_ERROR_RETRY;

            } else
                if (typeCallBack == CALLBACK_TYPE_EVENT_ABORT) {
                lua_call(Script, 5, 2); // Ожидаем 2 параметра: (результат выполения, буфер запроса (для протокольного уровня))
                // чтение резельтата:
                //p = (char*) lua_tolstring(Script, top + 1, &t);
                //strcpy(strValue, (char *) p);
                scrGetStrArg(Script, top + 1, strValue, sizeof (strValue));

                //Log_DBG("DBG.  Result: %s ", strValue);
                lua_settop(Script, top); // вернем вершину стека на место
                resultCallback = 0; // игнорируется значение...
            } else {
                Log_DBG("ERR! Script:CORE> typeCallBack is undef (%d)\n ", typeCallBack);
            }

            if (SystemState.fDebugCore)
                Log_DBG("DBG Script:CORE> Signal(...)...end\n ");
            core_synchronize(CORE_SYNC_UNLOCK, "Signal");

        }
    }

    return resultCallback;


#else
    Log_DBG("DBG. Script unused...");
    return 0;

#endif

}

int Start_script_run(void) {

#ifdef USE_START_SCRIPT

    Script = luaL_newstate();

    luaL_openlibs(Script);

    // определим функцию записи сигналов из скрипта
    lua_register(Script, "set", script_set_signal);
    lua_register(Script, "cmd", script_cmd_signal);
    lua_register(Script, "req", script_req_signal);
    lua_register(Script, "get", script_get_proc);
    lua_register(Script, "link", script_link_proc);
    lua_register(Script, "log", script_log);
    lua_register(Script, "hexlog", script_hexlog);
    lua_register(Script, "pause", script_pause);
    lua_register(Script, "setLastError", script_setLastError);
    lua_register(Script, "addObject", script_addObject_proc);
    lua_register(Script, "mcLinkOpen", script_mcLinkOpen_proc);
    lua_register(Script, "mcLinkTx", script_mcLinkTx_proc);
    lua_register(Script, "mcLinkRx", script_mcLinkRx_proc);
    lua_register(Script, "mcFunc", script_core_proc);
    lua_register(Script, "mcGetParam", script_mcGetParam_proc);
    lua_register(Script, "mcSetParam", script_mcSetParam_proc);
    lua_register(Script, "mcSignalsSelect", script_mcSignalsSelect_proc);
    lua_register(Script, "mcConfig", script_mcConfig_proc);
    lua_register(Script, "mcArgTest", script_mcArgTest_proc);
    //lua_register(Script, "mcExit", script_mcExit_proc);

    script_synchronize(SCRIPT_SYNC_INIT, "INIT");
    core_synchronize(CORE_SYNC_INIT, "INIT");


    if (luaL_dofile(Script, "config.lua") == 0) {
        lua_getglobal(Script, "Init");
        lua_call(Script, 0, 0);
        //lua_close(L);
    } else {
        Log_DBG("ERR!  Error in file 'config.lua'!");
    }
    //getchar();
    return 0;


#else
    Log_DBG("DBG. Script unused...");

#endif

}

