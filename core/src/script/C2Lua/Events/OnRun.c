//
#include "../../scriptAPI.h"
//
//
int scrOnRun(void) {
    int result = 0;
    //core_synchronize(CORE_SYNC_LOCK, "Run");
        scrDebugLog("OnRun", "DBG Script:CORE> script_OnRun()");
        lua_getglobal(Script, "Run");
        if (lua_pcall(Script, 0, 0, 0) != LUA_OK) {
            Log_DBG("ERR!  Error in Run() :[%s]", lua_tostring(Script, -1));
            result = -1;
        }
        lua_settop(Script, 0); // вернем вершину стека на место
    //core_synchronize(CORE_SYNC_UNLOCK, "Run");
        

    return result;
}


