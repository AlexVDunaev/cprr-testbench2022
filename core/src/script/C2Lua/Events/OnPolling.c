//
#include "../../scriptAPI.h"
//
//
int script_polling_callback(char * idEvent, char * Name, int func) {

    static char EventText[10] = "POLL"; // происходит непонятное падение в функции script_polling_callback, решил сократить аргумент idEvent
    int result;
    
   scrDebugLog("OnPolling", "DBG Script:CORE> polling(%s) coreLockCount = %d\n ", EventText, coreLockCount);

    core_synchronize(CORE_SYNC_LOCK, "polling");
    //!!!! Внимание! Тест!

    //Было !!! 
    int top = lua_gettop(Script);
    if (top != 0)
        Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG Script:CORE> polling> lua_gettop = %d", top);

    lua_rawgeti(Script, LUA_REGISTRYINDEX, func);

    // не работает lua_pushcfunction(Script, (lua_CFunction) func); // Аргумент 1.
    lua_pushstring(Script, EventText); // Аргумент 1.
    lua_pushstring(Script, Name); // Аргумент 2. Имя Объекта
    lua_call(Script, 2, 1); // вызовем связанную Lua - функцию
    result = lua_tonumber(Script, top + 1);
    lua_settop(Script, top); // вернем вершину стека на место
    //Log_DBG("DBG. Script: polling(%s) -> %d",idEvent, result);
    core_synchronize(CORE_SYNC_UNLOCK, "polling");

    scrDebugLog("OnPolling", "DBG Script:CORE> polling(%s)...end\n ", EventText);

    return result;
}

