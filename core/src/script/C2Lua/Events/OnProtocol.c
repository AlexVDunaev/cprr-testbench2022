//
#include "../../scriptAPI.h"
//
int script_protocol_callback(char * idEvent, int LineIndex, int func, void * UI, char * signalName, Tsub_param_rec * value, char * pktInBuffer, int pktInSize, char * pktOutBuffer, int * pktOutSize, char * ErrorText) {
    char result[255]; // Идентификатор события с текстовом виде
    int resultSize = 0; // резмер возвращенного буффера
    int resultValue = PROT_MNG_RESULT_ERR; // PROT_MNG_RESULT_ERR
    double f64value;
    char * p;
    size_t t;
    // ------------------------------------------------------------------------
    // function MyManagerProc(idEvent, signalName, signalValue, packet)
    // -- idEvent: "REQ", "SIGNAL_KU", "SIGNAL_REQ", "RX", "ERROR", "ABORT"
    // -- "REQ": событие запроса. Функция сама решает какой запрос формировать
    // -- "SIGNAL_KU": очередь на запись на данном интерфейсе не пустасобытие запроса. Функция сама решает какой запрос формировать
    // -- Возврат: ["OK" | "ERROR" | "REPEAT" | "RETRY" | "ABORT"], ПАКЕТ_НА_ЗАПИСЬ
    // ------------------------------------------------------------------------

    scrDebugLog("OnProtocol", "DBG Script:CORE> protocol(%s)\n ", idEvent);
    
    core_synchronize(CORE_SYNC_LOCK, "protocol");
    int top = lua_gettop(Script);
    if (top != 0)
        Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG Script:CORE> protocol(%s)> lua_gettop = %d", idEvent, top);


    lua_rawgeti(Script, LUA_REGISTRYINDEX, func);
    lua_pushstring(Script, idEvent); // Аргумент 1.
    lua_pushstring(Script, signalName); // Аргумент 2. (имя сигнала)
    if (value != NULL) {
        if (value->tp == TYPE_ID_STRUCT)
            lua_pushlstring(Script, value->stvalue, value->size); // передаем массив
        else
            if (value->tp == TYPE_ID_STRING0)
            lua_pushstring(Script, value->stvalue); // передаем строку
        else {
            lua_pushnumber(Script, 0); // Ошибка!
        }
    } else
        lua_pushstring(Script, ""); // Аргумент 3. (значение сигнала)
    if (pktInSize)
        lua_pushlstring(Script, pktInBuffer, pktInSize); // Аргумент 4 Буфер пакета
    else
        lua_pushstring(Script, ""); // Аргумент 4. пустой буфер

    if (LineIndex < SystemState.LLC.count)
        lua_pushnumber(Script, SystemState.LLC.Lines[LineIndex]->id); // Идентификатор линии
    else
        lua_pushnumber(Script, -1); // Идентификатор линии? Ошибка!

    script_ErrorText[0] = 0; // очистим признак ошибки
    lua_call(Script, 5, 2); // вызовем связанную Lua - функцию
    if (script_ErrorText[0]) // копирование строки ошибки (если есть)
        strcpy(ErrorText, script_ErrorText);
    //t = sizeof (result);
    //p = (char*) lua_tolstring(Script, top + 1, &t);
    //strcpy(result, (char *) p);
    scrGetStrArg("protocol", Script, top + 1, result, sizeof (result));
    //Log_DBG("DBG. Script: protocol_callback> : %s ", result);

    p = (char*) lua_tolstring(Script, top + 2, &t);
    resultSize = t;
    if (pktOutBuffer != NULL) { // переместим полученный от скрипта буфер по указанному адресу
        memcpy(pktOutBuffer, (char *) p, resultSize);
        *pktOutSize = resultSize;
    } else
        Log_DBG("DBG. Script: protocol_callback(pktOutBuffer == NULL)!");

    lua_settop(Script, top); // вернем вершину стека на место
        //Log_DBG("DBG. Script: protocol_callback(%s) : %s buff=%s",idEvent, result, Log_buf2str(pktOutBuffer, resultSize));
    if (strcmp(result, "OK") == 0) // 
    {
        resultValue = PROT_MNG_RESULT_OK;
    } else {
        Log_DBG("ERR!. Script: protocol_callback(%s) : %s", idEvent, result);
        if (strcmp(result, "ERROR") == 0)
            resultValue = PROT_MNG_RESULT_ERR;

    }
    core_synchronize(CORE_SYNC_UNLOCK, "protocol");
    
    scrDebugLog("OnProtocol", "DBG Script:CORE> protocol(%s):%d...end\n ", idEvent, resultValue);

    
    return resultValue; //

}


//