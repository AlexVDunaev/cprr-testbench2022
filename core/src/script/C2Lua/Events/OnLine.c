//
#include "../../scriptAPI.h"
//
//

int script_line_callback(char * idEvent, char * Name, int func, TMACDriverArg arg, char * ErrorText) {
    char result[8255]; // Идентификатор события с текстовом виде
    int resultSize = 0; // резмер возвращенного буффера
    char Buffer[8255]; // Идентификатор события с текстовом виде
    int BufferSize = 0; // резмер возвращенного буффера
    //    char * pResult;
    int top;
    char * pBuffer;
    int resultCallback = 0; // резмер возвращенного буффера
    size_t t;
    //  function MyLineProcTest(idEvent, buffer)

    //Log_DBG("core_synchronize(CORE_SYNC_LOCK)");
#warning "synchronize line here!"
    core_synchronize(CORE_SYNC_LOCK, "line");

    //Log_DBG(" ---->>>>>>");
    
    scrDebugLog("OnLine", "DBG Script:CORE> line(%s:%s)", Name, idEvent);
    
    top = lua_gettop(Script);
    
    if (top != 0) {
        //Log_Ex("luaerr", LEVEL_ERR, (char *) "DBG Script:CORE> line(%s)> lua_gettop = %d", idEvent, top);
        Log_DBG("DBG Script:CORE> line(%s:%s)> lua_gettop = %d", Name, idEvent, top);
		for (int i = 0; i < top; i++) {
			size_t len;
			char* p;
			if (lua_isstring(Script, i)) {
				p = (char*) lua_tolstring(Script, i, &len);
				bufToHexStr(p, (int)len, Buffer);
				Log_DBG("DBG Script:CORE> ARG[%d] String hex: [%s]", i, Buffer); 
			}
			if (lua_isnumber(Script, i)) {
				double f = lua_tonumber(Script, i);
				Log_DBG("DBG Script:CORE> ARG[%d] Float: %f", i, f); 
			}
			if (lua_isinteger(Script, i)) {
				long long f = lua_tointeger(Script, i);
				Log_DBG("DBG Script:CORE> ARG[%d] long: %ll", i, f); 
			}
		}
		Log_DBG("DBG Script:CORE> -----------------------------"); 
    }

    lua_rawgeti(Script, LUA_REGISTRYINDEX, func);
    // Аргумент 1 : Тип события [RX, TX, ERR...].
    lua_pushstring(Script, idEvent); 
    // Аргумент 2 : Буфер и размер.
    lua_pushlstring(Script, arg.prTX.inBuff, arg.prTX.inCount); 
    // Аргумент 3 Идентификатор линии. (актуально, если используется один обработчик на несколько линий)
    lua_pushstring(Script, Name); 
    // Указатель на начало буфера
	if (arg.prTX.pLightuserdata == NULL)
		lua_pushlightuserdata(Script, &arg.prTX.inBuff[0]);
	else
		lua_pushlightuserdata(Script, arg.prTX.pLightuserdata);
		
    // полный размер буфера (заголовок и тело данных, при этом данные 68 чирпов!)
    lua_pushinteger(Script, arg.prTX.inCountEx);

    // Log_DBG("lua_call>> [top:%d, %s, [%p, %d] [%p, %d]", top, idEvent, arg.prTX.inBuff, arg.prTX.inCount, &arg.prTX.inBuff[0], arg.prTX.inCountEx);
    
    // lua_call(Script, 5, 2); // вызовем связанную Lua - функцию

    if (lua_pcall(Script, 5, 2, 0) != LUA_OK) {
        Log_DBG("ERR!  Error in OnLine(%s, %s) :[%s]", idEvent, Name, lua_tostring(Script, -1));
    } else {
    
    // Log_DBG("lua_call>> DONE.");
    
    scrGetStrArg("line", Script, -2, result, sizeof (result)); //top + 1
    //    t = sizeof (result);
    //    pResult = (char*) lua_tolstring(Script, top + 1, &t);
    //    resultSize = t;
    pBuffer = (char*) lua_tolstring(Script, -1, &t); //top + 2
    BufferSize = t;
    scrDebugLog("OnLine", "DBG Script:CORE> line Lua(%s): Result size = %d, Buffer size = %d\n ", idEvent, resultSize, BufferSize);
    if (top != 0) {
        Log_DBG("DBG. Script: Line[%s] CallBack(%s, [%d]) -> ['%s' len(buf):%d]", Name, idEvent, arg.prTX.inCount, result, (int)t);
	}
	
    if (strcmp(result, "OK") == 0) { // Если буфер в ответе не пустой, значит уже получен ответ!
        if ((arg.prTX.outBuff != NULL)&&(BufferSize)) { // переместим полученный от скрипта буфер по указанному адресу
            if (BufferSize <= arg.prTX.outBuffLimit) {
                memcpy(arg.prTX.outBuff, pBuffer, BufferSize);
            } else {
                Log_DBG("[OnLine] Result buffer size error!!! BufferSize : %d, BufferLimit : %d", BufferSize, arg.prTX.outBuffLimit);
            }
            if (arg.prTX.outBuffCount) {
             *arg.prTX.outBuffCount = BufferSize; 
            } else {
                Log_DBG("[OnLine] arg.prTX.outBuffCount == NULL !!!");
             }
        }
        resultCallback = MAC_RESULT_OK; //OK
    } else
        if (strcmp(result, "IGNORE") == 0) { //
        *arg.prTX.outBuffCount = 0;
        resultCallback = MAC_RESULT_IGNORE; 
    } else
        if (strcmp(result, "BREAK") == 0) { //
        *arg.prTX.outBuffCount = 0;
        resultCallback = MAC_RESULT_BREAK; 
    } else
        if (strcmp(result, "RETRY") == 0) { //
        *arg.prTX.outBuffCount = 0;
        resultCallback = MAC_RESULT_RETRY; 
    } else {
        Log_DBG("ERR. Script: Line[%s] CallBack(%s, [%d]) -> '%s'", Name, idEvent, arg.prTX.inCount, result);
        resultCallback = MAC_RESULT_ERROR; //ERR
    }
    }
    lua_settop(Script, top); // вернем вершину стека на место

    scrDebugLog("OnLine", "DBG Script:CORE> line(%s:%s)...end\n ", Name, idEvent);
    
    core_synchronize(CORE_SYNC_UNLOCK, "line");
    //Log_DBG("core_synchronize(CORE_SYNC_UNLOCK)");

    return resultCallback;
}

