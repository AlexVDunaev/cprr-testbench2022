//
#include "../../scriptAPI.h"
//
int script_framing_callback(char * idEvent, int func, char * buffInput, int * buffInputSize, char * buffOutput, int * buffOutputSize, char * ErrorText) {
    char result[255]; // Идентификатор события с текстовом виде
    int resultSize = 0; // резмер возвращенного буффера
    int resultFrame = 0; // резмер возвращенного буффера
    char * p;
    size_t t;
    //  function MyLineProcTest(idEvent, buffer)
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script:CORE> framing(%s)\n ", idEvent);

    core_synchronize(CORE_SYNC_LOCK, "framing");

    lua_rawgeti(Script, LUA_REGISTRYINDEX, func);
    lua_pushstring(Script, idEvent); // Аргумент 1.
    lua_pushlstring(Script, buffInput, *buffInputSize); // Аргумент 2 Буфер на входе.
    script_ErrorText[0] = 0; // очистим признак ошибки
    if (strcmp(idEvent, "RX") == 0) // для события "RX" - возврат 2-х аргументов
        lua_call(Script, 2, 2); // вызовем связанную Lua - функцию
    else
        lua_call(Script, 2, 1); // вызовем связанную Lua - функцию
    if (script_ErrorText[0]) // копирование строки ошибки (если есть)
        strcpy(ErrorText, script_ErrorText);
    t = sizeof (result);
    p = (char*) lua_tolstring(Script, 1, &t);
    resultSize = t;
    resultFrame = t;
    if (buffOutput != 0) { // переместим полученный от скрипта буфер по указанному адресу
        memcpy(buffOutput, (char *) p, resultSize);
        *buffOutputSize = resultSize;
    }
    if (strcmp(idEvent, "RX") == 0) // для события "RX" - возврат 2-х аргументов
    { // присвоим новое значение входного буфера
        // !!! нужно помнить, что при работе функции Lua запись в rx-буфер со стороны линии связи не должна производиться
        p = (char*) lua_tolstring(Script, 2, &t);
        resultSize = t;
        memcpy(buffInput, (char *) p, resultSize);
        *buffInputSize = resultSize;
    }

    lua_settop(Script, 0); // вернем вершину стека на место
    //Log_DBG("DBG. Script: line(%s, buff[%d]) -> buff[%d]",idEvent, *buffInputSize, resultSize);
    core_synchronize(CORE_SYNC_UNLOCK, "framing");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script:CORE> framing(%s):%d...end\n ", idEvent, resultFrame);

    return resultFrame;
}
//