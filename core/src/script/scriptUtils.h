/* 
 * File:   scriptUtils.h
 * Author: Dunaev
 *
 * Created on 15 Декабрь 2016 г., 13:45
 */

#ifndef SCRIPTUTILS_H
#define	SCRIPTUTILS_H

#include "scriptAPI.h"




typedef struct {
    char typeId; // кодировка аргументов как в string.pack
    char valid; // Признак корректности данных внутри value
    Tsub_param_rec value; // 
} TUniArgument;

int scrGetUniArgFromStack(lua_State *L, char * dbgTitle, int stackBase, TUniArgument * uni, int targetType);
int scrGetStrArg(char *title, lua_State *L, int NumArg, char * buffer4Value, int bufferSize);
int scrGetArgsFromStack(lua_State *L, char * dbgTitle, int stackBase, const char * argsFrmt, ...);

int scrDebugLog(char * funcKey, const char * text, ...);
void scrInitDebugLog(int argc, char **argv);
void scrAppendDebugFunc(char * FuncName, char defaultLogging);

#define SCRIPT_SYNC_LOCK        1
#define SCRIPT_SYNC_UNLOCK      0
#define SCRIPT_SYNC_INIT        -1
#define SCRIPT_SYNC_FREE        -2

#define CORE_SYNC_LOCK        MCORE_SYNC_LOCK
#define CORE_SYNC_UNLOCK      MCORE_SYNC_UNLOCK
#define CORE_SYNC_INIT        MCORE_SYNC_INIT
#define CORE_SYNC_FREE        MCORE_SYNC_FREE

void script_synchronize(int lock, char * dbgText);
void core_synchronize(int lock, char * dbgText);
int core_synchronize_getCount(char * dbgText);

#endif	/* SCRIPTUTILS_H */

