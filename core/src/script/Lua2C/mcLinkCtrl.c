//
#include "../scriptAPI.h"
//
// Команда. PUA[2].ENABLED = 0
// Пример вызова из Lua:   cmd("PUA[2].ENABLED", 0)

int script_mcLinkCtrl_proc(lua_State *L) {
    int i;
    char CtrlText[255] = "---";
    char CtrlDevice[255] = "---";
    char CtrlParam[255] = "---";
    int Result = 0;
    int LineIndex;
    int LineFind = 0;

    TLineRec * a;
    
    scrDebugLog("mcLinkCtrl", "DBG Script>mcLinkCtrl(...)");

    script_synchronize(SCRIPT_SYNC_LOCK, "mcLinkCtrl()");

    i = lua_gettop(L);
    if ((i == 2)||(i == 4)) { // два/4 аргумента

        if (i == 2) {
        scrGetArgsFromStack(L, "mcLinkCtrl()", 1, "nz", &LineIndex, &CtrlText, sizeof (CtrlText));
        }
        else
        scrGetArgsFromStack(L, "mcLinkCtrl()", 1, "nzzz", &LineIndex, &CtrlText, sizeof (CtrlText), &CtrlDevice, sizeof (CtrlDevice),  &CtrlParam, sizeof (CtrlParam));
            
        scrDebugLog("mcLinkCtrl", "DBG Script>mcLinkCtrl(%d, %s)", LineIndex, CtrlText);
        lua_settop(L, 0);
        for (i = 0; i < SystemState.LLC.count; i++)
          {
            if (SystemState.LLC.Lines[i]->id == LineIndex)
            {
//                 a = SystemState.LLC.Lines[i];
                LineFind = 1;
                TMACDriverArg arg = {.prCTRL.ctrlCommand = &CtrlText[0], .prCTRL.ctrlValue = strlen(CtrlText), .prCTRL.device = &CtrlDevice[0], .prCTRL.param = &CtrlParam[0]};
                Result = RunMacDriverCallBackEx(MAC_ID_CTRL, i, arg, NULL);
                lua_pushnumber(L, Result); //
                Result = 1;
                break;
            }
        }

        if (LineFind == 0)
        {
            Log_Ex("luaerr", LEVEL_ERR, (char *) "LineID not found! (func: 'mcLinkCtrl(%d, '%s')",LineIndex, CtrlText );
        }
                
    }
    else
    {
    Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcLinkCtrl(idLine, StrParam)')");
    lua_settop(L, 0);
    }
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcLinkCtrl()");

    scrDebugLog("mcLinkCtrl", "DBG Script>cmd(...)...mcLinkCtrl\n ");

    return Result; /* Zero return value */
}
