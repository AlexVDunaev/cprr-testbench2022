//
#include "../scriptAPI.h"
//
#include <unistd.h>

#include "../../Algorithm/algorithmAdapter.h"

typedef enum {
    ALGAPI_CPRR_ID  = 0, // Идентификатор платф.завис. функций для CPRR
    ALGAPI_CPYC_ID  = 1,  // Идентификатор функций для Corner77 (CPYC)
	ALGAPI_UNDEF_ID		
} TAlgAPI_ID;

// Загрузка данных кадра из файла.
// AlgGetFrame(fileName, frameIndex)

int script_alg_getFrame(lua_State *L) {
    int i;
    int frameIndex = 0;
    void * a = NULL;
    TDataFrameInfo rosbagInfoDefault;
    TDataFrameInfo * rosbagInfo = &rosbagInfoDefault;
    script_synchronize(SCRIPT_SYNC_LOCK, "AlgGetFrame()");

    memset(&rosbagInfoDefault, 0, sizeof (rosbagInfoDefault));

    i = lua_gettop(L);
    if (i >= 2) { // == 2 аргумента (Имя файла, индекс кадра)
        char fileName[255] = "";
		int frameBufferSize = WORK_FRAME_BUFFER_SIZE;
		int algApiId = ALGAPI_CPRR_ID;
		int dataOffset = -1; // default
		
        scrGetStrArg("alg.getFrame", L, 1, fileName, sizeof (fileName));
        frameIndex = lua_tonumber(L, 2);
		if (i >= 3) {
			int v = lua_tonumber(L, 3);
			if ((v >= 0)&&(v < ALGAPI_UNDEF_ID)) algApiId = v;
		}
		if (i >= 4) {
			int v = lua_tonumber(L, 4);
			if ((v > 0)&&(v <= WORK_FRAME_BUFFER_SIZE*2)) {
				frameBufferSize = v;
                //Log_DBG("[alg.getFrame]> buffer size : %d \n", frameBufferSize);
			}
		}
		if (i >= 5) {
			dataOffset = lua_tonumber(L, 5);
		}
			
        // Log_DBG("[Script]> alg.getFrame(%s, %d)", fileName, frameIndex);
        lua_settop(L, 0);
        // Внимание! функция lua_newuserdata помещает на стек userdata!!!
        a = lua_newuserdata(L, frameBufferSize);
        if (a) {
            //Log_DBG("[Script]> alg.getFrame(Buffer: %p)", a);
			int getFrameResult = -1;
			
			if (algApiId == ALGAPI_CPRR_ID)
				getFrameResult = cprrAlgorithmGetFrame(fileName, frameIndex, a, frameBufferSize, dataOffset, &rosbagInfo);
			if (algApiId == ALGAPI_CPYC_ID)
				getFrameResult = cpycAlgorithmGetFrame(fileName, frameIndex, a, frameBufferSize, &rosbagInfo);
			
            if (getFrameResult <= 0) {
                // Кадр считать не удалось.
                lua_settop(L, 0); // Утечки памяти нет.
                lua_pushnil(L);
                lua_pushnil(L); // Замещаю результат nil (userdata - в мусор)
                Log_DBG("[alg.getFrame]> read file (%s) error!!! \n", fileName);
            } else {
                lua_newtable(L);
                lua_pushstring(L, "SN"); //
                lua_pushinteger(L, rosbagInfo->serialNumb);
                lua_settable(L, -3);
                lua_pushstring(L, "time");
                lua_pushnumber(L, (double) rosbagInfo->time);
                lua_settable(L, -3);
                lua_pushstring(L, "points"); //
                lua_pushinteger(L, rosbagInfo->points);
                lua_settable(L, -3);
                lua_pushstring(L, "channels"); //
                lua_pushinteger(L, rosbagInfo->channels);
                lua_settable(L, -3);
                lua_pushstring(L, "chirps"); //
                lua_pushinteger(L, rosbagInfo->chirps);
                lua_settable(L, -3);
                lua_pushstring(L, "temperature"); //
                lua_pushinteger(L, rosbagInfo->temperature[0]);
                lua_settable(L, -3);
                lua_pushstring(L, "startFreq");
                lua_pushnumber(L, (double) rosbagInfo->startFreq);
                lua_settable(L, -3);
                lua_pushstring(L, "idleTime");
                lua_pushnumber(L, (double) rosbagInfo->idleTime);
                lua_settable(L, -3);
                lua_pushstring(L, "adcStartTime");
                lua_pushnumber(L, (double) rosbagInfo->adcStartTime);
                lua_settable(L, -3);
                lua_pushstring(L, "rampTime");
                lua_pushnumber(L, (double) rosbagInfo->rampTime);
                lua_settable(L, -3);
                lua_pushstring(L, "freqSlope");
                lua_pushnumber(L, (double) rosbagInfo->freqSlope);
                lua_settable(L, -3);
                lua_pushstring(L, "adcRate");
                lua_pushinteger(L, rosbagInfo->adcRate);
                lua_settable(L, -3);
                lua_pushstring(L, "txEnable");
                lua_pushinteger(L, rosbagInfo->txEnable);
                lua_settable(L, -3);
            }
        } else {
            Log_DBG("[Script]> userdata ERROR!!! \n");
        }
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'getFrame')");
        lua_settop(L, 0);
        lua_pushnil(L);
        lua_pushnil(L);
    }
    script_synchronize(SCRIPT_SYNC_UNLOCK, "AlgGetFrame()");
    return 2; /* count param return value */
}


void ScramblerTest(void);

int script_alg_test(lua_State *L) {
    script_synchronize(SCRIPT_SYNC_LOCK, "AlgTest()");
    //Log_DBG("[Script]> ScramblerTest run... \n");
    //ScramblerTest();
    lua_settop(L, 0);
    lua_newtable(L);
    lua_pushstring(L, "TEST");
    lua_pushnumber(L, (double) 1.1);
    lua_settable(L, -3);
    lua_pushstring(L, "TEST2");
    lua_pushnumber(L, (double) 2.2);
    lua_settable(L, -3);
    lua_pushstring(L, "TEST4");
    lua_pushnumber(L, (double) 3.3);
    lua_settable(L, -3);
    lua_pushnumber(L, (double) 88876);
    script_synchronize(SCRIPT_SYNC_UNLOCK, "AlgTest()");

    return 2;
}

//
// Расчет кадра (userData, dTime, [mode]);

int script_alg_calcFrame(lua_State *L) {
    static char tgList[1024 * 100]; // Буфер для списка целей
    int i;

    script_synchronize(SCRIPT_SYNC_LOCK, "AlgCalcFrame()");
    i = lua_gettop(L);
    if (i >= 2) { // 
        void * a = lua_touserdata(L, 1);
        float dTime = lua_tonumber(L, 2);
        char rawMode = 0; //rawMode : 1 - Это режим откл. трекинга
        if (i >= 3) rawMode = (lua_tonumber(L, 3) > 0) ? 1 : 0;
        lua_settop(L, 0);
        if ((a)&&(dTime >= 0)) {
            TTrackingData* r = cprrAlgorithmCalcFrame(a, rawMode, dTime);
            int tgListSize = cprrAlgorithmPackTargets(r, tgList, sizeof (tgList));
            //lua_pushinteger(L, r->TargetCount);
            lua_pushlstring(L, tgList, tgListSize);
        } else {
            Log_DBG("CalcFrame() argument error! (frame:%p, dTime:%f)\n", a, dTime);
            lua_pushnil(L);
        }
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'AlgGetFrame')");
        lua_settop(L, 0);
        lua_pushnil(L);
    }
    script_synchronize(SCRIPT_SYNC_UNLOCK, "AlgCalcFrame()");

    return 1; /* count param return value */
}

int script_alg_getField(lua_State *L) {
    int i;
    int size = 4;
    script_synchronize(SCRIPT_SYNC_LOCK, "AlgGetField()");
    i = lua_gettop(L);
    if (i >= 2) { // 
        char * a = NULL;
        if (lua_islightuserdata(L, 1) || lua_isuserdata(L, 1))
            a = lua_touserdata(L, 1);

        if (a == NULL) {
            Log_Ex("luaerr", LEVEL_ERR, (char *) "1-st arg is not userdata");
            lua_settop(L, 0);
            lua_pushnil(L);
        } else {
            int index = lua_tointeger(L, 2);
            if (i >= 3) { // 
                size = lua_tointeger(L, 3);
            }
            lua_settop(L, 0);
            if ((a)&&(index >= 0)) {
                lua_newtable(L);
                lua_pushstring(L, "float");
                lua_pushnumber(L, (double) *((float*) &a[index]));
                lua_settable(L, -3);
                lua_pushstring(L, "i16");
                lua_pushinteger(L, *((int16_t*) & a[index]));
                lua_settable(L, -3);
                lua_pushstring(L, "i8");
                lua_pushinteger(L, *((int8_t*) & a[index]));
                lua_settable(L, -3);
                lua_pushstring(L, "i32");
                lua_pushinteger(L, *((int32_t*) & a[index]));
                lua_settable(L, -3);
                lua_pushstring(L, "mem");
                lua_pushlstring(L, &a[index], size);
                lua_settable(L, -3);
            } else {
                Log_DBG("getField() argument error! (userdata:%p, index:%d)\n", a, index);
                lua_pushnil(L);
            }
        }
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'AlgGetField')");
        lua_settop(L, 0);
        lua_pushnil(L);
    }
    script_synchronize(SCRIPT_SYNC_UNLOCK, "AlgGetField()");

    return 1; /* count param return value */
}

int script_alg_updateConfig(lua_State *L) {
    int i;

    i = lua_gettop(L);
    if (i == 1) { // 1 аргумент (Путь к папке конфигов алгоритма)
        if (lua_isstring(L, 1)) {
            char dirName[255] = "";
            scrGetStrArg("alg.updateConfig", L, 1, dirName, sizeof (dirName));
            cprrAlgorithmSetDefaultConfig(dirName);
        } else {
            Log_DBG("DBG Script> updateConfig(invalid arg)\n");
        }
    }
    lua_settop(L, 0);
    return 0; /* count param return value */

}

int script_alg_getConfig(lua_State *L) {
    int i;
    void * a = NULL;
    script_synchronize(SCRIPT_SYNC_LOCK, "AlgGetConfig()");
    int doneOk = 0;
    float tmCode = 0;

    i = lua_gettop(L);
    if ((i == 1)||(i == 2)) { // 1 аргумент (Путь к файлу конфига)
        if (lua_isstring(L, 1)) {
            char fileName[255] = "";
            scrGetStrArg("alg.getConfig", L, 1, fileName, sizeof (fileName));
            if (i == 2) {
                if (lua_isnumber(L, 2)) {
                    tmCode = lua_tonumber(L, 2);
                    } else {
                    Log_DBG("[Script]> getConfig ERROR format of tmCode -> 0!!!\n");
                }
            }
            lua_settop(L, 0);
            // Внимание! функция lua_newuserdata помещает на стек userdata!!!
            a = lua_newuserdata(L, sizeof (TRadarConfigHolder));
            lua_pushinteger(L, sizeof (TRadarConfigHolder));
            if (a) {
                if (cprrAlgorithmGetConfig(fileName, (TRadarConfigHolder*) a) <= 0) {
                    // файл считать не удалось.
                    Log_DBG("[alg.getConfig]> read file (%s) error!!! \n", fileName);
                } else {
                    doneOk = 1;
                    if (i == 2) {
                        // Пользователь указал темп. код. - нужно вернуть конфигурацию
                        // для этого кода
                        float * pTmVector = NULL;
                        int minIndex = cprrAlgorithmGetActualTmIndex((TRadarConfigHolder*) a, tmCode);
                        if (minIndex < 0)  {
                            Log_DBG("[Script]> getConfig ERROR tmVector!!!\n");
                        } else {
                            pTmVector = (float *) &((TRadarConfigHolder*)a)->tm.vector[minIndex].tCode;
                        }
                        
                        // передача кода температуры и вектора калибровки
                        if (pTmVector == NULL) {
                            lua_pushnil(L); // Замещаю результат nil : буфер текущей конфигурации
                        } else {
                            lua_pushlstring(L, (const char *)pTmVector, sizeof(TMCALVector));
                        }
                        
                        // Передаю пролаз
                        int16_t * pLeak = &((TRadarConfigHolder*)a)->leak.leak[0];
                        int  leakSize = ((TRadarConfigHolder*)a)->leak.size;
                        lua_pushlstring(L, (const char *)pLeak, leakSize);
                            
                    } else {
                        lua_pushnil(L); // Замещаю результат nil : буфер [tmCode][TmVector]
                        lua_pushnil(L); // Замещаю результат nil : буфер пролаза
                    }
                }
            } else {
                Log_DBG("[Script]> getConfig userdata ERROR!!! \n");
            }
        } else {
            Log_DBG("DBG Script> getConfig(invalid arg)\n");
        }
    } else {
        Log_DBG("DBG Script> getConfig(invalid kol-vo arg : %d)\n", i);
    }
    if (doneOk == 0) {
        lua_settop(L, 0); // Утечки памяти нет.
        lua_pushnil(L); // Замещаю результат nil :(userdata - конфигурация)
        lua_pushnil(L); // Замещаю результат nil : размер конфигурации
        lua_pushnil(L); // Замещаю результат nil : буфер [tmCode][TmVector]
        lua_pushnil(L); // Замещаю результат nil : буфер пролаза
    }

    script_synchronize(SCRIPT_SYNC_UNLOCK, "AlgGetConfig()");
    return 4; /* count param return value */
}

int script_alg_setConfig(lua_State *L) {
    int i;
    script_synchronize(SCRIPT_SYNC_LOCK, "AlgSetConfig()");

    i = lua_gettop(L);
    if (i >= 2) {
        // 1 аргумент - userdata (структура конфига)
        // 2 аргумент - температурный код
        void * a = lua_touserdata(L, 1);
        float tmCode = lua_tonumber(L, 2);
        tmCode = cprrAlgorithmSetConfig((TRadarConfigHolder*) a, tmCode);
        lua_settop(L, 0);
        lua_pushinteger(L, (int) tmCode);

    } else {
        Log_DBG("DBG Script> setConfig(invalid arg)\n");
        lua_settop(L, 0);
        lua_pushinteger(L, -1);
    }

    script_synchronize(SCRIPT_SYNC_UNLOCK, "AlgSetConfig()");
    return 1; /* count param return value */
}



/**
 * @brief Инициализация драйвера mmap. 
 *
 * Для расчета данных во внешней подсистеме, используется механизм 
 * обмена данными через mmap.
 * Инициализация mmap: lightuserdata = mmapInit(fileName, size)
 * @param fileName - имя mmap файла;
 * @param size - размер mmap файла (размер должен быть постоянный и совпадать 
 * с размером буфера при вызове mmapWrite();
 * @return lightuserdata - указатель на дескриптор драйвера mmap, используется при 
 * последующих вызовах mmapWrite() и mmapClose()
 */

#include "../utils/mmDriver.h"

int script_alg_mmapInit(lua_State *L) {
    int i;

    script_synchronize(SCRIPT_SYNC_LOCK, "mmapInit()");

    i = lua_gettop(L);
    if (i >= 2) { // Имя и размер файла
        char fileName[255] = "";
        scrGetStrArg("alg.mmapInit", L, 1, fileName, sizeof (fileName));
        int size = lua_tointeger(L, 2);
        lua_settop(L, 0);
        void * m = mmDriverInit(fileName, size);
        if (m == NULL)
            lua_pushnil(L);
        else
            lua_pushlightuserdata(L, m);
    } else {
        Log_DBG("DBG Script> mmapInit(invalid arg)\n");
        lua_settop(L, 0);
        lua_pushnil(L);
    }
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mmapInit()");

    return 1; /* count param return value */

}

int script_alg_mmapClose(lua_State *L) {
    script_synchronize(SCRIPT_SYNC_LOCK, "mmapClose()");
    int i;
    i = lua_gettop(L);
    if (i >= 1) { // дескриптор драйвера mmap
        if (lua_islightuserdata(L, 1)) {
            void * m = lua_touserdata(L, 1);
            mmDriverClose(m);
        }
    }
    lua_settop(L, 0);
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mmapClose()");
    return 0; /* count param return value */
}

int script_alg_mmapWrite(lua_State *L) {
    script_synchronize(SCRIPT_SYNC_LOCK, "mmapWrite()");
    int i;
    int result = 0;
    i = lua_gettop(L);
    if (i >= 2) { // дескриптор драйвера mmap, указатель на буфер
        if (lua_islightuserdata(L, 1))
            if (lua_islightuserdata(L, 2) || lua_isstring(L, 2)) {
                void * m = lua_touserdata(L, 1);
                void * buffer = NULL;
                if (lua_islightuserdata(L, 2)) {
                    buffer = (void *) lua_touserdata(L, 2);
                } else {
                    size_t len = 0;
                    buffer = (void *)lua_tolstring(L, 2, &len);
                }
                result = mmDriverWrite(m, buffer);
            }
    }
    lua_settop(L, 0);
    lua_pushinteger(L, result);

    script_synchronize(SCRIPT_SYNC_UNLOCK, "mmapWrite()");
    return 1; /* count param return value */
}



// TODO: Связать мой тип userdata с метатаблицей, что бы идентифицировать мой тип userdata
// https://gist.github.com/Youka/2a6e69584672f7cb0331
// 

/*

 формат конфига:
    int TMCAL_VECTOR_MAX_NUM;
    int TMCAL_POLY_MAX_POWER;
    TMCALCalibrationData cData;
    float Calibration_Rx[RX_CHANNELS_NUM*2];
    int16_t configLeakage[RX_CHANNELS_NUM][CHIRP_SAMPLE_FROM_RADAR];
    uint16_t checkCodeSum;
 
 
 */



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

static const struct luaL_Reg alglib [] = {
    {"test", script_alg_test},
    {"getFrame", script_alg_getFrame},
    {"calcFrame", script_alg_calcFrame},
    {"updateConfig", script_alg_updateConfig},
    {"getConfig", script_alg_getConfig},
    {"setConfig", script_alg_setConfig},
    {"getField", script_alg_getField},
    {"mmapInit", script_alg_mmapInit},
    {"mmapClose", script_alg_mmapClose},
    {"mmapWrite", script_alg_mmapWrite},

    {NULL, NULL}
};

int script_alg_Init(lua_State *L) {
    int i;

    i = lua_gettop(L);
    if (i >= 1) { // 1 аргумент (Путь к папке конфигов алгоритма)
        char dirName[255] = "";
        scrGetStrArg("alg.getFrame", L, 1, dirName, sizeof (dirName));
        cprrAlgorithmInit(dirName);
    }
    lua_settop(L, 0);
    luaL_newlib(L, alglib);
    return 1; /* count param return value */

}