//
#include "../scriptAPI.h"
#include "../../utils/crc16utils.h"

//
// "CRC16.L.0x8005.0x0000" 
//
#define MC_CALC_CRC_BUFFER_SIZE 4096

int script_mcCalcCRC(lua_State *L) {
    int i;
    char option[255];
    int bufferSize;
    char buffer[MC_CALC_CRC_BUFFER_SIZE];
    int crcResult = 0xFFFF;

    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>mcCalcCRC(...)\n ");

    script_synchronize(SCRIPT_SYNC_LOCK, "mcCalcCRC()");

    i = lua_gettop(L);
    if (i == 2) { // Формат вызова mcCaclCRC("CRC16.L.0x8005.0x0000" , "BUFFER")
        scrGetArgsFromStack(L, "mcCalcCRC()", -1, "zc", option, sizeof (option), &buffer, MC_CALC_CRC_BUFFER_SIZE, &bufferSize);

        if (SystemState.fDebugCore)
            Log_DBG("DBG.  Script: mcCalcCRC(%s, [...]:%d)", option, bufferSize);
        if (bufferSize >= (MC_CALC_CRC_BUFFER_SIZE)) { // Ошибка. Слишком большой буфер!
            Log_DBG("ERR!.  Script: mcCalcCRC() : buffer overflow");
        } else {
            crcResult = crc16u_calc(&buffer[0], bufferSize, option);
        }
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcCalcCRC')");
    }
    lua_settop(L, 0);
    lua_pushinteger(L, crcResult);

    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcCalcCRC()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>mcCalcCRC(...)...end\n ");

    return 1; /* count param return value */
}
