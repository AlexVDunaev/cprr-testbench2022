//
#include "../scriptAPI.h"
//
// Вывод в лог
// Пример вызова из Lua: hexlog("текст>", buffer)

int script_hexlog(lua_State *L) {
    int i;
    char text[255];
    char logfile[20] = "dbg"; // ограничение по длине в модуле log
    char * p;
    char buffer[255];
    char hexStr[255 * 3];
    char ResultStr[255 * 4];
    int bufferSize;
    char result = 0;


    script_synchronize(SCRIPT_SYNC_LOCK, "hexlog()");

    scrDebugLog("hexlog", "DBG Script> hexlog(%s)", "...");

    i = lua_gettop(L);

    if ((i == 2) || (i == 3)) { // == 2 аргументов
        if (i == 2)
            scrGetArgsFromStack(L, "hexlog()", 1, "zc", text, sizeof (text),
                buffer, sizeof (buffer), &bufferSize);
        else
            scrGetArgsFromStack(L, "hexlog()", 1, "zcz", text, sizeof (text),
                buffer, sizeof (buffer), &bufferSize,
                &logfile, sizeof (logfile));

        lua_settop(L, 0);

        if (bufferSize >= sizeof (buffer)) {
            Log_Ex("luaerr", LEVEL_ERR, (char *) "hexlog: buffer size too much (%d)", bufferSize);
            bufferSize = sizeof (buffer) - 1;
        }
        bufToHexStr(buffer, bufferSize, hexStr);
        snprintf(ResultStr, sizeof (ResultStr), "%s %s", text, hexStr);
        lua_pushstring(L, ResultStr); //
        result = 1;
        Log_Ex(logfile, LEVEL_ERR, "%s", ResultStr);
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'log')");
        lua_settop(L, 0);
    }

    scrDebugLog("hexlog", "DBG Script> hexlog(...)...end\n ");
    script_synchronize(SCRIPT_SYNC_UNLOCK, "hexlog()");
    return result;
}
