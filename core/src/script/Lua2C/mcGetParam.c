//
#include "../scriptAPI.h"
#include "../../core/other.h"

// доступ к параметрам сигнала

int script_mcGetParam_proc(lua_State *L) {
    int i;
    char signalName[255] = "args";
    char paramName[255];
    char paramDefaultValue[255] = "";
    char * param = NULL;
    char * p;
    size_t t;
    char result = 0;


    script_synchronize(SCRIPT_SYNC_LOCK, "mcGetParam()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcGetParam(...)\n ");
    i = lua_gettop(L);

    if (i >= 2) {
        // первый аргумент всегда "args"
        // scrGetStrArg("mcGetParam", L, 1, signalName, sizeof (signalName));
        scrGetStrArg("mcGetParam", L, 2, paramName, sizeof (paramName));
        if (i >= 3) {
            scrGetStrArg("mcGetParam", L, 3, paramDefaultValue, sizeof (paramDefaultValue));
        }
        lua_settop(L, 0);
        if (strcmp(signalName, "args") == 0) { // доступ к параметрам командной строки...
            for (i = 0; i < SystemState.argc; i++) {
                param = (char *) get_ParamValue(SystemState.argv[i], paramName);
                if (param != NULL) {
                    lua_pushstring(L, param);
                    break;
                }
            }
            if (param == NULL) { // Если параметр не найден вернем nil
                if (paramDefaultValue[0] == 0)
                    lua_pushnil(L); //
                else
                    lua_pushstring(L, paramDefaultValue);
            }
        } else {
            //
        }
        result = 1;
        //------------------------------------
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcGetParam')");
        lua_settop(L, 0);
    }

    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcGetParam(...)...end\n ");
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcGetParam()");
    return result;
}


//