//
#include "../scriptAPI.h"

#include <sys/stat.h>


#include <dirent.h>

#define FILE_LIST_MAX_NUM 100
#define FILE_NAME_MAX_SIZE 100
static char fileNameArray[FILE_LIST_MAX_NUM][FILE_NAME_MAX_SIZE];
static int getFileList(char * path, char isDir) {
	int result = 0;
	DIR *dir;
	struct dirent *ent;
	// d_type: file = 8 , dir = 4
	unsigned char filterCode = (isDir) ? DT_DIR : DT_REG;
	if ((dir = opendir (path)) != NULL) {
	  /* print all the files and directories within directory */
	  while ((ent = readdir (dir)) != NULL) {
		  // printf("%s : %d \n", ent->d_name, (int)ent->d_type);
		  if (ent->d_type == filterCode) {
			  strncpy(&fileNameArray[result][0], ent->d_name, FILE_NAME_MAX_SIZE - 1);
			  result++;
			  if (result >= FILE_LIST_MAX_NUM) {
				  Log_DBG((char *) "File item count limit detect!(%d)", result);
				  break;
			  }
		  }
	  }
	  closedir (dir);
	} 	
	return result;
	
}


// связь с ядром. Функции поиска

int script_core_proc(lua_State *L) {
    int i;
    char sName[255];
    char dirName[255];
    char * p;
    size_t t;
    TValue * d;
    int result = 0;
    script_synchronize(SCRIPT_SYNC_LOCK, "mcFunc()");
    i = lua_gettop(L);
    if (i >= 1) {
        scrGetStrArg("func", L, 1, sName, sizeof (sName));
        // Реализация различных функций сервера
        //------------------------------------
        result = 0; // кол-во возвращенных аргументов
        if (strcmp(sName, "EXIT") == 0) //
        {
            // завершение работы. Аргумент: код возврата
            if (i >= 2) {
                i = lua_tonumber(L, 2);
                Log_DBG((char *) "mcFunc(EXIT, %d)", i);
                exit(i);
            } else {
                Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcFunc(EXIT, ...)')");
            }
        }
        if (strcmp(sName, "CREATEDIR") == 0) {
            if (i >= 2) {
                scrGetStrArg("func", L, 2, dirName, sizeof (dirName));
                // создание директории 
                result = 1; // кол-во возвращенных аргументов
                struct stat sb;
                // проверим наличие директории
                //Log_DBG("[CREATEDIR] Check Directory: (%s)", dirName);
                if (stat(dirName, &sb) == 0 && S_ISDIR(sb.st_mode)) {
                    // Директория уже есть, все ОК
                    //Log_DBG("[CREATEDIR] Directory exists. (%s)", dirName);
                    lua_pushnumber(L, 1);
                } else {
					//Log_DBG("[CREATEDIR] Make Directory: (%s)", dirName);
                    if (mkdir(dirName, 0777) == 0) { 
                        // OK (0666 - не открывается на linux !!)
                        lua_pushnumber(L, 1);
                    } else {
                        // ERROR
                        Log_DBG("[CREATEDIR] Directory creation error! (%s)", dirName);
                        lua_pushnil(L);
                    }
                }

            } else {
                Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcFunc(CREATEDIR, ...)')");
            }
        }

        if (strcmp(sName, "FILELIST") == 0) {
            if (i >= 2) {
                scrGetStrArg("func", L, 2, dirName, sizeof (dirName));
                // Получить список директории
                result = 1; // кол-во возвращенных аргументов
                // проверим наличие директории
                //Log_DBG("[FILELIST] Directory: (%s)", dirName);
				int fileNum = getFileList(dirName, 0);
				lua_newtable(L);
				for (int n = 0; n < fileNum; n++) {
					lua_pushstring(L, &fileNameArray[n][0]); //
					lua_pushstring(L, "file"); //
					lua_settable(L, -3);
				}
            } else {
                Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcFunc(FILELIST, ...)')");
            }
        }
        if (strcmp(sName, "DIRLIST") == 0) {
            if (i >= 2) {
                scrGetStrArg("func", L, 2, dirName, sizeof (dirName));
                // Получить список директории
                result = 1; // кол-во возвращенных аргументов
                // проверим наличие директории
                //Log_DBG("[DIRLIST] Directory: (%s)", dirName);
				int fileNum = getFileList(dirName, 1);
				lua_newtable(L);
				for (int n = 0; n < fileNum; n++) {
					lua_pushstring(L, &fileNameArray[n][0]); //
					lua_pushstring(L, "dir"); //
					lua_settable(L, -3);
				}
            } else {
                Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcFunc(FILELIST, ...)')");
            }
        }
        
        if (strcmp(sName, "LOGLEVEL") == 0) {
            // Задать уровень вывода сообщение
            if (i >= 2) {
                int Level = lua_tonumber(L, 2);
                if ((Level >= 0)&&(Level < 200)) {
                    setLogLevel(Level);
                    if (Level == 0) Log_Ex("luaerr", LEVEL_TITLE, (char *) "LOGLEVEL -> DEBUG");
                    if (Level == 1) Log_Ex("luaerr", LEVEL_TITLE, (char *) "LOGLEVEL -> INFO");
                    if (Level == 2) Log_Ex("luaerr", LEVEL_TITLE, (char *) "LOGLEVEL -> WARNING");
                    if (Level >= 3) Log_Ex("luaerr", LEVEL_TITLE, (char *) "LOGLEVEL -> LEVEL_ERR");
                } else {
                    Log_Ex("luaerr", LEVEL_TITLE, (char *) "LOGLEVEL VALUE ERROR (%d)", Level);
                }
            } else {
                Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcFunc(LOGLEVEL, ...)')");
            }
        }

        if (strcmp(sName, "TIMEOUT") == 0) {
            // Задать таймаут для линии (вызов возможен в контексте событий линии)
            if (i >= 2) {
                int timeout = lua_tonumber(L, 2);
                extern int LineCallBackIndex;
                if (LineCallBackIndex != -1) {
                    if (SystemState.LLC.Lines[LineCallBackIndex])
                        SystemState.LLC.Lines[LineCallBackIndex]->LineControl.timeOutOption = timeout;
                } else {
                    Log_Ex("luaerr", LEVEL_ERR, (char *) "func: 'mcFunc(TIMEOUT, ...)' must be called inside the line event.");
                }
            } else {
                Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcFunc(TIMEOUT, ...)')");
            }
        }
        if (strcmp(sName, "GETTIME") == 0) {
            // функция ограничивает разрядность результата 
            lua_pushnumber(L, mcGetTime(0));
            // завершение работы. Аргумент: код возврата
            result = 1; // кол-во возвращенных аргументов
        }
        if (strcmp(sName, "GETTICK") == 0) {
            lua_pushnumber(L, mcGetDeltaTime(&SystemState.serverStartTime, NULL));
            // завершение работы. Аргумент: код возврата
            result = 1; // кол-во возвращенных аргументов
        }


        //------------------------------------
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcFunc')");
        lua_settop(L, 0);
    }
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcFunc()");
    return result;
}
