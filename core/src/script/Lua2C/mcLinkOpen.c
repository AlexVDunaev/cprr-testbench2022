//
#include "../scriptAPI.h"
//

int script_mcLinkOpen_proc(lua_State *L) {
    // Открытие линии связи. Пример вызова:
    //mcLinkOpen (11, "COM", "/dev/ttyS0", "115200,8N1", "RAW")
    int N;
    int LinkID;
    char DriverName[255];
    char DeviceName[255];
    char linkParam[255];
    char ProtocolName[255];
    char ProtocolParams[255] = "";
    char * p;
    size_t t;
    TValue * d;
    script_synchronize(SCRIPT_SYNC_LOCK, "mcLinkOpen()");
    N = lua_gettop(L);
    if (N >= 5) { // >= 5 аргументов
        LinkID = lua_tonumber(L, 1);
        scrGetStrArg("mcLinkOpen", L, 2, DriverName, sizeof (DriverName));
        scrGetStrArg("mcLinkOpen", L, 3, DeviceName, sizeof (DeviceName));
        scrGetStrArg("mcLinkOpen", L, 4, linkParam, sizeof (linkParam));
        scrGetStrArg("mcLinkOpen", L, 5, ProtocolName, sizeof (ProtocolName));
        if (N > 5) { // > 5 аргументов +(параметры протокола))
            scrGetStrArg("mcLinkOpen", L, 6, ProtocolParams, sizeof (ProtocolParams));
        }
        Log_DBG("mcLinkOpen(%d, %s, %s, %s, %s [%s])", LinkID, DriverName, DeviceName, linkParam, ProtocolName, ProtocolParams);
        // вызов функции открытия канала связи...
        // --------------------------------------
        coreLinkOpen(OBJ_SOURCE_SCRIPT, LinkID, DriverName, DeviceName, linkParam, ProtocolName, ProtocolParams);
        // --------------------------------------
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcLinkOpen')");
    }
    lua_settop(L, 0);
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcLinkOpen()");

    return 0;
}
