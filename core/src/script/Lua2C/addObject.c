//
#include "../scriptAPI.h"

// функция добавления объекта
// Пример вызова из Lua: addObject(OBJ_SIGNAL, "MySignalInt", "[LINE=1; ADDR=0x1000; TYPE=i32; HW=0x0001; VALUE={0}]", MySGProc)

int script_addObject_proc(lua_State *L) {
    int i;
    char sName[255];
    char sParam[255];
    int IntParam;
    int value;
    char objType[50] = "";
    char * p;
    TLineRec * line;
    TPollingRec* pollDrv;
    TProtFramingRec * framing;
    TProtocolRec * protocol;
    script_synchronize(SCRIPT_SYNC_LOCK, "addObject()");
    i = lua_gettop(L);
    if (i > 2) { // > 2 аргументов
        scrGetArgsFromStack(L, "addObject()", 1, "zz",
                &objType, sizeof (objType),
                &sName, sizeof (sName));

        if (strcmp(objType, "SIGNAL") == 0) { 
            // резерв : добавление сигнала
        } else
            if (strcmp(objType, "PROTOCOL") == 0) { // добавление протокола ("PROTOCOL.FRAMING" + "PROTOCOL.MANAGER"))
            // резерв
            scrDebugLog("addObject", "DBG. Script: addObject(PROTOCOL): ERROR! ");
        } else
            if (strcmp(objType, "PROTOCOL.FRAMING") == 0) { // добавление драйвера обертки
            // резерв
            scrGetStrArg("sig", L, 3, sParam, sizeof (sParam));
            value = luaL_ref(L, LUA_REGISTRYINDEX);

            scrDebugLog("addObject", "DBG. Script: addObject(id = %s, %s, %s)", objType, sName, sParam);
            framing = BuildFramingRec(&SystemState, sName, sParam, value);
            AppendFramingUnit(&SystemState, framing, OBJ_SOURCE_SCRIPT);
        } else
            if (strcmp(objType, "PROTOCOL.MANAGER") == 0) { // добавление драйвера обработки
            // резерв
            scrGetStrArg("sig", L, 3, sParam, sizeof (sParam));
            value = luaL_ref(L, LUA_REGISTRYINDEX);

            scrDebugLog("addObject", "DBG. Script: addObject(id = %s, %s, %s)", objType, sName, sParam);
            protocol = BuildProtocolRec(&SystemState, sName, sParam, value);
            AppendProtocolUnit(&SystemState, protocol, OBJ_SOURCE_SCRIPT);
        } else
            if (strcmp(objType, "POLLING") == 0) { // добавление драйвера опроса
            scrGetStrArg("sig", L, 3, sParam, sizeof (sParam));
            // резерв
            value = luaL_ref(L, LUA_REGISTRYINDEX);
            scrDebugLog("addObject", "DBG. Script: addObject(id = %s, %s, %s)", objType, sName, sParam);
            pollDrv = BuildPollingRec(&SystemState, sName, sParam, value);
            AppendPollingUnit(&SystemState, pollDrv, OBJ_SOURCE_SCRIPT);
        } else
            if (strcmp(objType, "LINE") == 0) { // добавление линии связи (открытие порта связи)
            scrGetArgsFromStack(L, "addObject()", 3, "nz", &IntParam, &sParam, sizeof (sParam));
            value = luaL_ref(L, LUA_REGISTRYINDEX);
            Log_DBG("addObject(LINE, Name=%s, id=%d, Param=%s)", sName, IntParam, sParam);

            scrDebugLog("addObject", "DBG. Script: addObject(LINE, Name=%s, id=%d, Param=%s)", sName, IntParam, sParam);
            line = BuildLineRec(&SystemState, sName, IntParam, sParam, value);
            scrDebugLog("addObject", "DBG. Script: addObject: BuildLineRec");
            AppendLine(&SystemState, line, OBJ_SOURCE_SCRIPT);
            scrDebugLog("addObject", "DBG. Script: addObject: AppendLine");
        }
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'addObject')");

    }
    lua_settop(L, 0);
    script_synchronize(SCRIPT_SYNC_UNLOCK, "addObject()");

    return 0;
}

