//
//
#include "../scriptAPI.h"
//
// передать сообщение об ошибке во время выполнения функции

int script_setLastError(lua_State *L) {
    int i;
    char * p;
    size_t t;
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>LastError(...)\n ");

    script_synchronize(SCRIPT_SYNC_LOCK, "LastError()");

    i = lua_gettop(L);
    if (i == 1) { // 1 аргументов
        scrGetStrArg("setLastError", L, 1, script_ErrorText, sizeof (script_ErrorText));
        Log_DBG("setLastError(%s)", script_ErrorText);
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'script_setLastError')");
    }
    lua_settop(L, 0);
    script_synchronize(SCRIPT_SYNC_UNLOCK, "LastError()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script>LastError(...)...end\n ");

    return 0;
}

