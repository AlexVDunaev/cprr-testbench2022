/**
 * @file  pause.c 
 * @brief Реализация функции передачи управления ядру на заданное время
 * 
 * Если в момент вызова нет ожидающих задач, то ядро просто ждет указанное время,
 * Если в момент вызова есть ожидающее событие, то ядро передает ему управление. 
 * 
 */
//
#include "../scriptAPI.h"
//
#include <unistd.h>




//
#include "../scriptAPI.h"
//
#include <unistd.h>


/**
 * @brief Передача управления ядру на заданное время
 * 
 */

static int pauseEntryCount = 0;

static int pause_mks(lua_State *L) {
    int i;
    int pause = 0;
    int result = 0;
    pauseEntryCount++;
    i = lua_gettop(L);
        // Контроль корректности стека LUA. При вызове pause() - аргумент всегда должен быть на вершине
        if (i > 0) { // == 1 аргументов
            pause = lua_tointeger(L, -1);
            lua_settop(L, -2);
            usleep(pause);
            } else {
                Log_DBG("Kol-vo agrs [%d] error (func: 'pause(...)')", i);
                result = -1;
            }
        if (SystemState.fDebugCore)
            Log_DBG("DBG Script> pause(%d)...end", pause);    
    pauseEntryCount--;    
    return result;
}


int script_pause(lua_State *L) {
    int result = 0;

    script_synchronize(SCRIPT_SYNC_LOCK, "pause()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> pause(...)\n ");

    if (core_synchronize_getCount("pause()")) {
        if (pauseEntryCount > 0) {
            Log_DBG("[Script]> pause(%d) : ERROR! Recursive function call is prohibited!\n", (int) lua_tointeger(L, lua_gettop(L)));
            exit(1);
        }
        // Если "поток" lua уже находится в синхронизации, на время выполнения pause 
        // снимаем блокировку, позволяя другим "потокам" (событиям) происходить
        core_synchronize(CORE_SYNC_UNLOCK, "pause");
        result = pause_mks(L);
        core_synchronize(CORE_SYNC_LOCK, "pause");
    } else {
        // поток не находится в синхронизации. Т.е. вызов pause не из события, а 
        // в основном потоке Lua.
        result = pause_mks(L);
    }

    script_synchronize(SCRIPT_SYNC_UNLOCK, "pause()");
    return result;
}
