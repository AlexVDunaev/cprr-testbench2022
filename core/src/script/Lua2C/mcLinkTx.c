//
#include "../scriptAPI.h"

int script_mcLinkTx_proc(lua_State *L) {
    // Запись в канал связи:
    //mcLinkTx (11, "DATABUFF")
    int N;
    int LinkID;
    char buffer[STR_MAX_SIZE];
    int bufferSize;
    int result = -1;
    char * p;
    size_t t;

    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcLinkTx(...)\n ");

    script_synchronize(SCRIPT_SYNC_LOCK, "mcLinkTx()");
    N = lua_gettop(L);
    if (N == 2) { // = 2 аргумента
        LinkID = lua_tonumber(L, 1);
        p = (char*) lua_tolstring(L, 2, &t);
        bufferSize = t;
        if (bufferSize >= STR_MAX_SIZE) {
            Log_Ex("luaerr", LEVEL_ERR, (char *) "buffer overflow (size = %d). (func: 'mcLinkTx')", bufferSize);
            bufferSize = STR_MAX_SIZE;
        }
        memcpy(buffer, (char *) p, bufferSize);
        //Log_DBG("mcLinkTx(%d, buff[%d])",LinkID, bufferSize);
        // вызов функции записи в канал связи...
        // --------------------------------------
        result = coreLinkTx(OBJ_SOURCE_SCRIPT, LinkID, buffer, bufferSize);
        // --------------------------------------
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcLinkTx')");
    }
    lua_settop(L, 0);
    if (result == MAC_RESULT_DEFERRED)
        lua_pushstring(L, "DEF");
    else
        if (result == MAC_RESULT_OK)
        lua_pushstring(L, "OK");
    else
        lua_pushstring(L, "ERR");
    lua_pushinteger(L, result); // числовое значение возврата. -1, -2, ... Коды ошибок (определяются драйвером)

    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcLinkTx()");
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcLinkTx(...)...end\n ");

    return 2;
}

