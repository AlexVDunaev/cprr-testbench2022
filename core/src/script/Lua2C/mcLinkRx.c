//
#include "../scriptAPI.h"
//

int script_mcLinkRx_proc(lua_State *L) {
    // Чтение из канала связи:
    //mcLinkRx (11) - чтение того что есть в буфере (кол-во не указывается)
    //mcLinkRx (11, count, timeOut ) - чтение не менее count байт c тайм-аутом
    int N;
    int LinkID;
    int TimeOut;
    int Count;
    char buffer[STR_MAX_SIZE];
    int bufferSize;
    int resultCoreLinkRx;
    int result = 0;
    char * p;
    size_t t;
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcLinkRx(...)\n ");

    script_synchronize(SCRIPT_SYNC_LOCK, "mcLinkRx()");
    N = lua_gettop(L);
    if (N == 3) { // = 3 аргумента
        LinkID = lua_tonumber(L, 1);
        Count = lua_tonumber(L, 2);
        TimeOut = lua_tonumber(L, 3);
        lua_settop(L, 0);
        // Log_DBG("mcLinkRx(%d, count = %d, timeOut = %d)", LinkID, Count, TimeOut);
        // вызов функции чтения канала связи...
        // --------------------------------------
        bufferSize = Count;
        resultCoreLinkRx = coreLinkRx(OBJ_SOURCE_SCRIPT, LinkID, buffer, &bufferSize, TimeOut);
        if (resultCoreLinkRx == MAC_RESULT_DEFERRED) {
            // резерв! на случай неблокирующих операций
            lua_pushstring(L, "DEF");
            lua_pushstring(L, "");
        } else
            if (resultCoreLinkRx == MAC_RESULT_OK) {
            lua_pushstring(L, "OK");
            lua_pushlstring(L, buffer, bufferSize);
        } else {
            lua_pushstring(L, "ERR");
            lua_pushstring(L, "");
        }
        //        script_synchronize(SCRIPT_SYNC_UNLOCK);
        //        return 2; // возвращаем 2 параметра - статус операции и буфер
        result = 2;

        // статус: {"OK" | "ERR"}
        // --------------------------------------
    } else
        if (N == 1) { // = 1 аргумента
        LinkID = lua_tonumber(L, 1);
        lua_settop(L, 0);
        //Log_DBG("mcLinkRx(%d)",LinkID);
        // вызов функции записи в канал связи...
        // --------------------------------------
        bufferSize = 0;
        resultCoreLinkRx = coreLinkRx(OBJ_SOURCE_SCRIPT, LinkID, buffer, &bufferSize, 0);
        // --------------------------------------
        if (resultCoreLinkRx == MAC_RESULT_DEFERRED) {
            lua_pushstring(L, "DEF");
            lua_pushstring(L, "");
        } else
            if (resultCoreLinkRx == MAC_RESULT_OK) {
            lua_pushstring(L, "OK");
            lua_pushlstring(L, buffer, bufferSize);
        } else {
            lua_pushstring(L, "ERR");
            lua_pushstring(L, "");
        }
        result = 2;
        //return 2; // возвращаем 2 параметра - статус операции и буфер
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'mcLinkRx')");
        lua_settop(L, 0);
    }
    if (SystemState.fDebugCore)
        Log_DBG("DBG Script> mcLinkRx(...)...end\n ");
    script_synchronize(SCRIPT_SYNC_UNLOCK, "mcLinkRx()");

    return result;
}

