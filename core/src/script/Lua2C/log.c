//
#include "../scriptAPI.h"
//
// Вывод в лог
// Пример вызова из Lua: log("текст")
// [Пример вызова из Lua: log("текст", "LogFileName")]

int script_log(lua_State *L) {
    int i;
    char text[255];
    char logfile[20] = "dbg"; // ограничение по длине в модуле log
    char * p;
    size_t t;


    script_synchronize(SCRIPT_SYNC_LOCK, "log()");
    scrDebugLog("log", "DBG Script>log(...)");
    
    i = lua_gettop(L);

    if ((i == 1) || (i == 2)) { // > 2 аргументов
        scrGetStrArg("log", L, 1, text, sizeof (text));
        if (i == 2) { // Если задано имя файла, пишем в него
            scrGetStrArg("log", L, 2, logfile, sizeof (logfile));
        }
        Log_Ex(logfile, LEVEL_ERR, "%s", text);
    } else {
        Log_Ex("luaerr", LEVEL_ERR, (char *) "Kol-vo agrs error (func: 'log')");
    }
    lua_settop(L, 0);
    scrDebugLog("log", "DBG Script>log(...)...end\n ");
    script_synchronize(SCRIPT_SYNC_UNLOCK, "log()");

    return 0;
}


