#ifndef scriptAPI_h
#define scriptAPI_h

#include "../../Lua/lua-5.3.2/src/lua.h"
#include "../../Lua/lua-5.3.2/src/lauxlib.h"                         
#include "../../Lua/lua-5.3.2/src/lualib.h"                          
#include "../main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../log/log.h"
#include "../core/commandUnit.h"
#include "../core/other.h"
#include "../core/coreUInterface.h"
#include "../core/coreScriptInterface.h"
#include "../synchronize/synchronize.h"
#include "scriptUtils.h"

extern TSystemState SystemState;


int script_set_signal(lua_State *L);
int script_cmd_signal(lua_State *L);
int script_req_signal(lua_State *L);
int script_get_proc(lua_State *L);
int script_link_proc(lua_State *L);
int script_log(lua_State *L);
int script_hexlog(lua_State *L);
int script_pause(lua_State *L);
int script_setLastError(lua_State *L);
int script_addObject_proc(lua_State *L);
int script_mcLinkOpen_proc(lua_State *L);
int script_mcLinkTx_proc(lua_State *L);
int script_mcLinkRx_proc(lua_State *L);
int script_core_proc(lua_State *L);
int script_mcGetParam_proc(lua_State *L);
int script_mcSetParam_proc(lua_State *L);
int script_mcSignalsSelect_proc(lua_State *L);
int script_mcConfig_proc(lua_State *L);
int script_mcArgTest_proc(lua_State *L);
int script_mcLinkCtrl_proc(lua_State *L);
int script_mcCalcCRC(lua_State *L);

int script_alg_Init(lua_State *L);

// нужно заменить...
void script_synchronize(int lock, char * dbgText);
void core_synchronize(int lock, char * dbgText);

TScriptInstance * scrInit(int argc, char **argv, char * startFileName);
int scrOnRun(void); // Запуск функции Run() в startFileName

extern TScriptInstance Instance;
extern lua_State * Script; //!!! потом нужно заменить на Instance.Script

//!!! потом нужно заменить...
extern char script_ErrorText[255];
extern Tsub_param_rec tempSignalBufferValue;
extern int coreLockCount;
extern int linkFuncEntryCount;


#endif


