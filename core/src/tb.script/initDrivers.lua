--
dofile("./tb.script/utils/common.lua") -- общие функции
dofile("./tb.script/drivers/GUI/GUI.lua") -- Связь с GUI
dofile("./tb.script/drivers/REPORT/report.lua") -- Генератор протокола
-- Оборудование
dofile("./tb.script/drivers/N5770/N5770.lua") -- ИП
dofile("./tb.script/drivers/CPRR/cprr24.lua") -- РАДАР CPRR24
dofile("./tb.script/drivers/N5183B/N5183B.lua") -- ГЕНЕРАТОР
dofile("./tb.script/drivers/N9040B/N9040B.lua") -- СПЕКТРОАНАЛИЗАТОР
dofile("./tb.script/drivers/OPU/opu.lua") -- ОПУ (192.168.70.10)
-------------------------------------------------
dofile("./tb.script/drivers/CPYC/cpyc77.lua") -- РАДАР CORNER77

isDebug = 0 -- <- 2022.06.01 Поменял тут
isEmul  = 0
cmdIP = "192.168.1.4"
cmdPort = 7000
RadarSN = "0"

function InitDrivers()

  REPORT = REPORT_Init()

if (mcGetParam("args", "woradar", "0") == "1") then
	GUI = GUI_Init(5457) 
    log("InitDrivers> GUI Port: 5457")
	return
end
  GUI = GUI_Init() 

  cmdIP = mcGetParam("args", "IP", "192.168.1.4")
  log(string.format("---------------> IP is: %s", cmdIP))

  cmdPort = mcGetParam("args", "PORT", "7000")
  log(string.format("---------------> PORT is: %d", cmdPort))

  radarSelect = mcGetParam("args", "radar", "cprr")
  if radarSelect == "cpyc" then
    log("---------------- !!!!! -------------------")
    log("---------------- cpyc  -------------------")
    log("---------------- !!!!! -------------------")
	isDebug = 1
  end


  if isDebug == 1 then
    log("---------------- !!!!! -------------------")
    log("---------------- Debug -------------------")
    log("---------------- !!!!! -------------------")
  if radarSelect == "cpyc" then
    log("---------------> RADAR: CPYC!!! -------------------")
	RADAR = CPYC_Init(cmdIP, cmdPort)
   -- OPU = OPU_Init("192.168.70.10", 23) 	
   -- POWER = N5770_Init("192.168.1.222", 5025) -- REAL (NOTE: CHECK IP: "LAN" BUTTON)
	
  else
    RADAR = CPRR_Init(cmdIP, cmdPort)
  end

    --OPU = OPU_Init("192.168.1.60", 8110) 
    --RADAR = CPRR_Init("127.0.0.1", 7000, 7001) 
	--RADAR = CPRR_Init("127.0.0.1", 7100, 7001) 
	--RADAR = CPRR_Init("192.168.1.6", 7006, 7001) 
    --POWER = N5770_Init("192.168.1.222", 5025)
    --RF_GEN = N5183B_Init("192.168.1.60", 8110)
    --SPECTRUM = N9040B_Init("192.168.1.60", 5025)
    --POWER = N5770_Init("192.168.1.60", 8110) -- EMUL
  else

    OPU = OPU_Init("192.168.70.10", 23) 

    RADAR = CPRR_Init(cmdIP, cmdPort)

    POWER = N5770_Init("192.168.1.222", 5025) -- REAL (NOTE: CHECK IP: "LAN" BUTTON)

    RF_GEN = N5183B_Init("192.168.1.200", 5025) -- REAL (NOTE: CHECK IP!)

    --SPECTRUM = N9040B_Init("192.168.1.40", 5025)
  end

  log("InitDrivers> done!")
end


