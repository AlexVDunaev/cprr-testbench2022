-- Общие функции модулей тестирования

-- TODO: перенести в утилиты.
-- fullPath : Путь к директории. Должен начинаться с имени папки ("./" - не нужна!)
-- Разделитель между директориями "/" (linux like)
function CreateDir(fullPath)
  local path = nil
  for s in string.gmatch(fullPath, "[^/]+") do  
    if path then
		path = path.."/"..s
	else
		path = s
	end
	--log("[CreateDir]> path:"..path)
    if s ~= ".." then
		--log("[mcFunc]> path:"..path)
		if (mcFunc("CREATEDIR", path)==nil) then
		  log("CREATEDIR> Error! Path : "..path)
		end
	end
  end
end

-- функция копирования файлов
function WriteToFile(path, data)
  local file = io.open(path, "wb")
  if not file then
    print(path, "WriteToFile ERROR!!!")
    return false
  end
  file:write(data)
  file:close()
  return true
end


-- функция копирования файлов
function CopyFile(old_path, new_path)
  log("CopyFile("..old_path.." --> "..new_path..")")
  local old_file = io.open(old_path)
  local new_file = io.open(new_path, "wb")
  if not old_file or not new_file then
    print(old_path, new_path, "ERROR")
    return false
  end
  while true do
    local block = old_file:read(2^13)
    if not block then break end
    new_file:write(block)
  end
  old_file:close()
  new_file:close()
  return true
end



function getReportPath(fileName)
  -- Если нет SN - пишем в директорию 000000000
  local sn = "000000000"
  if (RADAR.SN) then
    sn = RADAR.SN
  end

  if (fileName) then
    return REPORT_BASE_DIR .."/"..sn.."/"..fileName
  else
    return REPORT_BASE_DIR .."/"..sn
  end
end

-- Тест наличия радара. Запрос 1 кадра стрима.
-- При запуске режима стрима или трекинга, ядро запрашивает версию радара
-- --> после удачной записи стрима, обновятся параметры RADAR.[SN, Temperature, Version]
function checkRadar()
  local tryCount
  local result
  if (RADAR)and(GUI) then
    -- данный код, с запросом "tmp.bag" не нужен!
    log("Проверка наличия радара...")
    for tryCount = 1, 3 do
      log(GUI.Log("Попытка получить данные с радара..."))
      result = RADAR.SaveStream("tmp.bin", 1)
      if (result > 0) then
        log(GUI.Log("Проверка наличия радара...OK"))
		RADAR.GetTargets(1) --2022.06.01 for WDT OFF
        return 1
      end
    end
    log("Проверка наличия радара...Ошибка! Радар не отвечает!")
  end
  return nil
end
