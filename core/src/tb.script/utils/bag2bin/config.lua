


bagName = "./rf_noise.bag"
binName = "./rf_noise.bin"

function Init()
  log("Init(). Begin")
  
  alg        = Algorithm_Init()
  u, info = alg.getFrame(bagName, 0);  
  body = alg.getField(u, 0, 64*720*16*2).mem
  drop = string.sub(body, 1, 4*720*16*2)
  head = string.pack("i4i4i4i8i8i2i2i2i2i2i2i4i4i4", 0xABCD, 0, 10, 0, 1,
    0,0,0,0,0,0, info.chirps, info.channels, info.points)
  frame = head..drop..body

  sizeOfData = info.chirps * info.channels * info.points * 2 + 52
  if (sizeOfData == #frame) then 
    print(info.chirps, info.channels, info.points, sizeOfData)
    mmapDrv = alg.mmapInit(binName, sizeOfData) -- init mmap file
    alg.mmapWrite(mmapDrv, frame)
  else
    log("ERROR!!!")
  end

  log("Init(). End")
  mcFunc("EXIT",1)
end


function Run()
  log("Run()")
  mcFunc("EXIT",1)
end

