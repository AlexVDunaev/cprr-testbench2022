
-- переменные модуля уровень шума в приёмном тракте
algCprrCalib = {workDirectory = nil,  -- директория алгоритма
  frameCount    = 0,    -- счетчик полученных кадров обработчиком стриминга 
  totalframeCount = 0,  -- счетчик кадров для случая обработки нескольких файлов
  mmapDrv       = nil,  -- драйвер MMAP через, который обмениваемся данными с модулем алгоритма
  alg           = nil,  -- таблица методов Алгоритма CPRR
  isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
  debugMode     = nil,  -- режим отладки. (не пишем в mmap)
  calibVectors  = nil,  -- калибровочные вектора
  calibNum      = 0,    -- кол-во векторов
  inputFileNum  = 1,    -- кол-во входных файлов
  isLoadedConfig = nil, -- признак загрузки конфига
  errorText     = nil   -- сообщение об ошибке оператору
}

CALIB_CPRR_SAVE_STREAM = 0
-- CALIB_CPRR_FRAME_NUM = 200 -- вынес в конфиг
CALIB_CPRR_CALC_VECTOR = 1 -- 0: позволит отключить расчет калиб. вектора

CALIB_CPRR_LOAD_CONFIG = 1 -- загрузка файла конфигурации (калибровка+пролаз)
CALIB_CPRR_CONFIG_FILE = "config.bag"
CALIB_VECTORS_MAX_NUM  = 15 -- допустимое кол-во векторов калибровки
CALIB_USE_ONES_FOR_STARTVECTOR = 1 -- Использовать Единичную матрицу калибровки (на первом шаге)


-- обработчика прерывания теста пользователем
local function onBreakCalib()
  -- аварийное приведение в исх. состояние
  algCprrCalib.errorText = "Тест прерван"
  GUI.Log(algCprrCalib.errorText, "Red")
  algCprrCalib.isBreak = 1
  BREAK = 1
  log("Тест прерван!!!")
end

-- Удаление элемента с минимальным растоянием между его соседями (правым и левым) с списке
local function DeleteMostUnnecessary(T)
  local min = 1000
  local minIndex = 1
  local n = 0
  local k, v
  -- Узнаю размер списка
  for k, v in pairs(T) do
      n = n + 1
    end
    -- найду элемент, который безболезненно можно удалить
    for k = 1, n - 3 do
      local dT = T[k + 2] - T[k]
      -- print(k, dT, T[k + 2], T[k])
      if dT < min then
        min = dT
        minIndex = k + 1
        end
    end
  -- Удаляю элемент
  table.remove( T, minIndex )
  end


local function getCalibVector(str)
  local result = {}
  local resultIndex = 0
  -- log("getCalibVector str size: " .. #str)


  if str == "ERROR" then
	return nil
  end

  if #str > 0 then
	  local v
	  local n = 0
	  local real, imag
	  for v in (str.." "):gmatch("(.-) ") do
		  n = n + 1
		  if n == 1 then real = tonumber(v)
			elseif n == 2 then imag = tonumber(v)
			  n = 0
			  resultIndex = resultIndex + 1
			  result[resultIndex] = {real = real, imag = imag}
		  end
	  end
  end
  return resultIndex, result
  end

local function getConfigRadar(filename, tmCode)
  local tryCount
  local getConfigResult
  local cfg
  local a
  local tm
  local leak

  if (tmCode == nil) then
    --[[ начиная с версии v2.76, радар возвращает код температуры в любом режиме стриминга
    -- установка обработчика событий в стрим режиме. Интерисует только код температуры
    RADAR.StreamEvent = function (time, GNumber, t1, t2, t3, t4, t5, t6)
      algRefAngle.radarTemp = t1
      log(string.format("STREAM> TEMP:  [%d %d %d %d %d %d]", t1, t2, t3, t4, t5, t6))
      end
    -- Если Георгий добавит код температуры в режим №4 (чт. конфига), 
    -- данный код, с запросом "tmp.bag" не нужен!
    log("Чтение температурного кода...")
    for tryCount = 1, 3 do
      getConfigResult = RADAR.SaveStream("tmp.bag", 1)
      if (getConfigResult > 0) then
         if (algRefAngle.radarTemp) then
          log("температурный код:"..algRefAngle.radarTemp)
          break
          end
      else
        log("Во время температурного кода радара возникла ошибка!")
      end
    end
    tmCode = algRefAngle.radarTemp
    ]]
    -------------------------------------------------------
  end
  
if CALIB_CPRR_SAVE_STREAM == 1 then
  log("Чтение конфигурации радара...")

  for tryCount = 1, 3 do
    getConfigResult = RADAR.SaveStream(filename, 1, 4)
    if (getConfigResult > 0) then
      log("Конфигурация радара прочитана.")
      break
    else
      log("Во время чтения конфигурации радара возникла ошибка!")
    end
  end

  if (getConfigResult == 0) then
    log("Считать конфигурацию радара не удалось!")
    return nil
  end

  if (tmCode == nil) then
    tmCode = RADAR.Temperature
  end
end
  a = Algorithm_Init()

  cfg, size, tm, leak = a.getConfig(filename, tmCode)
  -- возврат вектора ТМ (подходящий к tmCode) и пролаза

  if (tmCode)and(tm) then
    log("Конфигурация для температурного кода: "..tmCode.." -> "..string.unpack("f", tm))
  end
  return tm, leak

end

local function CalibProcessFrame(time, GNumber, t1, t2, t3, t4, t5, t6, ptrData, sizeOfData)
  
  -- Кол-во кадров, в обработке
  algCprrCalib.frameCount = algCprrCalib.frameCount + 1

  -- Инициализация драйвера mmap (Однократно)
  if ((algCprrCalib.mmapDrv == nil)and(algCprrCalib.workDirectory)) then
    algCprrCalib.mmapDrv = algCprrCalib.alg.mmapInit(algCprrCalib.workDirectory.."/calib_adc.bin", sizeOfData) -- init mmap file
  end
  
  -- Передача кадра данных в mmap и сигнал алгоритму, что нужно обработать данные кадра
  if (algCprrCalib.mmapDrv) then
    algCprrCalib.alg.mmapWrite(algCprrCalib.mmapDrv, ptrData)
    GUI.RunAlg("RFCalib", "CALC", "wait") 
  end
  
  -- Индикация прогресса проверки
  algCprrCalib.totalframeCount = algCprrCalib.totalframeCount + 1
  GUI.Progress(math.floor(100 * algCprrCalib.totalframeCount / (CALIB_CPRR_FRAME_NUM* algCprrCalib.inputFileNum)))
  
  -- Проверка на прерывание теста
  if (algCprrCalib.isBreak) then
    return "BREAK" -- прекращаем стриминг
    end

end


-- Обработчик события стриминга
local function userStreamEventCalib(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)

  -- Кол-во кадров, полученных в стрименге
  algCprrCalib.frameCount = algCprrCalib.frameCount + 1

  --log(string.format("CPYCLineEvent1>[RX]: GNumber: %4d ", frameIndex)) 

  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algCprrCalib.frameCount / CALIB_CPRR_FRAME_NUM))

  -- Проверка на прерывание теста
  if (algCprrCalib.isBreak) then
    return "BREAK" -- прекращаем стриминг
  end
end


-- циклограмма получения калибровки
-- Возврат: ["OK" - Норма / "ABORT" - тест прерван], калибровочный вектор
function TEST_CPRR_CALIB(path, bagFileName, tmCode, dialogMode)
 local calibVector
 local calibNum
 local calibRawString 
 local info = ""
	if tmCode then
	 info = " (Т.Код:"..tmCode ..")"
	end
  --GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт калибровки..."..info)

  -- инициализация данных теста
  algCprrCalib.alg        = Algorithm_Init()
  algCprrCalib.isBreak    = nil
  algCprrCalib.mmapDrv    = nil
  algCprrCalib.errorText  = "[Не удалось получить сырые данные с радара]" -- ошибка по умолчанию
  algCprrCalib.frameCount = 0

  -- инициализация алгоритма (очистка буферов данных) Возврат: путь к файлам
  algCprrCalib.workDirectory = GUI.RunAlg("RFCalib", "START", "wait")
  --------------
  if (algCprrCalib.workDirectory == nil) then
    GUI.Log("Ошибка. Директория назначения не определена")
    return "ABORT"
  end

  -- проверка на наличие готового результат (например если произошел сбой на середине расчета)
	local tmpfile = io.open(path.."/"..tmCode..".tmp", "r")
	if tmpfile then
		calibRawString = tmpfile:read("*a")
		tmpfile:close()
		if calibRawString then
		end
		calibNum, calibVector = getCalibVector(calibRawString)
	    if calibNum == 32 then
			GUI.Log("Калибровка восстановлена!"..info.." для "..bagFileName)
			return "OK", calibVector
		end
	end

  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreakCalib

  bagFileName = path .."/" .. bagFileName
  local bagConfigFileName = path .."/" .. CALIB_CPRR_CONFIG_FILE  --getReportPath(CALIB_CPRR_CONFIG_FILE)

  if CALIB_CPRR_SAVE_STREAM == 1 then
	  RADAR.StreamEvent = userStreamEventCalib
	  -- запуск стрима на заданное число кадров
	  RADAR.SaveStream(bagFileName, CALIB_CPRR_FRAME_NUM)

	  RADAR.StreamEvent = nil
		
	  if (algCprrCalib.frameCount < CALIB_CPRR_FRAME_NUM) then 
		  -- Что-то пошло не так... не то количество кадров...
		  if (dialogMode) then
			GUI.Dialog("design/commonError.htm", "wait", algCprrCalib.errorText)
		  end
		  GUI.Log("Ошибка. Не получены сырые данные с радара или стримера", "red")
		  return "ABORT"
	  end
  end
  
  if CALIB_CPRR_LOAD_CONFIG == 1 then
	if algCprrCalib.isLoadedConfig == nil then
	  local tm, leak

	  tm, leak = getConfigRadar(bagConfigFileName, 0)
--	  tm, leak = getConfigRadar(CALIB_CPRR_CONFIG_FILE, 0)

	  if (tm)and(leak) then
		WriteToFile(algCprrCalib.workDirectory .. "/config.tm.bin", tm)
		WriteToFile(algCprrCalib.workDirectory .. "/config.leak.bin", leak)
		if CALIB_USE_ONES_FOR_STARTVECTOR == 1 then
			info = GUI.RunAlg("RFCalib", "CONFIG:ONE", "wait")
		else
			info = GUI.RunAlg("RFCalib", "CONFIG", "wait")
		end
		if info then
			log(string.format("%s", info))
		end
		
		algCprrCalib.isLoadedConfig = 1
	  else
		if (dialogMode) then
		  GUI.Dialog("design/rAngleErr.htm", "wait", "Не удалось считать конфигурацию радара!")
		end
		GUI.Log("Ошибка", "red")
		return "ABORT"
	  end
    end
  end

  local bagFileSN = "undefSN"
  
  if CALIB_CPRR_CALC_VECTOR == 1 then
	  log("Обработка кадров bag-файла...")
	  -- снова очистим счетчик кадров 
	  algCprrCalib.frameCount = 0
	  
	  local frameSize = 68*720*16*2
	  local index = 0
	  local frame, info
	  while (algCprrCalib.isBreak == nil) do
	  
	    frame, info = algCprrCalib.alg.getFrame(bagFileName, index, 0, 68*720*16*2, 0);
		
		if frame == nil then
			break
		end
		local t = info["temperature"]

		bagFileSN = string.format("%d", info["SN"])
		local field = algCprrCalib.alg.getField(frame, 0, 68*720*16*2)
		if field == nil then
			log("field == nil")
		end
		local body = field.mem
		if body == nil then
			log(string.format("READ ERROR! Frame:"))
			print(frame)
			break
		end
		local head = string.pack("i4i4i4i8i8i2i2i2i2i2i2i4i4i4", 0xABCD, 0, 10, 0, 1,
			0,0,0,0,0,0, info.chirps, info.channels, info.points)
		local radarframe = head..body

		--log(string.format("READ: %d", index))

		CalibProcessFrame(info["time"], index, t, t, t, t, t, t, radarframe, #radarframe)
	  
		index = index + 1
		if index == CALIB_CPRR_FRAME_NUM then 
		  break
	    end
	  end

	  log(string.format("Обработано %d кадров.", algCprrCalib.frameCount))
	  
	  if (algCprrCalib.mmapDrv) then
		-- Закроем драйвер MMAP
		algCprrCalib.alg.mmapClose(algCprrCalib.mmapDrv)
	  end
	  if algCprrCalib.isBreak then
		return "ABORT"
	  end
	  
	  if (algCprrCalib.frameCount < 1) then --CALIB_CPRR_FRAME_NUM
		-- Что-то пошло не так... не то количество кадров...
		if (dialogMode) then
		  GUI.Dialog("design/commonError.htm", "wait", "[Не удалось обработать сырые данные с радара]")
		end
		GUI.Log("Ошибка. Не удалось обработать сырые данные с радара", "red")
		return "ABORT"
	  end
	  -- Завершим алгоритм. Результат: файл калибровки, выход: 64 числа, разделенных " "
	  -- Calibration_matrix_32_radar_418169957_413.h 
	  
	  local  exportPath = REPORT_BASE_DIR .. "/" .. bagFileSN
	  log(string.format("CreateDir : [%s]", exportPath))
	  CreateDir(exportPath)
	  -- поскольку путь передаем в GUI, то и путь к REPORTS относительно GUI
	  exportPath = REPORT_BASE_DIR_GUI .. "/" .. bagFileSN
	  local hFileName = string.format("%s/Calibration_matrix_32_radar_%s_%d.h", exportPath, bagFileSN, tmCode)
	  log(string.format("getCalibVector. Export file[%s]", hFileName))
	  calibRawString = GUI.RunAlg("RFCalib", "DONE:"..hFileName, "wait")
	  calibNum, calibVector = getCalibVector(calibRawString)
	  if calibNum then
	  --log(string.format("getCalibVector size: %d", calibNum))
		  if calibNum ~= 32 then
			log(string.format("Неверный размер результирующего вектора! (%d)", calibNum))
			return "ABORT"
		  end
		  
		tmpfile = io.open(path.."/"..tmCode..".tmp", "w")
		if not tmpfile then
		  log('Ошибка создания tmp файла! для tmCode:' .. tmCode)
		  else
			tmpfile:write(calibRawString)
			tmpfile:close()
		end
      else
		log(string.format("Ошибка получения вектора калибровки!"))
		return "ABORT"
	  end
  end

  GUI.OnBreak = nil

  -- создать папку для отчетов
  -- 
  
  GUI.Log("Калибровка завершилась!"..info.." SN:"..bagFileSN)
  return "OK", calibVector

end



function TEST_CPRR_CALIB_DIR_PROCESS(path, dialogMode)

	local count = 0
	local tmArray = {}
	local tmFileList = {}
	local v, value, k, i, f, fType
	local bagSN, tmCode
	-- получим список файлов в директории
	local fileList = mcFunc("FILELIST", path)  

    algCprrCalib.alg        = Algorithm_Init()
	algCprrCalib.calibVectors = {}
	algCprrCalib.calibNum = 0
	algCprrCalib.totalframeCount = 0
	algCprrCalib.isLoadedConfig = nil
	
    for k, v in pairs(fileList) do
	  local sn
      -- print(k, v)
      tmCode, sn = string.match(k, '^calib_temper_(%d+)_(%d+)_.*%.bag$')
      if tmCode then
        tmCode = tonumber(tmCode)
		local isTmCodeOk = 1
		if count > 0 then
		 local j
		 for j = 1, count do
			if tmArray[j] == tmCode then
				log('Файлы с тм.кодами дублируются! Игнорируем дубли')
				isTmCodeOk = nil
			end
		 end
		end
		if isTmCodeOk then
			count = count + 1
			tmArray[count] = tmCode
			tmFileList[tmCode] = k
		end
		
		if bagSN then
			if bagSN ~= sn then
				log('Что-то пошло не так!!! SN в файлах разный!')
				break
			end
		else
			bagSN = sn
		end
	  end
    end
	-- Обязательно делаем сортировку
	table.sort(tmArray)

	algCprrCalib.inputFileNum = count
	
	-- нужно проредить, что бы в итоге осталось не более CALIB_VECTORS_MAX_NUM векторов
	for i = CALIB_VECTORS_MAX_NUM, count - 1 do
	  DeleteMostUnnecessary(tmArray)
	  algCprrCalib.inputFileNum = algCprrCalib.inputFileNum - 1
	  end

	for k, v in pairs(tmArray) do
	    f = tmFileList[v]
	  --for f, fType in pairs(fileList) do
		if string.find(f, '^calib_temper_'.. v ..'_.*%.bag$') then
		-- обработка файлов для соотв. температурных кодов
			log('обработка файлов для соотв. температурных кодов....')
			print(k, v, f)
			local result, calib = TEST_CPRR_CALIB(path, f, v, dialogMode)
			if result == "OK" then
				if calib then 
					log(string.format("Калибровка для %d есть!", v))
					algCprrCalib.calibVectors[v] = calib
					algCprrCalib.calibNum = algCprrCalib.calibNum + 1
				end
			else
				log('Калибровка не завершилась!!!')
				return "ABORT"
			end
        end
	  --end
    end
	log(string.format("Итого кол-во векторов: %d", algCprrCalib.calibNum))
	GUI.View("Вектора калиб.", ""..algCprrCalib.calibNum)
	
	if algCprrCalib.calibNum > 0 then
		-- Экспорт в telnet - файл
		log(string.format("Экспорт в telnet - файл"))

		local exportFileName = REPORT_BASE_DIR .. "/" .. string.format("%d/Telnet_radar_SN_%d_set.sh", bagSN, bagSN)
		
		local file = io.open(exportFileName, "w")
		if not file then
		  log('Ошибка экспорта в файл! ' .. exportFileName)
		  return "ABORT"
		end
		
		for n, tmCode in pairs(tmArray) do
			if algCprrCalib.calibVectors[tmCode] == nil then
			  log('Что-то пошло не так!!! calibVector == nil для tmCode='..tmCode)
			  return "ABORT"
			end
			file:write(string.format("calibration selvector %d\n", tmCode))
			for k, value in pairs(algCprrCalib.calibVectors[tmCode]) do
				file:write(string.format("calibration setvector %d %f %f\n", k, value.real, value.imag))
			end
		end
		file:close()
	GUI.Log("Папка с калибровками обработана!", "green")
	end
-- Удалим временные файлы
	fileList = mcFunc("FILELIST", path)  
    for k, v in pairs(fileList) do
      tmCode = string.match(k, '^(%d+).tmp$')
	  if tmCode then
		os.remove(path.."/"..k)
	  end
	end

end

-- обработка пакета калибов. Т.е. packpath - путь к папке, внутри которой лежат папки радарных калибов
-- packpath\
--          418119658\
--          418127430\
--          418157872\
--          ...
function TEST_CPRR_CALIB_PACK_PROCESS(packpath, dialogMode)
	-- получим список файлов в директории
	local v, k
	local fileList = mcFunc("DIRLIST", packpath)  
	algCprrCalib.isBreak = nil
    for k, v in pairs(fileList) do
	  local sn
      sn = string.match(k, '^(%d+)$')
	  if sn then
		log(string.format("Запуск расчета калибровки для радара №%s ...", sn))
		TEST_CPRR_CALIB_DIR_PROCESS(packpath .. "/" .. sn, dialogMode)
		-- Проверка на прерывание теста
		if (algCprrCalib.isBreak) then
			GUI.Log("Остановка расчета!", "green")
			return "BREAK" -- прекращаем стриминг
		end
		
	  end
	end
	GUI.Log("Обработка пакета завершена", "green")
end
