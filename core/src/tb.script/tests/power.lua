
powerShowedDialogFlag = nil

powerTestResultPmax = nil

if (power_test_error == nil) then
  power_test_error = 0
end
-- Определим обработчик ошибки (что-то с ИП)
function PowerFatalError(code)
  log(string.format("PowerFatalError> code : %s", code))
  -- аварийное приведение в исх. состояние
  GUI.Log("Авария! ИП не отвечает!", "Red")
  power_test_error = 1
end


function GetRadarPower()
  if (POWER) then
    POWER.OnError = PowerFatalError
    if (power_test_error > 0) then
      -- прошлая проверка завершилась ошибкой --> пробуем сделать реконнект
      GUI.Log("Попытка соединиться с ИП...")
      if (POWER.ReConnect() == 1) then
        GUI.Log("Попытка соединиться с ИП...ОК")
        power_test_error = 0
      else
        GUI.Log("Попытка соединиться с ИП...Ошибка!")
        return nil
      end
    end
    local I
    local V
    I, V = POWER.GetMeasure()
    if ((I)and(V)) then
      if (V < 1) then
        -- источник выключен
      end
      return I*V, I, V
    end
  end
  return nil
end

function SetRadarPower(value, dialogMode)
  if (POWER) then
    POWER.OnError = PowerFatalError
    if (power_test_error > 0) then
      -- прошлая проверка завершилась ошибкой --> пробуем сделать реконнект
      GUI.Log("Попытка соединиться с ИП...")
      if (POWER.ReConnect() == 1) then
        GUI.Log("Попытка соединиться с ИП...ОК")
        power_test_error = 0
      else
        GUI.Log("Попытка соединиться с ИП...Ошибка!")
        return "ERROR"
      end
    end
	
	  if (dialogMode) then
		if powerShowedDialogFlag == nil then
			if GUI.Dialog("design/power.htm", "wait") ~= "OK" then
			   GUI.Log("Проверка отменена!", "red")
			   return "ABORT"
			else
			   powerShowedDialogFlag = 1
			end
		end
	  end
	
	
    if (POWER.SetVoltage(55.0)) then
      if (POWER.SetCurrent(0.6)) then
        if(POWER.SetOutput(value)) then
          return "OK"
        end
      end
    end
  end
  return "ERROR"
end



-- циклограмма проверки параметров питания радара
function TEST_MODULE_POWER()
  if (POWER == nil) then
    GUI.Log("Ошибка ИП!")
    GUI.Done(1)
    return 0
  end
  local i
  local Voltage = 0.0
  local P = 0.0
  local maxPower = 0.0
  local stepNum = 20

  --POWER.Connect()
  GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт циклограммы проверки параметров питания радара...")
  userReady = nil
  if GUI.Dialog("design/power.htm", "wait") ~= "OK" then
      GUI.Log("Проверка отменена!", "red")
      return "ABORT"
    end
	
  
  GUI.Log("Проверка началась")

  -- проверка доступа к ИП
  if (GetRadarPower() == nil) then
    GUI.Done(1)
    return 0
  end

  if (POWER.SetVoltage(55.0)) then
    if (POWER.SetCurrent(0.6)) then
      if(POWER.SetOutput(1)) then
        GUI.Log("Питание подано")
      end
    end
  end
  GUI.Log("Сбор измерений...")
  -- Цикл контроля питания
  for i = 1, stepNum do
    if (BREAK == 1) then
      GUI.Log("Проверку прервали!")
      break
    end
    if (power_test_error > 0) then
      GUI.Dialog("design/powerError.htm")
      break
    end
    -- рабочий диапазон напряжения (11..13),В
    P, I, V = GetRadarPower()
    if ((P)and(I)and(V)) then
      GUI.View("Напряжение, В", V)
      GUI.View("Ток, А", I)
      P = I * V
      GUI.View("Мощность, Вт", P)
      if (maxPower < P) then
        maxPower = P
      end
    end
    GUI.Progress(100* i / stepNum)
    pause(200*1000) -- 1c
  end
  if ((BREAK == 1)or(power_test_error > 0)) then
    GUI.View("Результат", "Прервана")
    GUI.Log("Проверка завершилась нештатно!!!", "Red")
  else
    GUI.View("Мощность(MAX), Вт", maxPower)
	powerTestResultPmax = maxPower
    --POWER.SetVoltage(0.0)
    --POWER.SetOutput(0)
    if ((maxPower < 20)and(maxPower > 10)) then
      GUI.View("Результат", "НОРМА")
      GUI.Log("НОРМА", "Green")
    else
      GUI.View("Результат", "НЕНОРМА")
      GUI.Log("НЕНОРМА", "Red")
    end
  end
  GUI.Done(1)
  GUI.Log("Конец")

end