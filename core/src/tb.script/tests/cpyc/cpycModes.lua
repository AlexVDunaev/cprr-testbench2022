
-- модуль установки режимов (профиль дальности)

PROFILE_PATH = "./config/cpyc/"
PROFILE_NEAR_FILE = "mimo_near.json"
PROFILE_FIELD_FILE = "mimo_field.json"
PROFILE_FAR_FILE = "mimo_far.json"

PROFILE_WORK_DIR = "./"

USE_POWER_SUPPLY_FOR_RESET = 0


cpycCanDriverLoadFlag = 0




function CPYC_RUN_CANDRV(dialogMode)
	if cpycCanDriverLoadFlag == 0 then
	  -- запуск драйвера радара
  	  if (dialogMode) then
		if GUI.Dialog("design/kvaser.htm", "wait") ~= "OK" then
		  log("Пользователь отказался от запуска драйвера CAN...")
		  return "ABORT"
		end
	  end
	  
	  log("Run radar CAN driver...")
	  PROFILE_WORK_DIR = GUI.RunAlg("CPYCCANDrv", "START", "wait")
	  pause(1000*1000) -- 1c
	  cpycCanDriverLoadFlag = 1
	end
return "OK"
end

--[[ Остановка драйвера при выходе из приложения! 
local function CPYC_STOP_CANDRV()
	if cpycCanDriverLoadFlag == 1 then
	  log("Close radar CAN driver...")
	  GUI.RunAlg("CPYCCANDrv", "DONE", "wait")
	  pause(1000*1000) -- 1c
	  cpycCanDriverLoadFlag = 0
	end
end
]]

function CPYC_GET_SN(dialogMode)
  if CPYC_RUN_CANDRV(dialogMode) ~= "OK" then
	return "000000000"
  end
  return GUI.RunAlg("CPYCCANDrv", "SN", "wait")
end


function CPYC_PAUSE(sec, info)
  local i
  if info then
	log(info)
  end
  for i = 1, sec do
	pause(1000*1000) -- 1c
	log(string.format("%d sec", i))
	if BREAK then -- глобальный 
		break
	end
  end
end


function CPYC_WRITE(cmd, value, dialogMode)
local result = "OK"
local cmdinfo = ""
  if CPYC_RUN_CANDRV(dialogMode) ~= "OK" then
	return "ABORT"
  end
  if cmd == "AUTOMODE" then
    cmd = "SPN1"
	cmdinfo = "[AUTOMODE]"
  end
  if cmd == "CONFIG ID" then
    cmd = "SPN999"
	cmdinfo = "[CONFIG ID]"
  end
  if cmd == "RESET" then
    cmd = "SPN2224"
	cmdinfo = "[RESET]"
  end
  if cmd == "START" then
    cmd = "SPN2221"
	if value == 1 then
		cmdinfo = "[START]"
	 else
		cmdinfo = "[STOP]"
	 end
  end
  if cmd == "SAVE" then
    cmd = "SPN2223"
	cmdinfo = "[SAVE]"
  end
  
  log(string.format("[j1939] %s <- %s %s", cmd, value, cmdinfo))
  GUI.RunAlg("CPYCCANDrv", "WRITE:"..cmd.."="..value, "wait") -- TX: [OFF/ON OFF/ON OFF/ON]
  pause(500*1000) -- 0.5c
  if cmd == "SPN2224" then
	CPYC_PAUSE(5, "Ждем 5с после программного сброса радара...")
  end
return "OK"
end


function CPYC_RESET(dialogMode)
  if USE_POWER_SUPPLY_FOR_RESET == 1 then
  -- пересброс радара и стримера через отключение ИП
	if SetRadarPower(0, dialogMode) == "OK" then
		pause(3000*1000) -- 3*1c
		if SetRadarPower(1, dialogMode) == "OK" then
			CPYC_PAUSE(5, "Ждем 5с после включения ИП...")
			GUI.Log("Питание подано")
			log("Сброс питания радара и стримера...")
			return "OK"
		end
	end
  else
	-- ресет только радара
	return CPYC_WRITE("RESET", 1, dialogMode)
  end
	
log("Ошибка сброса питания радара!")
return "ABORT"
end


function CPYC_GET_TARGETS(filter, dialogMode)
local result = {}
local resultIndex = 0
  
  if CPYC_RUN_CANDRV(dialogMode) ~= "OK" then
	return "ABORT"
  end
  
  -- параметры целей разделенные пробелом. по 7 полей на цель
  local targets = GUI.RunAlg("CPYCCANDrv", "TARGET:"..filter, "wait")
  if targets == "ERROR" then
	return nil
  end

  if #targets > 0 then
	  local v
	  local n = 0
	  local id, x, y, vx, vy, a, amp
	  for v in (targets.." "):gmatch("(.-) ") do
		  n = n + 1
		  if n == 1 then id = tonumber(v)
			elseif n == 2 then x = tonumber(v)
			elseif n == 3 then y = tonumber(v)
			elseif n == 4 then vx = tonumber(v)
			elseif n == 5 then vy = tonumber(v)
			elseif n == 6 then a = tonumber(v)
			elseif n == 7 then amp = tonumber(v)
			  n = 0
			  result[resultIndex] = {id = id, X = x, Y = y, Xv = vx, Yv = vy, A = a, AMP = amp}
			  resultIndex = resultIndex + 1
		  end
	  end
  end
  return resultIndex, result
end


-- Возврат: ["OK" - Норма / "ERROR" - Ошибка / "ABORT" - прерван]
-- mode = ["NEAR" | "FAR" | "FIELD"]
-- txcode = [0 .. 7]
function CPYC_SET_MODE(mode, txcode, dialogMode, calib)
local result = "OK"
local configId
  
  if CPYC_RUN_CANDRV(dialogMode) ~= "OK" then
	return "ABORT"
  end
  
  local filename = nil
  if mode == "FIELD" then
    filename = PROFILE_FIELD_FILE
	configId = 0
  end
  if mode == "NEAR" then
    filename = PROFILE_NEAR_FILE
	configId = 1
  end
  if mode == "FAR" then
    filename = PROFILE_FAR_FILE
	configId = 2
  end
  
  if filename then
    filename = PROFILE_PATH .. filename
    log(string.format("Send config file '%s' ... ", filename))
    GUI.RunAlg("CPYCCANDrv", "UPLOAD:"..filename, "wait")
	CPYC_PAUSE(2, "Ждем 2с для передачи файла конфигурации в радар")
  end
  
  if calib then
	filename = PROFILE_WORK_DIR .. "/calib_"..mode..".json"
    log(string.format("Send calib file '%s' ...", filename))
    GUI.RunAlg("CPYCCANDrv", "UPLOAD:"..filename, "wait")
	CPYC_PAUSE(2, "Ждем 2с для передачи файла калибровки в радар")
  end

  if txcode then
    local tx1 = "OFF"
    local tx2 = "OFF"
    local tx3 = "OFF"
	if (txcode & 1) > 0 then
		tx1 = "ON"
	end
	if (txcode & 2) > 0 then
		tx2 = "ON"
	end
	if (txcode & 4) > 0 then
		tx3 = "ON"
	end
	
	local textInfo = string.format("Настройка режима (TX:[%s %s %s])", tx1, tx2, tx3)
    GUI.Log(textInfo, "green")
    log(textInfo .. string.format(" (code: %d)", txcode))
    GUI.RunAlg("CPYCCANDrv", "WRITE:SPN1011="..txcode, "wait") -- TX: [OFF/ON OFF/ON OFF/ON]
    pause(1000*1000) -- 1c
  end
  -- automode 1 (streaming mimo)
  if CPYC_WRITE("AUTOMODE", 1, dialogMode) ~= "OK" then
	return "ABORT"
  end
  pause(1000*1000) -- 1c
  if configId then
	  -- config id  (default = FIELD = 0)
	  if CPYC_WRITE("CONFIG ID", configId, dialogMode) ~= "OK" then
		return "ABORT"
	  end
	  pause(1000*1000) -- 1c
  end
  log("Save config...")
  if CPYC_WRITE("SAVE", 1, dialogMode) ~= "OK" then
	return "ABORT"
  end
  pause(1000*1000) -- 1c

  GUI.Log("Перезапуск радара...", "green")
  if CPYC_RESET(dialogMode) ~= "OK" then
	return "ABORT"
  end

  return "OK"

end


-- TX, ModeInfoText
function CPYC_READ_MODE_CONFIG(dialogMode)
local modeInfo
  CPYC_RUN_CANDRV(dialogMode)
  log("Запрос конфигурации. Ожидание... (до 60 сек)")
  GUI.RunAlg("CPYCCANDrv", "WRITE:SPN60002=2", "wait") -- переход на быстрый режим выдачи параметров
  --------------------------------------------   1       2       3       4       5       6       7       8       9
  modeInfo = GUI.RunAlg("CPYCCANDrv", "CONFIG(SPN1011,SPN1000,SPN1001,SPN1002,SPN1003,SPN1004,SPN1006,SPN1008,SPN1009)", "wait", 600) -- 60сек
  if (modeInfo ~= "ERROR")and(modeInfo) then
	  local p = {}
	  local v
	  for v in (modeInfo.." "):gmatch("(.-) ") do
		table.insert(p, v);
	  end
	  local txMask
	  
	  if p[1] and p[2] and p[3] and p[4] and p[5] and p[6] and p[7] and p[8] and p[9] then
		local hpf1 = {['0']="175 KHz", ['1']= "235 KHz", ['2']="350 KHz", ['3']="700 KHz"}
		local hpf2 = {['0']="350 KHz", ['1']= "700 KHz", ['2']="1.4 MHz", ['3']="2.8 MHz"}
		
		modeInfo = string.format("%s ГГц, Idle: %s мкс, [Slop: %s МГц/мкс, %s мкс], ADC:[%s мкс, %s ksps], HPF1:%s, HPF2:%s", 
		 string.format("%.2f", tonumber(p[2])), -- SPN1000: startFreq
		 string.format("%.1f", tonumber(p[4])), -- SPN1002: idleTime
		 string.format("%.1f", tonumber(p[3])), -- SPN1001: freqSlope
		 string.format("%.1f", tonumber(p[6])), -- SPN1004: rampEnd
		 string.format("%.1f", tonumber(p[5])), -- SPN1003: adcStartTime
		 p[7], -- SPN1006: digOutSampleRate
		 hpf1[p[8]], -- SPN1008: HPF1
		 hpf2[p[9]]) -- SPN1009: HPF2
		 
		 txMask = tonumber(p[1])
		 
		 return txMask, modeInfo
	  end
  else
  log("Error read config!!!")
  end
  return nil
end


function TEST_CPYC_SET_MODE(dialogMode)
  local n
  for m = 1,10 do
		CPYC_SET_MODE("FAR", 7, dialogMode)
		CPYC_RESET(dialogMode)
		CPYC_PAUSE(10)
		CPYC_WRITE("START", 1, dialogMode)
		n = RADAR.SaveStream("test.bag", 5)
		CPYC_WRITE("START", 0, dialogMode)
		if n == 0 then
			log("ERROR!")
			GUI.Log("Ошибка! выход их проверки!")
			return "ERROR"
		end
		CPYC_PAUSE(5)
		CPYC_SET_MODE("NEAR", 7, dialogMode)
		CPYC_RESET(dialogMode)
		CPYC_PAUSE(10)
		CPYC_WRITE("START", 1, dialogMode)
		n = RADAR.SaveStream("test.bag", 5)
		CPYC_WRITE("START", 0, dialogMode)
		if n == 0 then
			log("ERROR!")
			GUI.Log("Ошибка! выход их проверки!")
			return "ERROR"
		end
		
	end
	
--CPYC_STOP_CANDRV() - Драйвер выгружается при закрытии GUI
end


function CPYC_SET_WORK_MODE(dialogMode)
	local filename
	------------- FIELD ---------------
    filename = "../"..REPORT_BASE_DIR_GUI .. "/" .. RADAR.SN .. "/calib_FIELD.json"
    log(string.format("Send config file '%s' ... ", filename))
    GUI.RunAlg("CPYCCANDrv", "UPLOAD:"..filename, "wait")
	CPYC_PAUSE(2, "Ждем 2с для передачи файла конфигурации в радар")
	------------- NEAR ---------------
    filename = "../"..REPORT_BASE_DIR_GUI .. "/" .. RADAR.SN .. "/calib_NEAR.json"
    log(string.format("Send config file '%s' ... ", filename))
    GUI.RunAlg("CPYCCANDrv", "UPLOAD:"..filename, "wait")
	CPYC_PAUSE(2, "Ждем 2с для передачи файла конфигурации в радар")
	------------- FAR ---------------
    filename = "../"..REPORT_BASE_DIR_GUI .. "/" .. RADAR.SN .. "/calib_FAR.json"
    log(string.format("Send config file '%s' ... ", filename))
    GUI.RunAlg("CPYCCANDrv", "UPLOAD:"..filename, "wait")
	CPYC_PAUSE(2, "Ждем 2с для передачи файла конфигурации в радар")
	-------------- TARGET MODE ------
    if CPYC_WRITE("AUTOMODE", 0, dialogMode) ~= "OK" then
	  return "ABORT"
    end
    pause(1000*1000) -- 1c
	-------------- FIELD MODE ------
    if CPYC_WRITE("CONFIG ID", 0, dialogMode) ~= "OK" then
	  return "ABORT"
	end
    pause(1000*1000) -- 1c
    log("Save config...")
    if CPYC_WRITE("SAVE", 1, dialogMode) ~= "OK" then
	  return "ABORT"
    end
    pause(1000*1000) -- 1c
	log("Записали калибровку и режим FIELD")
	GUI.Log("ОК. Записали калибровку и режим FIELD", "green")
	return "OK"
end
