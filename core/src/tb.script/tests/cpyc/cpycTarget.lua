--TEST_TARGET = {30, 60, 90, 120, 150} -- углы, на которых производим замеры.

TEST_TARGET = {} 
-- формируем список углов: начальное, конечное значение, шаг:
for i = 40, 140, 50 do
  TEST_TARGET[#TEST_TARGET + 1] = i
end


-- определим предельную ошибку между положением ОПУ и детектированием пика
TEST_TARGET_BASE_ANGLE  = 90 -- Начальный угол ОПУ
TEST_TARGET_ERROR_LIMIT = 25.50 -- м
TEST_TARGET_FRAME_ON_STEP = 20 -- кол-во кадров контроля на каждой угловой позиции
TEST_TARGET_LENGTH = 2.1 -- растояние от радара до уголка в БЭК

-- переменные модуля уровень шума в приёмном тракте
algCpycTarget = {workDirectory = nil,  -- директория алгоритма
  frameCount    = 0,    -- счетчик полученных кадров обработчиком стриминга 
  isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
  debugMode     = nil,  -- режим отладки. (не пишем в mmap)
  targetResult   = {},   -- словарь кадр: угол детектирования пика
  targetCheckErr = nil,  -- ошибка контроля положения пика
  errorText     = nil   -- сообщение об ошибке оператору
}

--DEBUG_READ_DATA_FILE = "./rf_rTarget.bag"

TARGET_SAVE_STREAM = 1 -- 0: позволит не писать новый файл, а вести обработку уже имеющегося



-- обработчика прерывания теста пользователем
local function onBreakRTarget()
  -- аварийное приведение в исх. состояние
  algCpycTarget.errorText = "Тест прерван"
  GUI.Log(algCpycTarget.errorText, "Red")
  algCpycTarget.isBreak = 1
end

-- Определим обработчик ошибки (что-то с ОПУ)
local function OPUFatalError(code)
  log(string.format("Ошибка ОПУ> %s", code))
  -- аварийное приведение в исх. состояние
  algCpycTarget.errorText = "ОПУ не отвечает!"
  GUI.Log(algCpycTarget.errorText, "Red")
  algCpycTarget.isBreak = 1
end


-- Обработчик события стриминга
local function userEventRTarget(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)

  -- Кол-во кадров, полученных в стрименге
  algCpycTarget.frameCount = algCpycTarget.frameCount + 1

  log(string.format("CPYCLineEvent1>[RX]: GNumber: %4d ", frameIndex)) 

  local a =  TEST_TARGET[algCpycTarget.frameCount + 1]
  if (a)and(OPU) then
    -- a == nil : последний кадр. Поворацивать ОПУ больше не нужно
    OPU.SetAzimuth(a)
  end
  -- пауза для исключения вибрации от ОПУ
  pause(1000*1000) -- 1c

  --userRunAlgRTarget(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)

  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algCpycTarget.frameCount / #TEST_TARGET))

  -- Проверка на прерывание теста
  if (algCpycTarget.isBreak) then
    return "BREAK" -- прекращаем стриминг
  end

end


function CPYC_CHECK_TARGET(x,y, num, tg)
 local i
 local dErr = {}
 local dX, dY, dR
 local dErrMinIndex
 local dErrMinValue = 111111
 
 for i = 0, num - 1 do
	dX = tg[i].X - x
	dY = tg[i].Y - y
	dR = math.sqrt(dX*dX + dY*dY)
	dErr[i] = dR
	if dErrMinValue > dR then
		dErrMinValue = dR
		dErrMinIndex = i
	end
 end
 
 if dErrMinIndex then
  dR = dErr[dErrMinIndex]
  if dR <= TEST_TARGET_ERROR_LIMIT then
	return dErrMinIndex, dR
  end
 end
return nil
end

-- циклограмма получения зависимости амплитуды сигнала цели от угла поворота
-- Возврат: ["OK" - Норма, "ERROR" - ненорма, "ABORT" - тест прерван]
function CPYC_TARGET_MODE(rZone, dialogMode)
  -- итоговый результат проверки (получаем от модуля алгоритма)
  -- ["OK" | "ERROR" | "ABORT"]
  local result    
  local i
  local abort = nil
  local modeInfo


  if (#TEST_TARGET <= 0) then
    GUI.Log("Точки (углы) контроля не определены!", "red")
    return "ABORT"
  end

  if (dialogMode) then
    if GUI.Dialog("design/rTarget.htm", "wait") ~= "OK" then
      GUI.Log("CPYC. Проверка отменена!", "red")
      return "ABORT"
    end
  end

  GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт проверки контроля положения отражателя...")

  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreakRTarget
  
  -- инициализация данных теста
  algCpycTarget.isBreak    = nil
  algCpycTarget.targetResult= {}
  algCpycTarget.errorText  = "[Не удалось получить сырые данные с радара]" -- ошибка по умолчанию
  algCpycTarget.frameCount = 0

  -- Штатный режим радара (Отметки)
  if CPYC_WRITE("SPN1", 0, dialogMode) ~= "OK" then
	return "ABORT"
  end
  pause(1000*1000) -- 1c
  log("Save config...")
  if CPYC_WRITE("SAVE", 1, dialogMode) ~= "OK" then
	return "ABORT"
  end
  pause(1000*1000) -- 1c

  GUI.Log("Перезапуск радара...", "green")
  if CPYC_RESET(dialogMode) ~= "OK" then
	return "ABORT"
  end
  CPYC_PAUSE(5)
  log("Start (target mode)...")
  if CPYC_WRITE("START", 1, dialogMode) ~= "OK" then
	return "ABORT"
  end


  local stepControl = 0
  for i = 1, #TEST_TARGET do
	local angle = TEST_TARGET[i] - TEST_TARGET_BASE_ANGLE
    local x = TEST_TARGET_LENGTH * math.sin(3.141592 * angle / 180)
    local y = TEST_TARGET_LENGTH * math.cos(3.141592 * angle / 180)
	  ------------
	  if algCpycTarget.isBreak == 1 then
		break
	  end
	  ------------
	  log(string.format("STEP: Angle: %d xy:[%.1f, %.1f]", angle, x, y)) 
  
	  local hit = 0
	  for k = 1, TEST_TARGET_FRAME_ON_STEP do
		  stepControl = stepControl + 1
		  if algCpycTarget.isBreak == 1 then
			break
		  end
		  local num, tg = CPYC_GET_TARGETS("ALL", dialogMode)
		  if num then
		  
			   local index, dR = CPYC_CHECK_TARGET(x, y, num, tg)
			   if index then
				hit = hit + 1
				log(string.format("MIN(%f)>>>>[%d] id:%d, XY[%.1f, %.1f], AMP:%.1f", dR, index, tg[index].id, tg[index].X, tg[index].Y, tg[index].AMP)) 
			   end
		  else
			   log("Timeout!")
			   break
		  end
	     GUI.Progress(math.floor(100 * stepControl / (#TEST_TARGET * TEST_TARGET_FRAME_ON_STEP)))
	  end
	  log(string.format("TG [HIT]: %d/%d", hit, TEST_TARGET_FRAME_ON_STEP)) 
	  --targetResult[i] = 
  end

  log("Stop...")
  if CPYC_WRITE("START", 0, dialogMode) ~= "OK" then
	return "ABORT"
  end
  
  return "OK"

end

--[[
			   for i = 0, num - 1 do
				--log(string.format("tg[%d]: id:%d, XY[%.1f, %.1f], AMP:%.1f", i, tg[i].id, tg[i].X, tg[i].Y, tg[i].AMP)) 
			   end


]]
