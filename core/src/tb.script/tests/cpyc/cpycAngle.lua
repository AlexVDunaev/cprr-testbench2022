--TEST_ANGLE = {30, 60, 90, 120, 150} -- углы, на которых производим замеры.

TEST_ANGLE = {} 
-- формируем список углов: начальное, конечное значение, шаг:
for i = 40, 140, 5 do
  TEST_ANGLE[#TEST_ANGLE + 1] = i
end

-- определим предельную ошибку между положением ОПУ и детектированием пика
TEST_ANGLE_ERROR_LIMIT = 10.0

-- переменные модуля уровень шума в приёмном тракте
algCpycAngle = {workDirectory = nil,  -- директория алгоритма
  frameCount    = 0,    -- счетчик полученных кадров обработчиком стриминга 
  mmapDrv       = nil,  -- драйвер MMAP через, который обмениваемся данными с модулем алгоритма
  alg           = nil,  -- таблица методов Алгоритма CPRR
  isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
  debugMode     = nil,  -- режим отладки. (не пишем в mmap)
  angleResult   = {},   -- словарь кадр: угол детектирования пика
  angleCheckErr = nil,  -- ошибка контроля положения пика
  errorText     = nil   -- сообщение об ошибке оператору
}

--DEBUG_READ_DATA_FILE = "./rf_rAngle.bag"

ANGLE_SAVE_STREAM = 1 -- 0: позволит не писать новый файл, а вести обработку уже имеющегося



-- обработчика прерывания теста пользователем
local function onBreakRAngle()
  -- аварийное приведение в исх. состояние
  algCpycAngle.errorText = "Тест прерван"
  GUI.Log(algCpycAngle.errorText, "Red")
  algCpycAngle.isBreak = 1
end

-- Определим обработчик ошибки (что-то с ОПУ)
local function OPUFatalError(code)
  log(string.format("Ошибка ОПУ> %s", code))
  -- аварийное приведение в исх. состояние
  algCpycAngle.errorText = "ОПУ не отвечает!"
  GUI.Log(algCpycAngle.errorText, "Red")
  algCpycAngle.isBreak = 1
end


-- Функция расчета кадра.
local function userRunAlgRAngle(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)

  -- Кол-во кадров, полученных в стрименге
  algCpycAngle.frameCount = algCpycAngle.frameCount + 1

  -- Инициализация драйвера mmap (Однократно)
  
  if ((algCpycAngle.mmapDrv == nil)and(algCpycAngle.workDirectory)and(algCpycAngle.debugMode == nil)) then
    algCpycAngle.mmapDrv = algCpycAngle.alg.mmapInit(algCpycAngle.workDirectory.."/refcpycAngle.bin", sizeOfData) -- init mmap file
  end


  -- Передача кадра данных в mmap и сигнал алгоритму, что нужно обработать данные кадра
  if (algCpycAngle.mmapDrv) then
    algCpycAngle.alg.mmapWrite(algCpycAngle.mmapDrv, ptrData)
    local angle
    -- Модуль алгоритма возвращает угол детектирования цели (уголка)
	local strAlgResult
    strAlgResult = GUI.RunAlg("CPYCAngle", "CALC", "wait")  -- "-23.1 1" Строка результата Угол и Дальность
	local AlgResult = {}
	for v in (strAlgResult.." "):gmatch("(.-) ") do
		table.insert(AlgResult, v);
		end
	if AlgResult[1] then
		angle = tonumber(AlgResult[1])
		end
	if AlgResult[2] then
		rangeIndex = tonumber(AlgResult[2])
		log("Дальность пика FFT: "..AlgResult[2])
		if rangeIndex < 4 then
			GUI.Log("Дальность пика FFT: "..AlgResult[2], "red")
			end
		end
		
    if (angle) then
      algCpycAngle.angleResult[algCpycAngle.frameCount] = -1*(angle - 90)
      if (math.abs(TEST_ANGLE[algCpycAngle.frameCount] - algCpycAngle.angleResult[algCpycAngle.frameCount]) > TEST_ANGLE_ERROR_LIMIT) then 
        algCpycAngle.angleCheckErr = 1
      end
    else
      -- Ошибка в алгоритме!  
      algCpycAngle.errorText = "Ошибка в модуле алгоритма!"
      algCpycAngle.isBreak = 1
    end
  end

  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algCpycAngle.frameCount / #TEST_ANGLE))

end

-- Обработчик события стриминга
local function userStreamEventRAngle(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)

  -- Кол-во кадров, полученных в стрименге
  algCpycAngle.frameCount = algCpycAngle.frameCount + 1

  log(string.format("CPYCLineEvent1>[RX]: GNumber: %4d ", frameIndex)) 

  local a =  TEST_ANGLE[algCpycAngle.frameCount + 1]
  if (a)and(OPU) then
    -- a == nil : последний кадр. Поворацивать ОПУ больше не нужно
    OPU.SetAzimuth(a)
  end
  -- пауза для исключения вибрации от ОПУ
  pause(1000*1000) -- 1c

  --userRunAlgRAngle(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)

  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algCpycAngle.frameCount / #TEST_ANGLE))

  -- Проверка на прерывание теста
  if (algCpycAngle.isBreak) then
    return "BREAK" -- прекращаем стриминг
  end

end


 
local function buildReport(result,  rZone, radarModel, modeInfo)
  local i
  REPORT.new(RadarSN, "Тест: Контроль положения отражателя")
  if (result == "ERROR") then
    REPORT.add("Результат выполнения теста: "..[[<b style="font-size:20px;color:red;">ОШИБКА</b>]], "h3")
  else
    REPORT.add("Результат выполнения теста: <b>НОРМА</b>", "h3")
  end
  if powerTestResultPmax then
	REPORT.add(string.format("Мощность(MAX): %.1f Вт", powerTestResultPmax))				
  end
  if powerTestResultOnPmax then
	REPORT.add(string.format("Потребление по питанию (ON): %.1f Вт", powerTestResultOnPmax))				
  end
  
  REPORT.add("Кол-во кадров (углов) контроля: "..string.format("%d", #TEST_ANGLE))
  if rZone == "NEAR" then
   REPORT.add("Режим работы радара: Ближняя зона")
   end
  if rZone == "FAR" then
   REPORT.add("Режим работы радара: Дальняя зона")
   end
  if rZone == "FIELD" then
   REPORT.add("Режим работы радара: Поле")
   end
  if modeInfo then
   REPORT.add("Параметры режима: " .. modeInfo)
   end
  
  for i = 1, #TEST_ANGLE do
    if ((algCpycAngle.angleResult[i])and(TEST_ANGLE[i])) then
      local errValue = math.abs(TEST_ANGLE[i] - algCpycAngle.angleResult[i])
      local extString = ""
      if (errValue > TEST_ANGLE_ERROR_LIMIT) then
        extString = [[<b style="font-size:16x;color:red;"> ОШИБКА</b>]]
      end
      -- в протоколе приводим все к централи: 90..-90 градусов
      REPORT.add(string.format("№%d. Угол ОПУ: %.1f&deg Угол детектирования пика: %.1f&deg, Разность : %.1f&deg", i, -1*(TEST_ANGLE[i] - 90), -1*(algCpycAngle.angleResult[i] - 90), errValue)..extString)
    end
  end
  REPORT.add("График результата измерений", "h3") -- формат заголовка

  if (CopyFile(algCpycAngle.workDirectory .. "/refAngleResult.png", getReportPath("refAngle"..rZone.."Result.png"))) then
    REPORT.add(nil, nil, [[<img src="refAngle]]..rZone..[[Result.png" alt="RefAngle">]])
  end

  REPORT.add("График амплитуды сигнала по приемным каналам радара в 0&deg (без калибровки)", "h3") -- формат заголовка

  if (CopyFile(algCpycAngle.workDirectory .. "/refAngle0Result.png", getReportPath("refAngle0"..rZone.."Result.png"))) then
    REPORT.add(nil, nil, [[<img src="refAngle0]]..rZone..[[Result.png" alt="RefAngle0">]])
  end

  REPORT.done(getReportPath("refAngle"..rZone..".html"), radarModel)
end


-- циклограмма получения зависимости амплитуды сигнала цели от угла поворота
-- Возврат: ["OK" - Норма, "ERROR" - ненорма, "ABORT" - тест прерван]
function TEST_CPYC_ANGLE(rZone, dialogMode)
  -- итоговый результат проверки (получаем от модуля алгоритма)
  -- ["OK" | "ERROR" | "ABORT"]
  local result    
  local i
  local abort = nil
  local modeInfo


  if (#TEST_ANGLE <= 0) then
    GUI.Log("Точки (углы) контроля не определены!", "red")
    return "ABORT"
  end

  if (dialogMode) then
    if GUI.Dialog("design/rAngle.htm", "wait") ~= "OK" then
      GUI.Log("CPYC. Проверка отменена!", "red")
      return "ABORT"
    end
  end

  GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт проверки контроля положения отражателя...")

  -- инициализация данных теста
  algCpycAngle.alg        = Algorithm_Init()
  algCpycAngle.isBreak    = nil
  algCpycAngle.mmapDrv    = nil
  algCpycAngle.angleResult= {}
  algCpycAngle.errorText  = "[Не удалось получить сырые данные с радара]" -- ошибка по умолчанию
  algCpycAngle.frameCount = 0

  -- инициализация алгоритма (очистка буферов данных) Возврат: путь к файлам
  algCpycAngle.workDirectory = GUI.RunAlg("CPYCAngle", "START:"..rZone, "wait")
  --------------
  if (algCpycAngle.workDirectory == nil) then
    GUI.Log("Ошибка. Директория назначения не определена")
    return "ABORT"
  end


  local configResult = nil
  local readConfig = 1 --1 -- nil

  if readConfig then
	  -- запуск драйвера радара
	local modetx
	modeInfo = "none"
	-- Удалить файл конфигурации радара (если есть)
	GUI.RunAlg("CPYCAngle", "CLEAR", "wait")
	-- Запрос файла конфигурации
	modetx, modeInfo = CPYC_READ_MODE_CONFIG(dialogMode)
	
	if modetx == nil then
		if (dialogMode) then
		  GUI.Dialog("design/rAngleErr.htm", "wait", "Ошибка чтения конфигурации радара!")
		end
	    GUI.Log("Ошибка! не удалось считать статус радара!", "red")
	    return "ABORT"
    end
	  
    -- применить калибровку
    configResult = GUI.RunAlg("CPYCAngle", "CONFIG", "wait")
    if configResult ~= "OK" then
      if (dialogMode) then
	    GUI.Dialog("design/rAngleErr.htm", "wait", "Ошибка чтения файла конфигурации!")
	  end
	  GUI.Log("Ошибка чтения конфигурации радара!", "red")
	  return "ABORT"
    end

  -- глобальная переменная: Заводской номер
  RadarSN = GUI.RunAlg("CPYCAngle", "GET_SN", "wait")
  RADAR.SN = RadarSN
  end

  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreakRAngle

  -- установка ОПУ в начальное положение
  if (OPU) then
    OPU.OnError = OPUFatalError
    OPU.SetAzimuth(TEST_ANGLE[1])
    -- пауза для исключения вибрации от ОПУ
    pause(1000*1000) -- 1c
  end

  bagFileName = algCpycAngle.workDirectory .. "/refcpycAngle_".. rZone .. ".bag"


  if ANGLE_SAVE_STREAM == 1 then
    -- Штатный код: 
    -- Событие получения кадра. В нем будем вызывать алгоритм и управлять ОПУ
    RADAR.StreamEvent = userStreamEventRAngle
    -- запуск стрима на заданное число кадров
    RADAR.SaveStream(bagFileName, #TEST_ANGLE)
	
    if (algCpycAngle.frameCount < #TEST_ANGLE) then 
      -- Что-то пошло не так... не то количество кадров...
      if (dialogMode) then
        GUI.Dialog("design/rAngleErr.htm", "wait", algCpycAngle.errorText)
      end
      GUI.Log("Ошибка. Не получены сырые данные с радара или стримера", "red")
      return "ABORT"
    end
  end

  
    -- снова очистим счетчик кадров 
    algCpycAngle.frameCount = 0
  
	local frameSize = 2*2*256*4*128
	local index = 0
	while (1==1) do
		bag, info = algCpycAngle.alg.getFrame(bagFileName, index, 1, frameSize)
		if bag then
			frame = algCpycAngle.alg.getField(bag, 0, frameSize).mem
			
			log(string.format("angle[%d]:%d ", index+1, TEST_ANGLE[index + 1]))
			userRunAlgRAngle(0, info["SN"], info["time"], index, frame, frameSize)
			
			-- в нулевом положении нужно отобразить график распределения ампл. по каналам
			if TEST_ANGLE[index + 1] == 90 then
				GUI.RunAlg("CPYCAngle", "ZERO", "wait")
			end
		else
			log(string.format("[%s] -> bag == NULL Index:%d", bagFileName, index))
			break
		end
		index = index + 1
		if (index == #TEST_ANGLE) then --#TEST_ANGLE
			break
		end
		
		if (index == 11) then --#TEST_ANGLE
			log("!!!!!!!!!!!!!!!!!!!!!debug!!!!!!!!!!!!!!!!!!!!!!!!!!")
			--break
		end
	end
	log(string.format("---------------> READ DONE"))
	

  if (OPU) then
    -- установка ОПУ в центральное положение
    OPU.SetAzimuth(90)
    OPU.OnError = nil
  end

  RADAR.StreamEvent = nil
  GUI.OnBreak = nil

  result = "OK"

  if (algCpycAngle.frameCount < #TEST_ANGLE) then 
    -- Что-то пошло не так... не то количество кадров...
    if (dialogMode) then
      GUI.Dialog("design/rAngleErr.htm", "wait", "[Не удалось обработать сырые данные с радара]")
    end
    GUI.Log("Ошибка. Не удалось обработать сырые данные с радара", "red")
    return "ABORT"
  end
  -- Завершим алгоритм. Построение графика, получим итоговый результат
  -- финальный расчет занимает < 1 с
  result = GUI.RunAlg("CPYCAngle", "DONE", "wait")

  if (algCpycAngle.angleCheckErr) then
    -- ошибка контроля положения пика!
    result = "ERROR"
  end

  -- создать папку для отчетов
  CreateDir(getReportPath())
  -- Формирование отчета:
  buildReport(result, rZone, "CPYC", modeInfo)

  local dialogText = ""
  if (result == "ERROR") then
    dialogText = ":НЕНОРМА!"
    GUI.Log("Проверка 'Контроль положения отражателя'. Результат: НЕНОРМА!", "red")
  else
    GUI.Log("Проверка 'Контроль положения отражателя'. Значения в норме!", "green")
  end


  GUI.Log("Проверка завершилась, ждем реакции пользователя...")
  if (dialogMode) then
    GUI.Dialog("design/rAngleDone.htm"..dialogText, "wait")
  end
  return result

end