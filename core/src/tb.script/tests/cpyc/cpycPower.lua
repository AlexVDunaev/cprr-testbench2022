
-- переменные модуля контроля питания (потребления)
algCpycPower = {workDirectory = nil,  -- директория алгоритма
  isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
  errorText     = nil   -- сообщение об ошибке оператору
}

POWER_CONTROL_TIME = 10 -- секунд

powerTestResultOnPmax = nil
powerTestResultOffPmax = nil

if (power_test_error == nil) then
  power_test_error = 0
end

-- обработчика прерывания теста пользователем
local function onBreakPower()
  -- аварийное приведение в исх. состояние
  algCpycPower.errorText = "Тест прерван"
  GUI.Log(algCpycPower.errorText, "Red")
  algCpycPower.isBreak = 1
end


local function runPowerControl(dialogMode)
  local i
  local V
  local I
  local P
  local maxPower = 0

  for i = 1, POWER_CONTROL_TIME do
    -- Проверка на прерывание теста
	if (algCpycPower.isBreak) then
      GUI.Log("Проверку прервали!")
      return nil
    end
	
    if (power_test_error > 0) then
	  if (dialogMode) then
		GUI.Dialog("design/powerError.htm")
	  end
      return nil
    end
    -- рабочий диапазон напряжения (11..13),В
    P, I, V = GetRadarPower()
    if ((P)and(I)and(V)) then
      GUI.View("Напряжение, В", V)
      GUI.View("Ток, А", I)
      P = I * V
      GUI.View("Мощность, Вт", P)
      if (maxPower < P) then
        maxPower = P
      end
    end
    GUI.Progress(100* i / POWER_CONTROL_TIME)
    pause(1000*1000) -- 1c
  end
  return maxPower
end


-- циклограмма проверки параметров питания радара
function TEST_CPYC_POWER(dialogMode)
  local powerValueTX_ON  -- потребление радара при вкл. излучения
  local powerValueTX_OFF -- потребление радара на холостом ходу
  
  if (POWER == nil) then
    GUI.Log("Ошибка ИП!")
    GUI.Done(1)
    return "ABORT"
  end
  
  GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт циклограммы проверки параметров питания радара...")

  if (dialogMode) then
    if GUI.Dialog("design/power.htm", "wait") ~= "OK" then
       GUI.Log("Проверка отменена!", "red")
       return "ABORT"
    end
  end
  
  GUI.Log("Проверка началась")

  -- проверка доступа к ИП
  if (GetRadarPower() == nil) then
    GUI.Done(1)
    return "ABORT"
  end

  if (POWER.SetVoltage(55.0)) then
    if (POWER.SetCurrent(0.6)) then
      if(POWER.SetOutput(1)) then
        GUI.Log("Питание подано")
      end
    end
  end
  
  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreakPower
  
  GUI.Log("Сбор измерений...")
  -- Цикл контроля питания без включения ЛЧМ
  powerValueTX_OFF = runPowerControl()
  if powerValueTX_OFF then
	-- Включаем передатчик
	CPYC_WRITE("START", 1)
	powerValueTX_ON = runPowerControl()
	-- Отключаем передатчик
	CPYC_WRITE("START", 0)
  end
  
  
  if ((powerValueTX_ON == nil)or(powerValueTX_OFF == nil)or(power_test_error > 0)) then
    GUI.View("Результат", "Ошибка")
    GUI.Log("Проверка завершилась нештатно!!!", "Red")
  else
    GUI.View("Мощность(OFF), Вт", powerValueTX_OFF)
    GUI.View("Мощность(ON), Вт", powerValueTX_ON)
	powerTestResultOnPmax = powerValueTX_ON
	powerTestResultOffPmax = powerValueTX_OFF
    if ((powerValueTX_ON < 15)and(powerValueTX_ON > 5)and(powerValueTX_OFF < 15)and(powerValueTX_OFF > 5)) then
      GUI.View("Результат", "НОРМА")
      GUI.Log("НОРМА", "Green")
    else
      GUI.View("Результат", "НЕНОРМА")
      GUI.Log("НЕНОРМА", "Red")
    end
  end
  GUI.OnBreak = nil
  GUI.Done(1)
  GUI.Log("Конец")

end