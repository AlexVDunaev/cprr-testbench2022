
-- переменные модуля уровень шума в приёмном тракте
algCpycNoise = {workDirectory = nil,  -- директория алгоритма
  frameCount    = 0,    -- счетчик полученных кадров обработчиком стриминга 
  mmapDrv       = nil,  -- драйвер MMAP через, который обмениваемся данными с модулем алгоритма
  alg           = nil,  -- таблица методов Алгоритма CPRR
  isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
  debugMode     = nil,  -- режим отладки. (не пишем в mmap)
  errorText     = nil   -- сообщение об ошибке оператору
}

NOISE_FRAME_NUM = 200
NOISE_DATA_FILE = "noise"
NOISE_SAVE_STREAM = 1 -- 0: позволит не писать новый файл, а вести обработку уже имеющегося
NOISE_CALC_FILE = 1 -- расчет данных

powerTestResultPNoise = nil

-- обработчика прерывания теста пользователем
local function onBreakNoise()
  -- аварийное приведение в исх. состояние
  algCpycNoise.errorText = "Тест прерван"
  GUI.Log(algCpycNoise.errorText, "Red")
  algCpycNoise.isBreak = 1
  BREAK = 1
  log("Тест прерван!!!")
end



local function buildReport(result, rZone, radarModel, modeInfo)
  local i
  
  REPORT.new(RadarSN, "Тест: уровень шума в приёмном тракте")
  if (result == "ERROR") then
    REPORT.add("Результат выполнения теста: "..[[<b style="font-size:20px;color:red;">ОШИБКА</b>]], "h3")
   else
    REPORT.add("Результат выполнения теста: <b>НОРМА</b>", "h3")
  end
  if powerTestResultPNoise then
	REPORT.add(string.format("Потребление по питанию: %.1f Вт", powerTestResultPNoise))				
  end
  if powerTestResultOnPmax then
	REPORT.add(string.format("Потребление по питанию (ON): %.1f Вт", powerTestResultOnPmax))				
  end
  REPORT.add("Кол-во кадров контроля: "..string.format("%d", NOISE_FRAME_NUM))
  if rZone == "NEAR" then
   REPORT.add("Режим работы радара: Ближняя зона")
   end
  if rZone == "FAR" then
   REPORT.add("Режим работы радара: Дальняя зона")
   end
  if rZone == "FIELD" then
   REPORT.add("Режим работы радара: Поле")
   end
  if modeInfo then
   REPORT.add("Параметры режима: " .. modeInfo)
   end
   
   
  
  REPORT.add("График результата измерений", "h3") -- формат заголовка

  if (CopyFile(algCpycNoise.workDirectory .. "/rfnoiseResult.png", getReportPath("rfnoiseResult"..rZone..".png"))) then
    REPORT.add(nil, nil, [[<img src="rfnoiseResult]]..rZone..[[.png" alt="RF GEN ALL MAX">]])
  end

  REPORT.done(getReportPath("rfnoise"..rZone..".html"), radarModel)

end


-- Функция расчета кадра.
local function userRunAlgNoise(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)

  RadarSN = serialNum
  -- Кол-во кадров, полученных в стрименге
  algCpycNoise.frameCount = algCpycNoise.frameCount + 1

  -- Инициализация драйвера mmap (Однократно)
  if ((algCpycNoise.mmapDrv == nil)and(algCpycNoise.workDirectory)and(algCpycNoise.debugMode == nil)) then
    algCpycNoise.mmapDrv = algCpycNoise.alg.mmapInit(algCpycNoise.workDirectory.."/noise.bin", sizeOfData) -- init mmap file
  end

  -- Передача кадра данных в mmap и сигнал алгоритму, что нужно обработать данные кадра
  if (algCpycNoise.mmapDrv) then
    algCpycNoise.alg.mmapWrite(algCpycNoise.mmapDrv, ptrData)

    GUI.RunAlg("CPYCNOISE", "CALC", "wait")  -- "OK" 
  end
  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algCpycNoise.frameCount / NOISE_FRAME_NUM))

end

-- Обработчик события стриминга
local function userStreamEventNoise(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)

  -- Кол-во кадров, полученных в стрименге
  algCpycNoise.frameCount = algCpycNoise.frameCount + 1

  log(string.format("CPYCLineEvent1>[RX]: GNumber: %4d ", frameIndex)) 

  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algCpycNoise.frameCount / NOISE_FRAME_NUM))

  -- Проверка на прерывание теста
  if (algCpycNoise.isBreak) then
    return "BREAK" -- прекращаем стриминг
  end
end


-- циклограмма получения калибровки
-- Возврат: ["OK" - Норма / "ERROR" - Ошибка / "ABORT" - тест прерван]
function TEST_CPYC_NOISE(rZone, dialogMode)
local result = "OK"
local modeInfo

  GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт проверки уровня шума : "..rZone)
  --
  -- инициализация данных теста
  algCpycNoise.alg        = Algorithm_Init()
  algCpycNoise.isBreak    = nil
  algCpycNoise.mmapDrv    = nil
  algCpycNoise.errorText  = "[Не удалось получить сырые данные с радара]" -- ошибка по умолчанию
  algCpycNoise.frameCount = 0

  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreakNoise

  if SetRadarPower(1) == "OK" then
	CPYC_PAUSE(5)
	P, I, V = GetRadarPower()
	powerTestResultPNoise = P
  end

  -- инициализация алгоритма (очистка буферов данных) Возврат: путь к файлам
  algCpycNoise.workDirectory = GUI.RunAlg("CPYCNOISE", "START", "wait")
  --------------
  if (algCpycNoise.workDirectory == nil) then
    GUI.Log("Ошибка. Директория назначения не определена")
    return "ABORT"
  end

  --------------------------------------------
  --Настройка радара на режим без излучения --
  --------------------------------------------

  local radarReady = nil
  local setTxOFF = 1 --nil

  if setTxOFF then
  
    -- запуск драйвера радара
	if CPYC_RUN_CANDRV(dialogMode) == "ABORT" then
	  GUI.Log("Проверка отменена!", "red")
	  return "ABORT"
	  end
	log("Set radar mode: "..rZone)
    CPYC_SET_MODE(rZone, 0, dialogMode) -- Перезапуск радара
	  
	if (algCpycNoise.isBreak) then
	  return "ABORT" -- прекращаем обсчет
	end

	  log("Wait for reset....")
	  CPYC_PAUSE(10)
	  
      GUI.Log("запрос конфигурации...", "green")
	  
	  local txMask
	  
	  txMask, modeInfo = CPYC_READ_MODE_CONFIG()
	  
	  if txMask == nil then
		GUI.Log("Ошибка запроса конфигурации!", "red")
		return "ABORT"
	  end
	  
	  if txMask ~= 0 then
	    GUI.Log("Ошибка! Каналы включены!", "green")
	  else
	    radarReady = 1
	  end
	  
	  if radarReady == nil then
		if (dialogMode) then
          GUI.Dialog("design/commonError.htm", "wait", "Не удалось установить нужный режим радара")
		end
		GUI.Log("Ошибка! Не удалось установить нужный режим радара!", "red")
		return "ABORT"
      end
	  
  end
  -----------------------------------------
  -----------------------------------------
  -----------------------------------------

  local bagFileName = algCpycNoise.workDirectory .. "/" .. NOISE_DATA_FILE.."_"..rZone..".bag"

  if NOISE_SAVE_STREAM == 1 then 
	  RADAR.StreamEvent = userStreamEventNoise
	  -- запуск стрима на заданное число кадров
	  RADAR.SaveStream(bagFileName, NOISE_FRAME_NUM)

	  RADAR.StreamEvent = nil
		
	  if (algCpycNoise.frameCount < NOISE_FRAME_NUM) then 
		  -- Что-то пошло не так... не то количество кадров...
		  if (dialogMode) then
			GUI.Dialog("design/commonError.htm", "wait", algCpycNoise.errorText)
		  end
		  GUI.Log("Ошибка. Не получены сырые данные с радара или стримера", "red")
		  return "ABORT"
	  end
  end

  
  if NOISE_CALC_FILE == 1 then
	  -- снова очистим счетчик кадров 
	  algCpycNoise.frameCount = 0
	  
	  local frameSize = 2*2*256*4*128
	  local index = 0
	  while (1==1) do
		bag, info = algCpycNoise.alg.getFrame(bagFileName, index, 1, frameSize)
		if bag then
			frame = algCpycNoise.alg.getField(bag, 0, frameSize).mem
			userRunAlgNoise(0, info["SN"], info["time"], index, frame, frameSize)
		else
			log(string.format("[%s] EOF (Index:%d)", bagFileName, index))
			break
		end
		index = index + 1
		if (index == NOISE_FRAME_NUM) then
			break
		end
		  -- Проверка на прерывание теста
		if (algCpycNoise.isBreak) then
			return "ABORT" -- прекращаем обсчет
		end
	  end
	  log(string.format("READ DONE"))
	  
	  if (algCpycNoise.mmapDrv) then
		-- Закроем драйвер MMAP
		algCpycNoise.alg.mmapClose(algCpycNoise.mmapDrv)
	  end
	  
	  if (algCpycNoise.frameCount < NOISE_FRAME_NUM) then 
		-- Что-то пошло не так... не то количество кадров...
		if (dialogMode) then
		  GUI.Dialog("design/commonError.htm", "wait", "[Не удалось обработать сырые данные с радара]")
		end
		GUI.Log("Ошибка. Не удалось обработать сырые данные с радара", "red")
		return "ABORT"
	  end
	  -- Завершим алгоритм. Результат: файл калибровки
	  result = GUI.RunAlg("CPYCNOISE", "DONE", "wait")
	  
	  -- создать папку для отчетов
	  CreateDir(getReportPath())
	  
	-- Формирование отчета:
	  buildReport(result, rZone, "CPYC", modeInfo)
	  
	  local dialogText = ":("..rZone..")"
	  if (result == "ERROR") then
		dialogText = dialogText.." НЕНОРМА!"
		GUI.Log("Проверка 'уровень шума в приёмном тракте'. Результат: НЕНОРМА!", "red")
	  else
		GUI.Log("Проверка 'уровень шума в приёмном тракте'. Значения в норме!", "green")
	  end
		
	  GUI.Log("Проверка завершилась, ждем реакции пользователя...")
	  if (dialogMode) then
		  GUI.Dialog("design/rfnoiseDone.htm"..dialogText, "wait")
	  end
  end

  GUI.OnBreak = nil


  if setTxOFF then
	  -- Восстановим нормальный режим радара! Иначе остальные проверки не пройдут!
	  --CPYC_SET_MODE("NONE", 7, dialogMode) -- Перезапуск радара
  end


  GUI.Log("Проверка завершилась!")
  return result

end