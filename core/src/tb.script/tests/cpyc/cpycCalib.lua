
-- переменные модуля уровень шума в приёмном тракте
algCpycCalib = {workDirectory = nil,  -- директория алгоритма
  frameCount    = 0,    -- счетчик полученных кадров обработчиком стриминга 
  mmapDrv       = nil,  -- драйвер MMAP через, который обмениваемся данными с модулем алгоритма
  alg           = nil,  -- таблица методов Алгоритма CPRR
  isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
  debugMode     = nil,  -- режим отладки. (не пишем в mmap)
  errorText     = nil   -- сообщение об ошибке оператору
}

CALIB_FRAME_NUM = 200
CALIB_DATA_FILE = "calib"
CALIB_CALC_VECTOR = 1 -- 0: позволит отключить расчет калиб. вектора


-- обработчика прерывания теста пользователем
local function onBreakCalib()
  -- аварийное приведение в исх. состояние
  algCpycCalib.errorText = "Тест прерван"
  GUI.Log(algCpycCalib.errorText, "Red")
  algCpycCalib.isBreak = 1
  BREAK = 1
  log("Тест прерван!!!")
end


-- Функция расчета кадра.
local function userRunAlgCalib(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)

  -- Кол-во кадров, полученных в стрименге
  algCpycCalib.frameCount = algCpycCalib.frameCount + 1

  -- Инициализация драйвера mmap (Однократно)
  
  if ((algCpycCalib.mmapDrv == nil)and(algCpycCalib.workDirectory)and(algCpycCalib.debugMode == nil)) then
    algCpycCalib.mmapDrv = algCpycCalib.alg.mmapInit(algCpycCalib.workDirectory.."/calib.bin", sizeOfData) -- init mmap file
  end


  -- Передача кадра данных в mmap и сигнал алгоритму, что нужно обработать данные кадра
  if (algCpycCalib.mmapDrv) then
    algCpycCalib.alg.mmapWrite(algCpycCalib.mmapDrv, ptrData)
    local angle
    -- Модуль алгоритма возвращает угол детектирования цели (уголка)
	local strAlgResult
    strAlgResult = GUI.RunAlg("CPYCCALIB", "CALC", "wait")  -- "3.1 33" Строка результата Угол и Дальность
	local AlgResult = {}
	for v in (strAlgResult.." "):gmatch("(.-) ") do
		table.insert(AlgResult, v);
		end
	if AlgResult[1] then
		angle = tonumber(AlgResult[1])
		end
	if AlgResult[2] then
		rangeIndex = tonumber(AlgResult[2])
		--log("Дальность уголка (FFT bin): "..AlgResult[2].." Фаза:"..AlgResult[1])
		end
  end

  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algCpycCalib.frameCount / CALIB_FRAME_NUM))

end

-- Обработчик события стриминга
local function userStreamEventCalib(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)

  -- Кол-во кадров, полученных в стрименге
  algCpycCalib.frameCount = algCpycCalib.frameCount + 1

  --log(string.format("CPYCLineEvent1>[RX]: GNumber: %4d ", frameIndex)) 

  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algCpycCalib.frameCount / CALIB_FRAME_NUM))

  -- Проверка на прерывание теста
  if (algCpycCalib.isBreak) then
    return "BREAK" -- прекращаем стриминг
  end
end


-- циклограмма получения калибровки
-- Возврат: ["OK" - Норма / "ABORT" - тест прерван]
function TEST_CPYC_CALIB(rZone, dialogMode)

  GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт калибровки...")

  -- инициализация данных теста
  algCpycCalib.alg        = Algorithm_Init()
  algCpycCalib.isBreak    = nil
  algCpycCalib.mmapDrv    = nil
  algCpycCalib.errorText  = "[Не удалось получить сырые данные с радара]" -- ошибка по умолчанию
  algCpycCalib.frameCount = 0

  -- инициализация алгоритма (очистка буферов данных) Возврат: путь к файлам
  algCpycCalib.workDirectory = GUI.RunAlg("CPYCCALIB", "START", "wait")
  --------------
  if (algCpycCalib.workDirectory == nil)or(algCpycCalib.workDirectory == "ERROR") then
    GUI.Log("Ошибка. Директория назначения не определена")
    return "ABORT"
  end

  log("Work dir: " .. algCpycCalib.workDirectory)

  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreakCalib

  -- local bagFileName = algCpycCalib.workDirectory .. "/"..CALIB_DATA_FILE .."_"..rZone..".bag"

  RADAR.SN = CPYC_GET_SN()
  local folder = string.format("%s/%s", BAG_SAVE_FOLDER, RADAR.SN)
  local bagFileName = folder .. "/"..CALIB_DATA_FILE .."_"..rZone..".bag"
  CreateDir(folder)


  RADAR.StreamEvent = userStreamEventCalib
  RADAR.StreamerUpEvent = function ()
    -- Включаем радар только по готовности стримера
    log("STREAMER UP. START RADAR!")
	CPYC_WRITE("START", 1, dialogMode)
	end
  -- запуск стрима на заданное число кадров
  RADAR.SaveStream(bagFileName, CALIB_FRAME_NUM)

  RADAR.StreamEvent = nil
  RADAR.StreamerUpEvent = nil
  
  -- Отлючаем радар
  CPYC_WRITE("START", 0, dialogMode)  
	
  if (algCpycCalib.frameCount < CALIB_FRAME_NUM) then 
      -- Что-то пошло не так... не то количество кадров...
      if (dialogMode) then
        GUI.Dialog("design/commonError.htm", "wait", algCpycCalib.errorText)
      end
      GUI.Log("Ошибка. Не получены сырые данные с радара или стримера", "red")
      return "ABORT"
  end

  local SN_By_BagFile
  if CALIB_CALC_VECTOR == 1 then
	  -- снова очистим счетчик кадров 
	  algCpycCalib.frameCount = 0
	  
	  local frameSize = 2*2*256*4*128
	  local index = 0
	  while (1==1) do
		bag, info = algCpycCalib.alg.getFrame(bagFileName, index, 1, frameSize)
		if bag then
			SN_By_BagFile = info["SN"]
			frame = algCpycCalib.alg.getField(bag, 0, frameSize).mem
			userRunAlgCalib(0, info["SN"], info["time"], index, frame, frameSize)
		else
			log(string.format("[%s] EOF (Index:%d)", bagFileName, index))
			break
		end
		index = index + 1
		if (index == CALIB_FRAME_NUM) then
			break
		end
		  -- Проверка на прерывание теста
		if (algCpycCalib.isBreak) then
			return "ABORT" -- прекращаем обсчет
		end
	  end
	  log(string.format("READ DONE"))
	  
	  if (algCpycCalib.mmapDrv) then
		-- Закроем драйвер MMAP
		algCpycCalib.alg.mmapClose(algCpycCalib.mmapDrv)
	  end
	  
	  if (algCpycCalib.frameCount < CALIB_FRAME_NUM) then 
		-- Что-то пошло не так... не то количество кадров...
		if (dialogMode) then
		  GUI.Dialog("design/commonError.htm", "wait", "[Не удалось обработать сырые данные с радара]")
		end
		GUI.Log("Ошибка. Не удалось обработать сырые данные с радара", "red")
		return "ABORT"
	  end
	  -- Завершим алгоритм. Результат: файл калибровки
	  local filename = ""
	  if rZone then
		if SN_By_BagFile then
			RADAR.SN = string.format("%.9d", SN_By_BagFile)
		end
		CreateDir(REPORT_BASE_DIR .. "/" .. RADAR.SN)
		local exportDirName = REPORT_BASE_DIR_GUI .. "/" .. RADAR.SN
	    filename = ":" .. exportDirName .. "/calib_"..rZone..".json"
		
		log(string.format("calib export%s", filename))
	  end
	  GUI.RunAlg("CPYCCALIB", "DONE"..filename, "wait")
  end

  GUI.OnBreak = nil

  -- создать папку для отчетов
  -- CreateDir(getReportPath())
  GUI.Log("Калибровка завершилась!")
  return "OK"

end


function TEST_CPYC_CALIB_MODE(rZone, dialogMode)
  GUI.Log("Старт калибровки для режима..."..rZone)
  log("Set radar mode: "..rZone)
  if CPYC_SET_MODE(rZone, nil, dialogMode) ~= "OK" then
	return "ABORT"
	end
  -- CPYC_PAUSE(10) -- ожидание перезапуска
  local modeInfo
  local modetx
  
  modetx, modeInfo = CPYC_READ_MODE_CONFIG()
  if modeInfo then
   log("Режим: "..rZone)
   end
   local result
  
  result = TEST_CPYC_CALIB(rZone, dialogMode)
	
  return result
end

