dofile("./tb.script/drivers/CPRR/cprr24.lua") -- РАДАР CPRR24



-- проверка EEPROM
function EEPROM_LUA_START(dialogMode)
-- cmdIP = "192.168.1.4"
--cmdPort = 7000

--GUI.Log("EEPROM TEST---------------------------..."..cmdIP)
--  GUI.Clear()
  GUI.Progress(0)
  GUI.Log("EEPROM TEST---------------------------...")
  
  strAlgResult = GUI.RunAlg("EEPROM_TEST", "CALC", "wait", 600) -- таймаут задаётся в 0.1 секунда
  outstr = string.format("%s", strAlgResult)
  print(strAlgResult, outstr)
  if (strAlgResult ~= nil) then
    GUI.Log(strAlgResult, "Green")
  else
    GUI.Log("Return nil", "Red")
  end
  
  --GUI.Done(1)
  --GUI.Log("Конец")
end

-- стирание EEPROM
function EEPROM_LUA_ERASE(dialogMode)

  --GUI.Clear()
  GUI.Progress(0)
  GUI.Log("-------------- Telnet erase EEPROM  --------------")
  
  strAlgResult = GUI.RunAlg("EEPROM_ERASE", "CALC", "wait", 600); -- таймаут задаётся в 0.1 секунда

  if (strAlgResult ~= nil) then
    GUI.Log(strAlgResult, "Green")
  else
    GUI.Log("Return nil", "Red")
  end
  
  
  GUI.Done(1)
  GUI.Log("Конец")

end

-- чтение EEPROM
function EEPROM_LUA_READ(dialogMode)

  --GUI.Clear()
  GUI.Progress(0)
  GUI.Log("-------------- Telnet read EEPROM  --------------") 
  
  strAlgResult = GUI.RunAlg("EEPROM_READ", "CALC", "wait", 300); -- таймаут задаётся в 0.1 секунда

  if (strAlgResult ~= nil) then
    GUI.Log(strAlgResult, "Green")
  else
    GUI.Log("Return nil", "Red")
  end
  
  
  GUI.Done(1)
  GUI.Log("Конец")

end

-- Запись в EEPROM штатных настроек пролаза и калибровки
function EEPROM_LUA_EEPROMWRITELEAK(dialogMode)

  --GUI.Clear()
  GUI.Progress(0)
  GUI.Log("-------------- Write leak and calibration default  --------------")
  
  strAlgResult = GUI.RunAlg("EEPROM_WRITELEAK", "CALC", "wait", 600); -- таймаут задаётся в 0.1 секунда

  if (strAlgResult ~= nil) then
    GUI.Log(strAlgResult, "Green")
  else
    GUI.Log("Return nil", "Red")
  end
  
  
  GUI.Done(1)
  GUI.Log("Конец")

end

-- Запись в EEPROM вектора температурной калибровки
function EEPROM_LUA_EEPROMWRITECALIB(dialogMode)

  --GUI.Clear()
  GUI.Progress(0)
  GUI.Log("-------------- TELNET write temperature vector calibgation  --------------")
  
  strAlgResult = GUI.RunAlg("EEPROM_WRITECALIB", "CALC", "wait", 600); -- таймаут задаётся в 0.1 секунда

  if (strAlgResult ~= nil) then
    GUI.Log(strAlgResult, "Green")
  else
    GUI.Log("Return nil", "Red")
  end
  
  GUI.Progress(100)
  GUI.Done(1)
  GUI.Log("Конец")

end

-- Чтение по telnet текущего температрного кода на ADF01
function EEPROM_LUA_READ_TEMP_CODE(dialogMode)
  
  --GUI.Clear()
  GUI.Progress(0)
  --GUI.Log("-------------- TELNET temperature code READ --------------")
  
  strAlgResult = GUI.RunAlg("EEPROM_TEMPCODE_READ", "CALC", "wait", 20); -- таймаут задаётся в 0.1 секунда
  GUI.View("Temp code", strAlgResult)
  --[[if (strAlgResult ~= nil) then
    GUI.Log(strAlgResult, "Green")
  else
    GUI.Log("Return nil", "Red")
  end
  --]]
  
  --GUI.Log(dialogMode)
  --GUI.Done(1)
  return strAlgResult;

end

-- Отключение сторожевого таймера
function CPRR_LUA_OFF_WDT(dialogMode)
  
  --GUI.Clear()
  
  GUI.Log("EEPROM READ ---------------------------...")
  
  strAlgResult = GUI.RunAlg("CPRR_OFF_WDT", "CALC", "wait", 100); -- таймаут задаётся в 0.1 секунда

  if (strAlgResult ~= nil) then
    GUI.Log(strAlgResult, "Green")
  else
    GUI.Log("Return nil", "Red")
  end
  GUI.View("Wdt", strAlgResult)
  --GUI.Progress(100)
  --GUI.Done(1)
  --GUI.Log("Конец")
end

-- Задание IP и PORT настроек сети
function CPRR_SET_NET_SETTINGS(dialogMode)
  
  --GUI.Clear()
  
  GUI.Log("------------------- Set net param from file cprr_settings.json -------------------")
  
  strAlgResult = GUI.RunAlg("TELNET_SET_NET_PARAM", "CALC", "wait", 50); -- таймаут задаётся в 0.1 секунда

  if (strAlgResult ~= nil) then
    GUI.Log(strAlgResult, "Green")
  else
    GUI.Log("Return nil", "Red")
  end
  GUI.Progress(100)
  GUI.Log(dialogMode)
  --GUI.Done(1)
  --GUI.Log("Конец")
  return strAlgResult;

end

-- Задание IP и PORT настроек сети
function CPRR_GET_SN(dialogMode)
  
  GUI.Clear()
  
  strAlgResult = GUI.RunAlg("TELNET_GET_SN", "CALC", "wait", 50); -- таймаут задаётся в 0.1 секунда
  
  if (strAlgResult ~= nil) then
    GUI.Log(strAlgResult, "Green")
	CALIB_DATASET_LAST_PATH = BAG_SAVE_FOLDER .. "/" .. strAlgResult
	local leakAndCalibDir = REPORT_BASE_DIR_GUI .. "/" .. strAlgResult
	GUI.RunAlg("CALIBDIR", leakAndCalibDir, "wait");
    GUI.View("SN", strAlgResult)
	GUI.View("Путь к bag файлам", CALIB_DATASET_LAST_PATH)
	GUI.View("Путь к калибам", leakAndCalibDir)
  else
    GUI.Log("Return nil", "Red")
  end
  GUI.Done(1)
  --GUI.Log("Конец")
  return strAlgResult;

end