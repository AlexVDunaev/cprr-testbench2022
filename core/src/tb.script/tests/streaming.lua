
daq_enable = 0
daq_start_tick = 0
daq_season_count = 0 -- new.004.001.t25.bag (season : 004)
daq_episode_count = 0 -- new.004.001.t25.bag (episode : 001)
BAG_SAVE_FOLDER = "../../../testdata/record"

BAG_FILE_LEN_TIME = 10  -- 10 секунд

function DO_STREAMING()

  log("RADAR>DAQ. Streaming start!")
  daq_start_tick = mcFunc("GETTICK")
  
  RADAR.StreamEvent = function (time, GNumber, t1, t2, t3, t4, t5, t6)
      local dTime = mcFunc("GETTICK") - daq_start_tick
      log(string.format("STREAM> GN: %d Температура:  [%d %d %d %d %d %d]: EN :%d (%d ms)", GNumber, t1, t2, t3, t4, t5, t6, daq_enable, dTime))
    if (DAQ) then
    	DAQ.SendFrameInfo(time, GNumber)
      end

    if (daq_enable > 0) then
      -- запись идет
      -- Проверим время записи (если больше или равно 10с прервем и начнем новую запись)
      
      if (dTime/1000 >= BAG_FILE_LEN_TIME) then
        return "BREAK", ""
        end
      return "OK", ""
    else
      -- запись остановлена
      return "BREAK", ""
      end
    
  end
  
  if (RADAR) then
    -- изменим номер сезона
    daq_season_count = daq_season_count + 1
    
    local folder = string.format("%s/%s/new.%s.%.3d", BAG_SAVE_FOLDER, os.date("%Y%m%d"), os.date("%H%M%S"), daq_season_count)
    log("DIR:"..folder)
    
    CreateDir(folder)      
      
    if (1 == 1 ) then --checkRadar()

      
      while (daq_enable > 0) do
        -- изменим номер эпизода
        daq_episode_count = daq_episode_count + 1
        
        local filename = string.format("%s/new.%.3d.%.3d.t25.bag", folder, daq_season_count, daq_episode_count)
        
        -- метка начала стрима, мс
        daq_start_tick = mcFunc("GETTICK")
        
        if (RADAR.SaveStream(filename, 10000) > 0) then
          log("RADAR>SaveStream : OK")
        else
          log("RADAR>SaveStream : ERR")
        end
      end
    end
  end
  log("RADAR>DAQ. Streaming stop.")
  daq_episode_count = 0
end

