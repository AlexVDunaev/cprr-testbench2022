--TEST_ANGLE = {30, 60, 90, 120, 150} -- углы, на которых производим замеры.

TEST_ANGLE = {} 
-- формируем список углов: начальное, конечное значение, шаг:
for i = 40, 140, 5 do
  TEST_ANGLE[#TEST_ANGLE + 1] = i
end

-- определим предельную ошибку между положением ОПУ и детектированием пика
TEST_ANGLE_ERROR_LIMIT = 10.0

-- переменные модуля уровень шума в приёмном тракте
algRefAngle = {workDirectory = nil,  -- директория алгоритма
  frameCount    = 0,    -- счетчик полученных кадров обработчиком стриминга 
  mmapDrv       = nil,  -- драйвер MMAP через, который обмениваемся данными с модулем алгоритма
  alg           = nil,  -- таблица методов Алгоритма CPRR
  isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
  debugMode     = nil,  -- режим отладки. (не пишем в mmap)
  angleResult   = {},   -- словарь кадр: угол детектирования пика
  angleCheckErr = nil,  -- ошибка контроля положения пика
  errorText     = nil   -- сообщение об ошибке оператору
}

--DEBUG_READ_DATA_FILE = "./rf_rAngle.bag"




-- обработчика прерывания теста пользователем
local function onBreakRAngle()
  -- аварийное приведение в исх. состояние
  algRefAngle.errorText = "Тест прерван"
  GUI.Log(algRefAngle.errorText, "Red")
  algRefAngle.isBreak = 1
end

-- Определим обработчик ошибки (что-то с ОПУ)
local function OPUFatalError(code)
  log(string.format("Ошибка ОПУ> %s", code))
  -- аварийное приведение в исх. состояние
  algRefAngle.errorText = "ОПУ не отвечает!"
  GUI.Log(algRefAngle.errorText, "Red")
  algRefAngle.isBreak = 1
end


local function algLoadDebugFrames(bagName, N)
  local head
  local u
  local i
  if (bagName) then
    DATASET = {}
    for i = 0, N - 1 do
      u = algRefAngle.alg.getFrame(bagName, i);
      DATASET[i] = u
    end
  end
end


local function algGetDebugFrame(ptrData, frameIndex)
  local head
  local body
  if (DATASET) then
    if (DATASET[frameIndex]) then
      -- подменяю данные из датасета (только для отладки, когда нет стенда, только радар)
      head = algRefAngle.alg.getField(ptrData, 0, 52+4*720*16*2).mem
      body = algRefAngle.alg.getField(DATASET[frameIndex], 0, 64*720*16*2).mem
      ptrData = head..body
    end
  end
  return ptrData
end


-- Обработчик события стриминга
local function userStreamEventRAngle(time, GNumber, t1, t2, t3, t4, t5, t6, ptrData, sizeOfData)

  -- Кол-во кадров, полученных в стрименге
  algRefAngle.frameCount = algRefAngle.frameCount + 1

  log(string.format("CPRRLineEvent1>[RX]: GNumber: %4d [%d %d %d %d %d %d]", GNumber, t1, t2, t3, t4, t5, t6)) 

  local a =  TEST_ANGLE[algRefAngle.frameCount + 1]
  if (a)and(OPU) then
    -- a == nil : последний кадр. Поворацивать ОПУ больше не нужно
    OPU.SetAzimuth(a)
  end
  -- пауза для исключения вибрации от ОПУ
  pause(1000*1000) -- 1c

  -- Инициализация драйвера mmap (Однократно)
  if ((algRefAngle.mmapDrv == nil)and(algRefAngle.workDirectory)and(algRefAngle.debugMode == nil)) then
    algRefAngle.mmapDrv = algRefAngle.alg.mmapInit(algRefAngle.workDirectory.."/refangle.bin", sizeOfData) -- init mmap file
  end


  -- Передача кадра данных в mmap и сигнал алгоритму, что нужно обработать данные кадра
  if (algRefAngle.mmapDrv) then
    algRefAngle.alg.mmapWrite(algRefAngle.mmapDrv, ptrData)
    local angle
    -- Модуль алгоритма возвращает угол детектирования цели (уголка)
	local strAlgResult
    strAlgResult = GUI.RunAlg("RefAngle", "CALC", "wait")  -- "-23.1 1" Строка результата Угол и Дальность
	local AlgResult = {}
	for v in (strAlgResult.." "):gmatch("(.-) ") do
		table.insert(AlgResult, v);
		end
	if AlgResult[1] then
		angle = tonumber(AlgResult[1])
		end
	if AlgResult[2] then
		rangeIndex = tonumber(AlgResult[2])
		log("Дальность пика FFT: "..AlgResult[2])
		if rangeIndex < 4 then
			GUI.Log("Дальность пика FFT: "..AlgResult[2], "red")
			end
		end
		
    if (angle) then
      algRefAngle.angleResult[algRefAngle.frameCount] = -1*(angle - 90)
      if (math.abs(TEST_ANGLE[algRefAngle.frameCount] - algRefAngle.angleResult[algRefAngle.frameCount]) > TEST_ANGLE_ERROR_LIMIT) then 
        algRefAngle.angleCheckErr = 1
      end
    else
      -- Ошибка в алгоритме!  
      algRefAngle.errorText = "Ошибка в модуле алгоритма!"
      algRefAngle.isBreak = 1
    end
  end

  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algRefAngle.frameCount / #TEST_ANGLE))

  -- Проверка на прерывание теста
  if (algRefAngle.isBreak) then
    return "BREAK" -- прекращаем стриминг
  end

end


local function buildReport(result)
  local i
  REPORT.new(RadarSN, "Тест: Контроль положения отражателя")
  if (result == "ERROR") then
    REPORT.add("Результат выполнения теста: "..[[<b style="font-size:20px;color:red;">ОШИБКА</b>]], "h3")
  else
    REPORT.add("Результат выполнения теста: <b>НОРМА</b>", "h3")
  end
  if powerTestResultPmax then
	REPORT.add(string.format("Мощность(MAX): %.1f Вт", powerTestResultPmax))				
  end
  REPORT.add("Кол-во кадров (углов) контроля: "..string.format("%d", #TEST_ANGLE))
  for i = 1, #TEST_ANGLE do
    if ((algRefAngle.angleResult[i])and(TEST_ANGLE[i])) then
      local errValue = math.abs(TEST_ANGLE[i] - algRefAngle.angleResult[i])
      local extString = ""
      if (errValue > TEST_ANGLE_ERROR_LIMIT) then
        extString = [[<b style="font-size:16x;color:red;"> ОШИБКА</b>]]
      end
      -- в протоколе приводим все к централи: 90..-90 градусов
      REPORT.add(string.format("№%d. Угол ОПУ: %.1f&deg Угол детектирования пика: %.1f&deg, Разность : %.1f&deg", i, -1*(TEST_ANGLE[i] - 90), -1*(algRefAngle.angleResult[i] - 90), errValue)..extString)
    end
  end
  REPORT.add("График результата измерений", "h3") -- формат заголовка

  if (CopyFile(algRefAngle.workDirectory .. "/refAngleResult.png", getReportPath("refAngleResult.png"))) then
    REPORT.add(nil, nil, [[<img src="refAngleResult.png" alt="RefAngle">]])
  end

  REPORT.done(getReportPath("refAngle.html"))

end



local function getConfigRadar(filename, tmCode)
  local tryCount
  local getConfigResult
  local cfg
  local a
  local tm
  local leak

  if (tmCode == nil) then
    --[[ начиная с версии v2.76, радар возвращает код температуры в любом режиме стриминга
    -- установка обработчика событий в стрим режиме. Интерисует только код температуры
    RADAR.StreamEvent = function (time, GNumber, t1, t2, t3, t4, t5, t6)
      algRefAngle.radarTemp = t1
      log(string.format("STREAM> TEMP:  [%d %d %d %d %d %d]", t1, t2, t3, t4, t5, t6))
      end
    -- Если Георгий добавит код температуры в режим №4 (чт. конфига), 
    -- данный код, с запросом "tmp.bag" не нужен!
    log("Чтение температурного кода...")
    for tryCount = 1, 3 do
      getConfigResult = RADAR.SaveStream("tmp.bag", 1)
      if (getConfigResult > 0) then
         if (algRefAngle.radarTemp) then
          log("температурный код:"..algRefAngle.radarTemp)
          break
          end
      else
        log("Во время температурного кода радара возникла ошибка!")
      end
    end
    tmCode = algRefAngle.radarTemp
    ]]
    -------------------------------------------------------
  end
  log("Чтение конфигурации радара...")

  for tryCount = 1, 3 do
    getConfigResult = RADAR.SaveStream(filename, 1, 4)
    if (getConfigResult > 0) then
      log("Конфигурация радара прочитана.")
      break
    else
      log("Во время чтения конфигурации радара возникла ошибка!")
    end
  end

  if (getConfigResult == 0) then
    log("Считать конфигурацию радара не удалось!")
    return nil
  end

  if (tmCode == nil) then
    tmCode = RADAR.Temperature
  end

  a = Algorithm_Init()

  cfg, size, tm, leak = a.getConfig(filename, tmCode)
  -- возврат вектора ТМ (подходящий к tmCode) и пролаза

  if (tmCode)and(tm) then
    log("Конфигурация для температурного кода: "..tmCode.." -> "..string.unpack("f", tm))
  end
  return tm, leak

end


-- циклограмма получения зависимости амплитуды сигнала цели от угла поворота
-- Возврат: ["OK" - Норма, "ERROR" - ненорма, "ABORT" - тест прерван]
function TEST_MODULE_REFANGLE(dialogMode)
  -- итоговый результат проверки (получаем от модуля алгоритма)
  -- ["OK" | "ERROR" | "ABORT"]
  local result    
  local i
  local abort = nil

  if (#TEST_ANGLE <= 0) then
    GUI.Log("Точки (углы) контроля не определены!", "red")
    return "ABORT"
  end

  if (dialogMode) then
    if GUI.Dialog("design/rAngle.htm", "wait") ~= "OK" then
      GUI.Log("Проверка отменена!", "red")
      return "ABORT"
    end
  end
  -- начало каждой проверки начинаем с теста наличия радара
  -- проверка связи в радаром. Если все ОК, создаем директорию для отчетов
  if (checkRadar()) then
    CreateDir(getReportPath())
  else
    if (dialogMode) then
      GUI.Dialog("design/rAngleErr.htm", "wait", "Нет связи с радаром!")
    end
    GUI.Log("Ошибка! Нет связи с радаром!", "red")
    return "ABORT"
  end

  GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт проверки контроля положения отражателя...")

  -- инициализация данных теста
  algRefAngle.alg        = Algorithm_Init()
  algRefAngle.isBreak    = nil
  algRefAngle.mmapDrv    = nil
  algRefAngle.angleResult= {}
  algRefAngle.errorText  = "[Не удалось получить данные с радара]" -- ошибка по умолчанию
  algRefAngle.frameCount = 0

  -- инициализация алгоритма (очистка буферов данных) Возврат: путь к файлам
  algRefAngle.workDirectory = GUI.RunAlg("RefAngle", "START", "wait")
  --------------
  if (algRefAngle.workDirectory == nil) then
    GUI.Log("Ошибка. Директория назначения не определена")
    return "ABORT"
  end

  local tm
  local leak

  tm, leak = getConfigRadar(getReportPath("config.bag"))

  if (tm)and(leak) then
    WriteToFile(algRefAngle.workDirectory .. "/config.tm.bin", tm)
    WriteToFile(algRefAngle.workDirectory .. "/config.leak.bin", leak)
    GUI.RunAlg("RefAngle", "CONFIG", "wait")
  else
    if (dialogMode) then
      GUI.Dialog("design/rAngleErr.htm", "wait", "Не удалось считать конфигурацию радара!")
    end
    GUI.Log("Ошибка", "red")
  end
  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreakRAngle

  -- установка ОПУ в начальное положение
  if (OPU) then
    OPU.OnError = OPUFatalError
    OPU.SetAzimuth(TEST_ANGLE[1])
    -- пауза для исключения вибрации от ОПУ
    pause(1000*1000) -- 1c
  end

  if (algRefAngle.debugMode) then
    -- отладочный код. Запуск алгоритма без ОПУ
    for i = 1,10 do 
      GUI.RunAlg("RefAngle", "CALC", "wait") 
    end
  else
    -- Штатный код: 
    -- Событие получения кадра. В нем будем вызывать алгоритм и управлять ОПУ
    RADAR.StreamEvent = userStreamEventRAngle
    -- запуск стрима на заданное число кадров
    RADAR.SaveStream(getReportPath("refAngle.bag"), #TEST_ANGLE)
  end

  if (OPU) then
    -- установка ОПУ в центральное положение
    OPU.SetAzimuth(90)
    OPU.OnError = nil
  end

  RADAR.StreamEvent = nil
  GUI.OnBreak = nil

  if (algRefAngle.frameCount < #TEST_ANGLE) then
    -- Что-то пошло не так... не то количество кадров...
    if (dialogMode) then
      GUI.Dialog("design/rAngleErr.htm", "wait", algRefAngle.errorText)
    end
    GUI.Log("Ошибка. Не получены данные с радара", "red")
    return "ABORT"
  end

  -- Завершим алгоритм. Построение графика, получим итоговый результат
  -- финальный расчет занимает < 1 с
  result = GUI.RunAlg("RefAngle", "DONE", "wait")

  if (algRefAngle.angleCheckErr) then
    -- ошибка контроля положения пика!
    result = "ERROR"
  end

  -- Формирование отчета:
  buildReport(result)

  local dialogText = ""
  if (result == "ERROR") then
    dialogText = ":НЕНОРМА!"
    GUI.Log("Проверка 'Контроль положения отражателя'. Результат: НЕНОРМА!", "red")
  else
    GUI.Log("Проверка 'Контроль положения отражателя'. Значения в норме!", "green")
  end


  GUI.Log("Проверка завершилась, ждем реакции пользователя...")
  if (dialogMode) then
    GUI.Dialog("design/rAngleDone.htm"..dialogText, "wait")
  end
  return result

end

TEST_BEAM_ANGLE = {} 
-- формируем список углов: начальное, конечное значение, шаг:
for i = 70, 110, 0.5 do
  TEST_BEAM_ANGLE[#TEST_BEAM_ANGLE + 1] = i
end

-- Обработчик события стриминга
local function usrStreamEventBeamF(time, GNumber, t1, t2, t3, t4, t5, t6)

  log(string.format("CPRRLineEvent1>[RX]: GNumber: %4d [%d %d %d %d %d %d]", GNumber, t1, t2, t3, t4, t5, t6)) 

  -- Проверка на прерывание теста
  if (algRefAngle.isBreak) then
    return "BREAK" -- прекращаем стриминг
  end

end

-- проверка диаграммы направленности радара
-- Возврат: ["OK" - Норма, "ERROR" - ненорма, "ABORT" - тест прерван]
function TEST_BEAM_FORMING(dialogMode)
  -- итоговый результат проверки (получаем от модуля алгоритма)
  -- ["OK" | "ERROR" | "ABORT"]
  local result    
  local i
  local abort = nil

  if (#TEST_BEAM_ANGLE <= 0) then
    GUI.Log("Точки (углы) контроля не определены!", "red")
    return "ABORT"
  end

  if (dialogMode) then
    if GUI.Dialog("design/rAngle.htm", "wait") ~= "OK" then
      GUI.Log("Проверка отменена!", "red")
      return "ABORT"
    end
  end
  -- начало каждой проверки начинаем с теста наличия радара
  -- проверка связи в радаром. Если все ОК, создаем директорию для отчетов
  if (checkRadar()) then
    CreateDir(getReportPath())
  else
    if (dialogMode) then
      GUI.Dialog("design/rAngleErr.htm", "wait", "Нет связи с радаром!")
    end
    GUI.Log("Ошибка! Нет связи с радаром!", "red")
    return "ABORT"
  end

  GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт проверки диаграммы направленности...")

  -- инициализация данных теста
  algRefAngle.alg        = nil
  algRefAngle.isBreak    = nil
  algRefAngle.mmapDrv    = nil
  algRefAngle.angleResult= {}
  algRefAngle.errorText  = "[Не удалось получить данные с радара]" -- ошибка по умолчанию
  algRefAngle.frameCount = 0

  -- инициализация алгоритма (очистка буферов данных) Возврат: путь к файлам
  algRefAngle.workDirectory = GUI.RunAlg("RefAngle", "START", "wait")
  --------------
  if (algRefAngle.workDirectory == nil) then
    GUI.Log("Ошибка. Директория назначения не определена")
    return "ABORT"
  end

  local tm
  local leak

--[[
  tm, leak = getConfigRadar(getReportPath("config.bag"))

  if (tm)and(leak) then
    WriteToFile(algRefAngle.workDirectory .. "/config.tm.bin", tm)
    WriteToFile(algRefAngle.workDirectory .. "/config.leak.bin", leak)
    GUI.RunAlg("RefAngle", "CONFIG", "wait")
  else
    if (dialogMode) then
      GUI.Dialog("design/rAngleErr.htm", "wait", "Не удалось считать конфигурацию радара!")
    end
    GUI.Log("Ошибка", "red")
  end
--]]

  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreakRAngle

  -- установка ОПУ в начальное положение
  if (OPU) then
    GUI.Log("Позиционируем ОПУ в "..tostring(TEST_BEAM_ANGLE[1]).." градусов")
    OPU.OnError = OPUFatalError
    OPU.SetAzimuth(TEST_BEAM_ANGLE[1])
    -- пауза для исключения вибрации от ОПУ
    pause(2000*1000) -- 1c
  end

  if (algRefAngle.debugMode) then
    -- отладочный код. Запуск алгоритма без ОПУ
    for i = 1,10 do 
      GUI.RunAlg("RefAngle", "CALC", "wait") 
    end
  else
    -- Штатный код: 
    -- Событие получения кадра. В нем будем вызывать алгоритм и управлять ОПУ
    GUI.Log("Запуск стриминга "..tostring(TEST_BEAM_ANGLE[1]))
    RADAR.StreamEvent = usrStreamEventBeamF;
    -- запуск стрима на заданное число кадров

    local folder = string.format("%s/%s", BAG_SAVE_FOLDER, RADAR.SN)
    GUI.Log("Папка: "..folder);
    log("DIR: >>> "..folder)
    CreateDir(folder);
    local filepath = folder..'/'

    for i = 1, #TEST_BEAM_ANGLE, 1 do
      local a =  TEST_BEAM_ANGLE[i]
      if (a)and(OPU) then
        -- a == nil : последний кадр. Поворацивать ОПУ больше не нужно
        GUI.Log("Поворот ОПУ на "..tostring(TEST_BEAM_ANGLE[i]).." градусов");
        OPU.SetAzimuth(a)
      end
      -- пауза для исключения вибрации от ОПУ
      pause(2000*1000) -- 1c
      local filename = "refAngle".. string.format("_%f_", TEST_BEAM_ANGLE[i])..".t25.bag";
      GUI.Log("Имя файла: "..filename);
      RADAR.SaveStream(filepath..filename, 10);
      -- Индикация прогресса проверки
      GUI.Progress(math.floor(100 * algRefAngle.frameCount / #TEST_BEAM_ANGLE))
    end
  end

  if (OPU) then
    -- установка ОПУ в центральное положение
    OPU.SetAzimuth(90)
    OPU.OnError = nil
  end

  RADAR.StreamEvent = nil
  GUI.OnBreak = nil
  result = "DONE";
  
  return result

end