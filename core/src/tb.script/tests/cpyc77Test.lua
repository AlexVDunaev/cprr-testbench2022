TEST_CPYC77_POINT_NUM = 10

-- переменные модуля уровень шума в приёмном тракте
algCpyc = {workDirectory = nil,  -- директория алгоритма
         frameCount    = 0,    -- счетчик полученных кадров обработчиком стриминга 
         mmapDrv       = nil,  -- драйвер MMAP через, который обмениваемся данными с модулем алгоритма
         alg           = nil,  -- таблица методов Алгоритма 
         isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
         errorText     = nil   -- сообщение об ошибке оператору
         }


cpyc77frame = 0

function CPYC77_StreamEvent(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)

	cpyc77frame = cpyc77frame + 1
	if cpyc77frame > 15 then
		return "BREAK", ""
	end
	
--[[	if (cpyc77frame & 1) > 0 then
		return "IGNORE", ""
	end
	]]
  -- Кол-во кадров, полученных в стрименге
  algCpyc.frameCount = algCpyc.frameCount + 1
      log(string.format(">>>>>>>>> FRIME[%d] <<<<<<<<<", frameIndex)) 
	
	-- Инициализация драйвера mmap (Однократно)
	if ((algCpyc.mmapDrv == nil)and(algCpyc.workDirectory)) then
		algCpyc.mmapDrv = algCpyc.alg.mmapInit(algCpyc.workDirectory.."/cpyc_test.bin", sizeOfData) -- init mmap file
		log("mmapInit()")
	end

	-- Передача кадра данных в mmap и сигнал алгоритму, что нужно обработать данные кадра
	if (algCpyc.mmapDrv) then
		log("mmapWrite()...")
		algCpyc.alg.mmapWrite(algCpyc.mmapDrv, ptrData)
		log("------------ RunAlg()... ------------------------------")
		GUI.RunAlg("CPYCTEST", "CALC", "wait") 
		log("-------------------------------------------------------")
	end

	return "IGNORE", ""
end





-- циклограмма получения данных, при откл. TX
-- Возврат: ["OK" - Норма, "ERROR" - ненорма, "ABORT" - тест прерван]
function TEST_CPYC77(dialogMode)
  local result    -- итоговый результат проверки (получаем от модуля алгоритма)
  local i
  
  GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт проверки определения уровня шума в приёмном тракте...")
  GUI.Log("Проверка началась")

  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = nil
  -- инициализация данных теста
  algCpyc.alg        = Algorithm_Init()
  algCpyc.isBreak    = nil
  algCpyc.mmapDrv    = nil
  algCpyc.errorText  = "[Не удалось получить данные с радара]" -- ошибка по умолчанию
  algCpyc.frameCount = 0
  
  -- инициализация алгоритма (очистка буферов данных) Возврат: путь к файлам
  algCpyc.workDirectory = GUI.RunAlg("CPYCTEST", "START", "wait")
  --------------
  log("******************** workDirectory **********************")
  log(algCpyc.workDirectory)
  log("*********************************************************")
  
  
  local nTry -- кол-во попыток получить данные
  -- дальнейшая работа по изменению частоты идет в userStreamEventGenNext
  RADAR.StreamEvent = CPYC77_StreamEvent
  for nTry = 1,3 do
    algCpyc.frameCount = 0
    -- Начнем сбор данных ...
    RADAR.SaveStream(getReportPath("cpyc_test.bag"), TEST_CPYC77_POINT_NUM, 0)
    -- Выход из режима стриминга. 
    -- Если данных не было, повторим...
    if (algCpyc.frameCount >= TEST_CPYC77_POINT_NUM)or(algCpyc.isBreak) then
        break -- все норм. (или просто прервали)
    end
	break
  end
  
  RADAR.StreamEvent = nil
  GUI.OnBreak = nil

  if (algCpyc.mmapDrv) then
    -- Закроем драйвер MMAP
    algCpyc.alg.mmapClose(algCpyc.mmapDrv)
    end
  
  if (algCpyc.frameCount < TEST_CPYC77_POINT_NUM) then
    -- Что-то пошло не так... не то количество кадров...
    GUI.Log("Ошибка")
    return "ABORT"
  end
  
  -- Завершим алгоритм. Построение графика, получим итоговый результат
  -- финальный расчет занимает 1 .. 2 с
  result = GUI.RunAlg("CPYCTEST", "DONE", "wait")
  
  GUI.Log("Конец")
  return result
end

-- Режим стриминга
function TEST_STREAM_CPYC77()
  cmdFRAMENUM = mcGetParam("args", "FRAMENUM", "100")
  log(string.format("---------------> FRAMENUM is: %d", cmdFRAMENUM))

  cmdOUTPUTFILES = mcGetParam("args", "OUTPUTFILES", REPORT_BASE_DIR)
  log(string.format("---------------> OUTPUTFILES is: %s", cmdOUTPUTFILES))

  log(string.format("---------------> START STREAM MODE"))

  path_bag = string.format("new.000.111.222.t25.bag")

  RADAR.StreamEvent = userStreamEvent;

  log(path_bag)
  --while 1 == 1 do
	n = RADAR.SaveStream(path_bag, 110, 0)
	--end
	n = 0
  log(string.format("DO_TEST_CPYC77:DONE. N:%d", n))

  alg = Algorithm_Init();

  frameSize = 2*2*256*4*128
  mmapDrv = nil
  index = 0
  while 1 == 1 do
	bag, info = alg.getFrame(path_bag, index, 1, frameSize)
	if bag then
	    print(info)
	  for i, t in pairs(info) do
	    print(i, t)
	  end

		log(string.format("bag != NULL Index:%d", index))
		
		frame = alg.getField(bag, 0, frameSize).mem

		if (mmapDrv == nil) then
			mmapDrv = alg.mmapInit("test.bin", frameSize) -- init mmap file
			end

		if (mmapDrv) then
			alg.mmapWrite(mmapDrv, frame)
			log(string.format("RUN CALC"))
			pause(500*1000) -- засыпаем на 0.5c
			end
		
		
		else
		log(string.format("bag == NULL Index:%d", index))
		break;
		end
		index = index + 1
		break;
	end


if (mmapDrv) then
    -- Закроем драйвер MMAP
    --alg.mmapClose(mmapDrv)
    end


  RADAR.StreamEvent = nil;
  log(string.format("---------------> END STREAM MODE"))
end

-- Режим стриминга
function DO_TEST_CPYC77()
	log(string.format("---------DO_TEST_CPYC77--------"))
	-- TEST_STREAM_CPYC77()
	TEST_CPYC77()
	log(string.format("---------DO_TEST_CPYC77--------"))
end




