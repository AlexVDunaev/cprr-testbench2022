RAW_SIGNAL_FRAME_NUM = 100 -- кол-во кадров контроля
RAW_SIGNAL_DATA_FILE = "raw_adc.bag"
RAW_SIGNAL_WRITE_BAG = 1 -- 0: не писать файл. 1: Писать файл RAW_SIGNAL_DATA_FILE

-- переменные модуля контроля сбойных чирпов
algRawSignal = {workDirectory = nil,  -- директория алгоритма
         frameCount    = 0,    -- счетчик полученных кадров обработчиком стриминга 
         mmapDrv       = nil,  -- драйвер MMAP через, который обмениваемся данными с модулем алгоритма
         alg           = nil,  -- таблица методов Алгоритма CPRR
         isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
         errorText     = nil   -- сообщение об ошибке оператору
         }




-- Обработчик события стриминга
function algRawSignalProcessFrame(time, GNumber, t1, t2, t3, t4, t5, t6, ptrData, sizeOfData)
  
  -- Кол-во кадров, полученных в стрименге
  algRawSignal.frameCount = algRawSignal.frameCount + 1

  -- Инициализация драйвера mmap (Однократно)
  if ((algRawSignal.mmapDrv == nil)and(algRawSignal.workDirectory)) then
    algRawSignal.mmapDrv = algRawSignal.alg.mmapInit(algRawSignal.workDirectory.."/rawadc.bin", sizeOfData) -- init mmap file
  end
  
  -- отладочный код, для работы без стенда (но с радаром), подмена данных на данные из файла
  --ptrData = algGetDebugFrame(ptrData, algRawSignal.frameCount - 1)

  -- Передача кадра данных в mmap и сигнал алгоритму, что нужно обработать данные кадра
  if (algRawSignal.mmapDrv) then
    algRawSignal.alg.mmapWrite(algRawSignal.mmapDrv, ptrData)
    GUI.RunAlg("CPRRADC", "CALC", "wait") 
  end
  
  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algRawSignal.frameCount / RAW_SIGNAL_FRAME_NUM))
  
  -- Проверка на прерывание теста
  if (algRawSignal.isBreak) then
    return "BREAK" -- прекращаем стриминг
    end

end




local function buildReport(result, errorList)
  local i
  REPORT.new(RadarSN, "Тест: Контроль сбойных чирпов")
  if (result == "ERROR") then
    REPORT.add("Результат выполнения теста: "..[[<b style="font-size:20px;color:red;">ОШИБКА</b>]], "h3")
  else
    REPORT.add("Результат выполнения теста: <b>НОРМА</b>", "h3")
  end
  if powerTestResultPmax then
	REPORT.add(string.format("Мощность(MAX): %.1f Вт", powerTestResultPmax))				
  end
  REPORT.add("Кол-во кадров контроля: "..string.format("%d", algRawSignal.frameCount))
  for i = 1, 16 do
    if (errorList[i + 1]) then
		local errors = tonumber(errorList[i + 1])
		if errors == nil then
			errors = 0
		end
		local extString = ""
		
		if errors > 0 then
			extString = [[<b style="font-size:16x;color:red;"> ОШИБКА!</b>]]
		end

      REPORT.add(string.format("Канал %d. Ошибок: %d", i, errors)..extString)
    end
  end
  -- решил копировать всегда, т.к. если ошибок не было, будет картинка "NODATA"
  local copyDone = CopyFile(algRawSignal.workDirectory .. "/adcerrors.png", getReportPath("adcerrors.png"))
  if (result == "ERROR") then
	REPORT.add("Графики сбойных чирпов (первые из обнаруженных)", "h3") -- формат заголовка
	if (copyDone) then
		REPORT.add(nil, nil, [[<img src="adcerrors.png" alt="adcerrors">]])
	end
  end
REPORT.add("Графики полезной части сигнала первого чирпа", "h3") -- формат заголовка
if (CopyFile(algRawSignal.workDirectory .. "/adcall.png", getReportPath("adcall.png"))) then
	REPORT.add(nil, nil, [[<img src="adcall.png" alt="adcall">]])
end
REPORT.add("Статистика по выбросам", "h3") -- формат заголовка
if (CopyFile(algRawSignal.workDirectory .. "/adcDiffStat.png", getReportPath("adcDiffStat.png"))) then
	REPORT.add(nil, nil, [[<img src="adcDiffStat.png" alt="adcDiffStat">]])
end
  temp = getReportPath("rawSignal.html")
  print('-----------------------------------> '..temp)
  REPORT.done(getReportPath("rawSignal.html"))

end



-- обработчика прерывания теста пользователем
local function onBreak()
  algRawSignal.errorText = "Тест прерван"
  GUI.Log(algRawSignal.errorText, "Red")
  algRawSignal.isBreak = 1
end

-- Обработчик события стриминга
local function userStreamEventRawSignal(time, GNumber, t1, t2, t3, t4, t5, t6, ptrData, sizeOfData)
  
  -- Кол-во кадров, полученных в стрименге
  algRawSignal.frameCount = algRawSignal.frameCount + 1
  
  -- log(string.format("GNumber: %4d [%d %d %d %d %d %d]", GNumber, t1, t2, t3, t4, t5, t6)) 
  
  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algRawSignal.frameCount / RAW_SIGNAL_FRAME_NUM))
  
  -- Проверка на прерывание теста
  if (algRawSignal.isBreak) then
    return "BREAK" -- прекращаем стриминг
  end

end


-- циклограмма получения данных, при откл. TX
-- Возврат: ["OK" - Норма, "ERROR" - ненорма, "ABORT" - тест прерван]
function CPRR_TEST_RAWSIGNAL(dialogMode)
  local result    -- итоговый результат проверки (получаем от модуля алгоритма)
  local i
  
  -- начало каждой проверки начинаем с теста наличия радара
  if (checkRadar()) then
    CreateDir(getReportPath())
  else
    if (dialogMode) then
      GUI.Dialog("design/cprradcErr.htm", "wait", "Нет связи с радаром!")
    end
    GUI.Log("Ошибка! Нет связи с радаром!", "red")
    return "ABORT"
  end

  
  --GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт проверки сырого сигнала...")
  GUI.Log("Проверка началась")

  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreak
  -- инициализация данных теста
  algRawSignal.alg        = Algorithm_Init()
  algRawSignal.isBreak    = nil
  algRawSignal.mmapDrv    = nil
  algRawSignal.errorText  = "[Не удалось получить данные с радара(или файла)]" -- ошибка по умолчанию
  algRawSignal.frameCount = 0
  
  -- инициализация алгоритма (очистка буферов данных) Возврат: путь к файлам
  algRawSignal.workDirectory = GUI.RunAlg("CPRRADC", "START", "wait")
  --------------

  local bagFileName = RAW_SIGNAL_DATA_FILE
  
  if RAW_SIGNAL_WRITE_BAG == 1 then
    local folder = string.format("%s/%s", BAG_SAVE_FOLDER, RADAR.SN)
    CreateDir(folder)
	bagFileName = folder .. "/" .. bagFileName

    RADAR.StreamEvent = userStreamEventRawSignal
    -- запуск стрима на заданное число кадров
    local n = RADAR.SaveStream(bagFileName, RAW_SIGNAL_FRAME_NUM)
	if n ~= RAW_SIGNAL_FRAME_NUM then
		GUI.Log("Ошибка! Датасет не записан!", "Red")
		return "ABORT"
	end
  end
  
  algRawSignal.frameCount = 0
  
  for i = 0, RAW_SIGNAL_FRAME_NUM - 1 do
	frame, info = algRawSignal.alg.getFrame(bagFileName, i, 0, 68*720*16*2, 0);
	if ((frame == nil)or(algRawSignal.isBreak)) then
		break
	end
	local t = info["temperature"]

  local body = algRawSignal.alg.getField(frame, 0, 68*720*16*2).mem
  local head = string.pack("i4i4i4i8i8i2i2i2i2i2i2i4i4i4", 0xABCD, 0, 10, 0, 1,
    0,0,0,0,0,0, info.chirps, info.channels, info.points)
  local radarframe = head..body

  algRawSignalProcessFrame(info["time"], i, t, t, t, t, t, t, radarframe, #radarframe)
  end
  
  
  GUI.OnBreak = nil

  if (algRawSignal.mmapDrv) then
    -- Закроем драйвер MMAP
    algRawSignal.alg.mmapClose(algRawSignal.mmapDrv)
    end
  
  if (algRawSignal.frameCount == 0) then
    -- Что-то пошло не так... не то количество кадров...
    if (dialogMode) then
      GUI.Dialog("design/cprradcErr.htm", "wait", algRawSignal.errorText)
      end
    GUI.Log("Ошибка")
    return "ABORT"
  end
  
  -- Завершим алгоритм. Построение графика, получим итоговый результат
  -- финальный расчет занимает 1 .. 2 с
  local strAlgResult = GUI.RunAlg("CPRRADC", "DONE", "wait")
  -- если что-то пошло не так
  if strAlgResult == nil then
    if (dialogMode) then
      GUI.Dialog("design/cprradcErr.htm", "wait", algRawSignal.errorText)
      end
    GUI.Log("Ошибка")
    return "ABORT"
  end
  
  local errors
  
  local AlgResult = {}
  local v
  
  for v in (strAlgResult.." "):gmatch("(.-) ") do
	table.insert(AlgResult, v);
  end
  
  if AlgResult[1] then
	errors = tonumber(AlgResult[1]) -- общее число ошибок
  end
  
  if errors == nil then
	errors = 0
  end

  local dialogText = ""
  
  if (errors > 0) then
    dialogText = ":НЕНОРМА!"..string.format(" Ошибок %d", errors)
    GUI.Log("Проверка 'Контроль сбойных чирпов'. Результат: НЕНОРМА!", "red")
	result = "ERROR"
  else
    GUI.Log("Проверка 'Контроль сбойных чирпов'. Сбойных чирпов не обнаружено!", "green")
	result = "OK"
  end

  GUI.Log("Проверка завершилась, ждем реакции пользователя...")
  --GUI.Clear()
  buildReport(result, AlgResult)
  log("DONE!!!!!! result:"..result)
  GUI.View("Тест сигнала АЦП", result)

  if (dialogMode) then
	local userResult
	if (errors > 0) then
		userResult = GUI.Dialog("design/cprradcDoneERR.htm"..dialogText, "wait")
		else
		userResult = GUI.Dialog("design/cprradcDoneOK.htm"..dialogText, "wait")
		end
		print(">>>", userResult, "<<<")
	if userResult == "SAVE"	then
		-- копирование результата в отдельную папку
		local path = getReportPath()
		local copyPath = path .. "/" .. os.date("%Y-%m-%d_%H-%M-%S")
		-- только данного теста
		local fileList = {"adcall.png", "adcDiffStat.png", "adcerrors.png", "rawSignal.html", "dnvStyleSky.css", "logoCP.png", "qr-code.gif"}
		-- fileList = mcFunc("FILELIST", path)  -- Все файлы. (Имя файла это k, а не v!)
		local k,v
		CreateDir(copyPath)
		for k, v in pairs(fileList) do
			CopyFile(path .. "/" .. v, copyPath .. "/" .. v)
		end
	end
  end

  GUI.Done(1)
  return result
end