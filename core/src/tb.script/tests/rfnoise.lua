NOISE_POINT_NUM = 100 -- кол-во кадров контроля шума

-- переменные модуля уровень шума в приёмном тракте
algNoise = {workDirectory = nil,  -- директория алгоритма
         frameCount    = 0,    -- счетчик полученных кадров обработчиком стриминга 
         mmapDrv       = nil,  -- драйвер MMAP через, который обмениваемся данными с модулем алгоритма
         alg           = nil,  -- таблица методов Алгоритма CPRR
         isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
         errorText     = nil   -- сообщение об ошибке оператору
         }

--DEBUG_READ_DATA_FILE = "./rf_noise.bag"

-- обработчика прерывания теста пользователем
local function onBreakRFNoiseGen()
  -- аварийное приведение в исх. состояние
  algNoise.errorText = "Тест прерван"
  GUI.Log(algNoise.errorText, "Red")
  algNoise.isBreak = 1
  end

local function algLoadDebugFrames(bagName, N)
  local head
  local u
  local i
  if (bagName) then
    DATASET = {}
      for i = 0, N - 1 do
        u = algNoise.alg.getFrame(bagName, i);
        DATASET[i] = u
      end
    end
end


local function algGetDebugFrame(ptrData, frameIndex)
  local head
  local body
  if (DATASET) then
    if (DATASET[frameIndex]) then
      -- подменяю данные из датасета (только для отладки, когда нет стенда, только радар)
      head = algNoise.alg.getField(ptrData, 0, 52+4*720*16*2).mem
      body = algNoise.alg.getField(DATASET[frameIndex], 0, 64*720*16*2).mem
      ptrData = head..body
     end
   end
  return ptrData
end


-- Обработчик события стриминга
function userStreamEventNoise(time, GNumber, t1, t2, t3, t4, t5, t6, ptrData, sizeOfData)
  
  -- Кол-во кадров, полученных в стрименге
  algNoise.frameCount = algNoise.frameCount + 1
  
  log(string.format("GNumber: %4d [%d %d %d %d %d %d]", GNumber, t1, t2, t3, t4, t5, t6)) 
  
  -- Инициализация драйвера mmap (Однократно)
  if ((algNoise.mmapDrv == nil)and(algNoise.workDirectory)) then
    algNoise.mmapDrv = algNoise.alg.mmapInit(algNoise.workDirectory.."/rfNoise.bin", sizeOfData) -- init mmap file
  end
  
  -- отладочный код, для работы без стенда (но с радаром), подмена данных на данные из файла
  --ptrData = algGetDebugFrame(ptrData, algNoise.frameCount - 1)

  -- Передача кадра данных в mmap и сигнал алгоритму, что нужно обработать данные кадра
  if (algNoise.mmapDrv) then
    algNoise.alg.mmapWrite(algNoise.mmapDrv, ptrData)
    GUI.RunAlg("RFNoise", "CALC", "wait") 
  end
  -- GUI.RunAlg("RFNoise", "TEST", "wait") 
  
  
  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algNoise.frameCount / NOISE_POINT_NUM))
  
  -- Проверка на прерывание теста
  if (algNoise.isBreak) then
    return "BREAK" -- прекращаем стриминг
    end

end


local function buildReport(result)
  local i
  
  REPORT.new(RadarSN, "Тест: уровень шума в приёмном тракте")
  if (result == "ERROR") then
    REPORT.add("Результат выполнения теста: "..[[<b style="font-size:20px;color:red;">ОШИБКА</b>]], "h3")
   else
    REPORT.add("Результат выполнения теста: <b>НОРМА</b>", "h3")
  end
  if powerTestResultPmax then
	REPORT.add(string.format("Мощность(MAX): %.1f Вт", powerTestResultPmax))				
  end
  REPORT.add("Кол-во кадров контроля: "..string.format("%d", NOISE_POINT_NUM))
  REPORT.add("Графики результатов измерений", "h3") -- формат заголовка

  if (CopyFile(algNoise.workDirectory .. "/rfnoiseResult.png", getReportPath("rfnoiseResult.png"))) then
    REPORT.add(nil, nil, [[<img src="rfnoiseResult.png" alt="RF GEN ALL MAX">]])
  end

  -- копирование файлов графиков из директории алгоритма в директорию отчета
  for i = 1, 2 do
    if (CopyFile(string.format("%s/rfnoiseResultTX%d.png", algNoise.workDirectory, i), 
             string.format("%s/rfnoiseResultTX%d.png", getReportPath(), i))) then
    
    REPORT.add(nil, nil, string.format([[<img src="rfnoiseResultTX%d.png" alt="RF_NOISE RESULT%d">]], i, i))
    end
  end
  for i = 1, 2 do
    if (CopyFile(string.format("%s/rfnoiseResultMedTX%d.png", algNoise.workDirectory, i), 
             string.format("%s/rfnoiseResultMedTX%d.png", getReportPath(), i))) then
    
    REPORT.add(nil, nil, string.format([[<img src="rfnoiseResultMedTX%d.png" alt="RF_NOISE Median RESULT%d">]], i, i))
    end
  end

  REPORT.done(getReportPath("rfnoise.html"))

end


-- циклограмма получения данных, при откл. TX
-- Возврат: ["OK" - Норма, "ERROR" - ненорма, "ABORT" - тест прерван]
function TEST_MODULE_RF_NOISE(dialogMode)
  local result    -- итоговый результат проверки (получаем от модуля алгоритма)
  local i
  -- начало каждой проверки начинаем с теста наличия радара
  if (checkRadar()) then
    CreateDir(getReportPath())
  else
    if (dialogMode) then
      GUI.Dialog("design/rfnoiseErr.htm", "wait", "Нет связи с радаром!")
    end
    GUI.Log("Ошибка! Нет связи с радаром!", "red")
    return "ABORT"
  end
  
  --GUI.Clear()
  GUI.Progress(0)
  GUI.Log("Старт проверки определения уровня шума в приёмном тракте...")
  GUI.Log("Проверка началась")
  --[[
  RADAR.SaveStream(getReportPath("rf_noise.bag"), 100)
  if (NOISE_POINT_NUM) then
    return 
    end
    ]]

  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreakRFNoiseGen
  -- инициализация данных теста
  algNoise.alg        = Algorithm_Init()
  algNoise.isBreak    = nil
  algNoise.mmapDrv    = nil
  algNoise.errorText  = "[Не удалось получить данные с радара]" -- ошибка по умолчанию
  algNoise.frameCount = 0
  
  -- инициализация алгоритма (очистка буферов данных) Возврат: путь к файлам
  algNoise.workDirectory = GUI.RunAlg("RFNoise", "START", "wait")
  --------------
  
  -- для отладки. читаю данные из файла, а не из радара
  algLoadDebugFrames(DEBUG_READ_DATA_FILE, NOISE_POINT_NUM)
  
  
  local folder = string.format("%s/%s", BAG_SAVE_FOLDER, RADAR.SN)
  local bagFileName = folder .. "/rf_noise.bag" -- getReportPath("rf_noise.bag")
  CreateDir(folder)
  
  local nTry -- кол-во попыток получить данные
  -- дальнейшая работа по изменению частоты идет в userStreamEventGenNext
  RADAR.StreamEvent = userStreamEventNoise
  for nTry = 1,3 do
    algNoise.frameCount = 0
    -- Начнем сбор данных ...
    RADAR.SaveStream(bagFileName, NOISE_POINT_NUM, 5) -- 5 : Режим стриминга TX:OFF, ЛЧМ:OFF
    -- Выход из режима стриминга. 
    -- Если данных не было, повторим...
    if (algNoise.frameCount >= NOISE_POINT_NUM)or(algNoise.isBreak) then
        break -- все норм. (или просто прервали)
    end
  end
  
  RADAR.StreamEvent = nil
  GUI.OnBreak = nil

  if (algNoise.mmapDrv) then
    -- Закроем драйвер MMAP
    --algNoise.alg.mmapClose(algNoise.mmapDrv)
    end
  
  if (algNoise.frameCount < NOISE_POINT_NUM) then
    -- Что-то пошло не так... не то количество кадров...
    if (dialogMode) then
      GUI.Dialog("design/rfnoiseErr.htm", "wait", algNoise.errorText)
      end
    GUI.Log("Ошибка")
    return "ABORT"
  end
  
  -- Завершим алгоритм. Построение графика, получим итоговый результат
  -- финальный расчет занимает 1 .. 2 с
  result = GUI.RunAlg("RFNoise", "DONE", "wait")
  
  --powerTestResultPmax = GetRadarPower()
  
  -- Формирование отчета:
  buildReport(result)
  
  GUI.View("Проверка шума", result)

  local dialogText = ""
  if (result == "ERROR") then
    dialogText = ":НЕНОРМА!"
    GUI.Log("Проверка 'уровень шума в приёмном тракте'. Результат: НЕНОРМА!", "red")
  else
    GUI.Log("Проверка 'уровень шума в приёмном тракте'. Значения в норме!", "green")
    end
    
  GUI.Log("Проверка завершилась, ждем реакции пользователя...")
    if (dialogMode) then
		if GUI.Dialog("design/rfnoiseDone.htm"..dialogText, "wait") == "SAVE" then
			-- копирование результата в отдельную папку
			local path = getReportPath()
			local copyPath = path .. "/" .. os.date("%Y-%m-%d_%H-%M-%S")
			-- только данного теста
			local fileList = mcFunc("FILELIST", path)  -- Все файлы. (Имя файла это k, а не v!)
			local k,v
			CreateDir(copyPath)
			for k, v in pairs(fileList) do
				CopyFile(path .. "/" .. k, copyPath .. "/" .. k)
			end
		end
    end
  GUI.Log("Конец")
  GUI.Done(1)
  return result
end