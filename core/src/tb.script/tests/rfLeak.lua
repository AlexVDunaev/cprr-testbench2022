LEAK_POINT_NUM = 500 -- кол-во кадров контроля шума
LEAK_WRITE_BAG = 1

-- переменные модуля уровень шума в приёмном тракте
algLeak = {workDirectory = nil,  -- директория алгоритма
         frameCount    = 0,    -- счетчик полученных кадров обработчиком стриминга 
         mmapDrv       = nil,  -- драйвер MMAP через, который обмениваемся данными с модулем алгоритма
         alg           = nil,  -- таблица методов Алгоритма CPRR
         isBreak       = nil,  -- признак прерывания теста пользователем / по ошибке оборудования
         errorText     = nil   -- сообщение об ошибке оператору
         }

--DEBUG_READ_DATA_FILE = "./rf_leak.bag"

-- обработчика прерывания теста пользователем
local function onBreakRFLeakGen()
  -- аварийное приведение в исх. состояние
  algLeak.errorText = "Тест прерван"
  GUI.Log(algLeak.errorText, "Red")
  algLeak.isBreak = 1
  end



-- Обработчик события стриминга
local function userStreamEventLeak(time, GNumber, t1, t2, t3, t4, t5, t6, ptrData, sizeOfData)
  
  -- Кол-во кадров, полученных в стрименге
  algLeak.frameCount = algLeak.frameCount + 1
  
  log(string.format("CPRRLineEvent1>[RX]: GNumber: %4d [%d %d %d %d %d %d]", GNumber, t1, t2, t3, t4, t5, t6)) 
  
  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algLeak.frameCount / LEAK_POINT_NUM))
  
  -- Проверка на прерывание теста
  if (algLeak.isBreak) then
    return "BREAK" -- прекращаем стриминг
    end

end



-- Обработчик события стриминга
local function LeakProcessFrame(time, GNumber, t1, t2, t3, t4, t5, t6, ptrData, sizeOfData)
  
  -- Кол-во кадров, в обработке
  algLeak.frameCount = algLeak.frameCount + 1

  -- Инициализация драйвера mmap (Однократно)
  if ((algLeak.mmapDrv == nil)and(algLeak.workDirectory)) then
    algLeak.mmapDrv = algLeak.alg.mmapInit(algLeak.workDirectory.."/leak_adc.bin", sizeOfData) -- init mmap file
  end
  
  -- Передача кадра данных в mmap и сигнал алгоритму, что нужно обработать данные кадра
  if (algLeak.mmapDrv) then
    algLeak.alg.mmapWrite(algLeak.mmapDrv, ptrData)
    GUI.RunAlg("RFLeak", "CALC", "wait") 
  end
  
  -- Индикация прогресса проверки
  GUI.Progress(math.floor(100 * algLeak.frameCount / LEAK_POINT_NUM))
  
  -- Проверка на прерывание теста
  if (algLeak.isBreak) then
    return "BREAK" -- прекращаем стриминг
    end

end

-- циклограмма получения данных, при откл. TX
-- Возврат: ["OK" - Норма, "ERROR" - ненорма, "ABORT" - тест прерван]
function TEST_MODULE_RF_LEAK(dialogMode)
  local result    -- итоговый результат проверки (получаем от модуля алгоритма)
  local i
  -- начало каждой проверки начинаем с теста наличия радара
  if LEAK_WRITE_BAG == 1 then
	  if (checkRadar()) then
		CreateDir(getReportPath())
	  else
		GUI.Log("Ошибка! Нет связи с радаром!", "red")
		return "ABORT"
	  end
  end
  
  GUI.Progress(0)
  GUI.Log("Старт проверки определения пролаза...")
  GUI.Log("Проверка началась. Нужно крутить радар!", "blue")

  -- установка обработчика прерывания теста пользователем
  GUI.OnBreak = onBreakRFLeakGen
  -- инициализация данных теста
  algLeak.alg        = Algorithm_Init()
  algLeak.isBreak    = nil
  algLeak.mmapDrv    = nil
  algLeak.errorText  = "[Не удалось получить данные с радара]" -- ошибка по умолчанию
  algLeak.frameCount = 0
  
  -- инициализация алгоритма (очистка буферов данных) Возврат: путь к файлам
  algLeak.workDirectory = GUI.RunAlg("RFLeak", "START", "wait")
  --------------
  local radarSn = "000000000"
  if RADAR.SN then 
	radarSn = string.format("%d", RADAR.SN)
	else
	-- для отладки, если RADAR.SN не считан
	RADAR.SN = radarSn
	log("Создание папки протоколов: " .. getReportPath())
    CreateDir(getReportPath())
	RADAR.SN = nil
  end

  local folder = string.format("%s/%s", BAG_SAVE_FOLDER, RADAR.SN)
  CreateDir(folder)

  local bagFileName = string.format("%s/Leak_%s.bag", folder, radarSn)
--  local bagFileName = string.format("%s/Leak_000000000.bag", algLeak.workDirectory)
--  getReportPath("rf_leak.bag") Если надо сохранить в папку протоколов
  
  if LEAK_WRITE_BAG == 1 then
	  local nTry -- кол-во попыток получить данные
	  -- дальнейшая работа по изменению частоты идет в userStreamEventGenNext
	  RADAR.StreamEvent = userStreamEventLeak
	  for nTry = 1,3 do
		-- добавил проверку радара (+перевод в режим отметок для откл. WDT)
	  	--checkRadar()
		algLeak.frameCount = 0
		-- Начнем сбор данных ...
		RADAR.SaveStream(bagFileName, LEAK_POINT_NUM) 
		-- Выход из режима стриминга. 
		-- Если данных не было, повторим...
		if (algLeak.frameCount >= LEAK_POINT_NUM)or(algLeak.isBreak) then
			break -- все норм. (или просто прервали)
		end
	  end
	  RADAR.StreamEvent = nil
	if (algLeak.frameCount ~= LEAK_POINT_NUM)or(algLeak.isBreak) then
		GUI.Log("Что-то пошло не так!", "red")
		return "ABORT"
	end
  end
  --------------------------------
  GUI.Log("Датасет записан! Радар можно отложить. Идет расчет...", "green")
  
  algLeak.frameCount = 0
  
for i = 0, LEAK_POINT_NUM - 1 do
	frame, info = algLeak.alg.getFrame(bagFileName, i, 0, 68*720*16*2, 0);
	if ((frame == nil)or(algLeak.isBreak)) then
		break
	end
	local t = info["temperature"]

  local body = algLeak.alg.getField(frame, 0, 68*720*16*2).mem
  local head = string.pack("i4i4i4i8i8i2i2i2i2i2i2i4i4i4", 0xABCD, 0, 10, 0, 1,
    0,0,0,0,0,0, info.chirps, info.channels, info.points)
  local radarframe = head..body

  LeakProcessFrame(info["time"], i, t, t, t, t, t, t, radarframe, #radarframe)
  end  
  
  -- Завершим алгоритм. Построение графика, получим итоговый результат
  -- финальный расчет занимает 1 .. 2 с
  result = GUI.RunAlg("RFLeak", "DONE:"..radarSn, "wait")
  -- Копирую файлы пролаза в папку протоколов
  local fileName
  fileName = "Leak_radar_".. radarSn ..".bin"
  CopyFile(algLeak.workDirectory .. "/" .. fileName, getReportPath(fileName))
  fileName = "Leak_radar_".. radarSn ..".h"
  CopyFile(algLeak.workDirectory .. "/" .. fileName, getReportPath(fileName))
  --------------------------------  
  GUI.OnBreak = nil

  if (algLeak.mmapDrv) then
    -- Закроем драйвер MMAP
    algLeak.alg.mmapClose(algLeak.mmapDrv)
    end
  
  if (algLeak.frameCount < LEAK_POINT_NUM) then
    -- Что-то пошло не так... не то количество кадров...
    GUI.Log("Ошибка", "red")
    return "ABORT"
  end
    
  GUI.Log("Конец. Файлы (h,bin) тут:"..getReportPath(), "green")
  
  if (dialogMode) then
	GUI.Dialog("design/rfLeakDone.htm")
  end
  
  return "OK"
end