-- Summary:
-- [upd] module: 22.02.2022 Правки для автоматизации снятия калибровки
-- [upd] module: 09.06.2022 Убрал лишнюю функцию для снятия данных, правка имени bag файлов

-- Description:
-- Исправил ситуацию, когда запись данных прекращалось после сбоя в работе радара.
-- 

-- Testing Done:
-- Снял калибровочные данные для 4х радаров: 270017337, 270077745, 270146351, 270021270. 
-- Отладка в процессе снятия данных. 


-- Определим обработчик ошибки ()
function FatalError(code)
  log(string.format("FatalError> code : %s", code))
  -- аварийное приведение в исх. состояние
  mcFunc("EXIT", 1)
end

function TEST_SPECTRUM()
  SPECTRUM.OnError = FatalError

  mode = SPECTRUM.SetMode("RTSA")
  print("Mode: ", mode)
  pause(1000*1000) -- 1.0c
  Freq = 24E9
  SPECTRUM.SetFreq(Freq)
  print("SET FREQ: 24GHz")
  SPECTRUM.SetSpan() -- FULL SPAN
  print("SET FULL SPAN")
  pause(1000*1000) -- 1.0c
  print("START CHECK...")
  for i = 1, 20 do
    Freq = Freq + 5E6
    f, dB = SPECTRUM.SetMarker(Freq)
    if (dB > -999) then
      -- -1000.00dB - Error level
      print(string.format("STEP+:%d> FREQ[%.4fGHz]:%.2fdB", i, f/1E9, dB))
    end
    pause(500*1000) -- 0.5c
  end
  for i = 1, 20 do
    Freq = Freq - 5E6
    f, dB = SPECTRUM.SetMarker(Freq)
    print(string.format("STEP-:%d> FREQ[%.4fGHz]:%.2fdB", i, f/1E9, dB))
    pause(500*1000) -- 0.5c
  end
  SPECTRUM.PrintScreen("Test1.png")
  SPECTRUM.SaveData("Test1.csv")
end

function TEST_POWER()
  Voltage = 0.0
  --POWER.Connect()
  print("START!!!")
  POWER.SetVoltage(0.0)
  POWER.SetCurrent(1)
  pause(2000*1000) -- 1c
  POWER.SetOutput(1)
  for i = 1, 20 do
    Voltage = Voltage + 0.5
    POWER.SetVoltage(Voltage)
    I, V = POWER.GetMeasure(FatalError, "POWER")
    print("POWER:", I, V)
    pause(200*1000) -- 1c
  end
  POWER.SetVoltage(0.0)
  POWER.SetOutput(0)
end

function TEST_RF_GEN()
  Freq = 24E9
  dBm  = -130
  RF_GEN.OnError = FatalError
  RF_GEN.SetPower(-130)
  for i = 1, 20 do
    Freq = Freq + 5E6
    dBm  = dBm  + 1
    if (dBm > -120) then
      dBm = -130
    end
    f = RF_GEN.SetFreq(Freq)
    RF_GEN.SetPower(dBm)
    print(string.format("STEP+:%d> FREQ[%.4fGHz]", i, f/1E9))
    pause(500*1000) -- 0.5c
  end
end

function TEST_RF_GEN()
  RADAR.SaveStream(string.format("new.000.%.3d.t25.bag", 0), 10)
end

function TEST_OPU()
  local i
  local j
  local Azimuth
  if (OPU.Init() >= 0) then
    log("OPU> ------- START --------")
    -- Установим начальную позицию.
    for j = 1, 5 do 
      OPU.SetAzimuth(0)
      log("OPU> Azimuth(0)")
      for i = 1, 10 do
        pause(100*1000) -- 0.5c
        Azimuth = i*5
        log(string.format("OPU> Azimuth : %d", Azimuth))
        OPU.SetAzimuth(Azimuth)
      end
    end
    OPU.DeInit()
  else
    log("OPU> ------- ERROR  --------")

  end
end

function TEST_OPU_AND_CPRR()
  local i
  local n
  local Azimuth
  if (OPU.Init() >= 0) then
    log("OPU> ------- START --------")
    -- Установим начальную позицию.
    OPU.SetAzimuth(0)
    log("OPU> Azimuth(0)")
    for i = 1, 10 do
      pause(100*1000) -- 0.5c
      Azimuth = i*5
      log(string.format("OPU> Azimuth : %d", Azimuth))
      OPU.SetAzimuth(Azimuth)
      n = RADAR.SaveStream(string.format("new.000.%.3d.t25.bag", i), 10)     
      if (n) then
        log(string.format("WRITE %d frames", n))
      end
    end
    OPU.DeInit()
  else
    log("OPU> ------- ERROR  --------")

  end
end

function TEST_GUI()
  for i = 1, 100 do
    GUI.Log("TEST LOG"..i)
    GUI.Log("TEST LOG(!)"..i)
    pause(1000*1000) -- 0.5c
  end
end

function TEST_RADAR_TARGET()

  GUI.Log("RADAR>T25 RUN...")
  --RADAR.GetTargets(10)
  RADAR.SaveStream("config.bag", 1, 4)



end

function TEST_MODULE_DEMO()
  local i
  -- HTML Цвета (https://colorscheme.ru)
  local colors = {"#000000", "Red", "Lime", "Crimson", "Green", "Coral", "Teal", "Gold", "Aqua", "SkyBlue", "Blue", "Brown", "Gray", "Black"};
  GUI.Clear()
  GUI.Progress(0)
  for i = 1, 10 do
    GUI.View("Какой-то парам.№"..i, "0."..i)
  end
  for i = 1, 10000 do
    GUI.Progress(i*10)
    GUI.Log("RADAR>TEST LOG"..i, colors[i])
    pause(1000) -- 1.0c
    GUI.View("Генератор, МГц", i*1111.1)
    if (GUI.isBreak) then 
      break;
    end
  end
  GUI.View("Результат", "НОРМА")
  GUI.Log("RADAR>DONE")
  GUI.Done(1)

end


function TEST_RADAR_TARGET_xN()

  log("RADAR>T25 test run...")
  for i = 1, 10 do
    log("RADAR> #"..i)
    RADAR.GetTargets(10)
  end
  log("RADAR>T25 test done...")
end

-- Тест наличия радара. Запрос 1 кадра стрима.
-- При запуске режима стрима или трекинга, ядро запрашивает версию радара
-- --> после удачной записи стрима, обновятся параметры RADAR.[SN, Temperature, Version]
function checkForStreamRadar()
  local tryCount
  local result
  if (RADAR)and(GUI) then
    -- данный код, с запросом "tmp.bag" не нужен!
    log("-----------------------> Проверка наличия радара...")
    for tryCount = 1, 1 do
      --log(("-----------------------------------------------> Попытка получить данные с радара..."))
	  --RADAR.StreamEvent = function (time, GNumber, t1, t2, t3, t4, t5, t6);
	  --DELTA = math.abs(radarTemp - t1);
	  
      result = RADAR.SaveStream("tmp.bin", 1)
      if (result > 0) then
        --log(string.format("WAIT> GN: %d Температура:  [%d %d %d %d %d %d] dt = %d", GNumber, t1, t2, t3, t4, t5, t6, dt))
        return 1
      end
    end
    log("-----------------------> Проверка наличия радара...Ошибка! Радар не отвечает!")
  end
  return nil
end

function WRITE_DATASET(N,sfx)
  if (checkRadar()) then
    local filename = sfx..RADAR.SN .. os.date("_%Y-%m-%d_%H-%M-%S")..".t25"..".bag"
    local event = RADAR.StreamEvent

    GUI.Log(string.format("Запись датасета... [%s], %d Кадров", filename, N))

	local folder = string.format("%s/%s", BAG_SAVE_FOLDER, RADAR.SN)
    log("DIR: >>> "..folder)
	CreateDir(folder)
	CALIB_DATASET_LAST_PATH = folder
	
    RADAR.StreamEvent = function (time, GNumber, t1, t2, t3, t4, t5, t6)
      log(string.format("STREAM> GN: %d Температура:  [%d %d %d %d %d %d]", GNumber, t1, t2, t3, t4, t5, t6))
    end


	local filepath = folder..'/'
    if (RADAR.SaveStream(filepath..filename, N) == N) then
      GUI.Log("Датасет записан успешно.")
    else
      GUI.Log("Ошибка записи датасета!")
    end
    RADAR.StreamEvent = event
  else
    GUI.Log("Ошибка! Нет связи с радаром!", "red")
  end
end

radarTemp = 0
dspTemp = 0
radarTemp_prev = 0;

--dTest = -40;
--dTest_cnt = 0;
function GET_TEMP_DATASET(STEP_TEMP)
    local filename = "TEST.bag"
    RADAR.StreamEvent = function (time, GNumber, t1, t2, t3, t4, t5, t6)
		--dt = math.abs(radarTemp - t1);
		local dt = math.abs(radarTemp_prev - t1);
    
    --dTest_cnt = dTest_cnt + 1;
    --if (dTest_cnt > 9) then
    --  dTest_cnt = 0;
    --  dTest = dTest + 10;
    --  dt = 10;
    --end
    
    --t6 = dTest;
    
		local scaleG = 0;
		-- Отрицательные температуры снимаю с меньшим шагом
		-- затем переключаюсь на увеличенный шаг
		if (t1 < 370) then
		scaleG = 1;
		else
		scaleG = 3;
		end
		  log(string.format("WAIT> GN: %d Температура:  [%d %d %d %d %d %d] dt = %d", GNumber, t1, t2, t3, t4, t5, t6, dt))
		  if (dt >=  STEP_TEMP*scaleG) then
			radarTemp = t1;
			dspTemp = t6;
			log(string.format("-------------------------------------------> SAVE CURRENT TEMP"));
			return "OK"
		  end
      
		return "IGNORE"
    end

    if (RADAR.SaveStream(filename, 1) == 1) then
		log(string.format("------------------------> OK "));
		
      return 1
    else
	  
	  log(string.format("------------------------> OUT RETURN 0 "));
      return 0
    end
    
end

function TEST_WRITE_CALIB_DATASET(N)
  if (checkForStreamRadar()) then
    
    local event = RADAR.StreamEvent

    GUI.Log(string.format("Запись датасета... , %d Кадров", N))
	
	local folder = string.format("%s/%s", BAG_SAVE_FOLDER, RADAR.SN)
    log("DIR: >>>"..folder)
    
    CreateDir(folder) 
	-- сохраним последний путь к калибровкам. При расчете калибровки он будет использоваться
	CALIB_DATASET_LAST_PATH = folder
    print("CALIB_DATASET_LAST_PATH:", CALIB_DATASET_LAST_PATH)
    
    if (GET_TEMP_DATASET(2) == 1) then
      RADAR.StreamEvent = function (time, GNumber, t1, t2, t3, t4, t5, t6)
		
        log(string.format("WRITE> GN: %d Температура:  [%d %d %d %d %d %d]", GNumber, t1, t2, t3, t4, t5, t6))
      end
      local dspTemp_str = '';
      if (dspTemp >= 0) then
        dspTemp_str = string.format("+%02d", dspTemp);
      else
        dspTemp_str = string.format("%03d", dspTemp);
      end
      local filename = "calib_temper_".. string.format("%d_", radarTemp) ..RADAR.SN .. os.date("_%Y-%m-%d_%H-%M-%S").. string.format("_%s", dspTemp_str) ..".t25"..".bag";
	    --N = 10;
	  local filepath = folder..'/'
      if (RADAR.SaveStream(filepath..filename, N) == N) then
        GUI.Log("Датасет записан успешно.")
		    radarTemp_prev = radarTemp; -- для восстановления работы после сбоя
      else
		
        GUI.Log("Ошибка записи датасета!")
      end
      RADAR.StreamEvent = event
      
    end
    

  else
	
    GUI.Log("Ошибка! Нет связи с радаром!", "red")
  end


end

dofile("./tb.script/tests/test_lua_test.lua")

function READ_CONFIG()
  if (checkForStreamRadar()) then
    --local event = RADAR.StreamEvent
    local folder = string.format("%s/%s", BAG_SAVE_FOLDER, RADAR.SN);
    if tempACode == 0 then
      log("DIR:"..folder)
          CreateDir(folder) 
      
      log("Чтение конфигурации радара...")
      local tryCount, getConfigResult
      for tryCount = 1, 3 do
        getConfigResult = RADAR.SaveStream(folder .."/config.bag", 1, 4)
        if (getConfigResult > 0) then
          log("Конфигурация радара прочитана.")
          break
        else
          log("Во время чтения конфигурации радара возникла ошибка!")
          -- return "ABORT"
        end
      end
    end  
  end
end

tempACode = 0 
cnt_CalibDone = 0; 
function WRITE_CALIB_DATASET(N)
  GUI.Progress(cnt_CalibDone)
  if (checkForStreamRadar()) then
    local event = RADAR.StreamEvent
	
    local folder = string.format("%s/%s", BAG_SAVE_FOLDER, RADAR.SN)
    if tempACode == 0 then
      log("DIR:"..folder)
          CreateDir(folder) 
      
      log("Чтение конфигурации радара...")
      local tryCount, getConfigResult
      for tryCount = 1, 3 do
        getConfigResult = RADAR.SaveStream(folder .."/config.bag", 1, 4)
        if (getConfigResult > 0) then
          log("Конфигурация радара прочитана.")
          break
        else
          log("Во время чтения конфигурации радара возникла ошибка!")
          -- return "ABORT"
        end
      end
	end
	CALIB_DATASET_LAST_PATH = folder
    print("CALIB_DATASET_LAST_PATH:", CALIB_DATASET_LAST_PATH)
	GUI.View("Путь к калибам", CALIB_DATASET_LAST_PATH)

	
    local tempCode = 0;
    tempCode = EEPROM_LUA_READ_TEMP_CODE(0);
	if tempCode then 
		if ( math.abs(tempACode - tempCode) > 2) then -- было 4
        cnt_CalibDone = cnt_CalibDone + 1;
      
		  GUI.Log(string.format("Запись датасета... , %d Кадров", N))
		  tempACode = tempCode;
		  local dspTemp_str = '';
		  if (dspTemp >= 0) then
			dspTemp_str = string.format("+%02d", dspTemp);
		  else
			dspTemp_str = string.format("%03d", dspTemp);
		  end
		  local filepath = folder..'/'
		  local filename = "calib_temper_".. string.format("%d_", tempACode) ..RADAR.SN .. os.date("_%Y-%m-%d_%H-%M-%S") ..".t25"..".bag";
		  if (RADAR.SaveStream(filepath..filename, N) == N) then
			GUI.Log("Датасет записан успешно.")
				radarTemp_prev = radarTemp; -- для восстановления работы после сбоя
		  else   
			GUI.Log("Ошибка записи датасета!")
		  end      
		end
	end
    
    pause(5000*1000) -- 5.0c
  else
	  GUI.Log("Ошибка! Нет связи с радаром!", "red")
  end
  
  --GUI.OnBreak = nil
  return "OK";
  
end