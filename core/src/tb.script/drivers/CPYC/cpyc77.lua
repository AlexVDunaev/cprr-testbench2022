

CPYC_UDP_IP = "192.168.1.1" -- default IP
CPYC_UDP_PORT = 1772         -- default port
CPYC_UDP_PORT_LOCAL = nil    -- default local port

CPYC_LastCmd = ""
CPYC_FrameCount = 0


-- os.date("%Y-%m-%d %H-%M-%S") - получение времени, если нужно создавать файл имя-дата

-- Установка режима радара на получение сырых данных, разных форматов
-- mode == "STREAM" : Штатная RF конфигурация. size - кол-во кадров, прием стрима
-- mode == "T25" : Штатная RF конфигурация. size - кол-во кадров, прием целей
-- mode == "TESTER" : Штатная RF конфигурация. size - кол-во кадров, прием спец. сообщений
-- mode == "ADC" : Частная RF конф. Непрерывный сбор данных. size - кол-во отсчетов АЦП.

cpyc77retry = 0

function CPYC_SaveStream(filename, frameNum, mode) 
  -- Перевод в режим стрима и запись в файл frameNum кадров
  CPYC_FrameCount = 0
	  cpyc77retry = 0
  if (mode) then
    result = mcLinkCtrl(CPYC.Line, "STREAM:"..mode, filename, frameNum)
  else
    result = mcLinkCtrl(CPYC.Line, "STREAM", filename, frameNum)
  end
  log(string.format("SaveStream : write num = %d", result)) 
  return result
  -------------------------------------------------------
end

function CPYCLineEvent(idEvent, header, LineName, ptrData, sizeOfData)
  --print("CPYCLineEvent: ", idEvent, LineName, #header)

  if (idEvent == "INIT") then
    mcFunc("TIMEOUT",10000)
    if (CPYC_UDP_PORT_LOCAL) then
      mcLinkOpen (CPYC.Line, "CPYC77", CPYC_UDP_IP, string.format("%d;%d", CPYC_UDP_PORT, CPYC_UDP_PORT_LOCAL), "DUMMY")
    else
      mcLinkOpen (CPYC.Line, "CPYC77", CPYC_UDP_IP, string.format("%d", CPYC_UDP_PORT), "DUMMY")
    end
  end

  if (idEvent == "UP") then
	log("CPYCLineEvent> UP")
    if (CPYC.StreamerUpEvent) then
	  CPYC.StreamerUpEvent()
    end
  end
  if (idEvent == "DOWN") then
	log("CPYCLineEvent> UP")
 	if (CPYC.StreamerDownEvent) then
	  CPYC.StreamerDownEvent()
	end
  end
  
  
--  "RX" EVENT (STREAM MODE): header : [  "" | HEADER ]
--   header = "" : TIMEOUT или ERROR
--   header = HEADER - заголовок кадра.
--   return: "OK", "" - действия по умолчанию (запись кадра)
--   return: "OK", header - возможность изменить содержимое заголовока перед записью
--   return: "IGNORE", "" - игнорировать кадр
--   return: "BREAK", "" -  прервать запись стиминга
--   return: "RETRY", "" -  выдать команду start
  if (idEvent == "RX") then
    if (header == "") then
      -- CPYC DRIVER> TIMEOUT или ERROR
      log(string.format("CPYCLineEvent>[RX = nil] : header size: %d --> BREAK", #header)) 
	  cpyc77retry = cpyc77retry + 1
	  if cpyc77retry > 3 then
	  cpyc77retry = 0
		return "BREAK", "" --> STREAMING EXIT
		end
	  return "RETRY", "" --> START RADAR
    end

    local radarId
    local timeStamp
    local frameIndex
	local serialNum

    if (#header >= 24) then 
      radarId, timeStamp, frameIndex, serialNum = string.unpack("I4I8I8I4", header)
      --log(string.format("%d. SN%d: FRIME[%d, %d]:%d", radarId, serialNum, timeStamp, frameIndex, sizeOfData)) 
	  RadarSN = serialNum
	  CPYC.SN = serialNum
      cpyc77retry = 0
	  if (CPYC.StreamEvent) then
		local resultStatus
		local resultAction
		resultStatus, resultAction = CPYC.StreamEvent(radarId, serialNum, timeStamp, frameIndex, ptrData, sizeOfData)
		if (resultStatus) then
		  return resultStatus, ""
		end
	  end
    else
      log(string.format("CPYCLineEvent>[RX] : header size: %d", #header)) 
    end

    return "OK", ""
  end

  return "OK", ""
end



function CPYC_Init(IP, PORT, PORT_LOCAL)
  if (DriverLineId) then
    DriverLineId = DriverLineId + 1
  else
    DriverLineId = 1
  end
  log(string.format("CPYCInit( [IP: %s:%d] )", IP, PORT)) 

  CPYC = {}
  CPYC.Line       = DriverLineId
  CPYC.SaveStream = CPYC_SaveStream
  CPYC.TrackingEvent = nil
  CPYC.StreamEvent = nil
  CPYC.StreamerUpEvent = nil -- событие, когда стример перешел из состояния DOWN -> UP
  CPYC.StreamerDownEvent = nil -- событие, когда стример перешел из состояния UP -> DOWN
  -- Заводской номер радара (последний полученный)
  CPYC.SN         = nil
  -- температура радара (последнее полученное значение)
  CPYC.Temperature= 0
  -- Версия радара
  CPYC.Version    = nil

  if (IP) then -- nil --> use default
    CPYC_UDP_IP = IP
  end
  if (PORT) then -- nil --> use default
    CPYC_UDP_PORT = PORT
  end

  CPYC_UDP_PORT_LOCAL = PORT_LOCAL

  addObject("LINE",   "RADAR_CPYC",  CPYC.Line, "[nUse]", CPYCLineEvent)

  RadarSN = 0
  
  return CPYC
end

