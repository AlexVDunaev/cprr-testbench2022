

DAQ_UDP_IP = "127.0.0.1"    -- default IP
DAQ_UDP_PORT = 8080         -- default port


function DAQ_SendFrameInfo(time64, frameId) 
-- log(string.format("DAQ> %s", cmd)) 
  mcLinkTx (DAQ.Line, string.pack("i1i8i8", 2, time64, frameId))
end

function DAQ_SendVersion(protokol_ver, radar_ver) 
  mcLinkTx (DAQ.Line, string.pack("i1i8i8", 1, protokol_ver, radar_ver))
end

function DAQ_SendRaw(pkt) 
  mcLinkTx (DAQ.Line, string.pack("i1", 0) .. pkt)
end


function DAQLineEvent(idEvent, buffer, LineName, ptrData, sizeOfData)
  print("DAQLineEvent: ", idEvent, LineName, #buffer)

  if (idEvent == "INIT") then
    mcLinkOpen (DAQ.Line, "DAQ", DAQ_UDP_IP, string.format("%d", DAQ_UDP_PORT), "DUMMY")
  end
--  "RX" EVENT 
--   buffer = 1 : Команда ENABLE : 1
--   buffer = 0 : Команда ENABLE : 0
--   return: статус возврата игнорируется
  if (idEvent == "RX") then

    if (#buffer >= 1) then 
      daq_enable = string.unpack("i1", buffer)
      log(string.format("DAQLineEvent>[RX] : [INFO] enable: %d", daq_enable)) 
      if (daq_enable > 0) then
        runTestModule = "DO_STREAMING"
        end
    else
      log(string.format("DAQLineEvent>[RX] : buffer size: %d", #buffer)) 
    end
  end

  return "OK", ""
end



function DAQ_Init(IP, PORT, PORT_LOCAL)
  if (DriverLineId) then
    DriverLineId = DriverLineId + 1
  else
    DriverLineId = 1
  end
  log(string.format("DAQInit( [IP: %s:%d] )", IP, PORT)) 

  DAQ = {}
  DAQ.Line       = DriverLineId
  DAQ.SendFrameInfo = DAQ_SendFrameInfo
  DAQ.SendVersion= DAQ_SendVersion
  DAQ.SendRaw    = DAQ_SendRaw

  if (IP) then -- nil --> use default
    DAQ_UDP_IP = IP
  end
  if (PORT) then -- nil --> use default
    DAQ_UDP_PORT = PORT
  end

  addObject("LINE",   "DAQ",  DAQ.Line, "[nUse]", DAQLineEvent)

  return DAQ
end

