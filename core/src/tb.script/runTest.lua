-- Summary:
-- [upd] module: 22.02.2022 Правки для автоматизации снятия калибровки

-- Description:
-- Сделал вечный цикл для снятия калибровки CALIB_FUNC
-- 

-- лучше сразу загрузить файлы испытаний, что бы проверить корректность синтаксиса на старте
dofile("./tb.script/tests/demo.lua")
dofile("./tb.script/tests/power.lua")
dofile("./tb.script/tests/radarTest.lua")
dofile("./tb.script/tests/genStep.lua")
dofile("./tb.script/tests/rfnoise.lua")
dofile("./tb.script/tests/refAngle.lua")
dofile("./tb.script/tests/eeprom_lua_test.lua")
dofile("./tb.script/tests/test_lua_test.lua")
dofile("./tb.script/tests/rfLeak.lua")
dofile("./tb.script/tests/rfCalib.lua")
--
dofile("./tb.script/tests/cpyc77Test.lua")
dofile("./tb.script/tests/cprrrawsignal.lua")
dofile("./tb.script/tests/cpyc/cpycAngle.lua")
dofile("./tb.script/tests/cpyc/cpycCalib.lua")
dofile("./tb.script/tests/cpyc/cpycNoise.lua")
dofile("./tb.script/tests/cpyc/cpycModes.lua")
dofile("./tb.script/tests/cpyc/cpycPower.lua")
dofile("./tb.script/tests/cpyc/cpycTarget.lua")


-- Имя теста, запускаемого при старте
runTestModule = nil --"CALIB" -- nil !!!

-----------------------------------------
--      Функции запуска тестов         --
-----------------------------------------
-- Контроль АЧХ (БЭК)
function RFGEN(dialogMode)
  -- Повторная загрузка скриптов, для возможности правки между запусками
  dofile("./tb.script/tests/genStep.lua")
  TEST_MODULE_GENDOWN(dialogMode)
end

-- Контроль уровня шума (БЭК)
function RFNOISE(dialogMode)
  dofile("./tb.script/tests/rfnoise.lua")
  TEST_MODULE_RF_NOISE(dialogMode)
end

-- Контроль положения цели (БЭК)
function REFANGLE(dialogMode)
  dofile("./tb.script/tests/refAngle.lua")
  TEST_MODULE_REFANGLE(dialogMode)
end

-- Диаграмма направленности 
function BEAMFIND(dialogMode)
  dofile("./tb.script/tests/refAngle.lua")
  TEST_BEAM_FORMING(dialogMode);
end

-- Три теста в БЭК
function BEK_ALL(dialogMode)
  RFGEN(dialogMode)
  RFNOISE(dialogMode)
  REFANGLE(dialogMode)
end

-- Контроль питания
function POWERTEST(dialogMode)
  dofile("./tb.script/tests/power.lua")
  TEST_MODULE_POWER(dialogMode)
end

-- Демо. Для отладки
function DEMO(dialogMode)
  dofile("./tb.script/tests/demo.lua")
  --TEST_POWER(dialogMode)
  TEST_MODULE_DEMO()
end

function STREAM100(dialogMode)
  dofile("./tb.script/tests/demo.lua")
  WRITE_DATASET(100, "TEST_")
end

function READ_CPRR_CONFIG(dialogMode)
  dofile("./tb.script/tests/demo.lua")
  READ_CONFIG();
end

function STREAM500(dialogMode)
  dofile("./tb.script/tests/demo.lua")
  WRITE_DATASET(500, "LEAK_")
end

function CALIB_FUNC(dialogMode)
  dofile("./tb.script/tests/demo.lua")
  CPRR_LUA_OFF_WDT(dialogMode)
  local i = 0
  GUI.OnBreak = function ()
	BREAK = 1
	log("BREAK!")
	end
  
  while BREAK == nil do
    log("----------------------------------> TEST_WRITE_CALIB_DATASET <----------------------------------")
    TEST_WRITE_CALIB_DATASET(CALIB_CPRR_FRAME_NUM) -- 500 кадров избыточно
  end
  GUI.Log("CALIB_FUNC...Done", "green")
  
end

function CALIB_FUNC_TELNET(dialogMode)
  dofile("./tb.script/tests/demo.lua")
  CPRR_LUA_OFF_WDT(dialogMode)
  GUI.Log("Зашёл 0 ----------------------")
  local i = 0
  GUI.OnBreak = function ()
	BREAK = 1
	log("BREAK!")
	end
  while BREAK == nil do
    log("----------------------------------> TEST_WRITE_CALIB_DATASET <----------------------------------")
    WRITE_CALIB_DATASET(CALIB_CPRR_FRAME_NUM);
	EEPROM_LUA_START(dialogMode);
  end
  GUI.Log("CALIB_FUNC_TELNET...Done", "green")

end
-----------------------------------------



-- прием команд от GUI. 

-- Специальные команды: EXIT
function onEventCommand(cmd, value)
  log("GUI CMD> "..cmd.." --> "..value)
  
  if (cmd == "CMD") and (value == "EXIT") then
    GUI.Log("EXIT")
    mcFunc("EXIT",1)  
  end
 if cmd == "CMD" then
    if value == "BREAK" then
      BREAK = 1
      -- TODO: нужно выполнить корректное завершение проверки
    end
   --runTestModule = value
  end
  if cmd == "DIALOG" then
    log("GUI DIALOG> "..cmd.." --> "..value)
    userReady = 1
    end
end
-- События выбора теста
function onRunTest(testName)
  runTestModule = testName
end

-- Функция - запускатель тестов
function runCurrentTest(runTestModule)
  local result

  if runTestModule then
    -- GUI.Log("[TEST RUN] --> [ " .. runTestModule.." ]")
    local dialogMode = 1 -- [ ANY VALUE | nil]
	BREAK = nil

    if (runTestModule == "DEMO") then
      DEMO(dialogMode)
    end
    if (runTestModule == "POWERTEST") then
      POWERTEST(dialogMode)
    end
    if (runTestModule == "BEK_ALL") then
      BEK_ALL(nil)
    end
    if (runTestModule == "RFGEN") then
      RFGEN(dialogMode)
    end
    if (runTestModule == "RFNOISE") then
      RFNOISE(dialogMode)
    end
    if (runTestModule == "REFANGLE") then
      REFANGLE(dialogMode)
    end
    
    if (runTestModule == "BEAMFIND") then
      GUI.Log("[TEST RUN] --> [ " .. runTestModule.." ]")
      BEAMFIND(dialogMode)
    end

    if (runTestModule == "STREAM100") then
      STREAM100(dialogMode)
    end
	if (runTestModule == "STREAM500") then
      STREAM500(dialogMode)
    end

	if (runTestModule == "LEAK500") then
		dofile("./tb.script/tests/rfLeak.lua")
		TEST_MODULE_RF_LEAK(dialogMode)
    end
	
    if (runTestModule == "READCONFIG") then
	  dofile("./tb.script/tests/rfCalib.lua")
      READ_CPRR_CONFIG()
    end
    
    if (runTestModule == "CALIBCALC") then
	  dofile("./tb.script/tests/rfCalib.lua")
      TEST_CPRR_CALIB(dialogMode)
    end
	
    if (runTestModule == "CALIBDIRCALC") then
		if (mcGetParam("args", "woradar", "0") == "1") then
			-- для второй копии обновим конфиг. Что бы задавать руками путь без перезапуска tbtool
			dofile("./config.lua")
			end
	  dofile("./tb.script/tests/rfCalib.lua")
	  print("CALIB_DATASET_LAST_PATH:", CALIB_DATASET_LAST_PATH)
	  local calibpath = CALIB_DATASET_LAST_PATH
	  if calibpath == nil then
		--calibpath = [[D:\CPRR\Manufacture\2022.07.04\SN.418109722\raw_data]]
		calibpath = CALIB_CALC_PATH
	  end
      TEST_CPRR_CALIB_DIR_PROCESS(calibpath, dialogMode)
    end
	
	-- пакетная обработка калибов
    if (runTestModule == "CALIBPACKCALC") then
		if (mcGetParam("args", "woradar", "0") == "1") then
			-- для второй копии обновим конфиг. Что бы задавать руками путь без перезапуска tbtool
			dofile("./config.lua")
			end
	  dofile("./tb.script/tests/rfCalib.lua")
	  print("CALIB_DATASET_PACK_PATH:", CALIB_DATASET_PACK_PATH)
      TEST_CPRR_CALIB_PACK_PROCESS(CALIB_DATASET_PACK_PATH, dialogMode)
    end

	
    if (runTestModule == "CALIB") then
      CALIB_FUNC(dialogMode)
    end
    
    if (runTestModule == "CALIB_HTM_TELNET") then
      CALIB_FUNC_TELNET(dialogMode)
    end
        
    if (runTestModule == "TEST_CPYC77") then
      DO_TEST_CPYC77()
	  mcFunc("EXIT",1)  
    end
    if (runTestModule == "ANGLE77") then
	  dofile("./tb.script/tests/cpyc/cpycAngle.lua")
	  result = CPYC_SET_MODE("FAR", nil, dialogMode, "upLoadCalib!")
		if result ~= "ABORT" then
	       result = TEST_CPYC_ANGLE("FAR", dialogMode)
		   if result ~= "ABORT" then
				result = CPYC_SET_MODE("FIELD", nil, dialogMode, "upLoadCalib!")
				if result ~= "ABORT" then
					result = TEST_CPYC_ANGLE("FIELD", dialogMode)
					if result ~= "ABORT" then
						result = CPYC_SET_MODE("NEAR", nil, dialogMode, "upLoadCalib!")
						if result ~= "ABORT" then
							result = TEST_CPYC_ANGLE("NEAR", dialogMode)
							if result ~= "ABORT" then
								GUI.Log("Проверка завершилась:"..result)
							end
						end
					end
				end
		   end	
		end
		   
		   GUI.Log("Проверка завершилась:"..result)
		
	  -- 
	  --mcFunc("EXIT",1)  
    end
    if (runTestModule == "CPRR_TestRawSignal") then
	  dofile("./tb.script/tests/cprrrawsignal.lua")
      CPRR_TEST_RAWSIGNAL(dialogMode)
	  --mcFunc("EXIT",1)  
    end
    if (runTestModule == "CALIB77") then
	  dofile("./tb.script/tests/cpyc/cpycCalib.lua")
      --TEST_CPYC_CALIB(nil, dialogMode)
		result = TEST_CPYC_CALIB_MODE("FIELD", dialogMode)
		if result ~= "ABORT" then
		   GUI.Log("Калибровка завершилась!")
		end
	  
    end
    if (runTestModule == "NOISE77") then
	  dofile("./tb.script/tests/cpyc/cpycNoise.lua")
	  dofile("./tb.script/tests/cpyc/cpycModes.lua")
      result = TEST_CPYC_NOISE("FAR", dialogMode)
	  if result ~= "ABORT" then
       TEST_CPYC_NOISE("FIELD", dialogMode)
       TEST_CPYC_NOISE("NEAR", dialogMode)
	   -- Восстановим нормальный режим радара! Иначе остальные проверки не пройдут!
	   CPYC_SET_MODE("NONE", 7, dialogMode) -- Перезапуск радара
	   end
	   GUI.Log("Конец!")
    end
    if (runTestModule == "TESTMODE77") then
	  dofile("./tb.script/tests/cpyc/cpycModes.lua")
      TEST_CPYC_SET_MODE(dialogMode)
    end
    if (runTestModule == "POWER77") then
	  dofile("./tb.script/tests/cpyc/cpycPower.lua")
      TEST_CPYC_POWER(dialogMode)
    end
    if (runTestModule == "ALL77") then
	  dofile("./tb.script/tests/cpyc/cpycPower.lua")
	  dofile("./tb.script/tests/cpyc/cpycNoise.lua")
	  dofile("./tb.script/tests/cpyc/cpycModes.lua")
	  dofile("./tb.script/tests/cpyc/cpycCalib.lua")
	  powerShowedDialogFlag = nil
	  result = "OK"
	  if (POWER) then
	    result = SetRadarPower(1, dialogMode)
		CPYC_PAUSE(5, "ожидание перезапуска радара после включения ИП...")
		end
		
	  if result == "OK" then
		  
		  result = TEST_CPYC_CALIB_MODE("FAR", dialogMode)
		  if result ~= "ABORT" then
			result = TEST_CPYC_CALIB_MODE("NEAR", dialogMode)
			  if result ~= "ABORT" then
				result = TEST_CPYC_CALIB_MODE("FIELD", dialogMode)
				if result ~= "ABORT" then
				   GUI.Log("Калибровка завершилась!")
				   CPYC_SET_WORK_MODE(dialogMode)
				end
			  end
		  end
		  if result == "ABORT" then
		   GUI.Log("Калибровка прервана!!!")
		  end
	  end
    end
	
    if (runTestModule == "ANGLE77TG") then
	  dofile("./tb.script/tests/cpyc/cpycModes.lua")
	  dofile("./tb.script/tests/cpyc/cpycTarget.lua")
	
	  result = CPYC_TARGET_MODE("NEAR", dialogMode)
	  if result == "ABORT" then
	   GUI.Log("Проверка целей прервана!!!")
	  end
    end
	
	
	


    if (runTestModule == "CONNECT_TEST") then
      TEST_RADAR_TARGET_xN()
    end
    if (runTestModule == "RADARALGTEST") then
      -- обновим файл проверки (можно править между пусками проверки...)
      dofile("./tb.script/tests/radarTest.lua")
      switch_to_raw_mode()
      --while 1 == 1 do
      local result = TEST_RADAR_ALG_CMP()
      REPORT.new(RadarSN, "Тест алгоритма радара")
      if ((result)and(result > 0)) then
        REPORT.add("РЕЗУЛЬТАТ: ОК")
      end
      REPORT.done()
      --end
      --mcFunc("EXIT",1)  
    end
    if (runTestModule == "TESTDIALOG") then
      GUI.Log("WAIT...")
      GUI.Dialog("design/rfgenErr.htm", "wait") --, "TEXT кода ошибки...!!")
      GUI.Log("USER READY")
    end
    
    if (runTestModule == "EEPROMTEST") then
        EEPROM_LUA_START(dialogMode)
    end

    if (runTestModule == "EEPROMERASE") then
        EEPROM_LUA_ERASE(dialogMode)
    end
    
    if (runTestModule == "EEPROMREAD") then
        EEPROM_LUA_READ(dialogMode)
    end 
    
    if (runTestModule == "EEPROM_TEMP_CODE_READ") then
        EEPROM_LUA_READ_TEMP_CODE(dialogMode)
    end 
    
    if (runTestModule == "EEPROMWRITELEAK") then
        EEPROM_LUA_EEPROMWRITELEAK(dialogMode)
    end

    if (runTestModule == "EEPROMWRITECALIB") then
        EEPROM_LUA_EEPROMWRITECALIB(dialogMode)
    end   

    if (runTestModule == "TEST_TEST") then
        TEST_LUA_START(dialogMode)
    end    
    
    if (runTestModule == "CPRR_HTML_WDT_OFF") then
        CPRR_LUA_OFF_WDT(dialogMode)
    end   
    
    if (runTestModule == "CPRR_HTML_SET_NET_SETTINGS") then
        CPRR_SET_NET_SETTINGS(dialogMode)
    end  
    
    if (runTestModule == "TELNET_HTML_GET_SN") then
        CPRR_GET_SN(dialogMode)
    end      
    -----------------------------------
    -- сюда дополнять запуск циклограмм
    -- .....
    -----------------------------------
  end
end

---------------------------------------
-- Точка входа в программу испытаний --
---------------------------------------
function runTest()
  if (DAQ) then
    -- protokol_ver = 3, radar_ver = 2
    DAQ.SendVersion(3, 2)
    end
	
  local waitCount = 0
  log("TESTING> ------- START --------")
  -- Установим обработчик на прием команд от GUI
  GUI.OnEvent = onEventCommand
  GUI.OnRunTest = onRunTest
  -- Ожидание команды...
  while 1 == 1 do
    if (runTestModule) then
      ------------------------------
      runCurrentTest(runTestModule)
      ------------------------------
      GUI.Done(1)
      runTestModule = nil
    end
    pause(99*1000) -- засыпаем на 0.1c
	if (daq_enable == 0) then
		  if (DAQ) then
			DAQ.SendFrameInfo(0, 0)
			end
		  end	
    -- индикация ожидания 
    waitCount = waitCount + 1
    if waitCount > 50 then
      waitCount = 0
      --log("Wait command...")
      -- GUI.Log("Wait command...")
    end
  end
  log("TESTING> ------- DONE  --------")

end




