----------------------------------------------------------------
-- Главный файл, через который происходит запуск скриптов LUA --
----------------------------------------------------------------

-- Все глобальные константы задавать тут! 

-- Внимание! Все пути относительно tbTool.exe!
-- для windows нужно вместо "../" нужно указывать "..\\". В других случаях нужно использовать "/"
-- Например: ".\\..\\..\\..\\..\\CPRRTesting/Datasets/part001"
--REPORT_BASE_DIR  = "..\\..\\reports"
PART_NUMBER = '2022_08_19'

REPORT_BASE_DIR = ".\\..\\..\\..\\..\\CPRRTesting/reports/" .. PART_NUMBER
BAG_SAVE_FOLDER = ".\\..\\..\\..\\..\\CPRRTesting/Datasets/" .. PART_NUMBER
-- путь к отчетам относительно GUI
REPORT_BASE_DIR_GUI = ".\\..\\..\\..\\CPRRTesting/reports/" .. PART_NUMBER

-- Директория GUI/Алгоритмов относительно TBTOOL.EXE
GUI_ROOT_DIR = ".\\..\\..\\gui"
-- Путь папки в которой лежат калибровочные датасеты (для расчета калибровки постфактум). 
CALIB_CALC_PATH = ".\\..\\..\\..\\..\\CPRRTesting/CurrentCalib"
--CALIB_DATASET_LAST_PATH = nil -- Сюда нужно сохранить директорию, в которую записали калибровку, что бы сразу расчитать калиб. в этой папке
CALIB_DATASET_LAST_PATH = [[D:\work\dunaev\CPRRTesting\Datasets\part001\418127430]]
--CALIB_DATASET_LAST_PATH = [[d:\RADAR\CPRRTesting\Datasets\p7cnt2022_08\418118536]]
-- Путь к папке для пакетной обработке
CALIB_DATASET_PACK_PATH = [[D:\work\dunaev\CPRRTesting\Datasets\part001]]

CALIB_CPRR_FRAME_NUM = 200 -- кол-во кадров калибровки (пишем и обрабатываем)
CURRENT_RADAR_SN = "UNDEF" -- SN радара


cmdOUTPUTFILES = ""


function Init()
  log("Init(). Begin")

  a=mcGetParam("args", "IP")
  print(a)

--  mcFunc("EXIT",1)

  mcFunc("LOGLEVEL",0)


  A = mcGetParam("args", "test") -- tbTool.exe test=1 --> A = "1"

  dofile("./tb.script/initDrivers.lua")
  dofile("./tb.script/runTest.lua")
  log(string.format("Start Init. Lua version=%s",_VERSION))

  DriverLineId = 1
  InitDrivers()
  log("Init(). End")
end


function Run()
  log("Run()")
  runTest()
  mcFunc("EXIT",1)
end

