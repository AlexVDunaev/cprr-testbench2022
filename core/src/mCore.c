#include <string.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "./interfaces/uart/ttys.h"
#include "./interfaces/eth/eth.h"
#include "./protocols/protocols.h"
#include "./core/processing.h"
#include "./core/commandUnit.h"
#include "./core/other.h"
#include "./log/log.h"
#include "./script/script.h"
#include <stdlib.h>
#include "./interfaces/interfaces.h"
#include "core/coreUInterface.h"
#include "utils/argsParam.h"
#include "./synchronize/synchronize.h"
#include "./script/scriptAPI.h"

#include "main.h"
#include "mCore.h"
#include <pthread.h>
#include <errno.h>

int fatal_error(const char * text);

TSystemState SystemState;
char serverVersion[100] = SERVER_TITLE;
pthread_t mcProcessing_thread;
pthread_attr_t attr_mcProcessing_proc;
pthread_t mcREQProcessing_thread;
void mcLinePolling(int lineIndex);

int syncPollingIndex = -1;

void Polling_synchronize(int lock, char * dbgText) {

    if (lock == POLL_SYNC_INIT) {
        Log_DBG("Polling_synchronize> ERROR! 'INIT' Event outlaw! ");
        exit(1);
    } else {
        mcSynchronize(lock, syncPollingIndex, dbgText);
    }
}

int mcTestLib(int arg) {
    return ++arg;
}

int mcTestLibEx(int arg) {
    return ++arg;
}

void* ProcessingThread(void * arg) {
    while (SystemState.running)
        mcProcessing();
    return NULL;
}

void* LinePollingThread(void * arg) {
    int lineIndex = *(char *) arg;
    while (SystemState.running) {
        usleep(1000 * 5); // 1ms -> 5ms
        mcLinePolling(lineIndex);
    }
    return NULL;
}

void REQ_Processing(void) {
    int i, j;

    // Обработка событий POLLING (таймеры LUA. Пока не использую)
    for (i = 0; i < SystemState.PollingUnits.count; i++) {
        j = mcGetDeltaTime(&SystemState.PollingUnits.Polling[i]->lastCallTime, 0);

        if (j >= SystemState.PollingUnits.Polling[i]->nextDelay) {
            mcGetTime(&SystemState.PollingUnits.Polling[i]->lastCallTime); // сохраним время последнего вызова
            if (SystemState.PollingUnits.Polling[i]->source == OBJ_SOURCE_SCRIPT) { // передача управления скрипту
                j = SystemState.pScriptEngine->Events.OnPolling(NULL, SystemState.PollingUnits.Polling[i]->name, SystemState.PollingUnits.Polling[i]->callBackAddr);
                SystemState.PollingUnits.Polling[i]->nextDelay = j;
            }
        }
    }
}

void* REQProcessingThread(void * arg) {
    while (SystemState.running) {
        usleep(1000 * 20); // 20ms 
        REQ_Processing();
    }
    return NULL;
}

extern TMCUI * mcInit(int argc, char **argv, int RemotePort, char mode) {
    int i;
    int emulMode = 0;

    memset(&SystemState, 0, sizeof (SystemState));
    mcSynchronizeInit(argc, argv);
    mcGetArgsInit(argc, argv);
    SystemState.initMode = mode;
    SystemState.argc = argc;
    SystemState.argv = argv;
    SystemState.useGUI = 0;
    mcGetTime(&SystemState.serverStartTime);

    // ------------------------------------------------------------
    // инициализация структуры - интерфейса пользователя с сервером
    SystemState.UI.serverVersion = serverVersion;
    //    SystemState.UI.set = &UI_set_Funcs;
    //    SystemState.UI.signal = &UI_sg_Funcs;
    //    SystemState.UI.notify = &UI_notify_Funcs;

    // ------------------------------------------------------------
    syncPollingIndex = mcSynchronizeByName(MCORE_SYNC_INIT, "POLLING", "mcInit()...");

    pthread_mutex_init(&SystemState.NotifyList.mutex4Notify, NULL);
    SystemState.NotifyList.NotifyTestCount = 0;


    Log_Ex("dbg", LEVEL_TITLE, "-----------------------------------------------");
    Log_Ex("dbg", LEVEL_TITLE, "- %s  -", SERVER_TITLE);
    Log_Ex("dbg", LEVEL_TITLE, "-----------------------------------------------");

    initLinkDrivers(&SystemState);
    initProtocols(&SystemState);

    if (InitSignalValueBase(&SystemState, emulMode, SERVER_SIGNALS_LIMIT) == -1) {
        fatal_error("SIGNAL INIT FAILED!");
        return NULL;
    }

    SystemState.System.isWriteArc = 1;
    SystemState.System.globalBufferedMode = GLOBAL_BUFFERED_MODE_DEFAULT;


    for (i = 1; i < argc; i++) {
        Log_Ex("dbg", LEVEL_TITLE, "arg: %d '%s'", i + 1, argv[i]);
    }

    if (mcGetArgValue("dbg", 'b', NULL)) {
        Log_Ex("dbg", LEVEL_TITLE, "Debug server mode!");
        setLogLevel(LEVEL_DEBUG);
    }
    if (mcGetArgValue("debug", 'b', NULL)) {
        Log_Ex("dbg", LEVEL_TITLE, "Debug server mode!");
        setLogLevel(LEVEL_DEBUG);
        SystemState.fDebug = 1;
        SystemState.fDebugCore = SystemState.fDebug;
    }


    // -------------------------------------------------------
    SystemState.running = 1;
    // Предусмотрел порт управления 
    SystemState.RemotePort = RemotePort;

    for (i = 0; i < SystemState.FramingUnits.count; i++) {
        if (SystemState.FramingUnits.Framing[i]->source == OBJ_SOURCE_EMBEDDED)
            SystemState.FramingUnits.Framing[i]->callBack(PROT_FRAMING_INIT, NULL, NULL, NULL, NULL, NULL);
    }

    return &SystemState.UI;
}//main

int mcStart(void) {
    int i;
    int DriverIndex;

    //После инициализации всех интерфейсов запустим скрипт конфигурации (если нужно)
    SystemState.pScriptEngine = scrInit(SystemState.argc, SystemState.argv, "config.lua");

    if (SystemState.pScriptEngine->scrObject) {
        // Log_DBG("mcStart!");
        for (i = 0; i < SystemState.LLC.count; i++) {
            Log_DBG("Line [%d]: INIT", i);
            TMACDriverArg arg = {.prOPEN.device = "INIT", .prOPEN.reserv1 = 4, .prOPEN.param = NULL, .prOPEN.reserv2 = 0};
            RunLineCallBack(MAC_ID_OPEN, i, arg, SystemState.LLC.Lines[i]->buffers.lastError);
        }

        if (SystemState.initMode & MC_MODE_MULTITHREAD) { // запуск потока опроса
            pthread_create(&mcProcessing_thread, NULL, ProcessingThread, NULL);
            pthread_create(&mcREQProcessing_thread, NULL, REQProcessingThread, NULL);


            // запуск потоков опроса линии (если драйвер потдерживает POLL)
            for (i = 0; i < SystemState.LLC.count; i++) {
                SystemState.LLC.Lines[i]->SelfIndex = i;
                DriverIndex = SystemState.LLC.Lines[i]->DriverIndex;
                if ((DriverIndex >= 0)&&(DriverIndex < SystemState.MAC.count)) {
                    if (SystemState.MAC.Drivers[DriverIndex]->eventMask & MAC_ID_POLL) // драйвер потдерживает poll
                    {
                        pthread_create(&SystemState.LLC.Lines[i]->pollThread, NULL, &LinePollingThread, &SystemState.LLC.Lines[i]->SelfIndex);

                    }
                }
            }
        }

        // После инициализации скриптов, линий , драйверов и проч. запустим функцию run()
        // что бы пользователь мог выполнить разовые действия при старте программы.
        if (scrOnRun() != 0) {
            Log_DBG("Run() Error --> exit");
            return -1;
        }

        //ParamsHashDone();

    } else {
        Log_DBG("Error --> exit");
        return -1;
    }
    return 0;
}

int printSystemState(TSystemState * State) {
}

int fatal_error(const char * text) {
    Log_Ex("err", LEVEL_ERR, "FATAL ERROR: %s", text);
    getchar();
    return 1;
}

extern int mcProcessing(void) {
    //
    int i, j, k, l, p;
    struct pollfd fds [CLIENT_MAX];
    int fds_num = 0;
    int DriverIndex;
    char dbgText[255] = "";

    //while (SystemState.running)
    {
        fds_num = 0;
        //------------------------------------------------------
        //----- сетевые клиенты сервера ------------------------
        for (i = 0; i < SystemState.clients; i++) {
            fds[fds_num].revents = 0;
            if (SystemState.Line[i].LineStatus == CLIENT_STATUS_ON) {
                fds[fds_num].fd = SystemState.fds[i].fd;
                fds[fds_num++].events = (POLLIN | POLLRDHUP); //POLLHUP + POLLERR
            } else
                SystemState.fds[i].events = 0;
            SystemState.fds[i].revents = 0;
        }
        // usleep(1000);
        k = poll(fds, fds_num, TIMEOUT);

        if (k < 0) {
            return 1;
        }

        if (k) {
            for (p = 0; p < fds_num; p++)
                for (j = 0; j < SystemState.clients; j++)
                    if (fds[p].fd == SystemState.fds[j].fd) {
                        SystemState.fds[j].events = fds[p].events;
                        SystemState.fds[j].revents = fds[p].revents;

                        //if (SystemState.fds[j].revents)
                        //printf(" [%d].r = %d ",j, SystemState.fds[j].revents);
                        if (SystemState.fds[j].revents & (POLLRDHUP)) {
                            close(SystemState.fds[j].fd);
                            for (l = j; l < SystemState.clients; l++)
                                memcpy(&SystemState.fds[l], &SystemState.fds[l + 1], sizeof (SystemState.fds[l]));

                            SystemState.clients--;
                            SystemState.fds[j].revents = 0;
                            Log_DBG("DBG. Client is closed\n");
                        } else
                            if (SystemState.fds[j].revents & (POLLIN)) {
                            Log_DBG(">>SystemState.fds[j].revents & (POLLIN)");

                            if (SystemState.inf[j].Client_type == SERIAL_CLIENT_TYPE)
                                Log_DBG("ERR!!! SERIAL_CLIENT_TYPE UNSUPPORTED.");
                            //                                if (p232_read(&SystemState, j))
                            //                                    SR_CMD_Processing(&SystemState, j);

                            if ((SystemState.inf[j].Client_type == TCP_FROM_CLIENT_TYPE) ||
                                    (SystemState.inf[j].Client_type == TCP_CLIENT_TYPE)) {

                                if (mc_sock_read(&SystemState, j)) {
                                    if (SystemState.buf[j].TxSize) {
                                        mc_sock_write(&SystemState, j);
                                    }
                                }
                            }

                            if (SystemState.inf[j].Client_type == TCP_SERVER_TYPE) {
                                //Log_DBG("DBG. Client TCP_SERVER_TYPE activity!\n");
                                mc_client_accept(&SystemState, j, SystemState.inf[j].Client_prot, SystemState.Line[j].typeLine);
                                initProtocol(&SystemState, SystemState.clients - 1, SystemState.inf[j].Client_prot);
                            }
                        }
                    }
        }
        //------------------------------------------------------
        // опрос интерфейсов в одном потоке...
        if ((SystemState.initMode & MC_MODE_MULTITHREAD) == 0) {

            REQ_Processing();
            // запуск потоков опроса линии (если драйвер потдерживает POLL)
            for (i = 0; i < SystemState.LLC.count; i++) {
                SystemState.LLC.Lines[i]->SelfIndex = i;
                DriverIndex = SystemState.LLC.Lines[i]->DriverIndex;
                if ((DriverIndex >= 0)&&(DriverIndex < SystemState.MAC.count)) {
                    if (SystemState.MAC.Drivers[DriverIndex]->eventMask & MAC_ID_POLL) // драйвер потдерживает poll
                    {
                        mcLinePolling(i);
                    }
                }
            }
        }

        //---------------------------
    } //while(1)


}

void mcLinePolling(int lineIndex) {
#warning "Внимание! Введен режим работы с буфером линии "
    unsigned char appendRxBuffer = 0; //Ввел настройку добавлять буфер/не добавлять (потом сделаю опцию для линии)
    // предположительно, опция будет задаваться прямо в имени драйвера линии.
    // Например: "COM+", "TCP+", при этом "+" будет устанавливать опцию appendRxBuffer и заменяться на \0
    unsigned int TmpRxSize; // временная переменная для размещения RxSize в appendRxBuffer режиме
    //--------------------------------------------------------------------------
    int i, j, p;
    int framingIndex;
    int iResult;
    int packResult;
    int pResult;
    int macResult;
    BufData * B;
    struct pollfd fds [100]; //CLIENT_MAX];
    int fds_num = 0;
    struct pollfd fds_out [100]; //CLIENT_MAX]; //
    int fds_out_num = 0;
    char DriverIndex = 0;
    char dbgText[255] = "";
    TMCExtDriver extDriver;

    char errBuff[CLIENT_RX_SIZE] = "CLIENT_STATUS_OFF";

    memset(&extDriver, 0, sizeof (extDriver));

    //------------------------------------------------------
    //--- обработка интерфейсов пользователя ---------------
    // ( временно, нужно перенести в отдельный поток)
    fds_num = 0;
    //	for (i=0; i< SystemState.LLC.count; i++)
    {
        fds[fds_num].revents = 0;
        if (lineIndex < SystemState.LLC.count) {
            DriverIndex = SystemState.LLC.Lines[lineIndex]->DriverIndex;
            if ((strcmp(SystemState.MAC.Drivers[DriverIndex]->name, "TCPSRV") == 0) ||
                    (strcmp(SystemState.MAC.Drivers[DriverIndex]->name, "COM") == 0)) {
                if (SystemState.MAC.Drivers[DriverIndex]->eventMask & MAC_ID_POLL) {
                    // драйвер потдерживает poll
                    //if (SystemState.LLC.Lines[lineIndex]->LineControl.LineStatus == CLIENT_STATUS_ON)
                    fds[fds_num].revents = 0;
                    fds[fds_num].fd = SystemState.LLC.Lines[lineIndex]->fd;
                    fds[fds_num++].events = (POLLIN | POLLRDHUP | POLLERR | POLLHUP); //POLLHUP + POLLERR
                }
            }
        } else
            Log_DBG("ERR. lineIndex > LLC.count. LLC.count = %d", SystemState.LLC.count);
    }
    if (fds_num) {
        // для совместимости с драйверами, которые не потдерживают возврат дискрипторов
        // копируем fds в fds_out.
        memcpy(&fds_out, &fds, sizeof (fds[0]) * fds_num);
        fds_out_num = fds_num;
        // подготовим интерфейсную структуру...
        extDriver.ErrorText = &SystemState.LLC.Lines[lineIndex]->buffers.lastError[0];
        extDriver.id = MAC_ID_POLL;
        extDriver.param.prPOLL.in_fds = &fds[0];
        extDriver.param.prPOLL.in_fds_count = fds_num;
        extDriver.param.prPOLL.out_fds = &fds_out[0];
        // пометим fd в выходной структуре как невалидный
        fds_out[0].fd = -1;

        macResult = RunMacDriverCallBack(lineIndex, &extDriver); //id : MAC_ID_POLL

        //k = poll(fds, fds_num, TIMEOUT);
        if (macResult > 0) {
            // проанализируем выходную структуру. пользовательский драйвер может ее не использовать
            // если там fd == -1 то используем входную структуру, иначе, используем fd из fds_out
            // для чтения данных
            if (fds_out[0].fd != -1)
                memcpy(&fds[0], &fds_out[0], sizeof (fds[0]) * macResult);
            fds_out_num = macResult;
        }

        if (macResult < 0) {
            Log_DBG("ERR!. User Link Poll error");
            return;
        }

        if (macResult) {
            sprintf(dbgText, "mcLinePolling(%s)", SystemState.LLC.Lines[lineIndex]->name);
            Polling_synchronize(POLL_SYNC_LOCK, dbgText);

            j = lineIndex;
            for (p = 0; p < fds_out_num; p++)
                if (fds[p].revents & (POLLRDHUP | POLLERR | POLLHUP)) {
                    //close(fds[p].fd);
                    //SystemState.LLC.Lines[j]->LineControl.LineStatus = CLIENT_STATUS_OFF;
                    //Log_DBG("DBG. (fds[p].revents & (POLLRDHUP | POLLERR | POLLHUP))");
                } else
                    if (fds[p].revents & (POLLIN)) {

                    B = &SystemState.LLC.Lines[j]->buffers;
                    // чтение из устройства

                    extDriver.id = MAC_ID_READ_BUFFER;
                    extDriver.param.prRX.fdpoll = fds[p].fd;
                    if (appendRxBuffer) {
                        if (B->RxSize > (CLIENT_RX_SIZE / 2)) {
                            //недопустимо переполнение входного буфера!
                            Log_DBG("ERR!. rx buffer overflow! rxSize = %d (Line: %s)", B->RxSize, SystemState.LLC.Lines[j]->name);
                            B->RxSize = 0;
                        }
                        TmpRxSize = 0;
                        extDriver.param.prRX.rxBuff = &B->Rx[B->RxSize];
                        extDriver.param.prRX.rxCount = &TmpRxSize; //&B->RxSize;
                    } else {
                        B->RxSize = 0;
                        extDriver.param.prRX.rxBuff = &B->Rx[0];
                        extDriver.param.prRX.rxCount = &B->RxSize;
                    }
                    extDriver.param.prRX.timeOut = 0;

                    macResult = RunMacDriverCallBack(lineIndex, &extDriver); //id : MAC_ID_READ_BUFFER

                    if (macResult < MAC_RESULT_OK) { //Ошибка!
                        SystemState.LLC.Lines[j]->LineControl.LineStatus = CLIENT_STATUS_OFF;
                        Polling_synchronize(POLL_SYNC_UNLOCK, dbgText);
                        return;
                    }
                    // Т.к. функция чтения очень длительная, сделал синхронизацию после нее
                    //                    Polling_synchronize(POLL_SYNC_LOCK, dbgText);
                    if (appendRxBuffer) {
                        //в режиме добавления к входному буферу добавляем полученный
                        //Обёртка должна управлять входным буфером! иначе будет переполнение RX!
                        B->RxSize += TmpRxSize;
                    }

                    // данные считаны, нужно передать событие "RX" функции событий линии связи (пока только для скрипта)
                    if (SystemState.LLC.Lines[j]->source == OBJ_SOURCE_SCRIPT) //SystemState.LLC.Lines[j]->source == OBJ_SOURCE_SCRIPT)
                    { // Если интерфейс создан [через скрипт / пользователем или ядром], вызовим функцию обработки событий канала
                        //Log_DBG("DBG. Line(RX, RxSize = %d)", B->RxSize);
                        TMACDriverArg arg = {.prIO.inBuff = (char*) &B->Rx, .prIO.inCount = B->RxSize, .prIO.outBuff = (char*) &B->Rx, .prIO.outBuffCount = &B->RxSize};
                        RunLineCallBack(MAC_ID_READ_BUFFER, j, arg, B->lastError);

                        // нужно вызвать функцию проверки фрейма,
                        framingIndex = SystemState.LLC.Lines[j]->FramingIndex;
                        do {
                            iResult = 0;
                            B->lastError[0] = 0;
                            //Log_DBG("DBG. Framing[i]->callBack(UNPACK, RxSize = %d)", B->RxSize);
                            //вызов функции раскадровки...
                            if (B->RxSize) {
                                iResult = RunFramingCallBack(PROT_FRAMING_UNPACK, framingIndex, B->Rx, &B->RxSize, B->FrameRx, &B->FrameRxSize, B->lastError);
                                //SystemState.FramingUnits.Framing[framingIndex]->callBack(PROT_FRAMING_UNPACK, B->Rx, &B->RxSize, B->PacketRx, &B->PacketRxSize, B->lastError);
                                //Log_DBG("DBG. Framing[i]->callBack(RxSize-> %d, FrameRxSize->%d): %d", B->RxSize, B->PacketRxSize, iResult);
                            }
                            if (B->lastError[0] != 0)
                                Log_DBG("DBG. FramingError: %s", B->lastError[0]);
                            // Если кадр сообщения обнаружен, вызвать функцию протокола.
                            if (iResult) {
                                i = SystemState.LLC.Lines[j]->ProtocolIndex;
                                //Log_DBG("DBG. ProtocolcallBack(ProtocolIndex = %d)", i);
                                B->lastError[0] = 0;
                                B->PacketTxSize = 0;
                                pResult = RunProtocolCallBack(PROT_MNG_RX, j, i, NULL, "", NULL,
                                        B->FrameRx, B->FrameRxSize,
                                        B->ProtocolTx, &B->ProtocolTxSize, B->lastError);

                                //Log_DBG("DBG. ProtocolcallBack(PacketTxSize-> %d): %d", B->PacketTxSize, pResult);

                                if (B->lastError[0] != 0)
                                    Log_DBG("DBG. Protocol Error: %s", B->lastError[0]);

                                if (pResult == PROT_MNG_RESULT_OK) {
                                    // проверим, сформировал пользователь пакет
                                    if (B->ProtocolTxSize) {// упакуем и отправим...
                                        B->lastError[0] = 0;
                                        packResult = RunFramingCallBack(PROT_FRAMING_PACK, framingIndex, B->ProtocolTx, &B->ProtocolTxSize, B->FrameTx, &B->FrameTxSize, B->lastError);
                                        //                                                packResult = SystemState.FramingUnits.Framing[framingIndex]->callBack(PROT_FRAMING_PACK, B->PacketTx, &B->PacketTxSize, B->Tx, &B->TxSize, B->lastError);
                                        //Log_DBG("DBG. %s->callBack(PACK, TxSize->%d): %d", SystemState.FramingUnits.Framing[framingIndex]->name, B->TxSize, packResult);
                                        if ((packResult)&&(B->FrameTxSize)) { // отправим данные в канал связи
                                            //B->Tx[B->TxSize] = 0;
                                            //Log_DBG("DBG. Framing[i]: buff=[%s]", Log_buf2str(B->Tx, B->TxSize));
                                            TMACDriverArg arg = {.prIO.inBuff = (char*) &B->FrameTx, .prIO.inCount = B->FrameTxSize, .prIO.outBuff = (char*) &B->Rx[0], .prIO.outBuffCount = &B->RxSize};
                                            // Log_DBG("DBG. Protocol FrameTxSize: %d --> MAC_ID_WRITE_BUFFER.. ignore...", B->FrameTxSize);
                                            B->RxSize = 0; // !!! Удаляю без вызова обработчика!
                                            // предусмотрел вызов MAC_ID_WRITE_BUFFER, но в тек. версии не использую его 
                                            //                                            macResult = RunLineCallBack(MAC_ID_WRITE_BUFFER, j, arg, B->lastError);
                                            //                                            if (macResult < MAC_RESULT_OK) //Ошибка!
                                            //                                                Log_Ex("dbg", LEVEL_ERR, "ERROR! RunMacDriverCallBack(MAC_ID_WRITE_BUFFER) : %d ", macResult);
                                            //                                            // проверка RX. возможно пулучение ответа сразу!
                                            //                                            if (B->RxSize)
                                            //                                                Log_DBG("DBG. Line[i]: autoRx. Size=%d", j, B->RxSize);
                                        }
                                    }
                                }
                            }
                        } while (iResult);
                    }
                }
            Polling_synchronize(POLL_SYNC_UNLOCK, dbgText);
        }
    }
}