
#ifndef _ALGORITHMTYPES_H_
#define _ALGORITHMTYPES_H_

#include "algorithmConfig.h"
#include <stdint.h>
#include "./tracking_3d_polar_doppler/tracking/tracking.h"

typedef enum {
    FFTR_DATA_ID = 0x1U, // код данных для FFTR
    FFTDA_DATA_ID = 0x2U // код данных для FFTDA
} TCacheIDCode;


typedef enum {
    CHIRP_EOF = 0x0U, //чирпы кончились... (только не для Embedded)
    CHIRP_OK, //чирп получен      
    CHIRP_LOSS //чирп потерялся (по сети)
} TChirpResult;

typedef float complexPart;

typedef struct {
    complexPart Re;
    complexPart Im;
} TComplex;

//----------------------------------------
// Структура конфигурации модели расчета.
//----------------------------------------
typedef struct {
    double Re[RX_CHANNELS_NUM];
    double Im[RX_CHANNELS_NUM];
} TCalibration;

typedef struct {
    int16_t adcNumPerChirp; //фактическое число выборок АЦП в чирпе
    int16_t chirpNumPerFrame; //фактическое число выборок АЦП в чирпе
    //TFeedline feedline;
    TCalibration calibration;
    double BW; //Bandwidth of LFM signal
    double T_chirp; //Length of LFM chirp
    double T_CW; //Длительность CW части (с)
    double fd; //ADC sample rate
    double T_REPETITION;
    int16_t rangeWindowSize;
    int16_t azimuthWindowSize;
    int16_t dopplerWindowSize;
    int rangeWindowType; //Тип окна
    int dopplerWindowType; //Тип окна
    int azimuthWindowType; //Тип окна
    int16_t NFFT; //number of range fft point
    int16_t MFFT; //number of angle fft point
    int16_t LFFT; //number of Doppler fft point
    int16_t leakEnable;
    void * leakArray;
    // Выбор алгоритма порога. 0: minCFAR, 1: osCFAR
    int OS_CFAR_ALG;
    // T_OS
    double CFAR_LEVEL;
    // Дополнительная коррекция T_OS (в алгоритме идет как сумма: T_OS + T_OS_Add)
    double T_OS_Add;
    // самокалибровка
    int SELF_CALIBRATION;
    // Нижняя граница порога
    float * thresholdLimitArray;
    // дальность до которой применяется граница порога (включительно)
    int16_t thresholdLimitRange;
    
} TRF_Config;
//----------------------------------------

// Исторически сложилось, что в модель поступали данные с сложном формате,
// однако, в процессе рефакторинга, оставил только вормат стрим.файла CPRR
// типы старых данных : просто заглушка
typedef int * pFrameData;
typedef int * pChirpData;

//-----------------------------------------------------------------------------

// Срез куба по дальности:
typedef union {
    TComplex AD [RX_CHANNELS_NUM][CHIRPS_NUM];
    TComplex DA [CHIRPS_NUM][RX_CHANNELS_NUM];
} TSliceOfCube;

typedef struct {
    int count;
    float objSpeed[SPEED_INFO_MAX_NUM];
} TSpeedInfo;

typedef struct {
    TSpeedInfo history[SPEED_INFO_HISTORY];
    float EGO_speed[SPEED_INFO_HISTORY];
    int historyIndex;
    int historyCount;
} TSpeedInfoHistory;

typedef struct {
    float ID;
    float x;
    float vx;
    float ax;
    float y;
    float vy;
    float ay;
    float amp;   
    float liveTime;
    float sbrosTime;
} TTrackingObjectData;

typedef struct __attribute__ ((aligned (8))) { // !!!
    // основные точки, по которым происходит завязка
    int16_t PointCount;
    // точки поддержки, завязка по ним не происходит, они используются только для треков у которых нет своих отметок.
    int16_t PointExtCount;
    int16_t TargetCount;
    int16_t reserv;
    float Target[TRACKING_MODEL_MAX_TARGET][10]; // ID, x, vx, ax, y, vy, ay, Amp, lTime, sbTime
    float Point[TRACKING_MODEL_MAX_POINT][4];
} TTrackingData;

typedef struct __attribute__ ((aligned (8))) { // !!!
    int8_t dz;
    int8_t dy;
    int8_t dx;
} TPF_Neighbour;

typedef struct __attribute__ ((aligned (8))) { // !!!
    int16_t z; //0..AZIMUTH_FFT_SIZE - 1
    int16_t y; //0..FFT_DOPPLER_FULL_SIZE - 1
    int16_t x; //0..FFT_RANGE_USEFUL_WIDTH - 1
} TRadarPeakCoordinate;

typedef struct {
    float R;
    float A;
    float D;
    float amp;
} TRadarPeakCorrected;

typedef union {

    struct {
        float y[PF_D_GUARD * 2]; //D
        float z[PF_A_GUARD * 2]; //A
        float x[PF_R_GUARD * 2]; //R
    };
    float values[PF_NEIGHBOR_NUM];
} TRadarPeakNeighbourValue;

typedef enum {
    SL_PEAK_NEED_TEST = 0x0U, //Пик еще не тестирован
    SL_PEAK_NEED_IGNORE, //Пик протестирован (не подходит)
    SL_PEAK_TEST_OK //Пик протестирован (настоящий пик!)
} TSLPeakState;

typedef struct {
    int32_t count; // кол-во пиков
    TRadarPeakCoordinate peak[PEAKS_MAX_NUM]; // координаты пиков
    float peakValue[PEAKS_MAX_NUM]; // значение пиков
    TRadarPeakNeighbourValue peakNeighbourValue[PEAKS_MAX_NUM]; // значение соседей пиков
} TRadarCubePeak;

typedef struct {
    int32_t count; // кол-во пиков
    //int32_t maxCount; // кол-во пиков (для статистики)
    TRadarPeakCoordinate peak[PEAKS_MAX_NUM]; // координаты пиков
    TRadarPeakNeighbourValue peakNeighbourValue[PEAKS_MAX_NUM]; // значение соседей пиков
    float peakValue[PEAKS_MAX_NUM]; // значение пиков
    TRadarPeakCorrected peakCorrectedValue[PEAKS_MAX_NUM]; // скорректированные координаты
    //uint8_t correctedFail[PEAKS_MAX_NUM]; // Ошибка коррекции (нет соседа)
} TRadarCubeTruePeak;

typedef struct {
    TComplex value[RX_CHANNELS_NUM][FFT_DOPPLER_SIZE * FFT_DOPPLER_FACTOR];
} TPhaseCorrection;

typedef struct  __attribute__ ((aligned (8))) {
     float azimuth[AZIMUTH_FFT_SIZE];
} TPeakItem;

typedef struct {
    uint8_t  d; 
    uint16_t r; 
} TPeakItemCoordinate;

typedef struct  __attribute__ ((aligned (8))) {
    uint16_t   peakRefCube3D[FFT_DOPPLER_FULL_SIZE][FFT_RANGE_USEFUL_WIDTH]; // 31488б
    uint16_t   peakCount;
    // 134 байта на пик.
    TPeakItem  peak[PEAKS_MAX_NUM] __attribute__ ((aligned (8))) ;
    TPeakItemCoordinate peakCoordinate[PEAKS_MAX_NUM];
    uint8_t    Cube3DEmpty[FFT_DOPPLER_FULL_SIZE][(FFT_RANGE_USEFUL_WIDTH / 8) + 1]; // ~2kb
} TPeakItemList;

typedef struct  __attribute__ ((aligned (8))) {
        TComplex arg[FFT_RANGE_SIZE];
        TComplex result[FFT_RANGE_SIZE];
} TFFTRangeArgResult;

typedef struct  __attribute__ ((aligned (8))) {
        TComplex arg[AZIMUTH_FFT_SHORT_SIZE];
        TComplex result[AZIMUTH_FFT_SHORT_SIZE];
} TAzimuthShortArgResult;

typedef struct  __attribute__ ((aligned (8))) {
        TComplex arg[AZIMUTH_FFT_SIZE];
        TComplex result[FFT_DOPPLER_FACTOR][AZIMUTH_FFT_SIZE];
} TAzimuthFullArgResult;

// FFT-R: brev[64b], TW[8k], ARG/RES[16k], Leak[1696б], WIN[3392б]
typedef struct  __attribute__ ((aligned (8))) {
    uint64_t id;
    TFFTRangeArgResult fftRange; //8k / 16k
    //CHIRP_SAMPLE_NUM + 1: т.к. для операции diff нужен еще один элемент справа
#ifdef PINGPONG_MCASP
    // 17/12/2020 -> обработка на лету: 4 канала АЦП идут в перемешку [1:[A0,A1,A2,A3]2:[A0,A1,A2,A3]...349:[]]
    int16_t  adcData[CHIRP_SAMPLE_NUM + 1][4] __attribute__ ((aligned (8)));
#else
    int16_t  adcData[CHIRP_SAMPLE_NUM + 1] __attribute__ ((aligned (8)));
#endif
//    int16_t  dLeak[CHIRP_SAMPLE_NUM]  __attribute__ ((aligned (8))) ; //0.5k
} TFFT_RangeStageL1;

//для 877 точек, размер [win: 108544б][leak: 54 272] : 162816б (159Кб)
typedef struct  __attribute__ ((aligned (8))) {
    uint64_t id;
    TComplex caribration[RX_CHANNELS_NUM];
    TComplex tw_most[FFT_RANGE_SIZE]; //8k
#if WIN2D_ENABLE == 0
    float    chebWindow[CHIRPS_NUM];// __attribute__ ((aligned (8)));
    float    RangeDopplerWindow[CHIRP_SAMPLE_NUM];// __attribute__ ((aligned (8)));
#else
    float    RangeDopplerWindow[CHIRPS_NUM][CHIRP_SAMPLE_NUM];// __attribute__ ((aligned (8)));
#endif    
    int16_t  deltaLeak[RX_CHANNELS_NUM][CHIRP_SAMPLE_FROM_RADAR];// __attribute__ ((aligned (8)));
} TFFT_RangeStageL2;

typedef struct  __attribute__ ((aligned (8))) {
    uint64_t id;
    TComplex phase_correction[FFT_DOPPLER_SIZE][FFT_DOPPLER_FACTOR][AZIMUTH_FFT_SIZE]; //16Кб
    float    threashold[FFT_RANGE_USEFUL_WIDTH];
    float    threashold_dB[FFT_RANGE_USEFUL_WIDTH];
} TFFT_DAStageCfg;

typedef struct  __attribute__ ((aligned (8))) {
    TFFT_DAStageCfg cfg;
    float    mean4range[FFT_DOPPLER_SIZE];
    union {
        struct {
            //Два буфера вход, один - результат
            TSliceOfCube inputBuffer[2] __attribute__ ((aligned (8)));
            TSliceOfCube result __attribute__ ((aligned (8)));//
        } sliceOfCube;
    } buffer;
    
    TPeakItemList peakList;
    
} TFFT_DAStageL2;

typedef struct  __attribute__ ((aligned (8))) {
    TComplex tw_most[FFT_RANGE_SIZE]; //8k
    TComplex tw_middle[AZIMUTH_FFT_SIZE];
    TComplex tw_little[AZIMUTH_FFT_SHORT_SIZE];
    TComplex tw_64[AC_AZIMUTH_FFT_EX_SIZE];
} TFFT_TW;

    
typedef struct  __attribute__ ((aligned (8))) {
    uint64_t id;
    TComplex tw_middle[AZIMUTH_FFT_SIZE];
    TComplex tw_little[AZIMUTH_FFT_SHORT_SIZE];
    float azimuthFullAbs[FFT_DOPPLER_FACTOR][AZIMUTH_FFT_SIZE]; // 0.25k
    TComplex doppler[FFT_DOPPLER_SIZE]; // 0.25k
    TAzimuthShortArgResult azimuthShort; //0.125k
    TAzimuthFullArgResult azimuthFull; //0.75k
    TSliceOfCube A_sliceOfCube[2]; //8k*2
    TSliceOfCube B_sliceOfCube; //8k
} TFFT_DAStageL1;


#if RANGE_250M == 1
typedef struct  __attribute__ ((aligned (8))) {
    union {
        TFFT_RangeStageL2 FFTR;
        TFFT_DAStageL2    FFTDA;
    };
} TCacheL2;

typedef struct  __attribute__ ((aligned (8))) {
    unsigned char brev[64];
    union {
        TFFT_RangeStageL1 FFTR;
        TFFT_DAStageL1    FFTDA;
    };
} TCacheL1;

#else

typedef struct  __attribute__ ((aligned (8))) {
    TFFT_RangeStageL2 FFTR;
    TFFT_DAStageL2    FFTDA;
} TCacheL2;

typedef struct  __attribute__ ((aligned (8))) {
    unsigned char brev[64];
    TFFT_RangeStageL1 FFTR;
    TFFT_DAStageL1    FFTDA;
} TCacheL1;

#endif


//typedef struct  __attribute__ ((aligned (8))) {
//    TComplex tw_most[FFT_RANGE_SIZE];
//    TComplex tw_middle[AZIMUTH_FFT_SIZE];
//    TComplex tw_little[AZIMUTH_FFT_SHORT_SIZE];
//    float    threashold[FFT_RANGE_USEFUL_WIDTH];
//    float    threashold_dB[FFT_RANGE_USEFUL_WIDTH];
//    float    mean4range[FFT_DOPPLER_SIZE];
//    TComplex caribration[RX_CHANNELS_NUM];
//    TComplex phase_correction[FFT_DOPPLER_SIZE][FFT_DOPPLER_FACTOR][AZIMUTH_FFT_SIZE];
//    
//    // 2020.09.07 Добавил RangeDopplerWindow.
//    // float    RangeDopplerWindow[CHIRPS_NUM][CHIRP_SAMPLE_NUM] __attribute__ ((aligned (8)));
//    int16_t  deltaLeak[RX_CHANNELS_NUM][] __attribute__ ((aligned (8)));
//
//    TPeakItemList peakList;
//    union {
//        struct {
//            //Два буфера вход, один - результат
//            TSliceOfCube inputBuffer[2]; 
//            TSliceOfCube result; //
//        } sliceOfCube;
//        //int16_t dLeak[RX_CHANNELS_NUM / 2][CHIRP_SAMPLE_NUM];
//    } buffer;
//    
//} TCacheL2;
//
////#endif
//
//typedef struct  __attribute__ ((aligned (8))) {
//    unsigned char brev[64];
//    TComplex tw_little[AZIMUTH_FFT_SHORT_SIZE]; //используется часть, занимает мало. 64б
//    TComplex tw_middle[AZIMUTH_FFT_SIZE];//используется часть, занимает 256б.
//    union {
//        float azimuthFullAbs[FFT_DOPPLER_FACTOR][AZIMUTH_FFT_SIZE]; // 0.25k
//        TComplex doppler[FFT_DOPPLER_SIZE]; // 0.25k
//        TAzimuthShortArgResult azimuthShort; //0.125k
//        TAzimuthFullArgResult azimuthFull; //0.75k
//        //TComplex tw_most[FFT_RANGE_SIZE]; //4k
//    } fft;
//    union {
//        // Окно фазовой коррекции для двух зон доплера
//        // TComplex phase_correction[2][AZIMUTH_FFT_SIZE]; //0.5k
//        float rangeDoppler[CHIRP_SAMPLE_NUM]  __attribute__ ((aligned (8))); // 1396b / 3.5k
//    } win;
//    union {
//        TFFTRangeArgResult fftRange; //8k / 16k
//        TSliceOfCube sliceOfCube; //8k
//    } bufferA;
//    union {
//        TSliceOfCube sliceOfCube  __attribute__ ((aligned (8))) ; //8k
//        int16_t dLeak[CHIRP_SAMPLE_NUM]  __attribute__ ((aligned (8))) ; //0.5k
//    } bufferB;
//} TCacheL1;

    #define GET_THRESHOLD(index)       mx->pData->FFTDA_Data.threashold[index]
    #define GET_THRESHOLD_dB(index)    mx->pData->FFTDA_Data.threashold_dB[index]
    #define GET_pPEAKLIST()            &mx->pL2->FFTDA.peakList

//#if RANGE_250M == 1
//    #define GET_THRESHOLD(index)       mx->pL2->FFTDA.cfg.threashold[index]
//    #define GET_THRESHOLD_dB(index)    mx->pL2->FFTDA.cfg.threashold_dB[index]
//    #define GET_pPEAKLIST()            &mx->pL2->FFTDA.peakList
//#else
//    #define GET_THRESHOLD(index)       mx->pL2->threashold[index]
//    #define GET_THRESHOLD_dB(index)    mx->pL2->threashold_dB[index]
//    #define GET_pPEAKLIST()            &mx->pL2->peakList
//#endif



//Интерфейс поставщика чирпов:
//typedef TChirpResult(*TChirpReader)(pChirpData pData);
typedef TChirpResult(*TFrameReader)(pFrameData pData, char * pRawData);
//Если чирп есть (CHIRP_OK) то данные находятся в pData

//----------------------------------------
//интерфейс для доступа к Chirp Provider
//----------------------------------------
//Система получения данных и обработки чирп ориентированная
//В качестве поставщиков чирпов могут быть файл, UDP-поток стримера, ROS

typedef struct {
    // TChirpReader getChirpData; //доступ к чирпам
    TFrameReader getFrameData; //доступ к целому кадру
    // ...              getXXX          //расширять интерфейс провайдера ТУТ!
} TChirpProviderInt;

typedef TChirpProviderInt * pChirpInterface;
//----------------------------------------

typedef struct {
    // Данные McASP в том виде как они размещены DMA
#ifdef PINGPONG_MCASP
    int16_t adcData[4][CHIRP_SAMPLE_FROM_RADAR][CHANNELS_PER_ADCCHIP];
#else
    int16_t adcData[RX_CHANNELS_NUM / 2][CHIRP_SAMPLE_FROM_RADAR];
#endif
} TADCChirpBuffer;

//----------------------------------------
//Структура хранения всех данных расчета.
//----------------------------------------

#define SPECTR_3D_EXTENDED  0

typedef struct {

    int32_t frameIdFromRadar; // Индекс кадра получаемый от радара
    uint64_t frameTimeStampFromRadar; // Текущая метка времени в мкс
    int32_t frameIndex; // Индекс кадра с момента начала работы
    float frameDeltaTime; // Разница во времени между последним кадром и предыдущем.    
    float * pRangeWindow; // данные окна для расчета Range FFT
    float * pAzimuthWindow; // данные окна для расчета Azimuth FFT
    float * pRangeDopplerWindow; // квадратное окно для расчета Range Doppler FFT
    float    RangeDopplerWindow[CHIRPS_NUM][CHIRP_SAMPLE_NUM] __attribute__ ((aligned (8)));
    float egoSpeed; // Собственная скорость (в матлабе: mean(EGO_speed))
    float deltaEgoSpeed; // изменение скорости (в матлабе: delta_velocity)
    float porog_move; // адаптивная оценка порога
    TR_PlatformParam platformParam4debug[2]; // копия параметров трекинга
#if SPEED_CALC_ENABLE == 1
    TSpeedInfoHistory speedInfoHistory;
#endif    
    TPhaseCorrection * MIMO_phase_correction; //Коректировка фаз для MIMO каналов с TDM
    TLimitsDetected limitsDetected; // флаг обнаружения придела
    TRadarCubeTruePeak rTruePeak; //координаты проверенных пиков  
    //TFFT_RangeStageL2 RangeStageL2; //буфер для хранения данных для стадии FFT-RANGE
    int thresholdTuning;
    TFFT_TW tw_data;
    //TFFT_DAStageL1 L1FFT_BUFFER;
    //TFFT_DAStageL2    FFTDA_Data;
    TFFT_RangeStageL2 FFTR_Data;
    TFFT_DAStageCfg   FFTDA_Data;
    //float DEBUG_TICK[100][2];
    // Куб Range - Azimuth - Dopler (временный буфер) Нужен только для перехода FFTR->FFTDA
    TComplex Spectr_R_A_D_DSP[FFT_RANGE_USEFUL_WIDTH][RX_CHANNELS_NUM][FFT_DOPPLER_SIZE]__attribute__ ((aligned (8)));
    
#ifndef DSP
    TTrackingData trackingData[2]; // данные для функции трекинга
    float monitoring_fft3d_max[FFT_RANGE_USEFUL_WIDTH];
    float monitoring_adc[16][CHIRP_SAMPLE_NUM];
    float monitoring_adc_pp[16][32];
#else
    TTrackingData * trackingData;
#endif

    uint32_t correctionTargetCount;
    uint32_t autoCalibrationCount;
    uint32_t correctionTargetRejectCount;
    uint32_t acTargetRejectType;
    uint32_t tracking_raw_mode;
    
#if AUTOCALIBRATION_ENABLE == 1    
    // Счетчик накопленных калибровочных отметок
    TComplex correctionTargetVector[AZIMUTH_FFT_SIZE];
 #endif
    
} TData;
//----------------------------------------

typedef struct {
    float Short_3D_cube_mean[FFT_RANGE_USEFUL_WIDTH][FFT_DOPPLER_SIZE];
    
    float Spectr_3D_dB[FFT_RANGE_USEFUL_WIDTH][FFT_DOPPLER_SIZE * FFT_DOPPLER_FACTOR][RX_CHANNELS_NUM]__attribute__ ((aligned (8)));
#if     SPECTR_3D_EXTENDED == 1
    float (*pSpectr_3D_dB)[FFT_RANGE_USEFUL_WIDTH][FFT_DOPPLER_SIZE * FFT_DOPPLER_FACTOR][RX_CHANNELS_NUM];
    #define SPECTR3D(X, r, d, a) ((*X->pSpectr_3D_dB)[r][d][a]) // SPECTR3D(mx->pData, x, y, z) mx->pData->Spectr_3D_dB[x][y][z]
#else
    void * pSpectr_3D_dB;
    #define SPECTR3D(X, r, d, a) X->Spectr_3D_dB[r][d][a] // 
    
#endif

    TRadarCubePeak rPeak; //координаты локальных пиков
#if PEAK_NEED_SORT == 1
    int rdPeakPointsSort[RD_PEAK_MAX_POINTS];
#endif
//    // Куб Range - Azimuth - Dopler (временный буфер) Нужен только для перехода FFTR->FFTDA
//    TComplex Spectr_R_A_D_DSP[FFT_RANGE_USEFUL_WIDTH][RX_CHANNELS_NUM][FFT_DOPPLER_SIZE]__attribute__ ((aligned (8)));
    
#if AUTOCALIBRATION_ENABLE == 1    
    uint8_t  complexSpectr_3DMask[FFT_DOPPLER_FULL_SIZE][(FFT_RANGE_USEFUL_WIDTH / 8) + 1]; // ~2kb
    // Нужно только для калибровки
    TComplex complexSpectr_3D[FFT_RANGE_USEFUL_WIDTH][FFT_DOPPLER_SIZE * FFT_DOPPLER_FACTOR][AZIMUTH_FFT_SIZE];
    TComplex Spectr_R_D_A_DSP[FFT_RANGE_USEFUL_WIDTH][FFT_DOPPLER_SIZE][RX_CHANNELS_NUM]; // Куб Range - Dopler  - Azimuth // только для автокалибровки
 #endif
    
} TSingleFrameData;

//----------------------------------------
// Структура контекста модели
//----------------------------------------

typedef struct {
    TRF_Config Config; //всё что имеет отношение к конфигурации
    TData * pData; //данные (включая промежуточные) алгоритма
    //void * pUser; //Пользовательские данные
    TSingleFrameData * pCoreData; // временные данных для одного ядра
    void * RunConfig;
    void * chirpInterface;
    TCacheL2 * pL2; //хранение данных в быстром буфере L2
    TCacheL1 * pL1; //хранение данных в супер быстром буфере L1
    uint32_t frameDsp1;
    uint32_t frameDsp2;
} TModelContext;

typedef TModelContext * pMx;
//----------------------------------------

#endif

//***  Требования к памяти L1 ***
// Обработка одного чирпа (один канал АЦП) + zero padding: 512 * 2 = 1Kb
// Обработка одного чирпа (один канал) + zero padding: 512 * 2 = 1Kb
// предрасчет для FFT RANGE (tw_most) = 512 * 8 = 4Kb
// массив brev для FFT = 64 Байта

// Требования к памяти L2
// Накопление чирпа по 16 каналам АЦП : 349 * 16 * 2 = 11 Кб
// Окно дальности-коррекции для каждого канала: 349 * 32 * 8 = 89344 байт
// Массив пиков : (32*8 + )*1024 = 
