#ifndef _RADAR_CONFIG_H_
#define _RADAR_CONFIG_H_

// Данный файл содержит параметры относящиеся к конфигурации радара:
// - Длительность сигналов (время чирпа [прямой/обратный ход])
// Определяет параметры и формат потока данных поступающий на вход алгоритма:
// - Кол-во точек в чирпе;
// - Кол-во каналов и чирпов;
// - Формат данных и т.п.


#define RADAR                        0


#define RANGE_250M                   1 // [0 | 1] : режим [100м / 250м]

#ifdef DEBUG_VERSION
#define DEBUG_DATA_ETH               0 // вывод отладочных данных по Ethernet
#else
#define DEBUG_DATA_ETH               0 // вывод отладочных данных по Ethernet release => 0
#endif

#define CHANNEL_VAR                  1 // 0 - старая версия - старый порядок каналов, 1 - новая версия, чтобы порядок каналов был нужным

#if RANGE_250M == 1
 #define CHIRP_SAMPLE_FROM_RADAR     720  //
 #define CHIRP_TIME                  ((double)380.00e-6)  // Время прямого хода
 #define CHIRP_CW                    ((double)20.000e-6)  // Время обратного хода
#else
 #define CHIRP_SAMPLE_FROM_RADAR     378     // 349 - используем в расчетах
 #define CHIRP_TIME                  ((double)197.0e-6)    //
 #define CHIRP_CW                    ((double)13.00001e-6) //13.00001e-6 //12.7e-6
#endif
// #define CHIRP_BW                    ((double)250e6)       //
 #define CHIRP_BW                    (double)194.09e6       // 20.03.2021 всё таки полоса ЛЧМ 194.09e6 по измерениям


#ifdef PINGPONG_MCASP
 #define CHANNELS_PER_ADCCHIP        4
#else
 #define CHANNELS_PER_ADCCHIP        1 // для исключения влияния данного определения
#endif

//
#define CHIRPS_NUM                  (32) //Кол-во чирпов(полезных) в кадре
#define RX_CHANNELS_NUM             (32) //Кол-во приемных каналов (общее)

// Для McASP и стриминга
#define DOPPLER_SHIFT               (2U)
#define MCASP_TRANS_NUM             (CHIRPS_NUM * 2 + DOPPLER_SHIFT * 2) //68
#define NUM_CHIRP_IN_FRAME           MCASP_TRANS_NUM



#define LIGHT_SPEED                 ((double)299792458) //(299 792 458 м/с)


#define CHIRP_COMMON_TIME           (CHIRP_TIME + CHIRP_CW)
#if MATLAB_LIKE == 1
// #define CARRIER_FREQ                ((double)24.125e9) // в матлабе так!
 #define CARRIER_FREQ                ((double)24.15e9) //средняя несущая (TODO: Настроить)
#else
 #define CARRIER_FREQ                ((double)24.15e9) //средняя несущая (TODO: Настроить)
#endif

#define CHIRP_FD                    ((double)1.8e6)

#define ANTENNA_ELEMENTS_INTERVAL   ((double)(LIGHT_SPEED / CARRIER_FREQ)*0.5) //Interval between antenna elements
#define T_REPETITION_DEFAULT        (CHIRP_COMMON_TIME) //
#define SPEED_MAX_AMB               (LIGHT_SPEED/ (4.0 * CHIRP_COMMON_TIME * CARRIER_FREQ)) //
#define RADAR_D_LAMBDA              (ANTENNA_ELEMENTS_INTERVAL / (LIGHT_SPEED / CARRIER_FREQ)) 


#define UMB_VELOCITY                (0.5 * SPEED_MAX_AMB) //неопределенность по скорости
#define UMB_VELOCITY_NEG            (-(UMB_VELOCITY)) // отрицательное значение 
#define UMB_VELOCITY_x2             (SPEED_MAX_AMB)  //синоним для SPEED_MAX_AMB


// Максимальная радиальная относительная скорость объектов (м/с). Она же масимальная скорость движения объекта вне зависимости от ориентации относительно радара
// Начиная с 2021.03.12 определяется в зависимости от UMB_VELOCITY
// 2021.05.21 - Елена задала максимальную скорость, которая не зависит от параметров радара
#define OBJECT_VELOCITY_MAX         (200.0/3.6) //(UMB_VELOCITY * 16.0) 



#endif

