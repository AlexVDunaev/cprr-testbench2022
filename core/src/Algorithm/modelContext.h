#ifndef _MODEL_CONTEXT_H_
#define _MODEL_CONTEXT_H_

#include <smConfig.h>
#include <stdint.h>
#include "../../../Algorithm/algorithmTypes.h"




typedef enum {
    CHIRP_PROVIDER_UNDEF = 0x0U,
    CHIRP_PROVIDER_ADC, // Embedded режим
    CHIRP_PROVIDER_BIN_FILE, // Отладка (входной bin-файл)
    CHIRP_PROVIDER_ROS, // Отладка (данные качаем из ROS)
    CHIRP_PROVIDER_UDP, // Штатная работа модели (UDP режим)
    CHIRP_PROVIDER_TCP, // Штатная работа модели (TCP режим)
    CHIRP_PROVIDER_DEBUG // заглушка для отладки
} TChirpSource;

//----------------------------------------

typedef enum {
    CHIRP_BIN_FILE_INT16 = 0, // Формат без чередования данных
    CHIRP_BIN_FILE_INT16_AND, // Формат с чередованием данных (Формат Андрея П.)
    CHIRP_BIN_FILE_FLOAT32, // Формат данных по float
    CHIRP_BIN_FILE_INT16_RE,  // int16 без чередования данных (только Real)
    CHIRP_BIN_FILE_INT16_RE_DSP  // int16 без чередования данных (только Real) + 2*32 drop чирпов
} TChirpSourceFormat;

typedef enum {
    DBGLOG_LEVEL_DEBUG = 0, // Вывод всех сообщений
    DBGLOG_LEVEL_INFO, // Информационных и сообщений об ошибках
    DBGLOG_LEVEL_ERROR // Только вывод ошибок
} TDbgLogLevel;

// Параметры запуска модели 

typedef struct {
    int radarid; // тип радара, с которым работаем
    int autosync; // возможность компенсировать асинхроную работу каналов и джитер м/у кадрами
    float datafileFPS; // Если в датасете нет данных по времени, можно задать FPS
    int calcDelay; // Задержка, мкс, после расчета кадра (что бы ограничить FPS на выдачу)
    TChirpSource chirpSource;
    char * IP; // IP адрес радара
    char * chirpSourceParam;
    int UDPSourcePort; // Порт, на который летят данные
    // Порт ожидания внешней команды
    int listenerControlPort;
//    char * streamSaveFileName; // Имя файла, куда сохраняем стрим
    char isTimeOption; // Работа модели ограничена временем
    int timeOptionValue; // Время работы модели, в секундах
    char isROSLogging; // Сброс промежуточных данных алгоритма в ROS (резерв для Jenkins)
    char * rosID; // текстовый идентификатор [передаваемый при инициализации ROS]
    char isUDPThreadCount; // Кол-во потоков приема в UDP провайдере
    char isStreamFromProvider; // Соханение в файл, используя провайдер данных
    int frameLimit; // выход после обработки определенного числа чирпов
    //int chirpSaveFormat; // Возможность сохранять данные в разных форматах
    int chirpLoadFormat; // Возможность читать данные из bin-файлов в разных форматах
    char isSaveInfo; // сбрасывать текущий кадр в текстовый файл
    TDbgLogLevel logLevel; // Уровень вывода отладочной информации...
    // ---- "Сервер" T25 ------------------
    char * T25_ip;
    int T25_port;
    int T25_localPort;
    int T25_forceEnable;
    int T25_verDisable;
    int T25_maxTarget;
    // ---- передний радар (FRONT) ------------------
    char * front_ip; // IP адрес переднего радара
    int front_cmd;   // командный порт переднего радара
    int front_data;  // порт данных переднего радара
    // ---- задний радар (BACK) ------------------
    char * back_ip; // IP адрес заднего радара
    int back_cmd;   // командный порт заднего радара
    int back_data;  // порт данных заднего радара
    //--------------------------------------------
    char forward; // Значение направления по умолчанию
    //--------------------------------------------
    // Параметр запуска радара, определяющий скорость выдачи данных
    int radarDelayParam;
    // Опция разрешения записи при Enable=1
    int writeOnEnable;
    
    char * streamPath;
    // Формат записи стрима 0: ROSBAG, 1 : BIN_INT16
    char streamFormat; 
    // Ограничение записи по числу кадров
    int streamSaveFrameLimit;  
    // Ограничение записи по времени
    int streamSaveTimeLimit; 
    // Возможность запуска стрима сразу после старта
    char streamOnStart;
    // Возможность выхода после завершения стрима
    char exitAfterSaving;
    //--------------------------------------------
    int workTimeLimit;  
    char saveOnExit;
    char algMonitoring;
    char trackingRawMode;
    
} TRun_Config;
//----------------------------------------


//----------------------------------------
//Структура провайдеров
//----------------------------------------
//Все что является внешним по отношению к мат.функциям
//должно быть представлено в данной структуре в виде соответствующего интерфейса

typedef struct {
    pChirpInterface chirp;
    TRun_Config * RunConfig; // параметры запуска модели
} TUserExt;

//----------------------------------------
//








#endif

