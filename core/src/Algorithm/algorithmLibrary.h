/**
 * @file  algorithmLibrary.h 
 * @brief Интерфейс с библиотекой алгоритма DSP
 * 
 * Параметры сборки: -lcprr24alg
 */


#ifndef _ALG_LIBRARY_H_
#define _ALG_LIBRARY_H_

#include "./algorithmTypes.h"


typedef enum {
    ALGORITHM_DONE_OK = 0x0U,
    ALGORITHM_DONE_ERROR
} TAlgorithm_done_status;

typedef enum {
    ALGORITHM_CONFIG_OK = 0x0U,
    ALGORITHM_CONFIG_ERROR
} TAlgorithm_config_status;

typedef void (*TAlgorithm_done_callback)(TAlgorithm_done_status done_status);

/**
 * @brief Инициализация структуры данных алгоритма
 *
 * При первом вызове initAlgorithm() происходит выделение памяти под структуру данных
 * и ее инициализация. При последующих вызовах только реинициализация. 
 * @param mx - указатель на контекст алгоритма (TModelContext *)
 * @return код ошибки. 0 : нет ошибки, 1 : Ошибка
 */
TAlgorithm_config_status initAlgorithm(pMx mx);

/**
 * @brief Освободить память, выделенную при initAlgorithm
 *
 * При первом вызове initAlgorithm() происходит выделение памяти, функция 
 * deInitAlgorithm() позволяет ее освободить
 * @param mx - указатель на контекст алгоритма (TModelContext *)
 */
void deInitAlgorithm(pMx mx);

/**
 * @brief Расчитать кадр в соответствии с алгоритмом DSP
 *
 * @param mx - указатель на контекст алгоритма (TModelContext *)
 * @param done_callback - событие по завершению расчета.
 * Перед выходом из функции stepAlgorithmDSP происходит вызов done_callback.
 * @param pFrame - указатель на данные кадра
 * @return статус расчета кадра
 */
TAlgorithm_done_status stepAlgorithmDSP(pMx mx, TAlgorithm_done_callback done_callback, int16_t * pFrame);


/**
 * @brief Задать конфигурация радара (пролаз и калибровку)
 *
 * @param mx - указатель на контекст алгоритма (TModelContext *)
 * @param externalConfig - 1 : использовать внешний LEAK и калибровку
 *        Если externalConfig == 0 используется калибровка поумолчанию.
 * @param pLeakConfig - указатель LEAK (формат: Кол-во точек АЦП * число каналов)
 * @param pCalibration - указатель вектор калибровки
 */
void updateRadarConfig(pMx mx, char externalConfig, int16_t * pLeakConfig, float * pCalibration);


/**
 * @brief Задать RF параметры поумолчанию
 *
 * Функцию нужно вызывать всегда перед loadConfig, т.к. не все параметры могут
 * быть заданы в файлах конфигурации
 * @param[out] config - указатель на TModelContext.Config который нужно заполнить
 */
void staticConfig(TRF_Config * config);

/**
 * @brief загрузть параметры из фыйла dir/config.txt и dir/leak.bin
 *
 * @param[out] config - указатель на TModelContext.Config который нужно изменить
 */
void loadConfig(char * dir, TRF_Config * config);


/**
 * @brief вывести на экран сводку конфигурации
 *
 * Диагностическая функция, может не вызываться.
 * @param config - указатель на TModelContext.Config
 * @param runConfig - указатель на TRun_Config, которая содержит параметры запуска
 */
void showConfig(TRF_Config * config, void * runConfig);


// --------------------------------------------
// Сервис мониторинга промежуточных данных в GUI
// В программе GUI (GRAPH=1, SILENT=1) реализован функционал отображения графиков
// из массивов данных поступающих по UDP.

/**
 * @brief Инициализация сервиса мониторинга массивов данных
 *
 * @param ip - адрес куда посылать UDP (рекомендуется использовать сервис локально)
 * @param port - порт куда посылать UDP (по умолчанию в GUI 7200 порт)
 * @return код ошибки. 0 : нет ошибки, -1 : не удалось открыть сокет
 */
int initGMonitoring(char * ip, unsigned int port);


/**
 * @brief добавить массив данных для отображения в GUI
 *
 * Функция буферизирует массив данных, добавляя массив в конец буфера
 * 
 * @param  id - Идентификатор графика, в который будут отображаться массив данных
 * @param  subid - Идентификатор тренда. В GUI 0 - идентификатор для массива порога
 * @param  data -  начало массива данных
 * @param  count - кол-во элементов данных
 * @return общее кол-во элементов в буфере (элемент == float)
  */
int addGMonitoring(int id, float subid, float * data, int count);
//----------------------------

/**
 * @brief Отправить буфер данных в сеть
 *
 * @return код ошибки. 0 : нет ошибки, -1 : не удалось отправить в сокет
 */
int sendGMonitoring();

/**
 * @brief Закрыть сервис мониторинга
 *
 * Функция закрывает сокет, который был открыт initGMonitoring()
 * @return Всегда 0
 */
int closeGMonitoring();


#endif
