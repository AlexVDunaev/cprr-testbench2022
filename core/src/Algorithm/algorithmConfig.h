#ifndef _ALG_CONFIG_H_
#define _ALG_CONFIG_H_

#include <radarConfig.h>


#ifdef DSP
 #include "system_cfg.h"
#endif
// константы для фиксации случаев с достижением приделов
typedef enum {
    LIMIT_NO_FOUND = 0x0U, //предел не достигнут
    LIMIT_ADD_PEAK,        //предел при добавлении сырого пика (peakFilter: addPeak)
    LIMIT_ADD_TRUEPEAK,    //
    LIMIT_TRACKING
} TLimitsDetected;


#if RANGE_250M == 1
// для тестирования 250м и статичный кадр
//#define CHIRP_SAMPLE_NUM            349   // 877 - получаем от радара,
//#define FFT_RANGE_SIZE              512
//#define FFT_RANGE_USEFUL_END        245     // для обработки (индексы)

// изменения 05.02.2021 для 250 м
#define CHIRP_SAMPLE_NUM            670   //
#define FFT_RANGE_SIZE              1024
#define FFT_RANGE_USEFUL_END        486     // для обработки (индексы) 16.02.21 как у Жаргала

#else
#define CHIRP_SAMPLE_NUM            (349-2)   // 378 - получаем от радара,
#define FFT_RANGE_SIZE              512
#define FFT_RANGE_USEFUL_END        245     // для обработки (индексы)
#endif


#define ADC_SAMPLE_COMPLEX_FORMAT   (0)     // 0: REAL, 1: COMPLEX
#if RANGE_250M == 1
// Настройка основных параметров.
//
//#define CHIRP_SAMPLE_OFFSET         (12)    // TEST ALING 8
//#define CHIRP_SAMPLE_OFFSET         (11-0)    // первые 11 и последние 18 игнорируем (2020.04.07 : 11 -> 9)
//#define CHIRP_SAMPLE_OFFSET         (12+0)    // (2020.10.01 : 11 -> 10 Как в коде Жаргала) 28.01.2021 8 -> 21
#define CHIRP_SAMPLE_OFFSET         (14)    // (2021.05.13 : [12->14] Обнаружил что в матлаб установлено 15:684, сделал также
#define CHIRP_START_DISCARD_NUM     (2)     // Два начальных чирпа отбрасываем
#define CHIRP_END_DISCARD_NUM       (0)     // отбрасываем чирпы в конце
#define CHIRP_TAIL_DISCARD_NUM      (23)    // ???? 25 отсчетов в конце блока каналов мусор
#define CHIRP_TAIL_IGNORE_NUM       (0)     // Экспериментально: хвост чирпа (из числа CHIRP_SAMPLE_NUM) ингорируется.,
#else
// Настройка основных параметров.
//
//#define CHIRP_SAMPLE_OFFSET         (12)    // TEST ALING 8
//#define CHIRP_SAMPLE_OFFSET         (11-0)    // первые 11 и последние 18 игнорируем (2020.04.07 : 11 -> 9)
#define CHIRP_SAMPLE_OFFSET         (8+0)    // (2020.10.01 : 11 -> 10 Как в коде Жаргала) 28.01.2021 8 -> 21
#define CHIRP_START_DISCARD_NUM     (2)     // Два начальных чирпа отбрасываем
#define CHIRP_END_DISCARD_NUM       (0)     // отбрасываем чирпы в конце
#define CHIRP_TAIL_DISCARD_NUM      (25)    // ???? 25 отсчетов в конце блока каналов мусор
#define CHIRP_TAIL_IGNORE_NUM       (0)     // Экспериментально: хвост чирпа (из числа CHIRP_SAMPLE_NUM) ингорируется.,
#endif
//

#define FFT_RANGE_USEFUL_START      0       // диапазон дальностей,
#define FFT_RANGE_USEFUL_WIDTH      (FFT_RANGE_USEFUL_END - FFT_RANGE_USEFUL_START + 1)       // 

#define FFT_DOPPLER_SIZE            32
#define FFT_DOPPLER_FACTOR          2   // 2 - используется для решения неоднозначности по скорости
#define FFT_DOPPLER_FULL_SIZE       (FFT_DOPPLER_SIZE * FFT_DOPPLER_FACTOR)
#define MFFT_DEFAULT                (RX_CHANNELS_NUM)

#define AZIMUTH_FFT_SHORT_SIZE            8
#define AZIMUTH_FFT_SHORT_CH_START        4   //Начальный канал (от 0...)
#define AZIMUTH_FFT_SHORT_CH_END          11  //Конечный канал
#define AZIMUTH_FFT_SHORT_CH_WIDTH        (AZIMUTH_FFT_SHORT_CH_END - AZIMUTH_FFT_SHORT_CH_START + 1)
#define AZIMUTH_FFT_SIZE                  32


#define LFFT_DEFAULT                FFT_DOPPLER_SIZE

#define RADAR_AZIMUTH_ANGLE         ((double)45.0) // угол крепления радара
#define RANGE_WINDOW_TYPE           (1)
#define DOPPLER_WINDOW_TYPE         (0)
#define AZIMUTH_WINDOW_TYPE         (0)
#define RANGE_WINDOW_SIZE           (CHIRP_SAMPLE_NUM)
#define DOPPLER_WINDOW_SIZE         (LFFT_DEFAULT)
#define AZIMUTH_WINDOW_SIZE         (RX_CHANNELS_NUM)
#define PEAKS_MAX_NUM               580 //(4096)
#define TRUEPEAKS_MAX_NUM           (PEAKS_MAX_NUM)

#define RX_CHANNEL_1                (0)
#define RX_CHANNEL_2                (1)
#define RX_CHANNEL_3                (2)
#define RX_CHANNEL_4                (3)
#define RX_CHANNEL_5                (4)
//...
#define RX_CHANNEL_LAST             (RX_CHANNELS_NUM - 1)

#define RD_PEAK_MAX_POINTS          (FFT_RANGE_USEFUL_WIDTH*FFT_DOPPLER_SIZE)     // максимальное кол-во точек выше уровня порога

//---------------------------------------------------
// Параметры детектирования пиков.
// Диапазон хранения соседей
#define PF_R_GUARD  1
#define PF_D_GUARD  1
#define PF_A_GUARD  1
// Общее число соседей, которых сохранит функция addPeak()
#define PF_NEIGHBOR_NUM ((PF_R_GUARD + PF_A_GUARD + PF_D_GUARD)*2)
#define PF_RANGE_MIN  1 //4
#define PF_RANGE_MAX  (FFT_RANGE_USEFUL_WIDTH - 1)
//---------------------------------------------------
// Параметры функции Удаления боковых лепестков
//---------------------------------------------------
// Диапазоны по R,A,D по которым запираем срезы
#define SL_R_GUARD  2
#define SL_D_GUARD  2
#define SL_A_GUARD  2
// Ищем объекты которые находятся на одной дальности, примерно с одной радиальной скоростью
// и если их больше чем SL_AZIMUTH_SIMIL_NUM забираем только максимум
#define SL_AZIMUTH_SIMIL_NUM 3 // (введено 25.06.20)

//---------------------------------------------------

//-----------------------------------------------------------------------------
// Настройка CFAR

#define MINCFAR_ALG         1  // Алгоритм MINCFAR
#define CASOCFAR_ALG        2  // Алгоритм CASO_CFAR

// Выбор алгоритма
#define CFAR_ALG            CASOCFAR_ALG //[MINCFAR_ALG | CASOCFAR_ALG] // Выбор алгоритма CFAR

#define OS_CFAR_THRESHOLD_RECALC_FRAME_NUM  4   // кол-во кадров, через которые обновляем порог

#define OSCFAR_CENTERED_MODE 1  // Режим проход по одной координате Y = 65
#define OSCFAR_X_WIN_SIZE    51 // Размер скользящего окна по X
#define OSCFAR_Y_WIN_SIZE    9  // Размер скользящего окна по Y
#define OSCFAR_X_GUARD_SIZE  7  // Размер защитного интервала по X
#define OSCFAR_Y_GUARD_SIZE  5  // Размер защитного интервала по Y

#define OSCFAR_X_GUARD_INDEX  ((OSCFAR_X_WIN_SIZE - OSCFAR_X_GUARD_SIZE)/2) // Начало защитного интервала по X
#define OSCFAR_Y_GUARD_INDEX  ((OSCFAR_Y_WIN_SIZE - OSCFAR_Y_GUARD_SIZE)/2) // Начало защитного интервала по Y

#define OSCFAR_X_SIZE   FFT_RANGE_USEFUL_WIDTH // Размер данных по X (количество строчек в матрице)
#define OSCFAR_Y_SIZE   32 // Размер данных по Y (количество столбцов в матрице)

#define OSCFAR_X_STRIDE_SIZE 3 // Размер шага по X
#define OSCFAR_Y_STRIDE_SIZE 1 // Размер шага по Y

#define OSCFAR_K_LOW_VALUE   ((float)0.6000)
#define OSCFAR_K_HIGH_VALUE   ((float)0.3678)
// граница дальности на которой ограничиваем порог (включительно)

#if RANGE_250M == 0
#define CFAR_CUT_LEVEL  1.258925411794166e-05 // 100 m
#else
#define CFAR_CUT_LEVEL  5.623413251903490e-06 //2.5119e-06 2022.07.04 Александра указалы новые параметры
#endif
 

#if RANGE_250M == 1
    #define OSCFAR_CUT_INDEX_DEFAULT     (145)
#else
    #define OSCFAR_CUT_INDEX_DEFAULT     (81)
#endif

#if CFAR_ALG == CASOCFAR_ALG
    #define OSCFAR_T_OS_VALUE   ((float)80.0) //46 2022.07.04 Александра указалы новые параметры
#endif
#if CFAR_ALG == MINCFAR_ALG
    #define OSCFAR_T_OS_VALUE   ((float)mx->Config.CFAR_LEVEL + (float)mx->pData->thresholdTuning)//((float)100.0)
#endif

//-----------------------------------------------------------------------------
#define FFT3D_RANGE_SLIDE_WINDOW_SIZE     (4) //размер окна (по Range) с которым ведем обработку 3D куба
#define FFT3D_RANGE_SLIDE_WINDOW_TAIL_SIZE  2
#define FFT3D_RANGE_SLIDE_WINDOW_MAX_SIZE  (FFT3D_RANGE_SLIDE_WINDOW_SIZE + FFT3D_RANGE_SLIDE_WINDOW_TAIL_SIZE)
//-----------------------------------------------------------------------------

#define SPECTR_3D_NOISE_LEVEL_dB   (-160.0f)
#define SPECTR_3D_NOISE_LEVEL       (1e-12)


//-----------------------------------------------------------------------------
//--- Параметры трекинга ------------------------------------------------------
//-----------------------------------------------------------------------------
#include "./tracking_3d_polar_doppler/tracking/trackingConfig.h"
//-----------------------------------------------------------------------------
#define TRACKING_MODEL_MAX_POINT          (TRACKING_MAX_TARGET)       //максимальное кол-во точек
#define TRACKING_MODEL_MAX_TARGET         (TRACKING_MAX_TARGET)       //максимальное кол-во треков

#define PEAK_NEED_SORT   0     //Нужно производить сортировку пиков по x,y,z

#define TRACKING_VARIABLE_FPS   1   // 0: Постоянный FPS, 1: Переменный FPS
// INTERFRAME_T - Время для постоянного FPS

#define TRACKING_MOVING_MULTIPLE 0 //1: Размножаем отметки по доплеру перед трекингом

#define TRACKING_STATIC_FILTER  1 //1: будем вносить управляющий процесс в фильтрацию статических целей

#define SPEED_MOVING_PRE_LEVEL  (5.0 / 3.6) // 06.10.20 2км/ч -> 5км/ч

//#define TRACKING_OFF            1   // 0: Штатный трекинг. 1: Выдача сырых детекций

//-----------------------------------------------------------------------------
//--- Параметры SLAM ----------------------------------------------------------
//-----------------------------------------------------------------------------
#define SPEED_STATIC_LEVEL  (5.0 / 3.6)
#define SPEED_MEAN_LEVEL    (1.0)

#define SPEED_INFO_MAX_NUM  1024
#define SPEED_INFO_HISTORY  3

#define SPEED_CALC_ENABLE  0

//-----------------------------------------------------------------------------
//--- Параметры автокалибровки ------------------------------------------------
//-----------------------------------------------------------------------------
#define AUTOCALIBRATION_ENABLE                0    // [0 | 1] 0: исключаем все что связано с АК
#define AC_NUMBER_OF_TARGETS_FOR_CORRECTION   11    // Кол-во отметок для перерасчета калибровочной матрицы
#define AC_REFERENCE_CHANNEL_INDEX            7    // Опорный канал, относительно которого считаем калибровку

// Параметры Фильтра объектов, пригодных для ка
#define AC_FILTER_ANGLE   25.0 // Берём цели в ограниченном секторе по Азимуту (ограничение в градусах)
#define AC_FILTER_RANGE   15.0 // Не анализируем цели, что слишком близко к радару (ограничение в метрах)
#define AC_FILTER_SPEED   2.0  // Не анализируем статичные объекты (ограничение в м/с)
// максимальное кол-во нерассчитанных (dB == -120) соседей у нашей точки (в пикселях)
#define AC_UNDEFINED_NEIGHBORS_LINIT   3  
// Фильтр по "одиночности" цели. В заданных пределах не должно быть больше целей.
#define AC_NO_NEIGHBORS_RANGE   3  // (в пикселях)
#define AC_NO_NEIGHBORS_DOPPLER 3  // (в пикселях)
#define AC_NEIGHBORS_NUM 9  // всего соседних целей + сама цель

#define AC_SYMMETRIC_TEST_VALUE   0.15  // Порог, при проверке симметричности вершины
#define AC_THRESHOLD_OF_EXCELLENCE 5.0  // Порог превосходства главной вершины над остальными (что бы цель нам подходила)

#define AC_AZIMUTH_FFT_EX_SIZE   (AZIMUTH_FFT_SIZE * 2)
#define AC_SINGLE_TARGET_DELTA_VALUE   18.0f  // Порог оценки одиночной цели

//-----------------------------------------------------------------------------
#define ALG_OPTION_USE_LEAKAGE   1  // Используем вычитание пролаза
#define ALG_OPTION_USE_DIFF      1  // Используем фильтр-корректор
#define ALG_OPTION_USE_FINDDROP  0  // Используем алгоритм адаптивного поиска чирпа

#if ALG_OPTION_USE_LEAKAGE == 1
    #define APPLY_LEAKAGE(A, LEAK)   ((A) - LEAK)  // 
#else
    #define APPLY_LEAKAGE(A, LEAK)   (A)  // 
#endif

#if ALG_OPTION_USE_DIFF == 1
    #define APPLY_DIFF(A1, A0)   ((A1) - (A0))  // 
#else
    #define APPLY_DIFF(A1, A0)   (A0)  // 
#endif

// Использование квадратного окна (Range*Doppler)
#define WIN2D_ENABLE            1

// Проблема неверной очередности каналов в потоке данных.
// Для корректной работы, нужно привести в соответствие очереность каналов к их положению на антенне
#define UNPUZZLE_RX_CHANNELS    1

//-----------------------------------------------------------------------------
// Возможность предварительно переложить данные в L1
// для максимальной производительности нужно что бы DMA работал с мин. накладными расходами (для след. версий)
#define FFTR_L2_DIRECT_ACCESS    1  //1: непосредственно работаем с L2 (без копирования)


// ВОзможность фильтравать отметки по сектору. до 50м все отметки, после только сектор 30 градусов
#define TARGET_SECTOR_RANGE_FILTER 0 
//-----------------------------------------------------------------------------
//--- Параметры отладки -------------------------------------------------------
//-----------------------------------------------------------------------------
//#define DEBUG_FRAME_CALC
// #define DEBUG_THRESHOLD_FILTER
// #define DEBUG_OSCFAR_CALC
 #define DEBUG_ADC_INPUT_DATA
// #define TRACKING_LOG
#define DEBUG_SPECTR_3D     0
#define DEBUG_LOG_PEAK      0
#define DEBUG_LOG_SIDELOBE  1
#define DEBUG_LOG_INTERP    0
#define DEBUG_LOG_SECTORFILTER    0
#define DEBUG_LOG_SLAM      0
#define DEBUG_EXPORT_ADC_DATA   0
#define DEBUG_LOG_MINCFAR   0

#if DEBUG_LOG_PEAK == 1
#undef PEAK_NEED_SORT
#define PEAK_NEED_SORT  1
#endif

// Загрузка одного кадра в цикле
#define DBG_LOOP_READ       0
// Возможность поменять местами 16 приемных каналов для TX. TX1 <-> TX2
#define MIME_TX_INVERT      0

#ifndef DSP
    #define PROF_MEM_CHECKPOINT_DONE(x) // Зачем здесь переопределяем?
    #define PROF_MEM_CHECK_EXCLUSIVE(x)
#endif


#endif

