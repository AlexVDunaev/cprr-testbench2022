/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   tracking.h
 * Author: Dell
 *
 * Created on 17 июля 2019 г., 15:58
 */

#ifndef TRACKING_H
#define TRACKING_H

#include <stdint.h>
#include <stdlib.h>
#include "../kalman/kalman.h"
#include "trackingConfig.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef SUBSYS_DSS
#define DEBUG_PRINT(...)   
#else
#ifdef NO_DEBUG_TEXT
#define DEBUG_PRINT(...)   
#else
#define DEBUG_PRINT(...)   printf(__VA_ARGS__)
#endif
#endif

#define TRACKING_ERROR(text, param)  {DEBUG_PRINT("%s [%d]", text, param); exit(0);}

#define TRACKING_INSTANCE_NUM     2

#define TRACK_STATE_VECTOR_SIZE   6 //Размер вектора состояния трека

    /* ПИО
//Просто задаем константы типов.
typedef enum {
    PACK_TYPE_CONFIG = 1,
    PACK_TYPE_DATA,
    PACK_TYPE_INFO,
    PACK_TYPE_ERROR
} TPackType;

//Вводим тип Data_t в котором есть доступ ко всем видам данных
typedef union {
    struct PackConfig_t PackConfig;
    struct PackData_t   PackData;
    struct PackInfo_t   PackInfo;
    struct PackError_t  PackError;
}Data_t;

*/

    
    typedef enum {
        TRACKING_FOR_STATIC = 0 //, // Трек статика
        // TRACKING_FOR_MOVING = 1 // Трек динамика
    } TTrackingType;

    typedef enum {
        TR_STATE_UNDEF = 0, // Трек не инициализирован
        TR_STATE_VOID, // Трек в начальном, неопределенном состоянии
        TR_STATE_PROOF, // Трек завязался, достоверный
        TR_STATE_DELETE // Трек нужно удалить
    } TTrackState;

    typedef enum {
        TR_POINT_NOT_ACCEPTED, // отметка не подходит треку
        TR_POINT_ACCEPTED, // отметка подходит треку
        TR_POINT_SIMILAR, // Копия трека (similar)
        TR_POINT_ACCEPTED_AND_GOTO_MOVING // отметка распределена а также уходит в трекинг подвижных объектов
    } TTrackPointFlag;

    typedef struct {
        uint32_t track_id; // % Всем завязавшимся целям присваеваем номера, начиная с последнего присвоенного (по возрастанию)
        uint32_t nachalo; // % Номер кадра, когда трек создался (получил первую отметку)
        uint32_t zavazka_time; // % Номер кадра, когда трек был завязан
        uint32_t sbros; // % Если трек завязался, обнулим её счётчик сброса
        uint32_t flag_zavazka; // Кол-во измерений в траектории
    } TTrackStatusRec;

    typedef struct {
        TTrackState state; // Состояние трека
        TTrackStatusRec status;
        TKL_FilterState KFState; // Вектор состояния,
        float liveTime; // время жизни трека
        float sbrosTime; // время которое трек находится в сбросе
        float delta_x; // % разночтение между управлением и предсказанием, чтобы потом по этому признаку решить о том подвижная цель или нет
        uint32_t transferCount; // счетчик удовлетворения условию переброса цели Статика -> Подвижная
#if KALMAN_DEBUG_INFO == 1        
        float debugLog[6]; //!! Только для отладки !!!
#endif        
    } TTrackObject;

    typedef TTrackObject * PTrackObject;

    typedef struct {
        TKLVMeasure dZ; // массив отклонений точек от предсказанного состояния трека
    } TTRdZpoint;


    typedef struct {
        uint16_t count; // кол-радарных отметок
        TKLVMeasure * Zpoints; //Указатель на массив радарных отметок
        char flag[TRACKING_MAX_TARGET]; //TTrackPointFlag: Признак захвата отметки треком
        // --- буфер для хранения радарных отметок, относящихся к одному треку
        uint16_t dZpointCount; //Кол-во отметок в буфере
        TKLVMeasure dZpoint[TRACKING_MAX_TARGET];
        //-------------------------------------------------------------------
    } TR_zCloud;

    typedef struct {
        float x; // Координаты X,Y цели
        float y; // 
    } TObjectXY;

    typedef struct {
        float EGO_speed;
        float delta_kurs;
        float delta_velocity;
        float rotMatrix[4];
    } TR_PlatformParam;

    typedef struct {
        //Время прошедшее между предыдущем и текущем кадром
        float dTime; 
        // первый запуск трекинга
        unsigned char firstFlag; 
        TTrackingType trType;
        //глобальный идентификатор кадра (счетчик вызовов TR)
        uint32_t frameId; 
        //глобальный идентификатор трека (счетчик треков)
        uint32_t track_id; 
        // Список указателей на активные треки
        //------------------------------
        // кол-во активных объектов / треков
        uint16_t objectCount; 
        PTrackObject objectHandle[TRACKING_MAX_TARGET];
        TTrackObject object[TRACKING_MAX_TARGET];
        // введено 26.06.2020
        TObjectXY xyOld[TRACKING_MAX_TARGET]; //аналог X4_old в матлабе
        // введено 12.03.2021 буфер для хранения XY координат отметок
        TObjectXY zpointXY[TRACKING_MAX_TARGET];
        //
        TR_zCloud zCloud;
        //Тестирование новой отметки в качестве трека...
        TKL_FilterState voidTrack; 
        //Величина неоднозначности по скорости. (2 * SPEED_MAX_AMB) (аналог flag_tracking_multiple*2 в матлабе)
        float totalVelocityAmbiguity; 
        //параметры платформы: собст. скорость, приращение по курсу, изменение скорости
        TR_PlatformParam platformParam; 
        //предел по целям достигнут.
        char  limitsDetected;
        uint8_t * pointCorrectedFailStatus;
        //------------------------------
    } TTrackingState;

    typedef struct {
        uint32_t tid;

        union {
            FLOATx S[TRACK_STATE_VECTOR_SIZE];
            TKLVState X;
        };
        FLOATx amp;
        float liveTime;
        float sbrosTime;


    } TR_TargetDesc;


    // Возможно, будет необходимоть сделать структуру конфигурации. Пока резерв...

    typedef struct {
        unsigned char reserv; //
        //------------------------------
    } TTrackingConfig;


    typedef TTrackingState * PTrackingState;


    //-----------------------------------------------------------------------------
    //--- Внешний интерфейс -------------------------------------------------------
    //-----------------------------------------------------------------------------
    uint16_t TR_Step(PTrackingState TR, uint16_t mNum, uint16_t mNumExt, TKLVMeasure * pointCloud, uint16_t * tNum, TR_TargetDesc * target, char ** zPointStatus);
    PTrackingState TR_Create(TTrackingConfig * Config, int32_t *errCode);
    void TR_Delete(PTrackingState TR);
    void TR_Clear(void);
    //-----------------------------------------------------------------------------
    // Для переменного FPS:
    void TR_DeltaTimeUpDate(PTrackingState TR, float dTime);
    //-----------------------------------------------------------------------------
    // для отладки ----------------------------------------------------------------
    //-----------------------------------------------------------------------------
    void TR_SelfTest(void);
    //-----------------------------------------------------------------------------
    //-----------------------------------------------------------------------------
    //--- Внутренний интерфейс ----------------------------------------------------
    //-----------------------------------------------------------------------------
    //void TR_Init(void);
    //uint32_t TR_StepInput(uint16_t mNum, TKLVMeasure * pointCloud);
    //void TR_StepOutput(uint16_t * tNum, TR_TargetDesc * target);
    //int16_t TR_AddTrack(TKLVMeasure * Zpoint);
    //int16_t TR_DelTrack(uint16_t index);
    //-----------------------------------------------------------------------------
    uint32_t TR_GetTrackStartTime(PTrackingState TR, uint16_t trackIndex);
    //-----------------------------------------------------------------------------
    // бонус. функция медианы -----------------------------------------------------
    //-----------------------------------------------------------------------------
    float getMedian(float * speedList, const unsigned short int count);

#ifdef __cplusplus
}
#endif

#endif /* TRACKING_H */

