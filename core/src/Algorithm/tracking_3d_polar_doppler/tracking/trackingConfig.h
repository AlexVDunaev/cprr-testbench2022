/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   trackingConfig.h
 * Author: Dell
 *
 * Created on 24 июля 2019 г., 14:28
 */

#ifndef TRACKINGCONFIG_H
#define TRACKINGCONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#define TRACKING_VERSION                   2247

#include "../kalman/kalmanConfig.h"

//Определим предельное кол-во точек на входе.
#define TRACKING_MAX_TARGET     KALMAN_MAX_dVECTORS_NUM // Внимание! kalmanConfig.h : KALMAN_MAX_dVECTORS_NUM 
//Производить расчет курса или нет. В новых версиях трекинга (2021г) Елена отказалась от расчета курса
#define TRACK_CALC_DELTA_KURS  0 // [0 | 1] 

//#define TRACK_TTL 6     // кол-во кадров, (если цель отсутствует) после которых цель удаляется
#define TRACK_SB_NUM       6 // % Параметр критерия сброса траектории (сколько тактов может пропустить завязывающийся трек перед тем, как мы скажем, что ему не удалось)
#define TRACK_SB_NUM_ZAV   6 // % Количество возможных пропусков для уже завязанной цели (сколько тактов трек может оказываться без отметок до того момента, когда мы его сбросим)
#define TRACK_ZAV_NUM      2 // % Время, в течении которого завязывается траектория (критерий завязки траектории)
#define TRACK_SB_EX_NUM    1 // % Проверяем первые два кадра после того, как цель завязалась
//2021.05.12 в коде матлаб константа TRACK_SB_LIMIT4REJECT названа num_frames
#define TRACK_SB_LIMIT4REJECT    1 // В рамках SB_EX_NUM кадров после завязки, сколько сбросов должен получить трек для удаления
#define TRACK_N_MAX        6 // Внимание! Заполнить руками! max(TRACK_SB_NUM, TRACK_SB_NUM_ZAV, TRACK_ZAV_NUM )
#define TRACK_LOSS_OF_REPUTATION_LIMIT (TRACK_SB_NUM_ZAV/2) // если цель начнёт терять точки, то забрать уже забранные отметки она не сможет (потеря репутации, как сказала Елена ^__^)
#define TRACK_REPUTATION_ENABLE  1 // [0 | 1] 0 - трек не забирает занятую точку, 1 - трек с хорошей репутацией использует чужие точки
#define TRACK_SBROS_FOR_HIDE     3 // 3 и более : цель не идет на выход % цели до какого значения счётчика сброса отображать на РЛИ

// новый функционал с 26.06.2020
#define TRACK_AMP_POROG    0.000316227766016838  // 10^(-70/20) уровень ярких целей, на которые завязка идёт быстрее
#define TRACK_ZAV_NUM_AMP  1 // критерий завязки для ярких целей (Время, в течении которого завязывается траектория)
#define TRACK_TRANS_CHECK_TTL  3 // Минимальное время жизни трека, для тестирование его на "подвижность" (STATIC->MOVE)
#define TRACK_TRANS_CHECK_NUM  (2) // необходимое кол-во проверок на "подвижность" (STATIC->MOVE)
#define TRACK_TRANS_SECTOR_ANGLE  35.0f // максимальный угол цели (от 0 по азимуту), для ее переноса в подвижный трекинг
#define TRACK_TRANS_DELTA_X_LIMIT 0.07f // пороговая ошибка предсказания положения цели по Х

    //------------------------------------------------------------------------------    
#define TRACK_REFLECTION_KOEF_V  3.0 //% Коэффициент для контроля допустимого расстояния целей по скорости. То есть он определяет, насколько далеко может быть расположена цель по
    //% скорости, чтобы мы посчитали её отдельным объектом и сохранили отдельно.
    //% Чем больше коэффициент, тем больше вероятность, что цель будет сгруппирована с уже существующей.
    //% Чем меньше коэффициент, тем меньше вероятность, что цель будет сгруппирована.
    //------------------------------------------------------------------------------    
#define TRACK_REFLECTION_KOEF_X  4.0 //Коэффициент при sqrt(K11)
#define TRACK_REFLECTION_KOEF_Y  4.0 //Коэффициент при sqrt(K44)

    //------------------------------------------------------------------------------    
#define TRACK_ERROR_KOEF_K11  16.0 //Коэффициент при sqrt(K44)

#define ROUND_VALUE(X) X = round(X*(double)1000000.0)/(double)1000000.0

// функция переноса отметок из неподвижного в подвижный трекинг
#define TRACKING_POINT_TRANSFER 1

// Обнаружил, что на вход asin/acos иногда проходит значения > |1.0| 
// При этом на выходе asin() будет nan! И все последующие расчета также дадут NAN!
#define CHECK_ACOS_ARG(a) {if ((a) > 1.0f) a = 1.0f; if ((a) < -1.0f) a = -1.0f;}

#define TRACK_SIGMA_VELOCITY_MUL    4.0f

    
#ifdef __cplusplus
}
#endif

#endif /* TRACKINGCONFIG_H */

