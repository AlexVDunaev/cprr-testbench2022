#ifndef KALMAN_H
#define KALMAN_H

#include <stdint.h>
#include "kalmanConfig.h"

#if KALMAN_DEBUG_INFO == 1
    extern float kalmanDebugParam[6];
#endif
    
#ifdef SUBSYS_DSS
#define DEBUG_PRINT(...)   
#else
    #ifdef NO_DEBUG_TEXT
        #define DEBUG_PRINT(...)   
    #else
    #define DEBUG_PRINT(...)   printf(__VA_ARGS__)
    #endif
#endif


extern int kl_currTrackID;

#ifdef KALMAN_LOG
    #define KL_DEBUG_SET_TRACK_ID(id)   kl_currTrackID = id;
#else
    #define KL_DEBUG_SET_TRACK_ID(id)
#endif

typedef struct {
    FLOATx x;
    FLOATx vx;
    FLOATx ax;
    FLOATx y;
    FLOATx vy;
    FLOATx ay;
} TKLVState;

typedef struct {
    TKLVState Dx;
    TKLVState Dvx;
    TKLVState Dax;
    TKLVState Dy;
    TKLVState Dvy;
    TKLVState Day;
} TKL_P_Matrix_field;

typedef union {
    FLOATx array[6 * 6];
    TKL_P_Matrix_field matrix;
} TKL_P_Matrix;

typedef struct {
    FLOATx range;
    FLOATx azimuth;
    FLOATx velocity;
    FLOATx amp;
} TKLVMeasure_field;

#define KL_MEASUREMENT_VECTOR_SIZE      4

typedef union {
    TKLVMeasure_field vector;
    FLOATx array[KL_MEASUREMENT_VECTOR_SIZE];
} TKLVMeasure;

typedef struct {
    TKLVState Range;
    TKLVState Azimuth;
    TKLVState Velocity;
} TKL_H_Matrix_field;

typedef union {
    FLOATx array[3 * 6];
    TKL_H_Matrix_field matrix;
} TKL_H_Matrix;

typedef union {
    FLOATx array[3 * 3];
} TKL_S_Matrix;

typedef struct {
    int sbros; // Флаг сбора равен нулю, потому что это абсолютно новая цель
    int flag_zavazka; // Одно измерение в траектории есть
    int status; // 0 - цель потенциальная; 1 - цель утверждённая
    int nachalo; // Флаг начала захвата объекта
    //FLOATx Amp; // Амплитудная информация о цели    
} TKL_TargetFlags;

typedef struct {
    //    TKL_TargetFlags flags;
    TKLVState X; // вектор состояния цели
    TKL_P_Matrix P; // ков.М.оценки X
    FLOATx amp; //Энергитическая характеристика трека (взвеш. амплитуда)
} TKL_FilterState;

//// Функция пользователя, которая будет оценивать попадание точек в трек
//typedef void (*TKLStrobeTest_callback)(TKLVMeasure * Z_pred);


typedef struct {
    //работа с текущей целью
    TKLVState x; // вектор состояния
    TKLVMeasure z; // вектор измерения
    TKLVMeasure Z_pred; // предсказанный вектор измерения
    TKLVState X_pred; // предсказанный вектор состояния
    TKLVState Bu; // управляющее воздействие
    //TKLVState x_pred; 
    TKL_H_Matrix H; // матрица измерения
    TKL_S_Matrix S; // ков. матрица невязки измерений
    TKL_P_Matrix P_pred; // ков.М.оценки X
    //------------------------------
    TKL_FilterState FS; //состояние фильтра
    //------------------------------
    // Промежуточные вычисления
    //------------------------------
    //FLOATx betta;
    FLOATx betta_o;
    FLOATx FP[6 * 6];
    FLOATx FT[6 * 6];
    FLOATx HT[3 * 6];
    FLOATx HP[3 * 6];
    FLOATx HPHT[6 * 6];
    FLOATx PHT[6 * 6];
    //FLOATx S[3 * 3];
    FLOATx SmOne[3 * 3]; //1/S
    FLOATx K[3 * 6];
    FLOATx KT[6 * 3];
    TKLVMeasure nu_buf; // невязка
    TKLVMeasure NU; // невязка
    FLOATx NU_SUM[3 * 1];
    FLOATx NU_SUM3x3[3 * 3];
    FLOATx NUNUT[3 * 3]; //NU*NU.'
    FLOATx DELTA_NU[3 * 3]; //NU_sum - NU*NU.'
    FLOATx WNU[1 * 6];
    FLOATx WS[3 * 6]; //buf_W * buf_S
    FLOATx WH[6 * 6]; //W*H
    FLOATx Pc[6 * 6]; //Pc
    FLOATx WDELTA_NU[3 * 6]; //buf_W * (NU_sum - NU*NU.')
    FLOATx Pw[6 * 6]; //Pc
    //FLOATx PBuf[6 * 6]; //buf_P
    FLOATx EeS[1 * 3]; //Ee' / S
    FLOATx eArray[KALMAN_MAX_dVECTORS_NUM]; //Придется хранить все exp()
    FLOATx betta[KALMAN_MAX_dVECTORS_NUM];  //Придется хранить все betta
} TKalmanFilter;

typedef struct {
    FLOATx     dZ[KALMAN_MAX_dVECTORS_NUM][KL_MEASUREMENT_VECTOR_SIZE];
} TKLVMeasureArray;


#define  POW2(X)         ((X)*(X))     // квадрат
#define  POW3(X)         ((X)*(X)*(X))   // куб
#define  MATRIX2BUF(WIDTH, w, h)         ((WIDTH) * (h) + (w))   // Перевод из матричной формы ([i, j] в линейный буфер)

#define TRACK_STATE_INIT_POINT_ONE 0
#define TRACK_STATE_INIT_POINT_TWO 1

void KL_Init(void);
void KL_CalcInitialValue(TKL_FilterState * FS, TKLVMeasure * z, char twoPointsInit, FLOATx dTime);

void KL_FilterSetState(TKL_FilterState * FS);
void KL_FilterGetState(TKL_FilterState * FS);
int KL_Z2X(TKLVMeasure * z, TKLVState * X);
FLOATx KL_getP_Matrix_K11(FLOATx Range, FLOATx  Azimuth);
FLOATx KL_getP_Matrix_K44(FLOATx Range, FLOATx  Azimuth);

TKLVMeasure * KL_FilterStepPrediction(char Bu, float * rotMatrix, float delta_velocity);
int KL_FilterStepCorrection(uint16_t zCount, TKLVMeasure * dZ);

FLOATx KL_FilterGetMahalanobis(TKLVMeasure * z, FLOATx vZone2, TKLVMeasure * zp, FLOATx * dVelocityResult) ;
void KL_FilterShowState(void);

// Для ПК версии - переменный FPS.
void KL_DeltaTimeUpDate(float dTime);


#endif /* KALMAN_H */

