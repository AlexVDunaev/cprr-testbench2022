
#ifndef KALMAN_CONIG_H
#define KALMAN_CONIG_H

#include <math.h>
// нужно значения UMB_VELOCITY, OBJECT_VELOCITY_MAX - определяются интевалом м/у чирпами
#include <radarConfig.h>

#define KALMAN_DEBUG_INFO         0 //режим выдачи доп. отладки

#ifndef FLOATx
#ifdef USE_FLOAT32
#define FLOATx float
#else
#define FLOATx double
#endif
#endif

#define KALMAN_MAX_dVECTORS_NUM     (1024*2) // Максимальное кол-во точек коррекции

#ifndef M_PI
    #define M_PI		((FLOATx)3.14159265358979323846)
#endif

//#define SNR             (39.810717055349734)        // 10 ^ (16/ 10)
//#define SNR_SQRT        (6.30957344480193)          // Нужно пересчитать при изменении SNR (sqrt(SNR==39.8107))
#define SNR             (19.9526231496888)          // 10 ^ (13/ 10)
#define SNR_SQRT        (4.46683592150963)          // Нужно пересчитать при изменении SNR (sqrt(SNR==19.952))
#define SQRT12          (3.46410161513775)          //  sqrt(12)


#define delta_Range     (0.8000) // Разрешение по дальности (м)
#define delta_Azimuth   (4.5300 * M_PI / (FLOATx)180.0) // Разрешение по азимуту (радианы)
#define delta_Velocity  (1.5000) // Разрешенеи по скорости (м/с) // 1.5 --> 1.0 (2020.04.20)
#define A_MAX           (0.5)

#define sigma_Range     ((FLOATx)(delta_Range / SNR_SQRT)) // % СКО измерения дальности (м)
#define sigma_Azimuth   ((FLOATx)(delta_Azimuth / SNR_SQRT)) // % СКО измерения азимута (радианы)
#define sigma_Velocity  ((FLOATx)(delta_Velocity / SNR_SQRT )) // % СКО измерения радиальной скорости (м/с)
#define sigma_a         ((FLOATx)(A_MAX/SQRT12)) // СКО ускорения

#define noise_ax       ((FLOATx)1.5 * (FLOATx)sigma_a) //% Немного скорректируем полученные шумы по ускорению (ось X)
#define noise_ay       ((FLOATx)1.5 * (FLOATx)sigma_a) //% Немного скорректируем полученные шумы по ускорению (ось Y)

//#define T              ((FLOATx)1.0f/(FLOATx)10.0f) //0.1            // dT
#define INTERFRAME_T   ((FLOATx)1.0/(FLOATx)30.0) //0.1            // dT

#define Gamma          ((FLOATx)16.266236196238130) // Нормированный к сигме параметр, который ограничивает объём строба.
#define P_detection    ((FLOATx)0.9000)             // Вероятность детектирования цели.
#define P_gate         ((FLOATx)0.9990)             // Веротяность попадания точки от цели внутрь строба.

#define VELOCITY_DOPPLER_BOUNDS (1.0f) // доплер на границе разрыва


#endif /* KALMAN_CONIG_H */

