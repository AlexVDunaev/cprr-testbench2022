#ifndef _CONFIG_PROVIDER_H_
#define _CONFIG_PROVIDER_H_

#include <stdint.h>

typedef enum  {
    CONFIG_PROVIDER_UNDEF             =0x0U,
    CONFIG_PROVIDER_TEXT_FILE,
    CONFIG_PROVIDER_ROS,
    CONFIG_PROVIDER_DEBUG 
} TConfigSource;

typedef enum  {
    CONFIG_OPTION_INT32      =0x0U,
    CONFIG_OPTION_FLOAT,
    CONFIG_OPTION_DOUBLE 
} TConfigTypes;


typedef enum  {
    CONFIG_PROVIDER_OK       =0x0U,
    CONFIG_PROVIDER_ERROR
} TConfigProvider_status;

TConfigProvider_status initConfigProvider(TConfigSource source, char * options);
TConfigProvider_status getConfigOption(char * optionName, int32_t subOption, TConfigTypes optionType, void * out);

 #endif


