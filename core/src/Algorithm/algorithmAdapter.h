#ifndef algAdapter_h
#define algAdapter_h

#include "./algorithmTypes.h"
#include "./configProvider.h"

// Кол-во отбрасываемых чирпов
#define PREFIX_CHIRP_NUM        4

// только данные, которые идут в алгоритм
#define WORK_FRAME_BUFFER_SIZE   (CHIRP_SAMPLE_FROM_RADAR * 64 * 16 * 2)
// данные с переходным процессом
#define PREFIX_FRAME_BUFFER_SIZE   (CHIRP_SAMPLE_FROM_RADAR * PREFIX_CHIRP_NUM * 16 * 2)
// полный кадр, который находится в датасете
#define FULL_FRAME_BUFFER_SIZE   (WORK_FRAME_BUFFER_SIZE + PREFIX_FRAME_BUFFER_SIZE)
//---------------


//------------------------------------------------------------------------------    
// максимальное кол-во векторов, которое можем хранить
#define TM_HOLDER_SIZE     20
// максимальное кол-во точек пролаза
#define LEAK_HOLDER_SIZE  (32*1000)

typedef struct {
        float       tCode;  // значение температуры, для которого применяется этот вектор
        float       value[32][2]; // 32 x [Re, Im]
    } TMCALVector;
    
typedef struct {    
        int tmVectorNum;
        TMCALVector vector[TM_HOLDER_SIZE];
} TMVectorHolder;

typedef struct {    
        int size;
        int16_t  leak[LEAK_HOLDER_SIZE];
} TMLeakHolder;

// структура хранения конфига радара. 
typedef struct {    
    uint32_t        SN;
    TMVectorHolder  tm;
    TMLeakHolder    leak;
} TRadarConfigHolder;
//------------------------------------------------------------------------------    

#define ROSBAG_RADAR_TEMPERATURE_NUM    6 // ожидаемое кол-во элементов температуры

typedef struct {    
    float time;
    int points;
    int channels;
    int chirps;
    int serialNumb;
    int16_t temperature[ROSBAG_RADAR_TEMPERATURE_NUM];
    // дополнительные параметры
    int    errors;
    double startFreq;
    double idleTime;
    double adcStartTime;
    double rampTime;
    double freqSlope;
    int    adcRate;
    int    txEnable; // bit mask
    int    chirpTimeArraySize;
    double *chirpTimeArray;
    
 } TDataFrameInfo;


int cprrAlgorithmGetFrame(char * dataFile, int frameIndex, void * pFrameBuffer, int bufferSize, int offset, TDataFrameInfo ** pInfo);
int cprrAlgorithmInit(char * configPath);
TTrackingData* cprrAlgorithmCalcFrame(void * pFrameBuffer, int mode, float deltaTime);
int cprrAlgorithmPackTargets(TTrackingData* trd, char * pBuffer, int bufferSize);
int cprrAlgorithmSetDefaultConfig(char * configPath);
int cprrAlgorithmGetConfig(char * bagFileName, TRadarConfigHolder * pHolder);
float cprrAlgorithmSetConfig(TRadarConfigHolder * pHolder, float tmCode);
int cprrAlgorithmGetActualTmIndex(TRadarConfigHolder * pHolder, float tmCode);

// CPYC
int cpycAlgorithmGetFrame(char * dataFile, int frameIndex, void * pFrameBuffer, int bufferSize, TDataFrameInfo ** pInfo);


#endif