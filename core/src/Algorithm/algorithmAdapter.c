/**
 * @file  algorithmAdapter.c 
 * @brief Модуль поддержки библиотеки алгоритма DSP
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "algorithmAdapter.h"
#include "../ROS/rosbagInterface.h"
#include "algorithmLibrary.h"

/// контекст алгоритма. Включает в себя все промежуточные данные
static TModelContext m;

#ifdef CPRRALG_DISABLE
#warning "CPRRALG DISABLE! DUMMY CODE!"
// вместо функций обработки сырого потока данных - заглушки.
TAlgorithm_config_status initAlgorithm(pMx mx) {
	return ALGORITHM_CONFIG_ERROR;
}

void deInitAlgorithm(pMx mx) {}

TAlgorithm_done_status stepAlgorithmDSP(pMx mx, TAlgorithm_done_callback done_callback, int16_t * pFrame) {
	return ALGORITHM_DONE_ERROR;
}

void updateRadarConfig(pMx mx, char externalConfig, int16_t * pLeakConfig, float * pCalibration) {}

void staticConfig(TRF_Config * config) {}

void loadConfig(char * dir, TRF_Config * config) {}

void showConfig(TRF_Config * config, void * runConfig) {}

int initGMonitoring(char * ip, unsigned int port) {
	return 0;
}

int addGMonitoring(int id, float subid, float * data, int count) {
	return 0;
}
int sendGMonitoring() {
	return 0;
}

int closeGMonitoring() {
	return 0;
}

TConfigProvider_status initConfigProvider(TConfigSource source, char * options) {
	return CONFIG_PROVIDER_OK;
}
#endif

/**
 * @brief Событие завершения расчета кадра
 *
 * Callback - функция передается при инициации расчета кадра
 * Промежуточные расчеты доступны в переменной TModelContext
 */
void doneFrameEvent(TAlgorithm_done_status done_status) {
    static int fpsCount = 0;

    if (done_status != ALGORITHM_DONE_OK)
        return;

    fpsCount++;
    printf("---------DONE FRAME (POINT:%d, TARGET:%d)\n", (int) m.pData->trackingData[0].PointCount, (int) m.pData->trackingData[0].TargetCount);
    //if ((fpsCount % 4 == 0)||(fpsCount == 1))
    {
        addGMonitoring(1, 0, m.pData->FFTDA_Data.threashold_dB, FFT_RANGE_USEFUL_WIDTH);
        addGMonitoring(1, 1, m.pData->monitoring_fft3d_max, FFT_RANGE_USEFUL_WIDTH);
        for (int n = 0; n < 4; n++) {
            addGMonitoring(2, n, &m.pData->monitoring_adc[n][0], CHIRP_SAMPLE_NUM);
        }
        for (int n = 0; n < 16; n++) {
            addGMonitoring(3, n, &m.pData->monitoring_adc_pp[n][0], 32);
        }

        if (m.pL2->FFTDA.peakList.peakCount) {
            int i = m.pL2->FFTDA.peakList.peakCount - 1;
            float id = m.pL2->FFTDA.peakList.peakCoordinate[i].r * 1.0 + m.pL2->FFTDA.peakList.peakCoordinate[i].d * 0.01;
            addGMonitoring(4, id, &m.pL2->FFTDA.peakList.peak[i].azimuth[0], AZIMUTH_FFT_SIZE);
        }
        float fft1[FFT_RANGE_USEFUL_WIDTH];
        for (int n = 0; n < FFT_RANGE_USEFUL_WIDTH; n++) {
            fft1[n] = sqrtf(m.pData->Spectr_R_A_D_DSP[n][16][0].Re * m.pData->Spectr_R_A_D_DSP[n][16][0].Re +
                    m.pData->Spectr_R_A_D_DSP[n][16][0].Im * m.pData->Spectr_R_A_D_DSP[n][16][0].Im);
            fft1[n] = 20.0 * log10(fft1[n]);
        }
        addGMonitoring(5, 1, fft1, FFT_RANGE_USEFUL_WIDTH);
        sendGMonitoring();
    }
}


/// Переменные для взаимодействия cprrAlgorithmGetConfig и cprrGetConfigMessage
static TRadarConfigHolder * pHolderArg = NULL;
static int ConfigMessageReceiveCount = 0;

/**
 * @brief Событие чтения очередного сообщения из rosbag-файла (для cprrAlgorithmGetConfig)
 *
 * Callback - функция передается в rosReadMessage(), которая 
 * читает из файла сообщения и передает их в данную функцию,
 * Если мы возвращаем ROSBAG_READ_NEXT, rosReadMessage() продолжает 
 * читать из файла след. сообщение и т.д. пока файл не кончится.
 * В файле могут быть сообщения любых типов, берем только нужные.
 * pHolderArg - буфер выделяется и освобождается в Lua (lua_newuserdata)
 * 
 */
static rosbag_read_cmd cprrGetConfigMessage(ros_message_type msg_type, double time, void * buffer, uint32_t size) {
    ConfigMessageReceiveCount++;
    if (msg_type == RadarSerialNumb) {
        pHolderArg->SN = *((uint32_t*)buffer);
        printf("[READ CONFIG]> ADD SN : %u\n", pHolderArg->SN);
    }
    
    if (msg_type == RadarTMCalib) {
        if (size == 65 * sizeof(float)) {
            float * p = (float *)buffer;
            if (pHolderArg->tm.tmVectorNum <= TM_HOLDER_SIZE) {
                int i = pHolderArg->tm.tmVectorNum++;
                pHolderArg->tm.vector[i].tCode = p[0];
                memcpy((void*)&pHolderArg->tm.vector[i].value, &p[1], sizeof(pHolderArg->tm.vector[i].value));
                printf("[READ CONFIG]> ADD TM Calib :%d [ [%.5f, %.5f], [%.5f, %.5f], [%.5f, %.5f]...[%.5f, %.5f]]\n", 
                        (int)p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[63], p[64]);
            }
        } else {
            printf("[READ CONFIG]> invalid size of RadarTMCalib message (%d)\n", (int)size);
        }
    }
    if (msg_type == RadarLeak) {
        if (size < sizeof(pHolderArg->leak.leak)) {
            pHolderArg->leak.size = size;
            memcpy((void*)&pHolderArg->leak.leak, buffer, size);
            printf("[READ CONFIG]> ADD LEAK (num:%d) [%d, %d, %d ... %d]\n", (int)size / sizeof(pHolderArg->leak.leak[0]), (int)pHolderArg->leak.leak[0], (int)pHolderArg->leak.leak[1], (int)pHolderArg->leak.leak[2], (int)pHolderArg->leak.leak[size/2 - 1]);
        } else {
            printf("[READ CONFIG]> invalid size of RadarLeak message (%d)\n", (int)size);
        }
    }
    return ROSBAG_READ_NEXT;
}


/**
 * @brief Загрузить конфиг из bag-файла в буфер pHolder
 * 
 * Если файл в себе содержит данные (т.е. много других сообщений),
 * он будет прочитан полностью.
 *
 * @param bagFileName - полное имя [+путь] к bag-файлу
 * @param pHolder - указатель на буфер конфигурации (управляет Lua)
 * @return кол-во сообщений (любого типа) считанных из файла
 */
int cprrAlgorithmGetConfig(char * bagFileName, TRadarConfigHolder * pHolder) {
    ConfigMessageReceiveCount = 0;
    pHolderArg = pHolder;
    if (pHolder) {
        memset(pHolder, 0, sizeof(TRadarConfigHolder));
        printf("cprrAlgorithmGetConfig> file: %s, buffer: %p\n", bagFileName, pHolder);
        rosDriverInit(bagFileName, ROSBAG_READ);
        rosReadMessage(cprrGetConfigMessage);
        rosDriverClose();
    }
    return ConfigMessageReceiveCount;
}


int cprrAlgorithmGetActualTmIndex(TRadarConfigHolder * pHolder, float tmCode) {
  // нужно выбрать ближайший температурный код в векторах калибровки.
    float minError = 100000;
    int minIndex = 0; // индекс калибровки по умолчанию!

    if (pHolder == NULL) {
        printf("Config> Error! (%p)\n", pHolder);
        return -1;
    }
    
    // поиск в списке векторов.
    for (int i = 0; i < pHolder->tm.tmVectorNum; i++) {
        float error = fabsf(tmCode - pHolder->tm.vector[i].tCode);
        if (minError > error) {
            minError = error;
            minIndex = i;
        }
    }
    printf("Config> tmCode: %.1f -> vector: %.1f (index : %d)\n", tmCode, pHolder->tm.vector[minIndex].tCode, minIndex);

    return minIndex;
}

/**
 * @brief Применить конфигурацию из pHolder. 
 * 
 * В алгоритм загружаются новые данные конфигурации в соответствии
 * с указанным температурным кодом.
 * В соответствии с tmCode происходит поиск ближайшего вектора калибровки в
 * буфере конфигурации (pHolder).
 * Важно помнить, что в буфере конфигурации, массив векторов калибровок, всегда 
 * начинается с калибровки по умолчанию (код 0)
 *
 * @param pHolder - буфер конфигурации
 * @param tmCode - температурный код. 0 - вектор по умолчанию. 
 * @return температурный код примененной калибровки
 */
float cprrAlgorithmSetConfig(TRadarConfigHolder * pHolder, float tmCode) {
  // нужно выбрать ближайший температурный код в векторах калибровки.
    int minIndex = cprrAlgorithmGetActualTmIndex(pHolder, tmCode);

    if (minIndex < 0)
        return -1;
        
    if (m.pData) {
        int16_t * pLeakConfig = NULL;
        if (pHolder->leak.size)
            pLeakConfig = &pHolder->leak.leak[0];
        updateRadarConfig(&m, 1, pLeakConfig, &pHolder->tm.vector[minIndex].value[0][0]);
        //-----------
        // 
        printf("NEW LEAK: [%d, %d, ...]\n", (int)pHolder->leak.leak[0], (int)pHolder->leak.leak[1]);
        printf("NEW TM Calib: [ [%f, %f], [%f, %f] .. ]\n", m.pL2->FFTR.caribration[0].Re, 
                m.pL2->FFTR.caribration[0].Im, m.pL2->FFTR.caribration[1].Re, m.pL2->FFTR.caribration[1].Im);
    }
    return pHolder->tm.vector[minIndex].tCode;
}

/**
 * @brief Применить конфигурацию из фалов конфига. 
 * 
 * В алгоритм загружаются новые данные конфигурации из файлов config.txt и leak.bin
 * из указанной директории. В пути используются '/'. Если '/' в конце - удалим его
 *
 * @param configPath - директория где лежат файлы config.txt и leak.bin
 * @return код ошибки. 0 - нет ошибки.
 */
int cprrAlgorithmSetDefaultConfig(char * configPath) {
    char configFilePath[255];
    if (configPath)
        if (configPath[strlen(configPath) - 1] == '/')
            configPath[strlen(configPath) - 1] = 0;

    snprintf(configFilePath, sizeof (configFilePath), "%s/config.txt", configPath);

    if (initConfigProvider(CONFIG_PROVIDER_TEXT_FILE, configFilePath) == CONFIG_PROVIDER_ERROR) {
        printf("initConfigProvider>ERROR!!!\n");
        return -1;
    }

    staticConfig(&m.Config);
    loadConfig(configPath, &m.Config);
    showConfig(&m.Config, NULL);
    if (m.pData) {
        updateRadarConfig(&m, 0, NULL, NULL);
        return -2;
    }
    return 0;
}

/**
 * @brief Инициализация модуля алгоритма 
 * 
 * Читаем конфиги, инициализируем алгоритм.
 * Дополнитель инициализируется модуль мониторинга, что бы выдавать на графики
 * информацию на каждый кадр (doneFrameEvent вызовы addGMonitoring)
 *
 * @param configPath - директория где лежат файлы config.txt и leak.bin
 * @return код ошибки. 0 - нет ошибки.
 */
int cprrAlgorithmInit(char * configPath) {
    static int firstRun = 1;

    memset((void *) &m, 0, sizeof (m));

    cprrAlgorithmSetDefaultConfig(configPath);
    initAlgorithm(&m);
    if (firstRun) {
        initGMonitoring("127.0.0.1", 7200);
        firstRun = 0;
    }
    return 0;
}

/// реализация скремблирования / дескремблирования
#define CPRR_STREAM_FRAME_MAX_SIZE   (2*1024*1024)
static char rosbagOpenFileName[255] = "";
static uint32_t scramblerBuffer[CPRR_STREAM_FRAME_MAX_SIZE / 4];
static int16_t frameInterlacing[CPRR_STREAM_FRAME_MAX_SIZE / 2];
static uint8_t dataBuffer[CPRR_STREAM_FRAME_MAX_SIZE];

static void Scrambler(uint32_t * restrict raw_u32, uint32_t * restrict scrmb, uint32_t length) {
    const int size = length / 4;
    for (int i = 16; i < size; ++i) {
        scrmb[i] = raw_u32[i] ^ scrmb[i - 15] ^ scrmb[i - 16];
    };
}

#define DC_SH1_VALUE    (17)
static void unScrambler(uint32_t * restrict raw_u32, uint32_t * restrict scrmb, uint32_t length) {
    const int size = length / 4;
    for (int i = 0; i < size - (DC_SH1_VALUE - 1); i++) {
        scrmb[i + DC_SH1_VALUE - 1] = raw_u32[i] ^ raw_u32[i + 1] ^ raw_u32[i + DC_SH1_VALUE - 1];
    }
}

void frameDoInterlacing(uint16_t * restrict normal, uint16_t * restrict interlacing, const int points, const int channels, const int chirps);
void frameUnInterlacing(uint16_t * restrict interlacing, uint16_t * restrict normal, const int points, const int channels, const int chirps);


/// метаданные кадра по умолчанию (могут отсутствовать в rosbag файле)
static TDataFrameInfo rosbagInfo = {.time =0.0f, .points = 720, .channels = 16, .chirps = 68, .serialNumb = -1, .temperature = {0,0,0,0,0,0}};


/**
 * @brief Читаем из rosbag-файла кадр данных и метаданные
 * 
 * Читаем сообщения из rosbag-файла, Если это сообщения типа RadarSerialNumb,
 * RadarTemper, RadarChirps и т.п. фиксируем их значения в auxParam. Если
 * сообщение типа RadarInt32Array (длиной больше 1Кб) т.е. кадр данных,
 * копируем его в pFrameBuffer, а указатель на метаданные в *pInfo.
 * Т.к. чтение сообщений идет только последовательно,
 * frameIndex - может принимать значения только 0..N - по нарастающей.
 * frameIndex == 0 используется для признака начала чтения нового файла.
 * Данные копируются в буфер в формате последовательных чирпов. (выборки АЦП 
 * идут последовательно для каждого канала)
 * 
 * @param frameIndex - требуемый индекс кадра (0..N - по нарастающей, без пропусков)
 * @param pFrameBuffer - буфер куда копировать кадр данных (только полезная часть данных!)
 * @param bufferSize - размер буфера
 * @param pInfo - указатель на TDataFrameInfo *. Может быть NULL. 
 * @return фактический размер данных. 0 - EOF. 
 */
int cprrAlgorithmGetFrameRosBag(char * dataFile, int frameIndex, void * pFrameBuffer, int bufferSize, int offset, TDataFrameInfo ** pInfo) {
    static int readFrameCount = 0;
    // Если требуется открыть еще файл, то нужно закрыть предыдущий
    if ((strcmp(rosbagOpenFileName, dataFile) != 0)||(frameIndex == 0)) {
        if (rosbagOpenFileName[0] != 0) {
            printf("cprrAlgorithmGetFrameRosBag> rosDriverClose()...\n");
            rosDriverClose();
            rosbagOpenFileName[0] = 0;
        }
    }

    if (rosbagOpenFileName[0] == 0) {
        printf("cprrAlgorithmGetFrameRosBag> rosDriverInit()...\n");
        if (rosDriverInit(dataFile, ROSBAG_READ) == 0) {
            strncpy(rosbagOpenFileName, dataFile, sizeof (rosbagOpenFileName));
            printf("rosbagOpenFileName: %s\n", rosbagOpenFileName);

            readFrameCount = 0;
        } else {
            // Error
            printf("cprrAlgorithmGetFrameRosBag> ERROR! 2\n");
            return 0;
        }
    }
    TROSAuxiliaryParam auxParam[] = {
        {RadarSerialNumb, 0},
        {RadarTemper, 0}, 
        {RadarChirps, 0},
        {RadarChanels, 0},
        {RadarPoints, 0},
        {RadarUndef, 0}
    };
    double time = 0;
    int result = rosReadDataFrame((void*) &scramblerBuffer, sizeof (scramblerBuffer), &time, auxParam, "");
    if (pInfo) *pInfo = &rosbagInfo;
    if (result == 0) {
        // конец файла
        printf("cprrAlgorithmGetFrameRosBag> EOF --> rosDriverClose()\n");
        rosDriverClose();
        rosbagOpenFileName[0] = 0;
        return 0;
    } else {
        if (result > 0) {
            // printf("FRAME INFO> TIME : %.4f SIZE:%d\n", (float) time, result);
            rosbagInfo.time = time;
            if (auxParam[0].size) rosbagInfo.serialNumb = *((int*) (&auxParam[0].valueBuffer));
            if (auxParam[1].size)
                for (int i = 0; i < auxParam[1].size / sizeof (int16_t); i++)
                    if (i < ROSBAG_RADAR_TEMPERATURE_NUM)
                        rosbagInfo.temperature[i] = ((int16_t*) (&auxParam[1].valueBuffer))[i];

            if (auxParam[2].size) rosbagInfo.chirps = *((int*) (&auxParam[2].valueBuffer));
            if (auxParam[3].size) rosbagInfo.channels = *((int*) (&auxParam[3].valueBuffer));
            if (auxParam[4].size) rosbagInfo.points = *((int*) (&auxParam[4].valueBuffer));

            if (frameIndex == readFrameCount) {
                // OK
                if (frameIndex == 0) {
                    // Вывод параметров кадра (на первом кадре)
                    printf("FRAME INFO> Points x Channels x Chirps:(%d x %d x %d), T:[ ", rosbagInfo.points, rosbagInfo.channels, rosbagInfo.chirps);
                    for (int i = 0; i < ROSBAG_RADAR_TEMPERATURE_NUM; i++) {
                        printf("%d ", (int) rosbagInfo.temperature[i]);
                    }
					printf("]\n");
                }
                int expectedSize = rosbagInfo.points * rosbagInfo.channels * rosbagInfo.chirps * sizeof (int16_t);
                if (result != expectedSize) {
                    printf("FRAME ERROR> frame size error : %db (expected size : %db)\n", result, expectedSize);
                } else {
                    if (result > CPRR_STREAM_FRAME_MAX_SIZE) {
                        printf("FRAME ERROR> frame size > FRAME_MAX_SIZE! (%d)\n", result);
                        return 0;
                    } else {
                        unScrambler(&scramblerBuffer[0], (uint32_t *) & frameInterlacing[0], result);
                        frameUnInterlacing((uint16_t *) & frameInterlacing[0], (uint16_t *) &dataBuffer[0], rosbagInfo.points, rosbagInfo.channels, rosbagInfo.chirps);
						int prefixSize = 0;
						if (offset == -1) {
							prefixSize = rosbagInfo.points * rosbagInfo.channels * PREFIX_CHIRP_NUM * sizeof (int16_t);							
						} else {
							prefixSize = offset;
						}
                        int usefulSize = result - prefixSize;
                        if (bufferSize < usefulSize) {
                            printf("cprrAlgorithmGetFrameRosBag> user buffer(%d) < frame size! (%d)\n", bufferSize, usefulSize);
                        } else {
                            memcpy(pFrameBuffer, (void*)&dataBuffer[prefixSize], usefulSize);
                            // Выдаем наружу только полезные данные
                            result = usefulSize;
                        }
                    }
                }
            } else {
                printf("cprrAlgorithmGetFrameRosBag> frameIndex: %d != readFrameCount: %d\n", frameIndex, readFrameCount);
                return 0;
            }
            readFrameCount++;
        } else {
            // ERROR result < 0
            printf("cprrAlgorithmGetFrameRosBag> ERROR (result : %d)\n", result);
        }
    }

    return result;

}


/**
 * @brief Читаем из bin-файла кадр данных
 * 
 * Читаем кадр из bin-файла (в нем нет ничего кроме данных кадра, поэтому pInfo
 * игнорируется). Кадры в файле находятся в сыром виде уже пригодном для алгоритма.
 * 
 * @param frameIndex - требуемый индекс кадра (0..N - возможен произвольный доступ)
 * @param pFrameBuffer - буфер куда копировать кадр данных (только полезная часть данных!)
 * @param bufferSize - размер буфера
 * @param pInfo - игнорируется
 * @return фактический размер данных. 0 - EOF. 
 */
int cprrAlgorithmGetFrameBin(char * dataFile, int frameIndex, void * pFrameBuffer, int bufferSize, TDataFrameInfo ** pInfo) {
    int result = 0;
    // dataSet file
    FILE *dsFile;

    dsFile = fopen(dataFile, "rb");
    if (dsFile) {
        // Узнаем размер файла
        fseek(dsFile, 0L, SEEK_END);
        long fileSize = ftell(dsFile);
        if (fileSize >= FULL_FRAME_BUFFER_SIZE * (frameIndex + 1)) {
            // ставим позицию на запрашиваемый кадр (сразу за неиспольз. данными)
            fseek(dsFile, FULL_FRAME_BUFFER_SIZE * frameIndex + PREFIX_FRAME_BUFFER_SIZE, SEEK_SET);
            // читаем из файла. префикс игнорируем
            if (bufferSize >= WORK_FRAME_BUFFER_SIZE) {
                result = fread((void*) pFrameBuffer, 1, WORK_FRAME_BUFFER_SIZE, dsFile);
            }
            fclose(dsFile);
        }
    }
    return result;
}

/**
 * @brief Читаем из bag/bin-файла кадр данных
 * 
 * Проверяем расширение файла, если это "bag" - то читаем как rosbag, иначе как
 * сырой бинарный файл.
 * 
 * @param frameIndex - требуемый индекс кадра 
 *  для bin - произвольный доступ,
 *  для bag - только последовательный доступ
 * @param pFrameBuffer - буфер куда копировать кадр данных (только полезная часть данных!)
 * @param bufferSize - размер буфера
 * @param pInfo - указатель на TDataFrameInfo *. Может быть NULL. Для bin-файлов игнорируется
 * @return фактический размер данных. 0 - EOF. 
 */
int cprrAlgorithmGetFrame(char * dataFile, int frameIndex, void * pFrameBuffer, int bufferSize, int offset, TDataFrameInfo ** pInfo) {

    char ext[] = ".bag";
    int fnLen = strlen(dataFile);
    int extLen = strlen(ext);
    if (fnLen < extLen) return -1;
    char isRosbag = (strncmp(&dataFile[fnLen - extLen], ext, extLen) == 0) ? 1 : 0;

    if (isRosbag)
        return cprrAlgorithmGetFrameRosBag(dataFile, frameIndex, pFrameBuffer, bufferSize, offset, pInfo);
    else
        return cprrAlgorithmGetFrameBin(dataFile, frameIndex, pFrameBuffer, bufferSize, pInfo);
}


/**
 * @brief расчет кадра данных
 * 
 * Передача кадра данных в модуль алгоритма.
 * 
 * @param pFrameBuffer - буфер кадра (только полезные данные) 
 * @param mode - параметр режима: 0 - штатный; 1 - Сырые детекции (без трекинга)
 * @param deltaTime - время с момента предыдущего кадра, с
 * @return список треков/сырых детекций (зависит от режима)
 */
TTrackingData* cprrAlgorithmCalcFrame(void * pFrameBuffer, int mode, float deltaTime) {
    m.pData->tracking_raw_mode = mode;
    m.pData->frameDeltaTime = deltaTime;
    stepAlgorithmDSP(&m, doneFrameEvent, (int16_t *) pFrameBuffer);
    return &m.pData->trackingData[0];
}

/**
 * @brief Упаковка списка целей в буфер
 * 
 * Упаковка целей в структуру аналогичную выдаваемому по сети, 
 * только с одним заголовком (формат драйвера cprr24T25)
 * 
 * @param trd - список целей/отметок
 * @param pBuffer - буфер, куда копировать
 * @param bufferSize - размер буфера
 * @return фактический размер буфера
 */
int cprrAlgorithmPackTargets(TTrackingData* trd, char * pBuffer, int bufferSize) {

    typedef struct __attribute__ ((__packed__)) {
        uint32_t STX;
        uint32_t LEN;
        uint32_t TYPE;
        uint32_t Status;
        uint64_t GrabNumber;
        uint64_t TimeStamp;
        double Speed;
    }
    TRadarHeader;

    // ID, x, vx, ax, y, vy, ay, Amp, lTime, sbTime

    typedef struct __attribute__ ((__packed__)) {
        int64_t ObjID;
        double Range;
        double Azimuth;
        int64_t LiveTime;
        double Rcs;
        double Xoord;
        double Xrate;
        double Xacceleration;
        double Ycoord;
        double Yrate;
        double Yacceleration;
    }
    TRadarTarget;

    int result = 0;

    if ((!trd) || (!pBuffer) || (bufferSize < sizeof (TRadarHeader))) return 0;

    TRadarHeader * pHeader = (TRadarHeader *) pBuffer;
    pHeader->GrabNumber = 0;
    pHeader->Speed = 0;
    pHeader->Status = 0;
    pHeader->TimeStamp = 0;
    TRadarTarget * pTargetBuffer = (TRadarTarget *) & pBuffer[sizeof (TRadarHeader)];
    result = sizeof (TRadarHeader);
    for (int i = 0; i < trd->TargetCount; i++) {
        if (bufferSize > result + sizeof (TRadarTarget)) {
            float x = trd->Target[i][1];
            float y = trd->Target[i][4];

            pTargetBuffer[i].ObjID = (int64_t) trd->Target[i][0];
            pTargetBuffer[i].Xoord = x;
            pTargetBuffer[i].Xrate = trd->Target[i][2];
            pTargetBuffer[i].Xacceleration = trd->Target[i][3];
            pTargetBuffer[i].Ycoord = y;
            pTargetBuffer[i].Yrate = trd->Target[i][5];
            pTargetBuffer[i].Yacceleration = trd->Target[i][6];
            pTargetBuffer[i].Rcs = trd->Target[i][7];
            pTargetBuffer[i].LiveTime = (int64_t) trd->Target[i][8];
            if (y != 0)
                pTargetBuffer[i].Azimuth = atanf(x / y)*180.0 / M_PI;
            pTargetBuffer[i].Range = sqrtf(x * x + y * y);
            result += sizeof (TRadarTarget);
        }
    }
    return result;
}



//----------------------
// CPYC
//----------------------


/**
 * @brief Читаем из rosbag-файла кадр данных и метаданные
 * 
 * Читаем сообщения из rosbag-файла, Если это сообщения типа RadarSerialNumb,
 * RadarTemper, RadarChirps и т.п. фиксируем их значения в auxParam. Если
 * сообщение типа RadarInt32Array (длиной больше 1Кб) т.е. кадр данных,
 * копируем его в pFrameBuffer, а указатель на метаданные в *pInfo.
 * Т.к. чтение сообщений идет только последовательно,
 * frameIndex - может принимать значения только 0..N - по нарастающей.
 * frameIndex == 0 используется для признака начала чтения нового файла.
 * Данные копируются в буфер в формате последовательных чирпов. (выборки АЦП 
 * идут последовательно для каждого канала)
 * 
 * @param frameIndex - требуемый индекс кадра (0..N - по нарастающей, без пропусков)
 * @param pFrameBuffer - буфер куда копировать кадр данных (только полезная часть данных!)
 * @param bufferSize - размер буфера
 * @param pInfo - указатель на TDataFrameInfo *. Может быть NULL. 
 * @return фактический размер данных. 0 - EOF. 
 */

int testRosbagFile(char * fileName, int verbose) ;

static double cpycChirpTimeArray[128];


static int getParamIndex(const char * topic, TROSAuxiliaryParamByTopic * optList) {
	for (int i = 0; 1; i++) {
		if (optList[i].topic != NULL) {
			if (strcmp(optList[i].topic, topic) == 0) {
				return i;
			}
		} else return i;
	}
	
}

int cpycAlgorithmGetFrameRosBag(char * dataFile, int frameIndex, void * pFrameBuffer, int bufferSize, TDataFrameInfo ** pInfo) {
	
    static int readFrameCount = 0;
    // Если требуется открыть еще файл, то нужно закрыть предыдущий
    if ((strcmp(rosbagOpenFileName, dataFile) != 0)||(frameIndex == 0)) {
        if (rosbagOpenFileName[0] != 0) {
            printf("cpycAlgorithmGetFrameRosBag> rosDriverClose()...\n");
            rosDriverClose();
            rosbagOpenFileName[0] = 0;
        }
    }

    if (rosbagOpenFileName[0] == 0) {
        printf("cpycAlgorithmGetFrameRosBag> rosDriverInit()...\n");
        if (rosDriverInit(dataFile, ROSBAG_READ) == 0) {
            strncpy(rosbagOpenFileName, dataFile, sizeof (rosbagOpenFileName));
            printf("rosbagOpenFileName: %s\n", rosbagOpenFileName);

            readFrameCount = 0;
        } else {
            // Error
            printf("cpycAlgorithmGetFrameRosBag> ERROR! 2\n");
            return 0;
        }
    }
    TROSAuxiliaryParamByTopic auxParam[] = {
        {"/RadarSerialNumb", 0},
        {"/RadarChirps", 0},
        {"/RadarAdcNum", 0},
        {"/RadarPoints", 0},
        {"/RadarStartFreq", 0},
        {"/RadarIdleTime", 0},
        {"/RadarAdcStartTime", 0},
        {"/RadarRampTime", 0},
        {"/RadarFreqSlope", 0},
        {"/RadarAdcRate", 0},
        {"/RadarTxEnable", 0}, // <-- массив.
        {"/RadarErrors", 0},
        {NULL, 0}
    };
    double time = 0;
	
	memset(cpycChirpTimeArray, 0, sizeof(cpycChirpTimeArray));
    int result = rosReadDataFrameEx("/radar_data_int32Array", pFrameBuffer, bufferSize, &time, "/radar_chirp_timeArray", &cpycChirpTimeArray[0], auxParam, "");
    if (pInfo) *pInfo = &rosbagInfo;
    if (result == 0) {
        // конец файла
        printf("cpycAlgorithmGetFrameRosBag> EOF --> rosDriverClose()\n");
        rosDriverClose();
        rosbagOpenFileName[0] = 0;
        return 0;
    } else {
        if (result > 0) {
			
#if 1 // export data
			{
				FILE * f = fopen("testExport.bin", "wb");
				if (f) {
					fwrite((void*) pFrameBuffer, 1, result, f);
					fclose(f);
				}
			}
#endif			
			
            //printf("FRAME INFO> TIME : %.4f SIZE:%d\n", (float) time, result);
            rosbagInfo.time = time;
			rosbagInfo.chirpTimeArraySize = sizeof(cpycChirpTimeArray)/sizeof(double);
			rosbagInfo.chirpTimeArray = &cpycChirpTimeArray[0];
			int i;
			// Заполнение полей 
			i = getParamIndex("/RadarSerialNumb", auxParam);
            if (auxParam[i].size) rosbagInfo.serialNumb = *((int32_t*) (&auxParam[i].valueBuffer));

			i = getParamIndex("/RadarChirps", auxParam);
            if (auxParam[i].size) rosbagInfo.chirps = *((int32_t*) (&auxParam[i].valueBuffer));
			
			i = getParamIndex("/RadarAdcNum", auxParam);
            if (auxParam[i].size) rosbagInfo.channels = *((int32_t*) (&auxParam[i].valueBuffer));
			
			i = getParamIndex("/RadarPoints", auxParam);
            if (auxParam[i].size) rosbagInfo.points = *((int32_t*) (&auxParam[i].valueBuffer));
			
			i = getParamIndex("/RadarStartFreq", auxParam);
            if (auxParam[i].size) rosbagInfo.startFreq = *((double*) (&auxParam[i].valueBuffer));
			
			i = getParamIndex("/RadarIdleTime", auxParam);
            if (auxParam[i].size) rosbagInfo.idleTime = *((double*) (&auxParam[i].valueBuffer));
			
			i = getParamIndex("/RadarAdcStartTime", auxParam);
            if (auxParam[i].size) rosbagInfo.adcStartTime = *((double*) (&auxParam[i].valueBuffer));
			
			i = getParamIndex("/RadarRampTime", auxParam);
            if (auxParam[i].size) rosbagInfo.rampTime = *((double*) (&auxParam[i].valueBuffer));
			
			i = getParamIndex("/RadarFreqSlope", auxParam);
            if (auxParam[i].size) rosbagInfo.freqSlope = *((double*) (&auxParam[i].valueBuffer));
			
			i = getParamIndex("/RadarAdcRate", auxParam);
            if (auxParam[i].size) rosbagInfo.adcRate = (int) *((double*) (&auxParam[i].valueBuffer));

			i = getParamIndex("/RadarErrors", auxParam);
            if (auxParam[i].size) rosbagInfo.errors = *((int32_t*) (&auxParam[i].valueBuffer));
			
			i = getParamIndex("/RadarTxEnable", auxParam);
			if (auxParam[i].size)
				for (int n = 0; n < 3; n++) {
					rosbagInfo.txEnable |= ((int32_t*)(&auxParam[i].valueBuffer))[n] << n;
				}

            if (frameIndex == readFrameCount) {
                // OK
                if (frameIndex == 0) {
                    // Вывод параметров кадра (на первом кадре)
                    printf("INFO> SN:[%d] Frame format: [%d x %d x %d] ERRORS: %d\n", rosbagInfo.serialNumb, rosbagInfo.points, rosbagInfo.channels, rosbagInfo.chirps, rosbagInfo.errors);
                    printf("INFO> StartFreq:%.2lfGHz, IdleTime:%.1lfmks, AdcStartTime:%.1lfmks, RampTime:%.2lfmks\n", rosbagInfo.startFreq, rosbagInfo.idleTime, rosbagInfo.adcStartTime, rosbagInfo.rampTime);
                    printf("INFO> FreqSlope:%.2lfMHz/mks, AdcRate:%d kbps, TX MASK:[%d]\n", rosbagInfo.freqSlope, rosbagInfo.adcRate, (int)rosbagInfo.txEnable);
                }
                int expectedSize = rosbagInfo.points * rosbagInfo.channels * rosbagInfo.chirps * sizeof (int16_t) * 2;
                if (result != expectedSize) {
                    printf("FRAME ERROR> frame size error : %db (expected size : %db)\n", result, expectedSize);
                } 
            } else {
                printf("cpycAlgorithmGetFrameRosBag> frameIndex: %d != readFrameCount: %d\n", frameIndex, readFrameCount);
                return 0;
            }
            readFrameCount++;
        } else {
            // ERROR result < 0
            printf("cpycAlgorithmGetFrameRosBag> ERROR (result : %d)\n", result);
        }
    }
    return result;
}

/**
 * @brief Читаем из bag/bin-файла кадр данных
 * 
 * только "bag"
 * 
 * @param frameIndex - требуемый индекс кадра (только последовательный доступ)
 * 
 * @param pFrameBuffer - буфер куда копировать кадр данных (только полезная часть данных!)
 * @param bufferSize - размер буфера
 * @param pInfo - указатель на TDataFrameInfo *. Может быть NULL. Для bin-файлов игнорируется
 * @return фактический размер данных. 0 - EOF. 
 */
int cpycAlgorithmGetFrame(char * dataFile, int frameIndex, void * pFrameBuffer, int bufferSize, TDataFrameInfo ** pInfo) {
	return cpycAlgorithmGetFrameRosBag(dataFile, frameIndex, pFrameBuffer, bufferSize, pInfo);
}
