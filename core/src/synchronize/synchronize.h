/* 
 * File:   synchronize.h
 * Author: Dunaev
 *
 * Created on 15 Декабрь 2016 г., 14:19
 */

#ifndef SYNCHRONIZE_H
#define	SYNCHRONIZE_H

#include <pthread.h>
#include <errno.h>
#include "../log/log.h"


#define MCORE_SYNC_LOCK        1
#define MCORE_SYNC_UNLOCK      0
#define MCORE_SYNC_INIT        -1
#define MCORE_SYNC_FREE        -2

#define MC_SYNC_RESULT_ERROR   -1

int mcSynchronize(int actionId, int index, char * dbgText);
int mcSynchronizeByName(int actionId, char * name, char * dText);
int mcSynchronizeInit(int argc, char **argv);
int mcSynchronizeGetCount(void);

#endif	/* SYNCHRONIZE_H */


