//
#include <stdlib.h>
#include <string.h>
#include "../main.h"
#include "synchronize.h"


//pthread_mutex_t mutex4Core; // все вызовы из ядра будем производить последовательно
int coreLockCount = 0;

//для обеспечения синхронизованного доступа к объектам синхронизации
pthread_mutex_t selfSynchronizeMutex; // 

#define SYNC_OBJECT_NUM         10

typedef struct {
    pthread_mutex_t mutex; // 
    int flags; // 0: закрыт. 1: открыт - можно использовать...
    char ID[100]; // Имя объекта, для доступа по имени
} TSyncObject;

struct {
    int count;
    TSyncObject item[SYNC_OBJECT_NUM];
    // +статистика...
} mcSyncObjects = {.count = 0};

int mcSynchronizeInit(int argc, char **argv) {

    pthread_mutex_init(&selfSynchronizeMutex, NULL);

}

static int mcSynchronizeFindByName(char * name);
int mcSynchronizeByName(int actionId, char * name, char * dText);

int mcSynchronizeGetCount(void) {
    return coreLockCount;
}

// При actionId == MCORE_SYNC_INIT: dText - Строковый идентификатор объекта,
// в остальных случаях просто отладочная информация, которая попадет в лог.
// return: [0..SYNC_OBJECT_NUM) индекс, -1 - ошибка!

int mcSynchronize(int actionId, int index, char * dText) {
    //lock :-1 инициализация
    //lock : 1 захватить мутекс
    //lock : 0 освобoдить мутекс
    int result = MC_SYNC_RESULT_ERROR;
    int lockResult;
    char objectName[50];

    /*
    lockResult = pthread_mutex_lock(&selfSynchronizeMutex);
    if (lockResult != 0) {
        if (lockResult = EBUSY) {
            Log_DBG("ERR! mcSynchronize> selfSynchronizeMutex. LOCK ERROR! Deadlock condition ");
        } else
            Log_DBG("ERR! mcSynchronize> selfSynchronizeMutex. ERROR = [%d]", lockResult);
        exit(1);
    }
-*/

    if (actionId == MCORE_SYNC_INIT) {
        // создание объекта без имени...
        sprintf(objectName, "nameless_%d", mcSyncObjects.count);
        result = mcSynchronizeByName(actionId, objectName, dText);
    } else {
        if (index >= mcSyncObjects.count)
            result = MC_SYNC_RESULT_ERROR;
        else {
            result = index;
            if (actionId == MCORE_SYNC_LOCK) {
                if (mcSyncObjects.item[index].flags) {
                    coreLockCount++;
                    if (coreLockCount > 1) {
                        if (SystemState.fDebugCore)
                            Log_DBG("DBG core_synchronize> coreLockCount = %d\n ", coreLockCount);
                    }
                    lockResult = pthread_mutex_lock(&mcSyncObjects.item[index].mutex);
                    if (SystemState.fDebugCore)
                        Log_DBG("+++ SYNC[%s] LOCK : '%s'", mcSyncObjects.item[index].ID, dText);
                    if (lockResult != 0) {
                        if (lockResult = EBUSY) {
                            Log_Ex("sync", LEVEL_ERR, (char *) "[%s] LOCK ERROR! Deadlock condition. (text: '%s')", mcSyncObjects.item[index].ID, dText);
                        } else
                            Log_Ex("sync", LEVEL_ERR, (char *) "[%s] LOCK ERROR! = %d. (text: '%s')", mcSyncObjects.item[index].ID, lockResult, dText);
                    } else { // 

                    }
                }
            }
            if (actionId == MCORE_SYNC_UNLOCK) {
                if (mcSyncObjects.item[index].flags) {
                    if (SystemState.fDebugCore)
                        Log_DBG("+++ SYNC[%s] UNLOCK : '%s'", mcSyncObjects.item[index].ID, dText);
                    coreLockCount--;
                    pthread_mutex_unlock(&mcSyncObjects.item[index].mutex);
                }
            }

            if (actionId == MCORE_SYNC_FREE) {
                pthread_mutex_destroy(&mcSyncObjects.item[index].mutex);
                mcSyncObjects.item[index].flags = 0;
            }
        }
    }

    //------------------------------------------------
    //pthread_mutex_unlock(&selfSynchronizeMutex);


    return result;
}

static int mcSynchronizeFindByName(char * name) {
    int num;
    int result = -1;
    int i;

    num = mcSyncObjects.count;

    for (i = 0; i < num; i++)
        if (mcSyncObjects.item[ i ].flags) {
            if (strcmp(mcSyncObjects.item[ i ].ID, name) == 0) { //
                result = i;
                break;
            }
        }
    return result;
}

int mcSynchronizeByName(int actionId, char * name, char * dText) {
    int result = MC_SYNC_RESULT_ERROR;
    int index;
    int findObject;

    findObject = mcSynchronizeFindByName(name);

    if (actionId == MCORE_SYNC_INIT) {
        index = mcSyncObjects.count;
        if (index >= SYNC_OBJECT_NUM) {
            Log_DBG("mcSynchronize> ERROR. Objects.count = %d\n ", index);
            result = MC_SYNC_RESULT_ERROR;
        } else {
            if (findObject != -1) { // объект с таким именем уже существует
                Log_DBG("mcSynchronize> ERROR. duplicate name of object.(%s)", name);
                result = MC_SYNC_RESULT_ERROR;
            } else {
                pthread_mutex_init(&mcSyncObjects.item[index].mutex, NULL);
                mcSyncObjects.item[index].flags = 1;
                strncpy(mcSyncObjects.item[index].ID, name, sizeof (mcSyncObjects.item[index].ID));
                if (SystemState.fDebugCore)
                    Log_DBG("+++ SYNC[%s] INIT : '%s'", mcSyncObjects.item[index].ID, dText);
                mcSyncObjects.count++;
                result = index;
            }
        }
    } else {
        if (findObject != -1)
            result = mcSynchronize(actionId, findObject, dText);
        else {
            Log_DBG("mcSynchronize> ERROR. object not find. (%s)", name);
            result = MC_SYNC_RESULT_ERROR;
        }
    }
    return result;
}
