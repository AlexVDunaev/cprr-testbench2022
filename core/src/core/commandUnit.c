#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../main.h"
#include <time.h>
#include "../core/commandUnit.h"
#include "../core/other.h"
#include "../log/log.h"
#include "../script/script.h"
#include "../interfaces/uart/ttys.h"
#include "../interfaces/interfaces.h"
#include "../protocols/protocols.h"
#include "../script/scriptAPI.h"



extern TSystemState SystemState;

// ---------------------------------------------------------------------
// --- функции упаковки значений в структуру ---------------------------
// ---------------------------------------------------------------------

int VALUE_SET_TYPE(Tsub_param_rec * rec, unsigned int type) {
    return 0;
}

int VALUE_SET_DWord(Tsub_param_rec * rec, unsigned int value) {
    if (rec == NULL) {
        Log_DBG("ERR!... VALUE_SET_DWord(rec = NULL)!");
        return 0;
    }
    rec->size = sizeof ( int);
    rec->tp = TYPE_ID_DWORD;
    rec->dwvalue = value;
    return rec->size;
}

int VALUE_SET_Word(Tsub_param_rec * rec, int value) {
    if (rec == NULL) {
        Log_DBG("ERR!... VALUE_SET_Word(rec = NULL)!");
        return 0;
    }
    rec->size = sizeof (short int);
    rec->tp = TYPE_ID_WORD;
    rec->wvalue = value;
    return rec->size;
}

int VALUE_SET_Byte(Tsub_param_rec * rec, unsigned char value) {
    if (rec == NULL) {
        Log_DBG("ERR!... VALUE_SET_Byte(rec = NULL)!");
        return 0;
    }
    rec->size = sizeof (char);
    rec->tp = TYPE_ID_BYTE;
    rec->bvalue = value;
    return rec->size;
}

int VALUE_SET_Float(Tsub_param_rec * rec, float value) {
    if (rec == NULL) {
        Log_DBG("ERR!... VALUE_SET_Float(rec = NULL)!");
        return 0;
    }
    rec->size = sizeof (float);
    rec->tp = TYPE_ID_FLOAT;
    rec->fvalue = value;
    return rec->size;
}

int VALUE_SET_Double(Tsub_param_rec * rec, double value) {
    if (rec == NULL) {
        Log_DBG("ERR!... VALUE_SET_Double(rec = NULL)!");
        return 0;
    }
    rec->size = sizeof (double);
    rec->tp = TYPE_ID_DOUBLE;
    rec->tvalue = value;
    return rec->size;
}

int VALUE_SET_Struct(Tsub_param_rec * rec, char * valuebuf, int size) {
    if (rec == NULL) {
        Log_DBG("ERR!... VALUE_SET_Struct(rec = NULL)!");
        return 0;
    }
    if (valuebuf == NULL) {
        Log_DBG("ERR!... VALUE_SET_Struct(valuebuf = NULL)!");
        return 0;
    }

    if (size <= STR_MAX_SIZE) {
        rec->size = size;
        rec->tp = TYPE_ID_STRUCT;
        memcpy(&rec->bvalue, valuebuf, size);
        return rec->size;
    } else {
        Log_DBG("\nERR!... VALUE_SET_Struct: size =%d\n", size);
        Log_Ex("err", LEVEL_ERR, (char *) "\nERR!... VALUE_SET_Struct: size =%d\n", size);
        return 0;
    }
}

int VALUE_SET_String(Tsub_param_rec * rec, char * valuebuf, int size) {
    if (rec == NULL) {
        Log_DBG("ERR!... VALUE_SET_String(rec = NULL)!");
        return 0;
    }
    if (size < STR_MAX_SIZE) {
        rec->size = size + 1; // для строки в буфере сигнала всегда добавим 0 символ
        rec->tp = TYPE_ID_STRING0;
        memcpy(&rec->bvalue, valuebuf, size);
        rec->scvalue[size] = 0;
        return rec->size;
    } else {
        Log_DBG("\nERR!... VALUE_SET_String: size =%d\n", size);
        Log_Ex("err", LEVEL_ERR, (char *) "\nERR!... VALUE_SET_String: size =%d\n", size);
        return 0;
    }
}

int VALUE_SET_String0(Tsub_param_rec * rec, char * valuebuf) {
    return VALUE_SET_String(rec, valuebuf, strlen(valuebuf));
}

int VALUE_COPY(Tsub_param_rec * rec, Tsub_param_rec * value) {
    if (rec == NULL) {
        Log_DBG("ERR!... VALUE_COPY(rec = NULL)!");
        return 0;
    }
    if (value == NULL) {
        Log_DBG("ERR!... VALUE_COPY(value = NULL)!");
        return 0;
    }

    if (value->size <= STR_MAX_SIZE) {
        rec->size = value->size;
        rec->tp = value->tp;
        memcpy(&rec->bvalue, &value->bvalue, value->size);
        return rec->size;
    } else {
        Log_DBG("\nERR!... VALUE_COPY: size =%d\n", value->size);
        Log_Ex("err", LEVEL_ERR, (char *) "\nERR!... VALUE_COPY: size =%d\n", value->size);
        return 0;
    }

}


// -----------------------------------------------------------------------------
// --- функции работы со структурой Tsub_param_rec ---------------------------
// -----------------------------------------------------------------------------

int VALUE_CONVERT_to_Int(Tsub_param_rec * rec) {
    if (rec == NULL) {
        Log_DBG("ERR!... rec2int(NULL)!");
        return 0;
    }
    if (rec->tp == TYPE_ID_BYTE) return rec->bvalue;
    if (rec->tp == TYPE_ID_WORD) return rec->wvalue;
    if (rec->tp == TYPE_ID_DWORD) return rec->dwvalue;
    if (rec->tp == TYPE_ID_BYTE_SIGN) return rec->sbvalue;
    if (rec->tp == TYPE_ID_WORD_SIGN) return rec->swvalue;
    if (rec->tp == TYPE_ID_DWORD_SIGN) return rec->ivalue;
    if (rec->tp == TYPE_ID_FLOAT) return rec->fvalue;
    if (rec->tp == TYPE_ID_DOUBLE) return rec->tvalue;
    Log_DBG("ERR!... VALUE_CONVERT (TO INT): tp = %d", rec->tp);
    Log_Ex("err", LEVEL_ERR, (char *) "\nERR!... VALUE_CONVERT (TO INT): tp = %d", rec->tp);
    return 0;
}

char isVALUE_CONVERT_to_Double(Tsub_param_rec * rec) {
    char result = 0;
    if (rec != NULL) {
        switch (rec->tp) {
            case TYPE_ID_BYTE:
            case TYPE_ID_WORD:
            case TYPE_ID_DWORD:
            case TYPE_ID_BYTE_SIGN:
            case TYPE_ID_WORD_SIGN:
            case TYPE_ID_DWORD_SIGN:
            case TYPE_ID_FLOAT:
            case TYPE_ID_DOUBLE:
                result = 1;
        }
    }
    return result;
}

double VALUE_CONVERT_to_Double(Tsub_param_rec * rec) {
    if (rec == NULL) {
        Log_DBG("ERR!... rec2Double(NULL)!");
        return 0;
    }
    if (rec->tp == TYPE_ID_BYTE) return rec->bvalue;
    if (rec->tp == TYPE_ID_WORD) return rec->wvalue;
    if (rec->tp == TYPE_ID_DWORD) return rec->dwvalue;
    if (rec->tp == TYPE_ID_BYTE_SIGN) return rec->sbvalue;
    if (rec->tp == TYPE_ID_WORD_SIGN) return rec->swvalue;
    if (rec->tp == TYPE_ID_DWORD_SIGN) return rec->ivalue;
    if (rec->tp == TYPE_ID_FLOAT) return rec->fvalue;
    if (rec->tp == TYPE_ID_DOUBLE) return rec->tvalue;
    Log_DBG("ERR!... VALUE_CONVERT_to_Double (type unsupported): tp = %d", rec->tp);
    Log_Ex("err", LEVEL_ERR, (char *) "ERR!... VALUE_CONVERT_to_Double (type unsupported): tp = %d", rec->tp);
    // У Максима возникает проблема в данной функции...тип левый...
    return 0;
}

double VALUE_CONVERT_from_Double(Tsub_param_rec * rec, double value) {
    if (rec == NULL) {
        Log_DBG("ERR!... toDouble(NULL)!");
        return 0;
    }
    if (rec->tp == TYPE_ID_BYTE) rec->bvalue = (char) value;
    else
        if (rec->tp == TYPE_ID_WORD) rec->wvalue = value;
    else
        if (rec->tp == TYPE_ID_DWORD) rec->dwvalue = value;
    else
        if (rec->tp == TYPE_ID_BYTE_SIGN) rec->sbvalue = value;
    else
        if (rec->tp == TYPE_ID_WORD_SIGN) rec->swvalue = value;
    else
        if (rec->tp == TYPE_ID_DWORD_SIGN) rec->ivalue = value;
    else
        if (rec->tp == TYPE_ID_FLOAT) rec->fvalue = value;
    else
        if (rec->tp == TYPE_ID_DOUBLE) rec->tvalue = value;
    else {
        Log_DBG("ERR!... VALUE_CONVERT from_Double. TYPE is UNDEF = %d", rec->tp);
        Log_Ex("err", LEVEL_ERR, (char *) "ERR!... VALUE_CONVERT from_Double. TYPE is UNDEF = %d", rec->tp);

        return 0;
    }
    return value;
}

int SL_TypeID2TypeStr(int TypeID, char * TypeStr) {
    if (TypeID == TYPE_ID_BYTE)
        sprintf(TypeStr, "u8");
    else
        if (TypeID == TYPE_ID_BYTE_SIGN)
        sprintf(TypeStr, "i8");
    else
        if (TypeID == TYPE_ID_WORD)
        sprintf(TypeStr, "u16");
    else
        if (TypeID == TYPE_ID_WORD_SIGN)
        sprintf(TypeStr, "i16");
    else
        if (TypeID == TYPE_ID_DWORD)
        sprintf(TypeStr, "u32");
    else
        if (TypeID == TYPE_ID_DWORD_SIGN)
        sprintf(TypeStr, "i32");
    else
        if (TypeID == TYPE_ID_FLOAT)
        sprintf(TypeStr, "f32");
    else
        if (TypeID == TYPE_ID_DOUBLE)
        sprintf(TypeStr, "f64");
    else
        if (TypeID == TYPE_ID_STRING0)
        sprintf(TypeStr, "sz");
    else
        if (TypeID == TYPE_ID_STRUCT)
        sprintf(TypeStr, "c");
    else
        return 0;
    return 1;
}

int SL_TypeStr2TypeID(char * TypeStr) {
    int TypeID = TYPE_ID_UNDEF;

    if ((strcmp(TypeStr, "u8") == 0) || (strcmp(TypeStr, "I8") == 0)) TypeID = TYPE_ID_BYTE;
    else
        if (strcmp(TypeStr, "i8") == 0) TypeID = TYPE_ID_BYTE_SIGN;
    else
        if ((strcmp(TypeStr, "u16") == 0) || (strcmp(TypeStr, "I16") == 0)) TypeID = TYPE_ID_WORD;
    else
        if (strcmp(TypeStr, "i16") == 0) TypeID = TYPE_ID_WORD_SIGN;
    else
        if ((strcmp(TypeStr, "u32") == 0) || (strcmp(TypeStr, "I32") == 0)) TypeID = TYPE_ID_DWORD;
    else
        if (strcmp(TypeStr, "i32") == 0) TypeID = TYPE_ID_DWORD_SIGN;
    else
        if (strcmp(TypeStr, "f32") == 0) TypeID = TYPE_ID_FLOAT;
    else
        if (strcmp(TypeStr, "f64") == 0) TypeID = TYPE_ID_DOUBLE;
    else
        if (strcmp(TypeStr, "sz") == 0) TypeID = TYPE_ID_STRING0;
    else
        if (TypeStr[0] == 'c') TypeID = TYPE_ID_STRUCT;

    return TypeID;
}


//Tsub_param_rec * -> f32:[0.1]
//Tsub_param_rec * -> i8:[244]
//Tsub_param_rec * -> c:[AA 00 CC]

char * VALUE_CONVERT_to_Text(Tsub_param_rec * rec) {
    static char valueText[255] = "null";
    char typeid[55] = "";
    char HexStr[255] = "";
    int hexBuffLength = 0;

    if (rec == NULL)
        return valueText;
    SL_TypeID2TypeStr(rec->tp, typeid);
    switch (rec->tp) {
        case TYPE_ID_BYTE:
            sprintf(valueText, "I8:[%d]", rec->bvalue);
            break;
        case TYPE_ID_BYTE_SIGN:
            sprintf(valueText, "i8:[%d]", rec->sbvalue);
            break;
        case TYPE_ID_WORD:
            sprintf(valueText, "I16:[0x%04x]", rec->wvalue);
            break;
        case TYPE_ID_WORD_SIGN:
            sprintf(valueText, "i16:[%d]", rec->swvalue);
            break;
        case TYPE_ID_DWORD:
            sprintf(valueText, "I32:[0x%08x]", rec->dwvalue);
            break;
        case TYPE_ID_DWORD_SIGN:
            sprintf(valueText, "i32:[%d]", rec->ivalue);
            break;
        case TYPE_ID_FLOAT:
            sprintf(valueText, "f32:[%f]", rec->fvalue);
            break;
        case TYPE_ID_DOUBLE:
            sprintf(valueText, "f64:[%f]", rec->tvalue);
            break;
        case TYPE_ID_STRING0:
            sprintf(valueText, "sz:['%s']", rec->strvalue);
            break;
        case TYPE_ID_STRUCT:
            hexBuffLength = 60;
            if (rec->size < hexBuffLength) {
                bufToHexStr(rec->strvalue, rec->size, HexStr);
                sprintf(valueText, "c%d:[%s]", rec->size, HexStr);
            } else {
                bufToHexStr(rec->strvalue, hexBuffLength, HexStr);
                sprintf(valueText, "c%d:[%s ...]", rec->size, HexStr);
            }
            break;
        default:
            sprintf(valueText, "undef:[ ]");
    }
    return valueText;

}



//-----------------------------------------------------------------------------

TLineRec * BuildLineRec(TSystemState * State, char * lineName, int lineID, char * lineParams, int proc) {
    int j;
    TLineRec * a;

    a = (TLineRec *) mallocEx(sizeof (TLineRec));
    snprintf(a->name, sizeof (a->name), "%s", lineName);
    snprintf(a->params, sizeof (a->params), "%s", lineParams);
    a->id = lineID;
    a->callBackLua = proc;
    return a;
}

// Считаем что память для TLineRec * a уже виделена. Используем только указатель а

void AppendLine(TSystemState * State, TLineRec * a, char source) {

    a->source = source;
    a->DriverIndex = -1;
    a->ProtocolIndex = -1;
    a->LineControl.LineStatus = CLIENT_STATUS_OFF;
    a->fd = 0;
    State->LLC.Lines[State->LLC.count++] = a;
    return;

}


//-----------------------------------------------------------------------------

TProtFramingRec * BuildFramingRec(TSystemState * State, char * FramingName, char * FramingParams, int proc) {
    int j;
    TProtFramingRec * a;

    a = (TProtFramingRec *) mallocEx(sizeof (TProtFramingRec));

    snprintf(a->name, sizeof (a->name), "%s", FramingName);
    a->callBackLua = proc;

    return a;
}

void AppendFramingUnit(TSystemState * State, TProtFramingRec * a, char source) {

    a->source = source;
    State->FramingUnits.Framing[State->FramingUnits.count++] = a;
    return;

}
//-----------------------------------------------------------------------------

TProtocolRec * BuildProtocolRec(TSystemState * State, char * protocolName, char * protocolParams, int proc) {
    int j;
    TProtocolRec * a;
    char * strParams;

    a = (TProtocolRec *) mallocEx(sizeof (TProtocolRec));

    snprintf(a->name, sizeof (a->name), "%s", protocolName);
    snprintf(a->params, sizeof (a->params), "%s", protocolParams);
    // из protocolParams нужно вычленить обязательные поля:
    // FRAMING=FFF и POLLING=PPP, где FFF - имя драйвера обертки, PPP - имя драйвера опроса
    // Пример: "FRAMING=BSAP.FRAME POLLING=INTERNAL"
    a->framingName[0] = 0;
    a->pollingName[0] = 0;
    strParams = (char *) get_ParamValue(protocolParams, (char*) "FRAMING");
    if (strParams != NULL)
        snprintf(a->framingName, sizeof (a->framingName), "%s", strParams);
    strParams = (char *) get_ParamValue(protocolParams, (char*) "POLLING");
    if (strParams != NULL)
        snprintf(a->pollingName, sizeof (a->pollingName), "%s", strParams);
    a->callBackLua = proc;
    return a;
}

void AppendProtocolUnit(TSystemState * State, TProtocolRec * a, char source) {
    a->source = source;
    State->Protocols.Protocol[State->Protocols.count++] = a;
    return;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

TPollingRec * BuildPollingRec(TSystemState * State, char * lineName, char * lineParams, int proc) {
    int j;
    TPollingRec * a;

    a = (TPollingRec *) mallocEx(sizeof (TPollingRec));
    memset(a, 0, sizeof (TPollingRec));

    snprintf(a->name, sizeof (a->name), "%s", lineName);
    a->nextDelay = 0;
    a->callBackAddr = proc;

    return a;
}

void AppendPollingUnit(TSystemState * State, TPollingRec * a, char source) {
    a->source = source;
    State->PollingUnits.Polling[State->PollingUnits.count++] = a;
    return;
}
//-----------------------------------------------------------------------------

void AppendLinkDriver(TSystemState * State, char source, char * DriverName, char * DriverParams, void * proc, int EventMask) {
    TMACRec * a;

    a = (TMACRec *) mallocEx(sizeof (TMACRec));
    memset(a, 0, sizeof (TMACRec));

    snprintf(a->name, sizeof (a->name), "%s", DriverName);
    snprintf(a->params, sizeof (a->params), "%s", DriverParams);
    a->callBackVoid = proc;
    a->eventMask = EventMask;
    a->source = source;
    State->MAC.Drivers[State->MAC.count++] = a;
    Log_DBG("add driver [%s]", DriverName);

    return;
}
//-----------------------------------------------------------------------------

int LineDrvEvent(int lineID, char typeCallBack, void * ext, int clientIndex, unsigned char * TxBuffer, int TxCount, unsigned char * RxBuffer, int *RxCount) {
    Log_DBG("ERR!... LineDrvEvent!!!!");
}

//-----------------------------------------------------------------------------

void coreLinkOpen(char source, int LineID, char * DriverName, char * DeviceName, char * linkParam, char * ProtocolName, char * ProtocolParam) {
    int i;
    int j;
    int result = 0;
    char LineIndex = -1;
    char DriverIndex = -1;
    char ProtocolIndex = -1;
    char FramingIndex = -1;
    char * strParams;
    char ProtocolFramingName[255];

    TLineRec * a;
    TMCExtDriver extDriver;
    memset(&extDriver, 0, sizeof (extDriver));

    // проверим что требуемый драйвер существует
    for (i = 0; i < SystemState.MAC.count; i++) {
        if (strcmp(SystemState.MAC.Drivers[i]->name, DriverName) == 0) { // требуемый драйвер существует...
            result = 1;
            DriverIndex = i;
        }
    }
    if (result == 0) {
        Log_DBG("ERR!... coreLinkOpen(LineID=%d, Driver=%s, Device=%s, Params=%s, Protocol=%s ): Driver not found!", LineID, DriverName, DeviceName, linkParam, ProtocolName);
        return;
    }
    result = 0;
    // проверим что требуемый протокол существует
    for (i = 0; i < SystemState.Protocols.count; i++) {
        if (strcmp(SystemState.Protocols.Protocol[i]->name, ProtocolName) == 0) { // требуемый протокол существует...
            // проверим, в порядке ли драйвер обертки у протокола
            // драйвер может быть задан "жестко" при создании модуля протокола, а может быть передан параметром при открытии линии
            snprintf(ProtocolFramingName, sizeof (ProtocolFramingName), "%s", SystemState.Protocols.Protocol[i]->framingName);
            strParams = (char *) get_ParamValue(ProtocolParam, (char*) "FRAMING");
            if (strParams != NULL)
                snprintf(ProtocolFramingName, sizeof (ProtocolFramingName), "%s", strParams);
            // переопределен ли менеджер опроса
            strParams = (char *) get_ParamValue(ProtocolParam, (char*) "POLLING");
            if (strParams != NULL)
                snprintf(SystemState.Protocols.Protocol[i]->pollingName, sizeof (SystemState.Protocols.Protocol[i]->pollingName), "%s", strParams);

            for (j = 0; j < SystemState.FramingUnits.count; j++) {
                if (strcmp(SystemState.FramingUnits.Framing[j]->name, ProtocolFramingName) == 0) { // требуемый драйвер обертки найден
                    FramingIndex = j;
                    result = 1;
                }
            }
            ProtocolIndex = i;
        }
    }
    if (result == 0) {
        if ((FramingIndex == -1)&&(ProtocolIndex != -1))
            Log_DBG("ERR!... coreLinkOpen(LineID=%d, Driver=%s, Device=%s, Params=%s, Protocol=%s ): Framing driver (%s) not found!", LineID, DriverName, DeviceName, linkParam, ProtocolName,
                SystemState.Protocols.Protocol[ProtocolIndex]->framingName);
        else
            Log_DBG("ERR!... coreLinkOpen(LineID=%d, Driver=%s, Device=%s, Params=%s, Protocol=%s ): Protocol not found!", LineID, DriverName, DeviceName, linkParam, ProtocolName);
        return;
    }
    result = 0;

    // проверим на соответствие идентификатора линии
    for (i = 0; i < SystemState.LLC.count; i++) {
        if (SystemState.LLC.Lines[i]->id == LineID) { // все параметры в норме.
            result = 1;
            a = SystemState.LLC.Lines[i];
            LineIndex = i;
            a->DriverIndex = DriverIndex;
            a->ProtocolIndex = ProtocolIndex;
            a->FramingIndex = FramingIndex;

            snprintf(a->DriverName, sizeof (a->DriverName), "%s", DriverName);
        }
    }
    if (result == 0) {
        Log_DBG("ERR!... coreLinkOpen(LineID=%d, Driver=%s, Device=%s, Params=%s, Protocol=%s ): LineID not found!", LineID, DriverName, DeviceName, linkParam, ProtocolName);
        return;
    }
    //----------------------------
    extDriver.id = MAC_ID_OPEN;
    extDriver.LineIndex = LineIndex;
    extDriver.param.prOPEN.device = DeviceName;
    extDriver.param.prOPEN.param = linkParam;
    RunMacDriverCallBack(LineIndex, &extDriver); //MAC_ID_OPEN
    return;
}
//-----------------------------------------------------------------------------

int coreLinkTx(char source, int LineID, char * buffer, int bufferSize) {
    int i;
    int j;
    int result = 0;
    TLineRec * a;

    for (i = 0; i < SystemState.LLC.count; i++) {
        if (SystemState.LLC.Lines[i]->id == LineID) {
            result = 1;
            a = SystemState.LLC.Lines[i];
            if (source == OBJ_SOURCE_SCRIPT) { // функция вызванная из скрипта...
                TMACDriverArg arg = {.prTX.inBuff = buffer, .prTX.inCount = bufferSize, .prTX.outBuff = NULL, .prTX.outBuffCount = NULL};
                return RunMacDriverCallBackEx(MAC_ID_WRITE_BUFFER, i, arg, NULL);

            }
        }
    }
    if (result == 0) {
        Log_DBG("ERR!... coreLinkTx(): LineID not found! LineID = %d", LineID);

    }

    return MAC_RESULT_ERROR;
}
//-----------------------------------------------------------------------------

int coreLinkRx(char source, int LineID, char * buffer, int * bufferSize, int timeOut) {
    int i;

    for (i = 0; i < SystemState.LLC.count; i++) {
        if (SystemState.LLC.Lines[i]->id == LineID) {
            if (source == OBJ_SOURCE_SCRIPT) {
                //Log_DBG("coreLinkRx> [buffer : %p, timeOut : %d]", LineID, timeOut);
                TMACDriverArg arg = {.prRX.timeOut = timeOut, .prRX.rxBuff = buffer, .prRX.rxCount = bufferSize};
                return RunMacDriverCallBackEx(MAC_ID_READ_BUFFER, i, arg, NULL);
            }
        }
    }

    Log_DBG("ERR!... coreLinkRx(): LineID not found! LineID = %d", LineID);

    return MAC_RESULT_ERROR;
}
//-----------------------------------------------------------------------------

int RunFramingCallBack(char typeCallBack, int index, char * buffInput, int *buffInputSize, char * frameOutput, int * frameOutputSize, char * ErrorText) {
    int i;
    int j;
    int InputSize = 0;
    char idEvent[50] = "";
    int result = 0;
    char dbgText[255] = "";
    char dbgTextEx[255] = "";

    TProtFramingRec* a;

    if (SystemState.fDebug)
        Log_DBG("Call DBG>Framing(%d, index: %d ...)\n", typeCallBack, index);

    if (index >= SystemState.FramingUnits.count) {
        Log_DBG("ERR!... FramingUnits index! (index = %d)", index);
        return 0;
    }

    a = SystemState.FramingUnits.Framing[index];
    if (typeCallBack == PROT_FRAMING_PACK) sprintf(idEvent, "TX");
    if (typeCallBack == PROT_FRAMING_UNPACK) sprintf(idEvent, "RX");

    if (buffInputSize != NULL)
        InputSize = *buffInputSize;
    if (SystemState.fDebug) { // сохраним первоначальный буфер для лога
        if ((typeCallBack == PROT_FRAMING_PACK) || (typeCallBack == PROT_FRAMING_UNPACK)) {
            Log_buf2strEx(dbgText, sizeof (dbgText), buffInput, InputSize);
        }
    }

    if (a->source == OBJ_SOURCE_EMBEDDED) { // вызов обработчика в формате внутр. вызовов
        if (a->callBack)
            result = a->callBack(typeCallBack, buffInput, buffInputSize, frameOutput, frameOutputSize, ErrorText);
    } else
        if (a->source == OBJ_SOURCE_USER) { // вызов обработчика в формате внешних C-функций
        // !!! нужно сделать более удобный и изолированный интерфейс
        if (a->callBack)
            result = a->callBack(typeCallBack, buffInput, buffInputSize, frameOutput, frameOutputSize, ErrorText);
    } else
        if (a->source == OBJ_SOURCE_SCRIPT) { // вызов обработчика в формате внешних Lua-функций
        // !!! нужно заблокировать возможность изменения buffInput во время работы скрипта!
        if (a->callBackLua)
            result = SystemState.pScriptEngine->Events.OnFraming(idEvent, a->callBackLua, buffInput, buffInputSize, frameOutput, frameOutputSize, ErrorText);
    }

    if (SystemState.fDebug) {
        if ((typeCallBack == PROT_FRAMING_PACK) || (typeCallBack == PROT_FRAMING_UNPACK)) {
            Log_buf2strEx(dbgTextEx, sizeof (dbgTextEx), frameOutput, *frameOutputSize);
            Log_Ex("dbg", LEVEL_DEBUG, "FRAMING[%s](%s): [%d:[%s ]] : %d -> [%d:[%s ]]", a->name, idEvent, InputSize, dbgText, result, *frameOutputSize, dbgTextEx);
        }
        if (typeCallBack == PROT_FRAMING_INIT) {
            Log_Ex("dbg", LEVEL_DEBUG, "FRAMING[%s](INIT) : %d", a->name, result);
        }

    }

    return result;
}

//-----------------------------------------------------------------------------

int RunProtocolCallBack(char typeCallBack, int LineIndex, int index, void * UI, char * signalName, Tsub_param_rec * value, char * pktInBuffer, int pktInSize, char * pktOutBuffer, int * pktOutSize, char * ErrorText) {
    int i;
    int j;
    int size = 0;
    char idEvent[50] = "";
    int result = 0;
    char dbgText[255] = "";
    char dbgTextEx[255] = "";

    TProtocolRec * a;

    if (index >= SystemState.Protocols.count) {
        Log_DBG("ERR!... PollingUnits index! (index = %d)", index);
        return 0;
    }

    UI = &SystemState.UI;

    a = SystemState.Protocols.Protocol[index];

    if (SystemState.fDebug)
        Log_DBG("Call DBG>Protocol(%d, index: %d (%s)...)\n", typeCallBack, index, a->name);

    if (typeCallBack == PROT_MNG_RX) sprintf(idEvent, "RX");

    if (a->source == OBJ_SOURCE_EMBEDDED) { // вызов обработчика в формате внутр. вызовов
        if (a->callBack)
            result = a->callBack(typeCallBack, UI, signalName, value, pktInBuffer, pktInSize, pktOutBuffer, pktOutSize, ErrorText);
    } else
        if (a->source == OBJ_SOURCE_USER) { // вызов обработчика в формате внешних C-функций
        if (a->callBack)
            result = a->callBack(typeCallBack, UI, signalName, value, pktInBuffer, pktInSize, pktOutBuffer, pktOutSize, ErrorText);
    } else
        if (a->source == OBJ_SOURCE_SCRIPT) { // вызов обработчика в формате внешних Lua-функций
        // !!! нужно заблокировать возможность изменения buffInput во время работы скрипта!
        if (a->callBackLua) { //"REQ", "SIGNAL_KU", "SIGNAL_REQ", "RX", "ERROR", "ABORT"
            result = SystemState.pScriptEngine->Events.OnProtocol(idEvent, LineIndex, a->callBackLua, UI, signalName, value, pktInBuffer, pktInSize, pktOutBuffer, pktOutSize, ErrorText);
        }
    }

    if (SystemState.fDebug) {
        if ((typeCallBack == PROT_MNG_REQ) || (typeCallBack == PROT_MNG_RX)) {
            Log_buf2strEx(dbgText, sizeof (dbgText), pktInBuffer, pktInSize);
            if (pktOutBuffer == NULL)
                sprintf(dbgTextEx, "NULL");
            else
                Log_buf2strEx(dbgTextEx, sizeof (dbgTextEx), pktOutBuffer, *pktOutSize);
            Log_Ex("dbg", LEVEL_DEBUG, "PROTOCOL[%s](%s): [%d:[%s ]] : %d -> [%d:[%s ]]", a->name, idEvent, pktInSize, dbgText, result, *pktOutSize, dbgTextEx);
        }
        if ((typeCallBack == PROT_MNG_KU_SIGNAL) || (typeCallBack == PROT_MNG_REQ_SIGNAL)) {
            if (signalName == NULL)
                sprintf(dbgText, "NULL");
            else
                sprintf(dbgText, "%s", signalName);

            Log_Ex("dbg", LEVEL_DEBUG, "PROTOCOL[%s](%s): [Signal :'%s'] : %d ", a->name, idEvent, dbgText, result);
        }

    }


    return result;
}


//-----------------------------------------------------------------------------
// char * buffInput, int buffInputSize, char * buffOutput, int * buffOutputSize,
int LineCallBackIndex = -1;
int RunLineCallBack(char typeCallBack, int index, TMACDriverArg arg, char * ErrorText) {
    char idEvent[50] = "undef";
    TLineRec* a;
    int result = 0;
    char dbgText[255] = "";
    char dbgTextEx[255] = "";

    if (typeCallBack == MAC_ID_WRITE_BUFFER) sprintf(idEvent, "TX");
    if (typeCallBack == MAC_ID_READ_BUFFER) sprintf(idEvent, "RX");
    if (typeCallBack == MAC_ID_OPEN) sprintf(idEvent, "INIT");
    if (typeCallBack == MAC_ID_CLOSE) sprintf(idEvent, "CLOSE");
    if (typeCallBack == MAC_ID_CTRL) sprintf(idEvent, "CTRL");
    if (typeCallBack == MAC_ID_POLL) sprintf(idEvent, "POLL");
    if (typeCallBack == MAC_ID_ERROR) sprintf(idEvent, "ERROR");

    if (SystemState.fDebug)
        Log_DBG("Call DBG>Line('%s', index: %d ...)\n", idEvent, index);

    if ((index >= SystemState.LLC.count) || (index < 0)) {
        Log_DBG("ERR!... LLC index! (index = %d)", index);
        return 0;
    }

    a = SystemState.LLC.Lines[index];

    // контекст линии
    LineCallBackIndex = index;
    // Идея в том, что бы вызывать mcFunc("ANYPARAM", 111) и интерпретировать ANYPARAM
    // применительно к линии, внутри событий которой происходит вызов mcFunc.


    if (a->source == OBJ_SOURCE_EMBEDDED) { // вызов обобщенной функции работы с библиотекой интерфейсов ядра

        result = RunMacDriverCallBackEx(typeCallBack, index, arg, ErrorText);
    } else
        if (a->source == OBJ_SOURCE_USER) { // вызов обработчика в формате внешних C-функций
        result = RunMacDriverCallBackEx(typeCallBack, index, arg, ErrorText);

    } else
        if (a->source == OBJ_SOURCE_SCRIPT) { // вызов обработчика в формате внешних Lua-функций
        if (a->callBackLua) {
            result = SystemState.pScriptEngine->Events.OnLine(idEvent, a->name, a->callBackLua, arg, ErrorText);
        } else {
            Log_Ex("dbg", LEVEL_ERR, "Line callBack undef! Line id = %d", a->id);
            result = 0;
        }
    }
    // выход из контекста линии
    LineCallBackIndex = -1;

    return result;
}


//-----------------------------------------------------------------------------
//char * buffInput, int buffInputSize, char * buffOutput, int * buffOutputSize

int RunMacDriverCallBackEx(char typeCallBack, int LineIndex, TMACDriverArg arg, char * ErrorText) {
    TMCExtDriver extDriver;

    int result;

    if ((LineIndex >= SystemState.LLC.count) || (LineIndex < 0)) {
        Log_DBG("ERR!...MacDriver: LLC index! (index = %d)", LineIndex);
        return 0;
    }

    memset(&extDriver, 0, sizeof (extDriver));

    extDriver.LineIndex = LineIndex;
    extDriver.id = typeCallBack;
    extDriver.ErrorText = ErrorText;
    extDriver.param = arg;
    result = RunMacDriverCallBack(LineIndex, &extDriver);
    return result;

}

int RunMacDriverCallBack(int LineIndex, TMCExtDriver * event) {
    int MACIndex;
    // предусмотрел передачу в функцию драйвера информации о линии (LineIndex).
    TMACRec * mac;
    TLineRec * line;
    int result = 0;
    int i;
    char ErrorText[255] = "";
    char dbgText[255] = "";
    char dbgTextEx[255] = "";


    if ((LineIndex >= SystemState.LLC.count) || (LineIndex < 0)) {
        Log_DBG("ERR!... LLC index! (index = %d)", LineIndex);
        return 0;
    }

    MACIndex = SystemState.LLC.Lines[LineIndex]->DriverIndex;
    if ((MACIndex >= SystemState.MAC.count) || (MACIndex < 0)) {
        Log_DBG("ERR!... MAC index! (index = %d)", MACIndex);
        return 0;
    }

    line = SystemState.LLC.Lines[LineIndex];
    mac = SystemState.MAC.Drivers[MACIndex];

    if (event->id == MAC_ID_POLL)
        if (event->param.prPOLL.out_fds != NULL)
            event->param.prPOLL.out_fds[0].fd = -1; // ПОмечаем как неиспользуемый

    // работа с драйверами ядра и драйверами пользователя
    if ((mac->source == OBJ_SOURCE_EMBEDDED) || (mac->source == OBJ_SOURCE_USER)) { // вызов обобщенной функции работы с библиотекой интерфейсов ядра
        if (event->ErrorText != NULL) event->ErrorText[0] = 0;
        else event->ErrorText = &ErrorText[0];
        event->fd = line->fd; // для всех событий передаем идентификатор / главный дескриптор линии
        result = ifuncLinkDrivers(mac, line, event);

        if (event->ErrorText != NULL)
            if (event->ErrorText[0]) {// имеется сообщение об ошибке
                Log_DBG("ERR!... DRIVER MESSAGE: %s", event->ErrorText);
            }

        if (event->id == MAC_ID_CTRL) {
            //Log_DBG("event->id == MAC_ID_CTRL...");
        }

        if (event->id == MAC_ID_OPEN) {
            line->fd = result;
            if (result > 0)
                line->LineControl.LineStatus = CLIENT_STATUS_ON;
            else
                line->LineControl.LineStatus = CLIENT_STATUS_OFF;
        }
        if (event->id == MAC_ID_READ_BUFFER) {
            if (result < MAC_RESULT_OK) {
                line->LineControl.LineStatus = CLIENT_STATUS_OFF;
            }
        }

        if (event->id == MAC_ID_WRITE_BUFFER) {
            if (result < MAC_RESULT_OK) {
                line->LineControl.LineStatus = CLIENT_STATUS_OFF;
                Log_DBG("ERR!... MAC_ID_WRITE_BUFFER. result (%d)!= event->prTX.txCount (%d)", result, event->param.prIO.inCount);
            }
        }

    } else
        if (mac->source == OBJ_SOURCE_SCRIPT) { // создание драйверов в lua под вопросом...
        Log_DBG("ERR!... MacDriverCallBack. Source = OBJ_SOURCE_SCRIPT!!!");

    }

    if (SystemState.fDebug) {
        if (event->id == MAC_ID_OPEN) {
            Log_Ex("dbg", LEVEL_DEBUG, "%s(MAC_ID_OPEN): [device=%s, param=%s] : %d", mac->name, event->param.prOPEN.device, event->param.prOPEN.param, result);
        }
        if (event->id == MAC_ID_POLL) {

            if ((event->param.prPOLL.in_fds_count)&&(result)) { // что бы не засорять логи, будем выводить только если были события (result > 0))
                for (i = 0; i < event->param.prPOLL.in_fds_count; i++)
                    snprintf(dbgText, sizeof (dbgText), "%s %d", dbgText, event->param.prPOLL.in_fds[i].fd);

                for (i = 0; i < result; i++) {
                    if (event->param.prPOLL.out_fds[0].fd == -1) // используем in_fds
                        snprintf(dbgTextEx, sizeof (dbgTextEx), "%s %d(revent=%d)", dbgTextEx, event->param.prPOLL.in_fds[i].fd, event->param.prPOLL.in_fds[i].revents);
                    else
                        snprintf(dbgTextEx, sizeof (dbgTextEx), "%s %d(revent=%d)", dbgTextEx, event->param.prPOLL.out_fds[i].fd, event->param.prPOLL.out_fds[i].revents);
                }

                Log_Ex("dbg", LEVEL_DEBUG, "%s(MAC_ID_POLL): [fd=%d, fds:[%s ]] : %d -> [fds:[%s ]]", mac->name, event->fd, dbgText, result, dbgTextEx);
            }
        }
        if (event->id == MAC_ID_READ_BUFFER) {

            i = *event->param.prRX.rxCount;
            Log_buf2strEx(dbgText, sizeof (dbgText), event->param.prRX.rxBuff, i);
            Log_Ex("dbg", LEVEL_DEBUG, "%s(MAC_ID_READ_BUFFER): [fd=%d, fdpoll=%d, timeOut=%d] : %d -> [%d:[%s ]]", mac->name, event->fd, event->param.prRX.fdpoll, event->param.prRX.timeOut, result, i, dbgText);
        }
        if (event->id == MAC_ID_WRITE_BUFFER) {
            i = event->param.prIO.inCount;
            Log_buf2strEx(dbgText, sizeof (dbgText), event->param.prTX.inBuff, i);
            Log_Ex("dbg", LEVEL_DEBUG, "%s(MAC_ID_WRITE_BUFFER): [fd=%d, txCount=%d, txBuff: [%s]] : %d", mac->name, event->fd, i, dbgText, result);
        }
        if (event->id == MAC_ID_CTRL) {
            Log_Ex("dbg", LEVEL_DEBUG, "%s(MAC_ID_CTRL): [] : %d", mac->name, result);
        }

    }


    return result;
}


// Считаем что память для TSignalsRec * a уже выделена. Используем только указатель а

void mcAppendUserLineDriver(char * DriverName, char * DriverParams, TUserLineCallBack * proc, int EventMask) {
    AppendLinkDriver(&SystemState, OBJ_SOURCE_USER, DriverName, DriverParams, (void *) proc, EventMask);
}

void mcAppendUserProtocol(char * ProtocolName, char * ProtocolParams, TPMNGCallBack * proc) {
    TProtocolRec * a;

    // обязательно! В ProtocolParams должно быть определено "FRAMING" ("FRAMING=BSAP")
    a = BuildProtocolRec(&SystemState, ProtocolName, ProtocolParams, 0);
    a->callBack = proc;
    AppendProtocolUnit(&SystemState, a, OBJ_SOURCE_USER);

}

void mcAppendUserAlgorithm(char * algName, TExtAlgorithmInit fInit) {

    return;
}

int InitSignalValueBase(TSystemState * State, int emulMode, int signalCountLimit) {
    State->Data.Values.Count = 0;
    return 0;
}
