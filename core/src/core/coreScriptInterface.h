#ifndef CORE_SCRIPT_INT_H
#define CORE_SCRIPT_INT_H

#include "./coreUInterface.h"

typedef int ( TScriptOnSignal(int LuaFuncId, char typeCallBack, void * ext, int index, TMCSignalsInfo * sgInfo, unsigned short int addr, Tsub_param_rec * value, char * flag, char ExtMode, int srcInterface));
typedef int ( TScriptOnFraming(char * idEvent, int func, char * buffInput, int * buffInputSize, char * buffOutput, int * buffOutputSize, char * ErrorText));
typedef int ( TScriptOnLine(char * idEvent, char * Name, int func, TMACDriverArg arg, char * ErrorText));
typedef int ( TScriptOnPolling(char * idEvent, char * Name, int func));
typedef int ( TScriptOnProtocol(char * idEvent, int LineIndex, int func, void * UI, char * signalName, Tsub_param_rec * value, char * pktInBuffer, int pktInSize, char * pktOutBuffer, int * pktOutSize, char * ErrorText));
typedef Tsub_param_rec * ( TScriptOnCalc(char * idEvent, int index, int func, Tsub_param_rec * value, char * flag));


//структура функций обратного вызова

typedef struct {
    TScriptOnSignal  *OnSignal;
    TScriptOnFraming * OnFraming;
    TScriptOnLine  *OnLine;
    TScriptOnPolling  *OnPolling;
    TScriptOnProtocol * OnProtocol;
    TScriptOnCalc  *OnCalc;
} TScriptEvents;

typedef struct {
    void * scrObject; //указатель на объект скрипта.
    TScriptEvents Events; // интерфейс с ядром
    int syncEvents; // объект синхронизации событий (функ. обратного вызова)
    int syncFunc; // объект синхронизации Lua2C функций
    void * ScriptExt; // данные самого модуля скрипта (сервер их не обрабатывает)
} TScriptInstance;



#endif
