#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../main.h"
#include <time.h>
#include <sys/time.h>
#include "other.h"
//#include "../core/signals/defsignals.h"



//-----------------------------------------------------------------------------
//найти имя параметра в строке
//seek_substr  : ("RRT=1", "T") -> T=1
//seek_substrEx  : ("RRT=1", "T") -> NULL

static char paramBuff[255]; // временный буфер для хранения значения параметра.

//найти параметр в строке
static char* seek_substr(char s[], char substr[]){
    int i;
    int len=strlen(substr);
    for(i=0; s[i]!='\0'; i++){
        if(strncmp(&s[i], substr, len)==0) {
            return &s[i];
        }    
    }
    return NULL;
}

static char* seek_substrEx(char s[], char substr[]){
    int i;
    char prevChar = 0;
    int len=strlen(substr);
    for(i=0; s[i]!='\0'; i++){
        if ((prevChar == 0)||(prevChar == ' ')||(prevChar == ';'))
                {
                if(strncmp(&s[i], substr, len)==0) 
                    {
                    if ((s[i + len] == ' ')||(s[i + len] == '='))
                        return &s[i];
                    }
                }
        prevChar = s[i];
    }
    return NULL;
}


char* get_ParamValue(char * ParamsLine, char * ParamName)
{
    int i;
    int j;
    int size = 0;
    char * strParams;
    // Пример: "FRAMING=BSAP.FRAME POLLING=INTERNAL"
    // Пример: "FRAMING=BSAP.FRAME;POLLING=INTERNAL"
    // ParamName = "FRAMING"
    strParams = seek_substrEx(ParamsLine, ParamName );
    if (strParams != NULL)
        {
        // копируем строку в промежуточный буфер
        snprintf(paramBuff, sizeof(paramBuff), "%s", strParams);
        //printf("DBG: get_ParamValue(%s, %s)> strParams = %s\n", ParamsLine, ParamName, paramBuff);
        for (i = 0; i < strlen(paramBuff); i++ )
            if ((paramBuff[i] == ' ')||(paramBuff[i] == ';')) paramBuff[i] = 0;
        // strParams = "FRAMING=BSAP.FRAME"
        strParams = seek_substr(paramBuff, "=" );
        if (strParams != NULL)
            return &strParams[1]; //"BSAP.FRAME"
        }
    return NULL;
}



int memUsedByte = 0;

void convertStr(unsigned char * buf)
{
    int i = 0;
    
    while(1)
    {
     if (buf[i] == 0) break;
     if (buf[i] == 0xC0) buf[i] = 'A';
     if (buf[i] == 0xC1) buf[i] = 'B';
     if (buf[i] == 0xC2) buf[i] = 'V';
     if (buf[i] == 0xC3) buf[i] = 'G';
     if (buf[i] == 0xC4) buf[i] = 'D';
     if (buf[i] == 0xC5) buf[i] = 'E';
     if (buf[i] == 0xC6) buf[i] = 'J';
     if (buf[i] == 0xC7) buf[i] = 'Z';
     if (buf[i] == 0xC8) buf[i] = 'I';
     if (buf[i] == 0xC9) buf[i] = 'I';
     if (buf[i] == 0xCA) buf[i] = 'K';
     if (buf[i] == 0xCB) buf[i] = 'L';
     if (buf[i] == 0xCC) buf[i] = 'M';
     if (buf[i] == 0xCD) buf[i] = 'N';
     if (buf[i] == 0xCE) buf[i] = 'O';
     if (buf[i] == 0xCF) buf[i] = 'P';
     if (buf[i] == 0xD0) buf[i] = 'R';
     if (buf[i] == 0xD1) buf[i] = 'S';
     if (buf[i] == 0xD2) buf[i] = 'T';
     if (buf[i] == 0xD3) buf[i] = 'U';
     if (buf[i] == 0xD4) buf[i] = 'F';
     if (buf[i] == 0xD5) buf[i] = 'X';
     if (buf[i] == 0xD6) buf[i] = 'C';
     if (buf[i] == 0xD7) buf[i] = 'H';
     if (buf[i] == 0xD8) buf[i] = 's';
     if (buf[i] == 0xD9) buf[i] = 's';
     if (buf[i] == 0xDA) buf[i] = '-';
     if (buf[i] == 0xDB) buf[i] = '-';
     if (buf[i] == 0xDC) buf[i] = 'l';
     if (buf[i] == 0xDD) buf[i] = 'e';
     if (buf[i] == 0xDE) buf[i] = 'u';
     if (buf[i] == 0xDF) buf[i] = 'i';
     if ((buf[i] >= 0xE0)&&(buf[i] <= 0xFF)) buf[i] = '*';
     i++;
    }
}


void bufToHexStr(char * buf, int bufSize, char *hexStr)
{
 int    i;
 
 hexStr[0] = 0;
 for (i = 0; i < bufSize; i++)
    {
    sprintf(&hexStr[strlen(hexStr)], " %02X", (unsigned char)buf[i]); 
//    sprintf(hexStr, "%s %02X", hexStr, (unsigned char)buf[i]); 
    }
}


unsigned long long getLotosTimeNow(void) //Аналог функции GetSystemTimeAsFileTime()
{
 //01.01.1601    
 #define MAGIC_DIFF_TIME   11644473600LL    

    struct timeval tv;
    unsigned long long  result = MAGIC_DIFF_TIME;

    gettimeofday(&tv, NULL); 
    result += tv.tv_sec;
    result *= 10000000LL;
    result += tv.tv_usec * 10;
    return result;
    
}


double getTimeNow(void) //Аналог функции NOW()
{
    
 #define MAGIC_DELATA_TIME   25569    /* разница в днях. равна now() - time(0) */

    struct timeval tt;
    struct timezone tz;
    
    gettimeofday(&tt, &tz); // найдем смещение часового пояса
    
//    printf("getTimeNow = %f \n", MAGIC_DELATA_TIME  + (float)(time(0) - tz.tz_minuteswest*60 )/ (60*60*24));

    return MAGIC_DELATA_TIME  + (double)(time(0) - tz.tz_minuteswest*60 )/ (60.0*60.0*24.0);
    
}

unsigned long long mcGetTime(struct timespec *tp)
{
    struct timespec t;
    struct timespec * pt;
    
    if (tp)
        pt = tp;
    else
        pt = &t;
    clock_gettime(CLOCK_REALTIME, pt);

    return (((unsigned long long)pt->tv_nsec/1000000)&0xFFFFFFFF) + ((unsigned long long)pt->tv_sec & 0x000FFFFF) *1000;
}



int mcGetDeltaTime(void * t1, void * t2)
{
    
    struct timespec   time0;
    struct timespec * time1;
    struct timespec * time2;
    
    long   result;
    long   sec;
    
    time1 = (struct timespec *) t1;
    time2 = (struct timespec *) t2;
    
    if (t2 == 0)
    {
     mcGetTime(&time0);     
     time2 = &time0;
    }

    // Если time1 не инициализированна возврат 0!
    if ((time1->tv_sec + time1->tv_nsec) == 0) return 0;
    
    sec = time2->tv_sec - time1->tv_sec;
    result = time2->tv_nsec - time1->tv_nsec;
    
    result = result / 1000000;  // преобразуем ns в ms
    result+=sec * 1000;
    
    return result;
    
}

void mcGetTimeStr(char * str)
{
    struct timespec t;
    long   ms;
    
    ms = mcGetTime(&t);
    
    strftime(str, 100, "%d.%m %H:%M:%S", localtime (&t.tv_sec));
    sprintf(&str[strlen(str)], ".%.3d", (int)(ms % 1000));
    return ;
    
}
void getDateStr(char * str)
{
    struct timespec t;
    long   ms;
    
    ms = mcGetTime(&t);
    
    strftime(str, 100, "%d.%m.%Y", localtime (&t.tv_sec));
    return ;
    
}

void * mallocEx(int size)
{
    void * result;
    memUsedByte += size;
    result = malloc(size);
    if (result == NULL)
    {
    printf("\n MEMORY OVERFLOW\n");
    exit(1);
    }
    return result;
}

void freeEx(void * p, int size)
{
    memUsedByte -= size;
    free(p);
}

unsigned char getBit(unsigned char * buff, unsigned char n)
{
    return (buff[(n >> 3)] & (1 << (n & 7))) > 0;
}

void setBit(unsigned char * buff, unsigned char n, unsigned char bitvalue)
{
    buff[(n >> 3)] = (buff[(n >> 3)] & (0xff ^ (1 << (n & 7)))) | (bitvalue << (n & 7));
    return ;
}

void printBitBuf(unsigned char * buff, unsigned char size)
{
int i;

    printf("\n ");
for (i = 0; i < size; i++)
    printf("%x ", buff[i]);
return ;
}

void getParam_RecStr(Tsub_param_rec * value, char * str)
{
#define STR_VALUE_LINE_SIZE 100
#define STR_VALUE_EXT_SIZE 20
    // LONG TEXT ... [NUM]
    int     i;
    int     currLineSize;
    char    s[STR_VALUE_LINE_SIZE + STR_VALUE_EXT_SIZE] = ""; // временный буфер для значения
    
    if (value->tp == TYPE_ID_BYTE)       sprintf(s, "%d", value->bvalue);
    if (value->tp == TYPE_ID_BYTE_SIGN)  sprintf(s, "%d", value->bvalue);
    if (value->tp == TYPE_ID_WORD)       sprintf(s, "0x%04X", value->wvalue);
    if (value->tp == TYPE_ID_WORD_SIGN)  sprintf(s, "%d", value->wvalue);
    if (value->tp == TYPE_ID_DWORD)      sprintf(s, "0x%08X", value->dwvalue);
    if (value->tp == TYPE_ID_DWORD_SIGN) sprintf(s, "%d", value->dwvalue);
    if (value->tp == TYPE_ID_FLOAT)      sprintf(s, "%f", value->fvalue);
    if (value->tp == TYPE_ID_DOUBLE)     sprintf(s, "%f", value->dvalue);
    if (value->tp == TYPE_ID_STRING0)    snprintf(s, sizeof(s), "%s", value->stvalue);
//    if (value->tp == TYPE_ID_STRING)     sprintf(s, "%s", value->stvalue);
    if (value->tp == TYPE_ID_BOOL32)     sprintf(s, "%d", value->bvalue);
    if (value->tp == TYPE_ID_STRUCT)    
        {
        for (i = 0; i < value->size; i++)
            {
            // формат: %.2X = "00", формат %2X = "0"
            currLineSize = strlen(s);
            if ((currLineSize + 10) < STR_VALUE_LINE_SIZE)
             sprintf(&s[currLineSize], "%02X", (unsigned char)(value->stvalue[i] & 0xFF));
            else 
                {
                    snprintf(&s[currLineSize], STR_VALUE_EXT_SIZE, "[%d]", value->size - i);
                    break;
                }
            }
        }
    strcpy(str, s);
    return ;
    
}
