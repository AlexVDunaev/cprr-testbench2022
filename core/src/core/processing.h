/* 
 * File:   processing.h
 * Author: Dunaev
 *
 * Created on 14 Август 2014 г., 16:40
 */


#ifndef PROCESSING_H
#define	PROCESSING_H


#include "../main.h"

int SR_CMD_Processing (TSystemState * State, int interfaceIndex);
int KPA_CMD_Processing(TSystemState * State, int interfaceIndex);
int SIGNAL_REQ_Processing(TSystemState * State, int interfaceID);

#endif	/* PROCESSING_H */

