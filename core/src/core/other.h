/* 
 * File:   other.h
 * Author: Dunaev
 *
 * Created on 12 Сентябрь 2014 г., 8:51
 */


#include <time.h>
#include "../main.h"


#ifndef OTHER_H
#define	OTHER_H
double getTimeNow(void); //Аналог функции NOW()
unsigned long long getLotosTimeNow(void); ////Аналог функции GetSystemTimeAsFileTime
unsigned long long mcGetTime(struct timespec *tp);
int    mcGetDeltaTime(void * t1, void * t2);
void   mcGetTimeStr(char * str);
void   getDateStr(char * str);
//------------------------------------------------------
void * mallocEx(int size);  // учет используемой памяти
void freeEx(void * p, int size);

//------------------------------------------------------
void   getParam_RecStr(Tsub_param_rec * value, char * str);

//------------------------------------------------------
void bufToHexStr(char * buf, int bufSize, char *hexStr);
void convertStr (unsigned char * buf);

unsigned char getBit(unsigned char * buff, unsigned char n);
void          setBit(unsigned char * buff, unsigned char n, unsigned char bitvalue);

char* get_ParamValue(char * ParamsLine, char * ParamName);

extern int memUsedByte;


#endif	/* OTHER_H */

