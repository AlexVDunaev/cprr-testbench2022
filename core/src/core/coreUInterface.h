/* 
 * File:   coreUInterface.h
 * Author: Dunaev
 *
 * Created on 18 Апрель 2016 г., 11:20
 */

#ifndef COREUINTERFACE_H
#define	COREUINTERFACE_H



#ifdef MC_USE_LINUX
// Сборка под Linux
#define	MC_EXT_API   

#else
// Сборка под Windows (Cygwin)
#define	MC_EXT_API   __stdcall

#endif

#include <sys/poll.h>


#define	SIGNAL_INDEX_INVALID         -1


#define	MC_MODE_NO_THREAD       (0 << 0) // пользователь должен вызавать функцию mcProcessing()
#define	MC_MODE_MULTITHREAD     (1 << 0) // сервер запускается в отдельном потоке

#define	PROT_FRAMING_PACK   1
#define	PROT_FRAMING_UNPACK 2
#define	PROT_FRAMING_INIT   3

#define	PROT_MNG_REQ          1
#define	PROT_MNG_KU_SIGNAL    2
#define	PROT_MNG_REQ_SIGNAL   3
#define	PROT_MNG_RX           4

//-------------- L3. Типы сетевых сообщений (протокольный ур.)----
//#define CALLBACK_TYPE_PROT_DEFAULT  12  // обработка структуры запроса и формирование посылки для L2
#define PROT_MNG_DIR_OUT  12  // обработка структуры запроса и формирование посылки для L2
#define PROT_MNG_DIR_IN   13  // обработка посылки для L2 ответа и формирование структуры запроса-ответа
#define PROT_MNG_ERR_INFO 14  // Расшифровка поля ошибок
#define PROT_MNG_ERR_ADD  16  // инкрементирование ошибок для сигнала


#define	PROT_MNG_RESULT_ERR   0
#define	PROT_MNG_RESULT_OK    1

//--------------------------------------
#define	MAC_ID_WRITE_BUFFER 1
#define	MAC_ID_READ_BUFFER  2
#define	MAC_ID_OPEN         4
#define	MAC_ID_CLOSE        8
#define	MAC_ID_CTRL         16
#define	MAC_ID_POLL         32
#define	MAC_ID_ERROR        64
#define	MAC_ID_TEST         128 // само-тестирование модуля
// коды возврата для событий чтения/записи
#define	MAC_RESULT_OK       ((int)0)   //чтение/запись удачно завершились, данные считаны/отправлены
#define	MAC_RESULT_DEFERRED ((int)1)   //чтение/запись будут происходить в фоне
#define	MAC_RESULT_ERROR    ((int)-1)  //Коды ошибок имеют отрицательное значение.
#define	MAC_RESULT_IGNORE   ((int)2)   //буфер игнорируется
#define	MAC_RESULT_BREAK    ((int)3)   //прервать действие
#define	MAC_RESULT_RETRY    ((int)4)   //попытаться повторить действие
// разработчик драйвера может разделять виды ошибок, задав им разные отриц. величины
// 


#define	MAC_ID_MASK_ALL     0xFFFF

#define STR_MAX_SIZE    4095
#define STRING_FIELD_SIZE    127



#define GLOBAL_BUFFERED_MODE_DEFAULT    0
#define GLOBAL_BUFFERED_MODE_ON         1
#define GLOBAL_BUFFERED_MODE_OFF        2




#define CALLBACK_TYPE_DIR_GET       0x00   // направление
#define CALLBACK_TYPE_DIR_SET       0x80   // 
#define CALLBACK_TYPE_DIR_MASK      0x80   // признак направления запрос/команда
#define CALLBACK_TYPE_GET_SIGNAL    1   // команда вернуть значение сигнала
#define CALLBACK_TYPE_SET_SIGNAL    2   // команда установить сигнал
#define CALLBACK_TYPE_NEW_VALUE     3   // обновление значение сигнала по внутреннему интерфейсу
#define CALLBACK_TYPE_ACK_SIGNAL    4   // 
#define CALLBACK_TYPE_WRITE_BUFFER  5   // требование произвести запись буфера
#define CALLBACK_TYPE_NAK_SIGNAL    6   // запрос завершился с ошибкой
#define CALLBACK_TYPE_EVENT_REQ     7   // событие формирования запроса для данного сигнала.
#define CALLBACK_TYPE_EVENT_RX      21  // Получение данных, сразу при формировании запроса
#define CALLBACK_TYPE_EVENT_ERR     22  // При выполнении команды произошла ошибка
#define CALLBACK_TYPE_EVENT_ABORT   23  // Уведомление о прекращении команды
//-------------- L2. Типы канальных сообщений -------------------
#define CALLBACK_TYPE_LLC_WRITE     9   // передача буфера драйверу линии связи
#define CALLBACK_TYPE_LLC_READ      10  // прием буфера от драйвера линии связи
#define CALLBACK_TYPE_LLC_CTRL      11  // управление драйвером линии связи
#define CALLBACK_TYPE_LLC_REINIT    15  // reConnect
//-------------- L3. Типы сетевых сообщений (протокольный ур.)----
//#define CALLBACK_TYPE_PROT_DEFAULT  12  // обработка структуры запроса и формирование посылки для L2
#define CALLBACK_TYPE_PROT_DIR_OUT  12  // обработка структуры запроса и формирование посылки для L2
#define CALLBACK_TYPE_PROT_DIR_IN   13  // обработка посылки для L2 ответа и формирование структуры запроса-ответа
#define CALLBACK_TYPE_PROT_ERR_INFO 14  // Расшифровка поля ошибок
#define CALLBACK_TYPE_PROT_ERR_ADD  16  // инкрементирование ошибок для сигнала
//-------------- L7. Типы сетевых сообщений (протокольный ур.)----
//#define CALLBACK_TYPE_DEFAULT_REQ   8   // формирования запроса по умолчанию для данного сигнала.
//----------------------------------------------------------------

#define REQ_TYPE_INT                1   // тип отложенного запроса: к железу
#define REQ_TYPE_EXT                0   // тип отложенного запроса: от клиента 


#define CALLBACK_RESULT_NEED_REPEAT 255   //при возврате из функции пользователя нужно повторить ее вызов

//#define CALLBACK_TYPE_USES     0x80..0xFF   // пользовательские события (определенные протоколом)

#define CMD_ACK_TYPE_OK             1   // подтверждение запроса. флаг 1
#define CMD_ACK_TYPE_NAK            0   // ответ на запрос. флаг 0

#define CALLBACK_RESULT_OK       1
#define CALLBACK_RESULT_ERROR    0
#define CALLBACK_RESULT_OVERLAPPED    2
#define CALLBACK_ERROR_IGNORE    0
#define CALLBACK_ERROR_RETRY     1
#define CALLBACK_ERROR_ABORT     2

//--------- типы данных протокола ANSI - Lotos --------
// этиже типы используются для передачи данных внутри сервера
#define      TYPE_ID_UNDEF       0
#define      TYPE_ID_BYTE	 1
#define      TYPE_ID_BYTE_SIGN	 2
#define      TYPE_ID_WORD	 3
#define      TYPE_ID_WORD_SIGN	 4
#define      TYPE_ID_DWORD	 5
#define      TYPE_ID_DWORD_SIGN  6
#define      TYPE_ID_FLOAT	 7
#define      TYPE_ID_DOUBLE	 8
#define      TYPE_ID_STRING0	 9
#define      TYPE_ID_TIME        10
#define      TYPE_ID_STRUCT	 11
#define      TYPE_ID_STRING	 12 //Тип неподдерживается Лотос (только для вн.нужд)!
#define      TYPE_ID_BOOL32	 13
#define      TYPE_ID_EVENT       100
//-----------------------------------------------------

#define CMD_TYPE_READ             0  // тип запроса: чтение
#define CMD_TYPE_WRITE            1  // тип запроса: запись


#define      FIELD_ID_ENABLED   0x0001   // идентификатор свойства "ENABLED"
#define      FIELD_ID_ERROR     0x0002   // идентификатор свойства "ERROR"
#define      FIELD_ID_ERROR_TO  0x0003   // идентификатор свойства "ERROR_TIMEOUT"
#define      FIELD_ID_ERROR_CRC 0x0004   // идентификатор свойства "ERROR_CRC"
#define      FIELD_ID_ERROR_LLC 0x0005   // идентификатор свойства "ERROR_LLC"
#define      FIELD_ID_DELAY     0x0006   // идентификатор свойства "DELAY"
#define      FIELD_ID_ADDR      0x0007   // Изменение поля ADDR сигнала
#define      FIELD_ID_HW_ADDR   0x0008   // Изменение поля HW_ADDR сигнала
//-----------------------------------------------------
#pragma pack(push, 1) 
typedef struct {
    unsigned short int size;
    unsigned char tp;

    union {
        unsigned char bvalue;
        signed char sbvalue;
        unsigned short int wvalue;
        unsigned short int warray[STR_MAX_SIZE / 2];
        signed short swvalue;
        unsigned int dwvalue;
        signed int ivalue;
        float fvalue;
        double dvalue;
        char stvalue[STR_MAX_SIZE];
        double tvalue;
        char scvalue[STR_MAX_SIZE];
        char strvalue[STR_MAX_SIZE];
        char boolvalue;
    };
} Tsub_param_rec;


typedef struct //командный пакет. поступающий на вход протокольной функции
{
    //unsigned char       count;  // структура расчитана на запрос только 1 сигнала
    unsigned int ID; // Индекс посылки . копируется из CMDID
    unsigned int index; // индекс сигнала для непосредственного обращения к полям 
    unsigned int TYPE; // тип обращения: чтение / запись
    unsigned int blockAddess; // различная адресная информация... 
    unsigned int subBlockAddess; //
    unsigned int signalAddess; //
    unsigned int subSignalAddess; //
    unsigned int error; // тип ошибки
    unsigned char isError; // признак ошибки
    Tsub_param_rec value;
    // поля для размещения протокольных запросов пользователя
    unsigned int userSize; // размер пользовательского блока

    union {
        unsigned char b[100];
        unsigned short int w[100];
        unsigned int d[100];
        float f[100];
        char st[STR_MAX_SIZE];
    } user;
} TCMD;

typedef struct {
    char * name; // обозначение сигнала в системе
    unsigned char TYPE; // тип сигнала 
    unsigned int readingdelay; // задержка при опросе. для сигналов (для записи = -1)
    unsigned char interface; // идентификатор интерфейса, который обслуживает данный сигнал
    short int block; // [адрес] блок, в котором находится сигнал
    int addr; // логический адрес сигнала
    int master_addr; // логический адрес сигнала - источника данных.
    unsigned int hw_addr; // адрес сигнала в блоке
    char * ID; // Обозначение сигнала "ТП[1]"
    // что-то еще...
    char * reqName; // обозначение сигнала при запросе
    int reqStep; // для сигналов, которые выдаются поэтапно, счетчик шагов. (используется совместно CALLBACK_RESULT_NEED_REPEAT)
    TCMD * cmd; // указатель на структуру командного буфера
} TMCSignalsInfo;

typedef struct {
    int (*Byte2Rec) (Tsub_param_rec * rec, unsigned char value);
    int (*Word2Rec) (Tsub_param_rec * rec, int value);
    int (*DWord2Rec) (Tsub_param_rec * rec, unsigned int value);
    int (*Float2Rec) (Tsub_param_rec * rec, float value);
    int (*Double2Rec) (Tsub_param_rec * rec, double value);
    int (*Struct2Rec)(Tsub_param_rec * rec, char * value, int size);
    int (*Str2Rec) (Tsub_param_rec * rec, char * valuestr);
    int (*Rec2Rec) (Tsub_param_rec * rec, Tsub_param_rec * value);
} TMC_Set_Func;

typedef struct {
    int (*Rec2Int) (Tsub_param_rec * rec);
    double (*Rec2Double)(Tsub_param_rec * rec);
} TMC_Get_Func;

typedef struct {
    char * name;
    unsigned char TYPE; // тип сигнала 
    unsigned int readingdelay; // задержка при опросе. для сигналов (для записи = -1)
    unsigned char interface; // идентификатор интерфейса, который обслуживает данный сигнал
    short int block; // [адрес] блок, в котором находится сигнал
    int addr; // логический адрес сигнала
    int master_addr; //логический адрес сигнала - источника данных.
    unsigned int hw_addr; // адрес сигнала в блоке
    char * ID; //Обозначение сигнала "ТП[1]"
} TSignalProperty;

typedef struct {
    int (*SetValue)(int index, Tsub_param_rec * value, char flag);
    Tsub_param_rec * (* GetValue)(int index);
    int (*FindSignal)(char * nameSignal);
    int (*SetValueByte)(int index, unsigned char value, char flag);
    int (*GetSignalsCount)(void);
    TSignalProperty * (* GetSignalProperty)(int index);
    int (*addRequest)(int index, Tsub_param_rec * value);
    char * (* GetSignalParam)(int index, char * keyName);
    int  (* SetSignalParam)(int index, char * keyName, char * keyValue );
} TMC_SG_Func;



typedef struct {
    int (*sendToLine)(int id, char * buffer, int size);
} TMC_LAYER_Func;

typedef int (MC_EXT_API TNotifyToLine(char typeCallBack, char * buffer, int size));
typedef int (MC_EXT_API TNotifyToFrame(char typeCallBack, char * buffer, int size));
typedef int (MC_EXT_API TNotifyToProtocol(char typeCallBack, char * buffer, int size));
typedef int (MC_EXT_API TNotifyToSignal(char typeCallBack, char * signalName, Tsub_param_rec * value, char flag));

typedef struct {
    //int (*addNotifyToLine)(int idLine, TNotifyToLine * callBack);        // только события RX/TX
    //int (*addNotifyToFrame)(int idLine, TNotifyToFrame * callBack);      // только события PACK/UNPACK
    //int (*addNotifyToProtocol)(int idLine, TNotifyToProtocol * callBack);// только события RX/REQ
    int (*addNotifyToSignal)(char * mask, TNotifyToSignal * callBack, int idOfSubscriber);    // Пример: mask = "block.*"
} TMC_NOTIFY_Func;

// интерфейс пользователя с ядром

typedef struct {
    char * serverVersion; // версия сервера
//    TMC_Set_Func * set;   // пользователь может переопределять служебные функции 
//    TMC_Get_Func * get;
//    TMC_SG_Func * signal; // доступ к сигналам можно перехватывать, заменив функцию из структуры signal на свою
//    TMC_NOTIFY_Func * notify;
    // что-то еще...
} TMCUI;



typedef int ( TMCUserCallBack(char typeCallBack, TMCUI * ui, int index, TMCSignalsInfo * sgInfo, Tsub_param_rec * value, char * flag, char ExtMode, int srcInterface));


typedef struct {
    char name[STRING_FIELD_SIZE]; //обозначение сигнала в системе ("КАМАК.РД1.Д1")
    char param[STRING_FIELD_SIZE]; //Обозначение сигнала "ТП[1]"
    TMCUserCallBack * callBack;

} TMCSignalsRec;

typedef struct {
    char name[STRING_FIELD_SIZE]; //обозначение сигнала в системе ("КАМАК.РД1.Д1")
    unsigned char TYPE; // тип сигнала 
    unsigned int readingdelay; // задержка при опросе. для сигналов (для записи = -1)
    void * data; // указатель на данные сигнала
    unsigned char interface; // идентификатор интерфейса, который обслуживает данный сигнал
    short int block; // [адрес] блок, в котором находится сигнал
    int addr; // логический адрес сигнала
    int master_addr; //логический адрес сигнала - источника данных.
    unsigned int hw_addr; // адрес сигнала в блоке
    //unsigned short int  hw_sb_addr;   // суб.адрес сигнала в блоке (доп. адресация)
    char ID[STRING_FIELD_SIZE]; //Обозначение сигнала "ТП[1]"
    TMCUserCallBack * callBack;

} TMCSignalsRecEx;




// Возврат: 0 Ошибка. 
typedef int ( TCoreCallBack(char typeCallBack, void * ext, int index, unsigned short int addr, Tsub_param_rec * value, char * flag, char ExtMode, int srcInterface));


typedef int ( TFramingCallBack(char typeCallBack, char * buffInput, int *buffInputSize, char * frameOutput, int * frameOutputSize, char * ErrorText));


typedef struct {
    struct pollfd * in_fds;
    int in_fds_count;
    struct pollfd * out_fds;
    int reserv2;
} TMCExtDriver_POLL;

typedef struct {
    char * device;
    int reserv1;
    char * param;
    int reserv2;
} TMCExtDriver_OPEN;

typedef struct {
    // указатель на начало буфера
    char * inBuff;
    // размер данных, которые непосредственно помещаются пользователю
    int inCount;
    // указатель на буфер, который нужно передать как Lightuserdata. Если 
    // Lightuserdata == NULL передается inBuff
    void * pLightuserdata;
    // полный размер данных, который доступен через указатель
    int inCountEx; 
    // указатель на буфер куда помещается результат
    char * outBuff;
    // размер буфера, что записал пользователь
    int * outBuffCount;
    // размер буфера, доступный для записи
    int outBuffLimit;
} TMCExtDriver_TX;

typedef struct {
    int fdpoll; // fd по которому произошло событие чтение 
    int timeOut;
    char * rxBuff;
    int * rxCount;
} TMCExtDriver_RX;

typedef struct {
    char * ctrlCommand;
    int ctrlValue; // пока не используется!
    char * device;
    char * param;
//    char * ctrlAnswer; // пока не используется!
//    int reserv2;
} TMCExtDriver_CTRL;

typedef union {
    void * ptr;
    int    i;
} TMCExtDriver_uniType;


typedef struct {
    TMCExtDriver_uniType params[4];
} TMCExtDriver_RAW;


typedef union {
        TMCExtDriver_POLL prPOLL;
        TMCExtDriver_OPEN prOPEN;
        TMCExtDriver_CTRL prCTRL;
        TMCExtDriver_TX   prTX;        
        TMCExtDriver_RX   prRX;        
        TMCExtDriver_TX   prIO;        
        TMCExtDriver_RAW  prRAW;        
    } TMCExtDriverParam;


typedef struct {
    int    LineIndex;
    char   id; //Event id
    int    fd; //дискриптор открытого канала (или идентификатор)
    TMCExtDriverParam  param;
    char * ErrorText; // сообщение об ошибке, если есть
} TMCExtDriver;

typedef TMCExtDriverParam TMACDriverArg;

//typedef int ( TUserLineCallBack(char typeCallBack, unsigned char * TxBuffer, int TxCount, unsigned char * RxBuffer, int *RxCount));
typedef int ( TUserLineCallBack(TMCExtDriver *event));


#define USE_DEFAULT_BUFFER             0  // передается 0 указатель на буфер -> используется буфер по умолчанию 
// Функция унификации протокольного уровня
// Позволяет формировать пакеты протокольного уровня по типу запроса и перечню сигналов (их индексов)
typedef int ( TProtocolCallBack(char typeCallBack, void * ext, int clientIndex, TCMD * C, unsigned char * outBuffer));

typedef int ( TPMNGCallBack(char typeCallBack, TMCUI * ui, char * signalName, Tsub_param_rec * value, char * pktInBuffer, int pktInSize, char * pktOutBuffer, int * pktOutSize, char * ErrorText));

// Позволяет отрпавлять буфер в линию связи
// clientIndex : индекс клиента в структуре TSystemState, определяющий буфер и линию.
// возврат: кол-во переданных байт. Если RxCount <> 0 Кол-во считанных данных (в ответ). 
typedef int ( TLineCallBack(char typeCallBack, void * ext, int clientIndex, unsigned char * TxBuffer, int TxCount, unsigned char * RxBuffer, int *RxCount));

#define      RD_OFF        0x80000000
#define      RD_DISABLED   0x40000000
#define      RD_NODELAY    0
#define      RD_DELAY_IS   0
#define      RD_TIME_MASK  0xFFFFF


//-----------------
typedef void (TExtAlgorithmInit(void));
typedef void (TExtAlgorithmConfig(void));
typedef void (TExtAlgorithmProcessFrame(void));

// Структура определяющая алгоритм обработки РЛ данных

typedef struct {
    char   id[25]; //id ("CPRR24")
    TExtAlgorithmInit * fInit;
    TExtAlgorithmConfig* fConfig;
    TExtAlgorithmProcessFrame* fFrame;
} TMCExtAlgorithm;

// ----------------------------------------------------------------------------
// Определения для обработки значений сигналов: 
// ----------------------------------------------------------------------------
#define ADDR_nUSE               0xFFFFF     // Если сигнал не используется, сервер самостоятельно назначает ADDR из диапазона 0x100000 - 0x1FFFFF

#define HW_ADDR_NA              0
#define UNRELATED               -1

// Старшие 8 разрядов поля hw_addr зарезервированны под служебные признаки
//
#define BIT_OFFSET          (1 << 30)
#define BIT_STICKY          (1 << 29)       //аналог флага накопительного сигнала (игнорирует нулевые значения)
#define BIT_FILE            (1 << 28)       // переменная - файл. Для сигналов, которые заполняются в несколько заходов.
#define BIT_MASK            (1 << 27)       // позволяет извлекать битовую маску. 
#define PUA_CNTR_BIT        (1 << 26)       // для подадресов ПУА флаг необходимости считать контрольную сумму
#define BIT_MULTIPLICATION  (1 << 25)       // позволяет находить произведение 2-х сигналов
// ----------------------------------------------------------------------------
// Определение для поля ID: 
#define ARC_BY_NAME         "$"   // Значение сигнала будет сохраняться в файл, имя которого будет = имени сигнала.
#define ARC_DISABLE         ""    // Значение сигнала не будет сохраняться в файле
// ----------------

#define FIELD_SADDR         8   // поле подадреса .
#define FIELD_WNUM          0   // размер (кол-во слов)


// для дочерних сигналов с признаком "BIT_MASK" используется след. поля
#define FIELD_BYTEMASK      16  // поле маски
#define FIELD_RSHIFT        8   // кол-во сдвигаемых разрядов
// для дочерних сигналов типа "TYPE_ID_STRUCT" используется след. поля
// 000SSOOO - SS - размер поля в байтах, OOO - OFFSET - смешение начала в поле родителя
#define FIELD_OFFSET        0   // сдвиг поля смещения 
#define FIELD_SIZE          12  // размер 
// -----------------------------------------------------------------------------
// для опции "BIT_FILE" - определены след поля 
#define FIELD_REC_SIZE        0   // размер одной записи
#define FIELD_REC_NUM        16   // кол-во записей в файле.
//------------------------------------------------------------------------------
// ----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// функции библиотеки:
//----------------------------------------------------------------------------
extern void Log_DBG(const char * text, ...); // лог по умолчанию (замена printf)
extern void Log_ERR(const char * text, ...); // лог по умолчанию (замена printf)
extern void Log_Ex(const char * ID, char LEVEL, char * text, ...); //
extern char *  Log_buf2str(unsigned char *d,int size);
extern void mcAppendSignal(TMCSignalsRec * a, char initialFlag);
extern void mcAppendSignals(TMCSignalsRec a[], int count, char initialFlag);
extern void mcAppendSignalsEx(TMCSignalsRecEx a[], int count, char initialFlag);
extern int mcProcessing(void);
extern TMCUI * mcInit(int argc, char **argv, int LotosPort, char mode);
extern int mcStart(void);
extern int mcTestLib(int arg);
extern int mcTestLibEx(int arg);
extern int mcSetSignalValueByte(int index, unsigned char value, char flag);
extern TSignalProperty * mcGetSignalProperty(int index);
extern int mcGetSignalsCount(void);
extern int mcAddRequestForSignal(int index, Tsub_param_rec * value);
extern char * mcGetSignalParam(int index, char * keyName);
extern int mcSetSignalParam(int index, char * keyName, char * keyValue);
//----------------------------------------------------------------------------
extern void mcAppendUserLineDriver(char * DriverName,   char * DriverParams, TUserLineCallBack * proc, int EventMask);
extern void mcAppendUserProtocol  (char * ProtocolName, char * ProtocolParams, TPMNGCallBack * proc);
//----------------------------------------------------------------------------
extern int mcAddNotifyToSignal(char * mask, TNotifyToSignal * callBack, int idOfSubscriber);
extern int mcSendToLine(int id, char * buffer, int size);

extern int mcGetArgValue(char * paramName, char argFrmt, void * pValue);

extern void mcAppendUserAlgorithm(char * algName, TExtAlgorithmInit fInit) ;

#pragma pack(pop)





#endif	/* COREUINTERFACE_H */

