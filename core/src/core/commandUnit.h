/* 
 * File:   commandUnit.h
 * Author: Dunaev
 *
 * Created on 21 Август 2014 г., 9:15
 */

#ifndef COMMANDUNIT_H
#define	COMMANDUNIT_H

#include "../main.h"

#define	REQUEST_DIR_GET         0
#define	REQUEST_DIR_SET         1

//-----------------------------------------------------------------------------
int CMD_ADD_Request    (TSystemState * State, int interfaceIndex, int timeLimit, int signalIndex, int typeRequest, int dir);
int CMD_PRINT_Request  (TSystemState * State);
int CMD_TEST_Request   (TSystemState * State, int signalIndex, int typeRequest);
int CMD_TEST_AnyRequest(TSystemState * State, int interfaceIndex, int typeRequest);
int CMD_ACK_Request    (TSystemState * State, int RequestIndex, char ackType);
int CMD_DEL_Request    (TSystemState * State, int RequestIndex);
int CMD_CLEAN_TTL_Request(TSystemState * State);
//-----------------------------------------------------------------------------
int INT_GET_IndexByID(TSystemState * State, unsigned int ID);

//-----------------------------------------------------------------------------
int GetSignalByName(char * signalName);
int CompareSignalName(char * signalName, char * mask);
//-----------------------------------------------------------------------------
int InitSignalValueBase(TSystemState * State, int emulMode, int signalCountLimit);
// функция производит поиск в массиве Signals[] по полям "interface" и "addr" и задает новое значение сигналу
int SetSignalValue  (TSystemState * State, int  interfaceID, int addr, Tsub_param_rec * value, char * flag);
int SetSignalValueEx(TSystemState * State, int  index, Tsub_param_rec * value, char * flag);
//int SetSignalValueByIndex(TSystemState * State, int i, Tsub_param_rec * value, char * flag);
int SetSignalValueByIndex(TSystemState * State, int i, Tsub_param_rec * value, char * flag, int idOfSubscriber);
int SetSignalValueExNotify(TSystemState * State, int index, Tsub_param_rec * value, char * flag, int idOfSubscriber);
//-----------------------------------------------------------------------------
//--- доступ к служебным полям сигналов ---------------------------------------
int SL_SET_FieldByID(TSystemState * State, int index, int ID, int value);
int SL_GET_FieldByID(TSystemState * State, int index, int ID);
int SL_GET_IndexByID_ADDR (TSystemState * State, int interfaceID, int addr);
int SL_GET_IndexByID_MADDR(TSystemState * State, int interfaceID, int master_addr);
int SL_GET_Size(char typeId);
int SL_SET_Flag(int index, char value);
int SL_TypeStr2TypeID (char * TypeStr);
int SL_TypeID2TypeStr (int TypeID, char * TypeStr);
//-----------------------------------------------------------------------------
//int VALUE_GET_Size(char typeId);
int VALUE_SET_TYPE   (Tsub_param_rec * rec, unsigned int type);
int VALUE_SET_DWord  (Tsub_param_rec * rec, unsigned int value);
int VALUE_SET_Word   (Tsub_param_rec * rec, int value);
int VALUE_SET_Byte   (Tsub_param_rec * rec, unsigned char value);
int VALUE_SET_Float  (Tsub_param_rec * rec, float value);
int VALUE_SET_Double (Tsub_param_rec * rec, double value);
int VALUE_SET_Struct (Tsub_param_rec * rec, char * valuebuf, int size);
int VALUE_SET_String (Tsub_param_rec * rec, char * valuebuf, int size);
int VALUE_SET_String0(Tsub_param_rec * rec, char * valuebuf);
int VALUE_COPY       (Tsub_param_rec * rec, Tsub_param_rec * value);
//-----------------------------------------------------------------------------
int     VALUE_CONVERT_to_Int     (Tsub_param_rec * rec);
double  VALUE_CONVERT_to_Double  (Tsub_param_rec * rec);
char  isVALUE_CONVERT_to_Double  (Tsub_param_rec * rec);
double  VALUE_CONVERT_from_Double(Tsub_param_rec * rec, double value);
char * VALUE_CONVERT_to_Text(Tsub_param_rec * rec);
//-----------------------------------------------------------------------------
char* get_ParamValue(char * ParamsLine, char * ParamName);
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//  source: 0 - предопределенные сигналы,
//  source: 1 - сигналы пользователя, загрузка от внешнего модуля
//  source: 2 - сигналы пользователя, загрузка от скрипта (lua)
#define OBJ_SOURCE_UNDEF    0
#define OBJ_SOURCE_EMBEDDED 1
#define OBJ_SOURCE_USER     2
#define OBJ_SOURCE_SCRIPT   3
TSignalsRec * BuildSignalRec(TSystemState * State, char * signalName, char * signalParams, int proc);
// Считаем что память для TSignalsRec * a уже виделена. Используем только указатель а
void * AppendSignal(TSystemState * State, TSignalsRec * a, char source, char initialFlag);

TSignalsRec * BuildSignalRecEx(TSystemState * State, TMCSignalsRecEx * signal);

TLineRec * BuildLineRec(TSystemState * State, char * lineName, int LineID, char * lineParams, int proc);
void  AppendLine(TSystemState * State, TLineRec * a, char source);

TProtFramingRec * BuildFramingRec(TSystemState * State, char * lineName, char * lineParams, int proc);
void AppendFramingUnit(TSystemState * State, TProtFramingRec * a, char source);
TProtocolRec * BuildProtocolRec(TSystemState * State, char * lineName, char * lineParams, int proc);
void AppendProtocolUnit(TSystemState * State, TProtocolRec * a, char source);
TPollingRec * BuildPollingRec(TSystemState * State, char * lineName, char * lineParams, int proc);
void AppendPollingUnit(TSystemState * State, TPollingRec * a, char source);


void AppendLinkDriver(TSystemState * State,  char source, char * DriverName, char * DriverParams, void * proc, int EventMask);
void coreLinkOpen(char source, int LineID, char * DriverName, char * DeviceName, char * linkParam, char * ProtocolName, char * ProtocolParam);
int  coreLinkTx  (char source, int LineID, char * buffer, int bufferSize);
int  coreLinkRx  (char source, int LineID, char * buffer, int * bufferSize, int timeOut);
//-----------------------------------------------------------------------------
int RunUserCallBack    (char typeCallBack, void * ext, int  index, char * nameSignalReq, unsigned short int  addr, Tsub_param_rec * value, char * flag, char ExtMode, int srcInterface) ;
int RunFramingCallBack (char typeCallBack, int  index, char * buffInput, int *buffInputSize, char * frameOutput, int * frameOutputSize, char * ErrorText) ;
int RunProtocolCallBack(char typeCallBack, int LineIndex, int  index, void * UI, char * signalName, Tsub_param_rec * value, char * pktInBuffer, int pktInSize, char * pktOutBuffer, int * pktOutSize, char * ErrorText); 
int RunLineCallBack    (char typeCallBack, int  index, TMACDriverArg arg, char * ErrorText) ;
int RunMacDriverCallBack(int LineIndex, TMCExtDriver * event) ;
int RunMacDriverCallBackEx(char typeCallBack, int LineIndex, TMACDriverArg arg, char * ErrorText);
int mcSetSignalValueEx(int  index, Tsub_param_rec * value, char flag);
Tsub_param_rec * mcGetSignalValue(int  index);
TSignalsRec *    mcGetSignalRec(int  index);


#define NOTIFY_ID_SUBSCRIBER_UNDEFINE       0x000
#define NOTIFY_ID_SUBSCRIBER_UNITOOL        0x100
#define NOTIFY_ID_SUBSCRIBER_GUI_SCITER     0x101
int mcNotifyToSignal(char * signalName, Tsub_param_rec * value, char flag);
int mcNotifyToSignalEx(char * signalName, Tsub_param_rec * value, char flag, int idOfSubscriber);


extern TMC_Set_Func UI_set_Funcs;
extern TMC_SG_Func  UI_sg_Funcs;
extern TMC_Get_Func UI_get_Funcs;
extern TMC_NOTIFY_Func UI_notify_Funcs;
extern TMC_LAYER_Func UI_layer_Funcs;

#endif	/* COMMANDUNIT_H */

