#ifndef ROSBAG_UTILS_H
#define ROSBAG_UTILS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#if 0
    // старое описание. 
typedef enum {
    RadarInt16,
    RadarInt16Array,
    RadarInt32,
    RadarInt32Array,
    RadarFloat32,
    RadarFloat32Array,
    RadarFloat64,
    RadarFloat64Array,
    RadarArgumentsArray,
    RadarFrame,
    RadarSerialNumb,
    RadarTemper,
    RadarChirps,
    RadarChanels,
    RadarPoints,
    RadarTMCalib,
    RadarTMPoly,
    RadarLeak,
    RadarUndef
} ros_message_type;

#endif

typedef enum {
    RadarInt16,
    RadarInt16Array,
    RadarInt32,
    RadarInt32Array,
    RadarFloat32,
    RadarFloat32Array,
    RadarFloat64,
    RadarFloat64Array,
    RadarArgumentsArray,
    RadarFrame,
    RadarSerialNumb,
    RadarTemper,
    RadarChirps,
    RadarChanels,
    Device,       // дополнительные типы (использовал Сергей Ч. в программе стримера)
    HardVersion,  // <----
    SortVersion,  // <----
    NumSensors,   // <----
    RadarPoints,  // <----
    RadarTMCalib,
    RadarTMPoly,
    RadarLeak,
    RadarUndef
} ros_message_type;

typedef union {
    int16_t  int16;
    int16_t *int16Array;
    int32_t  int32;
    int32_t *int32Array;
    float    float32;
    float   *float32Array;
    double   float64;
    double  *float64Array;
    double  *argumentsArray;
    float   *radarFrame;
} ros_message_data;


typedef enum {
    ROSBAG_READ_NEXT, // Читаем следующее поле
    ROSBAG_READ_BREAK_OK, // выход из функции чтения при успешном чтении
    ROSBAG_READ_EOF // обнаружен конец файла

} rosbag_read_cmd;

typedef rosbag_read_cmd(rosbag_read_callback)(ros_message_type msg_type, double time, void * buffer, uint32_t size);

// 26.05.22 добавил выдачу 
typedef rosbag_read_cmd(rosbag_read_topic_callback)(ros_message_type msg_type, const char * topic, double time, void * buffer, uint32_t size);


// Прочитать данные из rosbag файла
rosbag_read_cmd rosbag_read_data(rosbag_read_callback * read_callback, uint32_t num);
rosbag_read_cmd rosbag_read_data_topic(rosbag_read_topic_callback * callback, uint32_t num);


typedef enum {
    ROSBAG_READ,
    ROSBAG_WRITE,
    ROSBAG_APPEND
} rosbag_open_mode;

/***************************
 * msg_type - тип сообщений
 * msg_num - порядковый номер сообщения
 * size - размер массива данных, задается внутри функции
 * data - данные сообщения
 * file_name - имя rosbag файла
 ***************************/

// Открыть rosbag файл
int rosbag_open_file(rosbag_open_mode open_mode, const char *file_name, int num);
// Закрыть rosbag файл
int rosbag_close_file(int num);

// Записать данные в rosbag файл
int rosbag_write_data(ros_message_type msg_type, uint32_t size, ros_message_data data, uint32_t sec, uint32_t nsec, int num);



#ifdef __cplusplus
}
#endif

#endif // ROSBAG_UTILS_H
