#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "rosbag_utils.h"
#include "rosbagInterface.h"
#define ROS_INDEX   0   // [0 | 1] - два файла (объекта ROS) доступны

static int rosIndex = -1;
static rosbag_open_mode rosCurrentMode = ROSBAG_WRITE;
static char * rosFrameFileName = "";
static uint32_t rosLastTime_sec = 0;
static uint32_t rosLastTime_nsec = 0;

int rosDriverInit(char * frameFileName, rosbag_open_mode mode) {
    int result = 0;
#ifndef ROS_DISABLE
if (rosIndex != -1)    {
    printf("[ROS] ROS is still in use --> close()\n");
    rosDriverClose();
}
    rosFrameFileName = frameFileName;
    printf("[ROS] Init(%s)\n", rosFrameFileName);
    if (frameFileName[0] == 0)
     result = rosbag_open_file(mode, NULL, ROS_INDEX);
    else
     result= rosbag_open_file(mode, rosFrameFileName, ROS_INDEX);
    rosCurrentMode = mode;
    rosIndex = 0;
#else
    printf("[ROS: DISABLED] Init(%s)\n", frameFileName);
#endif    
    return result;
}

void rosDriverSetFrameDataFile(char * frameFileName) {
    rosFrameFileName = frameFileName;
    printf("[ROS] Frame data: %s\n", rosFrameFileName);
}

int rosDriverClose(void) {
#ifndef ROS_DISABLE    
    printf("[ROS] Close(%d)\n", rosIndex);
    if (rosIndex != -1) {
        rosbag_close_file(ROS_INDEX);
    }
    rosIndex = -1;
#else
    printf("[ROS: DISABLED] Close(%d)\n", rosIndex);
#endif    
    return 0;
}

void rosSetTime(uint32_t sec, uint32_t nsec) {
    rosLastTime_sec = sec;
    rosLastTime_nsec = nsec;
}

int rosDriverSetReadMode(void) {
#ifndef ROS_DISABLE    
    if ((rosCurrentMode == ROSBAG_WRITE) || (rosCurrentMode == ROSBAG_APPEND)) {
        rosCurrentMode = ROSBAG_READ;
        rosbag_close_file(ROS_INDEX);
        int result = rosbag_open_file(rosCurrentMode, rosFrameFileName, ROS_INDEX);
        printf("[ROS] ROSBAG : READ [%s]: %d\n", rosFrameFileName, result);
    }
#else
    printf("[ROS: DISABLED] rosDriverSetReadMode()\n");
#endif    

}

int rosDriverSetWriteMode(void) {
#ifndef ROS_DISABLE    
    if (rosCurrentMode == ROSBAG_READ) {
        rosCurrentMode = ROSBAG_APPEND;
        rosbag_close_file(ROS_INDEX);
        rosbag_open_file(rosCurrentMode, NULL, ROS_INDEX);
        printf("[ROS] ROSBAG : WRITE\n");
    }
#else
    printf("[ROS: DISABLED] rosDriverSetReadMode()\n");
#endif    

}

static TROSAuxiliaryParam * frameAuxParam;
static void * rdFrameBuffer;
static  uint32_t rdBufferSize;
static  double * rdFrameTime;
static  uint32_t rdFrameSize;


static rosbag_read_cmd read_callback(ros_message_type msg_type, double time, void * buffer, uint32_t size) {
    // Основные данные, которые ждем - это кадр.
    // Другие сообщения которые получаем до кадра запомним в frameAuxParam
    //printf("[ROS: READ] msg_type %d, size: %d\n", (int)msg_type, (int)size);
    // Сергей делает запись данных кадра не как RadarFrame, а как RadarInt32Array
    if ((msg_type == RadarInt32Array)&&(size > 1024)) {
        if (rdFrameTime)
            *rdFrameTime = time;
        if (size <= rdBufferSize) {
            memcpy(rdFrameBuffer, buffer, size);
            rdFrameSize = size;
            return ROSBAG_READ_BREAK_OK;
        } else {
            printf("[ROS: ERROR!!!] Buffer size: %d, Frame size: %d\n", (int)rdBufferSize, (int)size);
            return ROSBAG_READ_EOF;
        }
    } else {
        if (frameAuxParam) {
            for (int i = 0; 1; i++) {
                if (frameAuxParam[i].id < RadarUndef) {
                    if (frameAuxParam[i].id == msg_type) {
                        // сохраняю оригинальный размер (может не все влазить в буфер!)
                        frameAuxParam[i].size = size;
                        if (size > sizeof(frameAuxParam[i].valueBuffer))
                            size = sizeof(frameAuxParam[i].valueBuffer);
                        // копируем то, что влезет в короткий буфер TROSAuxiliaryParam
                        memcpy((void*)&frameAuxParam[i].valueBuffer[0], buffer, size);
                    }
                } else break;
            }
        }
        return ROSBAG_READ_NEXT;
    }
}

// Возврат: 0 : EOF, 1..N: Размер, -1 : ERROR, 
int rosReadDataFrame(void * pData, uint32_t bufferSize, double *time, TROSAuxiliaryParam * auxParam, char * comment) {

#ifndef ROS_DISABLE 
    //printf("[ROS: DISABLED] rosReadDataFrame(%s)\n", comment);
    frameAuxParam = auxParam;
    rdFrameBuffer = pData;
    rdBufferSize = bufferSize;
    rdFrameTime = time;
    rdFrameSize = 0;
    rosbag_read_cmd r = rosbag_read_data(read_callback, ROS_INDEX);
    if (r == ROSBAG_READ_BREAK_OK) return rdFrameSize;
    if (r == ROSBAG_READ_EOF) return 0;
#else
    printf("[ROS: DISABLED] rosReadDataFrame(%s)\n", comment);
    return -1;
#endif    
    return -1;
}


typedef struct {
	TROSAuxiliaryParamByTopic * optList;
	char * topic; // Идентификатор топика данных
	char * timeTopic; // Идентификатор топика меток времени
	void * buffer; 
	uint32_t bufferSize;
	uint32_t frameSize;
	double * pTime;
	double * pTimeArray;
} TBagFrameReadParam;

static TBagFrameReadParam bagFrameReadParam;


static rosbag_read_cmd readEx_callback(ros_message_type msg_type, const char * topic, double time, void * buffer, uint32_t size) {
	
    // Основные данные, которые ждем - это кадр.
    // Другие сообщения которые получаем до кадра запомним в frameAuxParam
    //printf("[ROS: READ] msg_type %d, size: %d, topic: %s\n", (int)msg_type, (int)size, topic);
    if (strcmp(topic, bagFrameReadParam.topic) == 0) {
	    // printf("[ROS: READ] * data <size: %d>\n", (int)size);
        if (bagFrameReadParam.pTime)
            *bagFrameReadParam.pTime = time;
        if (size <= bagFrameReadParam.bufferSize) {
            memcpy(bagFrameReadParam.buffer, buffer, size);
            bagFrameReadParam.frameSize = size;
            return ROSBAG_READ_BREAK_OK;
        } else {
            printf("[ROS: ERROR!!!] Buffer size: %d, Frame size: %d\n", (int)bagFrameReadParam.bufferSize, (int)size);
            return ROSBAG_READ_EOF;
        }
    } else {
		// Извлеч метки времени чирпов
		if (bagFrameReadParam.timeTopic && bagFrameReadParam.pTimeArray)
		    if (strcmp(topic, bagFrameReadParam.timeTopic) == 0) {
				memcpy((void*)bagFrameReadParam.pTimeArray, buffer, size);
			}
		
        if (bagFrameReadParam.optList) {
            for (int i = 0; 1; i++) {
                if (bagFrameReadParam.optList[i].topic != NULL) {
                    if (strcmp(bagFrameReadParam.optList[i].topic, topic) == 0) {
					    // printf("[ROS: READ] * PARAM %s:<size: %d>\n", topic, (int)size);
                        // сохраняю оригинальный размер (может не все влазить в буфер!)
                        bagFrameReadParam.optList[i].size = size;
                        if (size > sizeof(bagFrameReadParam.optList[i].valueBuffer))
                            size = sizeof(bagFrameReadParam.optList[i].valueBuffer);
                        // копируем то, что влезет в короткий буфер TROSAuxiliaryParam
                        memcpy((void*)&bagFrameReadParam.optList[i].valueBuffer[0], buffer, size);
                    }
                } else break;
            }
        }
        return ROSBAG_READ_NEXT;
    }
}


// Возврат: 0 : EOF, 1..N: Размер, -1 : ERROR, 
int rosReadDataFrameEx(char * dataTopic, void * pData, uint32_t bufferSize, double * pOutTime, char * timeTopic, double * pOutTimeArray, TROSAuxiliaryParamByTopic * auxParam, char * comment) {

#ifndef ROS_DISABLE 
	bagFrameReadParam.frameSize = 0;
	bagFrameReadParam.topic = dataTopic;
	bagFrameReadParam.timeTopic = timeTopic;
	bagFrameReadParam.optList = auxParam;
	bagFrameReadParam.buffer = pData;
	bagFrameReadParam.bufferSize = bufferSize;
	bagFrameReadParam.pTime = pOutTime;
	bagFrameReadParam.pTimeArray = pOutTimeArray;
	
    rosbag_read_cmd r = rosbag_read_data_topic(readEx_callback, ROS_INDEX);
    if (r == ROSBAG_READ_BREAK_OK) return bagFrameReadParam.frameSize;
    if (r == ROSBAG_READ_EOF) return 0;
#else
    printf("[ROS: DISABLED] rosReadDataFrameEx(%s)\n", comment);
    return -1;
#endif    
    return -1;
}


//
int rosReadMessage(rosbag_read_callback * read_callback) {

#ifndef ROS_DISABLE 
    rosbag_read_cmd r = rosbag_read_data(read_callback, ROS_INDEX);
    if (r == ROSBAG_READ_BREAK_OK) return 1;
    if (r == ROSBAG_READ_EOF) return 0;
    
#else
    printf("[ROS: DISABLED] rosReadDataFrame(%s)\n", comment);
    return -1;
#endif    
    return -1;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Возврат: 0 - нет ошибок, -1: ошибка!
int rosWriteDataFrame(void * pData, uint32_t msg_num, uint32_t size, char * comment) {

    ros_message_data ptrRosData;

    int result = 0;

#ifndef ROS_DISABLE  
    rosDriverSetWriteMode();
    ptrRosData.radarFrame = (float*) pData;
    rosLastTime_nsec++;
    result = rosbag_write_data(RadarFrame, size, ptrRosData, rosLastTime_sec, rosLastTime_nsec, ROS_INDEX);
    if (result) {
        printf("[ROS] ROSBAG write frame error! (%d, msg_num:%d)[%s]\n", result, (int)msg_num, comment);
        return -1;
    }
#else
    printf("[ROS: DISABLED] rosWriteDataFrame(%s)\n", comment);
    return -1;
#endif    
    return result;
}
// Возврат: 0 - нет ошибок, -1: ошибка!
int rosWriteValueArray(ros_message_type dataType, ros_message_data uData, uint32_t size, char * comment) {

    int result = 0;

#ifndef ROS_DISABLE  
    rosDriverSetWriteMode();
    rosLastTime_nsec++;
    result = rosbag_write_data(dataType, size, uData, rosLastTime_sec, rosLastTime_nsec, ROS_INDEX);
    if (result) {
        printf("[ROS] ROSBAG write frame error! (%d)[%s]\n", result, comment);
        return -1;
    }
#else
    printf("[ROS: DISABLED] rosWriteDataFrame(%s)\n", comment);
    return -1;
#endif    
    return result;
}


int rosWriteValue(ros_message_type dataType, ros_message_data uData, char * comment) {
    return rosWriteValueArray(dataType, uData, 1, comment);
}


int rosDriverTest() {

#ifndef ROS_DISABLE    

#else
    printf("[ROS: DISABLE]\n");

#endif    

    return 0;
}

