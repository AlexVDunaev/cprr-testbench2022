#ifndef ROSBAG_INTERFACE_H
#define ROSBAG_INTERFACE_H


#include <stdint.h>
#include "rosbag_utils.h"

typedef struct {
    ros_message_type id;
    uint32_t size;
    uint8_t valueBuffer[20];  
} TROSAuxiliaryParam;

typedef struct {
    char * topic;
    uint32_t size;
    uint8_t valueBuffer[24];  
} TROSAuxiliaryParamByTopic;

int rosDriverInit(char * frameFileName, rosbag_open_mode mode);
void rosDriverSetFrameDataFile(char * frameFileName);

int rosDriverClose    (void);
int rosWriteValue     (ros_message_type dataType, ros_message_data uData, char * comment);
int rosWriteValueArray(ros_message_type dataType, ros_message_data uData, uint32_t size, char * comment);

int rosReadDataFrame(void * pData, uint32_t bufferSize, double *time, TROSAuxiliaryParam * auxParam, char * comment);
int rosReadDataFrameEx(char * dataTopic, void * pData, uint32_t bufferSize, double * pOutTime, char * timeTopic, double * pOutTimeArray, TROSAuxiliaryParamByTopic * auxParam, char * comment);
int rosWriteDataFrame (void * pData, uint32_t msg_num, uint32_t size, char * comment);
void rosSetTime(uint32_t sec, uint32_t nsec) ;
int rosReadMessage(rosbag_read_callback * read_callback);

#endif
