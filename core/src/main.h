#ifndef MAIN_H
#define MAIN_H


#include <sys/poll.h>
#include <pthread.h>
//#include "statestruct.h"	
//#include USER_HEADER
#include <time.h>
#include <stdio.h>

#include "core/coreUInterface.h"
#include "core/coreScriptInterface.h"
//#include "core/hash/hash.h"
//#include "core/search/srch.h"

#define	SERVER_SIGNALS_LIMIT            2000    // предельное кол-во сигналов с которым работает сервер
#define	SERVER_TITLE			(VERSION_TITLE)//  " ("   CREATE_DATE_TITLE ", " CREATE_TIME_TITLE ")")  //см. makefile "APRST SERVER V0.9"
#define	SERVER_CONFIG_FILE		"config.ini"



#define POLL_SYNC_LOCK        1
#define POLL_SYNC_UNLOCK      0
#define POLL_SYNC_INIT        -1
#define POLL_SYNC_FREE        -2

void Polling_synchronize(int lock, char * dbgText);

//-- определение идентификаторов протоколов
#define	BSAP_PROTOCOL		1
#define	RAW_PROTOCOL		0

#define	SERVER_BASE_PORT        4567
#define	SERVER_BUF_PORT_OFS     0
#define	SERVER_UNBUF_PORT_OFS	1

#define	SERVER_UNBUF_PORT	4558
#define	SERVER_BUF_PORT         4567

//-- определение идентификаторов для линий связи
#define	LOTOS_BUFFERED_LINE		100	// линия обмена c клиентами буферными данными
#define	LOTOS_UNBUFFERED_LINE		101	// линия обмена c клиентами данными

#define	LOTOS_BUFFERED_MODE_OFF         0
#define	LOTOS_BUFFERED_MODE_ON          1

#define	COMMAND_TTL_CONST_MS            5000    //время ожидания команды в очережи запроса.
//-- определение идентификаторов соединений
#define	SERIAL_CLIENT_TYPE	1
#define	TCP_FROM_CLIENT_TYPE	2   //соединения получающиеся при подключении клиентов к серверному сокету
#define	TCP_CLIENT_TYPE         5   //программа выступает в роли клиента. подключение к другому серверу.
//#define	TCP_CLIENT_TYPE		2
#define	TCP_SERVER_TYPE		3
#define	CONSOLE_CLIENT_TYPE     4
#define	REQUEST_LIST_SIZE       500

//---скорости порта связи (используются в main в функции p232_init())
#define	MB4800			000014 // придумал сам.
#define	MB9600			000015
#define	MB19200			000016
#define	MB38400			000017
#define MB57600 		0010001
#define MB115200 		0010002
//---

#ifndef POLLRDHUP
// #define POLLRDHUP		0x2000
 #define POLLRDHUP		0x0
#endif

#ifndef POLLHUP
 #define POLLHUP		0x0
#endif

#ifndef POLLERR
 #define POLLERR		0x0
#endif


//#define POLLRDHUP		0x2000
#define TIMEOUT 		100 //  2016.12.13 было 50

#define STR_MAX_SIZE            4095

#define SIGNAL_NAME_MAX_SIZE    127

#define	TCP_CLIENT_MAX		5	//кол-во сетевых клиентов с которыми система будет работать
#define	CLIENT_MAX		10	//общее число дискрипторов
//#define	LINE_COUNT		3	//кол-во последовательных линий связи
#define	CLIENT_STATUS_ON	1 //Клиент активен
#define	CLIENT_STATUS_OFF       56 //Клиент отключен
#define	CLIENT_STATUS_ERR	2 //Клиент возвращает ошибку (нужно пересоединиться)


#define	CLIENT_RX_SIZE		32*2048 //размер входного буфера
#define	CLIENT_TX_SIZE		32*2048 //размер выходного буфера
#define	CLIENT_CM_SIZE		2048 //размер буфера обработанных данных
#define	CLIENT_ERR_SIZE		255  //размер строки последней ошибки




typedef struct
{
	unsigned int	TimeOut;    // кол-во ошибок тайм-аута (нет ответа)
	unsigned int	CRC;        // несовпадение CRC
        unsigned char   LLC;        // ошибки драйвера канала (например ошибка манчестера)
}TErrorsStat;

typedef struct
{  
    char            flag;
    void   *        script_name;  // если имя функции скрипта задано по оно размещается тут
    int             script_func;  // функция скрипта. Либо используется script_name либо script_func
    int             master_index; // индекс "родительского" сигнала или -1 есль его нет. 
    int             slave_index;  // индекс первого связанного сигнала или -1 есль его нет. 
    char            disabled;
    float           K1;           // масштабный коэффициент для преобразования в физич. величину
    struct timespec lastReqTime;    //время формирования последнего запроса (для реализации паузы после запроса)
    FILE *          arcFile;      // дискриптор файла-архива 
    char            lockArchiving;  // флаг запрета архивирования сигнала (начальное значение: 0)
    int             errorsCount;  // кол-во ошибок позникших при опросе сигнала (общее)
    TErrorsStat     errors;       // ошибки по типу
    unsigned int    buffer;       // буферное значение сигнала (при обработки КУ на запись)
                                  // для большинства сигналов достаточно буфера (4б) 
    char            signalSource; // источник добавления сигнала
    char            lineIndex;    // индекс линии LLC[] на котором висит сигнал (заполняется при работе)
    void *          pSignalRec;   // начальные настройки сигнала
    char            signalParam[255]; // параметры сигнала при создании 
    Tsub_param_rec  v;            // текущее значение переменной
    Tsub_param_rec  reqValue;     // значение сигнала при размещении в очередь
} TValue;


typedef struct 
{
    char                name[SIGNAL_NAME_MAX_SIZE] ;        //обозначение сигнала в системе ("КАМАК.РД1.Д1")
    unsigned char       TYPE;   // тип сигнала 
    unsigned int        readingdelay;   // задержка при опросе. для сигналов (для записи = -1)
    TValue    *         data;   // указатель на данные сигнала
    unsigned char       interface; // идентификатор интерфейса, который обслуживает данный сигнал
    short int           block;  // [адрес] блок, в котором находится сигнал
    int                 addr;   // логический адрес сигнала
    int                 master_addr;   //логический адрес сигнала - источника данных.
    unsigned int        hw_addr;   // адрес сигнала в блоке
    //unsigned short int  hw_sb_addr;   // суб.адрес сигнала в блоке (доп. адресация)
    char                ID[SIGNAL_NAME_MAX_SIZE] ;        //Обозначение сигнала "ТП[1]"
    char                param[SIGNAL_NAME_MAX_SIZE] ;        //Обозначение сигнала "ТП[1]"
    union
        {
        TCoreCallBack*      CoreCallBack;
        TMCUserCallBack*      UserCallBack;
//        TUserCallBack*      callBackLua;
        unsigned int        callBackAddr;
        void *              pCallBack;
        };
} TSignalsRec;


typedef struct 
{
    char                name[SIGNAL_NAME_MAX_SIZE] ;        //обозначение драйвера в системе 
    char                params[SIGNAL_NAME_MAX_SIZE] ;      //параметры работы драйвера
    unsigned int        source;      // источник создания линии  USER / EMBEDDED
    int                 eventMask;   // События, которые драйвер обрабатывает
    union
        {
        TLineCallBack*      callBack;    // для встроенных драйверов
        TUserLineCallBack*  callBackExt; // для драйверов пользователя
        void *              callBackVoid;// 
        };
} TMACRec;


typedef struct 
{
    unsigned int        count;   
    TMACRec*            Drivers[255];               //максимальное кол-во драйверов
} TMAC;


//--- буфер для обслуживания линии связи ---
typedef struct //_buf
{
	unsigned char       Rx[CLIENT_RX_SIZE];
	unsigned int        RxSize;
	unsigned char       Tx[CLIENT_TX_SIZE];
	unsigned int        TxSize;
	unsigned char       RxOnTx[CLIENT_RX_SIZE]; //буфер RX для стадии автоприема
	unsigned int        RxSizeOnTx; //буфер RX для стадии автоприема
	unsigned char       PacketRx[CLIENT_RX_SIZE];
	unsigned int        PacketRxSize;
	unsigned char       PacketTx[CLIENT_RX_SIZE];
	unsigned int        PacketTxSize;
	unsigned char       FrameRx[CLIENT_RX_SIZE];
	unsigned int        FrameRxSize;
	unsigned char       FrameTx[CLIENT_RX_SIZE];
	unsigned int        FrameTxSize;
	unsigned char       ProtocolRx[CLIENT_RX_SIZE];
	unsigned int        ProtocolRxSize;
	unsigned char       ProtocolTx[CLIENT_RX_SIZE];
	unsigned int        ProtocolTxSize;
//	unsigned char       Protocol[CLIENT_CM_SIZE];   // буфер для протокола
//	unsigned int        ProtocolSize;
	union {
            unsigned char   Cmd[CLIENT_CM_SIZE];
            TCMD            CMD;
            };
	unsigned int        CmdSize;
	unsigned int        isCmdOut;
	unsigned char       CmdOut[CLIENT_CM_SIZE];
	unsigned int        CmdID;  // идентификатор посылки (команды)
	unsigned char       lastError[CLIENT_ERR_SIZE];
        TProtocolCallBack *  ProtocolCallBack;   //функция доступа к протоколу
        //void *  PBack;   //функция доступа к протоколу

} BufData;

typedef struct //_ClientInfo
{
	char Client_type;	// тип клиента
	char Client_ip[20];     // ip адрес для сетевых побключений
	char Client_prot;	// протокол на линии

} TClientInfo;


typedef struct
{
	char		LineStatus;	//статус линии: Отключен/Включен
	char		typeLine;	//тип линии
        //-------------- параметры работы сервера --------------------------
        char            BufferedMode;   // 0: режим работы в соответствии с настройками
                                        // 1: режим глобальной буферизации
                                        // 2: режим глобальной отмены буферизации
	unsigned int	State;		//состояние
	unsigned int	ExInit;		//Дополнительная опция при инициализации (параметр для пользователя)
	char		isSendReq;	//флаг. Отправлен ли запрос
	unsigned int	wait_time;	//время ожидания ответа 0.1 сек
	unsigned int	time_stamp;	//метка времени при посылке запроса
	unsigned char	reqNum;		//кол-во посылок запроса
	unsigned int	signalIndex;	//индекс последнего сигнала для которого был сформирован запрос
        struct timespec lastReqTime;    //время формирования последнего запроса (для реализации паузы после запроса)
        unsigned int    pollingDelay;   //Ограничение интенсивности опроса. пауза после ответа и перед след. запросом, мс
        int             signalstep;     // для сигналов, которые выдаются поэтапно, счетчик шагов. (используется совместно CALLBACK_RESULT_NEED_REPEAT)
        char            LineOption;     // дополнительная опция линии (для МКО выбор осн/рез. канала)
        int             reTryCount;     // счетчик повторов на интерфейсе
        int             reTryMax;       // кол-во  повторов на интерфейсе
        int             timeOutOption;  // опция таймаута
        
        TLineCallBack *  LineCallBack;   //функция доступа к драйверу интерфейса
} TLine;	


typedef struct 
{
    char                name[SIGNAL_NAME_MAX_SIZE] ;        //обозначение линии в системе 
    char                params[SIGNAL_NAME_MAX_SIZE] ;        //обозначение линии в системе 
    unsigned int        id;   
    TValue    *         data;     // указатель на данные сигнала
    unsigned int        source;   // источник создания линии SCRIPT / USER / EMBEDDED
    unsigned int        fd;       // дискриптор устройства (задается драйвером)
    char                DriverName[SIGNAL_NAME_MAX_SIZE] ;        //идентификатор драйвера линии
    char                DriverIndex;   // индекс драйвера устройства
    char                ProtocolIndex; // индекс драйвера протокола
    char                FramingIndex;  // индекс драйвера обертки
    pthread_t           pollThread;    // поток опроса (если драйвер линии потдерживает POLL)
    char                SelfIndex;     // данной структуры в массиве LLC
//    char                PollingIndex;  // индекс функции опроса
    
    // DriverName  - присваивается в момент вызова функции открытия драйвера
    void * extData; //
    //unsigned short int  hw_sb_addr;   // суб.адрес сигнала в блоке (доп. адресация)
    union
        {
        TLineCallBack*      callBack;
        TLineCallBack*      callBackExt;
        unsigned int        callBackLua;
        };
    BufData             buffers; // буфера для данной линии связи
    TLine               LineControl; // переменные для управления линиией
    union
        {
        TClientInfo             ipClientInfo;
        };
} TLineRec;

typedef struct 
{
    unsigned int        count;   
    TLineRec*           Lines[255];               //максимальное кол-во линий связи
} TLLC;


typedef struct 
{
    char                name[SIGNAL_NAME_MAX_SIZE] ;          //обозначение протокола в системе 
    char                params[SIGNAL_NAME_MAX_SIZE] ;        //обозначение линии в системе 
    char                framingName[SIGNAL_NAME_MAX_SIZE] ;   //обозначение обертки
    char                pollingName[SIGNAL_NAME_MAX_SIZE] ;   //обозначение драйвера опроса
    unsigned int        source;   // источник создания линии SCRIPT / USER / EMBEDDED
    union
        {
        TPMNGCallBack*      callBack;
        TPMNGCallBack*      callBackExt;
        unsigned int        callBackLua;
        void *              callBackVoid;
        };
} TProtocolRec;


typedef struct 
{
    unsigned int        count;   
    TProtocolRec*       Protocol[255];               //максимальное кол-во протоколов
} TProtocols;


typedef struct 
{
    char                name[SIGNAL_NAME_MAX_SIZE] ;        //обозначение протокола в системе 
    unsigned int        source;   // источник создания линии SCRIPT / USER / EMBEDDED
    union
        {
        TFramingCallBack*   callBack;
        TFramingCallBack*   callBackExt;
        unsigned int        callBackLua;
        };
} TProtFramingRec;


typedef struct 
{
    unsigned int        count;   
    TProtFramingRec*    Framing[255];               //максимальное кол-во модулей обарачивания
} TProtFraming;



typedef struct 
{
    char                name[SIGNAL_NAME_MAX_SIZE] ;        //обозначение протокола в системе 
    unsigned int        source;      // источник создания линии SCRIPT / USER / EMBEDDED
    unsigned int        nextDelay;   // задержка перед следующем вызовом
    struct timespec     lastCallTime;    // метка времени последнего запуска
    
    union
        {
        TCoreCallBack*      callBack;
        TMCUserCallBack*      callBackExt;
        TMCUserCallBack*      callBackLua;
        int                 callBackAddr;
        };
} TPollingRec;

typedef struct 
{
    unsigned int        count;   
    TPollingRec*        Polling[255];               
} TPolling;



typedef struct
{
    unsigned int        TYPE;        //тип запроса: REQ_TYPE_EXT - запрос значения сигнала от сетевого клиента. 
                                     // REQ_TYPE_INT - Запрос к железу.
    struct timespec     time;        //время получения запроса
    unsigned int        TTL;         //время жизни запроса, мс 
    int                 signalIndex; //Индекс сигнала в основной базе сигналов
    int                 interfaceIndex; //Индекс интерфейса от которого пришел запрос
    int                 dir;         // направление запроса
    void        *       reqValue;    // буфер (4Кб) для хранения значения запроса
}TRequest;

typedef struct
{
        int              count; // кол-во отложенных запросов в списке
        TRequest         R[REQUEST_LIST_SIZE];
}TRequests;

typedef struct
{
        int              current_index; // текущий индекс сигнала - события
        
}TProcessing;

typedef struct
{
        char              isWriteArc;
        FILE *            arcDataFile;  // дискриптор файла - архива (данные)
        FILE *            arcIdxFile;   // дискриптор файла - архива (индекс)
        //-------------- параметры работы сервера --------------------------
        char              globalBufferedMode;   // 0: режим работы в соответствии с настройками
                                                // 1: режим глобальной буферизации
                                                // 2: режим глобальной отмены буферизации
        
}TSystem;


typedef struct
{
#define NOTIFY_CALL_TYPE_SET      1     // идентификатор события на запись нового значения сигнала
    unsigned char	mask[255]; // маска сигнала
    int      id; // идентификатор рассылки
    TNotifyToSignal *   callBack;  // обработчик
} TNotifyItemToSignal;

typedef struct
{
#define NOTIFY_ITEM_TYPE_SIGNAL      1
    unsigned char       itemType; //
    unsigned char       useFlag;  // 0: удалено, !=0: используется
    union
    {
    TNotifyItemToSignal ToSignal;
    };
} TNotifyItem;


typedef struct
{
        int              NotifyTestCount;
        int              count;
        pthread_mutex_t  mutex4Notify; //
        TNotifyItem *    items[255]; // выделим память для указалелей на события
} TNotifyList;


typedef struct {  
    TValue * i[1];
} TPValue;


typedef struct {   
        int             MaxCount; // максимальное кол-во сигналов
        int             Count; // кол-во сигналов
        TPValue *       values;//указатели на данные сигналов
} TValues;

typedef struct {
    TValues         Values;
} TDataReserv;	


typedef struct //__attribute__((__packed__))  // используем атрибут для упаковки данной структуры (не глобально pack(1)!)
//typedef struct //_systemState
{
	//THwBlockState	state;
        int			clients;			//текущие число клиентов (заполненных дискрипторов)
   	struct pollfd   	fds [CLIENT_MAX];	//дискрипторы клиентов
   	TClientInfo		inf [CLIENT_MAX]; 	//информация о клиентах
	BufData			buf [CLIENT_MAX];	//буфер для работы с клиентами
	TLine			Line[CLIENT_MAX];	//описания линий обмена 
        TDataReserv             Data;
        TRequests               Requests;
        TProcessing             Processing;
        TSystem                 System;
        char			lockGlobalArchiving;    //флаг запрета записи в файлы архива
        char			useUniTool;             //флаг запуска uniTool
        char			useGUI;                 //флаг запуска окна
        char			fTestMode;              //флаг режима тестирования
        char			fDebug;                 //флаг полного логирования событий
        char			fDebugCore;             //флаг полного логирования событий всех вызовов
        char			fDebugScript;           //флаг полного логирования вызовов lua
        char			flag;
        char			running;                //флаг работы сервера
        struct timespec         serverStartTime;        //Время старта сервера
//        char			notifyBlockMask;        //для предотвращения зацикливания в ходе уведомлений от внеш. модулей
//    TOperationData	opData;				//оперативные данные
        TMAC                    MAC;                    //Управление доступом к каналам передачи. (драйвера устройств)
        TLLC                    LLC;                    //Логический уровень управления. Определяет лог. линию связи.
        TProtFraming            FramingUnits;           //модули оборачивания (подуровень протокола)
        TProtocols              Protocols;              //протоколы обмена (логика формирования пакета)
        TPolling                PollingUnits;           //Управление запросами
        TMCUI                   UI;
        TNotifyList             NotifyList;             // список уведомлений
        TScriptInstance  *      pScriptEngine;          // Указатель на структуру для работы со скриптом
        char			initMode;               // переменная mode при инициализации
        int                     RemotePort;              // Порт внешнего управления
        int                     argc;                   // сохраним параметры запуска
        char **                 argv;
}TSystemState;

int printSystemState(TSystemState * State);

extern TSystemState SystemState;




#endif
